//
//  PopupShoppingCartCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/30/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PopupShoppingCartCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var sizeProductLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
