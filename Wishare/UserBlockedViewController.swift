//
//  UserBlockedViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/13/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class UserBlockedViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var notFoundUserBlockedView: UIView!
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    var loader : Loader!
    var manager : Manager!
    var users : [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "UserSearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userSearchCell")
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)

        WishareAPI.blockedUsers { (users : [User], error : Error?) in
            if error == nil {
                
                self.users = users
                self.collectionView.reloadData()
                
                if self.users.count == 0 {
                    self.notFoundUserBlockedView.isHidden = false
                }
                
                RappleActivityIndicatorView.stopAnimation()
                
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension UserBlockedViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: "Desbloquear \(self.users[indexPath.row].username!)?", message: "Ele agora poderá enviar mensagem ou ver suas publicações. O Wishare não informará que você o desbloqueou.", preferredStyle: .actionSheet)
        let blockedAction = UIAlertAction(title: "Confirmar", style: .default) { ( _ ) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Desbloquear...", attributes: self.attributes)
            
            WishareAPI.unblockedUser(idUser: self.users[indexPath.row].id!, { (error : Error?) in
                if error  == nil {
                    self.users.remove(at: indexPath.row)
                    self.collectionView.reloadData()
                    if self.users.count == 0 {
                        self.notFoundUserBlockedView.isHidden = false
                    }
                    RappleActivityIndicatorView.stopAnimation()
                }
            })
            
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(blockedAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}

extension UserBlockedViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userSearchCell", for: indexPath) as! UserSearchCollectionViewCell
        
        cell.viewWithTag(101)?.removeFromSuperview()
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.users[indexPath.row].picture!)")
        
        cell.userLabel.text = self.users[indexPath.row].username
        
        
        if self.users[indexPath.row].profile_image!.type! == "local" {
            
            if self.users[indexPath.row].profile_image!.url == nil {
                cell.backgroundUserImageView.image = UIImage(named: "\(self.users[indexPath.row].profile_image!.url_local!)")
            } else {
                cell.backgroundUserImageView.image = UIImage(named: "\(self.users[indexPath.row].profile_image!.url!)")
            }
            
        } else {
            
            //let url = URL(string: "http://www.wishare.com.br/code/")!
            let urlBackground = urlBase.appendingPathComponent("\(self.users[indexPath.row].profile_image!.url!)")
            
            self.manager.loadImage(with: urlBackground, into: cell.backgroundUserImageView) { ( result, _ ) in
                 cell.backgroundUserImageView.image = result.value
            }

        }
        
        self.manager.loadImage(with: url, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        
        let blockedView : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.3)
            view.tag = 101
            return view
        }()
        
        cell.addSubview(blockedView)
        
        blockedView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
        blockedView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
        blockedView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
        blockedView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
        
        return cell
        
    }
    
}

extension UserBlockedViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 1.25, height: ((self.view.bounds.width / 3) - 2.5))
    }
    
}
