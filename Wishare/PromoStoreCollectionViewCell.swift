//
//  PromoStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PromoStoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var descriptionPromoLabel: UILabel!
    
    var delegate : PromoStoreCollectionViewCellDelegate!
    var row : String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gestureRecognizerPromoImageView =  UITapGestureRecognizer(target: self, action: #selector(pressPromoImageView))
        self.promoImageView.addGestureRecognizer(gestureRecognizerPromoImageView)
        
    }
    
    @objc func pressPromoImageView () {
        self.delegate.viewPromo(cell: self)
    }

}

protocol PromoStoreCollectionViewCellDelegate {
    func viewPromo(cell: PromoStoreCollectionViewCell)
}
