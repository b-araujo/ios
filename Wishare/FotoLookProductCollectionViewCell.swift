//
//  FotoLookProductCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class FotoLookProductCollectionViewCell : UICollectionViewCell {

    
    @IBOutlet weak var photoLookImageView: UIImageView!
    @IBOutlet weak var nameProductLookLabel: UILabel!
    @IBOutlet weak var brandProductLookLabel: UILabel!
    @IBOutlet weak var priceProductLookLabel: UILabel!
    @IBOutlet weak var addCardButton: UIButton!
    
    var indexPathNow : IndexPath?
    var delegate : FotoLookProductCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addCardButton.titleLabel?.lineBreakMode = .byWordWrapping
        self.addCardButton.titleLabel?.textAlignment = .center
    
        self.photoLookImageView.isUserInteractionEnabled = true
        
        let tapGestureRecognizerPhotoLookImageView = UITapGestureRecognizer(target: self, action: #selector(tapPhotoLook))
        self.photoLookImageView.addGestureRecognizer(tapGestureRecognizerPhotoLookImageView)
        
    }

    @IBAction func addCard(_ sender: UIButton) {
        self.delegate?.addCard(sender, indexPathNow)
    }
  
    @objc func tapPhotoLook() {
        self.delegate?.openProductDetail(cell: self, indexPath: indexPathNow!)
    }
    
}

protocol FotoLookProductCollectionViewCellDelegate {
    
    func addCard( _ sender : UIButton, _ indexPath : IndexPath?)
    func openProductDetail (cell : FotoLookProductCollectionViewCell, indexPath : IndexPath)
    
}
