//
//  FotoLookViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import STZPopupView

class FotoLookViewController: UIViewController {

    //fotoLookCell
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var shoppingModelViewBottomConstraint : NSLayoutConstraint!
    let collectionViewShoppginCart = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var post : Post!
    var loader : Loader!
    var manager : Manager!
    var isSelectedProduct : Int?
    
    var products : [String : [String]] = [:]
    
    var backIndicatorImage : UIImage?
    var backIndicatorTransitionMaskImage : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.tag = 10
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "FotoLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "fotoLookCell")
        
        
        // ---- Modal Carrinho de Compra
        
        let shoppingCartModel : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            return view
        }()
        
        self.view.addSubview(shoppingCartModel)
        
        shoppingCartModel.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        shoppingCartModel.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        shoppingCartModel.heightAnchor.constraint(equalToConstant: (self.view.bounds.height / 2) - 64).isActive = true
        self.shoppingModelViewBottomConstraint = shoppingCartModel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: +((self.view.bounds.height / 2) - 64))
        self.shoppingModelViewBottomConstraint.isActive = true
        
        let titleSize : UILabel = {
            let label = UILabel()
            label.text = "ADICIONADO AO CARRINHO"
            label.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let buttonExit : UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "Delete"), for: .normal)
            button.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            button.imageView?.contentMode = .scaleAspectFit
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(closeShoppingModel), for: .touchUpInside)
            return button
        }()
        
        let buyLook : UIButton = {
            let button = UIButton()
            button.layer.cornerRadius = 5
            button.backgroundColor = UIColor.lightGray
            button.setTitle("Ir para o carrinho de compras", for: .normal)
            button.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(goShoppingCart), for: .touchUpInside)
            return button
        }()
        
        buyLook.layer.masksToBounds = false
        buyLook.layer.shadowColor = UIColor.black.cgColor
        buyLook.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        buyLook.layer.shadowRadius = 2.0
        buyLook.layer.shadowOpacity = 0.24
        
        collectionViewShoppginCart.register(UINib(nibName: "PopupShoppingCartCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "popupShoppingCart")
        collectionViewShoppginCart.delegate = self
        collectionViewShoppginCart.dataSource = self
        collectionViewShoppginCart.tag = 75
        collectionViewShoppginCart.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: self.view.bounds.height - 20)
        collectionViewShoppginCart.backgroundView = blurEffectView
        
        collectionViewShoppginCart.isScrollEnabled = false
        collectionViewShoppginCart.translatesAutoresizingMaskIntoConstraints = false
        
        
        shoppingCartModel.addSubview(collectionViewShoppginCart)
        shoppingCartModel.addSubview(titleSize)
        shoppingCartModel.addSubview(buttonExit)
        shoppingCartModel.addSubview(buyLook)
        
        buttonExit.topAnchor.constraint(equalTo: shoppingCartModel.topAnchor, constant: 5).isActive = true
        buttonExit.rightAnchor.constraint(equalTo: shoppingCartModel.rightAnchor).isActive = true
        buttonExit.heightAnchor.constraint(equalToConstant: 40).isActive = true
        buttonExit.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleSize.topAnchor.constraint(equalTo: shoppingCartModel.topAnchor, constant: 5).isActive = true
        titleSize.leftAnchor.constraint(equalTo: shoppingCartModel.leftAnchor).isActive = true
        titleSize.rightAnchor.constraint(equalTo: buttonExit.leftAnchor).isActive = true
        titleSize.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        buyLook.leftAnchor.constraint(equalTo: shoppingCartModel.leftAnchor, constant: 15).isActive = true
        buyLook.rightAnchor.constraint(equalTo: shoppingCartModel.rightAnchor, constant: -15).isActive = true
        buyLook.bottomAnchor.constraint(equalTo: shoppingCartModel.bottomAnchor, constant: -10).isActive = true
        buyLook.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        collectionViewShoppginCart.leftAnchor.constraint(equalTo: shoppingCartModel.leftAnchor).isActive = true
        collectionViewShoppginCart.topAnchor.constraint(equalTo: titleSize.bottomAnchor).isActive = true
        collectionViewShoppginCart.rightAnchor.constraint(equalTo: shoppingCartModel.rightAnchor).isActive = true
        collectionViewShoppginCart.bottomAnchor.constraint(equalTo: buyLook.topAnchor, constant: -10).isActive = true
        
        
        // ---- Fim modal
        
        
        self.backIndicatorImage = self.navigationController?.navigationBar.backIndicatorImage
        self.backIndicatorTransitionMaskImage = self.navigationController?.navigationBar.backIndicatorTransitionMaskImage
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-left")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationBar()
    }
    
    
    func customNavigationBar () {
        let nav = (self.navigationController?.navigationBar)!
        let transparencia = UIImage(named: "transparente")
        nav.setBackgroundImage(transparencia, for: .default)
        nav.shadowImage = transparencia
        nav.backgroundColor = UIColor.clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        
        self.navigationController?.navigationBar.backIndicatorImage = self.backIndicatorImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = self.backIndicatorTransitionMaskImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showImageFotoLook" {
            
            let showImagePostViewController = segue.destination as! ShowImagePostViewController
            showImagePostViewController.pictureImageString = sender as? UIImage
            
        }
        
        if segue.identifier == "shareRepostSegue" {
            
            let repostViewController = segue.destination as! RepostViewController
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            repostViewController.imageRespost = senderValue[0] as? UIImage
            repostViewController.id = senderValue[1] as? String
            
        }
        
    }
    
    @objc func pressSelectedSize ( _ sender : UIButton, _ cell : FotoLookCollectionViewCell) {
    
        sender.backgroundColor = UIColor.black
        sender.setTitleColor(UIColor.white, for: .normal)

        if var product = self.products[sender.productID!] {
            
            print("EXISTE")
            
            if product[0] != sender.productSizeID {
                
                print("É DIFERENTE")
                let buttonzinho = sender.superview?.viewWithTag(Int(self.products[sender.productID!]![0])!) as! UIButton
                buttonzinho.backgroundColor = UIColor.white
                buttonzinho.setTitleColor(UIColor.black, for: .normal)
                
                self.products.removeValue(forKey: sender.productID!)
                
                if let productID = sender.productID {
                    self.products = [productID : [sender.productSizeID!, sender.productSize!]]
                }
                
            }
        
        } else {
            
            print("NAO EXISTE")            
            if let productID = sender.productID {
                self.products[productID] = [sender.productSizeID!, sender.productSize!]
            }
            
        }
        
    }
    
    @objc func pressBuyCompleteLook() {
        
        if self.products.count == self.post.products_info.count {
            
            // CODIGO DE ADICIONAR NA CARRINHO DE COMPRA
            WishareCoreData.showShoppingCart(completion: { (productsCart : [WS_ShoppingCart], _, error : Error?) in
                
                if error == nil {
                    
                    let cloneProducts = self.products
                    
                    for p in self.products {
                        for prod in productsCart {
                            if prod.id_product! == p.value[0] {
                                self.products.removeValue(forKey: p.key)
                            }
                        }
                    }
                    
                    var finishAddShoppingCart = 0
                    
                    for p in self.post.products_info {
                        if let _ = self.products[p.id!] {
                            
                            WishareCoreData.addShoppingCart(name: p.name!, size: self.products[p.id!]![1], color: p.color!, quantity: "1", price: (p.promo_price! == "") ? p.price! : p.promo_price!, idProduct: self.products[p.id!]![0], idStore: self.post.store!.id!, nameStore: self.post.store!.id!, completion: { ( _, error : Error?) in
                                if error == nil {
                                    
                                    finishAddShoppingCart += 1
                                    
                                    if finishAddShoppingCart == self.products.count {
                                        
                                        self.products = cloneProducts
                                        self.dismissPopupView()
                                        self.collectionViewShoppginCart.reloadData()
                                        
                                        UIView.animate(withDuration: 0.4, animations: {
                                            self.shoppingModelViewBottomConstraint.constant = 0
                                            self.view.layoutIfNeeded()
                                        })
                                        
                                    }
                                    
                                }
                            })

                            
                        }
                        
                    }
                    
                }
                self.products.removeAll()
                self.dismissPopupView()
                
            })
            
            
            
            
        } else {
            let alertController = UIAlertController(title: nil, message: "Selecione um tamanho para cada produto!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @objc func closePopup () {
        dismissPopupView()
    }
    
    // ---- Modal
    
    @objc func closeShoppingModel() {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
            self.view.layoutIfNeeded()
        })
        
    }
    
    @objc func goShoppingCart () {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
            self.view.layoutIfNeeded()
        })
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    // ---- Fim
    
    @IBAction func buyCompleteLook(_ sender: UIButton) {
        
        self.products.removeAll()
        
        let popup = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: self.view.bounds.height - 205))
        
        let titleSize : UILabel = {
            let label = UILabel()
            label.text = "SELECIONE O TAMANHO"
            label.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let buttonExit : UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "Delete"), for: .normal)
            button.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            button.imageView?.contentMode = .scaleAspectFit
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
            return button
        }()
        
        let buyLook : UIButton = {
            let button = UIButton()
            button.layer.cornerRadius = 5
            button.backgroundColor = UIColor.lightGray
            button.setTitle("COMPRAR LOOK COMPLETO", for: .normal)
            button.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(pressBuyCompleteLook), for: .touchUpInside)
            return button
        }()
        
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewFlowLayout)
        
        collectionView.register(UINib(nibName: "PopupCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "popupCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.tag = 11
        collectionView.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: self.view.bounds.height - 20)
        collectionView.backgroundView = blurEffectView
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        popup.addSubview(collectionView)
        popup.addSubview(titleSize)
        popup.addSubview(buttonExit)
        popup.addSubview(buyLook)
        
        buttonExit.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
        buttonExit.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        buttonExit.heightAnchor.constraint(equalToConstant: 40).isActive = true
        buttonExit.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleSize.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
        titleSize.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
        titleSize.rightAnchor.constraint(equalTo: buttonExit.leftAnchor).isActive = true
        titleSize.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        buyLook.leftAnchor.constraint(equalTo: popup.leftAnchor, constant: 15).isActive = true
        buyLook.rightAnchor.constraint(equalTo: popup.rightAnchor, constant: -15).isActive = true
        buyLook.bottomAnchor.constraint(equalTo: popup.bottomAnchor, constant: -10).isActive = true
        buyLook.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        collectionView.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: titleSize.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: buyLook.topAnchor, constant: -10).isActive = true
        
        
        let popupConfig = STZPopupViewConfig()
        popup.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        popupConfig.cornerRadius = 5
        popupConfig.overlayColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        popupConfig.showAnimation = .slideInFromBottom
        popupConfig.dismissAnimation = .slideOutToBottom
        
        presentPopupView(popup, config: popupConfig)
        
    }
    
    @IBAction func moreBarButton(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let denunciarAction = UIAlertAction(title: "Denunciar Imagem", style: .default) { (action : UIAlertAction) in
            
            WishareAPI.reportProblem("Denunciar Imagem", "Imagem denunciada do id: \(self.post.id!) da loja \(self.post.store!.name!)", { (error : Error?) in })
            let alertControllerConfirm = UIAlertController(title: "Algo está errado?", message: "Obrigado pela informação", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertControllerConfirm.addAction(okAction)
            self.present(alertControllerConfirm, animated: true, completion: nil)
            
        }
        
        let linkAction = UIAlertAction(title: "Link Quebrado", style: .default) { (action : UIAlertAction) in
            
            WishareAPI.reportProblem("Link Quebrado", "Link Quebrado do id: \(self.post.id!) da loja \(self.post.store!.name!)", { (error : Error?) in })
            let alertControllerConfirm = UIAlertController(title: "Algo está errado?", message: "Obrigado pela informação", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertControllerConfirm.addAction(okAction)
            self.present(alertControllerConfirm, animated: true, completion: nil)
            
        }
        
        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(denunciarAction)
        alertController.addAction(linkAction)
        alertController.addAction(cancelarAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func completionHandler(activityType: UIActivityType?, shared: Bool, items: [Any]?, error: Error?) {
        
        if shared {
            
            let alertController = UIAlertController(title: "Sucesso", message: "Compartilhamento realizado com sucesso!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
}

extension FotoLookViewController : UICollectionViewDelegate {
    
}

extension FotoLookViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 11 {
            return self.post.products_info.count
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 75 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popupShoppingCart", for: indexPath) as! PopupShoppingCartCollectionViewCell
            
            if self.products.count == 1 {

                for prod in self.products {
                    for p in self.post.products_info {
                        for s in p.sizes {
                            if prod.value[0] == s.id! {
                             
                                cell.nameProductLabel.text = p.name!
                                
                                if let _ = self.products[p.pId!] {
                                    cell.sizeProductLabel.text = "Tamanho: \(prod.value[1])"
                                }
                                
                                if let _ = self.products[p.id!] {
                                    cell.sizeProductLabel.text = "Tamanho: \(prod.value[1])"
                                }
                                
                                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                                let url = urlBase.appendingPathComponent("files/posts-image/products/\(p.pId!)/\(p.picture!)")
                                
                                
                                self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
                                    cell.productImageView.image = result.value
                                }
                                
                            }
                        }

                    }
                    
                }
            
                
            } else if self.products.count > 1 {
                
                cell.nameProductLabel.text = self.post.text!
                
                
                cell.sizeProductLabel.text = nil
                
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.post.id!)/\(self.post.picture!)")
                
                
                self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
                    cell.productImageView.image = result.value
                }
                
                self.products.removeAll()
                
            }
            
            
            return cell
            
        }
        
        
        if collectionView.tag == 10 {
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "fotoLookCell", for: indexPath) as! FotoLookCollectionViewCell
            
            cell.delegate = self
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.post.id!)/\(self.post.picture!)")
            
            self.manager.loadImage(with: url, into: cell.photoProductLookImageView) { ( result, _ ) in
                cell.photoProductLookImageView.image = result.value
            }
            
            var value : Float = 0.0
            
            for i in 1...self.post.products_info.count {
                
                if self.post.products_info[i-1].promo_price! == "" {
                    value += (self.post.products_info[i-1].price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue
                    
                } else {
                    value += (self.post.products_info[i-1].promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue
                }
                
            }
            
            cell.priceProductLabel.text = "\(value.asLocaleCurrency)"
            
            
            let parcelMinimal : Float = 50.0
            var parcelIsValidate : Bool = false
            var parcel : Float = 0.0
            
            for i in (1...6).reversed() {
                
                parcel = value / Float(i)
                
                if parcel >= parcelMinimal {
                    cell.infoProductLabel.text = "Até \(i)x de \(parcel.asLocaleCurrency)"
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
            }
            
            if parcelIsValidate == false {
                cell.infoProductLabel.text = "Até 1x de \(parcel.asLocaleCurrency)"
            }
            

            
            if self.post.ilikes == "1" {
                cell.likeStoreImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeStoreImageView.image = UIImage(named: "Like")
            }
            
            if self.post.iwish == "1" {
                cell.wishStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            cell.nameStoreLabel.text = self.post.store!.name!
            cell.descriptionProductLabel.text = self.post.text!
            cell.likesStoreLabel.text = "\(self.post.likes!)"
            cell.wishStoreLabel.text = nil
            cell.sharesStoreLabel.text = nil
            
            return cell
            
        } else if collectionView.tag == 11 {
            
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "popupCell", for: indexPath) as! PopupCollectionViewCell
            
            cell.productNamePopupLabel.text = self.post.products_info[indexPath.row].name!
            cell.continueButton.isHidden = true
            cell.viewButton.isHidden = true
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.post.products_info[indexPath.row].pId!)/\(self.post.products_info[indexPath.row].picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.productPopupImageView) { ( result, _ ) in
                cell.productPopupImageView.image = result.value
            }
            
            
            print(self.post.products_info[indexPath.row].sizes.count)
            
            for (index, size) in self.post.products_info[indexPath.row].sizes.enumerated() {
                
                let button : UIButton = {
                    let button = UIButton()
                    button.setTitle(size.size!, for: .normal)
                    button.translatesAutoresizingMaskIntoConstraints = false
                    button.backgroundColor = UIColor.white
                    button.setTitleColor(UIColor.black, for: .normal)
                    button.layer.borderWidth = 1.0
                    button.layer.borderColor = UIColor.black.cgColor
                    button.layer.cornerRadius = 2.0
                    button.tag = Int(size.id!)!
                    // Setando informacoes
                    button.productID = self.post.products_info[indexPath.row].id
                    button.productSizeID = size.id
                    button.productSize = size.size
                    return button
                }()
                
                cell.addSubview(button)
                
                if index == 0 {
                    
                    button.leftAnchor.constraint(equalTo: cell.productPopupImageView.rightAnchor, constant: 10).isActive = true
                    button.topAnchor.constraint(equalTo: cell.productNamePopupLabel.bottomAnchor, constant: 10).isActive = true
                    button.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    button.widthAnchor.constraint(equalToConstant: 30).isActive = true
                    
                } else {
                    
                    button.leftAnchor.constraint(equalTo: cell.productPopupImageView.rightAnchor, constant: CGFloat(index * 50)).isActive = true
                    button.topAnchor.constraint(equalTo: cell.productNamePopupLabel.bottomAnchor, constant: 10).isActive = true
                    button.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    button.widthAnchor.constraint(equalToConstant: 30).isActive = true
                    
                }
                
                button.addTarget(self, action: #selector(pressSelectedSize(_:_:)), for: .touchUpInside)
                
            }
            
            
            let lineView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1)
                view.translatesAutoresizingMaskIntoConstraints = false
                return view
            }()
            
            
            cell.addSubview(lineView)
            
            lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
            lineView.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 10).isActive = true
            lineView.rightAnchor.constraint(equalTo: cell.rightAnchor, constant: -10).isActive = true
            lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
            
            return cell

            
            
            
            
        }
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "popupCell", for: indexPath) as! PopupCollectionViewCell
 
        cell.delegate = self
        cell.productNamePopupLabel.text = self.post.products_info[self.isSelectedProduct!].name!
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.post.products_info[self.isSelectedProduct!].pId!)/\(self.post.products_info[self.isSelectedProduct!].picture!)")
        
        
        self.manager.loadImage(with: url, into: cell.productPopupImageView) { ( result, _ ) in
            cell.productPopupImageView.image = result.value
        }
        
        
        print(self.post.products_info[self.isSelectedProduct!].sizes.count)
        
        for (index, size) in self.post.products_info[self.isSelectedProduct!].sizes.enumerated() {
            
            let button : UIButton = {
                let button = UIButton()
                button.setTitle(size.size!, for: .normal)
                button.translatesAutoresizingMaskIntoConstraints = false
                button.backgroundColor = UIColor.white
                button.setTitleColor(UIColor.black, for: .normal)
                button.layer.borderWidth = 1.0
                button.layer.borderColor = UIColor.black.cgColor
                button.layer.cornerRadius = 2.0
                button.tag = Int(size.id!)!
                // Setando informacoes
                button.productID = self.post.products_info[self.isSelectedProduct!].id
                button.productSizeID = size.id
                button.productSize = size.size
                return button
            }()
            
            cell.addSubview(button)
            
            if index == 0 {
                
                button.leftAnchor.constraint(equalTo: cell.productPopupImageView.rightAnchor, constant: 10).isActive = true
                button.topAnchor.constraint(equalTo: cell.productNamePopupLabel.bottomAnchor, constant: 10).isActive = true
                button.heightAnchor.constraint(equalToConstant: 30).isActive = true
                button.widthAnchor.constraint(equalToConstant: 30).isActive = true
                
            } else {
            
                button.leftAnchor.constraint(equalTo: cell.productPopupImageView.rightAnchor, constant: CGFloat(index * 50)).isActive = true
                button.topAnchor.constraint(equalTo: cell.productNamePopupLabel.bottomAnchor, constant: 10).isActive = true
                button.heightAnchor.constraint(equalToConstant: 30).isActive = true
                button.widthAnchor.constraint(equalToConstant: 30).isActive = true
                
            }
            
            button.addTarget(self, action: #selector(pressSelectedSize(_:_:)), for: .touchUpInside)
            
        }

        return cell
        
    }
    
}

extension FotoLookViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 75 {
            return CGSize(width: self.view.bounds.width, height: (self.view.bounds.height / 2) - 100)
        }
        
        if collectionView.tag == 10 {
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.height + ((self.view.bounds.width / 2.10) * CGFloat(self.post.products_info.count)))
        } else if collectionView.tag == 11 {
            return CGSize(width: self.view.bounds.width - 20, height: 125)
        }
        
        return CGSize(width: self.view.bounds.width - 20, height: 170)
    }
    
}

extension FotoLookViewController : FotoLookCollectionViewCellDelegate {
    
    func numberOfItemsInSectionFotoLook(_ cellForFotoLook: FotoLookCollectionViewCell) -> Int {
        return self.post.products_info.count
    }
    
    func selectedItemFotoLook(_ collectionView: UICollectionView, _ indexPath: IndexPath) {
        
    }

    func cellForItemFotoLook(_ collectionView: UICollectionView, _ indexPath: IndexPath, _ cellForFotoLook: FotoLookCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fotoLookProductCell", for: indexPath) as! FotoLookProductCollectionViewCell
        
        cell.delegate = self
        cell.indexPathNow = indexPath

        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.post.products_info[indexPath.row].pId!)/\(self.post.products_info[indexPath.row].picture!)")
        
        
        self.manager.loadImage(with: url, into: cell.photoLookImageView) { ( result, _ ) in
            cell.photoLookImageView.image = result.value
        }
        
        cell.nameProductLookLabel.text = self.post.products_info[indexPath.row].name!
        cell.brandProductLookLabel.text = self.post.products_info[indexPath.row].brand!
        
        if self.post.products_info[indexPath.row].promo_price! == "" {
            cell.priceProductLookLabel.text = "R$ \(self.post.products_info[indexPath.row].price!)"
        } else {
            cell.priceProductLookLabel.text = "R$ \(self.post.products_info[indexPath.row].promo_price!)"
        }
        
        
        return cell
    }
    
    func moreFotoLook() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let denunciarAction = UIAlertAction(title: "Denunciar Imagem", style: .default, handler: nil)
        let linkAction = UIAlertAction(title: "Link Quebrado", style: .default, handler: nil)
        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(denunciarAction)
        alertController.addAction(linkAction)
        alertController.addAction(cancelarAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func pressPhotoProductLook( _ cellForItem : FotoLookCollectionViewCell) {
        
        self.performSegue(withIdentifier: "showImageFotoLook", sender: cellForItem.photoProductLookImageView.image)
        
    }
 
    func likePhotoLook(_ collectionView: UICollectionView, _ cell: FotoLookCollectionViewCell) {
        
        if self.post.ilikes == "1" {
            
            self.post.ilikes = "0"
            self.post.likes = self.post.likes! - 1
            self.collectionView.reloadData()
            WishareAPI.like(self.post.id!, false, { (validate : Bool, error : Error?) in })
            
        } else {
            
            self.post.ilikes = "1"
            self.post.likes = self.post.likes! + 1
            self.collectionView.reloadData()
            WishareAPI.like(self.post.id!, true, { (validate : Bool, error : Error?) in })
            
        }
        
    }
    
    func sharePhotoLook(_ collectionView: UICollectionView, _ cell: FotoLookCollectionViewCell) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.post.id!)/\(self.post.picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.post.id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.post.id!)/\(self.post.picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)

        
    }
    
    func wishPhotoLook(_ collectionView: UICollectionView, _ cell: FotoLookCollectionViewCell) {
        
        if self.post.iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.post.id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.post.iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.post.id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.post.iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
}

extension FotoLookViewController : FotoLookProductCollectionViewCellDelegate {
    
    func addCard( _ sender : UIButton, _ indexPath : IndexPath? ) {
        
        self.products.removeAll()
        self.isSelectedProduct = indexPath?.row
        
        let popup = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: 220))

        
        let titleSize : UILabel = {
            let label = UILabel()
            label.text = "SELECIONE O TAMANHO"
            label.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let buttonExit : UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "Delete"), for: .normal)
            button.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            button.imageView?.contentMode = .scaleAspectFit
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
            return button
        }()
        
        
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewFlowLayout)
        
        collectionView.register(UINib(nibName: "PopupCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "popupCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //collectionView.backgroundColor = UIColor.white
        collectionView.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: 170)
        collectionView.backgroundView = blurEffectView
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        popup.addSubview(collectionView)
        popup.addSubview(titleSize)
        popup.addSubview(buttonExit)
        
       
        buttonExit.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
        buttonExit.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        buttonExit.heightAnchor.constraint(equalToConstant: 40).isActive = true
        buttonExit.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleSize.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
        titleSize.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
        titleSize.rightAnchor.constraint(equalTo: buttonExit.leftAnchor).isActive = true
        titleSize.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        collectionView.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: titleSize.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: popup.bottomAnchor, constant: -5).isActive = true
        
        
        let popupConfig = STZPopupViewConfig()
        popup.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        popupConfig.cornerRadius = 5
        popupConfig.overlayColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        popupConfig.showAnimation = .slideInFromBottom
        popupConfig.dismissAnimation = .slideOutToBottom

        presentPopupView(popup, config: popupConfig)
        
        
    }
    
    func openProductDetail(cell: FotoLookProductCollectionViewCell, indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.post.products_info[indexPath.row].id!
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
}

extension FotoLookViewController : PopupCollectionViewCellDelegate {
    
    func buyProductOnly() {
        
        if self.products.count == 1 {
                
            WishareCoreData.showShoppingCart(completion: { (productsCart : [WS_ShoppingCart], _, error : Error?) in
                    
                if error == nil {
                        
                    var isAddShoppingCart : Bool = true
                        
                    for prod in productsCart {
                        for p in self.products {
                            if prod.id_product! == p.value[0] {
                                isAddShoppingCart = false
                            }
                        }
                    }
                        
                    if !isAddShoppingCart {
                            
                        let alertController = UIAlertController(title: nil, message: "Você ja adicionou este produto ao carrinho", preferredStyle: .alert)
                            
                        let goToCartAction = UIAlertAction(title: "Ir para o carrinho de compra", style: .default, handler: { (action : UIAlertAction) in
                            
                            self.dismissPopupView()
                            
                            UIView.animate(withDuration: 0.4, animations: {
                                self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
                                self.view.layoutIfNeeded()
                            })
                                
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
                            self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
                                
                        })
                            
                        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                            
                        alertController.addAction(goToCartAction)
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                            
                            
                    } else {
                            
                        for p in self.post.products_info {
                            if let _ = self.products[p.id!] {
                                    
                                WishareCoreData.addShoppingCart(name: p.name!, size: self.products[p.id!]![1], color: p.color!, quantity: "1", price: (p.promo_price! == "") ? p.price! : p.promo_price!, idProduct: self.products[p.id!]![0], idStore: self.post.store!.id!, nameStore: self.post.store!.id!, completion: { ( _, error : Error?) in
                                    if error == nil {
                                            
                                        self.dismissPopupView()
                                        self.collectionViewShoppginCart.reloadData()
                                                
                                        UIView.animate(withDuration: 0.4, animations: {
                                            self.shoppingModelViewBottomConstraint.constant = 0
                                            self.view.layoutIfNeeded()
                                        })
                                            
                                    }
                                })
                                    
                                    
                            }
                                
                        }
                            
                    
                    }
                        
                        
                }
                //self.products.removeAll()
            })
            
        } else {
            let alertController = UIAlertController(title: nil, message: "Selecione um tamanho!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
