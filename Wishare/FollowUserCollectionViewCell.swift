//
//  FollowUserCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/20/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class FollowUserCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var followUserButton: RoundButton!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var delegate : FollowUserCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nameUserLabel.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func tapFollowUser(_ sender: UIButton) {
        self.delegate?.followUser(cell: self, sender: sender)
    }
    

}

protocol FollowUserCollectionViewCellDelegate {
    func followUser(cell: FollowUserCollectionViewCell, sender : UIButton)
}
