//
//  OrdersDeliveredViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD

class OrdersDeliveredViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var orders : [[String : String]] = []
    var items : [[[String : String]]] = [[]]
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "OrderDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "orderDetailCell")
        
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        WishareAPI.getOrders(typeOrder: "PE") { (orders : [[String : String]], items : [[[String : String]]], error : Error?) in
            if error == nil {
                
                self.orders = orders
                self.items = items
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
}

extension OrdersDeliveredViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("openOrderDetail"), object: [self.orders[indexPath.row], self.items[indexPath.row]])
    }
    
}

extension OrdersDeliveredViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderDetailCell", for: indexPath) as! OrderDetailTableViewCell
        
        if self.orders.count > 0 {
            
            cell.numberOrderLabel.text = "Pedido: \(self.orders[indexPath.row]["id"]!)"
            cell.purshaseDateLabel.text = "Data: \(self.orders[indexPath.row]["purshase_date"]!.convertToDate().formatterTimeBrazilian())"
            cell.totalOrderLabel.text = "Total: \((((self.orders[indexPath.row]["tot"]!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue) + ((self.orders[indexPath.row]["value_freight"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue)).asLocaleCurrency)"
            cell.quantityItemsOrderLabel.text = "(\(self.orders[indexPath.row]["items"]!) itens)"
            
        }
        
        return cell
        
    }
    
}
