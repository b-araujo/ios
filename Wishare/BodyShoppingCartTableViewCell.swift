//
//  BodyShoppingCartTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class BodyShoppingCartTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subTotalItemLabel: UILabel!
    @IBOutlet weak var priceTotalMoneyLabel: UILabel!
    @IBOutlet weak var parcelPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var bodyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var cuponLabel: UILabel!
    
    var delegate : BodyShoppingCartDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cuponLabel.adjustsFontSizeToFitWidth = true
        self.finishButton.layer.cornerRadius = 5.0
        self.finishButton.layer.masksToBounds = false
        self.finishButton.layer.shadowColor = UIColor.black.cgColor
        self.finishButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.finishButton.layer.shadowRadius = 2.0
        self.finishButton.layer.shadowOpacity = 0.24
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func finishPurchase(_ sender: UIButton) {
        self.delegate?.finishPurchase(sender, cell: self)
    }
    
}

protocol BodyShoppingCartDelegate {
    func finishPurchase(_ sender : UIButton, cell : BodyShoppingCartTableViewCell)
}
