//
//  RecommendationViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/16/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin

class RecommendationViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories : [Category] = []
    var loader : Loader!
    var manager : Manager!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoryCell")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        
        WishareAPI.suggestMenu { (categories : [Category], error : Error?) in
            
            if error == nil {
                
                self.categories = categories
                self.collectionView.reloadData()
                
            }
            
        }
    
        
        if defaults.object(forKey: "id_category") != nil && defaults.object(forKey: "name_category") != nil {
            
            let recommendationSearchViewController = storyboard?.instantiateViewController(withIdentifier: "recommendationSearchID") as! RecommendationSearchViewController
            recommendationSearchViewController.id_category = defaults.object(forKey: "id_category") as! String
            recommendationSearchViewController.name_category = defaults.object(forKey: "name_category") as! String
            
            self.navigationController?.pushViewController(recommendationSearchViewController, animated: true)
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "recommendationSearchSegue" {
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
        
            let recommendationSearchViewController = segue.destination as! RecommendationSearchViewController
            recommendationSearchViewController.id_category = senderValue[0] as! String
            recommendationSearchViewController.name_category = senderValue[1] as! String
        }
        
    }
    
    @IBAction func unwindToCategories(segue: UIStoryboardSegue) {
    
    
    }

}

extension RecommendationViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        defaults.set(self.categories[indexPath.row].id!, forKey: "id_category")
        defaults.set(self.categories[indexPath.row].name!, forKey: "name_category")
        
        self.performSegue(withIdentifier: "recommendationSearchSegue", sender: [self.categories[indexPath.row].id!, self.categories[indexPath.row].name!])
    }
    
}

extension RecommendationViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        
        cell.categoryButton.setTitle(self.categories[indexPath.row].name, for: .normal)
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlPerfil = urlBase.appendingPathComponent("files/categories/image/\(self.categories[indexPath.row].picture!)")
        
        self.manager.loadImage(with: urlPerfil, into: cell.categoryImageView) { ( result, _ ) in
            cell.categoryImageView.image = result.value
        }
        
        return cell
        
    }

}

extension RecommendationViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 2) - 2.5, height: (self.view.bounds.width / 2) + 60)
    }
    
}
