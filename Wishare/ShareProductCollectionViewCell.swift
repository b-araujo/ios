//
//  ShareProductCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class ShareProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userThumbImageView: UIImageView!
    @IBOutlet weak var userNameTopButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var moreImageView: UIImageView!
    
    
    @IBOutlet weak var storeShareThumbImageView: UIImageView!
    @IBOutlet weak var storeNameShareButton: UIButton!
    @IBOutlet weak var buyStore: UIButton!
    
    @IBOutlet weak var shareStorePostImageView: UIImageView!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeShareProductImageView: UIImageView!
    @IBOutlet weak var commentShareProductImageView: UIImageView!
    @IBOutlet weak var shareShareProductImageView: UIImageView!
    @IBOutlet weak var wishListShareProductImageView: UIImageView!
    
    @IBOutlet weak var viewLikes: UIView!
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    @IBOutlet weak var userNameBottomButton: UIButton!
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var commentStoreLabel: ActiveLabel!
    @IBOutlet weak var commentsStoreLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var viewMore: UIButton!
    
    @IBOutlet weak var shareView: UIView!
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    var delegateBase : WishareBasePostDelegate!
    var delegate : ShareProductCollectionViewCellDelegate?
    var indexPathNow : IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.shareView.layer.cornerRadius = 5
        self.shareView.layer.borderWidth = 1
        self.shareView.layer.borderColor = UIColor.darkGray.cgColor
        
        // ================================================================
        
        
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerDreamImageView = UITapGestureRecognizer(target: self, action: #selector(tapGift))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapLikesView))
        let tapGestureRecognizerCommentShowLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        
        self.likeShareProductImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentShareProductImageView.addGestureRecognizer(tapGestureRecognizerCommentImageView)
        self.shareShareProductImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        self.wishListShareProductImageView.addGestureRecognizer(tapGestureRecognizerDreamImageView)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        self.commentsStoreLabel.addGestureRecognizer(tapGestureRecognizerCommentShowLabel)
        
        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMore))
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        
        
        let tapGestureRecognizerStoreShareThumbImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreShareThumb))
        let tapGestureRecognizerStoreNameShareButton = UITapGestureRecognizer(target: self, action: #selector(tapNameShare))
        
        self.storeShareThumbImageView.addGestureRecognizer(tapGestureRecognizerStoreShareThumbImageView)
        self.storeNameShareButton.addGestureRecognizer(tapGestureRecognizerStoreNameShareButton)
        
        self.buyStore.addTarget(self, action: #selector(tapBuy), for: .touchUpInside)
        
        let tapGestureRecognizerShareStorepostImageView = UITapGestureRecognizer(target: self, action: #selector(tapShareStorePost))
        self.shareStorePostImageView.addGestureRecognizer(tapGestureRecognizerShareStorepostImageView)
        
        // =================================================
        
        self.userNameTopButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        self.userNameBottomButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        
        let tapGestureRecognizerUserThumbImageVieww = UITapGestureRecognizer(target: self, action: #selector(openUserImage))
        self.userThumbImageView.addGestureRecognizer(tapGestureRecognizerUserThumbImageVieww)
        
    }
    
    @objc func tapBuy () {
        self.delegate?.buyShareStoreProduct(cell: self)
    }
    
    @objc func tapShareStorePost() {
        self.delegate?.postShareStoreProduct(cell: self)
    }
    
    @objc func tapMore() {
        self.delegateBase.more(cell: self)
    }

    @objc func tapLike() {
        self.delegateBase.like(cell: self)
    }
    
    @objc func tapComment() {
        self.delegateBase.showComments(cell: self)
    }
    
    @objc func tapShare() {
        self.delegateBase.share(cell: self, indexPath: indexPathNow)
    }
    
    @objc func tapGift() {
        self.delegateBase.wishlist(cell: self)
    }
    
    @objc func tapLikesView() {
        self.delegateBase.showLikes(cell: self)
    }
    
    @objc func tapStoreShareThumb () {
        self.delegateBase.thumbStorePostImageView(cell: self)
    }
    
    @objc func tapNameShare() {
        self.delegateBase.userNameStorePostLabel(cell: self)
    }
    
    @objc func openUserImage() {
        self.delegate?.openUserImageShareStoreProdutNotFound(indexPathNow)
    }
    
    @objc func openUserButton ( _ sender : UIButton) {
        self.delegate?.openUserButtonShareStoreProdutNotFound(sender, indexPathNow)
    }
    
    @IBAction func viewMoreClicked(_ sender: Any) {
        self.delegate?.showMoreCommentsShareProductStore(cell: self, indexPath: indexPathNow)
    }
}

protocol ShareProductCollectionViewCellDelegate {
    
    func buyShareStoreProduct(cell : ShareProductCollectionViewCell)
    func postShareStoreProduct(cell : ShareProductCollectionViewCell)
    func openUserButtonShareStoreProdutNotFound( _ sender : UIButton,  _ indexPath : IndexPath)
    func openUserImageShareStoreProdutNotFound( _ indexPath : IndexPath)
    func showMoreCommentsShareProductStore(cell : ShareProductCollectionViewCell, indexPath : IndexPath)
    
}
