//
//  FacebookAPI.swift
//  Wishare
//
//  Created by Wishare iMac on 4/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKShareKit


class FacebookAPI {
    
    static var facebookLoginManager : FBSDKLoginManager = FBSDKLoginManager()
    
    class func facebookAuthorization( _ view : UIViewController, _ completion : @escaping ( _ isPermission : Bool) -> Void) {
        
        self.facebookLoginManager.loginBehavior = .browser
        
        let params = ["email", "public_profile", "user_friends"]
        
        
        
        self.facebookLoginManager.logIn(withReadPermissions: params, from: view) { (loginResult : FBSDKLoginManagerLoginResult?, error : Error?) in
            
            if error == nil {
                
                let facebookLoginResult : FBSDKLoginManagerLoginResult = loginResult!
                
                if facebookLoginResult.grantedPermissions != nil {
                    
                    if facebookLoginResult.grantedPermissions.contains("public_profile") {
                        completion(true)
                    } else {
                        completion(false)
                    }
                    
                } else {
                    completion(false)
                }
                
            } else {
                completion(false)
            }
            
        }
        
    }
    
    
    
    class func getFacebookFriends(viewControllerIfNoPermition viewController : UIViewController, _ completion : @escaping ( _ result : NSArray, _ error : Error?) -> Void) {
        
        if ((FBSDKAccessToken.current()) != nil ) {
            
            
            
//            let params = ["fields": "id, first_name, last_name, middle_name, name, email, picture.type(large)"]
//            let request = FBSDKGraphRequest(graphPath: "me/taggable_friends", parameters: params)
//            let _ = request?.start(completionHandler : { (connection : FBSDKGraphRequestConnection!, result : Any?, error : Error!) -> Void in
//
//                if error != nil {
//                    let errorMessage = error.localizedDescription
//                    print(errorMessage)
//                }
//                else if let findFriends : [String : AnyObject] = result as? [String : AnyObject] {
//                    if let data : NSArray = findFriends["data"] as? NSArray {
//                        completion(data, nil)
//                    }
//                }
//            })
            
            let params = ["fields": "id, first_name, last_name, middle_name, name, email, picture.type(large)"]

            FBSDKGraphRequest(graphPath: "/me/friends", parameters: params).start(completionHandler: { (connection : FBSDKGraphRequestConnection?, result : Any?, error : Error?) in
                if error == nil {

                    if let findFriends : [String : AnyObject] = result as? [String : AnyObject] {

                        if let data : NSArray = findFriends["data"] as? NSArray {

                            completion(data, nil)

                        }
                    }

                }
            })
            
        } else {
            facebookAuthorization(viewController, { (granted) in
                if granted {
                    getFacebookFriends(viewControllerIfNoPermition: viewController, completion)
                }
                else {
                    completion([], nil)
                }
            })
        }
        
    }
    
    
    class func getFacebookUserData( _ completion : @escaping ( _ result : [String : AnyObject], _ error : Error?) -> Void) {
        
        if ((FBSDKAccessToken.current()) != nil ) {
            
            let params = ["fields" : "id, name, first_name, last_name, gender, email, picture.type(large)"]
            
            FBSDKGraphRequest(graphPath: "me", parameters: params).start(completionHandler: { (connection : FBSDKGraphRequestConnection?, result : Any?, error : Error?) in
                
                if error == nil {
                    
                    if let facebookResult : [String : AnyObject] = result as? [String : AnyObject] {
                        completion(facebookResult, nil)
                    } else {
                        completion([:], nil)
                    }
                    
                } else {
                    completion([:], error)
                }
                
            })
            
        }
        
    }
    
    class func inviteFriendApp ( _ view : UIViewController, _ delegate : FBSDKAppInviteDialogDelegate ) {
        
        let content = FBSDKAppInviteContent()   
        content.appLinkURL = URL(string: "https://fb.me/261852400943068")
        content.appInvitePreviewImageURL = URL(string: "http://wishare.com.br/emailmkt/Imagem.png")
        FBSDKAppInviteDialog.show(from: view, with: content, delegate: delegate)
        
    }
    
        
}



