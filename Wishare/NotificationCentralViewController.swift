//
//  NotificationCentralViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/31/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Pager
import RappleProgressHUD

class NotificationCentralViewController: PagerController {

    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var benefitsWish : NotificationGeneralViewController!
    var targetsWish : NotificationActionViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(selectedNotificationGeneralUserProfile(notification:)), name: Notification.Name.init("selectedNotificationGeneralUserProfile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectedNotificationGeneralStore(notification:)), name: Notification.Name.init("selectedNotificationGeneralStore"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectedNotificationGeneralPostInformation(notification:)), name: Notification.Name.init("selectedNotificationGeneralPostInformation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationSharePurchase(notification:)), name: Notification.Name.init("notificationSharePurchase"), object: nil)
        
        let storyboard = UIStoryboard(name: "Notification", bundle: nil)
        
        self.benefitsWish = storyboard.instantiateViewController(withIdentifier: "notificationGeneralID") as! NotificationGeneralViewController
        self.targetsWish = storyboard.instantiateViewController(withIdentifier: "notificationActionID") as! NotificationActionViewController
        
        self.indicatorColor = UIColor.black
        self.animation = .none
        self.centerCurrentTab = true
        self.setupPager(tabNames: ["Notificações", "Atividades"], tabControllers: [benefitsWish, targetsWish])
        self.customizeTabs()
        self.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UserDefaults.standard.removeObject(forKey: "numberBagde")
        self.tabBarController?.tabBar.items?[3].badgeValue = nil
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    
        self.benefitsWish.tableView.reloadData()
        
        if let _ = self.targetsWish.isActive {
            self.targetsWish.tableView.reloadData()
        }
        
        
    }
    
    func customizeTabs() {
        
        tabTopOffset = 0.0
        tabWidth = self.view.bounds.width / 2
        tabOffset = 0
//        fixMenu = true
        animation = .during
//        contentViewBackgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
        self.indicatorColor = UIColor.black
        self.tabHeight += 5
        self.indicatorColor = UIColor.black
        self.contentViewBackgroundColor = UIColor.white
        self.centerCurrentTab = true
        self.tabsViewBackgroundColor = UIColor.white
        self.tabsTextColor = UIColor.black
        self.selectedTabTextColor = UIColor.black
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "notificationSharePurchaseSegue" {
            let notificationSharePurshaseViewController = segue.destination as! NotificationSharePurchaseViewController
            let valueArray = sender as! [AnyObject]
            notificationSharePurshaseViewController.products = valueArray[0] as! [[String : AnyObject]]
            notificationSharePurshaseViewController.store = valueArray[1] as! Store
        }
        
    }
    
    @objc func selectedNotificationGeneralUserProfile(notification : Notification) {
        let not = notification.object as! WSNotification
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = not.user!.id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    @objc func selectedNotificationGeneralStore(notification : Notification) {
        let not = notification.object as! WSNotification
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
        
        if let store = not.store {
            pageStoreViewController.idStore = store.id
        }
        
        self.navigationController?.pushViewController(pageStoreViewController, animated: true)
    }
    
    @objc func selectedNotificationGeneralPostInformation(notification : Notification) {
        let not = notification.object as! WSNotification
        
        
        if not.idPost != nil {

            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando", attributes: attributes)
        
            WishareAPI.getPostOnly(idPost: not.idPost!) { (posts : [Post], error : Error?) in
                if error == nil {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let previewPostPageUserViewController = storyboard.instantiateViewController(withIdentifier: "previewPostUser") as! PreviewPostPageUserViewController
                    previewPostPageUserViewController.posts = posts[0]
                    
                    RappleActivityIndicatorView.stopAnimation()
                    self.navigationController?.pushViewController(previewPostPageUserViewController, animated: true)
                    
                }
            }
        }
    }
    
    @objc func notificationSharePurchase(notification : Notification) {
        self.performSegue(withIdentifier: "notificationSharePurchaseSegue", sender: notification.object)
    }

}

extension NotificationCentralViewController : PagerDataSource {
    
}
