//
//  FotoLookCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class FotoLookCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photoProductLookImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var descriptionProductLabel: UILabel!
    @IBOutlet weak var priceProductLabel: UILabel!
    @IBOutlet weak var infoProductLabel: UILabel!
    
    @IBOutlet weak var likeStoreImageView: UIImageView!
    @IBOutlet weak var shareStoreImageView: UIImageView!
    @IBOutlet weak var wishStoreImageView: UIImageView!
    
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var sharesStoreLabel: UILabel!
    @IBOutlet weak var wishStoreLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate : FotoLookCollectionViewCellDelegate!
    var indexPathNow : IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "FotoLookProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "fotoLookProductCell")
        
        let gestureRecognizerPhotoProductLookImageView = UITapGestureRecognizer(target: self, action: #selector(tapPhotoProductLook))
        self.photoProductLookImageView.addGestureRecognizer(gestureRecognizerPhotoProductLookImageView)
        
        let gestureRecognzerLikeStoreImageView : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let gestureRecognzerShareStoreImageView : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let gestureRecognzerWishStoreImageView : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapWish))
        self.likeStoreImageView.addGestureRecognizer(gestureRecognzerLikeStoreImageView)
        self.shareStoreImageView.addGestureRecognizer(gestureRecognzerShareStoreImageView)
        self.wishStoreImageView.addGestureRecognizer(gestureRecognzerWishStoreImageView)
        
    }
    
    @objc func tapPhotoProductLook() {
        self.delegate.pressPhotoProductLook(self)
    }
    
    @objc func tapLike() {
        self.delegate.likePhotoLook(self.collectionView, self)
    }
    
    @objc func tapShare() {
        self.delegate.sharePhotoLook(self.collectionView, self)
    }
    
    @objc func tapWish() {
        self.delegate.wishPhotoLook(self.collectionView, self)
    }
    
}

extension FotoLookCollectionViewCell : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate.selectedItemFotoLook(collectionView, indexPath)
    }
    
}

extension FotoLookCollectionViewCell : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate.numberOfItemsInSectionFotoLook(self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.delegate.cellForItemFotoLook(collectionView, indexPath, self)
    }
    
}

extension FotoLookCollectionViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width, height: self.collectionView.bounds.width / 2)
    }
    
}



protocol FotoLookCollectionViewCellDelegate {
    func selectedItemFotoLook( _ collectionView : UICollectionView, _ indexPath : IndexPath)
    func cellForItemFotoLook( _ collectionView : UICollectionView, _ indexPath : IndexPath, _ cellForFotoLook : FotoLookCollectionViewCell) -> UICollectionViewCell
    func numberOfItemsInSectionFotoLook(_ cellForFotoLook : FotoLookCollectionViewCell) -> Int
    func pressPhotoProductLook( _ cellForItem : FotoLookCollectionViewCell)
    
    func likePhotoLook( _ collectionView : UICollectionView, _ cell : FotoLookCollectionViewCell)
    func sharePhotoLook( _ collectionView : UICollectionView, _ cell : FotoLookCollectionViewCell)
    func wishPhotoLook( _ collectionView : UICollectionView, _ cell : FotoLookCollectionViewCell)
    
}

