//
//  OrderDetailTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class OrderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var numberOrderLabel: UILabel!
    @IBOutlet weak var purshaseDateLabel: UILabel!
    @IBOutlet weak var totalOrderLabel: UILabel!
    @IBOutlet weak var quantityItemsOrderLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.totalOrderLabel.adjustsFontSizeToFitWidth = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
