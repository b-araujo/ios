//
//  StoreSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/23/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes
import RappleProgressHUD

class StoreSearchViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var resultNotFoundLabel: UILabel!
    
    var storeResult = [Store]()
    var loader : Loader!
    var manager : Manager!

    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "UserSearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userSearchCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateStoreResult(notification:)), name: Notification.Name.init("storesResultUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loading(notification:)), name: Notification.Name.init("loadingTabStore"), object: nil)
        
        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.searchStore("9999") { (stores : [Store], error : Error?) in
            if error == nil {
                RappleActivityIndicatorView.stopAnimation()
                self.storeResult = stores
                let encodedStores = NSKeyedArchiver.archivedData(withRootObject: stores)
                UserDefaults.standard.set(encodedStores, forKey: "initialStores")
                self.collectionView.reloadData()
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Store WillApper")
        NotificationCenter.default.post(name: NSNotification.Name.init("changeTabs"), object:2)
    }
    
    @objc func updateStoreResult(notification: Notification) {
        self.storeResult = notification.object as! [Store]
        print(self.storeResult.count)
        if self.storeResult.count == 0 {
            self.collectionView.isHidden = true
            self.resultNotFoundLabel.isHidden = false
        } else {
            self.collectionView.isHidden = false
            self.resultNotFoundLabel.isHidden = true
        }
        self.collectionView.reloadData()
    }
    
    @objc func loading(notification: Notification) {
        
        let isLoading : Bool = notification.object as! Bool
        
        if isLoading {
            self.collectionView.isHidden = true
            self.resultNotFoundLabel.isHidden = true
            self.loadingActivityIndicator.startAnimating()
        } else {
            self.loadingActivityIndicator.stopAnimating()
            self.collectionView.isHidden = false
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.becomeFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init("storesResultUpdated"), object: nil)
    }

}

extension StoreSearchViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("openPageStore"), object: self.storeResult[indexPath.row].id!)
    }
    
}

extension StoreSearchViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storeResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userSearchCell", for: indexPath) as! UserSearchCollectionViewCell
        
        cell.userLabel.text = self.storeResult[indexPath.row].name
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/store-logo/thumb/")!
        let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.storeResult[indexPath.row].picture!)")
        
        self.manager.loadImage(with: url, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        return cell
        
    }
    
}

extension StoreSearchViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 1.25, height: (self.view.bounds.width / 3) - 2.5)
    }
    
}
