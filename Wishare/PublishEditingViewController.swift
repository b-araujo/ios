//
//  PublishEditingViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/15/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin

class PublishEditingViewController: UIViewController {

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    
    var post : Post!
    var indexPath : IndexPath!
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
        let url : URL!
        
        if self.post.typePost == 5 {
            url = urlBase.appendingPathComponent("\(self.post.products[0]["url"]!)")
        } else if self.post.typePost == 10 {
            url = urlBase.appendingPathComponent("files/orders_itens/\(self.post.products[0]["url"]!)")
        } else {
            url = urlBase.appendingPathComponent("files/posts-image/\(self.post.id!)/\(self.post.picture!)")
        }

        
        self.manager.loadImage(with: url, into: self.postImageView) { ( result, _ ) in
            self.postImageView.image = result.value
        }
        
        self.commentTextView.text = self.post.text!
        self.commentTextView.becomeFirstResponder()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }


}
