//
//  NotificationGeneralViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/31/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import SwiftyAttributes

class NotificationGeneralViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let refreshControl = UIRefreshControl()
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var notifications : [WSNotification] = []
    
    var loader : Loader!
    var manager : Manager!
    
    var pageIndex : Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(isChangedNotification(notification:)), name: Notification.Name.init("isChangedNotification"), object: nil)
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.refreshControl.tintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        self.refreshControl.addTarget(self, action: #selector(refreshShow), for: .valueChanged)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.refreshControl = refreshControl
        self.tableView.register(UINib(nibName: "NotificationFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "notificationFollowCell")
        self.tableView.register(UINib(nibName: "NotificationToFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "notificaitonToFollowCell")
        self.tableView.register(UINib(nibName: "NotificationInformationTableViewCell", bundle: nil), forCellReuseIdentifier: "notificationInformationCell")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.userNotification { (notifications : [WSNotification], error : Error?) in
            if error == nil {
                self.notifications = notifications
                
                if self.notifications.count > 0 {
                    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                }

                self.tableView.reloadData()
            }
            RappleActivityIndicatorView.stopAnimation()
        }
        
        
    }
    
    @objc func isChangedNotification(notification : Notification) {
        
        if let isChanged = notification.object as? Bool {
            
            if isChanged == true {
               
                self.refreshControl.beginRefreshing()
                
                WishareAPI.userNotification { (notifications : [WSNotification], error : Error?) in
                    if error == nil {
                        self.notifications = notifications
                        
                        if self.notifications.count > 0 {
                            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                        }
                        
                        
                        self.tableView.reloadData()
                        NotificationCenter.default.post(name: NSNotification.Name.init("isChangedNotification"), object: false)
                        self.refreshControl.endRefreshing()
                        
                    }
                }
               
                
            }
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.tableFooterView?.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        for n in self.notifications {
            n.seen = "1"
        }
        
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refreshShow() {
        
        WishareAPI.userNotification { (notifications : [WSNotification], error : Error?) in
            if error == nil {
                self.notifications = notifications
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                
                var numberBadge = 0
                
                for n in self.notifications {
                    if n.seen! == "0" {
                        numberBadge += 1
                    }
                }
                
                UserDefaults.standard.set("\(numberBadge)", forKey: "numberBagde")
                
            }
        }
        
    }

}

extension NotificationGeneralViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
            
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.color = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            spinner.frame = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 44.0)
            spinner.startAnimating()
            
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
            
            WishareAPI.userNotificationBy(page: "\(self.pageIndex)", lastId: self.notifications.last!.id!, { (notifications : [WSNotification], error : Error?) in
                if error == nil {
                    
                    if notifications.count == 0 {
                        
                        self.tableView.tableFooterView?.isHidden = true
                        
                    } else {
                        
                        self.pageIndex += 1
                        self.notifications.append(contentsOf: notifications)
                        self.tableView.reloadData()
                        
                    }
                    
                }
            })
            
        }
        
    }
    
}

extension NotificationGeneralViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let actionID = self.notifications[indexPath.row].actionID {
            
            switch actionID {
                
            case "1", "2", "3", "4", "10":
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificationInformationCell", for: indexPath) as! NotificationInformationTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                if self.notifications[indexPath.row].seen! == "0" {
                    cell.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                } else {
                    cell.backgroundColor = UIColor.white
                }
                
                let actionMessage = self.notifications[indexPath.row].actionMessage!.lowercased().withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                
                cell.descriptionLabel.attributedText = self.notifications[indexPath.row].user!.username!.withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + " ".withFont(.systemFont(ofSize: 12)) + actionMessage
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                cell.userNameButton.setTitle(self.notifications[indexPath.row].user!.username!, for: .normal)
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                var urlStringPicturePost : URL!
                
                if self.notifications[indexPath.row].picturePost == nil {
                    urlStringPicturePost = urlBase.appendingPathComponent("\(self.notifications[indexPath.row].productsPicture.first!["url"]!)")
                } else {
                    urlStringPicturePost = urlBase.appendingPathComponent("files/posts-image/\(self.notifications[indexPath.row].idPost!)/thumb/\(self.notifications[indexPath.row].picturePost!)")
                }
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                self.manager.loadImage(with: urlStringPicturePost, into: cell.previewPostImageView, handler: { (result, _) in
                    cell.previewPostImageView.image = result.value
                })
                
                return cell
                
            case "5":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificaitonToFollowCell", for: indexPath) as! NotificationToFollowTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                if self.notifications[indexPath.row].seen! == "0" {
                    cell.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                } else {
                    cell.backgroundColor = UIColor.white
                }
                
                if self.notifications[indexPath.row].user!.isFollowing! == "true" {
                    
                    cell.followButton.backgroundColor = UIColor.white
                    cell.followButton.setTitle("Seguindo", for: .normal)
                    cell.followButton.setTitleColor(UIColor.black, for: .normal)
                    cell.followButton.layer.borderWidth = 2.0
                    cell.followButton.layer.borderColor = UIColor.black.cgColor
                    
                    
                } else if self.notifications[indexPath.row].user!.isFollowing! == "false" {
                    
                    cell.followButton.backgroundColor = UIColor.black
                    cell.followButton.setTitle("Seguir", for: .normal)
                    cell.followButton.setTitleColor(UIColor.white, for: .normal)
                    cell.followButton.layer.borderWidth = 0.0
                    cell.followButton.layer.borderColor = UIColor.clear.cgColor
                    
                } else {
                    
                    cell.followButton.backgroundColor = UIColor.white
                    cell.followButton.setTitle("Aguardando", for: .normal)
                    cell.followButton.setTitleColor(UIColor.black, for: .normal)
                    cell.followButton.layer.borderWidth = 2.0
                    cell.followButton.layer.borderColor = UIColor.black.cgColor
                    
                }
                
                let actionMessage = self.notifications[indexPath.row].actionMessage!.lowercased().withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                
                cell.descriptionLabel.attributedText = self.notifications[indexPath.row].user!.username!.withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12))  + " ".withFont(.systemFont(ofSize: 12)) + actionMessage
                
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                cell.userNameButton.setTitle(self.notifications[indexPath.row].user!.username!, for: .normal)
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                return cell
                
            case "6":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificaitonToFollowCell", for: indexPath) as! NotificationToFollowTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                if self.notifications[indexPath.row].seen! == "0" {
                    cell.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                } else {
                    cell.backgroundColor = UIColor.white
                }
                
                let actionMessage = self.notifications[indexPath.row].actionMessage!.lowercased().withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                
                cell.descriptionLabel.attributedText = self.notifications[indexPath.row].user!.username!.withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + " ".withFont(.systemFont(ofSize: 12)) + actionMessage
                
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                cell.userNameButton.setTitle(self.notifications[indexPath.row].user!.username!, for: .normal)
                
                cell.followButton.backgroundColor = UIColor.black
                cell.followButton.setTitleColor(UIColor.white, for: .normal)
                cell.followButton.layer.borderWidth = 0.0
                cell.followButton.layer.borderColor = UIColor.clear.cgColor
                cell.followButton.setTitle("Compartilhar!", for: .normal)
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                return cell

            case "7":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificationFollowCell", for: indexPath) as! NotificationFollowTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                if self.notifications[indexPath.row].seen! == "0" {
                    cell.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                } else {
                    cell.backgroundColor = UIColor.white
                }
                
                let actionMessage = self.notifications[indexPath.row].actionMessage!.lowercased().withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                
                cell.descriptionLabel.attributedText = self.notifications[indexPath.row].user!.username!.withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + " ".withFont(.systemFont(ofSize: 12)) + actionMessage
                
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                cell.userNameButton.setTitle(self.notifications[indexPath.row].user!.username!, for: .normal)
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                return cell
                
            default:
                print("NENHUM")
            }
            
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
        
    }

}

extension NotificationGeneralViewController : WishareNotificationDelegate {
    
    
    func userProfile(_ tableView: UITableView, _ indexPath: IndexPath, _ gesture: UITapGestureRecognizer) {
        
    }
    
    func openUserProfile(_ tableView: UITableView, _ indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("selectedNotificationGeneralUserProfile"), object: self.notifications[indexPath.row])
    }
    
    func openPostInformation(_ tableView: UITableView, _ indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("selectedNotificationGeneralPostInformation"), object: self.notifications[indexPath.row])
    }
    
    func openStore(_ tableView: UITableView, _ indexPath: IndexPath) {

    }
    
    func acceptOrDeclineInvite(_ tableView: UITableView, _ indexPath: IndexPath, _ sender: UIButton) {
        
        if sender.tag == 1 {
            
            self.notifications[indexPath.row].actionID = "5"
            self.notifications[indexPath.row].actionMessage = "Começou a seguir você"
            //self.notifications[indexPath.row].user!.following = true
            self.notifications[indexPath.row].seen = "0"
            self.tableView.reloadData()
            
            if let number = self.tabBarController?.tabBar.items?[3].badgeValue {
                UserDefaults.standard.set("\(Int(number)! + 1)", forKey: "numberBagde")
            } else {
                UserDefaults.standard.set("1", forKey: "numberBagde")
            }
            
            WishareAPI.acceptInvite(idNotification: self.notifications[indexPath.row].id!, { ( _ ) in })
            
        } else {
            
            WishareAPI.declineInvite(idNotification: self.notifications[indexPath.row].id!, { ( _ ) in })
            self.notifications.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
        
    }
    
    func followOrShare(_ tableView: UITableView, _ indexPath: IndexPath, _ sender: UIButton) {
        
        if self.notifications[indexPath.row].store != nil {
            NotificationCenter.default.post(name: NSNotification.Name.init("notificationSharePurchase"), object: [self.notifications[indexPath.row].products, self.notifications[indexPath.row].store!])
        } else {
            
            if sender.title(for: .normal) == "Seguir" {
                
                if self.notifications[indexPath.row].user!.privateW! == "1" {
                
                    self.notifications[indexPath.row].user!.following = true
                    sender.backgroundColor = UIColor.white
                    sender.setTitle("Aguardando", for: .normal)
                    sender.setTitleColor(UIColor.black, for: .normal)
                    sender.layer.borderWidth = 2.0
                    sender.layer.borderColor = UIColor.black.cgColor
                    
                    WishareAPI.followUser(self.notifications[indexPath.row].user!.id!, isFollow: true, { (error : Error?) in })
                    
                
                } else {
                    
                    self.notifications[indexPath.row].user!.following = true
                    sender.backgroundColor = UIColor.white
                    sender.setTitle("Seguindo", for: .normal)
                    sender.setTitleColor(UIColor.black, for: .normal)
                    sender.layer.borderWidth = 2.0
                    sender.layer.borderColor = UIColor.black.cgColor
                    
                    WishareAPI.followUser(self.notifications[indexPath.row].user!.id!, isFollow: true, { (error : Error?) in })
                    
                }
                
                
            } else {
                
                self.notifications[indexPath.row].user!.following = false
                sender.backgroundColor = UIColor.black
                sender.setTitle("Seguir", for: .normal)
                sender.setTitleColor(UIColor.white, for: .normal)
                sender.layer.borderWidth = 0.0
                sender.layer.borderColor = UIColor.clear.cgColor
                
                WishareAPI.followUser(self.notifications[indexPath.row].user!.id!, isFollow: false, { (error : Error?) in })
                
            }
            
        }
        
    }
    
}
