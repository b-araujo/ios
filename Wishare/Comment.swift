//
//  Comment.swift
//  Wishare
//
//  Created by Wishare iMac on 5/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Comment {
    
    var id : String?
    var type : String?
    var action_type : String?
    var active : String?
    var user : User?
    var store : Store?
    var content : String?
    var created : String?
    
    
    
    init( postComments comments : [String : AnyObject] ) {
        
        if let feedAction : [String : AnyObject] = comments["FeedAction"] as? [String : AnyObject] {
            
            self.id = feedAction["id"] as? String
            self.type = feedAction["type"] as? String
            self.action_type = feedAction["action_type"] as? String
            self.active = feedAction["active"] as? String
            self.content = feedAction["content"] as? String
            self.created = feedAction["created"] as? String
            
        }
        
        
        if let userAction : [String : AnyObject] = comments["User"] as? [String : AnyObject] {
            self.user = User(commentUser: userAction)
        }
        
        if let storeAction : [String : AnyObject] = comments["Store"] as? [String : AnyObject] {
            self.store = Store(commentStore: storeAction)
        }
        
    }
    
    init( username : String?, text : String?, picture : String?) {
        
        self.content = text
        self.user = User(user: username, picture: picture)
        
    }
    
    init( username : String?, text: String?, picture : String?, nameStore : String?, pictureStore : String?) {
        
        self.content = text
        self.user = User(user: username, picture: picture)
        self.store = Store(name: nameStore, picture: pictureStore)
        
    }
    
}
