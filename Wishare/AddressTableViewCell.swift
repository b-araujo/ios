//
//  AddressTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/6/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var nameAddressLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
