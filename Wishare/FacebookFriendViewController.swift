//
//  FacebookFriendViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import FBSDKShareKit

class FacebookFriendViewController: UIViewController {

    
    // MARK: - Outlets
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var findFriendButton: UIButton!
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var nextStepButton: UIButton!
    
    var isSettingOrigin : Bool = false
    
    // MARK: - Properties
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isSettingOrigin {
            self.barView.isHidden = true
            self.nextStepButton.isHidden = true
        
            FacebookAPI.facebookAuthorization(self, { (validate : Bool) in
                if validate {
                 
                    FacebookAPI.getFacebookFriends(viewControllerIfNoPermition: self) { (result : NSArray, error : Error?) in
                        
                        if error == nil {
                            
                            if result.count > 0 {
                                self.facebookButton.isHidden = false
                                self.findFriendButton.isHidden = false
                            }
                            
                        }
                        
                    }
                    
                }
            })
        
        
        } else {
            
            FacebookAPI.getFacebookFriends(viewControllerIfNoPermition: self) { (result : NSArray, error : Error?) in
                
                if error == nil {
                    
                    if result.count > 0 {
                        self.facebookButton.isHidden = false
                        self.findFriendButton.isHidden = false
                    }
                    
                }
                
            }
            
        }
        
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Actions
    @IBAction func findFriends(_ sender: UIButton) {
        
//        self.nextStepButton.setTitle("Continuar", for: .normal)

        FacebookAPI.inviteFriendApp(self, self)
    }
    

}

extension FacebookFriendViewController : FBSDKAppInviteDialogDelegate {
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("error made")
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("invitation made")
    }
    
}
