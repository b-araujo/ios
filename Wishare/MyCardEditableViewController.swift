//
//  MyCardEditableViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Caishen
import RappleProgressHUD

class MyCardEditableViewController: UIViewController {

    @IBOutlet weak var registerCreditCardButton: UIButton!
    @IBOutlet weak var numberBankTextField: NumberInputTextField!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var nameBankTextField: UITextField!
    @IBOutlet weak var monthTextField: MonthInputTextField!
    @IBOutlet weak var yearTextField: YearInputTextField!
    
    var bankCardNumber : String = ""
    var monthAndYear : String = ""
    var flag : String = ""
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCreditCardButton.layer.cornerRadius = 5.0
        self.registerCreditCardButton.layer.masksToBounds = false
        self.registerCreditCardButton.layer.shadowColor = UIColor.black.cgColor
        self.registerCreditCardButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.registerCreditCardButton.layer.shadowRadius = 2.0
        self.registerCreditCardButton.layer.shadowOpacity = 0.24

        numberBankTextField.numberInputTextFieldDelegate = self
        monthTextField.cardInfoTextFieldDelegate = self
        yearTextField.cardInfoTextFieldDelegate = self
        
        monthTextField.deleteBackwardCallback = { _ in self.numberBankTextField.becomeFirstResponder() }
        yearTextField.deleteBackwardCallback = { _ in self.monthTextField.becomeFirstResponder() }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func registerCreditCard(_ sender: UIButton) {
    
        if card != nil {
            
            if self.nameBankTextField.text != "" {
            
                RappleActivityIndicatorView.startAnimatingWithLabel("Salvando...", attributes: attributes)
            
                WishareAPI.userCards(number: self.bankCardNumber, name: self.nameBankTextField.text!, valid: self.monthAndYear, flag: self.flag, { (validate : Bool, error : Error?) in
            
                    if error == nil {
                    
                        RappleActivityIndicatorView.stopAnimation()
                        self.navigationController?.popViewController(animated: true)
                    
                    }
                
                })
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Informe o nome impresso no cartão", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        } else {
         
            let alertController = UIAlertController(title: nil, message: "Número do Cartão Inválido / Bandeira não aceita", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }

}

extension MyCardEditableViewController : NumberInputTextFieldDelegate, CardInfoTextFieldDelegate {
    
    var card : Card? {
        
        let number = numberBankTextField.cardNumber
        let cvc = CVC(rawValue: "555")
        let expiry = Expiry(month: monthTextField.text ?? "", year: yearTextField.text ?? "") ?? Expiry.invalid
        
        let cardType = numberBankTextField.cardTypeRegister.cardType(for: numberBankTextField.cardNumber)
        
        if cardType.validate(cvc: cvc).union(cardType.validate(expiry: expiry)).union(cardType.validate(number: number)) == .Valid {
            return Card(number: number, cvc: cvc, expiry: expiry)
        } else {
            return nil
        }
    }
    
    func numberInputTextFieldDidComplete(_ numberInputTextField: NumberInputTextField) {
        monthTextField.becomeFirstResponder()
    }
    
    func numberInputTextFieldDidChangeText(_ numberInputTextField: NumberInputTextField) {
        
        self.flag = numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber).name
        cardImageView.image = UIImage(named: numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber).name, in: Bundle(for: CardTextField.self), compatibleWith: nil)
        
    }
    
    func textField(_ textField: UITextField, didEnterValidInfo: String) {
        
        switch textField {
        case is MonthInputTextField:
            print("Month: \(didEnterValidInfo)")
            yearTextField.becomeFirstResponder()
        case is YearInputTextField:
            print("YEAR")
        default:
            break
        }
        
        if card != nil {
            self.bankCardNumber = card!.bankCardNumber.description
            self.monthAndYear = card!.expiryDate.description
        }

    }
    
    func textField(_ textField: UITextField, didEnterPartiallyValidInfo: String) {
        
    }
    
    func textField(_ textField: UITextField, didEnterOverflowInfo overFlowDigits: String) {
        
    }
}
