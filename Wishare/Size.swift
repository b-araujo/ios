//
//  Size.swift
//  Wishare
//
//  Created by Vinicius França on 08/05/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation


class Size {
    
    var id : String?
    var price : String?
    var promo_price : String?
    var url : String?
    var size : String?
    var quantity : String?
    
    init( _ size : [String : AnyObject]) {
        
        self.id = size["id"] as? String
        self.price = size["price"] as? String
        self.promo_price = size["promo_price"] as? String
        self.url = size["url"] as? String
        self.size = size["size"] as? String
        
    }
    
    
    init (productDetailSize size : [String : AnyObject]) {
        
        self.id = size["id"] as? String
        self.price = size["price"] as? String
        self.promo_price = size["promo_price"] as? String
        self.url = size["url"] as? String
        self.size = size["size"] as? String
        self.quantity = size["qtd"] as? String
        
    }
    
}
