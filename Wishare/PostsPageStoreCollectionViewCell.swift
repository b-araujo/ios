//
//  PostsPageStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/9/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PostsPageStoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var quantityPostsLabel: UILabel!
    @IBOutlet weak var quantityFollowersLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var menuBlockButton: UIButton!
    @IBOutlet weak var menuListButton: UIButton!
    
    var delegate : PostsPageStoreCollectionViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gestureRecognizerFollowView = UITapGestureRecognizer(target: self, action: #selector(pressFollowers))
        self.followView.addGestureRecognizer(gestureRecognizerFollowView)
    }

    @objc func pressFollowers() {
        self.delegate.followersPageStore()
    }
    
    @IBAction func changeOption(_ sender: UIButton) {
        self.delegate.changeOption(sender)
    }
    
    @IBAction func followTapped(_ sender: UIButton) {
        self.delegate.followStore(sender)
    }
    
}

protocol PostsPageStoreCollectionViewCellDelegate {
    func followersPageStore()
    func changeOption(_ sender : UIButton)
    func followStore(_ sender : UIButton)
}
