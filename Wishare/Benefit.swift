//
//  Benefits.swift
//  Wishare
//
//  Created by Wishare iMac on 8/2/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Benefit {
    
    
    var id : String?
    var value_from : String?
    var message : String?
    var code : String?
    var type : String?
    
    init ( _ benefit : [String : AnyObject]) {
        
        self.id = benefit["id"] as? String
        self.value_from = benefit["value_from"] as? String
        self.type = benefit["type"] as? String
        
    }
    
}
