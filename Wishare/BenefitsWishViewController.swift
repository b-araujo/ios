//
//  BenefitsWishViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class BenefitsWishViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var wishPoints : WishPoints!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BenefitsWishTableViewCell", bundle: nil), forCellReuseIdentifier: "benefitsWishCell")
        self.tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BenefitsWishViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.wishPoints.benefits[indexPath.row].code != nil {
            NotificationCenter.default.post(name: NSNotification.Name.init("openPopupWishPointsBenefit"), object: self.wishPoints.benefits[indexPath.row])
        }
    }
    
}

extension BenefitsWishViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.wishPoints.benefits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "benefitsWishCell", for: indexPath) as! BenefitsWishTableViewCell
        
        cell.benefitsLevelLabel.text = "\(self.wishPoints.level! - 1)"
        cell.benefitsDescriptionLabel.text = self.wishPoints.benefits[indexPath.row].message
        
        return cell
        
    }
    
}
