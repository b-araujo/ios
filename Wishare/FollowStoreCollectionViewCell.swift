//
//  FollowStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/20/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class FollowStoreCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var followButton: RoundButton!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var indexPath : IndexPath?
    var delegate : FollowStoreCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.nameStoreLabel.adjustsFontSizeToFitWidth = true
        
        self.storeImageView.isUserInteractionEnabled = true
        let tapGestureRecognizerStoreImageView = UITapGestureRecognizer(target: self, action: #selector(pressStoreImageView))
        self.storeImageView.addGestureRecognizer(tapGestureRecognizerStoreImageView)
        
    }
    
    @IBAction func tapFollowStore(_ sender: UIButton) {
        self.delegate?.followStore(cell: self, sender: sender)
    }
    
    @objc func pressStoreImageView () {
        if let indexPath = self.indexPath {
           self.delegate?.showUserOrStore(self, indexPath)
        }
    }
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//
//        if followButton.title(for: .normal) == "Seguir" {
//
//
//
//        }
//
//    }
    
}


protocol FollowStoreCollectionViewCellDelegate {
    func followStore(cell : FollowStoreCollectionViewCell, sender : UIButton)
    func showUserOrStore( _ cell : FollowStoreCollectionViewCell, _ indexPath : IndexPath)
}
