//
//  RepostViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/9/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD

class RepostViewController: UIViewController {

    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var imagePost: UIImageView!
    
    var imageRespost : UIImage?
    var id : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePost.image = imageRespost
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sharePost(_ sender: UIButton) {
        
        let atributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Publicando...", attributes: atributes)
        
        WishareAPI.repost(id!, self.messageTextView.text) { (validate : Bool, error : Error?) in
            
            if error == nil {
                
                RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Publicado", completionTimeout: 1)
                
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                    self.dismiss(animated: true, completion: nil)
                })
                
            } else {
                RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "Erro", completionTimeout: 1)
            }
            
            
            
        }
        

    }
    
    @IBAction func cancel(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Descartar publicação", message: "Deseja descartar essa publicação ?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (alert : UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
       
    }
    

}
