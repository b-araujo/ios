//
//  CategoryCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/16/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryButton: RoundButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func pressCategory(_ sender: UIButton) {
        
    }
    
}
