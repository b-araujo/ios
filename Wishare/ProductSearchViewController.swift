//
//  ProductSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/22/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes
import RappleProgressHUD

class ProductSearchViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var resultNotFoundLabel: UILabel!
    
    @IBOutlet weak var topConstraintCollectionView: NSLayoutConstraint!
    
    var productResult = [Product]()
    var productResultOriginal = [Product]()
    var loader : Loader!
    var manager : Manager!
    
    // - Paginacao
    var isLoading : Bool = false
    var isPagination : Bool = false
    let footerViewReuseIdentifier = "footerViewReuseIdentifier"
    var footerView : FooterCollectionReusableView?
    // -

    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "BuyShareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "buyShareCell")
        self.collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerViewReuseIdentifier")
        
        NotificationCenter.default.addObserver(self, selector: #selector(searchUpdate(notification:)), name: Notification.Name.init("searchUpdate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateProductResult(notification:)), name: Notification.Name.init("productsResultUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loading(notification:)), name: Notification.Name.init("loadingTabProduct"), object: nil)
        
        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.searchProduct("16") { (products : [Product], error : Error?) in
            if error == nil {
                RappleActivityIndicatorView.stopAnimation()
                
                self.productResult = products
                
                let encodedProducts = NSKeyedArchiver.archivedData(withRootObject: products)
                UserDefaults.standard.set(encodedProducts, forKey: "initialProducts")
                self.collectionView.reloadData()
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Produtos WillApper")
        NotificationCenter.default.post(name: NSNotification.Name.init("changeTabs"), object: 0)
    }
    
    @objc func updateProductResult(notification: Notification) {
        
        self.productResult = notification.object as! [Product]
    
        print(self.productResult.count)
        
        if self.productResult.count == 0 {
            self.collectionView.isHidden = true
            self.resultNotFoundLabel.isHidden = false
        } else {
            self.collectionView.isHidden = false
            self.resultNotFoundLabel.isHidden = true
        }
    
        self.collectionView.reloadData()
    }

    @objc func loading(notification: Notification) {
        
        let isLoading : Bool = notification.object as! Bool
        
        if isLoading {
            self.collectionView.isHidden = true
            self.resultNotFoundLabel.isHidden = true
            self.loadingActivityIndicator.startAnimating()
        } else {
            self.loadingActivityIndicator.stopAnimating()
            self.collectionView.isHidden = false
        }
        
    }
    
    @objc func searchUpdate(notification: Notification) {
        
        let isSearch = notification.object as! Bool
        
        if isSearch {
            self.topConstraintCollectionView.constant = 53
            self.isPagination = true
        } else {
            self.topConstraintCollectionView.constant = 5
            self.isPagination = false
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.becomeFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init("productsResultUpdated"), object: nil)
    }
    
}

extension ProductSearchViewController : UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("openProductDetail"), object: self.productResult[indexPath.row].id!)
    }
    
    
}

extension ProductSearchViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyShareCell", for: indexPath) as! BuyShareCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        
        let url = urlBase.appendingPathComponent("files/products/\(self.productResult[indexPath.row].pId!)/thumb/\(self.productResult[indexPath.row].picture!)")
        
        self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
            cell.productImageView.image = result.value
        }
        
        cell.nameProductLabel.text = self.productResult[indexPath.row].brand
        cell.descriptionProductLabel.text = self.productResult[indexPath.row].name
        
        if self.productResult[indexPath.row].promo_price == "" {
            
            let price = "R$ \(self.productResult[indexPath.row].price!)".withFont(.boldSystemFont(ofSize: 14))
            
            cell.priceProductLabel.attributedText = price
            cell.promoPriceProductLabel.text = nil
            
        } else {
            
            let promo_price = "R$ \(self.productResult[indexPath.row].price!)".withStrikethroughStyle(.styleSingle).withBaselineOffset(0).withFont(.systemFont(ofSize: 14)).withTextColor(UIColor.lightGray)
            let price = "R$ \(self.productResult[indexPath.row].promo_price!)".withFont(.boldSystemFont(ofSize: 15)).withTextColor(UIColor.black)
            let por = " por ".withTextColor(UIColor.lightGray).withFont(.systemFont(ofSize: 14))
            
            cell.priceProductLabel.attributedText = promo_price + por
            cell.promoPriceProductLabel.attributedText = price
            
        }
        
        return cell
        
    }
    
}

extension ProductSearchViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 2) - 2.5, height: ((self.view.bounds.width / 2) - 5) + 115)
    }
    
    // - Paginacao
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if self.isLoading {
            return CGSize.zero
        }
        
        return CGSize(width: collectionView.bounds.size.width, height: 55.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter {
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
            self.footerView = footerView
            self.footerView?.backgroundColor = UIColor.clear
            return footerView
            
        } else {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold = 100.0
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold = Float((diffHeight - frameHeight))/Float(threshold);
        
        triggerThreshold = min(triggerThreshold, 0.0)
        let pullRatio = min(fabs(triggerThreshold),0.4);
        
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        
        if pullRatio >= 0.4 {
            self.footerView?.animateFinal()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        
        //print("ContentOffset: \(contentOffset)")
        //print("Content Height: \(contentHeight)")
        //print("Frame Height: \(frameHeight)")
        //print("pullHeight:\(pullHeight)");
        
        if pullHeight >= 0.0  && pullHeight <= 1.0 {
            if (self.footerView?.isAnimatingFinal)! {
                
                self.isLoading = true
                self.footerView?.startAnimate()
                
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                    
                    
                    if self.isPagination && (UserDefaults.standard.object(forKey: "isSearchTabs") as! Bool) != true {
                        
                        if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOption") as? [String : Any] {
                            
                            print("REFRESH APPENDING PARAMETROS ....")
                        
                            let orderBy = UserDefaults.standard.object(forKey: "orderBy") as! String
                            let typeBy = UserDefaults.standard.object(forKey: "typeBy") as! String
                            let page = UserDefaults.standard.object(forKey: "pageProductsFilters") as! String
                            let pageInt = Int(page)! + 1
                            UserDefaults.standard.set("\(pageInt)", forKey: "pageProductsFilters")
                            
                            WishareAPI.searchProductFilterByPagination(lastPreviewOption["text"] as! String, lastPreviewOption["category"] as! [String], lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], lastPreviewOption["store"] as! [String], orderBy, typeBy, page: "\(pageInt)", { (products : [Product], error : Error?) in
                                
                                if error == nil {
                                    self.productResult.append(contentsOf: products)
                                    self.collectionView.reloadData()
                                    self.isLoading = false
                                }
                                
                                
                            })
                            
                            
                        } else if let orderBy : String = UserDefaults.standard.object(forKey: "orderBy") as? String, let typeBy : String = UserDefaults.standard.object(forKey: "typeBy") as? String {
                            
                            if let page : String = UserDefaults.standard.object(forKey: "pageProductsFilters") as? String {
                             
                                print("REFRESH APPENDING ORDERBY ....")
                                
                                let word_search = UserDefaults.standard.object(forKey: "word_search") as! String
                                let pageInt = Int(page)! + 1
                                UserDefaults.standard.set("\(pageInt)", forKey: "pageProductsFilters")
                                
                                WishareAPI.searchProductFilterByPagination(word_search, [], [], [], [], [], orderBy, typeBy, page: "\(pageInt)", { ( products : [Product], error : Error?) in
                                    
                                    if error == nil {
                                        
                                        self.productResult.append(contentsOf: products)
                                        self.collectionView.reloadData()
                                        self.isLoading = false
                                        
                                    }
                                    
                                })
                                
                            } else {
                                self.collectionView.reloadData()
                                self.isLoading = false
                            }
                            
                        }
        
                        
                    } else {
                        self.collectionView.reloadData()
                        self.isLoading = false
                    }
                    
                })
                
            }
        }
    }
    
}
