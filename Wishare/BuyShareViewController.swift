//
//  BuyShareViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes

class BuyShareViewController: UIViewController {

    
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var post : Post!
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "BuyShareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "buyShareCell")
        
        self.nameStoreLabel.text = self.post.store!.name!
        self.navigationItem.title = "Compras de \(self.post.user!.username!)"
        
        if self.post.store!.following! {
            followButton.backgroundColor = UIColor.white
            followButton.setTitle("Seguindo", for: .normal)
            followButton.setTitleColor(UIColor.black, for: .normal)
            followButton.layer.borderWidth = 2.0
            followButton.layer.borderColor = UIColor.black.cgColor
        } else {
            followButton.backgroundColor = UIColor.black
            followButton.setTitle("Seguir", for: .normal)
            followButton.setTitleColor(UIColor.white, for: .normal)
            followButton.layer.borderWidth = 0.0
            followButton.layer.borderColor = UIColor.clear.cgColor
        }
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.post.store!.picture!)")
        
        
        self.manager.loadImage(with: urlStore, into: self.storeImageView) { ( result, _ ) in
            self.storeImageView.image = result.value?.circleMask
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func follow(_ sender: UIButton) {
        
        if sender.title(for: .normal) == "Seguir" {
                
            WishareAPI.followStore(self.post.store!.id!, true, { (result : [String : AnyObject], error : Error?) in
                    
                if error == nil {
                        
                    sender.backgroundColor = UIColor.white
                    sender.setTitle("Seguindo", for: .normal)
                    sender.setTitleColor(UIColor.black, for: .normal)
                    sender.layer.borderWidth = 2.0
                    sender.layer.borderColor = UIColor.black.cgColor
                        
                }
                    
            })
            
            
        } else {
            
            WishareAPI.followStore(self.post.store!.id!, false, { (result : [String : AnyObject], error : Error?) in
                    
                if error == nil {
                        
                    sender.backgroundColor = UIColor.black
                    sender.setTitle("Seguir", for: .normal)
                    sender.setTitleColor(UIColor.white, for: .normal)
                    sender.layer.borderWidth = 0.0
                    sender.layer.borderColor = UIColor.clear.cgColor
                        
                }
                    
            })
                
        }
        
    }

}

extension BuyShareViewController : UICollectionViewDelegate {
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        
        if self.post.products.count > 0 {
            productDetailViewController.idProduct =  self.post.products[indexPath.row]["id"]
        } else {
            productDetailViewController.idProduct =  self.post.products_info[indexPath.row].id!
        }
        
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
}

extension BuyShareViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.post.products_info.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyShareCell", for: indexPath) as! BuyShareCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        
        let url = urlBase.appendingPathComponent("files/orders_itens/\(self.post.products_info[indexPath.row].pId!)/\(self.post.products_info[indexPath.row].picture!)")
        
        self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
            cell.productImageView.image = result.value
        }
        
        cell.nameProductLabel.text = self.post.products_info[indexPath.row].brand
        cell.descriptionProductLabel.text = self.post.products_info[indexPath.row].name
        
        if self.post.products_info[indexPath.row].promo_price == "" {
            
            let price = "R$ \(self.post.products_info[indexPath.row].price!)".withFont(.boldSystemFont(ofSize: 14))
        
            cell.priceProductLabel.attributedText = price
            cell.promoPriceProductLabel.text = nil
            
        } else {
            
            let promo_price = "R$ \(self.post.products_info[indexPath.row].promo_price!)".withStrikethroughStyle(.styleSingle).withBaselineOffset(0).withFont(.systemFont(ofSize: 14)).withTextColor(UIColor.lightGray)
            let price = "R$ \(self.post.products_info[indexPath.row].price!)".withFont(.boldSystemFont(ofSize: 15)).withTextColor(UIColor.black)
            let por = " por ".withTextColor(UIColor.lightGray).withFont(.systemFont(ofSize: 14))
            
            cell.priceProductLabel.attributedText = promo_price + por
            cell.promoPriceProductLabel.attributedText = price
        }
        
        
        return cell
    }
    
}

extension BuyShareViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 2) - 5, height: ((self.view.bounds.width / 2) - 30) + 135)
    }
    
}
