//
//  AddressOrderViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SkyFloatingLabelTextField

class AddressOrderViewController: UIViewController {

    @IBOutlet weak var tipoEnderecoLabel: UILabel!
    @IBOutlet weak var nomeDaRuaLabel: UILabel!
    @IBOutlet weak var bairroCidadeEstadoLabel: UILabel!
    @IBOutlet weak var cepLabel: UILabel!
    
    @IBOutlet weak var tipoEnderecoCobrancaLabel: UILabel!
    @IBOutlet weak var nomeDaRuaCobrancaLabel: UILabel!
    @IBOutlet weak var bairroCidadeEstadoCobrancaLabel: UILabel!
    @IBOutlet weak var cepCobrancaLabel: UILabel!
    
    @IBOutlet weak var tipoEntregaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBOutlet weak var continuosButton : UIButton!
    
    var totalPrice : Double = 0.0
    
    var myAddressPaymentSelected : [[String : String]] = []
    var myAddressShippingSelected : [[String : String]] = []
    
    var store : Store!
    var cep : String = ""
    var fretes : [[String : AnyObject]] = []
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.continuosButton.layer.cornerRadius = 5.0
        self.continuosButton.layer.masksToBounds = false
        self.continuosButton.layer.shadowColor = UIColor.black.cgColor
        self.continuosButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.continuosButton.layer.shadowRadius = 2.0
        self.continuosButton.layer.shadowOpacity = 0.24
        
        
        self.navigationController?.navigationBar.backItem?.title = "    "
        self.store.order.products = self.store.products
        
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)

        for product in self.store.products {
            
            totalPrice += (product.select_quatity! as NSString).doubleValue * ((product.promo_price! == "") ? (product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)
            
        }
        
        self.totalPriceLabel.text = "\(self.totalPrice.asLocaleCurrency)"
        
        WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                var primaryS = false
                var primaryP = false
                
                for address in addresses {
                    
                    if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                        
                        if (oneAddress["type"] as! String) == "P" && primaryP ==  false {
                        
                            self.tipoEnderecoCobrancaLabel.text = oneAddress["name"] as? String ?? ""
                            self.nomeDaRuaCobrancaLabel.text = "\(oneAddress["address"] as? String ?? ""), \(oneAddress["number"] as? String ?? "")"
                            if let complemento = oneAddress["more"] as? String, !complemento.isEmpty {
                                self.nomeDaRuaCobrancaLabel.text?.append(", \(complemento)")
                            }
                            self.bairroCidadeEstadoCobrancaLabel.text = "\((oneAddress["neighborhood"] as! String)) - \((oneAddress["city"] as! String)) - \((oneAddress["state"] as! String))"
                            self.cepCobrancaLabel.text = "C.E.P.: \((oneAddress["post_code"] as! String))"
                            primaryP = true
                            
                        } else if (oneAddress["type"] as! String) == "S" && primaryS ==  false {
                            
                            self.store.order.id_address = oneAddress["id"] as? String
                            
                            self.tipoEnderecoLabel.text = oneAddress["name"] as? String
                            self.nomeDaRuaLabel.text = "\(oneAddress["address"] as? String ?? ""), \(oneAddress["number"] as? String ?? "")"
                            if let complemento = oneAddress["more"] as? String, !complemento.isEmpty {
                                self.nomeDaRuaLabel.text?.append(", \(complemento)")
                            }
                            self.bairroCidadeEstadoLabel.text = "\((oneAddress["neighborhood"] as! String)) - \((oneAddress["city"] as! String)) - \((oneAddress["state"] as! String))"
                            self.cepLabel.text = "C.E.P.: \((oneAddress["post_code"] as! String))"
                            
                            self.cep = oneAddress["post_code"] as! String
                            primaryS = true
                            
                        }
                        
                    }
                    
                }
                
                
                WishareAPI.postageService(storeId: self.store.id!, cep: self.cep, products: self.store.products, { (fretes : [[String : AnyObject]], error : Error?) in
                    if error == nil {
                        self.fretes = fretes
                        self.tipoEntregaTextField.delegate = self
                        RappleActivityIndicatorView.stopAnimation()
                    }
                })
                
                
            }
        }
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.myAddressPaymentSelected.count > 0 {
            
            self.tipoEnderecoCobrancaLabel.text = self.myAddressPaymentSelected[0]["name"]
            self.nomeDaRuaCobrancaLabel.text = self.myAddressPaymentSelected[0]["address"]
            self.bairroCidadeEstadoCobrancaLabel.text = "\(self.myAddressPaymentSelected[0]["neighborhood"]!) - \(self.myAddressPaymentSelected[0]["city"]!) - \(self.myAddressPaymentSelected[0]["state"]!)"
            self.cepCobrancaLabel.text = "C.E.P.: \(self.myAddressPaymentSelected[0]["post_code"]!)"
            
        }
        
        if self.myAddressShippingSelected.count > 0 {
            
            self.store.order.id_address = self.myAddressShippingSelected[0]["id"]
            
            self.tipoEnderecoLabel.text = self.myAddressShippingSelected[0]["name"]
            self.nomeDaRuaLabel.text = self.myAddressShippingSelected[0]["address"]
            self.bairroCidadeEstadoLabel.text = "\(self.myAddressShippingSelected[0]["neighborhood"]!) - \(self.myAddressShippingSelected[0]["city"]!) - \(self.myAddressShippingSelected[0]["state"]!)"
            self.cepLabel.text = "C.E.P.: \(self.myAddressShippingSelected[0]["post_code"]!)"
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "cardOrderSegue" {
            let cardOrderViewController = segue.destination as! CardOrderViewController
            cardOrderViewController.store = sender as! Store
        }
        
        
        if segue.identifier == "registerCardOrderSegue" {
            let registerCardOrderViewController = segue.destination as! RegisterCardOrderViewController
            registerCardOrderViewController.store = sender as! Store
        }
        
    }
    
    @IBAction func continuosPage ( _ sender : UIButton) {
        
        if (self.tipoEntregaTextField.text?.isEmpty)! {
            
            self.tipoEntregaTextField.errorMessage = "Selecione"
            
            let alertController = UIAlertController(title: nil, message: "Selecione o tipo de entrega", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
            
            WishareAPI.getCards({ (cards : [[String : AnyObject]], error : Error?) in
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation()
                    
                    if cards.count > 0 {
                        
                        self.performSegue(withIdentifier: "cardOrderSegue", sender: self.store)
                        
                    } else {
                        
                        self.performSegue(withIdentifier: "registerCardOrderSegue", sender: self.store)
                        
                    }
                    
                }
            })
            
            
        }
        
    }
    
    @IBAction func alterAddressPayment ( _ sender : UIButton) {
        
        let storyboard = UIStoryboard(name: "Setting", bundle: nil)
        let myAddressViewController = storyboard.instantiateViewController(withIdentifier: "myAddressID") as! MyAddressViewController
        myAddressViewController.selectedItem = true
        myAddressViewController.isPayment = true
        
        self.navigationController?.pushViewController(myAddressViewController, animated: true)
        
    }

    @IBAction func alterAddressShipping( _ sender : UIButton) {
        
        let storyboard = UIStoryboard(name: "Setting", bundle: nil)
        let myAddressViewController = storyboard.instantiateViewController(withIdentifier: "myAddressID") as! MyAddressViewController
        myAddressViewController.selectedItem = true
        myAddressViewController.isPayment = false
        
        self.navigationController?.pushViewController(myAddressViewController, animated: true)
        
    }
    
}

extension AddressOrderViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.becomeFirstResponder()
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for frete in self.fretes {
            
            let action = UIAlertAction(title: "\((frete["name"] as! String)) - R$ \((frete["value"] as! String)) - \((frete["days"] as! String)) dia(s)", style: .default, handler: { ( _ ) in
                self.store.order.freight_value = (frete["value"] as! String)
                self.store.order.id_service = (frete["cod"] as! String)
                self.store.order.delivery_days = (frete["days"] as! String)
                self.store.order.type_freight = (frete["name"] as! String)
                self.tipoEntregaTextField.text = "\((frete["name"] as! String)) - R$ \((frete["value"] as! String)) - \((frete["days"] as! String)) dia(s)"
                self.totalPriceLabel.text = "\((self.totalPrice + (frete["value"]!.components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue).asLocaleCurrency)"
                self.tipoEntregaTextField.errorMessage = nil
            })
            
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
}
