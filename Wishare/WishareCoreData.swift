//
//  WishareCoreData.swift
//  Wishare
//
//  Created by Wishare iMac on 4/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import UIKit
import CoreData


var entityName : String!

class WishareCoreData {
    
    class func isLoggedIn ( _ completion : @escaping ( _ isLoggedIn : Bool, _ error : Error?, _ firstAccess : String?) -> Void) {
    
        entityName = "UserActive"
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchFirstRequest.fetchLimit = 1
        
        var result = [UserActive]()
        
        do {
            
            let records = try managedContext.fetch(fetchFirstRequest)
            
            if let records = records as? [UserActive] {
                result = records
            }
            
        } catch let error {
            completion(false, error, nil)
        }
        
        
        if result.count == 1 {
            
            if let userFirst = result.first {
                
                print("API - isLoggedIn - userAuthorization")
                 
                
                WishareAPI.userAuthorization(userFirst.username!, userFirst.password!, userFirst.isSocial, { (result : Bool, error : Error?, firstAccess : String?) in
                
                    print("userAuthorization")
                    if result {
                        completion(true, nil, firstAccess)
                    } else {
                        completion(false, error, nil)
                    }
                    
                })
                
            }
            
        } else {
            completion(false, nil, nil)
        }
        
        
    }
    
    class func getFirstUser( _ completion : @escaping ( _ result : UserActive?, _ error : Error?) -> Void) {
        
        
        entityName = "UserActive"
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchFirstRequest.fetchLimit = 1
        
        var result = [UserActive]()
        
        do {
            
            let records = try managedContext.fetch(fetchFirstRequest)
            
            if let records = records as? [UserActive] {
                result = records
            }
            
        } catch let error {
            completion(nil, error)
        }
        
        
        if result.count == 1 {
            
            if let userFirst = result.first {
                completion(userFirst, nil)
            }
            
        } else {

            completion(nil, nil)
        }
        
        
    }
    
    class func saveUser(_ user : User, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "UserActive", in: managedContext)!
        let userEntity = NSManagedObject(entity: entity, insertInto: managedContext)
        
        
        if let birthday_date = user.birthday_date {
            userEntity.setValue(birthday_date, forKey: "birthday_date")
        }
        
        if let bloggers = user.bloggers {
            userEntity.setValue(bloggers, forKey: "bloggers")
        }
        
        if let cell = user.cell {
            userEntity.setValue(cell, forKey: "cell")
        }
        
        if let cpf = user.cpf {
            userEntity.setValue(cpf, forKey: "cpf")
        }
        
        if let email = user.email {
            userEntity.setValue(email, forKey: "email")
        }
        
        if let followers = user.followers {
            userEntity.setValue(followers, forKey: "followers")
        }
        
        if let gender = user.gender {
            userEntity.setValue(gender, forKey: "gender")
        }
        
        if let name = user.name {
            userEntity.setValue(name, forKey: "name")
        }
        
        if let picture = user.picture {
            userEntity.setValue(picture, forKey: "picture")
        }
        
        if let rg = user.rg {
            userEntity.setValue(rg, forKey: "rg")
        }
        
        if let state = user.state {
            userEntity.setValue(state, forKey: "state")
        }
        
        if let stores = user.stores {
            userEntity.setValue(stores, forKey: "stores")
        }
        
        if let tokenhash = user.tokenhash {
            userEntity.setValue(tokenhash, forKey: "tokenhash")
        }
        
        if let username = user.username {
            userEntity.setValue(username, forKey: "username")
        }
        
        if let password = user.password {
            userEntity.setValue(password, forKey: "password")
        }
        
        if let isSocial = user.isSocial {
            userEntity.setValue(isSocial, forKey: "isSocial")
        }
        
        if let id = user.id {
            userEntity.setValue(id, forKey: "id")
        }
        
        if let active = user.active {
            userEntity.setValue(active, forKey: "active")
        }
        
        if let lastName = user.last_name {
            userEntity.setValue(lastName, forKey: "last_name")
        }
        
        do {
            try managedContext.save()
            completion(nil)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completion(error)
        }
        
    }

    class func deleteUsers( _ completion : @escaping ( _ error : Error?) -> Void) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserActive")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
            completion(nil)
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
            completion(error)
        }
        
        
    }
    
    // - WS_RecentSearch
    class func recentSearch( text : String?, method : CDMethod, _ completion : @escaping ( _ words : [String], _ error : Error?) -> Void ) {
        
        switch method {
            
            case .save:
            
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                let managedContext = appDelegate.persistentContainer.viewContext
                let entity = NSEntityDescription.entity(forEntityName: "WS_RecentSearch", in: managedContext)!
                let recentSearchEntity = NSManagedObject(entity: entity, insertInto: managedContext)
                
                recentSearchEntity.setValue(text!, forKey: "word")
                
                do {
                    try managedContext.save()
                    completion([], nil)
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                    completion([], error)
                }
                
                
                break
            
            case .deleteAll:
            
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                let managedContext = appDelegate.persistentContainer.viewContext
                let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_RecentSearch")
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
                
                do {
                    try managedContext.execute(deleteRequest)
                    try managedContext.save()
                    completion([], nil)
                } catch let error as NSError {
                    print("Could not delete. \(error), \(error.userInfo)")
                    completion([], error)
                }
        
                break
            
            case .findAll:
                
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                let managedContext = appDelegate.persistentContainer.viewContext
                
                let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_RecentSearch")
                
                var result : [String] = []
                
                do {
                    
                    let records = try managedContext.fetch(fetchFirstRequest)
                    
                    
                    if let wordsEntity = records as? [WS_RecentSearch] {
                        for word in wordsEntity {
                            result.append(word.word!)
                        }
                    }
                    
                    completion(result, nil)
                } catch let error {
                    completion([], error)
                }
                
                break
        }
        
        
    }
    
    class func addShoppingCart(name : String, size : String, color : String, quantity : String, price : String, idProduct : String, idStore : String, nameStore : String, completion : @escaping ( _ validate : Bool, _ error : Error?) -> Void) {
    
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let storeExistsManagedContext = appDelegate.persistentContainer.viewContext
        
        // VERIFICAR SE A LOJA EXISTE
        let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_Store")
        var result : [String] = []
        
        do {
            let records = try storeExistsManagedContext.fetch(fetchFirstRequest)
            if let storeEntity = records as? [WS_Store] {
                for store in storeEntity {
                    result.append(store.id_store!)
                }
            }
        } catch let error {
            print("Error ao verificar loja existente. \(error)")
            completion(false, error)
        }
        
        
        if result.contains(idStore) {
            
            // SE EXISTIR, VINCULAR O PRODUTO A LOJA
            
            guard let appDelegateProduct = UIApplication.shared.delegate as? AppDelegate else { return }
            let productManagedContext = appDelegateProduct.persistentContainer.viewContext
            let shoppingCartEntity = NSEntityDescription.entity(forEntityName: "WS_ShoppingCart", in: productManagedContext)!
            let createRowEntity = NSManagedObject(entity: shoppingCartEntity, insertInto: productManagedContext)
            
            createRowEntity.setValue(idProduct, forKey: "id_product")
            createRowEntity.setValue(name, forKey: "name_product")
            createRowEntity.setValue(size, forKey: "size_product")
            createRowEntity.setValue(color, forKey: "color_product")
            createRowEntity.setValue(quantity, forKey: "quantity_product")
            createRowEntity.setValue(price, forKey: "price_product")
            createRowEntity.setValue(idStore, forKey: "id_store")
            
            do {
                try productManagedContext.save()
                completion(true, nil)
            } catch let error as NSError {
                print("Error ao salvar produto a loja já existe. \(error), \(error.userInfo)")
                completion(false, error)
            }

            
        } else {
            
            // SENAO ADICIONAR A LOJA E DEPOIS VINCULAR O PRODUTO
            let storeManagedContext = appDelegate.persistentContainer.viewContext
            let storeEntity = NSEntityDescription.entity(forEntityName: "WS_Store", in: storeManagedContext)!
            let createRowStoreEntity = NSManagedObject(entity: storeEntity, insertInto: storeManagedContext)
            
            createRowStoreEntity.setValue(idStore, forKey: "id_store")
            createRowStoreEntity.setValue(nameStore, forKey: "name_store")

            do {
                try storeManagedContext.save()
            } catch let error as NSError {
                print("Error ao salvar loja nao existente. \(error), \(error.userInfo)")
                completion(false, error)
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            let shoppingCartEntity = NSEntityDescription.entity(forEntityName: "WS_ShoppingCart", in: managedContext)!
            let createRowEntity = NSManagedObject(entity: shoppingCartEntity, insertInto: managedContext)
            
            createRowEntity.setValue(idProduct, forKey: "id_product")
            createRowEntity.setValue(name, forKey: "name_product")
            createRowEntity.setValue(size, forKey: "size_product")
            createRowEntity.setValue(color, forKey: "color_product")
            createRowEntity.setValue(quantity, forKey: "quantity_product")
            createRowEntity.setValue(price, forKey: "price_product")
            createRowEntity.setValue(idStore, forKey: "id_store")
            
            do {
                try managedContext.save()
                completion(true, nil)
            } catch let error as NSError {
                print("Error ao salvar produto a loja já existe. \(error), \(error.userInfo)")
                completion(false, error)
            }
            
        }
        
    }
    
    class func showShoppingCart(completion : @escaping ( _ shoppingCart : [WS_ShoppingCart], _ store : [WS_Store], _ error : Error?) -> Void) {
    
        // PRODUCTS SHOPPING CART
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_ShoppingCart")
        
        var result : [WS_ShoppingCart] = []
        
        do {
            
            let records = try managedContext.fetch(fetchFirstRequest)
            
            if let shoppingCartiEntity = records as? [WS_ShoppingCart] {
                result.append(contentsOf: shoppingCartiEntity)
            }
            
        } catch let error {
            completion([], [], error)
        }
        
        
        // STORE
        
        guard let storeAppDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let storeManagedContext = storeAppDelegate.persistentContainer.viewContext
        
        let fetchFirstRequestStore = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_Store")
        
        var resultStore : [WS_Store] = []
        
        do {
            
            let records = try storeManagedContext.fetch(fetchFirstRequestStore)
            
            if let storeEntity = records as? [WS_Store] {
                resultStore.append(contentsOf: storeEntity)
            }
            
            completion(result, resultStore, nil)
        } catch let error {
            completion([], [], error)
        }
        
    }
    
    class func removeShoppingCart(pId : String, size : String, sId : String, completion : @escaping ( _ error : Error?) -> Void) {
        
        // PRODUCTS SHOPPING CART
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_ShoppingCart")
        
        var result : [String] = []
        
        do {
            
            let records = try managedContext.fetch(fetchFirstRequest)
            
            if let shoppingCartiEntity = records as? [WS_ShoppingCart] {
                
                for shop in shoppingCartiEntity {
                    
                    if shop.id_store! == sId && shop.id_product! == pId && shop.size_product! == size {
                        
                        managedContext.delete(shop)
                        
                        do {
                            try managedContext.save()
                        } catch let error as NSError {
                            print("Error ao salvar produto a loja já existe. \(error), \(error.userInfo)")
                        }
                        
                    } else {
                        result.append(shop.id_store!)
                    }
                    
                }
                
            }
            
        } catch let error {
            completion(error)
        }
        
        
        // STORE
        
        guard let storeAppDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let storeManagedContext = storeAppDelegate.persistentContainer.viewContext
        
        let fetchFirstRequestStore = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_Store")
        
        do {
            
            let records = try storeManagedContext.fetch(fetchFirstRequestStore)
            
            if let storeEntity = records as? [WS_Store] {
                
                for store in storeEntity {
                    
                    if !(result.contains(store.id_store!)) {
                        
                        storeManagedContext.delete(store)
                        
                        do {
                            try storeManagedContext.save()
                        } catch let error as NSError {
                            print("Error ao salvar produto a loja já existe. \(error), \(error.userInfo)")
                        }
                        
                    }
                    
                }
                
            }
            
            completion(nil)
        } catch let error {
            completion(error)
        }


        
    }
    
    
    
    class func clearShoppingCart(completion : @escaping ( _ error : Error?) -> Void) {
        
        // PRODUCTS SHOPPING CART
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchFirstRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_ShoppingCart")
        
        do {
            
            let records = try managedContext.fetch(fetchFirstRequest)
            
            if let shoppingCartiEntity = records as? [WS_ShoppingCart] {
                
                for shop in shoppingCartiEntity {
                    
                    managedContext.delete(shop)
                    
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    print("Error ao apagar produto a loja. \(error), \(error.userInfo)")
                }
                
            }
            
        } catch let error {
            completion(error)
        }
        
        
        // STORE
        
        guard let storeAppDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let storeManagedContext = storeAppDelegate.persistentContainer.viewContext
        
        let fetchFirstRequestStore = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_Store")
        
        do {
            
            let records = try storeManagedContext.fetch(fetchFirstRequestStore)
            
            if let storeEntity = records as? [WS_Store] {
                
                for store in storeEntity {
                    storeManagedContext.delete(store)
                }
            }
            
            do {
                try storeManagedContext.save()
            } catch let error as NSError {
                print("Error ao apagar loja. \(error), \(error.userInfo)")
            }
            
            
            
            completion(nil)
        } catch let error {
            completion(error)
        }
        
        
        
    }
    
    class func quantityUpdateShoppingCart(quantity : String, pId : String, completion : @escaping ( _ error : Error?) -> Void) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WS_ShoppingCart")
    
        fetchRequest.predicate = NSPredicate(format: "id_product = \(pId)")
        
        do {
            
            let list = try managedContext.fetch(fetchRequest) as? [WS_ShoppingCart]
            
            if list!.count == 1 {

                list!.first!.setValue(quantity, forKey: "quantity_product")
                
                do {
                    try managedContext.save()
                    print("saved!")
                    completion(nil)
                } catch let error as NSError  {
                    completion(error)
                    print("Could not save \(error), \(error.userInfo)")
                }
    
            }
            
        } catch let error as NSError {
            completion(error)
            print("Fetch failed: \(error.localizedDescription)")
        }
        
    }
    
}

enum CDMethod {
    case save
    case deleteAll
    case findAll
}
