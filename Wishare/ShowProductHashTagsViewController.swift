//
//  ShowProductHashTagsViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/21/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class ShowProductHashTagsViewController: UIViewController {

    
    @IBOutlet weak var collectionView : UICollectionView!
    
    var textHashTag : String!
    var products : [Product] = []
    
    var manager : Manager!
    var loader : Loader!
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: self.attributes)
        
        WishareAPI.hashTagsProduct(text: self.textHashTag) { (products : [Product], error : Error?) in
            if error == nil {
                
                self.products = products
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
        }
        
        self.navigationItem.title = "#\(self.textHashTag!)"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ShowProductHashTagsViewController : UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.products[indexPath.row].id!
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
}

extension ShowProductHashTagsViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/products/\(self.products[indexPath.row].pId!)/thumb/\(self.products[indexPath.row].picture!)")
        
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { (result, _) in
            cell.imageProductsLook.image = result.value
        }
        
        return cell
        
    }
    
}

extension ShowProductHashTagsViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 2.5 , height: (self.view.bounds.width / 3) - 1.25)
    }
    
}
