//
//  Tag.swift
//  Wishare
//
//  Created by Wishare iMac on 4/25/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Tag {
    
    var id : Int?
    var text : String?
    var leftMargin : Float?
    var topMargin : Float?
    var screenWidth : Int?
    var type : String?
    var storeId : Int?
    
    
    init( _ tag : [String : AnyObject]) {

        self.id = tag["id"] as? Int
        self.text = tag["text"] as? String
        self.leftMargin = tag["leftMargin"] as? Float
        self.topMargin = tag["topMargin"] as? Float
        self.screenWidth = tag["screenWidth"] as? Int
        self.type = tag["type"] as? String
        self.storeId = tag["storeId"] as? Int
    }
    
}


//"id": 1,
//"text": "Adidas",
//"leftMargin": 8.518518,
//"topMargin": 50.555557,
//"screenWidth": 540,
//"type": "BRAND",
//"storeId": 0
