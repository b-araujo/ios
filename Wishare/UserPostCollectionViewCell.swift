//
//  UserPostCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/25/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class UserPostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var moreImageView: UIImageView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var dreamImageView: UIImageView!
    @IBOutlet weak var commentUserLabel: ActiveLabel!
    @IBOutlet weak var postDateLabel: UILabel!
    
    @IBOutlet weak var userNameTopButton: UIButton!
    @IBOutlet weak var userNameBottomButton: UIButton!
    
    @IBOutlet weak var tagInformation: UIView!
    @IBOutlet weak var tagImageView: UIImageView!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    
    @IBOutlet weak var viewLikes: UIView!
    @IBOutlet weak var countLikeLabel: UILabel!
    @IBOutlet weak var countCommentsLabel: UILabel!
    
    @IBOutlet weak var moreButton: UIButton!
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    
    var delegate : UserPostCollectionViewCellDelegate?
    var lastScale : CGFloat = 1.0
    var tagOnOrOff = false
    var indexPathNow : IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tagInformation.layer.cornerRadius = 20
        
        let tapGestureRecognizerUserImageView = UITapGestureRecognizer(target: self, action: #selector(tapUserImage))
        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMore))
        let tapGestureRecognizerPostImageView = UITapGestureRecognizer(target: self, action: #selector(tapPostImage))
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerDreamImageView = UITapGestureRecognizer(target: self, action: #selector(tapGift))
        let tapGestureRecognizerTagInformation = UITapGestureRecognizer(target: self, action: #selector(tapTag))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapLikesView))
        let tapGestureRecognizerCommentShowLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
//        let tapGestureRecognizerViewMore = UITapGestureRecognizer(target: self, action: #selector())
        
        self.userImageView.addGestureRecognizer(tapGestureRecognizerUserImageView)
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        self.postImageView.addGestureRecognizer(tapGestureRecognizerPostImageView)
        self.likeImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentImageView.addGestureRecognizer(tapGestureRecognizerCommentImageView)
        self.shareImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        self.dreamImageView.addGestureRecognizer(tapGestureRecognizerDreamImageView)
        self.tagImageView.addGestureRecognizer(tapGestureRecognizerTagInformation)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        self.countCommentsLabel.addGestureRecognizer(tapGestureRecognizerCommentShowLabel)
        
        self.tagInformation.backgroundColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 0.5)
        
        
//        let pinchGestureRecognizerPostImageView = UIPinchGestureRecognizer(target: self, action: #selector(zoom(sender:)))
//        self.postImageView.addGestureRecognizer(pinchGestureRecognizerPostImageView)
        
    }
    
    
    
    func zoom(sender: UIPinchGestureRecognizer) {
        
        if(sender.state == .began) {
            lastScale = sender.scale
        }
        
        if (sender.state == .began || sender.state == .changed) {
            let currentScale = sender.view!.layer.value(forKeyPath:"transform.scale")! as! CGFloat
            let kMaxScale:CGFloat = 2.0
            let kMinScale:CGFloat = 1.0
            var newScale = 1 -  (lastScale - sender.scale)
            newScale = min(newScale, kMaxScale / currentScale)
            newScale = max(newScale, kMinScale / currentScale)
            let transform = (sender.view?.transform)!.scaledBy(x: newScale, y: newScale);
            sender.view?.transform = transform
            lastScale = sender.scale
        }
        
    }

    @objc func tapTag() {
        self.delegate?.tagInformationUserPost(cell: self)
    }
    
    @objc func tapMore() {
        self.delegate?.moreUserPost(cell: self)
    }
    
    @objc func tapLike() {
        self.delegate?.likeUserPost(cell: self)
    }
    
    @objc func tapComment() {
        self.delegate?.commentUserPost(cell: self)
    }
    
    @objc func tapShare() {
        self.delegate?.shareUserPost(cell: self, indexPathNow)
    }
    
    @objc func tapGift() {
        self.delegate?.giftUserPost(cell: self)
    }
    
    @objc func tapUserImage() {
        self.delegate?.userImageUserPost(cell: self)
    }
    
    @objc func tapPostImage() {
        self.delegate?.postImageUserPost(cell: self)
    }

    @objc func tapLikesView() {
        self.delegate?.viewLikes(cell: self)
    }
    
    @IBAction func tapUserName(_ sender: UIButton) {
        self.delegate?.userNameUserPost(cell: self)
    }
    

    
}

protocol UserPostCollectionViewCellDelegate {
    
    func moreUserPost(cell : UserPostCollectionViewCell)
    func likeUserPost(cell : UserPostCollectionViewCell)
    func commentUserPost(cell : UserPostCollectionViewCell)
    func shareUserPost(cell : UserPostCollectionViewCell, _ indexPath : IndexPath)
    func giftUserPost(cell : UserPostCollectionViewCell)
    func userImageUserPost(cell : UserPostCollectionViewCell)
    func postImageUserPost(cell : UserPostCollectionViewCell)
    func userNameUserPost(cell : UserPostCollectionViewCell)
    func tagInformationUserPost(cell : UserPostCollectionViewCell)
    func viewLikes(cell : UserPostCollectionViewCell)
    
}
