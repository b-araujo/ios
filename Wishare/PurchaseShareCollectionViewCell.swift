//
//  PurchaseShareCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PurchaseShareCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var storeThumbImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var firstProductPurchase: UIImageView!
    @IBOutlet weak var twoProductPurchaseImageView: UIImageView!
    @IBOutlet weak var threeProductPurchaseImageView: UIImageView!
    @IBOutlet weak var fourProductPurchaseImageView: UIImageView!
    
    var delegate : PurchaseShareDelegate? = nil
    var indexPath : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureRecognizerStoreThumbImageView = UITapGestureRecognizer(target: self, action: #selector(openStoreImageView))
        let tapGestureRecognizerNameStoreLabel = UITapGestureRecognizer(target: self, action: #selector(openStoreLabel))
        let tapGestureRecognizerFirstProductPurchaseImageView = UITapGestureRecognizer(target: self, action: #selector(openProductShare))
        
        self.storeThumbImageView.addGestureRecognizer(tapGestureRecognizerStoreThumbImageView)
        self.nameStoreLabel.addGestureRecognizer(tapGestureRecognizerNameStoreLabel)
        self.firstProductPurchase.addGestureRecognizer(tapGestureRecognizerFirstProductPurchaseImageView)
        
        
    }
    
    @objc func openStoreImageView () {
        self.delegate?.openStoreByThumb(self.indexPath)
    }
    
    @objc func openStoreLabel() {
        self.delegate?.openStoreByLabel(self.indexPath)
    }
    
    @objc func openProductShare() {
        self.delegate?.openProductsByImageView(self.indexPath)
    }
    

}

protocol PurchaseShareDelegate {
    func openStoreByThumb(_ indexPath : IndexPath)
    func openStoreByLabel(_ indexPath : IndexPath)
    func openProductsByImageView(_ indexPath : IndexPath)
}
