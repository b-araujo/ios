//
//  StoreSugestionCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare on 21/03/18.
//  Copyright © 2018 Wishare iMac. All rights reserved.
//

import UIKit
import iCarousel
import Kingfisher

protocol StoreSuggestionsProtocol {
    func productTapped(productId: String)
    func openStore(storeId: String)
    func moreStores(ids: [String]?, cell: StoreSugestionCollectionViewCell)
}

class StoreSugestionCollectionViewCell: UICollectionViewCell, iCarouselDataSource, iCarouselDelegate, StoreCarouselProtocol {
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var storeCarousel: iCarousel!
    
    var delegate: StoreSuggestionsProtocol?
    
    var stores : [Store]?
    
    var products : [[Product]]? {
        didSet {
            storeCarousel.reloadData()
        }
    }
    
    var moreStores: Bool = true

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        logoImageView.layer.cornerRadius = logoImageView.frame.size.width / 2
        logoImageView.clipsToBounds = true
        
        storeCarousel.delegate = self
        storeCarousel.dataSource = self
        storeCarousel.type = .linear
        storeCarousel.centerItemWhenSelected = false
        
        storeCarousel.contentOffset = CGSize(width: -(storeCarousel.frame.width/7.5) + 10, height: -30)
        storeCarousel.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        if moreStores {
            if let storesDownloaded = stores {
                return storesDownloaded.count + 1
            }
        } else {
            if let storesDownloaded = stores {
                return storesDownloaded.count
            }
        }
        
        return 0
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        if let reusedView = view as? StoreCarousel {
            if moreStores {
                if let storeCount = stores?.count, index < storeCount {
                    reusedView.store = stores?[index]
                    reusedView.products = products?[index]
                    reusedView.loadMoreView.isHidden = true
                    reusedView.borderView.isHidden = false
                } else {
                    // Load More
                    reusedView.loadMoreView.isHidden = false
                    reusedView.borderView.isHidden = true
                }
            } else{
                reusedView.store = stores?[index]
                reusedView.products = products?[index]
                reusedView.loadMoreView.isHidden = true
                reusedView.borderView.isHidden = false
            }

            return reusedView
        }
        else {
            let newView : StoreCarousel = StoreCarousel(frame: CGRect(x: storeCarousel.frame.minX, y: storeCarousel.frame.minY, width: storeCarousel.frame.width/1.5, height: storeCarousel.frame.height/1.5))
            if moreStores {
                if let storeCount = stores?.count, index < storeCount {
                    newView.delegate = self
                    newView.store = stores?[index]
                    newView.products = products?[index]
                    newView.loadMoreView.isHidden = true
                    newView.borderView.isHidden = false
                } else {
                    // Load More
                    newView.loadMoreView.isHidden = false
                    newView.borderView.isHidden = true
                    newView.delegate = self
                }
            }
            else {
                newView.delegate = self
                newView.store = stores?[index]
                newView.products = products?[index]
                newView.loadMoreView.isHidden = true
                newView.borderView.isHidden = false
            }
            
            return newView
        }
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.spacing {
            let screen = UIScreen.main.bounds
            
            if screen.width >= 414.0 {
                return value * 1.2
            } else if screen.width == 375.0 {
                return value * 1.1
            } else if screen.width <= 320.0 {
                return value * 0.95
            }
            
        }
        
        return value
    }
    
    func productTapped(productId: String) {
        self.delegate?.productTapped(productId: productId)
    }
    
    func openStore(storeId: String) {
        self.delegate?.openStore(storeId: storeId)
    }
    
    func askedForMore() {
        var ids = [String]()
        for s in stores! {
            ids.append(s.id!)
        }
        self.delegate?.moreStores(ids: ids, cell: self)
    }
}

