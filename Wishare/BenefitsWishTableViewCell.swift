//
//  BenefitsWishTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class BenefitsWishTableViewCell: UITableViewCell {
    
    @IBOutlet weak var levelsView: UIView!
    @IBOutlet weak var benefitsLevelLabel: UILabel!
    @IBOutlet weak var benefitsDescriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.levelsView.layer.cornerRadius = 22.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
