//
//  UserBirthdayRule.swift
//  Wishare
//
//  Created by Wishare iMac on 4/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import SwiftValidator

public class UserBirthdayRule : Rule {
    
    private var message : String
    
    
    public init(_ message : String = "Idade mínima de 16 anos") {
        self.message = message
    }
    
    
    public func validate(_ value: String) -> Bool {
        
        if value == "" {
            return true
        }

        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/Y"
        dateFormatter.locale = Locale.current
        
        let dateObj = dateFormatter.date(from: value)
        let dateNow = Date()
        
        
        let calender : Calendar = Calendar.current
        
        if let date = dateObj {
            
            let components : DateComponents = calender.dateComponents([.year], from: date, to: dateNow)
            
            if components.year! >= 16 {
                return true
            }
        }
        
        
        return false
    }
    
    public func errorMessage() -> String {
        return self.message
    }
    
}
