//
//  WishPointsViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import AZDialogView
import UICircularProgressRing
import Pager
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class WishPointsViewController: PagerController {
    
    @IBOutlet weak var progressBar: UICircularProgressRingView!
    @IBOutlet weak var nameLevelWishLabel: UILabel!
    @IBOutlet weak var wishPointsLabel: UILabel!
    @IBOutlet weak var currentPoints: UILabel!

    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openPopupWishPoints(notification:)), name: Notification.Name.init("openPopupWishPoints"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPopupWishPointsBenefit(notification:)), name: Notification.Name.init("openPopupWishPointsBenefit"), object: nil)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.wishPoints { (wishPoints : WishPoints?, error : Error?) in
            if error == nil {
                
                self.nameLevelWishLabel.text = wishPoints!.nameLevel
                self.wishPointsLabel.text = "\(wishPoints!.actualPoints!) / \(wishPoints!.totalPoints!)"
                self.currentPoints.text = wishPoints!.actualPoints

                self.progressBar.setProgress(value: CGFloat((100.0 * (wishPoints!.actualPoints! as NSString).floatValue) / (wishPoints!.totalPoints! as NSString).floatValue), animationDuration: 2)
                
                let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                
                let benefitsWish = storyboard.instantiateViewController(withIdentifier: "benefitsWishID") as! BenefitsWishViewController
                benefitsWish.wishPoints = wishPoints!
                
                let targetsWish = storyboard.instantiateViewController(withIdentifier: "targetsWishID") as! TargetsWishViewController
                targetsWish.wishPoints = wishPoints!
                
                let acquireWish = storyboard.instantiateViewController(withIdentifier: "acquireWishID") as! AcquireWishViewController
                acquireWish.wishPoints = wishPoints!
                
                self.progressBar.font = UIFont.systemFont(ofSize: 38, weight: UIFont.Weight.light)
                self.setupPager(tabNames: ["Benefícios", "Objetivos", "Conquistas"], tabControllers: [benefitsWish, targetsWish, acquireWish])
                self.customizeTabs()
                self.reloadData()
                
                RappleActivityIndicatorView.stopAnimation()
                
                self.progressBar.setProgress(value: CGFloat((100.0 * (wishPoints!.actualPoints! as NSString).floatValue) / (wishPoints!.totalPoints! as NSString).floatValue), animationDuration: 3, completion: nil)
                
            }
        }
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customizeTabs() {
        
        tabTopOffset = 155.0
        tabWidth = self.view.bounds.width / 3
        tabOffset = 0
//        fixMenu = true
        animation = .during
//        withBorder = false
        contentViewBackgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
    
    }

    func convertToGrayScale(image: CGImage) -> CGImage {
        let height = image.height
        let width = image.width
        let colorSpace = CGColorSpaceCreateDeviceGray();
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        let context = CGContext.init(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        context.draw(image, in: rect)
        return context.makeImage()!
    }
    
    @objc func openPopupWishPoints(notification : Notification) {
        
        let objective = notification.object as! Objective
        
        let dialogController = AZDialogViewController(title: objective.title, message: objective.description)
        
        dialogController.dismissDirection = .bottom
        dialogController.dismissWithOutsideTouch = true
        dialogController.showSeparator = true
        dialogController.separatorColor = UIColor.lightGray
        dialogController.allowDragGesture = false
        dialogController.cancelEnabled = false
        
        dialogController.imageHandler = { (imageView) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/wish_objective/thumb/")!
            let url = urlBase.appendingPathComponent("files/wish_objective/thumb/\(objective.image!)")

            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                if objective.isClosed! {
                    imageView.layer.cornerRadius = imageView.layer.bounds.width / 2
                    imageView.image = result.value
                } else {
                    imageView.layer.cornerRadius = imageView.layer.bounds.width / 2
                    imageView.image = UIImage(cgImage: self.convertToGrayScale(image: result.value!.cgImage!))
                }
            }
            
            imageView.contentMode = .scaleAspectFit
            return true
            
        }
        
        
        dialogController.rightToolStyle = { (button) in
            button.setImage(UIImage(named: "Delete")?.withRenderingMode(.alwaysOriginal), for: .normal)
            return true
        }
        
        dialogController.rightToolAction = { (button) in
            dialogController.dismiss()
        }
        
        dialogController.customViewSizeRatio = 0.3
        
        let container = dialogController.container
        
        let containerView : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        dialogController.container.addSubview(containerView)
        containerView.leftAnchor.constraint(equalTo: container.leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: container.rightAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        
        let progressBarViewGray : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.lightGray
            return view
        }()
        
        let progressBarViewYellow : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            return view
        }()
        
        let pointsLabel : UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont.boldSystemFont(ofSize: 24)
            label.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            label.textAlignment = .center
            return label
        }()
        
        containerView.addSubview(progressBarViewGray)
        containerView.addSubview(progressBarViewYellow)
        
        progressBarViewGray.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5).isActive = true
        progressBarViewGray.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 5).isActive = true
        progressBarViewGray.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -5).isActive = true
        progressBarViewGray.heightAnchor.constraint(equalToConstant: 6).isActive = true
        progressBarViewGray.layer.cornerRadius = 3

        let sizeQuantity = ((self.view.layer.bounds.width * 0.9) - 12) / CGFloat((objective.qtd! as NSString).floatValue)
        let sizeBar = sizeQuantity * CGFloat(objective.difference!)
        
        progressBarViewYellow.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 6).isActive = true
        progressBarViewYellow.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 6).isActive = true
        progressBarViewYellow.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: (sizeBar == 0.0) ? -6 : sizeBar).isActive = true
        progressBarViewYellow.heightAnchor.constraint(equalToConstant: 4).isActive = true
        progressBarViewYellow.layer.cornerRadius = 2
        
        
        containerView.addSubview(pointsLabel)
        
        pointsLabel.topAnchor.constraint(equalTo: progressBarViewGray.bottomAnchor, constant: 5).isActive = true
        pointsLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        pointsLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        pointsLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        
        pointsLabel.text = "+ \(objective.points!) wpts"
        
        dialogController.show(in: self)
        
    }

    @objc func openPopupWishPointsBenefit(notification : Notification) {
        
        let objective = notification.object as! Benefit
        
        let dialogController = AZDialogViewController(title: objective.message)
        
        dialogController.dismissDirection = .bottom
        dialogController.dismissWithOutsideTouch = true
        dialogController.showSeparator = true
        dialogController.separatorColor = UIColor.lightGray
        dialogController.allowDragGesture = false
        dialogController.cancelEnabled = false
    
        dialogController.rightToolStyle = { (button) in
            button.setImage(UIImage(named: "Delete")?.withRenderingMode(.alwaysOriginal), for: .normal)
            return true
        }
        
        dialogController.rightToolAction = { (button) in
            dialogController.dismiss()
        }
    
        if objective.code != nil {
            
            dialogController.customViewSizeRatio = 0.3
            
            let container = dialogController.container
            
            let containerView : UIView = {
                let view = UIView()
                view.translatesAutoresizingMaskIntoConstraints = false
                return view
            }()
            
            dialogController.container.addSubview(containerView)
            containerView.leftAnchor.constraint(equalTo: container.leftAnchor).isActive = true
            containerView.rightAnchor.constraint(equalTo: container.rightAnchor).isActive = true
            containerView.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            containerView.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
            
            let valueCode : UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = UIFont.boldSystemFont(ofSize: 26)
                label.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                label.textAlignment = .center
                return label
            }()
            
            let numberCode : UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = UIFont.systemFont(ofSize: 22)
                label.textColor = UIColor.black
                label.textAlignment = .center
                return label
            }()
            
            let infoCode : UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = UIFont.systemFont(ofSize: 12)
                label.textColor = UIColor.black
                label.textAlignment = .center
                return label
            }()
            
            containerView.addSubview(valueCode)
            containerView.addSubview(numberCode)
            containerView.addSubview(infoCode)
            
            valueCode.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
            valueCode.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
            valueCode.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
            valueCode.heightAnchor.constraint(equalToConstant: 20).isActive = true
            
            numberCode.topAnchor.constraint(equalTo: valueCode.bottomAnchor, constant: 15).isActive = true
            numberCode.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
            numberCode.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
            numberCode.heightAnchor.constraint(equalToConstant: 20).isActive = true
            
            infoCode.topAnchor.constraint(equalTo: numberCode.bottomAnchor, constant: 5).isActive = true
            infoCode.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
            infoCode.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
            infoCode.heightAnchor.constraint(equalToConstant: 14).isActive = true
            
            valueCode.text = (objective.value_from! as NSString).doubleValue.asLocaleCurrency
            numberCode.text = objective.code
            infoCode.text = "Copie o código acima"
            
        }
        
        dialogController.show(in: self)
        
    }
    
}

extension WishPointsViewController : PagerDataSource {
    
}
