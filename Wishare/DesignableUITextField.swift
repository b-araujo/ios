//
//  TextField-Extension.swift
//  Wishare
//
//  Created by Wishare iMac on 4/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class DesignableUITextField : UITextField {
    
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    @objc var backgroundColorTextField : UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
    
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
            imageView.image = image
            imageView.tintColor = color
            imageView.contentMode = .scaleAspectFit
            leftView = imageView
            self.backgroundColor = backgroundColorTextField
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
            self.backgroundColor = backgroundColorTextField
        }
        
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
    
    
}

