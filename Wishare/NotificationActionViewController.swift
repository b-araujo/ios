//
//  NotificationActionViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/31/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import SwiftyAttributes

class NotificationActionViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let refreshControl = UIRefreshControl()
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var notifications : [WSNotification] = []
    
    var loader : Loader!
    var manager : Manager!
    
    var pageIndex : Int = 1
    var isActive : Bool? = nil
    var stopSpinner : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isActive = true
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.refreshControl.tintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        self.refreshControl.addTarget(self, action: #selector(refreshShow), for: .valueChanged)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.refreshControl = refreshControl
        self.tableView.register(UINib(nibName: "NotificationFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "notificationFollowCell")
        self.tableView.register(UINib(nibName: "NotificationToFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "notificaitonToFollowCell")
        self.tableView.register(UINib(nibName: "NotificationInformationTableViewCell", bundle: nil), forCellReuseIdentifier: "notificationInformationCell")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.userNotificationAction { (notifications : [WSNotification], error : Error?) in
            if error == nil {
                self.notifications = notifications
                
                if self.notifications.count > 0 {
                    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                }
                
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        for n in self.notifications {
            n.seen = "1"
        }
        
        self.tableView.reloadData()
        
    }
    @objc func refreshShow() {
        
        WishareAPI.userNotificationAction { (notifications : [WSNotification], error : Error?) in
            if error == nil {
                self.stopSpinner = false
                self.notifications = notifications
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
        
    }

}

extension NotificationActionViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
            
            if self.stopSpinner == false {
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                spinner.color = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                spinner.frame = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 44.0)
                spinner.startAnimating()
                
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
                
                WishareAPI.userNotificationActionBy(page: "\(self.pageIndex)", lastId: self.notifications.last!.id!, { (notifications : [WSNotification], error : Error?) in
                    if error == nil {
                        
                        if notifications.count == 0 {
                            
                            self.tableView.tableFooterView?.isHidden = true
                            self.stopSpinner = true
                            
                        } else {
                            
                            self.pageIndex += 1
                            self.notifications.append(contentsOf: notifications)
                            self.tableView.reloadData()
                            
                        }
                        
                    }
                })
                
            } else {
                self.tableView.tableFooterView?.isHidden = true
            }
            
        }
        
    }
    
}

extension NotificationActionViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let actionID = self.notifications[indexPath.row].actionID {
            
            switch actionID {
                
            case "1", "2", "3", "4", "10":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificationInformationCell", for: indexPath) as! NotificationInformationTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                cell.backgroundColor = UIColor.white
                
                cell.userNameButton.setTitle("Você ", for: .normal)
                cell.descriptionLabel.attributedText = "Você ".withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + self.notifications[indexPath.row].actionMessage!.withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                var urlStringPicturePost : URL!
                
                if self.notifications[indexPath.row].picturePost == nil {
                    urlStringPicturePost = urlBase.appendingPathComponent("\(self.notifications[indexPath.row].productsPicture.first!["url"]!)")
                } else {
                    urlStringPicturePost = urlBase.appendingPathComponent("files/posts-image/\(self.notifications[indexPath.row].idPost!)/thumb/\(self.notifications[indexPath.row].picturePost!)")
                }
                
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                self.manager.loadImage(with: urlStringPicturePost, into: cell.previewPostImageView, handler: { (result, _) in
                    cell.previewPostImageView.image = result.value
                })
                
                return cell
                
            case "5":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificaitonToFollowCell", for: indexPath) as! NotificationToFollowTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                cell.backgroundColor = UIColor.white
                
                cell.heightButton.constant = 5
                cell.followButton.isHidden = true
                
                
                cell.userNameButton.setTitle("Você ", for: .normal)
                cell.descriptionLabel.attributedText = "Você ".withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + self.notifications[indexPath.row].actionMessage!.withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                return cell
                
            case "6":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificaitonToFollowCell", for: indexPath) as! NotificationToFollowTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                cell.backgroundColor = UIColor.white
                
                cell.userNameButton.setTitle("Você ", for: .normal)
                cell.descriptionLabel.attributedText = "Você ".withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + self.notifications[indexPath.row].actionMessage!.withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                cell.heightButton.constant = 5
                cell.followButton.isHidden = true
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                return cell
                
            case "7":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificationFollowCell", for: indexPath) as! NotificationFollowTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                cell.backgroundColor = UIColor.white
                
                cell.userNameButton.setTitle("Você ", for: .normal)
                cell.descriptionLabel.attributedText = "Você ".withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + self.notifications[indexPath.row].actionMessage!.withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                return cell
                
            case "11":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "notificationInformationCell", for: indexPath) as! NotificationInformationTableViewCell
                
                cell.tableView = tableView
                cell.indexPath = indexPath
                cell.delegate = self
                
                cell.store = self.notifications[indexPath.row].store
                    
                cell.backgroundColor = UIColor.white
                
                cell.userNameButton.setTitle("Você ", for: .normal)
                cell.descriptionLabel.attributedText = "Você ".withTextColor(UIColor.white).withFont(.boldSystemFont(ofSize: 12)) + self.notifications[indexPath.row].actionMessage!.withFont(.systemFont(ofSize: 12, weight: UIFont.Weight.thin))
                cell.publishedDateLabel.text = self.notifications[indexPath.row].date!.convertToDate().timeAgoDisplay()
                
                
                let urlStringUser : URL = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.notifications[indexPath.row].user!.picture!)")
                
                var urlStringPicturePost : URL!

                if let storeLogo = self.notifications[indexPath.row].store?.picture {
                    urlStringPicturePost = urlBase.appendingPathComponent("files/store-logo/thumb/\(storeLogo)")
                }

                
                
                self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                    cell.userImageView.image = result.value?.circleMask
                })
                
                self.manager.loadImage(with: urlStringPicturePost, into: cell.previewPostImageView, handler: { (result, _) in
                    cell.previewPostImageView.image = result.value?.circleMask
                })
                
                
                return cell
                
            default:
                print("NENHUM")
            }
            
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
        
    }
    
}

extension NotificationActionViewController : WishareNotificationDelegate {
    
    
    func userProfile(_ tableView: UITableView, _ indexPath: IndexPath, _ gesture: UITapGestureRecognizer) {
        
    }
    
    func openUserProfile(_ tableView: UITableView, _ indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("selectedNotificationGeneralUserProfile"), object: self.notifications[indexPath.row])
    }
    
    func openStore(_ tableView: UITableView, _ indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("selectedNotificationGeneralStore"), object: self.notifications[indexPath.row])
    }
    
    func openPostInformation(_ tableView: UITableView, _ indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("selectedNotificationGeneralPostInformation"), object: self.notifications[indexPath.row])
    }
    
    func acceptOrDeclineInvite(_ tableView: UITableView, _ indexPath: IndexPath, _ sender: UIButton) {
        
        if sender.tag == 1 {
            
            self.notifications[indexPath.row].actionID = "5"
            self.notifications[indexPath.row].actionMessage = "Começou a seguir você"
            self.notifications[indexPath.row].user!.following = true
            self.tableView.reloadData()
            WishareAPI.acceptInvite(idNotification: self.notifications[indexPath.row].id!, { ( _ ) in })
            
        } else {
            
            self.notifications.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            WishareAPI.declineInvite(idNotification: self.notifications[indexPath.row].id!, { ( _ ) in })
            
        }
        
    }
    
    func followOrShare(_ tableView: UITableView, _ indexPath: IndexPath, _ sender: UIButton) {
        if self.notifications[indexPath.row].store != nil {
            NotificationCenter.default.post(name: NSNotification.Name.init("notificationSharePurchase"), object: [self.notifications[indexPath.row].products, self.notifications[indexPath.row].store!])
        } else {
            
            if sender.title(for: .normal) == "Seguir" {
                
                WishareAPI.followUser(self.notifications[indexPath.row].user!.id!, isFollow: true, { (error : Error?) in
                    if error == nil {
                        
                        self.notifications[indexPath.row].user!.following = true
                        sender.backgroundColor = UIColor.white
                        sender.setTitle("Seguindo", for: .normal)
                        sender.setTitleColor(UIColor.black, for: .normal)
                        sender.layer.borderWidth = 2.0
                        sender.layer.borderColor = UIColor.black.cgColor
                        
                    }
                })
                
            } else {
                
                WishareAPI.followUser(self.notifications[indexPath.row].user!.id!, isFollow: false, { (error : Error?) in
                    if error == nil {
                        
                        self.notifications[indexPath.row].user!.following = false
                        sender.backgroundColor = UIColor.black
                        sender.setTitle("Seguir", for: .normal)
                        sender.setTitleColor(UIColor.white, for: .normal)
                        sender.layer.borderWidth = 0.0
                        sender.layer.borderColor = UIColor.clear.cgColor
                        
                    }
                })
                
            }
            
        }
    }
    
}
