//
//  CardOrderViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SkyFloatingLabelTextField

class CardOrderViewController: UIViewController {

    @IBOutlet weak var flagCardLabel: UILabel!
    @IBOutlet weak var numberCardLabel: UILabel!
    @IBOutlet weak var validCardLabel: UILabel!
    @IBOutlet weak var cvvCardTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var numberParcelTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var numberParcelLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var continuosButton : UIButton!
    @IBOutlet weak var couponCodeTextField: SkyFloatingLabelTextField!
    
    var totalPrice : Double = 0.0
    var quantityParcel : Int = 0
    
    var store : Store!
    
    var bottomConstraint : NSLayoutConstraint?
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    var myCardSelected : [[String : String]] = []
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomConstraint = NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillHide, object: nil)
        
        self.continuosButton.layer.cornerRadius = 5.0
        self.continuosButton.layer.masksToBounds = false
        self.continuosButton.layer.shadowColor = UIColor.black.cgColor
        self.continuosButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.continuosButton.layer.shadowRadius = 2.0
        self.continuosButton.layer.shadowOpacity = 0.24
        
        self.numberParcelTextField.tag = 10
        self.numberParcelTextField.delegate = self
        self.cvvCardTextField.delegate = self
        
        self.navigationController?.navigationBar.backItem?.title = "    "
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getCards { (cards : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                if let card : [String : AnyObject] = cards[0]["UsersCard"] as? [String : AnyObject] {
                    
                    self.flagCardLabel.text = (card["flag"] as? String)?.uppercased()
                    self.numberCardLabel.text = "XXXX - XXXX - XXXX - \((card["number"] as! String))"
                    self.validCardLabel.text = "Validade: \(card["valid"] as! String)"
                    
                    self.store.order.id_card = card["id"] as? String
                    self.store.order.flag_card = (card["flag"] as? String)?.uppercased()
                    
                    RappleActivityIndicatorView.stopAnimation()
                    
                }
                
            }
        }
        
        
        for product in self.store.products {
            
            totalPrice += (product.select_quatity! as NSString).doubleValue * ((product.promo_price! == "") ? (product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)
        }
        
        self.totalPriceLabel.text = "\(self.totalPrice.asLocaleCurrency)"
        
        parcelCalculator()
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if self.myCardSelected.count > 0 {
            
            self.flagCardLabel.text = self.myCardSelected[0]["flag"]?.uppercased()
            self.numberCardLabel.text = "XXXX - XXXX - XXXX - \((self.myCardSelected[0]["number"]! as NSString).substring(from: 12))"
            self.validCardLabel.text = "Validade: \(self.myCardSelected[0]["valid"]!)"
            
            self.store.order.id_card = self.myCardSelected[0]["id"]!
            self.store.order.flag_card = self.myCardSelected[0]["flag"]?.uppercased()
            
        }
        
    }
    
    fileprivate func parcelCalculator() {
        if self.store.advance_commission! == "0" {
            
            let parcelMinimal : Double = 50.0
            var parcelIsValidate : Bool = false
            
            for i in (1...6).reversed() {
                
                var parcel : Double = 0.0
                parcel = totalPrice / Double(i)
                
                if parcel >= parcelMinimal {
                    self.quantityParcel = i
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
                
            }
            
            if parcelIsValidate == false {
                self.quantityParcel = 1
            }
            
        } else {
            
            let parcelMinimal : Double = 100.0
            var parcelIsValidate : Bool = false
            
            for i in (1...6).reversed() {
                
                var parcel : Double = 0.0
                parcel = totalPrice / Double(i)
                
                if parcel >= parcelMinimal {
                    self.quantityParcel = i
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
                
            }
            
            if parcelIsValidate == false {
                self.quantityParcel = 1
            }
            
        }
        
        
        
        let frete : Double = (self.store.order.freight_value!.components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue
        let totalFinal = totalPrice + frete
        
        self.numberParcelTextField.text = "1x de \((totalFinal / 1.0).asLocaleCurrency)"
        self.totalPriceLabel.text = "\(totalFinal.asLocaleCurrency)"
        self.numberParcelLabel.text = "1x de \((totalFinal / 1.0).asLocaleCurrency)"
        self.store.order.parcel_number = "1"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func alterMyCard(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Setting", bundle: nil)
        let myCardViewController = storyboard.instantiateViewController(withIdentifier: "myCardID") as! MyCardViewController
        myCardViewController.selectedItem = true
        
        self.navigationController?.pushViewController(myCardViewController, animated: true)
        
    }

    @IBAction func applyCouponCode(_ sender: UIButton) {
        WishareAPI.applyCoupon(coupon: self.couponCodeTextField.text, addressId: store.order.id_address, idStore: store.id, freightValue: store.order.freight_value, service: store.order.id_address, products: store.order.products) { (valid: Bool, products: [ProductUpdate], total: String, error: Error?) in
            if error == nil {
                if valid {
                    if total.doubleValue > 0 {
                        self.totalPrice = total.doubleValue
                        
                        let freight = self.store.order.freight_value?.doubleValue
                        
                        self.totalPrice += freight ?? 0
                        self.store.order.total = total.doubleValue
                        self.totalPriceLabel.text = "\(self.totalPrice.asLocaleCurrency)"
                        self.store.order.total = total.doubleValue
                        
                        for prod in products {
                            if let index = self.store.order.products.index(where: {$0.id == prod.id}) {
                                self.store.order.products[index].price = prod.price
                            }
                        }
                        self.store.order.cupon_code = self.couponCodeTextField.text ?? ""
                        
                        self.parcelCalculator()
                        
                        let alert = UIAlertController(title: "A aplicação do cupom implica na alteração do valor que pode afetar a sua forma de pagamento, número de parcelas e/ou valor do frete.", message: nil, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                        
                    }
                    
                    
                } else {
                    self.couponCodeTextField.errorMessage = "Cupom inválido"
                }
            } else {
                self.couponCodeTextField.errorMessage = "Cupom inválido"
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "finishOrderSegue" {
            let finishOrderViewController = segue.destination as! FinishOrderViewController
            finishOrderViewController.store = sender as! Store
        }
    }

    
    @IBAction func continuosPage ( _ sender : UIButton) {
        
        if (self.cvvCardTextField.text?.isEmpty)! {
            
            self.cvvCardTextField.errorMessage = "Código de Segurança"
            
            let alertController = UIAlertController(title: nil, message: "Informe o CVV", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if (self.numberParcelTextField.text?.isEmpty)! {
            
            
            let alertController = UIAlertController(title: nil, message: "Selecione a quantidade de parcelas", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            self.store.order.cvv = self.cvvCardTextField.text
            self.performSegue(withIdentifier: "finishOrderSegue", sender: self.store)
            
        }
        
    }
    
    @objc func handleKeyBoardNotification(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                
                //let indexPath = IndexPath(item: (self.coments.count != 0) ? self.coments.count - 1 : 0, section: 0)
                //self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
            })
            
        }
        
    }
    
}

extension CardOrderViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 10 {
        
            self.becomeFirstResponder()
        
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
            let frete : Double = (self.store.order.freight_value!.components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue
            let totalFinal = totalPrice + frete

            for i in 1...self.quantityParcel {
            
                let parcelAction = UIAlertAction(title: "\(i)x de \((totalFinal / Double(i)).asLocaleCurrency)", style: .default, handler: { ( _ ) in
                
                    self.numberParcelTextField.text = "\(i)x de \((totalFinal / Double(i)).asLocaleCurrency)"
                    self.numberParcelLabel.text = "\(i)x de \((totalFinal / Double(i)).asLocaleCurrency)"
                    self.store.order.parcel_number = "\(i)"
                })
            
                alertController.addAction(parcelAction)
            
            }
        
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        
            self.present(alertController, animated: true, completion: nil)
        
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag != 10 {
            
            print("SAIU DO CVV")
            
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag != 10 {
            self.cvvCardTextField.errorMessage = nil
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
}
