//
//  RoundButton.swift
//  Wishare
//
//  Created by Wishare on 19/03/17.
//  Copyright © 2017 Wishare. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class RoundButton: UIButton {

    @IBInspectable var cornerRadius : CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderWidth : CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }


}
