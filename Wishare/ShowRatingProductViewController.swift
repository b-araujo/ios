//
//  ShowRatingProductViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/20/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import FloatRatingView
import RappleProgressHUD

class ShowRatingProductViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var rate : [String : Int] = ["5" : 0, "4" : 0, "3" : 0, "2" : 0, "1" : 0]
    var users : [[String : AnyObject]] = []
    var tot : Int = 0
    var myUserId : String = "0"
    var pId : String!
    var myRatingProduct : [String : AnyObject] = [:]
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "PreviewRatingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ratingViewCell")
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.ratingProduct(idProduct: self.pId) { (rate : [String : Int], tot : Int, users : [[String : AnyObject]], error : Error?) in
            
            if error == nil {
                
                WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                    
                    if error == nil {
                        
                        self.users = users
                        self.rate = rate
                        self.tot = tot
                        self.myUserId = user!.id!
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                }
                
            }
            
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.ratingProduct(idProduct: self.pId) { (rate : [String : Int], tot : Int, users : [[String : AnyObject]], error : Error?) in
            
            if error == nil {
                
                WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                    
                    if error == nil {
                        
                        self.users = users
                        self.rate = rate
                        self.tot = tot
                        self.myUserId = user!.id!
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                }
                
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ratingOpnionSegue" {
            
            let ratingProductViewController = segue.destination as! RatingProductViewController
            let senderValue = sender as! [AnyObject]
            
            
            ratingProductViewController.idProduct = senderValue[0] as! String
            ratingProductViewController.isEditingRating = senderValue[1] as! Bool
            ratingProductViewController.myRatingProduct = senderValue[2] as! [String : AnyObject]
            
        }
        
    }
    
    func customNavigationBar () {
        let nav = (self.navigationController?.navigationBar)!
        let transparencia = UIImage(named: "transparente")
        nav.setBackgroundImage(transparencia, for: .default)
        nav.shadowImage = transparencia
        nav.backgroundColor = UIColor.clear
    }

}

extension ShowRatingProductViewController : UICollectionViewDelegate {
    
}

extension ShowRatingProductViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ratingViewCell", for: indexPath) as! PreviewRatingCollectionViewCell
        
        cell.delegate = self
        
        if self.tot == 0 {
            cell.totalEvaluationLabel.text = "Nenhuma Avaliação"
        } else if self.tot == 1 {
            cell.totalEvaluationLabel.text = "\(self.tot) avaliação"
        } else {
            cell.totalEvaluationLabel.text = "\(self.tot) avaliações"
        }
        
        cell.fiveRateLabel.text = (self.rate["5"]! >= 0 && self.rate["5"]! <= 9) ? "(0\(self.rate["5"]!))" : "(\(self.rate["5"]!))"
        cell.fourRateLabel.text = (self.rate["4"]! >= 0 && self.rate["4"]! <= 9) ? "(0\(self.rate["4"]!))" : "(\(self.rate["4"]!))"
        cell.threeRateLabel.text = (self.rate["3"]! >= 0 && self.rate["3"]! <= 9) ? "(0\(self.rate["3"]!))" : "(\(self.rate["3"]!))"
        cell.twoRateLabel.text = (self.rate["2"]! >= 0 && self.rate["2"]! <= 9) ? "(0\(self.rate["2"]!))" : "(\(self.rate["2"]!))"
        cell.oneRateLabel.text = (self.rate["1"]! >= 0 && self.rate["1"]! <= 9) ? "(0\(self.rate["1"]!))" : "(\(self.rate["1"]!))"
        
        var ids : [String] = []
        
        for userId in self.users {
            ids.append(userId["user_id"] as! String)
        }
        
        if ids.contains(myUserId) {
            // EDITANDO
            print("EDITANDO")
            self.myRatingProduct = self.users[indexPath.row]
            cell.ratingProductButton.setTitle("EDITAR SUA AVALIAÇÃO", for: .normal)
        } else {
            // NOVO
            print("NOVO")
            self.myRatingProduct = [:]
            cell.ratingProductButton.setTitle("AVALIAR ESSE PRODUTO", for: .normal)
        }

        var mediumRating : CGFloat = 0.0
        
        mediumRating += CGFloat(self.rate["5"]!) * 5.0
        mediumRating += CGFloat(self.rate["4"]!) * 4.0
        mediumRating += CGFloat(self.rate["3"]!) * 3.0
        mediumRating += CGFloat(self.rate["2"]!) * 2.0
        mediumRating += CGFloat(self.rate["1"]!) * 1.0

        let baseView = cell.fiveBaseRateView.bounds.size.width / CGFloat(tot)
        
        let fiveRateView : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            return view
        }()
        
        let fourRateView : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            return view
        }()
        
        let threeRateView : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            return view
        }()
        
        let twoRateView : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            return view
        }()
        
        let oneRateView : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            return view
        }()

        
        let subViewFive = cell.fiveBaseRateView.subviews
        let subViewFour = cell.fourBaseRateView.subviews
        let subViewThree = cell.threeBaseRateView.subviews
        let subViewTwo = cell.twoBaseRateView.subviews
        let subViewOne = cell.oneBaseRateView.subviews
        
        for subview in subViewFive {
            subview.removeFromSuperview()
        }
        
        for subview in subViewFour {
            subview.removeFromSuperview()
        }
        
        for subview in subViewThree {
            subview.removeFromSuperview()
        }
        
        for subview in subViewTwo {
            subview.removeFromSuperview()
        }
        
        for subview in subViewOne {
            subview.removeFromSuperview()
        }
        
        
        cell.fiveBaseRateView.addSubview(fiveRateView)
        cell.fourBaseRateView.addSubview(fourRateView)
        cell.threeBaseRateView.addSubview(threeRateView)
        cell.twoBaseRateView.addSubview(twoRateView)
        cell.oneBaseRateView.addSubview(oneRateView)
        
        fiveRateView.topAnchor.constraint(equalTo: cell.fiveBaseRateView.topAnchor).isActive = true
        fiveRateView.leftAnchor.constraint(equalTo: cell.fiveBaseRateView.leftAnchor).isActive = true
        fiveRateView.bottomAnchor.constraint(equalTo: cell.fiveBaseRateView.bottomAnchor).isActive = true
        fiveRateView.widthAnchor.constraint(equalToConstant: ((self.rate["5"] == 0) ? 0 : baseView * CGFloat(self.rate["5"]!))).isActive = true

        fourRateView.topAnchor.constraint(equalTo: cell.fourBaseRateView.topAnchor).isActive = true
        fourRateView.leftAnchor.constraint(equalTo: cell.fourBaseRateView.leftAnchor).isActive = true
        fourRateView.bottomAnchor.constraint(equalTo: cell.fourBaseRateView.bottomAnchor).isActive = true
        fourRateView.widthAnchor.constraint(equalToConstant: ((self.rate["4"] == 0) ? 0 : baseView * CGFloat(self.rate["4"]!))).isActive = true
        
        threeRateView.topAnchor.constraint(equalTo: cell.threeBaseRateView.topAnchor).isActive = true
        threeRateView.leftAnchor.constraint(equalTo: cell.threeBaseRateView.leftAnchor).isActive = true
        threeRateView.bottomAnchor.constraint(equalTo: cell.threeBaseRateView.bottomAnchor).isActive = true
        threeRateView.widthAnchor.constraint(equalToConstant: ((self.rate["3"] == 0) ? 0 : baseView * CGFloat(self.rate["3"]!))).isActive = true

        twoRateView.topAnchor.constraint(equalTo: cell.twoBaseRateView.topAnchor).isActive = true
        twoRateView.leftAnchor.constraint(equalTo: cell.twoBaseRateView.leftAnchor).isActive = true
        twoRateView.bottomAnchor.constraint(equalTo: cell.twoBaseRateView.bottomAnchor).isActive = true
        twoRateView.widthAnchor.constraint(equalToConstant: ((self.rate["2"] == 0) ? 0 : baseView * CGFloat(self.rate["2"]!))).isActive = true

        oneRateView.topAnchor.constraint(equalTo: cell.oneBaseRateView.topAnchor).isActive = true
        oneRateView.leftAnchor.constraint(equalTo: cell.oneBaseRateView.leftAnchor).isActive = true
        oneRateView.bottomAnchor.constraint(equalTo: cell.oneBaseRateView.bottomAnchor).isActive = true
        oneRateView.widthAnchor.constraint(equalToConstant: ((self.rate["1"] == 0) ? 0 : baseView * CGFloat(self.rate["1"]!))).isActive = true
        
        cell.mediumRateLabel.text = (self.tot == 0) ? "0.0" : "\(mediumRating / CGFloat(tot))"
        cell.mediumRateView.rating = Float(mediumRating / CGFloat(tot))

        
        
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.estimatedRowHeight = 104
        cell.tableView.rowHeight = UITableViewAutomaticDimension
        cell.tableView.register(UINib(nibName: "RatingTableViewCell", bundle: nil), forCellReuseIdentifier: "ratingCommentCell")
        cell.tableView.reloadData()
        
        
        return cell
        
    }
    
}

extension ShowRatingProductViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width, height: self.view.bounds.height)
    }
    
}

extension ShowRatingProductViewController : UITableViewDelegate {
    
    
    
}

extension ShowRatingProductViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ratingCommentCell", for: indexPath) as! RatingTableViewCell
        
        cell.titleCommentRatingLabel.text = self.users[indexPath.row]["title"] as? String
        cell.dateCommentRatingLabel.text = (self.users[indexPath.row]["date"] as? String)?.convertToDate().timeAgoDisplay()
        cell.byCommentRatingLabel.text = (self.users[indexPath.row]["anonymous"] as! String == "0") ? "por \(self.users[indexPath.row]["user"] as! String)" : "por Anônimo"
        cell.commentRatingLabel.text = self.users[indexPath.row]["description"] as? String
        cell.ratingCommentView.rating = Float((self.users[indexPath.row]["rate"] as! String))!
        
        return cell
        
    }
    
}

extension ShowRatingProductViewController : PreviewRatingDelegate {
    
    func ratingProduct(_ sender: UIButton) {
    
        var ids : [String] = []
                
        for userId in self.users {
            ids.append(userId["user_id"] as! String)
        }
                
        if ids.contains(myUserId) {
            // EDITANDO
            print("EDITANDO")
            self.performSegue(withIdentifier: "ratingOpnionSegue", sender: [self.pId, true, self.myRatingProduct])
            
        } else {
            // NOVO
            print("NOVO")
            self.performSegue(withIdentifier: "ratingOpnionSegue", sender: [self.pId, false, self.myRatingProduct])
        
        }
        
    }
    
    func orderBy(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let moreStarAction = UIAlertAction(title: "Mais Estrelas", style: .default) { (action : UIAlertAction) in
            
            let users = self.users
            var orderUsers : [[String : AnyObject]] = []
            
            for i in (1...5).reversed() {
                
                for user in users {
                    
                    if (user["rate"] as! String) == "\(i)" {
                        orderUsers.append(user)
                    }
                    
                }
                
            }
            
            self.users = orderUsers
            self.collectionView.reloadData()
            
        }
        
        let lessStarAction = UIAlertAction(title: "Menos Estrelas", style: .default) { (action : UIAlertAction) in
            
            let users = self.users
            var orderUsers : [[String : AnyObject]] = []
            
            for i in 1...5 {
                
                for user in users {
                    
                    if (user["rate"] as! String) == "\(i)" {
                        orderUsers.append(user)
                    }
                    
                }
                
            }
            
            self.users = orderUsers
            self.collectionView.reloadData()
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(moreStarAction)
        alertController.addAction(lessStarAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}
