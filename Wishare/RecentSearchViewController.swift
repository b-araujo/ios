//
//  RecentSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD

class RecentSearchViewController: UIViewController {

    var category : Category!
    var store : Store!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var clearRecentButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var words : [String] = []
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.separatorStyle = .none
        
        self.searchBar.delegate = self
        
        
        WishareCoreData.recentSearch(text: nil, method: .findAll) { ( words : [String], error : Error?) in
            
            if error == nil {
                
                self.words = words
                self.words.reverse()
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
            }
            
        }
        
        
        self.navigationItem.title = self.category.name
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.recentSearch(text: nil, method: .findAll) { ( words : [String], error : Error?) in
            if error == nil {
                self.words = words
                self.words.reverse()
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
            }
        }
        
        UserDefaults.standard.set("", forKey: "categoryMother")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "productsSubCategoriesStoreSegue" {
            
            let senderObject : [AnyObject] = sender as! [AnyObject]
            
            let productsCategoriesStoreViewController : ProductsCategoriesStoreViewController = segue.destination as! ProductsCategoriesStoreViewController
            productsCategoriesStoreViewController.category = senderObject[0] as? Category
            productsCategoriesStoreViewController.store = senderObject[1] as? Store
            productsCategoriesStoreViewController.origin = true
            productsCategoriesStoreViewController.searchWord = senderObject[2] as? String
            
        }
        
    }
    
    @IBAction func clearRecentSearch(_ sender: UIButton) {
        
        WishareCoreData.recentSearch(text: nil, method: .deleteAll) { (words : [String], error : Error?) in
            if error == nil {
                self.words = []
                self.tableView.reloadData()
            }
        }
        
    }

}

extension RecentSearchViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "productsSubCategoriesStoreSegue", sender: [self.category, self.store, self.words[indexPath.row]])
    }
    
}

extension RecentSearchViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.words.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.isHighlighted = false
        cell.isUserInteractionEnabled = true
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = self.words[indexPath.row]
        cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        
        return cell
    }
    
}

extension RecentSearchViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        
        if self.words.contains(searchBar.text!) {
            print("CONTEM")
            self.performSegue(withIdentifier: "productsSubCategoriesStoreSegue", sender: [self.category, self.store, searchBar.text!])
            
        } else {
            print("NAO CONTEM")
            WishareCoreData.recentSearch(text: searchBar.text, method: .save) { (words : [String], error : Error?) in
                if error == nil {
                    self.performSegue(withIdentifier: "productsSubCategoriesStoreSegue", sender: [self.category, self.store, searchBar.text!])
                }
            }
            
        }
        

        
    }
    
}
