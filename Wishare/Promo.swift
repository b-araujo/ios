//
//  Promo.swift
//  Wishare
//
//  Created by Wishare iMac on 6/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Promo {
    
    var id : String?
    var name : String?
    var imageUrl : String?
    
    init (id : String?, name : String?, imageUrl : String?) {
        
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
        
    }
    
}
