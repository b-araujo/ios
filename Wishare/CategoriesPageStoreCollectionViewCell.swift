//
//  CategoriesPageStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/5/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class CategoriesPageStoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moreCategoriesButoon: UIButton!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var barCategoryView: UIView!
    
    var delegate : CategoriesPageStoreCollectionViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewCategory.layer.cornerRadius = 5.0
        self.barCategoryView.layer.cornerRadius = 5.0
        self.viewCategory.layer.masksToBounds = false
        self.viewCategory.layer.shadowColor = UIColor.black.cgColor
        self.viewCategory.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.viewCategory.layer.shadowRadius = 2.0
        self.viewCategory.layer.shadowOpacity = 0.24
        
    }
    
    
    @IBAction func pressMoreCategories(_ sender: UIButton) {
        self.delegate.moreCategories(cell: self)
    }

}

protocol CategoriesPageStoreCollectionViewCellDelegate {
    
    func moreCategories(cell: CategoriesPageStoreCollectionViewCell)
    
}
