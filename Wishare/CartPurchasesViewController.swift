//
//  CartPurchasesViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/23/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Savory
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class CartPurchasesViewController: UIViewController {
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var continuosPurchase: UIButton!
    
    var savoryView: SavoryView!
    var stores : [Store] = []
    
    var loader : Loader!
    var manager : Manager!
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    @IBOutlet weak var quantityViewBottomConstraint : NSLayoutConstraint!
    
    var isQuantityViewOpen : Bool = false
    
    var quantityView : UIView = {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    
    var quantityInformationView : UIView = {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1)
        return view
    }()
    
    
    var quantityInformationLabel : UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        return label
    }()
    
    var okQuantityButton : UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        return label
    }()
    
    
    var quantityPickerView : UIPickerView = {
        let pickerView = UIPickerView(frame: CGRect.zero)
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        return pickerView
    }()
    
    var quantitySelected = 1
    var quantity : Int = 0
    var storeIndex : Int = 0
    var productIndex : Int = 0
    
    fileprivate func loadproducts() {
        // ==========================================================
        // ============ CORRECOES BUSCA DOS PRODUTOS ================
        // ==========================================================
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)

        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                
                if stores.count > 0 {
                    
                    var actualStoreCount : Int = 0
                    
                    for storeCore in stores {
                        
                        WishareAPI.storeDetail(storeCore.id_store!, { (store : Store?, error : Error?) in
                            if error == nil {
                                
                                actualStoreCount += 1
                                
                                let actualStore : Store = store!
                                var productsActualStore : [String] = []
                                var productsActualStoreCount : Int = 0
                                
                                for prod in products {
                                    
                                    if prod.id_store! == actualStore.id! {
                                        productsActualStore.append(prod.id_product!)
                                    }
                                    
                                }
                                
                                for idProduct in productsActualStore {
                                    
                                    WishareAPI.productDetail(idProduct: idProduct, { ( product : Product, _, error : Error?) in
                                        if error == nil {
                                            
                                            for prod in products {
                                                
                                                if prod.id_product! == idProduct {
                                                    
                                                    product.size = prod.size_product
                                                    product.select_quatity = (product.quantity! == "0") ? "0" : prod.quantity_product
                                                    actualStore.products.append(product)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            // ===================================================
                                            // =============== PARANDO CARREGAMENTO ==============
                                            // ===================================================
                                            
                                            productsActualStoreCount += 1
                                            
                                            if productsActualStore.count == productsActualStoreCount {
                                                
                                                self.stores.append(actualStore)
                                                
                                                if stores.count == actualStoreCount {
                                                    
                                                    var stateProvider : [SavoryPanelState] = [SavoryPanelState]()
                                                    for _ in self.stores {
                                                        stateProvider.append(SavoryPanelState.collapsed)
                                                    }
                                                    
                                                    self.savoryView.stateProvider = SimpleStateProvider(stateProvider)
                                                    self.savoryView.reloadData()
                                                    
                                                    Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { ( _ ) in
                                                        RappleActivityIndicatorView.stopAnimation()
                                                    })
                                                    
                                                }
                                                
                                            }
                                            
                                            // ===================================================
                                            // ===================================================
                                            // ===================================================
                                            
                                            
                                        }
                                    })
                                    
                                    
                                }
                                
                            }
                            else {
                                RappleActivityIndicatorView.stopAnimation()
                            }
                        })
                        
                    }
                } else {
                    
                    // EXIBE A MENSAGEM QUANDO NAO EXISTE PRODUTOS NO CARRINHO!
                    self.savoryView.isHidden = true
                    RappleActivityIndicatorView.stopAnimation()
                    
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.continuosPurchase.layer.cornerRadius = 5.0
        self.continuosPurchase.layer.masksToBounds = false
        self.continuosPurchase.layer.shadowColor = UIColor.black.cgColor
        self.continuosPurchase.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.continuosPurchase.layer.shadowRadius = 2.0
        self.continuosPurchase.layer.shadowOpacity = 0.24
        
        self.navigationController?.navigationBar.backItem?.title = "    "
        
        self.quantityInformationLabel.text = "Selecione a Quantidade"
        self.okQuantityButton.text = "Ok"
        self.quantityPickerView.delegate = self
        self.quantityPickerView.dataSource = self
        
        let tapGestureRecognizerLabel = UITapGestureRecognizer(target: self, action: #selector(selecQuantityLabel))
        self.okQuantityButton.addGestureRecognizer(tapGestureRecognizerLabel)
        
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        savoryView = SavoryView(frame: CGRect.zero)
        savoryView.translatesAutoresizingMaskIntoConstraints = false
        savoryView.separatorStyle = .none
        savoryView.tag = 2

        savoryView.stateProvider = SimpleStateProvider([])
        
        savoryView.headerIdentifier = "headerShoppingCartCell"
        savoryView.bodyIdentifier = "bodyShoppingCartCell"
        
        savoryView.register(UINib(nibName: "HeaderShoppingCartTableViewCell", bundle: nil), forCellReuseIdentifier: "headerShoppingCartCell")
        savoryView.register(UINib(nibName: "BodyShoppingCartTableViewCell", bundle: nil), forCellReuseIdentifier: "bodyShoppingCartCell")
        
        savoryView.savoryDelegate = self
        
        
        view.addSubview(savoryView)
        
        self.savoryView.topAnchor.constraint(equalTo: self.messageView.bottomAnchor).isActive = true
        self.savoryView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.savoryView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.savoryView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        
        
        loadproducts()
        
        // ==========================================================
        // ========================== FIM ===========================
        // ==========================================================
        
        
        
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
        
        self.view.addSubview(self.quantityView)
        
        self.quantityView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.quantityView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.quantityView.heightAnchor.constraint(equalToConstant: (self.view.bounds.height / 2) - 44).isActive = true
        self.quantityViewBottomConstraint = self.quantityView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: +((self.view.bounds.height / 2) - 44))
        self.quantityViewBottomConstraint.isActive = true
        
        self.quantityView.addSubview(self.quantityInformationView)
        
        self.quantityInformationView.topAnchor.constraint(equalTo: self.quantityView.topAnchor).isActive = true
        self.quantityInformationView.leftAnchor.constraint(equalTo: self.quantityView.leftAnchor).isActive = true
        self.quantityInformationView.rightAnchor.constraint(equalTo: self.quantityView.rightAnchor).isActive = true
        self.quantityInformationView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.quantityInformationView.addSubview(self.quantityInformationLabel)
        self.quantityInformationView.addSubview(okQuantityButton)
        
        self.okQuantityButton.rightAnchor.constraint(equalTo: self.quantityInformationView.rightAnchor).isActive = true
        self.okQuantityButton.topAnchor.constraint(equalTo: self.quantityInformationView.topAnchor).isActive = true
        self.okQuantityButton.bottomAnchor.constraint(equalTo: self.quantityInformationView.bottomAnchor).isActive = true
        self.okQuantityButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.quantityInformationLabel.topAnchor.constraint(equalTo: self.quantityInformationView.topAnchor).isActive = true
        self.quantityInformationLabel.bottomAnchor.constraint(equalTo: self.quantityInformationView.bottomAnchor).isActive = true
        self.quantityInformationLabel.rightAnchor.constraint(equalTo: self.okQuantityButton.leftAnchor).isActive = true
        self.quantityInformationLabel.leftAnchor.constraint(equalTo: self.quantityInformationView.leftAnchor, constant: 40).isActive = true
        
        self.quantityView.addSubview(self.quantityPickerView)
        
        self.quantityPickerView.topAnchor.constraint(equalTo: self.quantityInformationView.bottomAnchor).isActive = true
        self.quantityPickerView.leftAnchor.constraint(equalTo: self.quantityView.leftAnchor).isActive = true
        self.quantityPickerView.rightAnchor.constraint(equalTo: self.quantityView.rightAnchor).isActive = true
        self.quantityPickerView.bottomAnchor.constraint(equalTo: self.quantityView.bottomAnchor).isActive = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func selecQuantityLabel() {

        WishareCoreData.quantityUpdateShoppingCart(quantity: "\(self.quantitySelected)", pId: self.stores[self.storeIndex].products[self.productIndex].id!) { (error : Error?) in
            
            if error == nil {
                
                UIView.animate(withDuration: 0.4, animations: {
                    self.quantityViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 44)
                    self.view.layoutIfNeeded()
                })
                
                self.stores[self.storeIndex].products[self.productIndex].select_quatity = "\(self.quantitySelected)"
                self.savoryView.reloadData()
                
            }
            
        }

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "addressOrderSegue" {
            let addressOrderViewController = segue.destination as! AddressOrderViewController
            addressOrderViewController.store = sender as! Store
        }
        
        
        if segue.identifier == "registerAddressOrderSegue" {
            let registerAddressOrderViewController = segue.destination as! RegisterAddressOrderViewController
            registerAddressOrderViewController.store = sender as! Store
        }
        
    }
    
    @IBAction func continuosPurchaseProduct(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension CartPurchasesViewController : SavoryViewDelegate {
    
    func headerCell(forPanelAt index: SavoryPanelIndex, in savoryView: SavoryView) -> SavoryHeaderCell {
        
        let cell = savoryView.dequeueReusableHeaderCell(forPanelAt: index) as! HeaderShoppingCartTableViewCell

        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/store-logo/thumb/")!
        let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.stores[index].picture!)")
        
        self.manager.loadImage(with: url, into: cell.storeImageView) { ( result, _ ) in
            cell.storeImageView.image = result.value?.circleMask
        }
        
        cell.nameStoreLabel.text = self.stores[index].name
        
        var quantityProductStore = 0
        
        for product in self.stores[index].products {
            quantityProductStore += Int(product.select_quatity!)!
        }
        
        cell.descriptionPurchaseLabel.text = "Você possui \(quantityProductStore) item nesta loja selecione aqui para realizar o check-out desse item"
    
        cell.selectionStyle = .none
        
        return cell
    }
    
    func bodyCell(forPanelAt index: SavoryPanelIndex, in savoryView: SavoryView) -> SavoryBodyCell {
        
        let cell = savoryView.dequeueReusableBodyCell(forPanelAt: index) as! BodyShoppingCartTableViewCell
        
        cell.delegate = self
        cell.accessibilityHint = "\(index)"
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.accessibilityHint = "\(index)"
        cell.tableView.register(UINib(nibName: "CartPurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: "cartPurchaseCell")
        cell.tableView.rowHeight = UITableViewAutomaticDimension
        cell.tableView.estimatedRowHeight = 140.0
        cell.tableView.isScrollEnabled = false
        cell.tableView.showsVerticalScrollIndicator = false
        cell.tableView.showsHorizontalScrollIndicator = false
        cell.bodyHeightConstraint.constant = 250.0 + (170.0 * CGFloat(self.stores[index].products.count))
        cell.tableView.reloadData()
        
        var totalItems : Double = 0.0
        var totalPrice : Double = 0.0
        
        for product in self.stores[index].products {
            
            totalItems += (product.select_quatity! as NSString).doubleValue
            totalPrice += (product.select_quatity! as NSString).doubleValue * ((product.promo_price! == "") ? (product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)
            
        }
        
        cell.subTotalItemLabel.text = "Subtotal (\(Int(totalItems)) item)"
        cell.priceTotalMoneyLabel.text = "\(totalPrice.asLocaleCurrency)"
        cell.totalPriceLabel.text = "\(totalPrice.asLocaleCurrency)"
        
        // ==========================================
        // ========= FORMA DE PARCELAMENTO ==========
        // ==========================================
        
        if self.stores[index].advance_commission! == "0" {
            
            let parcelMinimal : Double = 50.0
            var parcelIsValidate : Bool = false
            
            for i in (1...6).reversed() {
                
                var parcel : Double = 0.0
                
                parcel =  totalPrice / Double(i)
                
                if parcel >= parcelMinimal {
                    cell.parcelPriceLabel.text = "Até \(i)x de \(parcel.asLocaleCurrency)"
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
                
            }
            
            if parcelIsValidate == false {
                cell.parcelPriceLabel.text = "Até 1x de \(totalPrice.asLocaleCurrency)"
            }
            
        } else {
            
            let parcelMinimal : Double = 100.0
            var parcelIsValidate : Bool = false
            
            for i in (1...6).reversed() {
                
                var parcel : Double = 0.0
                
                parcel =  totalPrice / Double(i)
                
                if parcel >= parcelMinimal {
                    cell.parcelPriceLabel.text = "Até \(i)x de \(parcel.asLocaleCurrency)"
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
                
            }
            
            if parcelIsValidate == false {
                cell.parcelPriceLabel.text = "Até 1x de \(totalPrice.asLocaleCurrency)"
            }
            
        }
        
        // ==========================================
        // ==========================================
        // ==========================================
        

        return cell
        
    }
    
    func didExpand(panelAt index: SavoryPanelIndex, in savoryView: SavoryView) {
        self.messageLabel.text = "Resumo da compra"
    }
    
    func didCollapse(panelAt index: SavoryPanelIndex, in savoryView: SavoryView) {
        self.messageLabel.text = "Selecione a loja que deseja finalizar a compra"
    }
    
}

extension CartPurchasesViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stores[Int(tableView.accessibilityHint!)!].products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartPurchaseCell", for: indexPath) as! CartPurchaseTableViewCell
        
        cell.delegate = self
        cell.storeId = Int(tableView.accessibilityHint!)!
        cell.productId = indexPath.row
        cell.selectionStyle = .none
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/products/\(self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].pId!)/\(self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].picture!)")
        
        
        self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
            cell.productImageView.image = result.value
        }
        
        cell.nameProductLabel.text = self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].name
        cell.sizeProductLabel.text = "Tamanho: \(self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].size!)"
        cell.colorProductLabel.text = self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].color
        cell.quantityProductLabel.text = "\(self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].select_quatity!)"
        
        cell.priceProductLabel.text = "\((((self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].promo_price! == "") ? (self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)).asLocaleCurrency)"
        
        cell.totalPriceProductsStoreLabel.text = "\(((self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].select_quatity! as NSString).doubleValue * ((self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].promo_price! == "") ? (self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)).asLocaleCurrency)"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
}

extension CartPurchasesViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Remover") { (action : UITableViewRowAction, indexPath : IndexPath) in

            let alertController = UIAlertController(title: "Remover do carrinho", message: "Deseja excluir esse produto do carrinho de compras ?", preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (action : UIAlertAction) in
                
                
                WishareCoreData.removeShoppingCart(pId: self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].id!, size: self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].size!, sId: self.stores[Int(tableView.accessibilityHint!)!].id!, completion: { (error : Error?) in
                    
                    if error == nil {
                        
                        self.stores[Int(tableView.accessibilityHint!)!].products.remove(at: indexPath.row)
                        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                        
                        Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { (timer : Timer) in
                            
                            
                            if self.stores[Int(tableView.accessibilityHint!)!].products.count == 0 {

                                self.stores.remove(at: Int(tableView.accessibilityHint!)!)
                                
                                var stateProvider : [SavoryPanelState] = []
                                
                                for _ in self.stores {
                                    stateProvider.append(SavoryPanelState.collapsed)
                                }
                                
                                self.savoryView.stateProvider = SimpleStateProvider(stateProvider)
                                
                                self.savoryView.reloadData()
                                
                            } else {
                                
                                var stateProvider : [SavoryPanelState] = []
                                
                                for store in self.stores {
                                    if store.id! == self.stores[Int(tableView.accessibilityHint!)!].id! {
                                        stateProvider.append(SavoryPanelState.expanded)
                                    } else {
                                        stateProvider.append(SavoryPanelState.collapsed)
                                    }
                                }
                                
                                self.savoryView.stateProvider = SimpleStateProvider(stateProvider)
                                
                                tableView.reloadData()
                                self.savoryView.reloadData()
                                self.view.setNeedsLayout()
                                self.view.layoutIfNeeded()
                                
                            }
                            
                            
                            if self.stores.count == 0 {
                                
                                self.savoryView.isHidden = true
                                
                            } else {
                                
                                self.savoryView.isHidden = false
                                
                            }

                            
                            
                        })
                        

                        
                    }
                    
                })
                
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }
        
        
        let quantity = UITableViewRowAction(style: .default, title: "Quantidade") { (action : UITableViewRowAction, indexPath : IndexPath) in
            
            self.quantity = Int(self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].quantity!)!
            
            if self.quantity > 0 {
                
                self.storeIndex = Int(tableView.accessibilityHint!)!
                self.productIndex = indexPath.row
                
                print(Int(tableView.accessibilityHint!)!)
                print(indexPath.row)
                
                self.quantityPickerView.reloadAllComponents()
                self.quantityPickerView.selectRow(Int(self.stores[Int(tableView.accessibilityHint!)!].products[indexPath.row].select_quatity!)! - 1, inComponent: 0, animated: false)
                
                UIView.animate(withDuration: 0.4, animations: {
                    self.quantityViewBottomConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
                
            } else {
                
                let alertController = UIAlertController(title: "Produto Indisponível", message: "Esse produto não possui estoque disponível", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }

            
        }
        
        quantity.backgroundColor = UIColor.lightGray
        
        return [delete, quantity]
        
    }
    
}

extension CartPurchasesViewController : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.quantitySelected = row + 1
    }
    
}

extension CartPurchasesViewController : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.quantity
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 1)"
    }
    
}

extension CartPurchasesViewController : CartPurchaseProduct {
    
    func removeItem(storeId: Int?, productId: Int?) {
        
        let alertController = UIAlertController(title: "Remover do carrinho", message: "Deseja excluir esse produto do carrinho de compras ?", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (action : UIAlertAction) in
            
            WishareCoreData.removeShoppingCart(pId: self.stores[storeId!].products[productId!].id!, size: self.stores[storeId!].products[productId!].size!, sId: self.stores[storeId!].id!, completion: { (error : Error?) in
                
                if error == nil {
                    
                    self.stores[storeId!].products.remove(at: productId!)
                    
                    Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { (timer : Timer) in
                        
                        var stateProvider : [SavoryPanelState] = []
                        
                        for store in self.stores {
                            
                            var storeExists : Bool = false
                            
                            for s in self.stores {
                                if store.id! == s.id! {
                                    stateProvider.append(SavoryPanelState.expanded)
                                    storeExists = true
                                }
                            }
                            
                            if !storeExists {
                                stateProvider.append(SavoryPanelState.collapsed)
                            }
                        }
                        
                        self.savoryView.stateProvider = SimpleStateProvider(stateProvider)
                        
                        self.savoryView.reloadData()
                    })
                    
                    if self.stores[storeId!].products.count == 0 {
                        
                        self.stores.remove(at: storeId!)
                        
                        var stateProvider : [SavoryPanelState] = []
                        
                        for _ in self.stores {
                            stateProvider.append(SavoryPanelState.collapsed)
                        }
                        
                        self.savoryView.stateProvider = SimpleStateProvider(stateProvider)
                        
                        self.savoryView.reloadData()
                        
                    }
                    
                    if self.stores.count == 0 {
                        
                        self.savoryView.isHidden = true
                        
                    } else {
                        
                        self.savoryView.isHidden = false
                        
                    }
                    
                }
                
            })
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func changeQuantity(storeId: Int?, productId: Int?) {
        
        self.quantity = Int(self.stores[storeId!].products[productId!].quantity!)!
        
        if self.quantity > 0 {
            
            self.storeIndex = storeId!
            self.productIndex = productId!
            
            self.quantityPickerView.reloadAllComponents()
            self.quantityPickerView.selectRow(Int(self.stores[storeId!].products[productId!].select_quatity!)! - 1, inComponent: 0, animated: false)
            
            UIView.animate(withDuration: 0.4, animations: {
                self.quantityViewBottomConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
            
        } else {
            
            let alertController = UIAlertController(title: "Produto Indisponível", message: "Esse produto não possui estoque disponível", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
}

extension CartPurchasesViewController : BodyShoppingCartDelegate {
    
    func finishPurchase(_ sender: UIButton, cell: BodyShoppingCartTableViewCell) {
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
            
            if error == nil {
                
                RappleActivityIndicatorView.stopAnimation()
                
                if addresses.count > 0 {
                    self.performSegue(withIdentifier: "addressOrderSegue", sender: self.stores[Int(cell.accessibilityHint!)!])
                } else {
                    self.performSegue(withIdentifier: "registerAddressOrderSegue", sender: self.stores[Int(cell.accessibilityHint!)!])
                }
                
            }
            
        }
        
    }
    
}
