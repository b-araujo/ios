//
//  OrderProductCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class OrderProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var quantiyLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var unitPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var avaliarButton: UIButton!
    
    var delegate : OrderProductDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avaliarButton.layer.cornerRadius = 5.0
        
    }

    @IBAction func avaliar(_ sender: UIButton) {
        self.delegate?.avaliar(sender, cell: self)
    }
    
    
}

protocol OrderProductDelegate {
    func avaliar( _ sender : UIButton, cell : OrderProductCollectionViewCell)
}
