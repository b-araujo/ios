//
//  EmailRulePt.swift
//  Wishare
//
//  Created by Wishare on 05/04/18.
//  Copyright © 2018 Wishare iMac. All rights reserved.
//

import Foundation
import SwiftValidator
/**
 `EmailRule` is a subclass of RegexRule that defines how a email is validated.
 */
public class EmailRulePt: RegexRule {
    
    /// Regular express string to be used in validation.
    static let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    /**
     Initializes an `EmailRule` object to validate an email field.
     
     - parameter message: String of error message.
     - returns: An initialized object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public convenience init(message : String = "Este e-mail não é válido"){
        self.init(regex: EmailRulePt.regex, message: message)
    }
}
