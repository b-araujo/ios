//
//  ShowBrandAndStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 8/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class ShowBrandAndStoreViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var brandOrStore : [[String : String]] = []
    var brandOrStoreClone : [[String : String]] = []
    
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeTextSearch(notification:)), name: Notification.Name.init("changeTextSearch"), object: nil)
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "UserMentionTableViewCell", bundle: nil), forCellReuseIdentifier: "mentionCell")
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.estimatedRowHeight = 65.0
        self.tableView.rowHeight = 65.0
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getAllStores { (stores : [Mention], error : Error?) in
            if error == nil {
                
                WishareAPI.getAllBrands({ (brands : [String], error : Error?) in
                    if error == nil {
                        
                        for m in stores {
                            if !m.objectMentin.contains("@username") {
                                self.brandOrStore.append(["id": m.id, "name": m.username, "picture": m.picture])
                                self.brandOrStoreClone.append(["id": m.id, "name": m.username, "picture": m.picture])
                            }
                        }
                        
                        
                        for b in brands {
                            self.brandOrStore.append(["id": "0", "name": b, "picture": "LOCAL"])
                            self.brandOrStoreClone.append(["id": "0", "name": b, "picture": "LOCAL"])
                        }
                        
                        self.tableView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                })
                
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func changeTextSearch(notification : Notification) {
        
        let text = notification.object as! String
        
        if text == "" {
            
            self.brandOrStore.removeAll()
            self.brandOrStore = self.brandOrStoreClone
            self.tableView.reloadData()
            
        } else {
            
            self.brandOrStore.removeAll()
            
            for bs in self.brandOrStoreClone {
                if bs["name"]!.contains(text) {
                    self.brandOrStore.append(bs)
                }
            }
            
            self.tableView.reloadData()
            
        }
        
    }

}

extension ShowBrandAndStoreViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("createTag"), object: self.brandOrStore[indexPath.row])
    }
    
}

extension ShowBrandAndStoreViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.brandOrStore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mentionCell", for: indexPath) as! UserMentionTableViewCell
        
        cell.loadingAcitivityIndicator.startAnimating()
        
        if self.brandOrStore[indexPath.row]["picture"]! == "LOCAL" {
            
            cell.loadingAcitivityIndicator.stopAnimating()
            cell.topConstraintImageView.constant = 18
            cell.heightConstraintImageView.constant = 20
            cell.userImageView.image = UIImage(named: "tag-off")
            
        } else {
            
            let urlStringUser : URL = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.brandOrStore[indexPath.row]["picture"]!)")
            
            cell.topConstraintImageView.constant = 12
            cell.heightConstraintImageView.constant = 40
            
            self.manager.loadImage(with: urlStringUser, into: cell.userImageView, handler: { (result, _) in
                cell.loadingAcitivityIndicator.stopAnimating()
                cell.userImageView.image = result.value?.circleMask
            })
            
        }
        
        cell.userNameLabel.text = self.brandOrStore[indexPath.row]["name"]! + ((self.brandOrStore[indexPath.row]["id"]! == "0") ? " (Marca)" : " (Loja)")
        
        
        return cell
        
    }
    
}

