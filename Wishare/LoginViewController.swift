//
//  ViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Alamofire
import Alamofire_Synchronous
import FBSDKLoginKit
import RappleProgressHUD

class LoginViewController : UIViewController {

    
    // MARK: - Outlets
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var registerLabelView: UIView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    
    
    // MARK: - Properties
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    var dict : [String : AnyObject]!
    var face_id : String?
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideOrShowComponentsAndLoading(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        WishareCoreData.isLoggedIn { (result : Bool, error : Error?, firstAccess : String?) in
            print(result)
            
            if result {
            
                let token: String? = UserDefaults.standard.object(forKey: "device_token") as? String
                
                WishareAPI.enableNotificationDevice(token: token!, { (error : Error?) in
                    if error == nil {
                        
                        // ======= PRIMEIRO ACESSO ========
                        
                        if firstAccess! == "0" {
                            self.performSegue(withIdentifier: "startLoginSegue", sender: nil)
                        } else {
                            self.performSegue(withIdentifier: "facebookFirstAccessSegue", sender: nil)
                        }
                        
                        // ================================
                    
                    }
                })
                
                
            } else {
                
                if error != nil {
                    
                    let actionController = UIAlertController(title: "Problema no Login", message: "Login ou senha inválidos", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    actionController.addAction(okAction)
                    self.present(actionController, animated: true, completion: nil)
                    
                }

                self.hideOrShowComponentsAndLoading(false)
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Actions
    @IBAction func userLogin(_ sender: UIButton) {
        
        self.hideOrShowComponentsAndLoading(true)
        
        if let user = self.userTextField.text, let password = self.passwordTextField.text {
        
            WishareAPI.userAuthorization(user, password, false, { (isValidate : Bool, error : Error?, firstAccess : String?) in
                
                if error == nil && isValidate != false {
                  
                    WishareCoreData.getFirstUser({ (userCore : UserActive?, error : Error?) in
                        if userCore != nil && error == nil {
                            
                            if userCore!.active == "0" {
                                
                                let alertController = UIAlertController(title: "Conta Desativada", message: "Sua conta esta desativada, você poderá reativa-la em um período de até 180 dias.", preferredStyle: .alert)
                                
                                let reactiveAction = UIAlertAction(title: "Reativar agora!", style: .default, handler: { ( _ ) in
                                    
                                    WishareAPI.reactiveAccount({ (error : Error?) in
                                        if error == nil {
                                            
                                            
                                            WishareAPI.enableNotificationDevice(token: UserDefaults.standard.object(forKey: "device_token") as! String, { (error : Error?) in
                                                if error == nil {
                                                    
                                                    if firstAccess! == "0" {
                                                        self.performSegue(withIdentifier: "startLoginSegue", sender: nil)
                                                    } else {
                                                        self.performSegue(withIdentifier: "firstAccessSegue", sender: nil)
                                                    }
                                                    
                                                }
                                            })
                                            

                                        }
                                    })
                                    
                                })
                                
                                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: { ( _ ) in
                                    WishareCoreData.deleteUsers({ ( _ ) in })
                                })
                                
                                alertController.addAction(reactiveAction)
                                alertController.addAction(cancelAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                            } else {

                                WishareAPI.enableNotificationDevice(token: UserDefaults.standard.object(forKey: "device_token") as! String, { (error : Error?) in
                                    if error == nil {
                                        
                                        if firstAccess! == "0" {
                                            self.performSegue(withIdentifier: "startLoginSegue", sender: nil)
                                        } else {
                                            self.performSegue(withIdentifier: "firstAccessSegue", sender: nil)
                                        }
                                        
                                    }
                                })
                        
                            }
                            
                        }
                    })
                    
                    

                    
                } else {
                    
                    let actionController = UIAlertController(title: nil, message: "Login ou senha inválidos", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                    actionController.addAction(okAction)
                    self.present(actionController, animated: true, completion: nil)
                    
                }
                
                self.hideOrShowComponentsAndLoading(false)
                
            })
            
        }
        
    }
    
    
    @IBAction func userLoginFacebook(_ sender: UIButton) {
        
        self.hideOrShowComponentsAndLoading(true)
        
        FacebookAPI.facebookAuthorization(self) { (isValidate : Bool) in
            
            if isValidate {
                
                let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
                
                FacebookAPI.getFacebookUserData({ ( result : [String : AnyObject], error : Error?) in
                    
                    if let facebook_id : String = result["id"] as? String {
                        
                        WishareAPI.userAuthorization(facebook_id, facebook_id, true, { (isLogIn : Bool, error : Error?, firstAccess : String?) in
                            
                            if isLogIn {
                                
                                let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
                                
                                WishareAPI.enableNotificationDevice(token: UserDefaults.standard.object(forKey: "device_token") as! String, { (error : Error?) in
                                    if error == nil {
                                        
                                        // ======= FIRST ACCESS FACEBOOK =======
                                        
                                        if firstAccess! == "0" {
                                            self.performSegue(withIdentifier: "startLoginSegue", sender: nil)
                                        } else {
                                            self.performSegue(withIdentifier: "facebookFirstAccessSegue", sender: nil)
                                        }
                                        
                                        RappleActivityIndicatorView.stopAnimation()
                                        
                                        // =====================================
                                        
                                    }
                                })
                                
                                
                                
                            } else {
                                self.performSegue(withIdentifier: "registerSegue", sender: result)
                            }
                            
                        })
                        
                    }
                    
                })
                
            }
            else {
                self.hideOrShowComponentsAndLoading(false)
            }
            
        }
        
    }
    
    
    // MARK: - Utils
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.becomeFirstResponder()
    }
    
    
    func hideOrShowComponentsAndLoading( _ isHidden : Bool) {
        
        self.userTextField.isHidden = isHidden
        self.passwordTextField.isHidden = isHidden
        self.continueButton.isHidden = isHidden
        self.facebookButton.isHidden = isHidden
        self.forgotPasswordButton.isHidden = isHidden
        self.registerLabelView.isHidden = isHidden
        
        if isHidden {
            self.loadingActivityIndicator.startAnimating()
        } else {
            self.loadingActivityIndicator.stopAnimating()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "registerSegue" {
            
            let registerController = segue.destination as! RegisterViewController
            registerController.fbUserData = sender as? [String : AnyObject]
            
        }
        
        
    }


}



