//
//  WishesProductCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 8/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class WishesProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!

    var delegate : WishesProductDelegate? = nil
    var indexPath : IndexPath? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.storeImageView.isUserInteractionEnabled = true
        self.nameStoreLabel.isUserInteractionEnabled = true
        self.productImageView.isUserInteractionEnabled = true
        
        let tapGestureRecognizerStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreImageView))
        let tapGestureRecognizerNameStoreLabel = UITapGestureRecognizer(target: self, action: #selector(tapNameStoreLabel))
        let tapGestureRecognizerProductImageView = UITapGestureRecognizer(target: self, action: #selector(tapProductImageView))
        
        self.storeImageView.addGestureRecognizer(tapGestureRecognizerStoreImageView)
        self.nameStoreLabel.addGestureRecognizer(tapGestureRecognizerNameStoreLabel)
        self.productImageView.addGestureRecognizer(tapGestureRecognizerProductImageView)
        
        
    }
    
    @IBAction func buyProduct(_ sender: UIButton) {
        if let indexPath = self.indexPath {
            self.delegate?.openProductWishes(cell: self, indexPath: indexPath, sender: sender)
        }
    }
    
    @objc func tapStoreImageView () {
        if let indexPath = self.indexPath {
           self.delegate?.openStoreWishes(cell: self, indexPath: indexPath)
        }
    }
    
    @objc func tapNameStoreLabel() {
        if let indexPath = self.indexPath {
            self.delegate?.openStoreWishes(cell: self, indexPath: indexPath)
        }
    }
    
    @objc func tapProductImageView() {
        if let indexPath = self.indexPath {
            self.delegate?.openProductImage(cell: self, indexPath: indexPath)
        }
    }

}

protocol WishesProductDelegate {
    func openProductWishes(cell : WishesProductCollectionViewCell, indexPath : IndexPath, sender : UIButton)
    func openStoreWishes(cell : WishesProductCollectionViewCell, indexPath : IndexPath)
    func openProductImage(cell : WishesProductCollectionViewCell, indexPath : IndexPath)
}
