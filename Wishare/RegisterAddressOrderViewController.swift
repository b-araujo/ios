//
//  RegisterAddressOrderViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SkyFloatingLabelTextField
import TLCustomMask

class RegisterAddressOrderViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var rgTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cpfTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var telefoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var celularTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var cepTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var enderecoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var numeroTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var complementoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var bairroTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cidadeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var estadoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tipoEnderecoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var destinatarioTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textCobrancaView: UIView!
    @IBOutlet weak var cobrancaView: UIView!
    
    @IBOutlet weak var otherEnderecoButton: UIButton!
    
    @IBOutlet weak var cepCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var enderecoCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var numeroCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var completmentoCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var bairroCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cidadeCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var estadoCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tipoEnderecoCobrancaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var destinatarioCobrancaTextField: SkyFloatingLabelTextField!
    
    
    var store : Store!
    
    let statePickerView = ["AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SE","SP","TO"]
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var otherEnd :  Bool = false
    
    var cpfMask = TLCustomMask(formattingPattern: "$$$.$$$.$$$-$$")
    
    var homePhoneMaskForPhone = TLCustomMask(formattingPattern: "($$) $$$$-$$$$")
    var mobilePhoneMaskForPhone = TLCustomMask(formattingPattern: "($$) $$$$$-$$$$")
    var usingMobileForPhone : Bool = false
    
    var homePhoneMaskForMobile = TLCustomMask(formattingPattern: "($$) $$$$-$$$$")
    var mobilePhoneMaskForMobile = TLCustomMask(formattingPattern: "($$) $$$$$-$$$$")
    var usingMobileForMobile : Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textCobrancaView.isHidden = true
        self.cobrancaView.isHidden = true
        self.navigationController?.navigationBar.backItem?.title = "    "
        
        // Picker View
        let pickerView = UIPickerView()
        pickerView.delegate = self
        self.estadoTextField.inputView = pickerView
        
        self.cepTextField.tag = 10
        self.cepCobrancaTextField.tag = 20
        
        self.cepTextField.delegate = self
        self.cepCobrancaTextField.delegate = self
        
        self.telefoneTextField.delegate = self
        self.celularTextField.delegate = self
        
        self.cpfTextField.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Continuar", style: .plain, target: self, action: #selector(nextShowAddress))
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.getFirstUser { (userData : UserActive?, error : Error?) in
            
            if error ==  nil && userData != nil {
            
                WishareAPI.getUser(idUser: userData!.id!, { (user : User?, error : Error?) in
                    
                    if error == nil {
                        
                        self.nameTextField.text = user!.name
                        self.lastNameTextField.text = user!.last_name
                        self.rgTextField.text = user!.rg
                        if let cpf = user?.cpf {
                            self.cpfTextField.text = self.cpfMask.formatString(string: cpf)
                        }
                        self.telefoneTextField.text = user!.phone
                        self.celularTextField.text = user!.cell
                        self.destinatarioTextField.text = "\(user!.name!) \(user!.last_name!)"
                        self.destinatarioCobrancaTextField.text = "\(user!.name!) \(user!.last_name!)"
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                })
            
            }
            
        }
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "addressOrderSegue" {
            let addressOrderViewController = segue.destination as! AddressOrderViewController
            addressOrderViewController.store = sender as! Store
        }
        
    }
    
    
    @objc func nextShowAddress() {
        
        var countError = 0
        var firstTextFieldWithError: SkyFloatingLabelTextField?
        
        if (self.nameTextField.text?.isEmpty)! {
            
            self.nameTextField.errorMessage = "Nome Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.nameTextField
            }
            
        } else {
            
            self.nameTextField.errorMessage = nil
            
        }
        
        if (self.lastNameTextField.text?.isEmpty)! {
            
            self.lastNameTextField.errorMessage = "Sobrenome Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.lastNameTextField
            }
            
        } else {
            
            self.lastNameTextField.errorMessage = nil
            
        }
        
        if (self.rgTextField.text?.isEmpty)! {
            
            self.rgTextField.errorMessage = "R.G. Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.rgTextField
            }
            
        } else {
            
            self.rgTextField.errorMessage = nil
            
        }
        
        if (self.cpfTextField.text?.isEmpty)! {
            
            self.cpfTextField.errorMessage = "CPF Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.cpfTextField
            }
            
        } else {
            
            if self.cpfMask.cleanText.isValidCPF {
               self.cpfTextField.errorMessage = nil
            } else {
                self.cpfTextField.errorMessage = "CPF Inválido"
                countError += 1
                if firstTextFieldWithError == nil {
                    firstTextFieldWithError = self.cpfTextField
                }
            }
            
        }
        
        if (self.telefoneTextField.text?.isEmpty)! {
            
            self.telefoneTextField.errorMessage = "Telefone Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.telefoneTextField
            }
            
        } else {
            
            self.telefoneTextField.errorMessage = nil
            
        }
        
        if (self.celularTextField.text?.isEmpty)! {
            
            self.celularTextField.errorMessage = "Celular Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.celularTextField
            }
            
        } else {
            
            self.celularTextField.errorMessage = nil
            
        }
        
        if (self.cepTextField.text?.isEmpty)! {
            
            self.cepTextField.errorMessage = "C.E.P. Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.cepTextField
            }
            
        } else {
            
            self.cepTextField.errorMessage = nil
            
        }
        
        if (self.enderecoTextField.text?.isEmpty)! {
            
            self.enderecoTextField.errorMessage = "Endereço Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.enderecoTextField
            }
            
        } else {
            
            self.enderecoTextField.errorMessage = nil
            
        }
        
        if (self.numeroTextField.text?.isEmpty)! {
            
            self.numeroTextField.errorMessage = "Número Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.numeroTextField
            }
            
        } else {
            
            self.numeroTextField.errorMessage = nil
            
        }
        
        if (self.bairroTextField.text?.isEmpty)! {
            
            self.bairroTextField.errorMessage = "Bairro Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.bairroTextField
            }
            
        } else {
            
            self.bairroTextField.errorMessage = nil
            
        }
        
        if (self.cidadeTextField.text?.isEmpty)! {
            
            self.cidadeTextField.errorMessage = "Cidade Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.cidadeTextField
            }
            
        } else {
            
            self.cidadeTextField.errorMessage = nil
            
        }
        
        if (self.estadoTextField.text?.isEmpty)! {
            
            self.estadoTextField.errorMessage = "Estado Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.estadoTextField
            }
        
        } else {
            
            self.estadoTextField.errorMessage = nil
            
        }
        
        if (self.tipoEnderecoTextField.text?.isEmpty)! {
        
            self.tipoEnderecoTextField.errorMessage = "Tipo Endereço Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.tipoEnderecoTextField
            }
            
        } else {
            
            self.tipoEnderecoTextField.errorMessage = nil
            
        }
        
        if (self.destinatarioTextField.text?.isEmpty)! {
            
            self.destinatarioTextField.errorMessage = "Destinatário Obrigatorio"
            countError += 1
            if firstTextFieldWithError == nil {
                firstTextFieldWithError = self.destinatarioTextField
            }
            
        } else {
            
            self.destinatarioTextField.errorMessage = nil
            
        }
        
        
        if countError == 0 {
            if !self.otherEnd {
                
                RappleActivityIndicatorView.startAnimatingWithLabel("Cadastrando...", attributes: attributes)
                
                WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: self.complementoTextField.text!, city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "P", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                    
                    if error == nil && validate == true {
                        
                        WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: self.complementoTextField.text!, city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "S", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                            
                            if error == nil && validate == true {
                                
                                let homePhone = self.usingMobileForPhone ? self.mobilePhoneMaskForPhone : self.homePhoneMaskForPhone
                                let mobilePhone = self.usingMobileForMobile ? self.mobilePhoneMaskForMobile : self.homePhoneMaskForMobile
                            
                                
                                WishareAPI.updateUser(name: self.nameTextField.text!, lastName: self.lastNameTextField.text!, rg: self.rgTextField.text!, cpf: self.cpfMask.cleanText, telefone: homePhone.cleanText, celular: mobilePhone.cleanText, { (isSuccess : Bool, error : Error?) in
                                    
                                    if isSuccess {
                                        
                                        RappleActivityIndicatorView.stopAnimation()
                                        self.performSegue(withIdentifier: "addressOrderSegue", sender: self.store)
                                        
                                    }
                                    
                                })
                                
                            }
                            
                        })
                        
                    }
                    
                })
                
            } else {
                
                
                RappleActivityIndicatorView.startAnimatingWithLabel("Cadastrando...", attributes: attributes)
                
                WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: self.complementoTextField.text!, city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "S", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                    
                    if error == nil && validate == true {
                        
                        WishareAPI.addresses(name: self.tipoEnderecoCobrancaTextField.text!, postCode: self.cepCobrancaTextField.text!, address: self.enderecoCobrancaTextField.text!, number: self.numeroCobrancaTextField.text!, more: self.completmentoCobrancaTextField.text!, city: self.cidadeCobrancaTextField.text!, state: self.estadoCobrancaTextField.text!, country: "Brasil", type: "P", neighborhood: self.bairroCobrancaTextField.text!, recipientName: self.destinatarioCobrancaTextField.text!, { (validate : Bool, error : Error?) in
                            
                            if error == nil && validate == true {
                                
                                let homePhone = self.usingMobileForPhone ? self.mobilePhoneMaskForPhone : self.homePhoneMaskForPhone
                                let mobilePhone = self.usingMobileForMobile ? self.mobilePhoneMaskForMobile : self.homePhoneMaskForMobile
                                
                                WishareAPI.updateUser(name: self.nameTextField.text!, lastName: self.lastNameTextField.text!, rg: self.rgTextField.text!, cpf: self.cpfMask.cleanText, telefone: homePhone.cleanText, celular: mobilePhone.cleanText, { (isSuccess : Bool, error : Error?) in
                                    
                                    if isSuccess {
                                        
                                        RappleActivityIndicatorView.stopAnimation()
                                        self.performSegue(withIdentifier: "addressOrderSegue", sender: self.store)
                                        
                                    }
                                    
                                })
                                
                            }
                            
                        })
                        
                    }
                    
                })
                
            }
        } else {
            if let textFieldToScroll = firstTextFieldWithError {
                self.scrollView.scrollRectToVisible(textFieldToScroll.frame, animated: true)
                textFieldToScroll.becomeFirstResponder()
            }
        }
        
    }

    @IBAction func addOtherEndereco(_ sender: UIButton) {
        
        if !self.otherEnd {
            
            sender.setImage(UIImage(named: "checkbox_filled"), for: .normal)
            self.otherEnd = true
            self.bottomViewConstraint.constant = 750
            self.textCobrancaView.isHidden = false
            self.cobrancaView.isHidden = false
            
        } else {
            
            sender.setImage(UIImage(named: "checkbox"), for: .normal)
            self.otherEnd = false
            self.bottomViewConstraint.constant = 250
            self.textCobrancaView.isHidden = true
            self.cobrancaView.isHidden = true
        }
        
    }
    
}


extension RegisterAddressOrderViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 10 {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Buscando CEP...", attributes: attributes)
        
            WishareAPI.getAddressByCep(cep: textField.text!) { (address : [String : AnyObject], error : Error?) in
                
                if error == nil {
                    
                    self.bairroTextField.text = address["bairro"] as? String
                    self.cidadeTextField.text = address["cidade"] as? String
                    self.enderecoTextField.text = address["end"] as? String
                    self.estadoTextField.text = address["uf"] as? String
                    
                    
                    if (self.nameTextField.text?.isEmpty)! {
                        
                        self.nameTextField.errorMessage = "Nome Obrigatorio"
                        
                    } else {
                        
                        self.nameTextField.errorMessage = nil
                        
                    }
                    
                    if (self.lastNameTextField.text?.isEmpty)! {
                        
                        self.lastNameTextField.errorMessage = "Sobrenome Obrigatorio"
                        
                    } else {
                        
                        self.lastNameTextField.errorMessage = nil
                        
                    }
                    
                    if (self.rgTextField.text?.isEmpty)! {
                        
                        self.rgTextField.errorMessage = "R.G. Obrigatorio"
                        
                    } else {
                        
                        self.rgTextField.errorMessage = nil
                        
                    }
                    
                    if (self.cpfTextField.text?.isEmpty)! {
                        
                        self.cpfTextField.errorMessage = "C.P.F. Obrigatorio"
                        
                    } else {
                        
                        self.cpfTextField.errorMessage = nil
                        
                    }
                    
                    if (self.telefoneTextField.text?.isEmpty)! {
                        
                        self.telefoneTextField.errorMessage = "Telefone Obrigatorio"
                        
                    } else {
                        
                        self.telefoneTextField.errorMessage = nil
                        
                    }
                    
                    if (self.celularTextField.text?.isEmpty)! {
                        
                        self.celularTextField.errorMessage = "Celular Obrigatorio"
                        
                    } else {
                        
                        self.celularTextField.errorMessage = nil
                        
                    }
                    
                    if (self.cepTextField.text?.isEmpty)! {
                        
                        self.cepTextField.errorMessage = "C.E.P. Obrigatorio"
                        
                    } else {
                        
                        self.cepTextField.errorMessage = nil
                        
                    }
                    
                    if (self.enderecoTextField.text?.isEmpty)! {
                        
                        self.enderecoTextField.errorMessage = "Endereço Obrigatorio"
                        
                    } else {
                        
                        self.enderecoTextField.errorMessage = nil
                        
                    }
                    
                    if (self.numeroTextField.text?.isEmpty)! {
                        
                        self.numeroTextField.errorMessage = "Número Obrigatorio"
                        
                    } else {
                        
                        self.numeroTextField.errorMessage = nil
                        
                    }
                    
                    if (self.bairroTextField.text?.isEmpty)! {
                        
                        self.bairroTextField.errorMessage = "Bairro Obrigatorio"
                        
                    } else {
                        
                        self.bairroTextField.errorMessage = nil
                        
                    }
                    
                    if (self.cidadeTextField.text?.isEmpty)! {
                        
                        self.cidadeTextField.errorMessage = "Cidade Obrigatorio"
                        
                    } else {
                        
                        self.cidadeTextField.errorMessage = nil
                        
                    }
                    
                    if (self.estadoTextField.text?.isEmpty)! {
                        
                        self.estadoTextField.errorMessage = "Estado Obrigatorio"
                        
                    } else {
                        
                        self.estadoTextField.errorMessage = nil
                        
                    }
                    
                    if (self.tipoEnderecoTextField.text?.isEmpty)! {
                        
                        self.tipoEnderecoTextField.errorMessage = "Tipo Endereço Obrigatorio"
                        
                    } else {
                        
                        self.tipoEnderecoTextField.errorMessage = nil
                        
                    }
                    
                    if (self.destinatarioTextField.text?.isEmpty)! {
                        
                        self.destinatarioTextField.errorMessage = "Destinatário Obrigatorio"
                        
                    } else {
                        
                        self.destinatarioTextField.errorMessage = nil
                        
                    }
                    
                }
            
                RappleActivityIndicatorView.stopAnimation()

                
        }
        
        } else {
            

            
            WishareAPI.getAddressByCep(cep: textField.text!) { (address : [String : AnyObject], error : Error?) in
                
                if error == nil {
                    
                    self.bairroCobrancaTextField.text = address["bairro"] as? String
                    self.cidadeCobrancaTextField.text = address["cidade"] as? String
                    self.enderecoCobrancaTextField.text = address["end"] as? String
                    self.estadoCobrancaTextField.text = address["uf"] as? String
                    
                    
                }
                
                RappleActivityIndicatorView.stopAnimation()
                
            }
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if textField == telefoneTextField || textField == celularTextField {
//            if string.first == "0" {
//                textField.text = String(string.dropFirst())
//            }
//        }

        
        if textField.tag == 10 || textField.tag == 20 {
            
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        if textField == cpfTextField {
            let cpfFormatted = self.cpfMask.formatStringWithRange(range: range, string: string)
            textField.text = cpfFormatted
            
            return false
        }
        
        if textField == telefoneTextField {
            
            var mobile = self.mobilePhoneMaskForPhone.formatStringWithRange(range: range, string: string)
            let home = self.homePhoneMaskForPhone.formatStringWithRange(range: range, string: string)

            if home == "" {
                mobile = self.mobilePhoneMaskForPhone.formatStringWithRange(range: range, string: string)
            }
            
            if self.mobilePhoneMaskForPhone.cleanText.count > 10 {
                textField.text = mobile
                usingMobileForPhone = true

            } else {
                textField.text = home
                usingMobileForPhone = false
            }

            return false


        }
        
        if textField == celularTextField {
            
            var mobile = self.mobilePhoneMaskForMobile.formatStringWithRange(range: range, string: string)
            let home = self.homePhoneMaskForMobile.formatStringWithRange(range: range, string: string)
            
            if home == "" {
                mobile = self.mobilePhoneMaskForMobile.formatStringWithRange(range: range, string: string)
            }
            
            if self.mobilePhoneMaskForMobile.cleanText.count > 10 {
                textField.text = mobile
                usingMobileForMobile = true
                
            } else {
                textField.text = home
                usingMobileForMobile = false
            }
            
            return false
            
            
        }
        
        

        return true
        
    }

    
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
}

extension RegisterAddressOrderViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statePickerView.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statePickerView[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.estadoTextField.text = statePickerView[row]
    }
    
}
