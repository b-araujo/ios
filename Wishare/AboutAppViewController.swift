//
//  AboutAppViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import SafariServices

class AboutAppViewController: UIViewController {

    @IBOutlet weak var myWebSiteButton: UIButton!
    @IBOutlet weak var facebookImageView: UIImageView!
    @IBOutlet weak var instagramImageView: UIImageView!
    @IBOutlet weak var copyrightLabel: UILabel!
    @IBOutlet weak var versionNumberLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizerFacebook = UITapGestureRecognizer(target: self, action: #selector(openFacebook))
        let tapGestureRecognizerInstagram = UITapGestureRecognizer(target: self, action: #selector(openInstagram))
        
        self.facebookImageView.addGestureRecognizer(tapGestureRecognizerFacebook)
        self.instagramImageView.addGestureRecognizer(tapGestureRecognizerInstagram)
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
    
        self.copyrightLabel.text = "Copyright © 2015 - \(components.year!) - Wishare"
        
        updateVersion()
        
    }

    func updateVersion() {
        let versionInfo: Any? = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
        
        if let version = versionInfo as? String {
            print(version)
            versionNumberLabel.text = version
        }
        else {
            versionNumberLabel.text = ""
        }
        
        let buildInfo: Any? = Bundle.main.infoDictionary?["CFBundleVersion"]
        
        if let build = buildInfo as? String {
            print(build)
            versionNumberLabel.text?.append(" (\(build))")
        }


        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func openMyWebSite( _ sender : UIButton) {
        let safariViewController = SFSafariViewController(url: URL(string: "http://wishare.com.br/")!)
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    @objc func openFacebook() {
        let safariViewController = SFSafariViewController(url: URL(string: "https://www.facebook.com/wishareapp")!)
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    @objc func openInstagram() {
        let safariViewController = SFSafariViewController(url: URL(string: "https://www.instagram.com/wishareapp/")!)
        self.present(safariViewController, animated: true, completion: nil)
    }
    
}
