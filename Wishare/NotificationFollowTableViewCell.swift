//
//  NotificationFollowTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 8/1/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class NotificationFollowTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var acceptFollowButton: UIButton!
    @IBOutlet weak var notAcceptFollowButton: UIButton!
    
    var delegate : WishareNotificationDelegate? = nil
    var indexPath : IndexPath? = nil
    var tableView : UITableView? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureRecognizerUserImageView = UITapGestureRecognizer(target: self, action: #selector(pressUserImageView))
        self.userImageView.addGestureRecognizer(tapGestureRecognizerUserImageView)
        
        self.userNameButton.addTarget(self, action: #selector(pressUserNameButton(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func acceptOrNotFollow(_ sender: UIButton) {
        if let tableView = self.tableView, let indexPath = self.indexPath {
            self.delegate?.acceptOrDeclineInvite(tableView, indexPath, sender)
        }
    }
    
    @objc func pressUserImageView() {
        if let tableView = self.tableView, let indexPath = self.indexPath {
            self.delegate?.openUserProfile(tableView, indexPath)
        }
    }
    
    @objc func pressUserNameButton( _ sender : UIButton) {
        if let tableView = self.tableView, let indexPath = self.indexPath {
            self.delegate?.openUserProfile(tableView, indexPath)
        }
    }
}
