//
//  TagCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 8/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var tagView: UIView!
    
    var delegate : TagCollectionDelegate? = nil
    var indexPath : IndexPath? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func pressCloseTag(_ sender: UIButton) {
        if let indexPath = indexPath {
            self.delegate?.closeTag(indexPath)
        }
        
    }

}

protocol TagCollectionDelegate {
    func closeTag( _ indexPath : IndexPath)
}
