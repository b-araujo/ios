//
//  MyAddressViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/6/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD

class MyAddressViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var registerAddressButton: UIButton!
    
    var myAddressShipping : [[String : String]] = []
    var myAddressPayment : [[String : String]] = []
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var selectedItem : Bool = false
    var isPayment : Bool = false
    var myAddressSelected : [[String : String]] = []
    var otherAddress : [String : String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerAddressButton.layer.cornerRadius = 5.0
        self.registerAddressButton.layer.masksToBounds = false
        self.registerAddressButton.layer.shadowColor = UIColor.black.cgColor
        self.registerAddressButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.registerAddressButton.layer.shadowRadius = 2.0
        self.registerAddressButton.layer.shadowOpacity = 0.24
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "addressCell")
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                self.myAddressPayment.removeAll()
                self.myAddressShipping.removeAll()
                
                for address in addresses {
                    
                    if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                        
                        if (oneAddress["type"] as! String) == "P" {
                            
                            self.myAddressPayment.append([
                                "id" : oneAddress["id"] as! String,
                                "name" : oneAddress["name"] as! String,
                                "address" : oneAddress["address"] as! String,
                                "neighborhood" : oneAddress["neighborhood"] as! String,
                                "city" : oneAddress["city"] as! String,
                                "state" : oneAddress["state"] as! String,
                                "post_code" : oneAddress["post_code"] as! String,
                                "number" : oneAddress["number"] as! String,
                                "more" : oneAddress["more"] as! String,
                                "type" : oneAddress["type"] as! String,
                                "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                            ])
                            
                        } else if (oneAddress["type"] as! String) == "S" {
                            
                            self.myAddressShipping.append([
                                "id" : oneAddress["id"] as! String,
                                "name" : oneAddress["name"] as! String,
                                "address" : oneAddress["address"] as! String,
                                "neighborhood" : oneAddress["neighborhood"] as! String,
                                "city" : oneAddress["city"] as! String,
                                "state" : oneAddress["state"] as! String,
                                "post_code" : oneAddress["post_code"] as! String,
                                "number" : oneAddress["number"] as! String,
                                "more" : oneAddress["more"] as! String,
                                "type" : oneAddress["type"] as! String,
                                "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                            ])
                            
                        }
                        
                    }
                    
                }
                
                if !self.otherAddress.isEmpty {
                    
                    let alertController = UIAlertController(title: nil, message: "Toque para selecionar um endereço de cobrança existente ou registre um nova abaixo.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                if self.selectedItem == true && self.isPayment == true && self.otherAddress.isEmpty {
                    self.registerAddressButton.setTitle("Usar Endereço Selecionado", for: .normal)
                    self.registerAddressButton.alpha = 0.3
                    self.registerAddressButton.isEnabled = false
                }
                
                
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
        }
        
    }
    
    @IBAction func registerAddress ( _ sender : UIButton) {
        
        if self.selectedItem == true {
            if self.isPayment == true {
                
                if self.myAddressSelected.count > 0 {
                    
                    if !self.otherAddress.isEmpty {
                        
                        // =================================================
                        // ==== CADASTRANDO SOMENTE ENDERECO DE ENTREGA ====
                        // =================================================
                        
                        
                        RappleActivityIndicatorView.startAnimatingWithLabel("Salvando...", attributes: attributes)
                        
                        WishareAPI.addresses(name: self.otherAddress["name"]!, postCode: self.otherAddress["post_code"]!, address: self.otherAddress["address"]!, number: self.otherAddress["number"]!, more: self.otherAddress["more"]!, city: self.otherAddress["city"]!, state: self.otherAddress["state"]!, country: "Brasil", type: "S", neighborhood: self.otherAddress["neighborhood"]!, recipientName: self.otherAddress["recipent_name"]!, { (validate : Bool, error : Error?) in
                            
                            if error == nil {
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                var containsAddressOrderViewController : Bool = false
                                
                                for viewController in self.navigationController!.viewControllers {
                                    if viewController is AddressOrderViewController {
                                        containsAddressOrderViewController = true
                                    }
                                }
                                
                                if containsAddressOrderViewController == true {
                                    
                                    // ==============================
                                    // ===== CARRINHO DE COMPRA =====
                                    // ==============================
                                    
                                    for viewController in self.navigationController!.viewControllers {
                                        if viewController is AddressOrderViewController {
                                            let addressOrderViewController = viewController as! AddressOrderViewController
                                            addressOrderViewController.myAddressPaymentSelected = self.myAddressSelected
                                            addressOrderViewController.myAddressShippingSelected = [self.otherAddress]
                                            self.navigationController?.popToViewController(addressOrderViewController, animated: true)
                                        }
                                    }
                                    
                                    // ==============================
                                    // ==============================
                                    // ==============================
                                    
                                } else {
                                    
                                    // ==============================
                                    // ======= MEUS ENDERECOS =======
                                    // ==============================
                                    
                                    for viewController in self.navigationController!.viewControllers {
                                        if viewController is MyAddressViewController {
                                            let myAddressViewController = viewController as! MyAddressViewController
                                            myAddressViewController.selectedItem = false
                                            myAddressViewController.isPayment = false
                                            self.navigationController?.popToViewController(myAddressViewController, animated: true)
                                        }
                                    }

                                    // ==============================
                                    // ==============================
                                    // ==============================
                                    
                                }
                                
                                
                            }
                            
                        })
                        
                        // =================================================
                        // =================================================
                        // =================================================
                        
                    } else {
                        
                        for viewController in self.navigationController!.viewControllers {
                            if viewController is AddressOrderViewController {
                                
                                // ===============================================
                                // ======= RETORNAR O ENDERECO DE COBRANCA =======
                                // ===============================================
                                
                                let addressOrderViewController = viewController as! AddressOrderViewController
                                addressOrderViewController.myAddressPaymentSelected = self.myAddressSelected
                                self.navigationController?.popToViewController(addressOrderViewController, animated: true)
                                
                                // ===============================================
                                // ===============================================
                                // ===============================================
                                
                            }
                        }
                        
                    }
                    
                } else {
                    
                    if !self.otherAddress.isEmpty {
                        self.performSegue(withIdentifier: "editAddressSegue", sender: self.otherAddress)
                    } else {
                        self.performSegue(withIdentifier: "editAddressSegue", sender: [:])
                    }
                    
                    
                
                }
                
            } else {
                
                if self.myAddressSelected.count > 0 {
                    
                    for viewController in self.navigationController!.viewControllers {
                        if viewController is AddressOrderViewController {
                            
                            // ==============================================
                            // ======= RETORNAR O ENDERECO DE ENTREGA =======
                            // ==============================================
                            
                            let addressOrderViewController = viewController as! AddressOrderViewController
                            addressOrderViewController.myAddressShippingSelected = self.myAddressSelected
                            self.navigationController?.popToViewController(addressOrderViewController, animated: true)
                            
                            // ==============================================
                            // ==============================================
                            // ==============================================
                            
                        }
                    }
                    
                } else {
                
                    self.performSegue(withIdentifier: "editAddressSegue", sender: [:])
                
                }
            
            }
            

        } else {
            
           self.performSegue(withIdentifier: "editAddressSegue", sender: [:])
        
        }
    
    }

    override func viewDidAppear(_ animated: Bool) {
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                self.myAddressPayment.removeAll()
                self.myAddressShipping.removeAll()
                
                for address in addresses {
                    
                    if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                        
                        if (oneAddress["type"] as! String) == "P" {
                            
                            self.myAddressPayment.append([
                                "id" : oneAddress["id"] as! String,
                                "name" : oneAddress["name"] as! String,
                                "address" : oneAddress["address"] as! String,
                                "neighborhood" : oneAddress["neighborhood"] as! String,
                                "city" : oneAddress["city"] as! String,
                                "state" : oneAddress["state"] as! String,
                                "post_code" : oneAddress["post_code"] as! String,
                                "number" : oneAddress["number"] as! String,
                                "more" : oneAddress["more"] as! String,
                                "type" : oneAddress["type"] as! String,
                                "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                ])
                            
                        } else if (oneAddress["type"] as! String) == "S" {
                            
                            self.myAddressShipping.append([
                                "id" : oneAddress["id"] as! String,
                                "name" : oneAddress["name"] as! String,
                                "address" : oneAddress["address"] as! String,
                                "neighborhood" : oneAddress["neighborhood"] as! String,
                                "city" : oneAddress["city"] as! String,
                                "state" : oneAddress["state"] as! String,
                                "post_code" : oneAddress["post_code"] as! String,
                                "number" : oneAddress["number"] as! String,
                                "more" : oneAddress["more"] as! String,
                                "type" : oneAddress["type"] as! String,
                                "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                ])
                            
                        }
                        
                    }
                    
                }
                
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
                if self.myAddressPayment.count == 0 && self.myAddressShipping.count == 0 {
                    
                    let alertController = UIAlertController(title: "Nennhum endereço cadastrado. Gostaria de cadastrar um novo endereço?", message: nil, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                        self.performSegue(withIdentifier: "editAddressSegue", sender: [:])
                    })
                    let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: { ( _ ) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
            }
            
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editAddressSegue" {
            
            let myAddressEditableViewController = segue.destination as! MyAddressEditableViewController
            if !self.otherAddress.isEmpty {
                myAddressEditableViewController.otherAddress = sender as! [String : String]
            } else {
                myAddressEditableViewController.address = sender as! [String : String]
            }
            
        }
        
    }

}

extension MyAddressViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if self.selectedItem == true {
            
            if self.myAddressSelected.count > 0 {
                if self.isPayment {
                    
                    if self.myAddressSelected[0]["id"] != self.myAddressPayment[indexPath.row]["id"] {
                        self.myAddressSelected.removeAll()
                        self.myAddressSelected.append(self.myAddressPayment[indexPath.row])
                    } else {
                        
                        tableView.reloadData()
                        self.myAddressSelected.removeAll()
                        
                        if !self.otherAddress.isEmpty {
                            self.registerAddressButton.setTitle("Registar Novo Endereço", for: .normal)
                        } else {
                            self.registerAddressButton.alpha = 0.3
                            self.registerAddressButton.isEnabled = false
                        }
                        
                    }
                    
                } else {
                    
                    if self.myAddressSelected[0]["id"] != self.myAddressShipping[indexPath.row]["id"] {
                    
                        self.myAddressSelected.removeAll()
                        self.myAddressSelected.append(self.myAddressShipping[indexPath.row])
                    
                    } else {
                    
                        tableView.reloadData()
                        self.myAddressSelected.removeAll()
                        self.registerAddressButton.setTitle("Registar Novo Endereço", for: .normal)
                    
                    }
                
                }
                
            } else {
                
                if self.isPayment {
                    
                    self.myAddressSelected.append(self.myAddressPayment[indexPath.row])
                    
                    if self.otherAddress.isEmpty {
                        self.registerAddressButton.alpha = 1
                        self.registerAddressButton.isEnabled = true
                    }
                    
                } else {
                    
                    self.myAddressSelected.append(self.myAddressShipping[indexPath.row])
                }
                
                
                self.registerAddressButton.setTitle("Usar Endereço Selecionado", for: .normal)
            }
            
        } else {
            
            if indexPath.section == 0 {
                self.performSegue(withIdentifier: "editAddressSegue", sender: self.myAddressShipping[indexPath.row])
            } else {
                self.performSegue(withIdentifier: "editAddressSegue", sender: self.myAddressPayment[indexPath.row])
            }
            
        }

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Excluir") { ( _ , indexPath : IndexPath) in
            
            if self.selectedItem == true {
                
                if self.isPayment == true {
                    
                    RappleActivityIndicatorView.startAnimatingWithLabel("Removendo...", attributes: self.attributes)
                    
                    WishareAPI.removeAddress(idAddress: self.myAddressPayment[indexPath.row]["id"]!, { (error : Error?) in
                        
                        if error == nil {
                            
                            WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    
                                    self.myAddressPayment.removeAll()
                                    self.myAddressShipping.removeAll()
                                    
                                    for address in addresses {
                                        
                                        if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                                            
                                            if (oneAddress["type"] as! String) == "P" {
                                                
                                                self.myAddressPayment.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            } else if (oneAddress["type"] as! String) == "S" {
                                                
                                                self.myAddressShipping.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    self.tableView.reloadData()
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Removido!", completionTimeout: 1)
                                }
                            }
                            
                        } else {
                            
                            RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "Falhou", completionTimeout: 1)
                            
                        }
                        
                    })
                    
                } else {
                    
                    RappleActivityIndicatorView.startAnimatingWithLabel("Removendo...", attributes: self.attributes)
                    
                    WishareAPI.removeAddress(idAddress: self.myAddressShipping[indexPath.row]["id"]!, { (error : Error?) in
                        
                        if error == nil {
                            
                            WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    
                                    self.myAddressPayment.removeAll()
                                    self.myAddressShipping.removeAll()
                                    
                                    for address in addresses {
                                        
                                        if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                                            
                                            if (oneAddress["type"] as! String) == "P" {
                                                
                                                self.myAddressPayment.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            } else if (oneAddress["type"] as! String) == "S" {
                                                
                                                self.myAddressShipping.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    self.tableView.reloadData()
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Removido!", completionTimeout: 1)
                                }
                            }
                            
                        } else {
                            
                            RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "Falhou", completionTimeout: 1)
                            
                        }
                        
                    })
                    
                }
                
            } else {
                
                if indexPath.section == 0 {
                    
                    RappleActivityIndicatorView.startAnimatingWithLabel("Removendo...", attributes: self.attributes)
                    
                    WishareAPI.removeAddress(idAddress: self.myAddressShipping[indexPath.row]["id"]!, { (error : Error?) in
                        
                        if error == nil {
                            
                            WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    
                                    self.myAddressPayment.removeAll()
                                    self.myAddressShipping.removeAll()
                                    
                                    for address in addresses {
                                        
                                        if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                                            
                                            if (oneAddress["type"] as! String) == "P" {
                                                
                                                self.myAddressPayment.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            } else if (oneAddress["type"] as! String) == "S" {
                                                
                                                self.myAddressShipping.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    self.tableView.reloadData()
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Removido!", completionTimeout: 1)
                                }
                            }
                            
                        } else {
                            
                            RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "Falhou", completionTimeout: 1)
                            
                        }
                        
                    })
                    
                } else {
                    
                    RappleActivityIndicatorView.startAnimatingWithLabel("Removendo...", attributes: self.attributes)
                    
                    WishareAPI.removeAddress(idAddress: self.myAddressPayment[indexPath.row]["id"]!, { (error : Error?) in
                        
                        if error == nil {
                            
                            WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    
                                    self.myAddressPayment.removeAll()
                                    self.myAddressShipping.removeAll()
                                    
                                    for address in addresses {
                                        
                                        if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                                            
                                            if (oneAddress["type"] as! String) == "P" {
                                                
                                                self.myAddressPayment.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            } else if (oneAddress["type"] as! String) == "S" {
                                                
                                                self.myAddressShipping.append([
                                                    "id" : oneAddress["id"] as! String,
                                                    "name" : oneAddress["name"] as! String,
                                                    "address" : oneAddress["address"] as! String,
                                                    "neighborhood" : oneAddress["neighborhood"] as! String,
                                                    "city" : oneAddress["city"] as! String,
                                                    "state" : oneAddress["state"] as! String,
                                                    "post_code" : oneAddress["post_code"] as! String,
                                                    "number" : oneAddress["number"] as! String,
                                                    "more" : oneAddress["more"] as! String,
                                                    "type" : oneAddress["type"] as! String,
                                                    "recipient_name" : ((oneAddress["recipient_name"] is NSNull) ? "" : oneAddress["recipient_name"] as! String),
                                                    ])
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    self.tableView.reloadData()
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Removido!", completionTimeout: 1)
                                }
                            }
                            
                        } else {
                            
                            RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "Falhou", completionTimeout: 1)
                            
                        }
                        
                    })
                    
                }
                
            }
            
        }
        
        
        let editAction = UITableViewRowAction(style: .default, title: "Editar") { ( _ , indexPath : IndexPath) in
            
            if self.selectedItem == true {
                
                if self.isPayment == true {
                    self.performSegue(withIdentifier: "editAddressSegue", sender: self.myAddressPayment[indexPath.row])
                } else {
                    self.performSegue(withIdentifier: "editAddressSegue", sender: self.myAddressShipping[indexPath.row])
                }
                
            } else {
                
                if indexPath.section == 0 {
                    
                    self.performSegue(withIdentifier: "editAddressSegue", sender: self.myAddressShipping[indexPath.row])
                    
                } else {
                    
                    self.performSegue(withIdentifier: "editAddressSegue", sender: self.myAddressPayment[indexPath.row])
                    
                }
                
            }
            
            

            
        }
        
        editAction.backgroundColor = UIColor.lightGray
        
        if indexPath.section == 0 {
            if self.myAddressShipping.count == 1 {
                return [editAction]
            }
        } else {
            if self.myAddressPayment.count == 1 {
                return [editAction]
            }
        }
        
        return [editAction, deleteAction]
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension MyAddressViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.selectedItem == true {
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.selectedItem == true {
            if self.isPayment == true {
                return self.myAddressPayment.count
            } else {
                return self.myAddressShipping.count
            }
        }
        
        if section == 0 {
            return self.myAddressShipping.count
        }
        
        return self.myAddressPayment.count
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.selectedItem == true {
            if self.isPayment == true {
                return "ENDEREÇO DE COBRANÇA"
            } else {
                return "ENDEREÇO DE ENTREGA"
            }
        }
        
        if section == 0 {
            return "ENDEREÇO DE ENTREGA"
        }
        
        return "ENDEREÇO DE COBRANÇA"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! AddressTableViewCell
        
        
        if self.selectedItem == true {
            if self.isPayment == true {
                cell.typeImageView.image = UIImage(named: "price")
                cell.nameAddressLabel.text = self.myAddressPayment[indexPath.row]["name"]
                cell.addressLabel.text = self.myAddressPayment[indexPath.row]["address"]
            } else {
                cell.typeImageView.image = UIImage(named: "icons8-dog_house_filled")
                cell.nameAddressLabel.text = self.myAddressShipping[indexPath.row]["name"]
                cell.addressLabel.text = self.myAddressShipping[indexPath.row]["address"]
            }
        } else {
            if indexPath.section == 0 {
                // Endereco de Entrega
                cell.typeImageView.image = UIImage(named: "icons8-dog_house_filled")
                cell.nameAddressLabel.text = self.myAddressShipping[indexPath.row]["name"]
                cell.addressLabel.text = self.myAddressShipping[indexPath.row]["address"]
            } else {
                // Endereco de Cobranca
                cell.typeImageView.image = UIImage(named: "price")
                cell.nameAddressLabel.text = self.myAddressPayment[indexPath.row]["name"]
                cell.addressLabel.text = self.myAddressPayment[indexPath.row]["address"]
            }
            
        }
        
        return cell
        
    }
    
}
