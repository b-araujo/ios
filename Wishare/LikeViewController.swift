//
//  LikeViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class LikeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var post : Post!
    var users : [User] = []
    var loader : Loader!
    var manager : Manager!
    
    var myUserId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LikeUserTableViewCell", bundle: nil), forCellReuseIdentifier: "likesCell")
        
        self.navigationItem.title = "Curtidas"
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        let atributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando curtidas", attributes: atributes)
        
        WishareCoreData.getFirstUser { (user, error) in
            if error == nil {
                self.myUserId = user?.id
            }
        }
        
        WishareAPI.likeUsers(self.post.id!) { (result : [User], error : Error?) in
            
            if error == nil {
                
                RappleActivityIndicatorView.stopAnimation()
                self.users.append(contentsOf: result)
                self.tableView.reloadData()
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}

extension LikeViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension LikeViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "likesCell", for: indexPath) as! LikeUserTableViewCell
        
        cell.indexPathNow = indexPath
        cell.delegate = self
        cell.userNameButton.setTitle(self.users[indexPath.row].username, for: .normal)
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlPerfil : URL!
        
        if self.users[indexPath.row].isStore! {
            urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.users[indexPath.row].picture!)")
        } else {
            urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.users[indexPath.row].picture!)")
        }
        
        
        if self.users[indexPath.row].following! {
            cell.followUser.isHidden = true
        } else {
            cell.followUser.isHidden = false
        }
        
        if self.users[indexPath.row].id == myUserId {
            cell.followUser.isHidden = true
        }
        
        self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        return cell
        
    }
    
}

extension LikeViewController : LikeUserTableViewCellDelegate {
    
    func followLikeUser(sender: UIButton, cell: LikeUserTableViewCell) {
        print("SEGUIR USUARIO")
        
        if sender.title(for: .normal) == "Seguir" {
            
            sender.backgroundColor = UIColor.white
            sender.setTitle("Seguindo", for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            sender.layer.borderWidth = 2.0
            sender.layer.borderColor = UIColor.black.cgColor
            
            WishareAPI.followStore(self.users[cell.indexPathNow.row].id!, true, { (result : [String : AnyObject], error : Error?) in
                
                if let _ = result["success"] as? Bool {
                    self.tableView.reloadData()
                }
                
            })
            
            
        } else {
            
            let alertController = UIAlertController(title: "Deseja deixar de seguir \(self.users[cell.indexPathNow.row].name!)?", message: nil, preferredStyle: .actionSheet)
            let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                
                sender.backgroundColor = UIColor.black
                sender.setTitle("Seguir", for: .normal)
                sender.setTitleColor(UIColor.white, for: .normal)
                sender.layer.borderWidth = 0.0
                sender.layer.borderColor = UIColor.clear.cgColor
                WishareAPI.followStore(self.users[cell.indexPathNow.row].id!, false, { (result : [String : AnyObject], error : Error?) in
                    
                    if let _ = result["success"] as? Bool {
                        self.tableView.reloadData()
                    }
                    
                })
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func showUserLikeUser(cell: LikeUserTableViewCell) {
        print("ABRIR TELA")
        
        if users[cell.indexPathNow.row].isStore! {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
            pageStoreViewController.idStore = users[cell.indexPathNow.row].id
            
            self.navigationController?.pushViewController(pageStoreViewController, animated: true)
        } else {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
            pageUserViewController.idUser = self.users[cell.indexPathNow.row].id!
            
            if self.users[cell.indexPathNow.row].id == myUserId {
                pageUserViewController.isMyProfile = true
            } else {
                pageUserViewController.isMyProfile = false
            }
            
            self.navigationController?.pushViewController(pageUserViewController, animated: true)
        }
    }
    
}
