//
//  FollowStoresAndUsersViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/28/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class FollowStoresAndUsersViewController: UIViewController {

    @IBOutlet weak var storeCollectionView: UICollectionView!
    @IBOutlet weak var userCollectionView: UICollectionView!
    @IBOutlet weak var continuosButton: UIButton!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var stores : [Store] = []
    var users : [User] = []
    
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.storeCollectionView.delegate = self
        self.storeCollectionView.dataSource = self
        self.storeCollectionView.tag = 10
        self.userCollectionView.delegate = self
        self.userCollectionView.dataSource = self
        self.userCollectionView.tag = 11
        
        self.storeCollectionView.register(UINib(nibName: "FollowStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "followStoreCell")
        self.userCollectionView.register(UINib(nibName: "FollowStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "followStoreCell")
        self.continuosButton.addTarget(self, action: #selector(continuosAfter(_:)), for: .touchUpInside)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.storeByLimit(quantity: "10") { (stores : [Store], error : Error?) in
            if error == nil {
                
                WishareAPI.userByLimit(quantity: "10", { (users : [User], error : Error?) in
                    if error == nil {
                        
                        self.stores = stores
                        self.users = users
                        self.userCollectionView.reloadData()
                        self.storeCollectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                })
                
            }
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func continuosAfter( _ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension FollowStoresAndUsersViewController : UICollectionViewDelegate {
    
}

extension FollowStoresAndUsersViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 10 {
            return self.stores.count
        }
        return self.users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "followStoreCell", for: indexPath) as! FollowStoreCollectionViewCell
        
        cell.delegate = self
        
        if collectionView.tag == 10 {
            
            cell.tag = 1
            cell.indexPath = indexPath
            cell.accessibilityHint = self.stores[indexPath.row].id
            cell.nameStoreLabel.text = self.stores[indexPath.row].name
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/store-logo/thumb/")!
            let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.stores[indexPath.row].picture!)")
            cell.loadingActivityIndicator.startAnimating()
            
            self.manager.loadImage(with: url, into: cell.storeImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storeImageView.image = result.value?.circleMask
            }
            
            if self.stores[indexPath.row].following! {
                
                cell.followButton.backgroundColor = UIColor.white
                cell.followButton.setTitle("Seguindo", for: .normal)
                cell.followButton.setTitleColor(UIColor.black, for: .normal)
                cell.followButton.layer.borderWidth = 2.0
                cell.followButton.layer.borderColor = UIColor.black.cgColor
                
            } else {
                
                cell.followButton.backgroundColor = UIColor.black
                cell.followButton.setTitle("Seguir", for: .normal)
                cell.followButton.setTitleColor(UIColor.white, for: .normal)
                cell.followButton.layer.borderWidth = 0.0
                cell.followButton.layer.borderColor = UIColor.clear.cgColor
                
            }
            
        } else {
            
            cell.tag = 2
            cell.indexPath = indexPath
            cell.accessibilityHint = self.users[indexPath.row].id
            cell.nameStoreLabel.text = self.users[indexPath.row].username
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.users[indexPath.row].picture!)")
            
            cell.loadingActivityIndicator.startAnimating()
            
            self.manager.loadImage(with: url, into: cell.storeImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storeImageView.image = result.value?.circleMask
            }
            
            if self.users[indexPath.row].following! {
                
                cell.followButton.backgroundColor = UIColor.white
                cell.followButton.setTitle("Seguindo", for: .normal)
                cell.followButton.setTitleColor(UIColor.black, for: .normal)
                cell.followButton.layer.borderWidth = 2.0
                cell.followButton.layer.borderColor = UIColor.black.cgColor
                
            } else {
                
                cell.followButton.backgroundColor = UIColor.black
                cell.followButton.setTitle("Seguir", for: .normal)
                cell.followButton.setTitleColor(UIColor.white, for: .normal)
                cell.followButton.layer.borderWidth = 0.0
                cell.followButton.layer.borderColor = UIColor.clear.cgColor
                
            }
            
        }
        
        return cell
        
    }
    
}

extension FollowStoresAndUsersViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 10, height: self.storeCollectionView.bounds.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold = 100.0
        let contentOffset = scrollView.contentOffset.x;
        let contentHeight = scrollView.contentSize.width;
        
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.width;
        var triggerThreshold = Float((diffHeight - frameHeight))/Float(threshold);
        
        triggerThreshold = min(triggerThreshold, 0.0)
        let pullRatio = min(fabs(triggerThreshold),0.4);
        
        print("pullRation:\(pullRatio)")
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.x;
        let contentHeight = scrollView.contentSize.width;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.width;
        let pullHeight  = fabs(diffHeight - frameHeight);
        
        print("ContentOffset: \(contentOffset)")
        print("Content Height: \(contentHeight)")
        print("Frame Height: \(frameHeight)")
        print("pullHeight:\(pullHeight)");
        
        if pullHeight >= 0.0  && pullHeight <= 1.0 {
           
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                
                print("REFRESH APPENDING ....")
                WishareAPI.storeByLimit(quantity: "10", name: self.stores.last!.name, { (stores : [Store], error : Error?) in
                    if error == nil {
                        WishareAPI.userByLimit(quantity: "10", name: self.users.last!.username, { (users : [User], error : Error?) in
                            if error == nil {
                                
                                self.stores.append(contentsOf: stores)
                                self.users.append(contentsOf: users)
                                self.userCollectionView.reloadData()
                                self.storeCollectionView.reloadData()
                                
                            }
                        })
                    }
                })
                
            })

        }
        
    }

    
}

extension FollowStoresAndUsersViewController : FollowStoreCollectionViewCellDelegate {
    
    func followStore(cell: FollowStoreCollectionViewCell, sender: UIButton) {
        
        if sender.title(for: .normal) == "Seguir" {
            
            if let id = cell.accessibilityHint {
                
                if cell.tag == 1 {
                    
                    sender.backgroundColor = UIColor.white
                    sender.setTitle("Seguindo", for: .normal)
                    sender.setTitleColor(UIColor.black, for: .normal)
                    sender.layer.borderWidth = 2.0
                    sender.layer.borderColor = UIColor.black.cgColor
                    
                    self.stores[cell.indexPath!.row].following = true
                    
                    self.continuosButton.setTitle("Continuar", for: .normal)
                    self.continuosButton.setTitleColor(UIColor.black, for: .normal)
                    self.continuosButton.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                    self.continuosButton.layer.cornerRadius = 5
                    
                    WishareAPI.followStore(id, true, { (result : [String : AnyObject], error : Error?) in })
                    
                } else if cell.tag == 2 {
                    
                    sender.backgroundColor = UIColor.white
                    sender.setTitle("Seguindo", for: .normal)
                    sender.setTitleColor(UIColor.black, for: .normal)
                    sender.layer.borderWidth = 2.0
                    sender.layer.borderColor = UIColor.black.cgColor
                    
                    self.users[cell.indexPath!.row].following = true
                    
                    self.continuosButton.setTitle("Continuar", for: .normal)
                    self.continuosButton.setTitleColor(UIColor.black, for: .normal)
                    self.continuosButton.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                    self.continuosButton.layer.cornerRadius = 5
                    
                    WishareAPI.followUser(id, isFollow: true, { (error : Error?) in })
                    
                }
                
            }
            
        } else {
            
            if let id = cell.accessibilityHint {
                
                if cell.tag == 1 {
                    
                    sender.backgroundColor = UIColor.black
                    sender.setTitle("Seguir", for: .normal)
                    sender.setTitleColor(UIColor.white, for: .normal)
                    sender.layer.borderWidth = 0.0
                    sender.layer.borderColor = UIColor.clear.cgColor
                    
                    self.stores[cell.indexPath!.row].following = false
                    
                    WishareAPI.followStore(id, false, { (result : [String : AnyObject], error : Error?) in })
                    
                    
                } else if cell.tag == 2 {
                    
                    sender.backgroundColor = UIColor.black
                    sender.setTitle("Seguir", for: .normal)
                    sender.setTitleColor(UIColor.white, for: .normal)
                    sender.layer.borderWidth = 0.0
                    sender.layer.borderColor = UIColor.clear.cgColor
                    
                    self.users[cell.indexPath!.row].following = false
                    
                    WishareAPI.followUser(id, isFollow: false, { (error : Error?) in })
                    
                }
                
            }
            
        }
        
    }
    
    func showUserOrStore(_ cell: FollowStoreCollectionViewCell, _ indexPath: IndexPath) {
        
        if let id = cell.accessibilityHint {
            
            if cell.tag == 1 {
             
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
                pageStoreViewController.idStore = id
                
                self.navigationController?.pushViewController(pageStoreViewController, animated: true)

                
            } else {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                pageUserViewController.isMyProfile = false
                pageUserViewController.idUser = id
                
                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                
            }
            
        }
        
    }
    
}
