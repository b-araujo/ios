//
//  BuyShareCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class BuyShareCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var descriptionProductLabel: UILabel!
    @IBOutlet weak var priceProductLabel: UILabel!
    @IBOutlet weak var promoPriceProductLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        productImageView.image = nil
    }
    
}
