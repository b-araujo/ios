//
//  FollowPageStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes
import RappleProgressHUD
import Kingfisher

class FollowPageStoreViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    var store : Store!
    var loader : Loader!
    var manager : Manager!
    var usersResult = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "UserSearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userSearchCell")
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.storeGetFollowers(self.store.id!) { (users : [User], error : Error?) in
            
            if error == nil {
                
                self.usersResult = users
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension FollowPageStoreViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.usersResult[indexPath.row].id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
}

extension FollowPageStoreViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.usersResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userSearchCell", for: indexPath) as! UserSearchCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.usersResult[indexPath.row].picture!)")
        
        cell.userLabel.text = self.usersResult[indexPath.row].username
        
        self.manager.loadImage(with: url, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        if self.usersResult[indexPath.row].isProfileImageLocal {
           cell.backgroundUserImageView.image = UIImage(named: self.usersResult[indexPath.row].profile_image?.url ?? "background_perfil")
        } else {
            if let backgroundUrlString = self.usersResult[indexPath.row].profile_image?.urlThumb {
                cell.backgroundUserImageView.kf.setImage(with: URL(string: backgroundUrlString), placeholder: #imageLiteral(resourceName: "blur_image"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
        
        return cell
        
    }
    
}

extension FollowPageStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 1.25, height: ((self.view.bounds.width / 3) - 2.5))
    }
    
}
