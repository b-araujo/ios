//
//  AcquireWishViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin

class AcquireWishViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var wishPoints : WishPoints!
    var objectivesTarget : [Objective] = []
    
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "AcquireWishTableViewCell", bundle: nil), forCellReuseIdentifier: "acquireWishCell")
        
        for a in self.wishPoints.objectives {
            
            if a.isClosed! {
                self.objectivesTarget.append(a)
            }
            
        }
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


extension AcquireWishViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("openPopupWishPoints"), object: self.objectivesTarget[indexPath.row])
    }
    
}

extension AcquireWishViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objectivesTarget.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "acquireWishCell", for: indexPath) as! AcquireWishTableViewCell
        
        cell.nameAcquireLabel.text = self.objectivesTarget[indexPath.row].title
        cell.pointsAcquireLabel.text = "+ \(self.objectivesTarget[indexPath.row].points!) wpts"
        cell.dataAcquireLabel.text = self.objectivesTarget[indexPath.row].finalizedObjective?.convertToDate().formatterTimeBrazilian()
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/wish_objective/thumb/")!
        let url = urlBase.appendingPathComponent("files/wish_objective/thumb/\(self.objectivesTarget[indexPath.row].image!)")
        
        self.manager.loadImage(with: url, into: cell.acquireImageView) { ( result, _ ) in
            cell.acquireImageView.layer.cornerRadius = cell.acquireImageView.layer.bounds.width / 2
            cell.acquireImageView.image = result.value
        }
        
        return cell
        
    }
    
}
