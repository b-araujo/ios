//
//  HeaderShoppingCartTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class HeaderShoppingCartTableViewCell: UITableViewCell {

    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var descriptionPurchaseLabel: UILabel!
    @IBOutlet weak var moreOrLessImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
