//
//  ShowSubCategoriesStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import SwiftyAttributes
import ENMBadgedBarButtonItem

class ShowSubCategoriesStoreViewController: UIViewController {

    //showSubCategoriesStoreSegue
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchViewBottomConstraint : NSLayoutConstraint!
    
    var category : Category!
    var store : Store!
    var categories : [Category] = []
    var loader : Loader!
    var manager : Manager!
    var scrollDirectionDetermined : String = ""
    
    var barButton : BadgedBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.tag = 12
        self.collectionView.register(UINib(nibName: "CategoriesPageStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoriesPageStoreCell")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        
        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.productsByCategory(self.store.id!, self.category.id!) { (categories : [Category], error : Error?) in
            
            if error == nil {
                
                for cat in categories {
                    if cat.products.count > 0 {
                        self.categories.append(cat)
                    }
                }
                
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
                print("\(self.categories.count)")
                
            }
            
        }
        
        self.navigationItem.title = self.category.name
        
        self.searchBar.delegate = self
        self.searchBar.layer.borderWidth = 1.0
        self.searchBar.layer.borderColor = UIColor(red: 234/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
        
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func myCart() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "productsSubCategoriesStoreSegue" {
            
            let senderObject : [AnyObject] = sender as! [AnyObject]
            
            let productsCategoriesStoreViewController : ProductsCategoriesStoreViewController = segue.destination as! ProductsCategoriesStoreViewController
            productsCategoriesStoreViewController.category = senderObject[0] as? Category
            productsCategoriesStoreViewController.store = senderObject[1] as? Store
            
        }
        
        if segue.identifier == "recentSearchSegue" {
            
            let senderObject : [AnyObject] = sender as! [AnyObject]
            
            let recentSearchViewController = segue.destination as! RecentSearchViewController
            recentSearchViewController.category = senderObject[0] as? Category
            recentSearchViewController.store = senderObject[1] as? Store
            
        }
        
    }

}

extension ShowSubCategoriesStoreViewController : UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let translation = scrollView.panGestureRecognizer.translation(in: self.view)
        
        if translation.y > 0 {
            
            if self.scrollDirectionDetermined != "UP" {
                UIView.animate(withDuration: 0.4, animations: {
                    self.searchViewBottomConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
            }
            
            self.scrollDirectionDetermined = "UP"
            
            
        } else if translation.y < 0 {
            
            if self.scrollDirectionDetermined != "DOWN" {
                UIView.animate(withDuration: 0.4, animations: {
                    self.searchViewBottomConstraint.constant = -44
                    self.view.layoutIfNeeded()
                })
            }
            
            self.scrollDirectionDetermined = "DOWN"
            
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag != 12 {
            if let indexPathRow = collectionView.accessibilityHint {
                if self.categories[Int(indexPathRow)!].products.count > 0 {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                    productDetailViewController.idProduct = self.categories[Int(indexPathRow)!].products[indexPath.row].id!
                    productDetailViewController.isFirst = true
                    
                    self.navigationController?.pushViewController(productDetailViewController, animated: true)
                    
                }
            }
        }
        
    }
    
}

extension ShowSubCategoriesStoreViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 12 {
            return self.categories.count
        }
        
        if let row = collectionView.accessibilityHint {
            return self.categories[Int(row)!].products.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        if collectionView.tag == 12 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesPageStoreCell", for: indexPath) as! CategoriesPageStoreCollectionViewCell
            
            cell.categoryNameLabel.text = self.categories[indexPath.row].name
            cell.delegate = self
            cell.accessibilityHint = "\(indexPath.row)"
            
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.accessibilityHint = "\(indexPath.row)"
            cell.collectionView.register(UINib(nibName: "BuyShareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "buyShareCell")
            cell.collectionView.reloadData()
            
            return cell
            
        }
        
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyShareCell", for: indexPath) as! BuyShareCollectionViewCell
        
        if let indexPathRow = collectionView.accessibilityHint {
        
            cell.nameProductLabel.text = self.categories[Int(indexPathRow)!].products[indexPath.row].brand
            cell.descriptionProductLabel.text = self.categories[Int(indexPathRow)!].products[indexPath.row].name
        
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        
            let url = urlBase.appendingPathComponent("files/products/\(self.categories[Int(indexPathRow)!].products[indexPath.row].pId!)/thumb/\(self.categories[Int(indexPathRow)!].products[indexPath.row].picture!)")
        
            self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
                cell.productImageView.image = result.value
            }
        
            if self.categories[Int(indexPathRow)!].products[indexPath.row].promo_price == "" {
            
                let price = "R$ \(self.categories[Int(indexPathRow)!].products[indexPath.row].price!)".withFont(.boldSystemFont(ofSize: 14))
            
                cell.priceProductLabel.attributedText = price
                cell.promoPriceProductLabel.text = nil
            
            } else {
            
                let promo_price = "R$ \(self.categories[Int(indexPathRow)!].products[indexPath.row].price!)".withStrikethroughStyle(.styleSingle).withBaselineOffset(0).withFont(.systemFont(ofSize: 14)).withTextColor(UIColor.lightGray)
                let price = "R$ \(self.categories[Int(indexPathRow)!].products[indexPath.row].promo_price!)".withFont(.boldSystemFont(ofSize: 15)).withTextColor(UIColor.black)
                let por = " por ".withTextColor(UIColor.lightGray).withFont(.systemFont(ofSize: 14))
            
                cell.priceProductLabel.attributedText = promo_price + por
                cell.promoPriceProductLabel.attributedText = price
            
            }
            
        }
        
        return cell
        
    }
}

extension ShowSubCategoriesStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // Produto
        
        if collectionView.tag == 12 {
            if self.categories.count > 0 {
                let sizeCategories = (self.categories[indexPath.row].products.count % 2) + (self.categories[indexPath.row].products.count / 2)
                return CGSize(width: self.view.bounds.width, height: 45 + (((self.view.bounds.width / 2) + ((sizeCategories == 1) ? 100.0 : 100.0)) * CGFloat(sizeCategories)))
            }
        }
        
        
       
        if let indexPathRow = collectionView.accessibilityHint {
            if self.categories[Int(indexPathRow)!].products.count > 0 {
                let sizeCategories = (self.categories[Int(indexPathRow)!].products.count % 2) + (self.categories[Int(indexPathRow)!].products.count / 2)
                return CGSize(width: (self.view.bounds.width / 2) - 12.5, height: ((self.view.bounds.width / 2) - 15) + ((sizeCategories == 1) ? 102.5 : 100.0))
            }
        }
        
        
        return CGSize.zero

        
    }
    
    
}

extension ShowSubCategoriesStoreViewController : CategoriesPageStoreCollectionViewCellDelegate {
    
    func moreCategories(cell: CategoriesPageStoreCollectionViewCell) {
        
        if let row = cell.accessibilityHint {
            self.performSegue(withIdentifier: "productsSubCategoriesStoreSegue", sender: [self.categories[Int(row)!], self.store])
        }
        
    }
    
}

extension ShowSubCategoriesStoreViewController : UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.performSegue(withIdentifier: "recentSearchSegue", sender: [self.category, self.store])
        return false
    }
    
}
