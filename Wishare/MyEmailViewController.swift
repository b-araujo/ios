//
//  MyEmailViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SkyFloatingLabelTextField

class MyEmailViewController: UIViewController {

    @IBOutlet weak var emailAtualTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var novoEmailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var repitaEmailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var informationLabel: UILabel!
    
    @IBOutlet weak var newsletterSwitch: UISwitch!
    
    @IBOutlet weak var alterEmailButton: UIButton!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alterEmailButton.layer.cornerRadius = 5.0
        self.alterEmailButton.layer.masksToBounds = false
        self.alterEmailButton.layer.shadowColor = UIColor.black.cgColor
        self.alterEmailButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.alterEmailButton.layer.shadowRadius = 2.0
        self.alterEmailButton.layer.shadowOpacity = 0.24
        
        self.informationLabel.adjustsFontSizeToFitWidth = true
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if user != nil && error == nil {
                
                WishareAPI.getUser(idUser: user!.id!, { (user : User?, error : Error?) in
                
                    if error == nil {
                        
                        self.emailAtualTextField.text = user!.email
                        
                        if user!.optin! == "0" {
                            self.newsletterSwitch.isOn = false
                        } else {
                            self.newsletterSwitch.isOn = true
                        }
                        
                        RappleActivityIndicatorView.stopAnimation()
                        
                        
                    }
                    
                })
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func alterEmail ( _ sender : UIButton) {
        
        if self.novoEmailTextField.text == self.repitaEmailTextField.text && self.novoEmailTextField.text?.components(separatedBy: .whitespaces).joined() != "" && (self.novoEmailTextField.text?.contains("@"))! {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Alterando...", attributes: attributes)
            
            
            
            WishareAPI.updateEmailUser(newEmail: self.novoEmailTextField.text!, informationByEmail: (self.newsletterSwitch.isOn) ? "1" : "0" , { (error : Error?) in
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation()
                    
                    let alertController = UIAlertController(title: "Sucesso", message: "E-mail alterado com sucesso", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            })
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "E-mails não conferem", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        

        
    }
    

}
