//
//  PreviewPhotoViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/5/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Fusuma

class PreviewPhotoViewController: UIViewController {

    
    @IBOutlet weak var photoImage: UIImageView!
    var imagePhoto : UIImage?
    var fusuma : FusumaViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoImage.image = imagePhoto
        
        fusuma = FusumaViewController()
        fusuma.delegate = self
        fusumaTintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.present(self.fusuma, animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "previewSegue", sender: imagePhoto)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "previewSegue" {
    
            let photoViewController : PhotoViewController = segue.destination as! PhotoViewController
            photoViewController.photoUser = sender as? UIImage
            
        }
        
    }

}

extension PreviewPhotoViewController : FusumaDelegate {
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
    }
    
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "navPreviewID") as! UINavigationController
        
        if let viewcontroller = navigationController.viewControllers.first as? PreviewPhotoViewController {
            viewcontroller.imagePhoto = image
        }
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.set(rootViewController: navigationController, withTransition: transition)
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
    }
    
    func fusumaClosed() {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : TabBarViewController = storyboard.instantiateViewController(withIdentifier: "tabsID") as! TabBarViewController
        
        vc.modalTransitionStyle = .flipHorizontal
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController = vc
        
    }
    
}
