//
//  OrdersViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Pager

class OrdersViewController: PagerController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(openOrderDetail(notification:)), name: Notification.Name.init("openOrderDetail"), object: nil)
        
        let storyboard = UIStoryboard(name: "Setting", bundle: nil)
        
        let allOrders = storyboard.instantiateViewController(withIdentifier: "allOrdersID") as! AllOrdersViewController
        let openOrders = storyboard.instantiateViewController(withIdentifier: "openOrdersID") as! OpenOrdersViewController
        let ordersDelivered = storyboard.instantiateViewController(withIdentifier: "ordersDeliveredID") as! OrdersDeliveredViewController
        let canceledOrders = storyboard.instantiateViewController(withIdentifier: "canceledOrdersID") as! CanceledOrdersViewController
        
        self.dataSource = self
        self.setupPager(tabNames: ["Todos os Pedidos", "Pedidos em Aberto", "Pedidos Entregues", "Pedidos Cancelados"], tabControllers: [allOrders, openOrders, ordersDelivered, canceledOrders])
        self.customizeTab()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func customizeTab() {
        tabTopOffset = 0
        tabWidth = 160.0
        fixFormerTabsPositions = true
        animation = .during
        self.indicatorColor = UIColor.black
        self.contentViewBackgroundColor = UIColor.white
        self.tabsViewBackgroundColor = UIColor.white
        self.tabsTextColor = UIColor.black
        self.selectedTabTextColor = UIColor.black
        self.tabsTextFont = UIFont.systemFont(ofSize: 16.0)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "orderDetailSegue" {
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            let myOrderDetailViewController = segue.destination as! MyOrderDetailViewController
            myOrderDetailViewController.orderSelected = senderValue[0] as! [String : String]
            myOrderDetailViewController.itemsSelected = senderValue[1] as! [[String : String]]
            
        }
        
    }
    
    
    @objc func openOrderDetail(notification : Notification) {
        self.performSegue(withIdentifier: "orderDetailSegue", sender: notification.object)
    }

}

extension OrdersViewController : PagerDataSource {
    
}
