//
//  RegisterCardOrderViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import RappleProgressHUD
import Caishen

class RegisterCardOrderViewController: UIViewController {

    
    @IBOutlet weak var numberCardTextField: NumberInputTextField!
    @IBOutlet weak var monthInputTextField: MonthInputTextField!
    @IBOutlet weak var yearInputTextField: YearInputTextField!
    @IBOutlet weak var cvvTextField: CVCInputTextField!
    @IBOutlet weak var cardImage: UIImageView!
    
    @IBOutlet weak var nameCardTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var numberParcelTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBOutlet weak var continuosButton : UIButton!
    
    @IBOutlet weak var couponCodeTextField: SkyFloatingLabelTextField!
    
    var totalPrice : Double = 0.0
    var quantityParcel : Int = 0
    
    var store : Store!
    
    var monthAndYear : String = ""
    var bankCardNumber : String = ""
    var flag : String = ""
    
    @IBOutlet weak var scrollView: UIScrollView!
    var bottomConstraint : NSLayoutConstraint?
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bottomConstraint = NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillHide, object: nil)
        
        self.continuosButton.layer.cornerRadius = 5.0
        self.continuosButton.layer.masksToBounds = false
        self.continuosButton.layer.shadowColor = UIColor.black.cgColor
        self.continuosButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.continuosButton.layer.shadowRadius = 2.0
        self.continuosButton.layer.shadowOpacity = 0.24
        
        numberCardTextField.numberInputTextFieldDelegate = self
        monthInputTextField.cardInfoTextFieldDelegate = self
        yearInputTextField.cardInfoTextFieldDelegate = self
        cvvTextField.cardInfoTextFieldDelegate = self
        couponCodeTextField.delegate = self
        
        self.numberParcelTextField.tag = 10
        self.numberParcelTextField.delegate = self
        
        monthInputTextField.deleteBackwardCallback = { _ in self.numberCardTextField.becomeFirstResponder() }
        yearInputTextField.deleteBackwardCallback = { _ in self.monthInputTextField.becomeFirstResponder() }
        cvvTextField.deleteBackwardCallback = { _ in self.yearInputTextField.becomeFirstResponder() }
        
        self.navigationController?.navigationBar.backItem?.title = "    "
        
        for product in self.store.products {
            
            totalPrice += (product.select_quatity! as NSString).doubleValue * ((product.promo_price! == "") ? (product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)
        }
        
        self.totalPriceLabel.text = "\(self.totalPrice.asLocaleCurrency)"
        
        parcelCalculator()
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }
    
    fileprivate func parcelCalculator() {
        if self.store.advance_commission! == "0" {
            
            let parcelMinimal : Double = 50.0
            var parcelIsValidate : Bool = false
            
            for i in (1...6).reversed() {
                
                var parcel : Double = 0.0
                parcel = totalPrice / Double(i)
                
                if parcel >= parcelMinimal {
                    self.quantityParcel = i
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
                
            }
            
            if parcelIsValidate == false {
                self.quantityParcel = 1
            }
            
        } else {
            
            let parcelMinimal : Double = 100.0
            var parcelIsValidate : Bool = false
            
            for i in (1...6).reversed() {
                
                var parcel : Double = 0.0
                parcel = totalPrice / Double(i)
                
                if parcel >= parcelMinimal {
                    self.quantityParcel = i
                    parcelIsValidate = true
                    break
                } else {
                    continue
                }
                
            }
            
            if parcelIsValidate == false {
                self.quantityParcel = 1
            }
            
        }
        
        
        
        let frete : Double = (self.store.order.freight_value!.components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue
        let totalFinal = totalPrice + frete
        
        self.numberParcelTextField.text = "1x de \((totalFinal / 1.0).asLocaleCurrency)"
        self.totalPriceLabel.text = "\(totalFinal.asLocaleCurrency)"
        self.store.order.parcel_number = "1"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "finishOrderSegue" {
            let finishOrderViewController = segue.destination as! FinishOrderViewController
            finishOrderViewController.store = sender as! Store
        }
        
    }
    
    @IBAction func applyCouponCode(_ sender: UIButton) {
        WishareAPI.applyCoupon(coupon: self.couponCodeTextField.text, addressId: store.order.id_address, idStore: store.id, freightValue: store.order.freight_value, service: store.order.id_address, products: store.order.products) { (valid: Bool, products: [ProductUpdate], total: String, error: Error?) in
            if error == nil {
                if valid {
                    if total.doubleValue > 0 {
                        self.totalPrice = total.doubleValue
                        
                        let freight = self.store.order.freight_value?.doubleValue
                        
                        self.totalPrice += freight ?? 0
                        self.store.order.total = total.doubleValue
                        self.totalPriceLabel.text = "\(self.totalPrice.asLocaleCurrency)"
                        self.store.order.total = total.doubleValue
                        
                        for prod in products {
                            if let index = self.store.order.products.index(where: {$0.id == prod.id}) {
                                self.store.order.products[index].price = prod.price
                            }
                        }
                        self.store.order.cupon_code = self.couponCodeTextField.text ?? ""
                        
                        self.parcelCalculator()
                        
                        let alert = UIAlertController(title: "A aplicação do cupom implica na alteração do valor que pode afetar a sua forma de pagamento, número de parcelas e/ou valor do frete.", message: nil, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                        
                    }
                    
                    
                } else {
                    self.couponCodeTextField.errorMessage = "Cupom inválido"
                }
            } else {
                self.couponCodeTextField.errorMessage = "Cupom inválido"
            }
        }
    }
    
    @IBAction func continuosPage ( _ sender : UIButton) {
        
        var countError = 0
        
        if (self.nameCardTextField.text?.isEmpty)! {
            
            self.nameCardTextField.errorMessage = "Nome Obrigatorio"
            countError += 1
            
        } else {
            
            self.nameCardTextField.errorMessage = nil
            
        }
        
        if (self.numberParcelTextField.text?.isEmpty)! {
            
            self.numberParcelTextField.errorMessage = "Quantidade Parcelas Obrigatorio"
            countError += 1
            
        } else {
            
            self.numberParcelTextField.errorMessage = nil
            
        }
        
        
        
        if countError == 0 && self.monthAndYear != "" && self.flag != "" && self.flag != "Unknown" && self.bankCardNumber != "" {
            
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Cadastrando...", attributes: attributes)
            
            WishareAPI.userCards(number: self.bankCardNumber, name: self.nameCardTextField.text!, valid: self.monthAndYear, flag: self.flag, { (validate : Bool, error : Error?) in
                
                
                WishareAPI.getCards { (cards : [[String : AnyObject]], error : Error?) in
                    if error == nil {
                        
                        if let card : [String : AnyObject] = cards[0]["UsersCard"] as? [String : AnyObject] {
                            
                            self.store.order.id_card = card["id"] as? String
                            self.store.order.flag_card = (card["flag"] as? String)?.uppercased()
                            self.store.order.cvv = self.cvvTextField.text
                            
                            RappleActivityIndicatorView.stopAnimation()
                            self.performSegue(withIdentifier: "finishOrderSegue", sender: self.store)
                        }
                        
                    }
                }
                
            })
            
            
        }
        
    }
    
    @objc func handleKeyBoardNotification(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                
                //let indexPath = IndexPath(item: (self.coments.count != 0) ? self.coments.count - 1 : 0, section: 0)
                //self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
            })
            
        }
        
    }
    
}

extension RegisterCardOrderViewController : UITextFieldDelegate {
    
    fileprivate func selectParcels(_ textField: UITextField) {
        textField.resignFirstResponder()
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let frete : Double = (self.store.order.freight_value!.components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue
        let totalFinal = totalPrice + frete
        
        for i in 1...self.quantityParcel {
            
            let parcelAction = UIAlertAction(title: "\(i)x de \((totalFinal / Double(i)).asLocaleCurrency)", style: .default, handler: { ( _ ) in
                
                self.numberParcelTextField.text = "\(i)x de \((totalFinal / Double(i)).asLocaleCurrency)"
                self.store.order.parcel_number = "\(i)"
            })
            
            alertController.addAction(parcelAction)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.hideKeyboardWhenTappedAround()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == numberParcelTextField {
            selectParcels(textField)
            return false
        }
        
        return true
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension RegisterCardOrderViewController : NumberInputTextFieldDelegate, CardInfoTextFieldDelegate {
    
    var card : Card? {
        
        let number = numberCardTextField.cardNumber
        let cvc = CVC(rawValue: cvvTextField.text ?? "")
        let expiry = Expiry(month: monthInputTextField.text ?? "", year: yearInputTextField.text ?? "") ?? Expiry.invalid
        
        let cardType = numberCardTextField.cardTypeRegister.cardType(for: numberCardTextField.cardNumber)
        
        if cardType.validate(cvc: cvc).union(cardType.validate(expiry: expiry)).union(cardType.validate(number: number)) == .Valid {
            return Card(number: number, cvc: cvc, expiry: expiry)
        } else {
            return nil
        }
    }
    
    func numberInputTextFieldDidComplete(_ numberInputTextField: NumberInputTextField) {
        
        cvvTextField.cardType = numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber)
        print("Card number: \(numberInputTextField.cardNumber)")
        
        print(card ?? "Ainda nao criado")
        monthInputTextField.becomeFirstResponder()
        
    }
    
    func numberInputTextFieldDidChangeText(_ numberInputTextField: NumberInputTextField) {
        
        self.flag = numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber).name
        cardImage.image = UIImage(named: numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber).name, in: Bundle(for: CardTextField.self), compatibleWith: nil)
        
    }
    
    func textField(_ textField: UITextField, didEnterValidInfo: String) {
        
        switch textField {
        case is MonthInputTextField:
            print("Month: \(didEnterValidInfo)")
            yearInputTextField.becomeFirstResponder()
        case is YearInputTextField:
            print("Year: \(didEnterValidInfo)")
            cvvTextField.becomeFirstResponder()
        case is CVCInputTextField:
            print("CVC: \(didEnterValidInfo)")
            self.bankCardNumber = card!.bankCardNumber.description
            self.monthAndYear = card!.expiryDate.description
        default:
            break
        }
        
        print(card ?? "Ainda nao criado")
        
        
        
    }
    
    func textField(_ textField: UITextField, didEnterPartiallyValidInfo: String) {
        
    }
    
    func textField(_ textField: UITextField, didEnterOverflowInfo overFlowDigits: String) {
        
    }
}
