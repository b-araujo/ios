//
//  ProductCartCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/4/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class ProductCartCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var sizeProductLabel: UILabel!
    @IBOutlet weak var colorProductLabel: UILabel!
    @IBOutlet weak var quantityProductLabel: UILabel!
    @IBOutlet weak var priceFinalProductLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
