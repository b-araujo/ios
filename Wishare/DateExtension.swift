//
//  DateExtension.swift
//  Wishare
//
//  Created by Wishare iMac on 4/25/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    func timeAgoDisplay() -> String {
        
        let secondsAgo = Int(Date().timeIntervalSince(self))
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        
        
        if secondsAgo < minute {
            return "Há \(secondsAgo) segundos"
        } else if secondsAgo < hour {
            return "Há \(secondsAgo / minute) minutos"
        } else if secondsAgo < day {
            return "Há \(secondsAgo / hour) horas"
        } else if secondsAgo < week {
            return "Há \(secondsAgo / day) dias"
        }
        
        return formatterTimeAgo(date: self)
        
    }
    
    
    fileprivate func formatterTimeAgo(date : Date) -> String {
        
        let labelMonth : [String] = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
        
        let calendar : Calendar = Calendar.current
        let nowYear = calendar.component(.year, from: Date())
        
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        
        if year != nowYear {
            return "\(day) de \(labelMonth[month - 1]) de \(year)"
        }
        
        return "\(day) de \(labelMonth[month - 1])"
        
    }
    
    func formatterTimeBrazilian() -> String {
        
        let calendar : Calendar = Calendar.current
        
        let day = calendar.component(.day, from: self)
        let month = calendar.component(.month, from: self)
        let year = calendar.component(.year, from: self)
        let hour = calendar.component(.hour, from: self)
        let minutes = calendar.component(.minute, from: self)
        let seconds = calendar.component(.second, from: self)
        
        let dayFormatter = (day <= 9) ? "0\(day)" : "\(day)"
        let monthFormatter = (month <= 9) ? "0\(month)" : "\(month)"
        let hourFormatter = (hour <= 9) ? "0\(hour)" : "\(hour)"
        let minutesFormatter = (minutes <= 9) ? "0\(minutes)" : "\(minutes)"
        let secondsFormatter = (seconds <= 9) ? "0\(seconds)" : "\(seconds)"
        
        return "\(dayFormatter)/\(monthFormatter)/\(year) - \(hourFormatter):\(minutesFormatter):\(secondsFormatter)"
        
    }
    
}

extension String {
    
    func convertToDate() -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "Y-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale.current
        
        print(self)
      
        
        let dateObj = dateFormatter.date(from: self)
        let dateNow = Date()
        
        if let date = dateObj {
            print(date)
            return date
        }
        
        return dateNow
        
    }
    
    
    func printableAscii() -> String {
        let data = self.data(using: String.Encoding.ascii, allowLossyConversion: true)
        return NSString(data: data!, encoding: String.Encoding.ascii.rawValue)! as String
    }
    
    
}


extension Float {
    
    var asLocaleCurrency:String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.maximumFractionDigits = 2
        return formatter.string(for: self)!
    
    }
    
}

extension Double {
    
    var asLocaleCurrency:String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.maximumFractionDigits = 2
        return formatter.string(for: self)!
        
    }
    
}

extension UIAlertAction {
    
    convenience init(title: String?, style: UIAlertActionStyle, image: UIImage, handler: ((UIAlertAction) -> Void)? = nil) {
        self.init(title: title, style: style, handler: handler)
        self.actionImage = image
    }
    
    convenience init?(title: String?, style: UIAlertActionStyle, imageNamed imageName: String, handler: ((UIAlertAction) -> Void)? = nil) {
        if let image = UIImage(named: imageName) {
            self.init(title: title, style: style, image: image, handler: handler)
        } else {
            return nil
        }
    }
    
    var actionImage: UIImage {
        get {
            return self.value(forKey: "image") as? UIImage ?? UIImage()
        }
        set(image) {
            self.setValue(image, forKey: "image")
        }
    }
    
}

class TriangleView : UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.closePath()
        
        context.setFillColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1)
        context.fillPath()
    }
}

func heightLabelWithText( _ width : CGFloat, _ text : String) -> CGFloat {
    
    let font : UIFont = UIFont.systemFont(ofSize: 15)
    let label : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = .byTruncatingTail
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
    
}

func validateCpf( _ value : String ) -> Bool {
    
    // TRUE - Passou na validacao
    // FALSE - Nao passou
    
    if value.characters.count < 11 {
        return false
    }
    
    if value == "11111111111" || value == "22222222222" || value == "33333333333" || value == "44444444444" || value == "55555555555" || value == "66666666666" || value == "77777777777" || value == "88888888888" || value == "99999999999" {
        return false
    }
    
    let cpf = value
    
    if value.contains(".") || value.contains("-") {
        return false
    }
    
    var valueTotal = 0
    
    let primaryVerification = (cpf as NSString).substring(with: NSRange(location: 9, length: 1))
    let secondVerification = (cpf as NSString).substring(with: NSRange(location: 10, length: 1))
    
    print(primaryVerification)
    print(secondVerification)
    
    var firstValidate = (cpf as NSString).substring(with: NSRange(location: 0, length: 9))
    var secondValidate = (cpf as NSString).substring(with: NSRange(location: 0, length: 10))
    
    for (key, word) in firstValidate.characters.enumerated() {
        valueTotal += Int(word.description)! * (10 - key)
    }
    
    if ((valueTotal * 10) % 11) != Int(primaryVerification)! {
        return false
    }
    
    valueTotal = 0
    
    for (key, word) in secondValidate.characters.enumerated() {
        valueTotal += Int(word.description)! * (11 - key)
    }
    
    if ((valueTotal * 10) % 11) != Int(secondVerification)! {
        return false
    }
    
    
    return true
    
}
