//
//  UserActionTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class UserActionTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameUserButton: UIButton!
    @IBOutlet weak var comentLabel: ActiveLabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    
    var indexPath : IndexPath? = nil
    var delegate : UserActionDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureRecognizerUserImageView = UITapGestureRecognizer(target: self, action: #selector(tapUserImageView))
        self.userImageView.isUserInteractionEnabled = true
        self.userImageView.addGestureRecognizer(tapGestureRecognizerUserImageView)
        
        self.nameUserButton.isUserInteractionEnabled = true
        self.nameUserButton.addTarget(self, action: #selector(tapNameUserButton(_:)), for: .touchUpInside)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func tapUserImageView () {
        self.delegate?.openUserProfile(cell: self, indexPath: indexPath!)
    }
    
    @objc func tapNameUserButton ( _ sender : UIButton) {
        self.delegate?.openUserProfile(cell: self, indexPath: indexPath!)
    }
    
}

protocol UserActionDelegate {
    func openUserProfile(cell : UserActionTableViewCell, indexPath : IndexPath)
}
