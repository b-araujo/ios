//
//  MyOrderDetailViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Savory
import Alamofire
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes
import SafariServices
import MessageUI
import WebKit
import QuickLook

class MyOrderDetailViewController: UIViewController, QLPreviewControllerDataSource {
    

    // Border Views
    @IBOutlet weak var informationStoreView: UIView!
    @IBOutlet weak var orderProductView: UIView!
    @IBOutlet weak var goShippingView: UIView!
    @IBOutlet weak var trackingView: UIView!
    
    // Progress Order
    @IBOutlet weak var firstStepView: UIView!
    @IBOutlet weak var twoStepView: UIView!
    @IBOutlet weak var threeStepView: UIView!
    @IBOutlet weak var fourStepView: UIView!
    @IBOutlet weak var fiveStepView: UIView!
    
    @IBOutlet weak var pagerView: UICollectionView!
    
    @IBOutlet weak var fiscalButton: UIButton!
    @IBOutlet weak var tradeButton: UIButton!
    
    @IBOutlet weak var savoryView: SavoryView!
    @IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var trackButton: UIButton!
    
    
    // Editable
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var purshaseDateLabel: UILabel!
    
    @IBOutlet weak var oneBarView: UIView!
    @IBOutlet weak var twoBarView: UIView!
    @IBOutlet weak var threeBarView: UIView!
    @IBOutlet weak var fourBarView: UIView!
    @IBOutlet weak var fiveBarView: UIView!
    @IBOutlet weak var sixBarView: UIView!
    @IBOutlet weak var sevenBarView: UIView!
    @IBOutlet weak var eightBarView: UIView!
    
    @IBOutlet weak var errorTwoStepLabel: UILabel!
    @IBOutlet weak var twoStepLabel: UILabel!
    
    @IBOutlet weak var errorThreeStepLabel: UILabel!
    @IBOutlet weak var threeStepLabel: UILabel!
    
    @IBOutlet weak var oneImageView: UIImageView!
    @IBOutlet weak var twoImageView: UIImageView!
    @IBOutlet weak var threeImageView: UIImageView!
    @IBOutlet weak var fourImageView: UIImageView!
    @IBOutlet weak var fiveImageView: UIImageView!
    
    var titlesForHeader : [String] = []
    var imagesForHeader : [String] = ["icons8-bank_cards_filled", "icons8-cheap_2_filled", "icons8-dog_house_filled"]
    
    var orderSelected : [String : String] = [:]
    var itemsSelected : [[String : String]] = []
    
    var loader : Loader!
    var manager : Manager!
    
    var fileURL = URL(string: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.heightConstraint.constant = 130.0 * CGFloat(self.itemsSelected.count)
        
        self.errorTwoStepLabel.isHidden = true
        self.errorThreeStepLabel.isHidden = true
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "OrderProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "orderProductCell")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.navigationItem.title = "Pedido: \(self.orderSelected["id"]!)"
        self.nameStoreLabel.text = self.orderSelected["store_name"]
        self.purshaseDateLabel.text = self.orderSelected["purshase_date"]!.convertToDate().formatterTimeBrazilian()
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.orderSelected["store_picture"]!)")
        
        self.manager.loadImage(with: urlPerfil, into: self.storeImageView) { ( result, _ ) in
            self.storeImageView.image = result.value?.circleMask
        }
        
        self.titlesForHeader.append(self.orderSelected["flag"]!.uppercased())
        self.titlesForHeader.append("\(((self.orderSelected["tot"]!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue + (self.orderSelected["value_freight"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue).asLocaleCurrency) - \(self.orderSelected["items"]!) itens")
        self.titlesForHeader.append(self.orderSelected["name_address"]!)
        
        self.informationStoreView.layer.cornerRadius = 5.0
        self.informationStoreView.layer.borderWidth = 1.0
        self.informationStoreView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
        self.orderProductView.layer.cornerRadius = 5.0
        self.orderProductView.layer.borderWidth = 1.0
        self.orderProductView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
        self.goShippingView.layer.cornerRadius = 5.0
        self.goShippingView.layer.borderWidth = 1.0
        self.goShippingView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
        self.trackingView.layer.cornerRadius = 5.0
        self.trackingView.layer.borderWidth = 1.0
        self.trackingView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
        if (self.orderSelected["status_id"]! as NSString).integerValue == 1 {
        
            self.oneImageView.alpha = 1
            self.oneBarView.backgroundColor = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: 1)
            self.twoBarView.backgroundColor = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: 1)
            
            self.firstStepView.layer.cornerRadius = 10.0
            self.firstStepView.layer.borderWidth = 1.0
            self.firstStepView.layer.borderColor = UIColor.darkGray.cgColor
            self.firstStepView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
            self.twoStepView.layer.cornerRadius = 10.0
            self.twoStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
            self.twoStepView.layer.borderWidth = 1.0
            self.twoStepView.backgroundColor = UIColor.white
            
            self.threeStepView.layer.cornerRadius = 10.0
            self.threeStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
            self.threeStepView.layer.borderWidth = 1.0
            self.threeStepView.backgroundColor = UIColor.white
            
            self.fourStepView.layer.cornerRadius = 10.0
            self.fourStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
            self.fourStepView.layer.borderWidth = 1.0
            self.fourStepView.backgroundColor = UIColor.white
            
            self.fiveStepView.layer.cornerRadius = 10.0
            self.fiveStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
            self.fiveStepView.layer.borderWidth = 1.0
            self.fiveStepView.backgroundColor = UIColor.white
            
            
        } else if (self.orderSelected["status_id"]! as NSString).integerValue == 2 || (self.orderSelected["status_id"]! as NSString).integerValue == 5 {
            
            self.twoImageView.alpha = 1
            self.oneBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.twoBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
            if (self.orderSelected["status_id"]! as NSString).integerValue == 5 {
                
                self.twoStepLabel.text = "Pagamento Reprovado"
                self.errorTwoStepLabel.isHidden = false
                
                self.firstStepView.layer.cornerRadius = 10.0
                self.firstStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                self.firstStepView.layer.borderWidth = 1.0
                self.firstStepView.backgroundColor = UIColor.white
                
                self.twoStepView.layer.cornerRadius = 10.0
                self.twoStepView.layer.borderWidth = 1.0
                self.twoStepView.layer.borderColor = UIColor.red.cgColor
                self.twoStepView.backgroundColor = UIColor.white
                
                self.threeStepView.layer.cornerRadius = 10.0
                self.threeStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.threeStepView.layer.borderWidth = 1.0
                self.threeStepView.backgroundColor = UIColor.white
                
                self.fourStepView.layer.cornerRadius = 10.0
                self.fourStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fourStepView.layer.borderWidth = 1.0
                self.fourStepView.backgroundColor = UIColor.white
                
                self.fiveStepView.layer.cornerRadius = 10.0
                self.fiveStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fiveStepView.layer.borderWidth = 1.0
                self.fiveStepView.backgroundColor = UIColor.white
                
            } else {
                
                self.firstStepView.layer.cornerRadius = 10.0
                self.firstStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                self.firstStepView.layer.borderWidth = 1.0
                self.firstStepView.backgroundColor = UIColor.white
                
                self.twoStepView.layer.cornerRadius = 10.0
                self.twoStepView.layer.borderWidth = 1.0
                self.twoStepView.layer.borderColor = UIColor.darkGray.cgColor
                self.twoStepView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                
                self.threeStepView.layer.cornerRadius = 10.0
                self.threeStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.threeStepView.layer.borderWidth = 1.0
                self.threeStepView.backgroundColor = UIColor.white
                
                self.fourStepView.layer.cornerRadius = 10.0
                self.fourStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fourStepView.layer.borderWidth = 1.0
                self.fourStepView.backgroundColor = UIColor.white
                
                self.fiveStepView.layer.cornerRadius = 10.0
                self.fiveStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fiveStepView.layer.borderWidth = 1.0
                self.fiveStepView.backgroundColor = UIColor.white
                
            }
            
        } else if (self.orderSelected["status_id"]! as NSString).integerValue == 17 || (self.orderSelected["status_id"]! as NSString).integerValue == 26 {
            
            self.threeImageView.alpha = 1
            self.oneBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.twoBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.threeBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.fourBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
            if (self.orderSelected["status_id"]! as NSString).integerValue == 26 {
                
                self.threeStepLabel.text = "Pedido Cancelado"
                self.errorThreeStepLabel.isHidden = false
                
                self.firstStepView.layer.cornerRadius = 10.0
                self.firstStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                self.firstStepView.layer.borderWidth = 1.0
                self.firstStepView.backgroundColor = UIColor.white
                
                self.twoStepView.layer.cornerRadius = 10.0
                self.twoStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                self.twoStepView.layer.borderWidth = 1.0
                self.twoStepView.backgroundColor = UIColor.white
                
                self.threeStepView.layer.cornerRadius = 10.0
                self.threeStepView.layer.borderWidth = 1.0
                self.threeStepView.layer.borderColor = UIColor.red.cgColor
                self.threeStepView.backgroundColor = UIColor.white
                
                self.fourStepView.layer.cornerRadius = 10.0
                self.fourStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fourStepView.layer.borderWidth = 1.0
                self.fourStepView.backgroundColor = UIColor.white
                
                self.fiveStepView.layer.cornerRadius = 10.0
                self.fiveStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fiveStepView.layer.borderWidth = 1.0
                self.fiveStepView.backgroundColor = UIColor.white
                
            } else {
                
                self.firstStepView.layer.cornerRadius = 10.0
                self.firstStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                self.firstStepView.layer.borderWidth = 1.0
                self.firstStepView.backgroundColor = UIColor.white
                
                self.twoStepView.layer.cornerRadius = 10.0
                self.twoStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                self.twoStepView.layer.borderWidth = 1.0
                self.twoStepView.backgroundColor = UIColor.white
                
                self.threeStepView.layer.cornerRadius = 10.0
                self.threeStepView.layer.borderWidth = 1.0
                self.threeStepView.layer.borderColor = UIColor.darkGray.cgColor
                self.threeStepView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                
                self.fourStepView.layer.cornerRadius = 10.0
                self.fourStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fourStepView.layer.borderWidth = 1.0
                self.fourStepView.backgroundColor = UIColor.white
                
                self.fiveStepView.layer.cornerRadius = 10.0
                self.fiveStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
                self.fiveStepView.layer.borderWidth = 1.0
                self.fiveStepView.backgroundColor = UIColor.white
            
            }
            

        } else if (self.orderSelected["status_id"]! as NSString).integerValue == 18 {
            
            self.fourImageView.alpha = 1
            self.trackButton.isHidden = false
            
            self.oneBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.twoBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.threeBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.fourBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.fiveBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.sixBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
            self.firstStepView.layer.cornerRadius = 10.0
            self.firstStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.firstStepView.layer.borderWidth = 1.0
            self.firstStepView.backgroundColor = UIColor.white
            
            self.twoStepView.layer.cornerRadius = 10.0
            self.twoStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.twoStepView.layer.borderWidth = 1.0
            self.twoStepView.backgroundColor = UIColor.white
            
            self.threeStepView.layer.cornerRadius = 10.0
            self.threeStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.threeStepView.layer.borderWidth = 1.0
            self.threeStepView.backgroundColor = UIColor.white
            
            self.fourStepView.layer.cornerRadius = 10.0
            self.fourStepView.layer.borderWidth = 1.0
            self.fourStepView.layer.borderColor = UIColor.darkGray.cgColor
            self.fourStepView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
            self.fiveStepView.layer.cornerRadius = 10.0
            self.fiveStepView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
            self.fiveStepView.layer.borderWidth = 1.0
            self.fiveStepView.backgroundColor = UIColor.white
            
            
            
        } else if (self.orderSelected["status_id"]! as NSString).integerValue == 25 {
            
            self.fiveImageView.alpha = 1
            self.trackButton.isHidden = false
            
            self.oneBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.twoBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.threeBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.fourBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.fiveBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.sixBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.sevenBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.eightBarView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
            self.firstStepView.layer.cornerRadius = 10.0
            self.firstStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.firstStepView.layer.borderWidth = 1.0
            self.firstStepView.backgroundColor = UIColor.white
            
            self.twoStepView.layer.cornerRadius = 10.0
            self.twoStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.twoStepView.layer.borderWidth = 1.0
            self.twoStepView.backgroundColor = UIColor.white
            
            self.threeStepView.layer.cornerRadius = 10.0
            self.threeStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.threeStepView.layer.borderWidth = 1.0
            self.threeStepView.backgroundColor = UIColor.white
            
            self.fourStepView.layer.cornerRadius = 10.0
            self.fourStepView.layer.borderColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
            self.fourStepView.layer.borderWidth = 1.0
            self.fourStepView.backgroundColor = UIColor.white
            
            self.fiveStepView.layer.cornerRadius = 10.0
            self.fiveStepView.layer.borderWidth = 1.0
            self.fiveStepView.layer.borderColor = UIColor.darkGray.cgColor
            self.fiveStepView.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            
        }
        

        self.fiscalButton.layer.cornerRadius = 5.0
        self.fiscalButton.layer.borderWidth = 1.0
        self.fiscalButton.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
        self.tradeButton.layer.cornerRadius = 5.0
        self.tradeButton.layer.borderWidth = 1.0
        self.tradeButton.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
        
        self.savoryView.stateProvider = SimpleStateProvider([.collapsed, .collapsed, .collapsed])
        savoryView.tag = 1
        self.savoryView.headerIdentifier = "headerOrderCell"
        self.savoryView.bodyIdentifier = "bodyOrderCell"
        
        
        self.savoryView.register(UINib(nibName: "OrderOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "headerOrderCell")
        self.savoryView.register(UINib(nibName: "BodyOrderOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "bodyOrderCell")
        
        self.savoryView.savoryDelegate = self
        
 
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { _ in
            self.collectionView.reloadData()
        }

//        self.collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func trackOrder(_ sender: UIButton) {
        
        if (self.orderSelected["status_id"]! as NSString).integerValue == 18 {
            
            let safariViewController = SFSafariViewController(url: URL(string: "\(urlBase.absoluteString)postageService/getTrackView/\(self.orderSelected["id"]!)")!, entersReaderIfAvailable: true)
            self.present(safariViewController, animated: true, completion: nil)
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Rastreamento não disponivel", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    
    }
    
    @IBAction func notaFiscal(_ sender: UIButton) {
        print(self.orderSelected["id"] ?? "No ID")
        
        if let userId = self.orderSelected["id"] {
            WishareAPI.getNotaFiscal(id: userId) { (urls: [String]?, error: Error?) in
                
                if let urlsExist : [String] = urls {
                    if urlsExist.count == 0 {
                        let alertController = UIAlertController(title: nil, message: "Nota Fiscal não disponivel", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else if urlsExist.count == 1 {
                        let extensionOfFile : String = urlsExist[0].suffix(3).uppercased()
                        let alertController = UIAlertController(title: "Visualizar Nota fiscal", message: "Selecione como deseja visualizar:", preferredStyle: .alert)
                        let firstAction = UIAlertAction(title: extensionOfFile, style: .default) { (alert: UIAlertAction) in
                            self.showNotaFiscal(url: urlsExist[0])
                        }
                        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        
                        alertController.addAction(firstAction)
                        alertController.addAction(cancelAction)

                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    else if urlsExist.count == 2 {
                        let extensionOfFirstFile : String = urlsExist[0].suffix(3).uppercased()
                        let extensionOfSecondFile : String = urlsExist[1].suffix(3).uppercased()
                        let alertController = UIAlertController(title: "Visualizar Nota fiscal", message: "Selecione como deseja visualizar:", preferredStyle: .alert)
                        let firstAction = UIAlertAction(title: extensionOfFirstFile, style: .default) { (alert: UIAlertAction) in
                            self.showNotaFiscal(url: urlsExist[0])
                        }
                        let secondAction = UIAlertAction(title: extensionOfSecondFile, style: .default) { (alert: UIAlertAction) in
                            self.showNotaFiscal(url: urlsExist[1])
                        }
                        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

                        
                        
                        alertController.addAction(firstAction)
                        alertController.addAction(secondAction)
                        alertController.addAction(cancelAction)


                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                else {
                    let alertController = UIAlertController(title: nil, message: "Nota Fiscal não disponivel.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
        }
        
        
    }
    func showNotaFiscal(url: String) {

        let quickLookController = QLPreviewController()
        quickLookController.dataSource = self
        
        
        let urlObject = URL(string: url)
        
        do {
            
            let data = try Data(contentsOf: urlObject!)

            // Get the documents directory
            let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            // Give the file a name and append it to the file path
            
            if let fileName = urlObject?.lastPathComponent {
                fileURL = documentsDirectoryURL.appendingPathComponent(fileName)

            } else {
                fileURL = documentsDirectoryURL.appendingPathComponent("Nota_\(self.orderSelected["id"]!)")
            }
            // Write the pdf to disk
            try data.write(to: fileURL!, options: .atomic)
            
            // Make sure the file can be opened and then present the pdf
            if QLPreviewController.canPreview(urlObject! as QLPreviewItem) {
                quickLookController.currentPreviewItemIndex = 0
                self.navigationController?.pushViewController(quickLookController, animated: true)
            }
        } catch {
            // cant find the url resource
            print("can't find the url resource")
        }
    }

    @IBAction func trade(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: "Em caso de Devolução, Troca ou Arrependimento", preferredStyle: .alert)
        
        let callAction = UIAlertAction(title: "Telefone", style: .default) { ( _ ) in
            
            if let url = URL(string: "tel://\(1130808600)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
        }
        
        let emailAction = UIAlertAction(title: "Mandar e-mail", style: .default, handler: { ( _ ) in
            
            let email = "hello@wishare.com.br"
            let url = URL(string: "mailto:\(email)")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(callAction)
        alertController.addAction(emailAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return fileURL! as QLPreviewItem
    }
}

extension MyOrderDetailViewController : SavoryViewDelegate {
    
    func headerCell(forPanelAt index: SavoryPanelIndex, in savoryView: SavoryView) -> SavoryHeaderCell {
        
        let cell = savoryView.dequeueReusableHeaderCell(forPanelAt: index) as! OrderOptionTableViewCell
        
        cell.typeImageView.image = UIImage(named: self.imagesForHeader[index])
        cell.typeLabel.text = self.titlesForHeader[index]
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func bodyCell(forPanelAt index: SavoryPanelIndex, in savoryView: SavoryView) -> SavoryBodyCell {
        
        let cell = savoryView.dequeueReusableBodyCell(forPanelAt: index) as! BodyOrderOptionTableViewCell
        
        if index == 0 {
            
            cell.addressOrNameOrSubtotalLabel.text = self.orderSelected["name_card"]
            cell.stateOrNumberCardOrFreteLabel.text = "**** **** **** \((self.orderSelected["number_card"]! as NSString).substring(from: 12))"
            
            cell.freteLabel.text = "\(self.orderSelected["installments"]!)x de \((((self.orderSelected["tot"]!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue + (self.orderSelected["value_freight"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue) / (self.orderSelected["installments"]! as NSString).floatValue).asLocaleCurrency)"
            
            cell.cepOrTotalLabel.text = nil
            cell.numberParcelOrTotalLabel.text = nil
            cell.subTotalLabel.text = nil
            
            
            
        } else if index == 1 {
            
            cell.addressOrNameOrSubtotalLabel.text = "Subtotal"
            cell.stateOrNumberCardOrFreteLabel.text = "Frete"
            cell.cepOrTotalLabel.attributedText = "Valor Total".withFont(.boldSystemFont(ofSize: 18)).withTextColor(UIColor.black)
            
            cell.freteLabel.text = "\(((self.orderSelected["value_freight"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue).asLocaleCurrency)"
            cell.subTotalLabel.text = "\(((self.orderSelected["tot"]!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue).asLocaleCurrency)"
            cell.numberParcelOrTotalLabel.attributedText = "\(((self.orderSelected["tot"]!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue + (self.orderSelected["value_freight"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue).asLocaleCurrency)".withFont(.boldSystemFont(ofSize: 18)).withTextColor(UIColor.black)
            
        } else {
            
            cell.addressOrNameOrSubtotalLabel.text = "\(self.orderSelected["address"]!), \(self.orderSelected["number"]!)"
            cell.stateOrNumberCardOrFreteLabel.text = "\(self.orderSelected["neighborhood"]!) - \(self.orderSelected["city"]!) - \(self.orderSelected["state"]!)"
            cell.cepOrTotalLabel.text = "CEP: \(self.orderSelected["post_code"]!)"
            cell.freteLabel.text = nil
            cell.subTotalLabel.text = nil
            cell.numberParcelOrTotalLabel.text = nil
            
        }
        
        cell.selectionStyle = .none
        
        return cell
        
    }

    func didExpand(panelAt index: SavoryPanelIndex, in savoryView: SavoryView) {
        
        UIView.animate(withDuration: 0.4) {
            if let headerCellExpanded = (self.savoryView.savoryDelegate.headerCell(forPanelAt: index, in: savoryView)) as? OrderOptionTableViewCell {
                headerCellExpanded.arrowImageView.transform = CGAffineTransform(rotationAngle: .pi)
                
            }
            self.heightTableViewConstraint.constant += 100.0
            self.view.layoutIfNeeded()
        }
        
        
        
    }
    
    func didCollapse(panelAt index: SavoryPanelIndex, in savoryView: SavoryView) {
        
        UIView.animate(withDuration: 0.4) {
            if let headerCellExpanded = (self.savoryView.savoryDelegate.headerCell(forPanelAt: index, in: savoryView)) as? OrderOptionTableViewCell {
                headerCellExpanded.arrowImageView.transform = CGAffineTransform(rotationAngle: .pi)
            }
            self.heightTableViewConstraint.constant -= 100.0
            self.view.layoutIfNeeded()
        }
    
    }
    
    
    
}

extension MyOrderDetailViewController : UICollectionViewDelegate {
    
    
    
}

extension MyOrderDetailViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsSelected.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderProductCell", for: indexPath) as! OrderProductCollectionViewCell
        
        cell.delegate = self
        cell.accessibilityHint = self.itemsSelected[indexPath.row]["product_id"]
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlPerfil = urlBase.appendingPathComponent("files/orders_itens/\(self.itemsSelected[indexPath.row]["product_id"]!)/\(self.itemsSelected[indexPath.row]["url"]!)")
        
        self.manager.loadImage(with: urlPerfil, into: cell.productImageView) { ( result, _ ) in
            cell.productImageView.image = result.value
        }
        
        cell.nameProductLabel.text = self.itemsSelected[indexPath.row]["item"]
        cell.sizeLabel.text = "Tamanho: \(self.itemsSelected[indexPath.row]["size"]!)"
        cell.quantiyLabel.text = "Qtd.: \(self.itemsSelected[indexPath.row]["qtd"]!)"
        cell.unitPriceLabel.text = "Valor Unitário: R$ \(self.itemsSelected[indexPath.row]["value"]!)"
        cell.totalPriceLabel.text = "\(((self.itemsSelected[indexPath.row]["value"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue * (self.itemsSelected[indexPath.row]["qtd"]!.components(separatedBy: ",").joined(separator: ".") as NSString).floatValue).asLocaleCurrency)"
        
        
        if (self.orderSelected["status_id"]! as NSString).integerValue == 25 {
            cell.avaliarButton.isHidden = false
        } else {
            cell.avaliarButton.isHidden = true
        }
        
        return cell
    }
    
}

extension MyOrderDetailViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width, height: 120)
    }
    
}

extension MyOrderDetailViewController : OrderProductDelegate {
    
    func avaliar(_ sender: UIButton, cell: OrderProductCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let showRatingProductViewController = storyboard.instantiateViewController(withIdentifier: "showRatingID") as! ShowRatingProductViewController
        showRatingProductViewController.pId = cell.accessibilityHint!
        
        self.navigationController?.pushViewController(showRatingProductViewController, animated: true)
        
    }
    
}
