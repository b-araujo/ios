//
//  Category.swift
//  Wishare
//
//  Created by Wishare iMac on 5/16/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Category {
    
    var id : String?
    var name : String?
    var picture : String?
    var active : String?
    var products : [Product] = []
    var promo : [Promo] = []
    
    init( _ category : [String : AnyObject]) {
        
        let categoryAPI : [String : AnyObject] = category["Category"] as! [String : AnyObject]
    
        self.id = categoryAPI["id"] as? String
        self.name = categoryAPI["name"] as? String
        self.picture = categoryAPI["picture"] as? String
        
    }
    
    
    init( withProduct category : [String : AnyObject]) {
        
        let categoryAPI : [String : AnyObject] = category["category"] as! [String : AnyObject]
        
        self.id = categoryAPI["id"] as? String
        self.name = categoryAPI["name"] as? String
        self.picture = categoryAPI["picture"] as? String
        
        
        let productAPI : [String : AnyObject] = category["Products"] as! [String : AnyObject]
        let productsLevelOne : [[String : AnyObject]] = productAPI["level1"] as! [[String : AnyObject]]
        let productsLevelTwo : [[String : AnyObject]] = productAPI["level2"] as! [[String : AnyObject]]
        
        
        for product in productsLevelOne {
            self.products.append(Product(byCategory: product))
        }
        
        for product in productsLevelTwo {
            self.products.append(Product(byCategory: product))
        }
        
        
    }
    
    init (categoriesResponse categories : [String : AnyObject]) {
        
        self.id = categories["id"] as? String
        self.name = categories["name"] as? String
        
    }
    
    init (id : String, name : String) {
        
        self.id = id
        self.name = name
        
    }
    
}
