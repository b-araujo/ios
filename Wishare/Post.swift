//
//  Post.swift
//  Wishare
//
//  Created by Wishare iMac on 4/25/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation


class Post {
    
    var id : String?
    var created : String?
    var type : String?
    var store_id : String?
    var user_id : String?
    var share_type : String?
    var text : String?
    var picture : String?
    var user : User?
    var typePost : Int = 0
    var tags : [Tag] = []
    var store : Store?
    var products : [[String : String]] = []
    var share_user_id : String?
    var share_store_id : String?
    var share_user_username : String?
    var share_user_picture : String?
    var share_text : String?
    var share_store_following : String?
    var share_user_following : String?
    var share_store_name : String?
    var likes : Int?
    var ilikes : String?
    var iwish : String?
    var ishare : Bool?
    var comments : Int?
    var products_info : [Product] = []
    var hashtag : String?
    var showMoreComments : Bool = false
    
    
    // Tipos de posts
    // 1 - Post de Usuario
    // 2 - Post de Loja Simples
    // 3 - Post de Loja Produto
    // 4 - Post de Loja Look
    // 5 - Compartilhamento de compra
    // 6 - Post de Usuario (Compartilhamento por usuario)
    // 7 - Post de Loja - Simples - (Compartilhamento por usuario)
    // 8 - Post de Loja - Produto - (Compartilhamento por usuario)
    // 9 - Post de Loja - Look    - (Compartilhamento por usuario)
    // 10 - Compartilhamento de produto sem post
    
    init( _ post : [String : AnyObject]) {
        
        let postAPI : [String : AnyObject] = post["Post"] as! [String : AnyObject]
        
        self.id = postAPI["id"] as? String
        self.created = postAPI["created"] as? String
        self.type = postAPI["type"] as? String
        self.store_id = (postAPI["store_id"] is NSNull) ? nil : postAPI["store_id"] as? String
        self.user_id = (postAPI["user_id"] is NSNull) ? nil : postAPI["user_id"] as? String
        self.share_type = postAPI["share_type"] as? String
        self.text = postAPI["text"] as? String
        self.picture = postAPI["picture"] as? String
        self.share_user_id = (postAPI["share_user_id"] is NSNull) ? nil : postAPI["share_user_id"] as? String
        self.share_store_id = (postAPI["share_store_id"] is NSNull) ? nil : postAPI["share_store_id"] as? String
        self.share_user_username = (postAPI["share_user_username"] is NSNull) ? nil : postAPI["share_user_username"] as? String
        self.share_user_picture = (postAPI["share_user_picture"] is NSNull) ? nil : postAPI["share_user_picture"] as? String
        self.share_text = postAPI["share_text"] as? String
        self.share_store_following = (postAPI["share_store_following"] is NSNull) ? nil : postAPI["share_store_following"] as? String
        self.share_user_following = (postAPI["share_user_following"] is NSNull) ? nil : postAPI["share_user_following"] as? String
        self.share_store_name = (postAPI["share_store_name"] is NSNull) ? nil : postAPI["share_store_name"] as? String
        self.likes = (postAPI["likes"] is String) ? Int(postAPI["likes"] as! String) : 0
        self.ilikes = postAPI["ilike"] as? String
        self.iwish = postAPI["iwish"] as? String
        self.ishare = postAPI["ishare"] as? Bool
        self.comments = (postAPI["comments"] is String) ? Int(postAPI["comments"] as! String) : 0
        self.hashtag = (postAPI["hashtag"] is NSNull) ? "" : postAPI["hashtag"] as? String
        
        if let products : String = postAPI["products"] as? String {
            if products.contains("url") {
                
                var productsArray : [String] = []
                var productsJSON : [String] = []
                var url : [String] = []
                
                let prod = products.components(separatedBy: CharacterSet(charactersIn: ",[{}]")).filter({ (value) -> Bool in
                    return value != "" ? true : false
                })
                
                
                for string in prod {
                    
                    if string.contains("\"id\":") {
                        productsArray.append(string.components(separatedBy: "\"id\":")[1])
                    } else if string.contains("\"url\":") {
                        productsArray.append(string.components(separatedBy: "\"url\":")[1])
                    }
                
                }
                
                for string in productsArray {
                    
                    if string.contains("\"") {
                        productsJSON.append(string.components(separatedBy: "\"")[1])
                    } else {
                        productsJSON.append(string)
                    }
                    
                }
                
                for string in productsJSON {
                    
                    if string.contains("\\") {
                        let parseString : String = string.components(separatedBy: "\\")[0]
                        url.append("\(parseString)\(string.components(separatedBy: "\\")[1])")
                    } else {
                        url.append(string)
                    }
                    
                }
                
                
                for i in 0...((url.count / 2) - 1) {
                    self.products.append(["id": url[i * 2], "url" : url[(i * 2) + 1]])
                }
                
            }
        }
        
        let userAPI : [String : AnyObject] = post["User"] as! [String : AnyObject]
        let storeAPI : [String : AnyObject] = post["Store"] as! [String : AnyObject]
        
        self.user = User(postUser: userAPI)
        self.store = Store(postStore: storeAPI)
        
        //ajuste index_fast
        
        if(self.share_user_id != nil){
            self.share_type = "USER"
        }else if(self.share_store_id != nil){
            self.share_type = "STORE"
        }else{
            self.share_type = ""
        }
        
        //
        
        if self.type == "3" && self.share_type == "" && self.store_id == nil && self.user_id != nil {
            self.typePost = 1
        } else if self.type == "3" && self.store_id != nil && self.user_id == nil && self.share_type == "" {
            self.typePost = 2
        } else if self.type == "1" && self.store_id != nil && self.user_id == nil && self.share_type == "" {
            self.typePost = 3
        } else if self.type == "2" && self.store_id != nil && self.user_id == nil && self.share_type == "" {
            self.typePost = 4
        } else if self.type == "6" && self.store_id != nil && self.user_id != nil && self.share_store_id == nil {
            self.typePost = 5
        } else if self.type == "3" && self.store_id == nil && self.user_id != nil && self.share_user_username != nil && self.share_user_picture != nil  && self.share_type == "USER" {
            self.typePost = 6
        } else if self.type == "3" && self.store_id != nil && self.user_id != nil && self.share_type == "STORE" && self.share_store_following != nil && self.share_user_following != nil && self.share_store_name != nil {
            self.typePost = 7
        } else if self.type == "1" && self.store_id != nil && self.user_id != nil && self.share_type == "STORE" && self.share_store_following != nil && self.share_user_following != nil && self.share_store_name != nil {
            self.typePost = 8
        } else if self.type == "2" && self.store_id != nil && self.user_id != nil && self.share_type == "STORE" && self.share_store_following != nil && self.share_user_following != nil && self.share_store_name != nil {
            self.typePost = 9
        } else if self.type == "9" {
            self.typePost = 10
        } else if self.type == "8" && self.store_id == nil && self.user_id == nil {
            // Post Wishare
            self.typePost = 11
        }
        
        
        
        if let tagsAPI : [[String : AnyObject]] = postAPI["tags"] as? [[String : AnyObject]] {
            if tagsAPI.count > 0 {
                for tag in tagsAPI {
                    let tagInstance = Tag(tag)
                    self.tags.append(tagInstance)
                }
            }
        }
        
    }
    
    init( post byUser : [String : AnyObject]) {
        
        let postAPI : [String : AnyObject] = byUser["Post"] as! [String : AnyObject]
        
        self.id = postAPI["id"] as? String
        self.created = postAPI["created"] as? String
        self.type = postAPI["type"] as? String
        self.store_id = (postAPI["store_id"] is NSNull) ? nil : postAPI["store_id"] as? String
        self.user_id = (postAPI["user_id"] is NSNull) ? nil : postAPI["user_id"] as? String
        self.text = postAPI["text"] as? String
        self.picture = postAPI["picture"] as? String
        self.share_user_id = (postAPI["share_user_id"] is NSNull) ? nil : postAPI["share_user_id"] as? String
        self.share_store_id = (postAPI["share_store_id"] is NSNull) ? nil : postAPI["share_store_id"] as? String
        self.share_text = postAPI["share_text"] as? String
        self.hashtag = (postAPI["hashtag"] is NSNull) ? "" : postAPI["hashtag"] as? String
        self.likes = Int((postAPI["likes_qtd"] as! String))
        self.comments = Int((postAPI["comments_qtd"] as! String))
        
//        "likes_qtd": "0",
//        "wishs_qtd": "0",
//        "comments_qtd": "0",
        
        let userAPI : [String : AnyObject] = byUser["User"] as! [String : AnyObject]
        let storeAPI : [String : AnyObject] = byUser["Store"] as! [String : AnyObject]
        
        self.user = User(postUser: userAPI)
        self.store = Store(postStore: storeAPI)
        
        if let products : String = postAPI["products"] as? String {
            if products.contains("url") {
                
                var productsArray : [String] = []
                var productsJSON : [String] = []
                var url : [String] = []
                
                let prod = products.components(separatedBy: CharacterSet(charactersIn: ",[{}]")).filter({ (value) -> Bool in
                    return value != "" ? true : false
                })
                
                
                for string in prod {
                    
                    if string.contains("\"id\":") {
                        productsArray.append(string.components(separatedBy: "\"id\":")[1])
                    } else if string.contains("\"url\":") {
                        productsArray.append(string.components(separatedBy: "\"url\":")[1])
                    }
                    
                }
                
                for string in productsArray {
                    
                    if string.contains("\"") {
                        productsJSON.append(string.components(separatedBy: "\"")[1])
                    } else {
                        productsJSON.append(string)
                    }
                    
                }
                
                for string in productsJSON {
                    
                    if string.contains("\\") {
                        let parseString : String = string.components(separatedBy: "\\")[0]
                        url.append("\(parseString)\(string.components(separatedBy: "\\")[1])")
                    } else {
                        url.append(string)
                    }
                    
                }
                
                
                for i in 0...((url.count / 2) - 1) {
                    self.products.append(["id": url[i * 2], "url" : url[(i * 2) + 1]])
                }
                
            }
        }
        
        if self.share_user_id == nil && self.share_store_id == nil {
            self.share_type = ""
        } else if self.share_user_id != nil {
            
            self.share_type = "USER"
            let shareUser : [String : AnyObject] = byUser["ShareUser"] as! [String : AnyObject]
            self.share_user_username = (shareUser["username"] is NSNull) ? nil : shareUser["username"] as? String
            self.share_user_picture = (shareUser["picture"] is NSNull) ? nil : shareUser["picture"] as? String
            
        } else if self.share_store_id != nil {
            self.share_type = "STORE"
            let shareStore : [String : AnyObject] = byUser["ShareStore"] as! [String : AnyObject]
            self.share_store_name = (shareStore["name"] is NSNull) ? nil : shareStore["name"] as? String
        }
        
        let following : [String : AnyObject] = byUser["0"] as! [String : AnyObject]
        
        self.share_store_following = (following["share_store_following"] is NSNull) ? nil : following["share_store_following"] as? String
        self.share_user_following = (following["share_user_following"] is NSNull) ? nil : following["share_user_following"] as? String
        
        self.ilikes = (following["ilike"] as? String == "true") ? "1" : "0"
        self.iwish = (following["iwish"] as? String == "true") ? "1" : "0"
        self.ishare = (following["iwish"] as? String == "true") ? true : false
        

        
        if let tagsAPI : [[String : AnyObject]] = postAPI["tags"] as? [[String : AnyObject]] {
            if tagsAPI.count > 0 {
                for tag in tagsAPI {
                    let tagInstance = Tag(tag)
                    self.tags.append(tagInstance)
                }
            }
        }
        
        if self.type == "3" && self.share_type == "" && self.store_id == nil && self.user_id != nil {
            self.typePost = 1
        } else if self.type == "3" && self.store_id != nil && self.user_id == nil && self.share_type == "" {
            self.typePost = 2
        } else if self.type == "1" && self.store_id != nil && self.user_id == nil && self.share_type == "" {
            self.typePost = 3
        } else if self.type == "2" && self.store_id != nil && self.user_id == nil && self.share_type == "" {
            self.typePost = 4
        } else if self.type == "6" && self.store_id != nil && self.user_id != nil && self.share_store_id == nil {
            self.typePost = 5
        } else if self.type == "3" && self.store_id == nil && self.user_id != nil && self.share_user_username != nil && self.share_user_picture != nil  && self.share_type == "USER" {
            self.typePost = 6
        } else if self.type == "3" && self.store_id != nil && self.user_id != nil && self.share_type == "STORE" && self.share_store_following != nil && self.share_user_following != nil && self.share_store_name != nil {
            self.typePost = 7
        } else if self.type == "1" && self.store_id != nil && self.user_id != nil && self.share_type == "STORE" && self.share_store_following != nil && self.share_user_following != nil && self.share_store_name != nil {
            self.typePost = 8
        } else if self.type == "2" && self.store_id != nil && self.user_id != nil && self.share_type == "STORE" && self.share_store_following != nil && self.share_user_following != nil && self.share_store_name != nil {
            self.typePost = 9
        } else if self.type == "9" {
            self.typePost = 10
        }
        
    }
    
    init () {
        
    }

}





