//
//  OrderOptionTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Savory

class OrderOptionTableViewCell: SavoryHeaderCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.borderView.layer.cornerRadius = 5.0
        self.borderView.layer.borderWidth = 1.0
        self.borderView.layer.borderColor = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1).cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
