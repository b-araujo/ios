//
//  TargetsWishCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class TargetsWishCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var targetImageView: UIImageView!
    @IBOutlet weak var nameTargetLabel: UILabel!
    @IBOutlet weak var pointsTargetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        self.backView.layer.cornerRadius = 5.0
        self.backView.layer.masksToBounds = false
        self.backView.layer.shadowColor = UIColor.black.cgColor
        self.backView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.backView.layer.shadowRadius = 2.0
        self.backView.layer.shadowOpacity = 0.24
        
    }

}
