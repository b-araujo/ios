//
//  PreviewBackgroundUserViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/6/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class PreviewBackgroundUserViewController: UIViewController {

    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var userPictureImageView: UIImageView!
    @IBOutlet weak var defineBackgroundButton: UIButton!
    
    var loader : Loader!
    var manager : Manager!
    
    var origin : Int = 0
    var content : String = ""
    var image : UIImage!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.defineBackgroundButton.layer.cornerRadius = 5.0
        self.defineBackgroundButton.layer.masksToBounds = false
        self.defineBackgroundButton.layer.shadowColor = UIColor.black.cgColor
        self.defineBackgroundButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.defineBackgroundButton.layer.shadowRadius = 2.0
        self.defineBackgroundButton.layer.shadowOpacity = 0.24
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.backgroundImageView.image = image
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                WishareAPI.getInformationUser(idUser: user!.id!, { (user : User?, error : Error?) in
                    
                    if error == nil {
                        
                        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                        let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(user!.picture!)")
                        let urlPerfilFull = urlBase.appendingPathComponent("files/photo-profile/user/\(user!.picture!)")
                        
                        
                        self.manager.loadImage(with: urlPerfil, into: self.userPictureImageView) { ( result, _ ) in
                            if result.error == nil {
                                self.userPictureImageView.image = result.value?.circleMask
                                RappleActivityIndicatorView.stopAnimation()
                            } else {
                                self.manager.loadImage(with: urlPerfilFull, into: self.userPictureImageView) { ( result, _ ) in
                                    self.userPictureImageView.image = result.value?.circleMask
                                    RappleActivityIndicatorView.stopAnimation()
                                }
                            }
                        }

                    }
                    
                })
                
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func defineBackground(_ sender: UIButton) {
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Atualizando...", attributes: attributes)
        
        let fixed = image.fixOrientation()
        
        if let base64String = UIImageJPEGRepresentation(fixed , 1)?.base64EncodedString() {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Atualizando", attributes: attributes)
            
            WishareAPI.updateUserProfileCover(type: (self.origin == 1) ? "local" : "URL", content: self.content, image: (self.origin == 1) ? "" : base64String, { (error : Error?) in
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation()
                    
                    for viewController in self.navigationController!.viewControllers {
                        if viewController is SettingProfileViewController {
                            let settingProfileViewController = viewController as! SettingProfileViewController
                            NotificationCenter.default.post(name: NSNotification.Name.init("updateUserProfile"), object: 1)
                            self.navigationController?.popToViewController(settingProfileViewController, animated: true)
                        }
                    }
                    
                    //self.navigationController?.popViewController(animated: true)
                    //self.navigationController?.popToRootViewController(animated: true)
                    
                }
            })
            
        }
        
       
        
    }

}
