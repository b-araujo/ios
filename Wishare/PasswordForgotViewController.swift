//
//  PasswordForgotViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import SwiftValidator

class PasswordForgotViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var backLoginButton: UIButton!
    @IBOutlet weak var passwordForgotButton: UIButton!
    @IBOutlet weak var userOrEmailTextField: UITextField!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userOrEmailErrorLabel: UILabel!
    
    
    // MARK: - Properties
    let validator : Validator = Validator()
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        validator.styleTransformers(success: { (validationRule : ValidationRule) in
            
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = nil
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.clear.cgColor
                textField.layer.borderWidth = 0.0
            }
            
        }) { (validationError : ValidationError) in
            
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
            
        }
        
        
        validator.registerField(self.userOrEmailTextField, errorLabel: self.userOrEmailErrorLabel, rules: [RequiredRule(), EmailRule()])
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    @IBAction func passwordForgot(_ sender: UIButton) {
        
        self.validator.validate(self)
        
    }
    
    @IBAction func backLogin(_ sender: UIButton) {
    
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    // MARK: - Auxiliaries
    
    func hideOrShowComponentsAndLoading ( _ isActive : Bool) {
        
        self.view.endEditing(true)
        
        if isActive {
            self.loadingActivityIndicator.startAnimating()
        } else {
            self.loadingActivityIndicator.stopAnimating()
        }
        
        self.userOrEmailTextField.isHidden = isActive
        self.passwordForgotButton.isHidden = isActive
        self.backLoginButton.isHidden = isActive
        
    }
    
    

}

extension PasswordForgotViewController : ValidationDelegate {
    
    func validationSuccessful() {
        
        self.hideOrShowComponentsAndLoading(true)
        
        if let userOrEmail = self.userOrEmailTextField.text {
            
            WishareAPI.passwordForgot(userOrEmail, completion: { (result : [String : AnyObject], error : Error?) in
                
                
                if error == nil {
                    
                    if let boolResult = result["success"] {
                        
                        let isValidate = (boolResult as! CFBoolean) as! Bool
                        
                        if !isValidate {
                            
                            self.userOrEmailErrorLabel.isHidden = false
                            self.userOrEmailTextField.layer.borderColor = UIColor.red.cgColor
                            self.userOrEmailTextField.layer.borderWidth = 1.0
                            self.userOrEmailErrorLabel.text = (result["error"] as! String)
                            
                        } else {
                            
                            self.userOrEmailErrorLabel.isHidden = true
                            self.userOrEmailTextField.layer.borderColor = UIColor.clear.cgColor
                            self.userOrEmailTextField.layer.borderWidth = 0.0
                            
                            let alertController = UIAlertController(title: "Senha Recuperada", message: "Uma mensagem foi enviado para o seu e-mail", preferredStyle: .alert)
                            
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alertAction : UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                } else {
                    
                    self.userOrEmailErrorLabel.isHidden = true
                    self.userOrEmailTextField.layer.borderColor = UIColor.clear.cgColor
                    self.userOrEmailTextField.layer.borderWidth = 0.0
                    
                    let alertController = UIAlertController(title: "Erro ao recuperar senha", message: "Ocorreu um erro ao tentar recuperar a senha, tente novamente.", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                self.hideOrShowComponentsAndLoading(false)
                
                
            })
            
        }
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) { }
    
}
