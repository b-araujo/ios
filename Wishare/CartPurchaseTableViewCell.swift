//
//  CartPurchaseTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/23/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class CartPurchaseTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var deleteImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var sizeProductLabel: UILabel!
    @IBOutlet weak var colorProductLabel: UILabel!
    @IBOutlet weak var quantityProductLabel: UILabel!
    @IBOutlet weak var priceProductLabel: UILabel!
    @IBOutlet weak var totalPriceProductsStoreLabel: UILabel!
    
    var delegate : CartPurchaseProduct? = nil
    var storeId : Int? = nil
    var productId : Int? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.quantityProductLabel.backgroundColor = UIColor.white
        self.quantityProductLabel.textColor = UIColor.lightGray
        self.quantityProductLabel.layer.backgroundColor = UIColor.white.cgColor
        self.quantityProductLabel.layer.borderWidth = 1
        self.quantityProductLabel.layer.borderColor = UIColor.black.cgColor
        self.quantityProductLabel.layer.cornerRadius = 5.0
        self.quantityProductLabel.layer.masksToBounds = false
        self.quantityProductLabel.layer.shadowColor = UIColor.black.cgColor
        self.quantityProductLabel.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.quantityProductLabel.layer.shadowRadius = 2.0
        self.quantityProductLabel.layer.shadowOpacity = 0.24
        
        
        let tapGestureRecognizerDeleteImageView = UITapGestureRecognizer(target: self, action: #selector(removeItem))
        self.deleteImageView.addGestureRecognizer(tapGestureRecognizerDeleteImageView)
        
        let tapGestureRecognizerQuantityProductLabel = UITapGestureRecognizer(target: self, action: #selector(changeQuantity))
        self.quantityProductLabel.addGestureRecognizer(tapGestureRecognizerQuantityProductLabel)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func removeItem () {
        self.delegate?.removeItem(storeId: storeId, productId: productId)
    }
    
    @objc func changeQuantity () {
        self.delegate?.changeQuantity(storeId: storeId, productId: productId)
    }
    
}

protocol CartPurchaseProduct {
    func removeItem(storeId : Int?, productId : Int?)
    func changeQuantity(storeId : Int?, productId : Int?)
}
