//
//  ProductDetailViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/16/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import STZPopupView
import CollieGallery
import FloatRatingView
import SwiftyAttributes

class ProductDetailViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buyProductButton: UIButton!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var quantityRating: UILabel!
    
    @IBOutlet weak var shoppingModelViewBottomConstraint : NSLayoutConstraint!
    
    var attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    var product : Product = Product(id: "", pId: "", name: "", desc: "", brand: "", price: "", promo_price: "", unavailable: "", quantity: "", picture: "")
    var store : Store = Store(id: "", name: "", picture: "", advance_commission: "")
    
    var pictures = [CollieGalleryPicture]()
    var indexCurrentPicture = 0
    
    var idProduct : String!
    var isFirst : Bool = false
    
    var loader : Loader!
    var manager : Manager!
    
    var products : [String : [String]] = [:]
    
    var showMoreIsHidden : Bool = true
    var descriptionHeight: CGFloat = 0
    
    var rate : [String : Int] = ["5" : 0, "4" : 0, "3" : 0, "2" : 0, "1" : 0]
    var tot : Int = 0
    
    var guideSizeWebView : UIWebView = {
        let webView = UIWebView(frame: CGRect.zero)
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    
    var backIndicatorImage : UIImage?
    var backIndicatorTransitionMaskImage : UIImage?
    
    let collectionViewShoppginCart = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "ProductDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "productDetailCell")
//        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
//        }
        
        
        let tapGestureRecognizerRatingView = UITapGestureRecognizer(target: self, action: #selector(showRating))
        self.ratingView.addGestureRecognizer(tapGestureRecognizerRatingView)
        
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        if self.isFirst == false {
            self.idProduct = "1399"
        }
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.productDetail(idProduct: self.idProduct) { (product : Product, store : Store, error : Error?) in
            
            if error == nil {
                
                
                self.product = product
                self.store = store
                
                for p in self.product.pictures {
                    let url = "\(urlBase.absoluteString)files/products/\(self.product.pId!)/\(p)"
                    let pict = CollieGalleryPicture(url: url)
                    self.pictures.append(pict)
                }
                
                
                WishareAPI.ratingProduct(idProduct: self.product.pId!) { (rate : [String : Int], tot : Int, users : [[String : AnyObject]], error : Error?) in
                    
                    if error == nil {
                        
                        self.rate = rate
                        self.tot = tot
                        
                        var mediumRating : CGFloat = 0.0
                        
                        mediumRating += CGFloat(self.rate["5"]!) * 5.0
                        mediumRating += CGFloat(self.rate["4"]!) * 4.0
                        mediumRating += CGFloat(self.rate["3"]!) * 3.0
                        mediumRating += CGFloat(self.rate["2"]!) * 2.0
                        mediumRating += CGFloat(self.rate["1"]!) * 1.0
                        
                        
                        if self.tot >= 0 && self.tot <= 9 {
                            self.quantityRating.text = "(0\(self.tot))"
                        } else {
                            self.quantityRating.text = "(\(self.tot))"
                        }
                        
                        
                        self.ratingView.rating = Float(mediumRating / CGFloat(self.tot))
                        
                        
                        self.collectionView.reloadData()
                        self.collectionViewShoppginCart.reloadData()
                        
                    }
                    
                    RappleActivityIndicatorView.stopAnimation()
                    
                }
                
                RappleActivityIndicatorView.stopAnimation()
                
            } else if product.id == "-1" {
                RappleActivityIndicatorView.stopAnimation()
                
                let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)

                let okAction = UIAlertAction(title: "OK", style: .default) { (alert) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
        self.buyProductButton.layer.masksToBounds = false
        self.buyProductButton.layer.shadowColor = UIColor.black.cgColor
        self.buyProductButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.buyProductButton.layer.shadowRadius = 2.0
        self.buyProductButton.layer.shadowOpacity = 0.24
        
        
        self.backIndicatorImage = self.navigationController?.navigationBar.backIndicatorImage
        self.backIndicatorTransitionMaskImage = self.navigationController?.navigationBar.backIndicatorTransitionMaskImage
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-left")
        
        
        
        
        //------------
        
        let shoppingCartModel : UIView = {
            let view = UIView(frame: CGRect.zero)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            return view
        }()
        
        self.view.addSubview(shoppingCartModel)
        
        shoppingCartModel.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        shoppingCartModel.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        shoppingCartModel.heightAnchor.constraint(equalToConstant: (self.view.bounds.height / 2) - 64).isActive = true
        self.shoppingModelViewBottomConstraint = shoppingCartModel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: +((self.view.bounds.height / 2) - 64))
        self.shoppingModelViewBottomConstraint.isActive = true
        
        let titleSize : UILabel = {
            let label = UILabel()
            label.text = "ADICIONADO AO CARRINHO"
            label.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let buttonExit : UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "Delete"), for: .normal)
            button.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            button.imageView?.contentMode = .scaleAspectFit
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(closeShoppingModel), for: .touchUpInside)
            return button
        }()
        
        let buyLook : UIButton = {
            let button = UIButton()
            button.layer.cornerRadius = 5
            button.backgroundColor = UIColor.lightGray
            button.setTitle("Ir para o carrinho de compras", for: .normal)
            button.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(goShoppingCart), for: .touchUpInside)
            return button
        }()
        
        buyLook.layer.masksToBounds = false
        buyLook.layer.shadowColor = UIColor.black.cgColor
        buyLook.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        buyLook.layer.shadowRadius = 2.0
        buyLook.layer.shadowOpacity = 0.24
        
        collectionViewShoppginCart.register(UINib(nibName: "PopupShoppingCartCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "popupShoppingCart")
        collectionViewShoppginCart.delegate = self
        collectionViewShoppginCart.dataSource = self
        collectionViewShoppginCart.tag = 75
        collectionViewShoppginCart.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: self.view.bounds.height - 20)
        collectionViewShoppginCart.backgroundView = blurEffectView
        
        collectionViewShoppginCart.isScrollEnabled = false
        collectionViewShoppginCart.translatesAutoresizingMaskIntoConstraints = false
        
        
        shoppingCartModel.addSubview(collectionViewShoppginCart)
        shoppingCartModel.addSubview(titleSize)
        shoppingCartModel.addSubview(buttonExit)
        shoppingCartModel.addSubview(buyLook)
        
        buttonExit.topAnchor.constraint(equalTo: shoppingCartModel.topAnchor, constant: 5).isActive = true
        buttonExit.rightAnchor.constraint(equalTo: shoppingCartModel.rightAnchor).isActive = true
        buttonExit.heightAnchor.constraint(equalToConstant: 40).isActive = true
        buttonExit.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleSize.topAnchor.constraint(equalTo: shoppingCartModel.topAnchor, constant: 5).isActive = true
        titleSize.leftAnchor.constraint(equalTo: shoppingCartModel.leftAnchor).isActive = true
        titleSize.rightAnchor.constraint(equalTo: buttonExit.leftAnchor).isActive = true
        titleSize.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        buyLook.leftAnchor.constraint(equalTo: shoppingCartModel.leftAnchor, constant: 15).isActive = true
        buyLook.rightAnchor.constraint(equalTo: shoppingCartModel.rightAnchor, constant: -15).isActive = true
        buyLook.bottomAnchor.constraint(equalTo: shoppingCartModel.bottomAnchor, constant: -10).isActive = true
        buyLook.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        collectionViewShoppginCart.leftAnchor.constraint(equalTo: shoppingCartModel.leftAnchor).isActive = true
        collectionViewShoppginCart.topAnchor.constraint(equalTo: titleSize.bottomAnchor).isActive = true
        collectionViewShoppginCart.rightAnchor.constraint(equalTo: shoppingCartModel.rightAnchor).isActive = true
        collectionViewShoppginCart.bottomAnchor.constraint(equalTo: buyLook.topAnchor, constant: -10).isActive = true
        

        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadImages(cell : ProductDetailCollectionViewCell) {
        
        for (index, image) in self.product.pictures.enumerated() {
            
            if let imageView = Bundle.main.loadNibNamed("Feature", owner: self, options: nil)?.first as? Feature {
                

                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url = urlBase.appendingPathComponent("files/products/\(self.product.pId!)/\(image)")
                
                self.manager.loadImage(with: url, into: imageView.image, handler: { (result, _) in
                    imageView.image.image = result.value
                })
                
                cell.imageScrollView.addSubview(imageView)
                
                imageView.frame.size.width = self.view.bounds.width
                imageView.frame.size.height = self.view.bounds.width
                imageView.frame.origin.x = CGFloat(index) * self.view.bounds.width
                
                let tapGestureRecognizerZoomImageView = UITapGestureRecognizer(target: self, action: #selector(self.openZoomImageView(_:)))
                tapGestureRecognizerZoomImageView.accessibilityHint = "\(index)"
                imageView.image.addGestureRecognizer(tapGestureRecognizerZoomImageView)

                
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        if self.product.pId != "" {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
            
            WishareAPI.ratingProduct(idProduct: self.product.pId!) { (rate : [String : Int], tot : Int, users : [[String : AnyObject]], error : Error?) in
                
                if error == nil {
                    
                    self.rate = rate
                    self.tot = tot
                    
                    var mediumRating : CGFloat = 0.0
                    
                    mediumRating += CGFloat(self.rate["5"]!) * 5.0
                    mediumRating += CGFloat(self.rate["4"]!) * 4.0
                    mediumRating += CGFloat(self.rate["3"]!) * 3.0
                    mediumRating += CGFloat(self.rate["2"]!) * 2.0
                    mediumRating += CGFloat(self.rate["1"]!) * 1.0
                    
                    
                    if self.tot >= 0 && self.tot <= 9 {
                        self.quantityRating.text = "(0\(self.tot))"
                    } else {
                        self.quantityRating.text = "(\(self.tot))"
                    }
                    
                    
                    self.ratingView.rating = Float(mediumRating / CGFloat(self.tot))
                    
                    
                    self.collectionView.reloadData()
                    
                }
                
                RappleActivityIndicatorView.stopAnimation()

                
            }
            
        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        
        self.navigationController?.navigationBar.backIndicatorImage = self.backIndicatorImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = self.backIndicatorTransitionMaskImage
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "hashTagsProductsSegue" {
            
            let showProductHashTagsViewController = segue.destination as! ShowProductHashTagsViewController
            showProductHashTagsViewController.textHashTag = sender as! String
            
        }
        
        if segue.identifier == "showRatingSegue" {
            
            let showRatingProductViewController = segue.destination as! ShowRatingProductViewController
            showRatingProductViewController.pId = sender as! String
            
        }
        
        if segue.identifier == "shareRepostSegue" {
            
            let repostProductViewController = segue.destination as! RepostProductViewController
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            repostProductViewController.imageRespost = senderValue[0] as? UIImage
            repostProductViewController.idProduct = senderValue[1] as? String
            
        }
        
    }
    
    @objc func openZoomImageView( _ gestureRecognizer : UITapGestureRecognizer) {
    
        let options = CollieGalleryOptions()
        options.openAtIndex = self.indexCurrentPicture
        options.excludedActions = [UIActivityType.assignToContact, UIActivityType.copyToPasteboard, UIActivityType.print]
        
        let gallery = CollieGallery(pictures: self.pictures, options: options, theme: nil)
        gallery.delegate = self
        gallery.presentInViewController(self, transitionType: CollieGalleryTransitionType.zoom(fromView: self.view, zoomTransitionDelegate: self))
        
    }
    
    func customNavigationBar() {
        let nav = (self.navigationController?.navigationBar)!
        let transparencia = UIImage(named: "transparente")
        nav.setBackgroundImage(transparencia, for: .default)
        nav.shadowImage = transparencia
        nav.backgroundColor = UIColor.clear
    }
    
    @objc func pressSelectedSize ( _ sender : UIButton, _ cell : ProductDetailCollectionViewCell) {
        
        
        for size in self.product.sizes {
            
            if size.id! == sender.productSizeID! {
                
                if size.quantity! == "0" {
                    
                    let alertController = UIAlertController(title: "OPS, O ESTOQUE FOI VENDIDO!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                        WishareAPI.waitingStock(size.id!, { (error : Error?) in })
                    })
                    
                    let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    // ADICIONAR A QUANTIDADE NA LISTA DE PRODUTOS
                    
                    self.product.quantity = size.quantity
                    self.product.price = size.price
                    self.product.promo_price = size.promo_price
                    self.collectionView.reloadData()
                    
                    // ADICONANDO NO "CARRINHO" DE TAMANHOS
                    
                    sender.backgroundColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1)
                    sender.setTitleColor(UIColor.darkGray, for: .normal)
                    sender.layer.borderColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1).cgColor
                    
                    if var product = self.products[sender.productID!] {
                        
                        print("EXISTE")
                        
                        if product[0] != sender.productSizeID {
                            
                            print("É DIFERENTE")
                            
                            let buttonzinho = sender.superview?.viewWithTag(Int(self.products[sender.productID!]![0])!) as! UIButton
                            buttonzinho.backgroundColor = UIColor.white
                            buttonzinho.setTitleColor(UIColor.lightGray, for: .normal)
                            buttonzinho.layer.borderColor = UIColor.black.cgColor
                            
                            self.products.removeValue(forKey: sender.productID!)
                            
                            if let productID = sender.productID {
                                self.products = [productID : [sender.productSizeID!, sender.productSize!]]
                            }
                            
                        }
                        
                    } else {
                        
                        print("NAO EXISTE")
                        if let productID = sender.productID {
                            self.products[productID] = [sender.productSizeID!, sender.productSize!]
                        }
                        
                    }
                    
                }
                
            }
        
        }
        
    }
    
    @objc func closePopup () {
        dismissPopupView()
    }
    
    @objc func closeShoppingModel() {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
            self.view.layoutIfNeeded()
        })
        
    }
    
    @objc func goShoppingCart () {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
            self.view.layoutIfNeeded()
        })
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    
    @objc func pressBuyProductDetail() {
        
        if let _ = self.products[self.product.id!] {
            
            WishareCoreData.showShoppingCart(completion: { (productsCart : [WS_ShoppingCart], _, error : Error?) in
            
                if error == nil {
                    
                    var isAddShoppingCart : Bool = true
                    
                    for prod in productsCart {
                        if prod.id_product! == self.products[self.product.id!]![0] {
                            isAddShoppingCart = false
                        }
                    }
                    
                    if !isAddShoppingCart {
                        
                        let alertController = UIAlertController(title: nil, message: "Você ja adicionou este produto ao carrinho", preferredStyle: .alert)
                        
                        let goToCartAction = UIAlertAction(title: "Ir para o carrinho de compra", style: .default, handler: { (action : UIAlertAction) in
                            
                            UIView.animate(withDuration: 0.4, animations: {
                                self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
                                self.view.layoutIfNeeded()
                            })
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
                            self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
                            
                        })
                        
                        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        
                        alertController.addAction(goToCartAction)
                        alertController.addAction(cancelAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    } else {
                        
                        WishareCoreData.addShoppingCart(name: self.product.name!, size: self.products[self.product.id!]![1], color: self.product.color!, quantity: "1", price: (self.product.promo_price! == "") ? self.product.price! : self.product.promo_price!, idProduct: self.products[self.product.id!]![0], idStore: self.store.id!, nameStore: self.store.name!, completion: { (validate : Bool, error : Error?) in
                            
                            if error == nil {
                                
                                print("ADICIONADO NO CARRINHO")
                                self.dismissPopupView()
                                self.collectionViewShoppginCart.reloadData()
                                
                                UIView.animate(withDuration: 0.4, animations: {
                                    self.shoppingModelViewBottomConstraint.constant = 0
                                    self.view.layoutIfNeeded()
                                })
                                
                                
                            } else {
                                print("NAO FOI ADICIONADO NO CARRINHO")
                            }
                            
                        })
                    }
                    
                    
                    
                }
            })
        
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Selecione um tamanho!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        
        }
        
    }
    
    @objc func pressOpenHashTag( _ sender : UIButton) {
        
        if let text = sender.accessibilityHint {
            self.performSegue(withIdentifier: "hashTagsProductsSegue", sender: text)
        }
        
    }
    
    @IBAction func moreBarButton(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let denunciarAction = UIAlertAction(title: "Denunciar Imagem", style: .default) { (action : UIAlertAction) in
            
            WishareAPI.reportProblem("Denunciar Imagem", "Imagem denunciada do id: \(self.product.id!) e produto id \(self.product.pId!) da loja \(self.store.name!)", { (error : Error?) in })
            let alertControllerConfirm = UIAlertController(title: "Algo está errado?", message: "Obrigado pela informação", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertControllerConfirm.addAction(okAction)
            self.present(alertControllerConfirm, animated: true, completion: nil)
            
        }
        
        
        let linkAction = UIAlertAction(title: "Link Quebrado", style: .default) { (action : UIAlertAction) in
            
            WishareAPI.reportProblem("Link Quebrado", "Link Quebrado do id: \(self.product.id!) e produto id \(self.product.pId!) da loja \(self.store.name!)", { (error : Error?) in })
            let alertControllerConfirm = UIAlertController(title: "Algo está errado?", message: "Obrigado pela informação", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertControllerConfirm.addAction(okAction)
            self.present(alertControllerConfirm, animated: true, completion: nil)
            
        }
        
        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(denunciarAction)
        alertController.addAction(linkAction)
        alertController.addAction(cancelarAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    

    @IBAction func buyProductDetail(_ sender: UIButton) {
    
        //self.products.removeAll()
        
        if let _ = self.products[self.product.id!] {
            
            WishareCoreData.showShoppingCart(completion: { (productsCart : [WS_ShoppingCart], _, error : Error?) in
                
                if error == nil {
                    
                    var isAddShoppingCart : Bool = true
                    
                    for prod in productsCart {
                        if prod.id_product! == self.products[self.product.id!]![0] {
                            isAddShoppingCart = false
                        }
                    }
                    
                    if !isAddShoppingCart {
                        
                        let alertController = UIAlertController(title: nil, message: "Você ja adicionou este produto ao carrinho", preferredStyle: .alert)
                        
                        let goToCartAction = UIAlertAction(title: "Ir para o carrinho de compra", style: .default, handler: { (action : UIAlertAction) in
                            
                            UIView.animate(withDuration: 0.4, animations: {
                                self.shoppingModelViewBottomConstraint.constant = +((self.view.bounds.height / 2) - 64)
                                self.view.layoutIfNeeded()
                            })
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
                            self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
                            
                        })
                        
                        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        
                        alertController.addAction(goToCartAction)
                        alertController.addAction(cancelAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    } else {
                        
                        WishareCoreData.addShoppingCart(name: self.product.name!, size: self.products[self.product.id!]![1], color: self.product.color!, quantity: "1", price: (self.product.promo_price! == "") ? self.product.price! : self.product.promo_price!, idProduct: self.products[self.product.id!]![0], idStore: self.store.id!, nameStore: self.store.name!, completion: { (validate : Bool, error : Error?) in
                            
                            if error == nil {
                                
                                print("ADICIONADO NO CARRINHO")
                                self.dismissPopupView()
                                self.collectionViewShoppginCart.reloadData()
                                
                                UIView.animate(withDuration: 0.4, animations: {
                                    self.shoppingModelViewBottomConstraint.constant = 0
                                    self.view.layoutIfNeeded()
                                })
                                
                                
                            } else {
                                print("NAO FOI ADICIONADO NO CARRINHO")
                            }
                            
                        })
                    }
                    
                    
                    
                }
            })
            
        } else {
            
            let popup = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: 240))
            
            let titleSize : UILabel = {
                let label = UILabel()
                label.text = "SELECIONE O TAMANHO"
                label.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                label.textAlignment = .center
                label.translatesAutoresizingMaskIntoConstraints = false
                return label
            }()
            
            let buttonExit : UIButton = {
                let button = UIButton()
                button.setImage(UIImage(named: "Delete"), for: .normal)
                button.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                button.imageView?.contentMode = .scaleAspectFit
                button.translatesAutoresizingMaskIntoConstraints = false
                button.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
                return button
            }()
            
            let buyLook : UIButton = {
                let button = UIButton()
                button.layer.cornerRadius = 5
                button.backgroundColor = UIColor.lightGray
                button.setTitle("CONTINUAR", for: .normal)
                button.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                button.translatesAutoresizingMaskIntoConstraints = false
                button.addTarget(self, action: #selector(pressBuyProductDetail), for: .touchUpInside)
                return button
            }()
            
            let collectionViewFlowLayout = UICollectionViewFlowLayout()
            let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewFlowLayout)
            
            collectionView.register(UINib(nibName: "PopupCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "popupCell")
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.tag = 11
            collectionView.backgroundColor = UIColor.clear
            let blurEffect = UIBlurEffect(style: .extraLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: self.view.bounds.height - 20)
            collectionView.backgroundView = blurEffectView
            
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            
            popup.addSubview(collectionView)
            popup.addSubview(titleSize)
            popup.addSubview(buttonExit)
            popup.addSubview(buyLook)
            
            buttonExit.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
            buttonExit.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
            buttonExit.heightAnchor.constraint(equalToConstant: 40).isActive = true
            buttonExit.widthAnchor.constraint(equalToConstant: 40).isActive = true
            
            titleSize.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
            titleSize.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
            titleSize.rightAnchor.constraint(equalTo: buttonExit.leftAnchor).isActive = true
            titleSize.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            buyLook.leftAnchor.constraint(equalTo: popup.leftAnchor, constant: 15).isActive = true
            buyLook.rightAnchor.constraint(equalTo: popup.rightAnchor, constant: -15).isActive = true
            buyLook.bottomAnchor.constraint(equalTo: popup.bottomAnchor, constant: -10).isActive = true
            buyLook.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            collectionView.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
            collectionView.topAnchor.constraint(equalTo: titleSize.bottomAnchor).isActive = true
            collectionView.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: buyLook.topAnchor, constant: -10).isActive = true
            
            
            let popupConfig = STZPopupViewConfig()
            popup.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            popupConfig.cornerRadius = 5
            popupConfig.overlayColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            popupConfig.showAnimation = .slideInFromBottom
            popupConfig.dismissAnimation = .slideOutToBottom
            
            presentPopupView(popup, config: popupConfig)
            
        }
    
    }
    
    @objc func showRating() {
        
        self.performSegue(withIdentifier: "showRatingSegue", sender: self.product.pId!)
        
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
}

extension ProductDetailViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 10 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.product.allColor[indexPath.row].id!
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
    }
    
}

extension ProductDetailViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 10 {
            return self.product.allColor.count
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 75 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popupShoppingCart", for: indexPath) as! PopupShoppingCartCollectionViewCell
            
            cell.nameProductLabel.text = self.product.name
            
            if let _ = self.products[self.product.id!] {
                cell.sizeProductLabel.text = "Tamanho: \(self.products[self.product.id!]![1])"
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.product.pId!)/\(self.product.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
                cell.productImageView.image = result.value
            }
            
            
            return cell
            
        }
        
        
        if collectionView.tag == 10 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCell", for: indexPath) as! ColorCollectionViewCell
            
            cell.layer.masksToBounds = false
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 0.24
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.product.pId!)/\(self.product.pictures[indexPath.row])")
            
            
            self.manager.loadImage(with: url, into: cell.productColorImageView) { ( result, _ ) in
                cell.productColorImageView.image = result.value
            }
            
            
            return cell
            
        } else if collectionView.tag == 11 {
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "popupCell", for: indexPath) as! PopupCollectionViewCell
            
            cell.productNamePopupLabel.text = self.product.name!
            cell.continueButton.isHidden = true
            cell.viewButton.isHidden = true
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.product.pId!)/\(self.product.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.productPopupImageView) { ( result, _ ) in
                cell.productPopupImageView.image = result.value
            }
            
            var lastWidthSize : CGFloat = 5.0
            var lastTopSize : CGFloat = 5.0
            var screenSizeProductSize : CGFloat = self.view.bounds.width - 20

            for (_, size) in self.product.sizes.enumerated() {
                
                let button : UIButton = {
                    let button = UIButton()
                    button.setTitle(size.size!, for: .normal)
                    button.titleLabel?.adjustsFontSizeToFitWidth = true
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.backgroundColor = UIColor.white
                    button.setTitleColor(UIColor.lightGray, for: .normal)
                    button.layer.borderColor = UIColor.black.cgColor
                    
                    button.layer.borderWidth = 1.0
                    button.layer.cornerRadius = 2.0
                    button.tag = Int(size.id!)!
                    // Setando informacoes
                    button.productID = self.product.id
                    button.productSizeID = size.id
                    button.productSize = size.size
                    return button
                }()
                
                cell.addSubview(button)
                
                if (screenSizeProductSize - lastWidthSize) < 0 {
                    
                    lastTopSize += 35.0
                    screenSizeProductSize = self.view.bounds.width - 20
                    lastWidthSize = 5.0
                    
                    button.topAnchor.constraint(equalTo: cell.productNamePopupLabel.bottomAnchor, constant: lastTopSize).isActive = true
                    button.leftAnchor.constraint(equalTo: cell.productPopupImageView.rightAnchor, constant: lastWidthSize).isActive = true
                    button.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    button.widthAnchor.constraint(equalToConstant: 30).isActive = true
                    lastWidthSize += 30 + 5.0
                    
                    
                } else {
                    
                    button.topAnchor.constraint(equalTo: cell.productNamePopupLabel.bottomAnchor, constant: lastTopSize).isActive = true
                    button.leftAnchor.constraint(equalTo: cell.productPopupImageView.rightAnchor, constant: lastWidthSize).isActive = true
                    button.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    button.widthAnchor.constraint(equalToConstant: 30).isActive = true
                    lastWidthSize += 30 + 5.0
                    screenSizeProductSize -= lastWidthSize
                    
                }
                
                button.addTarget(self, action: #selector(pressSelectedSize(_:_:)), for: .touchUpInside)
                
            }
            
            
            let lineView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1)
                view.translatesAutoresizingMaskIntoConstraints = false
                return view
            }()
            
            
            cell.addSubview(lineView)
            
            lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
            lineView.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 10).isActive = true
            lineView.rightAnchor.constraint(equalTo: cell.rightAnchor, constant: -10).isActive = true
            lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
            
            return cell
            
        }
        
        
        
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productDetailCell", for: indexPath) as! ProductDetailCollectionViewCell
        
        cell.delegate = self
        self.loadImages(cell: cell)
        
        cell.imageScrollView.isPagingEnabled = true
        cell.imageScrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat(self.product.pictures.count), height: 250)
        cell.imageScrollView.showsHorizontalScrollIndicator = false
        cell.imageScrollView.tag = 20
        cell.pageControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        cell.pageControl.numberOfPages = self.product.pictures.count
        
        cell.storeNameLabel.text = self.product.brand
        cell.productNameLabel.text = self.product.name
        
        cell.likeLabel.text = self.product.likes
        cell.wishLabel.text = self.product.wishes
        cell.shareLabel.text = self.product.shares
        
        if self.product.promo_price! == "" {
        
            cell.promoPriceLabel.isHidden = true
            cell.priceProductLabel.text = "R$ \(self.product.price!)"
            
        } else {
            
            cell.promoPriceLabel.isHidden = false
            
            let promo_price = "De R$ \(self.product.price!)".withStrikethroughStyle(.styleSingle).withBaselineOffset(0).withFont(.systemFont(ofSize: 12)).withTextColor(UIColor.darkGray)
            
            let por = " por ".withTextColor(UIColor.darkGray).withFont(.systemFont(ofSize: 12))
            
            cell.promoPriceLabel.attributedText = promo_price + por
            cell.priceProductLabel.text = "R$ \(self.product.promo_price!)"
    
        }
        
        
        if self.product.ilike == "true" {
            cell.likeImageView.image = UIImage(named: "Like Filled")
        } else {
            cell.likeImageView.image = UIImage(named: "Like")
        }
        
        if self.product.iwish == "true" {
            cell.wishImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
        } else {
            cell.wishImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
        }
        
        // FORMA DE PARCELAMENTO
        if self.product.price! != "" {
            
            if self.store.advance_commission! == "0" {
            
                let parcelMinimal : Float = 50.0
                var parcelIsValidate : Bool = false
            
                for i in (1...6).reversed() {
                    
                    var parcel : Float = 0.0
                    
                    if self.product.promo_price == "" {
                        parcel = (self.product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue / Float(i)
                        //parcel = (self.product.price!.components(separatedBy: ".").joined() as NSString).floatValue / Float(i)
                    } else {
                        parcel = (self.product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue / Float(i)
                        //parcel = (self.product.promo_price!.components(separatedBy: ".").joined() as NSString).floatValue / Float(i)
                    }
                    
                    
                    if parcel >= parcelMinimal {
                        cell.parcelProductLabel.text = "Até \(i)x de \(parcel.asLocaleCurrency)"
                        parcelIsValidate = true
                        break
                    } else {
                        continue
                    }
                    
                }
                
                if parcelIsValidate == false {
                    
                    if self.product.promo_price == "" {
                        cell.parcelProductLabel.text = "Até 1x de \((self.product.price! as NSString).floatValue.asLocaleCurrency)"
                    } else {
                        cell.parcelProductLabel.text = "Até 1x de \((self.product.promo_price! as NSString).floatValue.asLocaleCurrency)"
                    }
                    
                }
            
            } else {
           
                let parcelMinimal : Float = 100.0
                var parcelIsValidate : Bool = false
                
                for i in (1...6).reversed() {
                    
                    var parcel : Float = 0.0
                    
                    if self.product.promo_price == "" {
                        parcel = (self.product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue / Float(i)
                        //parcel = (self.product.price!.components(separatedBy: ".").joined() as NSString).floatValue / Float(i)
                    } else {
                        parcel = (self.product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).floatValue / Float(i)
                        //parcel = (self.product.promo_price!.components(separatedBy: ".").joined() as NSString).floatValue / Float(i)
                    }
                    
                    if parcel >= parcelMinimal {
                        cell.parcelProductLabel.text = "Até \(i)x de \(parcel.asLocaleCurrency)"
                        parcelIsValidate = true
                        break
                    } else {
                        continue
                    }
                    
                }
                
                if parcelIsValidate == false {
                    
                    if self.product.promo_price == "" {
                        cell.parcelProductLabel.text = "Até 1x de \((self.product.price! as NSString).floatValue.asLocaleCurrency)"
                    } else {
                       cell.parcelProductLabel.text = "Até 1x de \((self.product.promo_price! as NSString).floatValue.asLocaleCurrency)"
                    }
                    
                }
            }
            
        }
        
        cell.storePageLabel.text = self.store.name
        cell.descriptionProductLabel.lineBreakMode = .byTruncatingTail
        cell.descriptionProductLabel.text = self.product.desc
        descriptionHeight = cell.descriptionProductLabel.optimalHeight
        cell.descriptionProductLabel.sizeToFit()
        cell.referenceProductLabel.text = self.product.ref
        
        cell.descriptionProductLabel.preferredMaxLayoutWidth = self.collectionView.frame.width
        
        if self.product.model == "" {
        
            cell.colorToModelConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.colorToReferenceConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.colorToModelProductConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.colorToReferenceProductConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.modelLabel.isHidden = true
            
            
        } else {
            
            cell.colorToModelConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.colorToReferenceConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.colorToModelProductConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.colorToReferenceProductConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.modelLabel.isHidden = false
            cell.modelProductLabel.text = self.product.model
            
        }
        
        
        
        
        cell.colorProductLabel.text = self.product.color
        cell.brandProductLabel.text = self.product.brand
        
        if self.product.allColor.count > 0 {
            
            cell.descriptionToColorsConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.descriptionToSizesConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.otherColorView.isHidden = false
            
            cell.colorCollectionView.delegate = self
            cell.colorCollectionView.dataSource = self
            cell.colorCollectionView.register(UINib(nibName: "ColorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "colorCell")
            cell.colorCollectionView.tag = 10
            cell.colorCollectionView.reloadData()
            
        } else {
            
            // SE ELA FOR IGUAL A ZERO ESCONDER VIEW OUTRAS CORES
            
            cell.descriptionToColorsConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.descriptionToSizesConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.otherColorView.isHidden = true
            
        }
        
        if self.product.tags.count > 0 {
            
            if self.product.tags.first! != "" {
            
                cell.tagsView.isHidden = false
                
                var lastWidthButton : CGFloat = 5.0
                var lastTopButton : CGFloat = 5.0
                let screenSizeWidth : CGFloat = cell.tagsView.bounds.width - 10
            
                for tag in self.product.tags {
                
                    let tagButton : UIButton = UIButton(frame: CGRect.zero)
                    tagButton.sizeToFit()
                    tagButton.translatesAutoresizingMaskIntoConstraints = false
                    tagButton.setTitle(tag, for: .normal)
                    tagButton.setTitleColor(UIColor.darkGray, for: .normal)
                    tagButton.backgroundColor = UIColor.lightGray
                    tagButton.layer.cornerRadius = 5.0
                    tagButton.accessibilityHint = tag
                    tagButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
                
                
                    cell.tagsView.addSubview(tagButton)
                
                    if (screenSizeWidth - lastWidthButton) < (CGFloat(tag.characters.count) * 12.0) {
                    
                        lastTopButton += 30
                        lastWidthButton = 5.0
                    
                        tagButton.topAnchor.constraint(equalTo: cell.tagTitleLabel.bottomAnchor, constant: lastTopButton).isActive = true
                        tagButton.leftAnchor.constraint(equalTo: cell.tagsView.leftAnchor, constant: lastWidthButton).isActive = true
                        tagButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
                        tagButton.widthAnchor.constraint(equalToConstant: CGFloat(tag.characters.count) * 10.0).isActive = true
                        lastWidthButton += CGFloat(tag.characters.count) * 10.0 + 5.0
                    
                    
                    } else {

                
                        tagButton.topAnchor.constraint(equalTo: cell.tagTitleLabel.bottomAnchor, constant: lastTopButton).isActive = true
                        tagButton.leftAnchor.constraint(equalTo: cell.tagsView.leftAnchor, constant: lastWidthButton).isActive = true
                        tagButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
                        tagButton.widthAnchor.constraint(equalToConstant: CGFloat(tag.characters.count) * 10.0).isActive = true
                    
                        lastWidthButton += CGFloat(tag.characters.count) * 10.0 + 5.0
                    
                    }
                
                    tagButton.addTarget(self, action: #selector(pressOpenHashTag(_:)), for: .touchUpInside)
                
                }
            
                cell.tagsHeightConstraint.constant = 70.0 + lastTopButton
                
            } else {
                
                cell.tagsView.isHidden = true
                cell.tagsHeightConstraint.constant = 0
                
            }
            
        } else {
            
            cell.tagsView.isHidden = true
            cell.tagsHeightConstraint.constant = 0
            
        }
        
        if self.product.sizes.count > 0 {
            
            for subview in cell.sizeView.subviews {
                
                if let button = subview as? UIButton {
                    button.removeFromSuperview()
                }
            
            }
            
            var lastWidthSize : CGFloat = 5.0
            var lastTopSize : CGFloat = 5.0
            let screenSizeProductSize : CGFloat = cell.sizeView.bounds.width - 10
            
            for (_, size) in self.product.sizes.enumerated() {
                
                let sizeButton : UIButton = UIButton(frame: CGRect.zero)
                sizeButton.translatesAutoresizingMaskIntoConstraints = false
                
                
                if let productSize = self.products[self.product.id!] {
                    
                    if productSize[0] == size.id! {
                        
                        sizeButton.backgroundColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1)
                        sizeButton.setTitleColor(UIColor.darkGray, for: .normal)
                        sizeButton.layer.borderColor = UIColor(red: 227/255, green: 226/255, blue: 218/255, alpha: 1).cgColor
                    
                    } else {
                        
                        sizeButton.backgroundColor = UIColor.white
                        sizeButton.setTitleColor(UIColor.lightGray, for: .normal)
                        sizeButton.layer.borderColor = UIColor.black.cgColor
                        
                    }
                    
                    
                } else {
                    
                    sizeButton.backgroundColor = UIColor.white
                    sizeButton.setTitleColor(UIColor.lightGray, for: .normal)
                    sizeButton.layer.borderColor = UIColor.black.cgColor
                    
                }
                
            
                sizeButton.setTitle(size.size!.components(separatedBy: .whitespaces).joined(), for: .normal)
                sizeButton.titleLabel?.adjustsFontSizeToFitWidth = true
                
                sizeButton.layer.borderWidth = 1.0
                sizeButton.layer.cornerRadius = 2.0
                
                sizeButton.layer.masksToBounds = false
                sizeButton.layer.shadowColor = UIColor.black.cgColor
                sizeButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                sizeButton.layer.shadowRadius = 2.0
                sizeButton.layer.shadowOpacity = 0.24
                
                sizeButton.tag = Int(size.id!)!
                
                sizeButton.productID = self.product.id
                sizeButton.productSizeID = size.id
                sizeButton.productSize = size.size
                
                cell.sizeView.addSubview(sizeButton)
                
                
                if (screenSizeProductSize - lastWidthSize) <= 0 {
                    
                    lastTopSize += 50
                    lastWidthSize = 5.0
                    
                    
                    sizeButton.topAnchor.constraint(equalTo: cell.sizeTitleLabel.bottomAnchor, constant: lastTopSize).isActive = true
                    sizeButton.leftAnchor.constraint(equalTo: cell.sizeView.leftAnchor, constant: lastWidthSize).isActive = true
                    sizeButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
                    sizeButton.widthAnchor.constraint(equalToConstant: 45).isActive = true
                    lastWidthSize += 45 + 5.0
                    
                } else {
                    
                    sizeButton.topAnchor.constraint(equalTo: cell.sizeTitleLabel.bottomAnchor, constant: lastTopSize).isActive = true
                    sizeButton.leftAnchor.constraint(equalTo: cell.sizeView.leftAnchor, constant: lastWidthSize).isActive = true
                    sizeButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
                    sizeButton.widthAnchor.constraint(equalToConstant: 45).isActive = true
                    lastWidthSize += 45 + 5.0
                    
                }
                
                sizeButton.addTarget(self, action: #selector(pressSelectedSize(_:_:)), for: .touchUpInside)
                
            }
            
            cell.sizeHeightConstraint.constant = 80.0 + lastTopSize
            
        }
        
        if self.product.sizes_guide! == "" {
            cell.guideSizeButton.isHidden = true
        } else {
            cell.guideSizeButton.isHidden = false
            
            cell.guideSizeButton.titleLabel?.textAlignment = .center
            cell.guideSizeButton.backgroundColor = UIColor.white
            cell.guideSizeButton.setTitleColor(UIColor.darkGray, for: .normal)
            
            cell.guideSizeButton.layer.borderWidth = 1.0
            cell.guideSizeButton.layer.borderColor = UIColor.black.cgColor
            cell.guideSizeButton.layer.cornerRadius = 2.0
            
            cell.guideSizeButton.layer.masksToBounds = false
            cell.guideSizeButton.layer.shadowColor = UIColor.black.cgColor
            cell.guideSizeButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            cell.guideSizeButton.layer.shadowRadius = 2.0
            cell.guideSizeButton.layer.shadowOpacity = 0.24
        }
        
        if self.showMoreIsHidden {
        
            cell.tagsToDescription.priority = UILayoutPriority(rawValue: 999)
            cell.tagsToInformation.priority = UILayoutPriority(rawValue: 750)
            
            cell.moreTextConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.lessTextConstraint.priority = UILayoutPriority(rawValue: 750)
            
            
            
            cell.informationView.isHidden = self.showMoreIsHidden
            cell.moreDescriptionButton.isHidden = false
        
        } else {
            
            cell.tagsToDescription.priority = UILayoutPriority(rawValue: 750)
            cell.tagsToInformation.priority = UILayoutPriority(rawValue: 999)
            
            cell.moreTextConstraint.priority = UILayoutPriority(rawValue: 750)
            cell.lessTextConstraint.priority = UILayoutPriority(rawValue: 999)
            
            cell.informationView.isHidden = self.showMoreIsHidden
        
        }

        return cell
        
    }
}

extension ProductDetailViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag ==  10 {
            return CGSize(width: (self.view.bounds.width / 2) - 20.0, height: (self.view.bounds.width / 2) - 20.0)
        }
        
        if collectionView.tag == 11 {
            return CGSize(width: self.view.bounds.width - 20, height: 125)
        }
        
        if collectionView.tag == 75 {
            return CGSize(width: self.view.bounds.width, height: (self.view.bounds.height / 2) - 100)
        }
        

        
        
        if self.product.allColor.count > 0 {
            
            if self.product.tags.count > 0 {
                
                if self.product.tags.first! != "" {
                    
                    var lastWidthButton : CGFloat = 5.0
                    var lastTopButton : CGFloat = 5.0
                    let screenSizeWidth : CGFloat = self.view.bounds.width - 10
                    
                    for tag in self.product.tags {
                        
                        if (screenSizeWidth - lastWidthButton) < (CGFloat(tag.count) * 12.0) {
                            
                            lastTopButton += 30
                            lastWidthButton = 5.0
                            lastWidthButton += CGFloat(tag.count) * 10.0 + 5.0
                            
                        } else {
                            
                            lastWidthButton += CGFloat(tag.count) * 10.0 + 5.0
                            
                        }
                        
                    }
                    
                    lastTopButton = 70.0 + lastTopButton
                    
                    return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + ((self.showMoreIsHidden) ? (600 + lastTopButton) : (760 + lastTopButton)) + self.descriptionHeight)
                    
                } else {
                    
                    return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + ((self.showMoreIsHidden) ? 600 : 760) + self.descriptionHeight)
                
                }
                
            } else {
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + ((self.showMoreIsHidden) ? 600 : 760) + self.descriptionHeight)
            }
            
        } else {
            
            if self.product.tags.count > 0 {
                
                if self.product.tags.first! != "" {
                    
                    var lastWidthButton : CGFloat = 5.0
                    var lastTopButton : CGFloat = 5.0
                    let screenSizeWidth : CGFloat = self.view.bounds.width - 10
                    
                    for tag in self.product.tags {

                        if (screenSizeWidth - lastWidthButton) < (CGFloat(tag.count) * 12.0) {
                            
                            lastTopButton += 30
                            lastWidthButton = 5.0
                            lastWidthButton += CGFloat(tag.count) * 10.0 + 5.0
                            
                        } else {
                            
                            lastWidthButton += CGFloat(tag.count) * 10.0 + 5.0
                            
                        }
                        
                    }
                    
                     lastTopButton = 70.0 + lastTopButton
                    
                    return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + ((self.showMoreIsHidden) ? (440 + lastTopButton) : (600 + lastTopButton)) + self.descriptionHeight)
                
                } else {
                    return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + ((self.showMoreIsHidden) ? 420 : 580) + self.descriptionHeight)
                }
            
            } else {
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + ((self.showMoreIsHidden) ? 420 : 580) + self.descriptionHeight)
            }
            
        }
        

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        

    }
    
}

extension ProductDetailViewController : ProductDetailDelegate {
    
    func scrollViewDidScrollProduct(_ scrollView: UIScrollView, cell: ProductDetailCollectionViewCell) {
        
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        self.indexCurrentPicture = Int(page)
        cell.pageControl.currentPage = Int(page)
        
    }
    
    func openPageStoreByProductDetail() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
        pageStoreViewController.idStore = self.store.id!
        
        self.navigationController?.pushViewController(pageStoreViewController, animated: true)
        
    }

    func moreDescription(_ sender: UIButton) {
        self.showMoreIsHidden = false
        sender.isHidden = true
        self.products.removeAll()
        self.collectionView.reloadData()
    }
    
    func lessDescription(_ sender: UIButton) {
        self.showMoreIsHidden = true
        self.products.removeAll()
        self.collectionView.reloadData()
    }
    
    func showGuideSize(_ sender: UIButton) {
        
        let popup = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: self.view.bounds.height - 100))
        
        let titleSize : UILabel = {
            let label = UILabel()
            label.text = "GUIA DE TAMANHOS"
            label.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let buttonExit : UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "Delete"), for: .normal)
            button.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            button.imageView?.contentMode = .scaleAspectFit
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
            return button
        }()
        

        
        
        self.guideSizeWebView.loadHTMLString(self.product.sizes_guide!, baseURL: nil)
        
        popup.addSubview(self.guideSizeWebView)
        popup.addSubview(titleSize)
        popup.addSubview(buttonExit)
        
        buttonExit.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
        buttonExit.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        buttonExit.heightAnchor.constraint(equalToConstant: 40).isActive = true
        buttonExit.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleSize.topAnchor.constraint(equalTo: popup.topAnchor, constant: 5).isActive = true
        titleSize.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
        titleSize.rightAnchor.constraint(equalTo: buttonExit.leftAnchor).isActive = true
        titleSize.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        self.guideSizeWebView.leftAnchor.constraint(equalTo: popup.leftAnchor).isActive = true
        self.guideSizeWebView.topAnchor.constraint(equalTo: titleSize.bottomAnchor).isActive = true
        self.guideSizeWebView.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        self.guideSizeWebView.bottomAnchor.constraint(equalTo: popup.bottomAnchor, constant: -10).isActive = true
        
        
        let popupConfig = STZPopupViewConfig()
        popup.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        popupConfig.cornerRadius = 5
        popupConfig.overlayColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        popupConfig.showAnimation = .slideInFromBottom
        popupConfig.dismissAnimation = .slideOutToBottom
        
        presentPopupView(popup, config: popupConfig)

        
    }
    
    func likeProduct() {
        
        if self.product.ilike! == "false" {
            
            self.product.ilike = "true"
            self.product.likes = "\(Int(self.product.likes!)! + 1)"
            
            self.collectionView.reloadData()
            WishareAPI.likeProduct(self.product.id!, true, { (isValidate : Bool, error : Error?) in })
            
        } else {
            
            self.product.ilike = "false"
            self.product.likes = "\(Int(self.product.likes!)! - 1)"
            self.collectionView.reloadData()
            WishareAPI.likeProduct(self.product.id!, false, { (isValidate : Bool, error : Error?) in })
            
        }
        
    }
    
    func wishProduct() {
        
        if self.product.iwish! == "false" {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.product.iwish = "true"
            self.product.wishes = "\(Int(self.product.wishes!)! + 1)"
            
            self.present(alertController, animated: true, completion: nil)
            self.collectionView.reloadData()
            WishareAPI.wishlistProduct(self.product.id!, true, { (error : Error?) in })
            
        } else {
            
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                self.product.iwish = "false"
                self.product.wishes = "\(Int(self.product.wishes!)! - 1)"
                self.collectionView.reloadData()
                WishareAPI.wishlistProduct(self.product.id!, false, { (error : Error?) in })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    func shareProduct() {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            if self.product.ishare! == "false" {
                self.product.ishare = "true"
                self.product.shares = "\(Int(self.product.shares!)! + 1)"
                self.collectionView.reloadData()
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.product.pId!)/\(self.product.picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.product.id!])
            }
            
        }

        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.product.pId!)/\(self.product.picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 97.5, y: result.value!.size.height - 35, width: 87.5, height: 25), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
            }
        
            
        }

        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func showLikes() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let likesAndWishesViewController = storyboard.instantiateViewController(withIdentifier: "productLikesAndWishesID") as! LikesAndWishesViewController
        likesAndWishesViewController.product = self.product
        likesAndWishesViewController.isLikeOrWish = true
        
        self.navigationController?.pushViewController(likesAndWishesViewController, animated: true)
        
    }
    
    func showWishes() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let likesAndWishesViewController = storyboard.instantiateViewController(withIdentifier: "productLikesAndWishesID") as! LikesAndWishesViewController
        likesAndWishesViewController.product = self.product
        likesAndWishesViewController.isLikeOrWish = false
        
        self.navigationController?.pushViewController(likesAndWishesViewController, animated: true)
        
    }
    
}

extension ProductDetailViewController : CollieGalleryDelegate {
    
    func gallery(_ gallery: CollieGallery, indexChangedTo index: Int) {
        
    }
    
}

extension ProductDetailViewController : CollieGalleryZoomTransitionDelegate {
    
    func zoomTransitionContainerBounds() -> CGRect {
        return self.view.frame
    }
    
    func zoomTransitionViewToDismissForIndex(_ index: Int) -> UIView? {
        return nil
    }
    
}
