//
//  RegisterViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import SwiftValidator
import RappleProgressHUD

class RegisterViewController: UIViewController {

    
    // MARK: - Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var genreSegmentedControl: UISegmentedControl!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backgroundScrollView: UIScrollView!
    
    @IBOutlet weak var nameErrorLabel: UILabel!
    @IBOutlet weak var lastNameErrorLabel: UILabel!
    @IBOutlet weak var dateErrorLabel: UILabel!
    @IBOutlet weak var stateErrorLabel: UILabel!
    @IBOutlet weak var userNameError: UILabel!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var termsLabel: UILabel!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var activeTextField: UITextField!
    
    
    // MARK: - Properties
    let statePickerView = ["AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SE","SP","TO"]
    var birthday_date : Int64?
    let validator : Validator = Validator()
    var fbUserData : [String : AnyObject]?
    var facebook_id : String?
    var facebook_picture : String?
    
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Metodo que esconde o teclado quando houver um tap na UIScrollView
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        
        let tapGestureRecognizerTerms = UITapGestureRecognizer(target: self, action: #selector(openTerms))
        self.termsLabel.addGestureRecognizer(tapGestureRecognizerTerms)
        
        tapGesture.cancelsTouchesInView = false
        self.backgroundScrollView.addGestureRecognizer(tapGesture)
        
        // Picker View
        let pickerView = UIPickerView()
        pickerView.delegate = self
        self.stateTextField.inputView = pickerView
        
        // Definindo Delegate
        self.nameTextField.delegate = self
        self.lastNameTextField.delegate = self
        self.dateTextField.delegate = self
        self.stateTextField.delegate = self
        self.userNameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.userNameTextField.tag = 1909
        
        // SwiftValidator
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        validator.styleTransformers(success: { (validationRule : ValidationRule) in
            
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.clear.cgColor
                textField.layer.borderWidth = 0.0
            }
            
            
        }) { (validationError : ValidationError) in
            
            
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
            
        }
        
        
        // Registrando campos para validacao
        validator.registerField(self.nameTextField, errorLabel: self.nameErrorLabel, rules: [RequiredRulePt()])
        validator.registerField(self.lastNameTextField, errorLabel: self.lastNameErrorLabel, rules: [RequiredRulePt()])
        validator.registerField(self.dateTextField, errorLabel: self.dateErrorLabel, rules: [UserBirthdayRule()])
        validator.registerField(self.stateTextField, errorLabel: self.stateErrorLabel, rules: [RequiredRulePt()])
        
        validator.registerField(self.userNameTextField, errorLabel: self.userNameError, rules: [RequiredRulePt(), UserLengthPt(), UserExistRule()])
        
        validator.registerField(self.emailTextField, errorLabel: self.emailErrorLabel, rules: [RequiredRulePt(), EmailRulePt(), EmailExistRule()])
        validator.registerField(self.passwordTextField, errorLabel: self.passwordErrorLabel, rules: [RequiredRulePt(), PasswordRulePt()])
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardEvent(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardEvent(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let fbUser = self.fbUserData {
            
            self.passwordTextField.isHidden = true
            self.passwordErrorLabel.isHidden = true
            validator.unregisterField(self.passwordTextField)
            
            genreSegmentedControl.selectedSegmentIndex = -1
        
            if let name : String = fbUser["first_name"] as? String {
                
                self.nameTextField.text = name
                
            }
            
            
            if let last_name : String = fbUser["last_name"] as? String {
                
                self.lastNameTextField.text = last_name
                
            }
            
            if let username : String = fbUser["name"] as? String {
                
                let usernameNotSpace = username.replacingOccurrences(of: " ", with: "")
                self.userNameTextField.text = usernameNotSpace.lowercased()
                
            }
            
            
            if let gender : String = fbUser["gender"] as? String {
                
                if gender == "male" {
                    
                    self.genreSegmentedControl.selectedSegmentIndex = 1
                    
                } else {
                    
                    self.genreSegmentedControl.selectedSegmentIndex = 0
                    
                }
                
                
            }
            
            
            if let email = fbUser["email"] {
                
                self.emailTextField.text = email as? String
            
                
                
                if let pictureData : [String : AnyObject] = fbUser["picture"] as? [String : AnyObject] {
                    
                    if let urlPicture : [String : AnyObject] = pictureData["data"] as? [String : AnyObject]{
                    
                        self.facebook_picture = urlPicture["url"] as? String
                        
                    }
                    
                }
                
                
            }
            
            
            self.passwordTextField.text = "\(round(Double(1000 * 1000)))"
            
            if let faceId = fbUser["id"] as? String {
                
                self.facebook_id = faceId
            
            }
            
        }
        
        RappleActivityIndicatorView.stopAnimation()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
    // MARK: - Notification Listener
    
    @objc func keyboardEvent(notification: Notification) {
        
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if notification.name == .UIKeyboardWillShow {
//            if activeTextField.frame.maxY < keyboardRect.height {
//                self.view.frame.origin.y = -keyboardRect.height
//            }
            
            let keyboardFrame = self.view.convert(keyboardRect, from: nil)
            
            var contentInset:UIEdgeInsets = self.backgroundScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height + 5
            backgroundScrollView.contentInset = contentInset
            
        }
        
        if notification.name == .UIKeyboardWillHide {

            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            backgroundScrollView.contentInset = contentInset
            
        }
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func textFieldBegin(_ sender: UITextField) {
        let datePicker : UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
    }
    
    
    @IBAction func existsLogin(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        
        self.loadingActivityIndicator.startAnimating()
        self.backgroundScrollView.isHidden = true
        self.validator.validate(self)
    
    }
    
    
    // MARK: - Utils
    
    @objc func datePickerValueChanged( _ sender : UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/Y" 
        self.birthday_date = Int64(sender.date.timeIntervalSince1970)
        dateTextField.text = dateFormatter.string(from: sender.date)
        
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func openTerms() {
        UIApplication.shared.open(URL(string: "http://www.wishare.com.br/code/users/getTerms")!, options: [:], completionHandler: nil)
    }
    
    
}


extension RegisterViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statePickerView.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statePickerView[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.stateTextField.text = statePickerView[row]
    }
    
}

extension RegisterViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
        if textField == nameTextField || textField == lastNameTextField {
            textField.text = textField.text?.capitalized
        }
        self.validator.validateField(textField) { (validationError : ValidationError?) in }

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 1909 {
            
            let notCharacters = "`~!@#$%^&*()+=\\|]}[{'\":;/?.><,àãáäâÀÃÁÄÂèéëêÈÉËÊìíïîÌÍÏÎòõöóôÒÕÖÓÔùúüûÙÚÜÛ "
            
            if notCharacters.contains(string) {
                return false
            }
            
        }
        
        return true
    }
    
}

extension RegisterViewController : ValidationDelegate {
    
    func validationSuccessful() {
        print("VALIDACAO SUCESSO")
        
        let user : User = User()
        var userName : String!
        
        user.isSocial = false
        
        if let name = self.nameTextField.text {
            user.name = name
        }
        
        if let lastname = self.lastNameTextField.text {
            user.last_name = lastname
        }
        
        var genre : String?
        
        if self.genreSegmentedControl.selectedSegmentIndex == 0 {
            genre = "F"
        } else if self.genreSegmentedControl.selectedSegmentIndex == 1 {
            genre = "M"
        } else {
            genre = nil
        }
        
        user.gender = genre
        
        user.birthday_date = self.birthday_date

        
        
        if let state = self.stateTextField.text {
            user.state = state
        }
        
        if let username = self.userNameTextField.text {
            user.username = username
            userName = username
        }
        
        if let email = self.emailTextField.text {
            user.email = email
        }
        
        if let password = self.passwordTextField.text {
            user.password = password
        }
        
        
        if let faceId = self.facebook_id {
            
            user.id_face = faceId
            user.id_face_password = faceId
            user.picture = self.facebook_picture
            userName = faceId
            user.password = faceId
            user.isSocial = true
            
            
        }
        
        
        
        WishareAPI.userCreateOrFails(user, user.isSocial!, { (isCreated : Bool, error : Error?) in
                
            if error == nil && isCreated != false {
                    
                WishareAPI.userAuthorization(userName, user.password!, user.isSocial!, { (isLogIn : Bool, error : Error?, firstAccess : String?) in
                        
                    if error == nil && isLogIn != false {
                        
                        // FIRST ACCESS
                        if firstAccess! == "0" {
                            self.performSegue(withIdentifier: "startLoginSegue", sender: nil)
                        } else {
                            self.performSegue(withIdentifier: "facebookFirstAccessSegue", sender: nil)
                        }
                            
                    } else {
                        
                        self.loadingActivityIndicator.stopAnimating()
                        self.backgroundScrollView.isHidden = false
                        
                        let actionController = UIAlertController(title: "Erro ao cadastrar-se", message: "Erro durante realizar o cadastro! Tente novamente mais tarde", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                        actionController.addAction(okAction)
                        self.present(actionController, animated: true, completion: nil)
                            
                            
                    }
                        
                })
                    
            }
                
        })
            
        
        
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("VALIDACAO FALHOU")
        self.loadingActivityIndicator.stopAnimating()
        self.backgroundScrollView.isHidden = false
    }
    
}


