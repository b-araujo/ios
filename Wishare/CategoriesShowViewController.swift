//
//  CategoriesShowViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 8/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class CategoriesShowViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var category_id : String!
    var productsTop : [Product] = []
    var categories : Categories? = nil
    
    var loader : Loader!
    var manager : Manager!
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.tag = 10
        self.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
        self.collectionView.register(UINib(nibName: "TopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "topCell")
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando Produtos...", attributes: attributes)
        
        WishareAPI.recommendations(self.category_id) { (categories : Categories?, productsTop : [Product]?, error : Error?) in
            if error == nil {
                
                self.productsTop = productsTop!
                self.categories = categories
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

extension CategoriesShowViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        NotificationCenter.default.post(name: NSNotification.Name.init("isLoadingOrNot"), object: nil)
        
        if collectionView.tag == 11 {
            NotificationCenter.default.post(name: NSNotification.Name.init("openProductDetail"), object: self.productsTop[indexPath.row].id!)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name.init("openProductDetail"), object: self.categories!.category.first!.products[indexPath.row - 1].id!)
        }
        
    }
    
}

extension CategoriesShowViewController : UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 10 {
            if categories != nil {
                return self.categories!.category.first!.products.count
            } else {
                return 0
            }
        }
        
        return self.productsTop.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 11 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.productsTop[indexPath.row].pId!)/thumb/\(self.productsTop[indexPath.row].picture!)")
            
            cell.imageProductsLook.image = nil
            cell.loadingActivityIndicator.startAnimating()
            
            self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.imageProductsLook.image = result.value
            }
            
            return cell
            
        }
        
        
        if indexPath.row == 0 && collectionView.tag == 10 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topCell", for: indexPath) as! TopCollectionViewCell
            
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
            cell.collectionView.tag = 11
            cell.collectionView.isPrimary = "true"
            cell.collectionView.backgroundColor = UIColor.white
            cell.collectionView.reloadData()
            
            return cell
            
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/products/\(self.categories!.category.first!.products[indexPath.row - 1].pId!)/\(self.categories!.category.first!.products[indexPath.row - 1].picture!)")
        
        cell.imageProductsLook.image = nil
        cell.loadingActivityIndicator.startAnimating()
        
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        return cell
        
    }
    
    
}

extension CategoriesShowViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        

        if indexPath.row == 0 && collectionView.tag == 10 {
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width / 2)
        }
        
        
        if collectionView.tag == 11 {
            return CGSize(width: self.view.bounds.width / 2, height: self.view.bounds.width / 2)
        }
        
        return CGSize(width: (self.view.bounds.width / 3) - 2.5, height: (self.view.bounds.width / 3) - 5)
        
    }
    
}
