//
//  ProblemReportViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/14/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD

class ProblemReportViewController: UIViewController {

    @IBOutlet weak var titleReportLabel: UILabel!
    @IBOutlet weak var commentReportLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var titleReport : String?
    var commentReport : String?
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleReportLabel.text = self.titleReport
        self.commentReportLabel.text = self.commentReport
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Enviar", style: .plain, target: self, action: #selector(sendReport))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        
//        let logoImageView = UIImageView(image: UIImage(named: "logo_login"))
//        logoImageView.frame.size.width = 100;
//        logoImageView.frame.size.height = 30;
//        logoImageView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = logoImageView

        self.navigationItem.title = self.titleReport
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sendReport() {
        
        if self.descriptionTextView.text != "" {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Enviando...", attributes: attributes)
            
            WishareAPI.reportProblem(self.titleReportLabel.text!, self.descriptionTextView.text) { (error : Error?) in
                if error == nil {
                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Enviado", completionTimeout: 1)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Preencha a descrição", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    
    }

}
