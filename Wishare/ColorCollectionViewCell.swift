//
//  ColorCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class ColorCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var productColorImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.colorView.layer.cornerRadius = 5.0
        self.layer.cornerRadius = 5.0
    }

}
