//
//  WSWKViewController.swift
//  Wishare
//
//  Created by Wishare on 28/03/18.
//  Copyright © 2018 Wishare iMac. All rights reserved.
//

import UIKit
import WebKit

class WSWKViewController: UIViewController, WKNavigationDelegate {

    var wkWebView : WKWebView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wkWebView.frame = self.view.frame
        self.view = wkWebView
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
