//
//  LikesAndWishesViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/28/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class LikesAndWishesViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    
    var loader : Loader!
    var manager : Manager!
    
    var idUser : String = "X"
    var users : [User] = []
    
    var product : Product!
    var isLikeOrWish : Bool = false
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LikeUserTableViewCell", bundle: nil), forCellReuseIdentifier: "likesCell")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.navigationItem.title = (self.isLikeOrWish) ? "Curtidas" : "Wishes"
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.likeAndWishProductDetail(self.product.pId!, type: self.isLikeOrWish) { (users : [User], error : Error?) in
            
            if error == nil {
                
                WishareCoreData.getFirstUser({ (user : UserActive?, error : Error?) in
                    
                    if error ==  nil && user != nil {
                    
                        self.idUser = user!.id!
                        self.users = users
                        self.tableView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    
                    }
                    
                })
                
                
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

extension LikesAndWishesViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

}

extension LikesAndWishesViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "likesCell", for: indexPath) as! LikeUserTableViewCell
        
        cell.indexPathNow = indexPath
        cell.delegate = self
        cell.userNameButton.setTitle(self.users[indexPath.row].username, for: .normal)
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.users[indexPath.row].picture!)")
        
        if self.users[indexPath.row].following! || self.users[indexPath.row].id! == self.idUser {
            cell.followUser.isHidden = true
        } else {
            cell.followUser.isHidden = false
        }
        
        self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        return cell
        
    }
    
}

extension LikesAndWishesViewController : LikeUserTableViewCellDelegate {
    
    func followLikeUser(sender: UIButton, cell: LikeUserTableViewCell) {
        print("SEGUIR USUARIO")
        
        if sender.title(for: .normal) == "Seguir" {
            
            sender.backgroundColor = UIColor.white
            sender.setTitle("Seguindo", for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            sender.layer.borderWidth = 2.0
            sender.layer.borderColor = UIColor.black.cgColor
            
        } else {
            
            sender.backgroundColor = UIColor.black
            sender.setTitle("Seguir", for: .normal)
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.layer.borderWidth = 0.0
            sender.layer.borderColor = UIColor.clear.cgColor
            
        }
        
    }
    
    func showUserLikeUser(cell: LikeUserTableViewCell) {
        print("ABRIR TELA")
    }
    
}
