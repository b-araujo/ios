//
//  CommentViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import SwiftyAttributes
import ActiveLabel

class CommentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageInputContainerView: UIView!
    @IBOutlet weak var inputMessageTextField: UITextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    
    var post : Post!
    var coments : [Comment] = []
    var comentsEdit : [Comment] = []
    var loader : Loader!
    var manager : Manager!
    var bottomConstraint : NSLayoutConstraint?
    var isEditingMessage : Bool = false
    var idMessageEditing : String = "0"
    var idUser : String = "0"
    var myComment : Comment!
    
    
    // - Mentions
    var previewCaracter : String = "-1"
    
    var mentions : [Mention] = []
    var mentionShow : [Mention] = []
    var isMention : Bool = false
    var nameMention : String = ""
    var indexMention : [Int] = []
    var indexObjectMetion : [Int : [Int]] = [:]
    var messageComment : String = ""
    var messageCommentObject : String = ""
    
    var mentionTableView : UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.white
        tableView.tag = 10
        tableView.isHidden = true
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.mentionTableView.delegate = self
        self.mentionTableView.dataSource = self
        self.mentionTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.mentionTableView.register(UINib(nibName: "UserMentionTableViewCell", bundle: nil), forCellReuseIdentifier: "mentionCell")
        self.mentionTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.mentionTableView.estimatedRowHeight = 65.0
        self.mentionTableView.rowHeight = 65.0
        
        self.view.addSubview(self.mentionTableView)
        
        self.mentionTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.mentionTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.mentionTableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 64).isActive = true
        self.mentionTableView.bottomAnchor.constraint(equalTo: self.messageInputContainerView.topAnchor).isActive = true
        
        //self.myComment = Comment(username: self.post.user!.username, text: (self.post.share_text != nil) ? self.post.share_text : self.post.text, picture: self.post.user!.picture)
        self.myComment = Comment(username: self.post.user!.username, text: (self.post.share_text != nil) ? self.post.share_text : self.post.text, picture: self.post.user!.picture, nameStore: self.post.store!.name, pictureStore: self.post.store!.picture)
        
        self.myComment.user!.id = self.post.user!.id
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil  {
                self.idUser = user!.id!
            }
            
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "UserActionTableViewCell", bundle: nil), forCellReuseIdentifier: "userActionCell")
        self.tableView.estimatedRowHeight = 85
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.sendMessageButton.imageView?.contentMode = .scaleAspectFit
        
        let atributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando comentários", attributes: atributes)
        
        WishareAPI.comments(self.post.id!, false, nil, nil) { ( comments : [Comment]?, error : Error?) in
            
            if error == nil {
                
                self.myComment.content! += " " + self.post.hashtag!.components(separatedBy: ",").joined(separator: " ")
                self.coments.append(self.myComment)
                self.coments.append(contentsOf: comments!)
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                self.inputMessageTextField.becomeFirstResponder()
                
                WishareAPI.commentMentions({ (mentions : [Mention], error : Error?) in
                    
                    if error == nil {
        
                        self.mentions.append(contentsOf: mentions)
                        self.mentionTableView.reloadData()
                        
                    }
                    
                })
                
            }
            
        }
        
        bottomConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillHide, object: nil)
        
        self.inputMessageTextField.delegate = self
        
    }
    
    @objc func handleKeyBoardNotification(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
            let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            
            let indexPath = IndexPath(item: (self.coments.count != 0) ? self.coments.count - 1 : 0, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
        }
        
    }

    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tableView.resignFirstResponder()
    }
    
    @IBAction func sendingMessage(_ sender: UIButton) {
        
        self.isMention = false
        
        if !self.isEditingMessage {
            
            let atributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
            RappleActivityIndicatorView.startAnimatingWithLabel("Enviando comentário", attributes: atributes)
            
            WishareAPI.comments(self.post.id!, true, 4, self.messageCommentObject) { (comments : [Comment]?, error : Error?) in
                
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation()
                    self.coments.removeAll()
                    self.coments.append(self.myComment)
                    self.coments.append(contentsOf: comments!)
                    self.inputMessageTextField.text = nil
                    self.tableView.reloadData()
                    
                }
                
            }
            
        } else {
            
            let atributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
            RappleActivityIndicatorView.startAnimatingWithLabel("Editando comentário", attributes: atributes)
            
            WishareAPI.commentEdit(self.idMessageEditing, self.inputMessageTextField.text!, { (error : Error?) in
                
                if error == nil {
                    
                    WishareAPI.comments(self.post.id!, false, nil, nil, { (comments : [Comment]?, error : Error?) in
                        
                        if error == nil {
                            
                            RappleActivityIndicatorView.stopAnimation()
                            self.inputMessageTextField.text = nil
                            self.coments.removeAll()
                            self.coments.append(self.myComment)
                            self.coments.append(contentsOf: comments!)
                            
                            self.navigationItem.title = "Comentários"
                            self.view.endEditing(true)
                            self.navigationItem.rightBarButtonItem = nil
                            self.tableView.backgroundColor = UIColor.white
                            self.isEditingMessage = false
                            self.tableView.reloadData()
                            
                        }
                        
                    })
                    
                }
                
            })
            
        }
        

        
    }
    
    @objc func cancelEditing() {
        
        self.navigationItem.title = "Comentários"
        self.view.endEditing(true)
        self.navigationItem.rightBarButtonItem = nil
        self.tableView.backgroundColor = UIColor.white
        self.inputMessageTextField.text = nil
        
        self.isEditingMessage = false
        self.coments.removeAll()
        self.coments.append(self.myComment)
        self.coments = self.comentsEdit
        self.comentsEdit.removeAll()
        self.tableView.reloadData()
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
        
            if let lastId = self.coments.last!.id {
            
                WishareAPI.moreComments(self.post.id!, lastId, { (commentsResult : [Comment]?, error : Error?) in
                    if error == nil {
                        self.coments.append(contentsOf: commentsResult!)
                        self.tableView.reloadData()
                    }
                })
                
                print("carregar novas comentarios")
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pageStoreSegue" {
            let pageStoreViewController = segue.destination as! PageStoreViewController
            pageStoreViewController.idStore = sender as! String
        }
        
    }
    
}

extension CommentViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 10 {
    
            self.inputMessageTextField.text = "\(self.messageComment)@\(self.mentionShow[indexPath.row].username!)"
            
            self.messageComment = "\(self.messageComment)@\(self.mentionShow[indexPath.row].username!)"

            self.indexObjectMetion[self.indexMention.last!] = []
            self.indexObjectMetion[self.indexMention.last!]!.append(self.messageCommentObject.count)
            self.messageCommentObject = "\(self.messageCommentObject)\(self.mentionShow[indexPath.row].objectMentin!)"
            self.indexObjectMetion[self.indexMention.last!]!.append(self.messageCommentObject.count)
            
            print(self.messageComment)
            print(self.messageCommentObject)
            
            self.mentionTableView.isHidden = true
            self.isMention = false
            
        } else {
            self.view.resignFirstResponder()
        }
    
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView.tag == 10 {
            return false
        }
        
        if indexPath.row == 0 {
            return false
        }
        
        return true
    }
    
    func deleteImage(forHeight height: CGFloat, _ image : UIImage, _ color : UIColor) -> UIImage {
        
        let frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(62), height: height)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(62), height: height), false, UIScreen.main.scale)
        let context: CGContext? = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(frame)
        image.draw(in: CGRect(x: CGFloat(frame.size.width / 2.0 - 4), y: CGFloat(frame.size.height / 2.0 - 15), width: CGFloat(27), height: CGFloat(30)))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    
    }
    
    func denunciarImage(forHeight height: CGFloat, _ image : UIImage, _ color : UIColor) -> UIImage {
        
        let frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(62), height: height)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(62), height: height), false, UIScreen.main.scale)
        let context: CGContext? = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(frame)
        image.draw(in: CGRect(x: CGFloat(frame.size.width / 2.0 - 4), y: CGFloat(frame.size.height / 2.0 - 13.5), width: CGFloat(27), height: CGFloat(27)))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .default, title: "          ") { (action, indexPath) in
            
            WishareAPI.commentDelete(self.coments[indexPath.row].id!, { (error : Error?) in })
            self.coments.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
        
        let commentCell = tableView.cellForRow(at: indexPath)
        let height = commentCell?.frame.size.height
        let backgroundImage = deleteImage(forHeight: height!, UIImage(named: "delete_comment")!, UIColor.red)
        delete.backgroundColor = UIColor(patternImage: backgroundImage)

        
        
        let edit = UITableViewRowAction(style: .normal, title: "          ") { (action, indexPath) in
            
            self.navigationItem.title = "Editando comentário"
            self.inputMessageTextField.becomeFirstResponder()
            tableView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(self.cancelEditing))
            
            self.isEditingMessage = true
            self.comentsEdit = self.coments
            self.coments.removeAll()
            self.coments.append(self.comentsEdit[indexPath.row])
            self.inputMessageTextField.text = self.coments[0].content
            self.idMessageEditing = self.coments[0].id!
            self.tableView.reloadData()
            
        }
        
        let commentCellEdit = tableView.cellForRow(at: indexPath)
        let heightEdit = commentCellEdit?.frame.size.height
        let backgroundImageEdit = deleteImage(forHeight: heightEdit!, UIImage(named: "edit_comment")!, UIColor.lightGray)
        edit.backgroundColor = UIColor(patternImage: backgroundImageEdit)
        
        
        let denunciar = UITableViewRowAction(style: .normal, title: "          ") { (action, indexPath) in
            WishareAPI.commentToReport(self.coments[indexPath.row].id!, { (error : Error?) in })
            self.tableView.reloadData()
            let alertController = UIAlertController(title: "Algo está errado?", message: "Obrigado pela informação", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        let commentCellDenunciar = tableView.cellForRow(at: indexPath)
        let heightDenunciar = commentCellDenunciar?.frame.size.height
        let backgroundImageDenunciar = denunciarImage(forHeight: heightDenunciar!, UIImage(named: "denunciar_comment")!, UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1))
        denunciar.backgroundColor = UIColor(patternImage: backgroundImageDenunciar)
        

        if indexPath.row != 0 {
        
            // VERIFICAR ID DO USER NIL
            let commentUserId = self.coments[indexPath.row].user!.id == nil ? "x" : self.coments[indexPath.row].user!.id!
            
            if self.idUser == commentUserId {
                return [delete, edit]
            } else if self.post.user!.id == self.idUser {
                return [delete, denunciar]
            } else {
                return [denunciar]
            }
            
        } else {
            return nil
        }
        
    }
}

extension CommentViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 10 {
            return self.mentionShow.count
        }
        
        return self.coments.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        // - Mentions
        if tableView.tag == 10 {
            
            //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "mentionCell", for: indexPath) as! UserMentionTableViewCell
            
            cell.userNameLabel.text = self.mentionShow[indexPath.row].username
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let urlPerfil : URL!
            
            if self.mentionShow[indexPath.row].objectMentin.contains("@storename") {
                urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.mentionShow[indexPath.row].picture!)")
            } else {
                urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.mentionShow[indexPath.row].picture!)")
            }
            
            
            cell.loadingAcitivityIndicator.startAnimating()
            
            self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                cell.loadingAcitivityIndicator.stopAnimating()
                cell.userImageView.image = result.value?.circleMask
            }
            
            
            //cell.textLabel?.text = self.mentionShow[indexPath.row].username
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userActionCell", for: indexPath) as! UserActionTableViewCell
        
        let username = self.coments[indexPath.row].user!.username
        let storeName = self.coments[indexPath.row].store!.name
        
        if username != nil && storeName != nil {
            cell.nameUserButton.setTitle(username, for: .normal)
        } else if username == nil && storeName != nil {
            cell.nameUserButton.setTitle(storeName, for: .normal)
        } else if username != nil && storeName == nil {
            cell.nameUserButton.setTitle(username, for: .normal)
        } else {
            cell.nameUserButton.setTitle("Wishare", for: .normal)
        }
        
//        cell.nameUserButton.setTitle((self.coments[indexPath.row].user!.username != nil) ? self.coments[indexPath.row].user!.username! : self.coments[indexPath.row].store!.name!, for: .normal)
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        
        // =========== Hashtags/Mention ===========
        
        cell.comentLabel.enabledTypes = [.hashtag, .mention]
        
        cell.comentLabel.handleHashtagTap({ (hashtag : String) in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
            showProductHashTagsViewController.textHashTag = hashtag
            
            self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
            
        })
        
        cell.comentLabel.handleMentionTap({ (mention : String) in
            
            if (self.coments[indexPath.row].content!.contains("@id:") && self.coments[indexPath.row].content!.contains("@username:")) || (self.coments[indexPath.row].content!.contains("@id:") && self.coments[indexPath.row].content!.contains("@storename:")) {
                
                let comment = self.coments[indexPath.row].content!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                
                var isActiveMention = false
                
                for word in comment {
                    
                    let information_comments = word.components(separatedBy: ":")
                    
                    if (word.contains("@id:") && word.contains("@storename:")) {
                        
                        if information_comments.last! == mention && isActiveMention == false {
                            
                            isActiveMention = true
                            self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                            
                        }
                        
                        
                    } else if (word.contains("@id:") && word.contains("@username:"))  {
                        
                        if information_comments.last! == mention && isActiveMention == false {
                            
                            isActiveMention = true
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                            pageUserViewController.isMyProfile = false
                            pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                            
                            self.navigationController?.pushViewController(pageUserViewController, animated: true)
                            
                        }
                        
                    }
                    
                    
                }
                
                
            }
            
            
        })
        
        if (self.coments[indexPath.row].content!.contains("@id:") && self.coments[indexPath.row].content!.contains("@username:")) || (self.coments[indexPath.row].content!.contains("@id:") && self.coments[indexPath.row].content!.contains("@storename:")) {
            
            let comment = self.coments[indexPath.row].content!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                return value != "" ? true : false
            })
            
            var comments_final : String = ""
            
            for word in comment {
                
                if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                    let information_comments = word.components(separatedBy: ":")
                    comments_final += "@\(information_comments.last!)"
                } else {
                    comments_final += word
                }
                
            }
            
            cell.comentLabel.text = comments_final
            
        } else {
            cell.comentLabel.text = self.coments[indexPath.row].content
        }
        
        // ========================================
        
        
        
        
        
//        if (self.coments[indexPath.row].content!.contains("@id:") && self.coments[indexPath.row].content!.contains("@username:")) || (self.coments[indexPath.row].content!.contains("@id:") && self.coments[indexPath.row].content!.contains("@storename:")){
//            
//            let comment = self.coments[indexPath.row].content!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
//                return value != "" ? true : false
//            })
//            
//            var commentsFinal : NSMutableAttributedString = "".withTextColor(UIColor.black)
//            
//            for word in comment {
//                
//                if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
//                    let information_comments = word.components(separatedBy: ":")
//                    commentsFinal = commentsFinal + "@\(information_comments.last!)".withTextColor(UIColor(red: 0/255, green: 53/255, blue: 105/255, alpha: 1))
//                } else {
//                    commentsFinal = commentsFinal + word.withTextColor(UIColor(red: 59/255, green: 59/255, blue: 59/255, alpha: 1))
//                }
//                
//            }
//            
//            cell.comentLabel.attributedText = commentsFinal
//            
//        } else {
//            cell.comentLabel.text = self.coments[indexPath.row].content
//        }
        
        
        
        
        
        
        
        if self.coments[indexPath.row].created != nil {
            cell.publishedDateLabel.text = self.coments[indexPath.row].created!.convertToDate().timeAgoDisplay()
        } else {
            cell.publishedDateLabel.text = nil
        }
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let urlPerfil : URL!
        
        if username != nil && storeName != nil {
            urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.coments[indexPath.row].user!.picture!)")
            self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                cell.userImageView.image = result.value?.circleMask
            }
        } else if username == nil && storeName != nil {
            urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.coments[indexPath.row].store!.picture!)")
            self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                cell.userImageView.image = result.value?.circleMask
            }
        } else if username != nil && storeName == nil {
            urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.coments[indexPath.row].user!.picture!)")
            self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                cell.userImageView.image = result.value?.circleMask
            }
        } else {
            cell.userImageView.image = #imageLiteral(resourceName: "logo_app").circleMask
        }
        
        return cell
    }
    
}

extension CommentViewController : UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
}

extension CommentViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        // MUDAR A COR DE UMA MARCACAO OU HASHTAG PARA O AZUL, SUGESTAO PARA MARCACOES
        // APOS O USUARIO DIGITAR A MARCACAO E SELECIONAR JOGAR EM UM ARRAY OS DADOS (ID, USERNAME, STORENAME)
        // E NA HORA DE ENVIAR O COMENTARIO TROCAR OS "@JUNO" PELO DADOS DO ARRAY
        
        
        if range.location == 0 {
            self.messageCommentObject = ""
            self.messageComment = ""
        }
        
        if self.indexMention.contains(range.location) {
            for (key, mentionLocation) in self.indexMention.enumerated() {
                if mentionLocation == range.location {
                    
                    self.indexMention.remove(at: key)
                
                    if self.indexObjectMetion.count > 0 {
                        
                        let startIndex = self.messageCommentObject.index(self.messageCommentObject.startIndex, offsetBy: 0)
                        let endIndex = self.messageCommentObject.index(self.messageCommentObject.startIndex, offsetBy: self.indexObjectMetion[mentionLocation]![0])
                        let range = startIndex..<endIndex
                    
                        self.messageCommentObject = String(self.messageCommentObject[range])
                        self.indexObjectMetion.removeValue(forKey: mentionLocation)
                    
                    }
                    
                    self.nameMention = ""
                    self.isMention = false
                    self.mentionTableView.isHidden = true
                
                
                }
            }
        }
        
        if self.isMention {
            
            if string == " " {
                
                self.isMention = false
                self.mentionTableView.isHidden = true
                
            } else if string == "" {
                
                let startIndex = self.nameMention.index(self.nameMention.startIndex, offsetBy: 0)
                let endIndex = self.nameMention.index(self.nameMention.startIndex, offsetBy: self.nameMention.count - 1)
                let range = startIndex..<endIndex
                
                self.nameMention = String(self.nameMention[range])
                
                
                let startIndexName = self.nameMention.index(self.nameMention.startIndex, offsetBy: 1)
                let endIndexName = self.nameMention.index(self.nameMention.startIndex, offsetBy: self.nameMention.count)
                let rangeName = startIndexName..<endIndexName
                
                let name = self.nameMention[rangeName]
                
                
                if name.count > 0 {
                    
                    self.mentionShow.removeAll()
                    
                    for mention in self.mentions {
                        if mention.username!.uppercased().hasPrefix(name.uppercased()){
                            self.mentionShow.append(mention)
                        }
                    }
                    
                    if self.mentionShow.count > 0 {
                        self.mentionTableView.isHidden = false
                        self.mentionTableView.reloadData()
                    }
                    
                }
                
                
            } else {
                
                self.nameMention = "\(self.nameMention)\(string)"
                
                
                // -
                
                let startIndex = self.nameMention.index(self.nameMention.startIndex, offsetBy: 1)
                let endIndex = self.nameMention.index(self.nameMention.startIndex, offsetBy: self.nameMention.count)
                let range = startIndex..<endIndex
                
                let name = self.nameMention[range]
                
                
                if name.count > 0 {
                    
                    self.mentionShow.removeAll()
                    
                    for mention in self.mentions {
                        if mention.username!.uppercased().hasPrefix(name.uppercased()){
                            self.mentionShow.append(mention)
                        }
                    }
                    
                    if self.mentionShow.count > 0 {
                        self.mentionTableView.isHidden = false
                        self.mentionTableView.reloadData()
                    }
                    
                }
                
            }
            
        } else {
            
            self.messageComment = "\(self.messageComment)\(string)"
            
            if string == "" {
                
                if self.messageCommentObject.count > 0 {
                    
                    let startIndex = self.messageCommentObject.index(self.messageCommentObject.startIndex, offsetBy: 0)
                    let endIndex = self.messageCommentObject.index(self.messageCommentObject.startIndex, offsetBy: self.messageCommentObject.count - 1)
                    let range = startIndex..<endIndex
                    
                    self.messageCommentObject = "\(self.messageCommentObject[range])"
                }
            
            } else if string != "@" {
                self.messageCommentObject = "\(self.messageCommentObject)\(string)"
            }
            
        }
        
        if (range.location == 0 && string == "@") || (self.previewCaracter == " " && string == "@") || (self.previewCaracter == "" && string == "@") {
            print("INICIOU MARCACAO DO USUARIO")
            self.nameMention = string
            self.indexMention.append(range.location)
            
            self.isMention = true
            
            //self.mentionTableView.isHidden = false
            //self.mentionTableView.reloadData()
            
            self.messageComment = "\(textField.text!)"
            
            
            //self.messageCommentObject = "\(self.messageCommentObject)\(textField.text!)"
            
            
        }
        
        self.previewCaracter = string
        
        return true
    }
    
}

extension CommentViewController : UserActionDelegate {
    
    func openUserProfile(cell: UserActionTableViewCell, indexPath: IndexPath) {
                
        if self.coments[indexPath.row].user!.username != nil {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
            pageUserViewController.isMyProfile = false
            pageUserViewController.idUser = self.coments[indexPath.row].user!.id!
            
            self.navigationController?.pushViewController(pageUserViewController, animated: true)
            
        } else {
            if let storeId = self.coments[indexPath.row].store!.id {
                self.performSegue(withIdentifier: "pageStoreSegue", sender: storeId)
            }
        }
        
    }
    
}
