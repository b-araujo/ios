//
//  PostsPageStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/9/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import SwiftyAttributes
import ENMBadgedBarButtonItem
import ActiveLabel

class PostsPageStoreViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    //var categories : [Category] = []
    //var store : Store = Store(id: "1", name: "", picture: "", advance_commission: "")
    var store : Store!
    var loader : Loader!
    var manager : Manager!
    var previewPosts : Int = 0 {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("changeViewPosts"), object: previewPosts)
        }
    }
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var barButton : BadgedBarButtonItem!
    
    var lastPost : String? {
        return self.store.posts.last?.created
    }
    
    var morePosts: Bool = true
    
    var waiting: Bool = false
    
    let footerViewReuseIdentifier = "footerViewReuseIdentifier"
    var footerView : FooterCollectionReusableView?
    
    var emptyFooterView: FooterCollectionReusableView?
    let emptyFooterViewReuseIdentifier = "emptyFooterViewReuseIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeViewPosts), name: NSNotification.Name.init("changeViewPosts"), object: nil)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.tag = 10
        self.collectionView.register(UINib(nibName: "PostsPageStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "postsPageStoreCell")
        self.collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: emptyFooterViewReuseIdentifier)

        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
        
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func myCart() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "followStoreSegue" {
            let followPageStoreViewController : FollowPageStoreViewController = segue.destination as! FollowPageStoreViewController
            followPageStoreViewController.store = sender as? Store
        }
        
        
        if segue.identifier == "commentStoreSegue" {
            
            let commentViewController = segue.destination as! CommentViewController
            commentViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "fotoLookStoreSegue" {
            
            let fotoLookViewController = segue.destination as! FotoLookViewController
            fotoLookViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "previewPostSegue" {
            
            let previewPostPageStoreViewController = segue.destination as! PreviewPostPageStoreViewController
            previewPostPageStoreViewController.posts = sender as! Post
            
        }
        
        if segue.identifier == "shareRepostSegue" {
            
            let repostViewController = segue.destination as! RepostViewController
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            repostViewController.imageRespost = senderValue[0] as? UIImage
            repostViewController.id = senderValue[1] as? String
            
        }
        
    }
    
    @objc func changeViewPosts () {
        let indexPath = IndexPath(row: 0, section: 0)
        self.collectionView.reloadItems(at: [indexPath])
        
        if self.previewPosts == 0 {
            
            let blockButton = self.view.viewWithTag(1) as! UIButton
            blockButton.setImage(UIImage(named: "menu-block-filled"), for: .normal)
            
            let listButton = self.view.viewWithTag(2) as! UIButton
            listButton.setImage(UIImage(named: "menu-list"), for: .normal)
            
        } else {
            
            let blockButton = self.view.viewWithTag(1) as! UIButton
            blockButton.setImage(UIImage(named: "menu-block"), for: .normal)
            
            let listButton = self.view.viewWithTag(2) as! UIButton
            listButton.setImage(UIImage(named: "menu-list-filled"), for: .normal)
            
        }
        
    }

    func likeAnimation(_ image : UIImageView ) {
        
        UIView.animate(withDuration: 1.6, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: .allowUserInteraction, animations: {
            image.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
            image.alpha = 1.0
        }, completion: { (finished : Bool) in
            image.alpha = 0.0
            image.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        
    }
    
    func completionHandler(activityType: UIActivityType?, shared: Bool, items: [Any]?, error: Error?) {
        
        if shared {
            
            let alertController = UIAlertController(title: "Sucesso", message: "Compartilhamento realizado com sucesso!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
}

extension PostsPageStoreViewController : UICollectionViewDelegate {
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 11 {
            if self.previewPosts == 0 {
                self.performSegue(withIdentifier: "previewPostSegue", sender: self.store.posts[indexPath.row])
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag == 11 {
            print("indexPath = \(indexPath.row)")
            print("count = \(self.store.posts.count)")
            if indexPath.row >= self.store.posts.count - 1 && morePosts {
                updateNextSet(collectionView)
            }
        }
    }

    func updateNextSet(_ collectionViewWithPosts: UICollectionView){
            WishareAPI.getPostsStore(self.store.id!, self.lastPost) { (result : [Post], error : Error?) in
                if error == nil {
                    if result.count == 0 {
                        self.morePosts = false
                    }
                    print("QUANTIDADE DE NOVOS POSTS NO FEED: \(result.count)")
                    self.store.posts.append(contentsOf: result)
                    self.collectionView.reloadData()
                }
            }

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
//        if collectionView.tag == 11 {
//            if elementKind == UICollectionElementKindSectionFooter {
//                self.footerView?.loadingActivityIndicator.startAnimating()
//            }
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
//        if collectionView.tag == 11 {
//            if elementKind == UICollectionElementKindSectionFooter {
//                self.footerView?.loadingActivityIndicator.stopAnimating()
//            }
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
//        if self.isLoading {
//            return CGSize.zero
//        }
        if collectionView.tag == 10 {
            return CGSize(width: collectionView.bounds.size.width, height: 0)
        }
        
        return CGSize(width: collectionView.bounds.size.width, height: 55.0)
    }
    
}

extension PostsPageStoreViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView.tag == 10 {
            let emptyFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: emptyFooterViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
            self.emptyFooterView = emptyFooterView
            return emptyFooterView
        }
        
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
        self.footerView = footerView

        return footerView

    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 10 {
            return 1
        }
        
        
        if self.store != nil {
            print("QUANTIDADE DE POSTS NO COUNT: \(self.store!.posts.count)")
            return self.store!.posts.count
        }
        else {
            return 0
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 10 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postsPageStoreCell", for: indexPath) as! PostsPageStoreCollectionViewCell
            
            cell.delegate = self
            cell.collectionView.tag = 11
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
            cell.collectionView.register(UINib(nibName: "SimpleStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "simpleStoreCell")
            cell.collectionView.register(UINib(nibName: "ProductStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "productStoreCell")
            cell.collectionView.register(UINib(nibName: "LookStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "lookStoreCell")
            cell.collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
            
            cell.collectionView.reloadData()
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/store-logo/thumb/")!
            let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.store.picture!)")
            
            self.manager.loadImage(with: url, into: cell.storeImageView) { ( result, _ ) in
                cell.storeImageView.image = result.value?.circleMask
            }
            
            cell.nameStoreLabel.text = self.store.name
            
            if let _ : String = self.store.followers {
                cell.quantityFollowersLabel.text = self.store.followers!
                cell.quantityPostsLabel.text = self.store.qtdPosts
                
                if self.store.following! {
                    
                    cell.followButton.backgroundColor = UIColor.white
                    cell.followButton.setTitle("Seguindo", for: .normal)
                    cell.followButton.setTitleColor(UIColor.black, for: .normal)
                    cell.followButton.layer.borderWidth = 1.0
                    cell.followButton.layer.borderColor = UIColor.black.cgColor
                    
                } else {
                    
                    cell.followButton.backgroundColor = UIColor.black
                    cell.followButton.setTitle("Seguir", for: .normal)
                    cell.followButton.setTitleColor(UIColor.white, for: .normal)
                    cell.followButton.layer.borderWidth = 0.0
                    cell.followButton.layer.borderColor = UIColor.clear.cgColor
                    
                }
                
            }
            
            return cell
            
        }
        
    
        if self.previewPosts == 1 {

            switch self.store.posts[indexPath.row].typePost {
                
            case 2:
                
                // POST DE LOJA SIMPLES
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simpleStoreCell", for: indexPath) as! SimpleStoreCollectionViewCell
                
                cell.delegate = self
                cell.IndexPathNow = indexPath
                
                if self.store.posts[indexPath.row].showMoreComments {
                    cell.commentsLabel.numberOfLines = 0
                } else {
                    cell.commentsLabel.numberOfLines = 3
                }
                
                cell.loadingActivityIndicator.startAnimating()
                cell.nameStoreTopButton.setTitle(self.store.posts[indexPath.row].store!.name!, for: .normal)
                cell.nameStoreBottomButton.setTitle(self.store.posts[indexPath.row].store!.name!, for: .normal)
                //cell.commentsLabel.text = self.store.posts[indexPath.row].text!
                cell.datePostLabel.text = self.store.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                cell.postStoreImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
                
                cell.commentsLabel.enabledTypes = [.hashtag, .mention, customType]
                
                cell.commentsLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                cell.commentsLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                
                cell.commentsLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                
                
                
                cell.commentsLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@username:")) || (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.store.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                if (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@username:")) || (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.store.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentsLabel.text = comments_final + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    //cell.commentsLabel.text = self.store.posts[indexPath.row].text! + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                    let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                    if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                        
                        cell.moreComment.isHidden = true
                        cell.commentsLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if self.store.posts[indexPath.row].showMoreComments {
                            
                            cell.moreComment.isHidden = true
                            cell.commentsLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            
                        } else {
                            
                            if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                                cell.moreComment.isHidden = false
                                let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                                cell.commentsLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                                
                            } else {
                                cell.moreComment.isHidden = true
                                cell.commentsLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            }
                            
                        }
                        
                    }
                    
                    
                }
                
//                cell.commentsLabel.text = self.store.posts[indexPath.row].text! + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                // ========================================
                
                
                
                // - CURTIDAS
                if self.store.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.countLikesLabel.text = "\(self.store.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.store.posts[indexPath.row].comments! > 0 {
                    cell.countCommentsLabel.isHidden = false
                    cell.countCommentsLabel.text = "Ver \(self.store.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.countCommentsLabel.isHidden = true
                }
                
                if self.store.posts[indexPath.row].ilikes == "1" {
                    cell.likeImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeImageView.image = UIImage(named: "Like")
                }
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.store.posts[indexPath.row].store!.picture!)")
                
                self.manager.loadImage(with: url, into: cell.postStoreImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.postStoreImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.storeImageView) { ( result, _ ) in
                    cell.storeImageView.image = result.value?.circleMask
                }
                
                return cell
                
            case 3:
                
                // POST DE LOJA PRODUTO
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productStoreCell", for: indexPath) as! ProductStoreCollectionViewCell
                
                cell.delegate = self
                cell.indexPathNow = indexPath
                
                if self.store.posts[indexPath.row].showMoreComments {
                    cell.commentStoreLabel.numberOfLines = 0
                } else {
                    cell.commentStoreLabel.numberOfLines = 3
                }
                
                cell.loadingActivityIndicator.startAnimating()
                cell.nameStoreTopButton.setTitle(self.store.posts[indexPath.row].store!.name!, for: .normal)
                cell.nameStoreBottomButton.setTitle(self.store.posts[indexPath.row].store!.name!, for: .normal)
                //cell.commentStoreLabel.text = self.store.posts[indexPath.row].text!
                cell.publishedDateLabel.text = self.store.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                cell.storePostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
                
                cell.commentStoreLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                cell.commentStoreLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@username:")) || (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.store.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
                if (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@username:")) || (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.store.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    
                    if (heightLabelWithText(self.view.bounds.width - 20, self.store.posts[indexPath.row].text!) + 18.0) <= 30.0 {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if self.store.posts[indexPath.row].showMoreComments {
                            
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            
                        } else {
                            
                            if (heightLabelWithText(self.view.bounds.width - 20, self.store.posts[indexPath.row].text!) + 18.0) > 54.0 {
                                cell.moreComment.isHidden = false
                                let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                                cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.55))) + " ..."
                                
                            } else {
                                cell.moreComment.isHidden = true
                                cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            }
                            
                        }
                        
                    }
                    
                    
                } else {
                    
                    
                    
                    let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                    if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if self.store.posts[indexPath.row].showMoreComments {
                            
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            
                        } else {
                            
                            if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                                cell.moreComment.isHidden = false
                                let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                                cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                                
                            } else {
                                cell.moreComment.isHidden = true
                                cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            }
                            
                        }
                        
                    }
                    
                }
//                cell.commentStoreLabel.text = self.store.posts[indexPath.row].text! + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                // ========================================
                
                
                // - CURTIDAS
                if self.store.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.store.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.store.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.store.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                
                if self.store.posts[indexPath.row].ilikes == "1" {
                    cell.likeProductStoreImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeProductStoreImageView.image = UIImage(named: "Like")
                }
                
                if self.store.posts[indexPath.row].iwish == "1" {
                    cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.store.posts[indexPath.row].store!.picture!)")
            
                
                self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.storePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.storeThumbImageView) { ( result, _ ) in
                    cell.storeThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
            default:
                
                // POST DE LOJA LOOK
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lookStoreCell", for: indexPath) as! LookStoreCollectionViewCell
                
                cell.delegate = self
                cell.indexPathNow = indexPath
                cell.collectionViewCarouselLook.reloadData()
                
                if self.store.posts[indexPath.row].showMoreComments {
                    cell.commentStoreLabel.numberOfLines = 0
                } else {
                    cell.commentStoreLabel.numberOfLines = 3
                }
                
                cell.loadingActivityIndicator.startAnimating()
                cell.nameStoreTopButton.setTitle(self.store.posts[indexPath.row].store!.name!, for: .normal)
                cell.nameStoreBottomButton.setTitle(self.store.posts[indexPath.row].store!.name!, for: .normal)
                //cell.commentStoreLabel.text = self.store.posts[indexPath.row].text!
                cell.publishedDateLabel.text = self.store.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                cell.storePostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
                
                cell.commentStoreLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                cell.commentStoreLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@username:")) || (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.store.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                if (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@username:")) || (self.store.posts[indexPath.row].text!.contains("@id:") && self.store.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.store.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    //cell.commentStoreLabel.text = self.store.posts[indexPath.row].text! + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                    let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                    if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if self.store.posts[indexPath.row].showMoreComments {
                            
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            
                        } else {
                            
                            if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                                cell.moreComment.isHidden = false
                                let string = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                                cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                                
                            } else {
                                cell.moreComment.isHidden = true
                                cell.commentStoreLabel.text = "^\(self.store.posts[indexPath.row].store!.name!)~" + " " + self.store.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            }
                            
                        }
                        
                    }
                }
                
//                cell.commentStoreLabel.text = self.store.posts[indexPath.row].text! + " " + self.store.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                // ========================================
                
                
                // - CURTIDAS
                if self.store.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.store.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.store.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.store.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.store.posts[indexPath.row].ilikes == "1" {
                    cell.likeLookStoreImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeLookStoreImageView.image = UIImage(named: "Like")
                }
                
                if self.store.posts[indexPath.row].iwish == "1" {
                    cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.store.posts[indexPath.row].store!.picture!)")
                
                
                self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.storePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.storeThumbImageView) { ( result, _ ) in
                    cell.storeThumbImageView.image = result.value?.circleMask
                }
                
                
                return cell
                
            }
            
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        cell.imageProductsLook.image = nil
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url : URL!
        
        
        if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != "" {
            url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
        } else {
            
            if let fullUrlString = URL(string: urlBase.absoluteString + "files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)") {
                let lastPathUrl = fullUrlString.lastPathComponent
                let urlWithoutLastComponent = fullUrlString.deletingLastPathComponent()
                
                url = URL(string: urlWithoutLastComponent.absoluteString + "thumb/" + lastPathUrl)
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
        }
        
        
        //let url = urlBase.appendingPathComponent("posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
        
        cell.loadingActivityIndicator.startAnimating()
        
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        return cell
        
    }
    
}

extension PostsPageStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 10 {
            
            if self.previewPosts == 0 {
                let sizePreviewPosts = (self.store.posts.count % 2) + (self.store.posts.count / 2) + 2
                return CGSize(width: self.view.bounds.width, height: 288 + ((self.view.bounds.width / 2) * CGFloat(sizePreviewPosts)) )
            } else {
                return CGSize(width: self.view.bounds.width, height: 288 + ((self.view.bounds.width + 275) * CGFloat(self.store.posts.count)))
            }

        }
        
        
        if self.previewPosts == 0 {
            return CGSize(width: (self.view.bounds.width / 2) - 1.25, height: (self.view.bounds.width / 2) - 2.5)
        } else {
            var base : CGFloat = 0.0
            if self.store.posts[indexPath.row].comments! == 0 { base += 30.0 }
            if self.store.posts[indexPath.row].likes! == 0 { base += 30.0 }
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (270 - base))
        }
       
    }
    
}

extension PostsPageStoreViewController : PostsPageStoreCollectionViewCellDelegate {
    
    func followersPageStore() {
        self.performSegue(withIdentifier: "followStoreSegue", sender: self.store)
    }
    
    func changeOption(_ sender: UIButton) {
    
        if sender.tag == 1 {
            self.previewPosts = 0
        } else {
            self.previewPosts = 1
        }
        
    }
    
    func followStore(_ sender: UIButton) {
        if sender.title(for: .normal) == "Seguir" {
            
            sender.backgroundColor = UIColor.white
            sender.setTitle("Seguindo", for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            sender.layer.borderWidth = 1.0
            sender.layer.borderColor = UIColor.black.cgColor
            
            WishareAPI.followStore(self.store.id!, true, { (result : [String : AnyObject], error : Error?) in
                
                if let followed = result["success"] as? Bool {
                    self.store.following = followed
                }
                
            })
            
            
        } else {
            
            let alertController = UIAlertController(title: "Deseja deixar de seguir \(self.store.name!)?", message: nil, preferredStyle: .actionSheet)
            let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                
                sender.backgroundColor = UIColor.black
                sender.setTitle("Seguir", for: .normal)
                sender.setTitleColor(UIColor.white, for: .normal)
                sender.layer.borderWidth = 0.0
                sender.layer.borderColor = UIColor.clear.cgColor
                WishareAPI.followStore(self.store.id!, false, { (result : [String : AnyObject], error : Error?) in
                    
                    if let unfollowed = result["success"] as? Bool {
                        self.store.following = !unfollowed
                    }
                    
                })
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }
    }
    
}

// MARK: - SimpleStoreCollectionViewDelegate
extension PostsPageStoreViewController : SimpleStoreCollectionViewCellDelegate {
    func viewLikes(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let likeViewController = storyboard.instantiateViewController(withIdentifier: "likesID") as! LikeViewController
        likeViewController.post = self.store.posts[indexPath.row]
        
        self.navigationController?.pushViewController(likeViewController, animated: true)
    }
    
    
    func showMoreCommentsSimpleStore(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        
        
        
    }
    
    func moreSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                if self.store.posts[cell.IndexPathNow.row].user_id == user!.id {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                        self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.store.posts[cell.IndexPathNow.row], cell.IndexPathNow])
                    })
                    
                    let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                            
                            WishareAPI.postDelete(self.store.posts[cell.IndexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                    
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                        
                                        self.store.posts.remove(at: cell.IndexPathNow.row)
                                        //self.feedCollectionView.deleteItems(at: [cell.IndexPathNow])
                                        self.collectionView.reloadData()
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                        
                        alertController.addAction(okAction)
                        alertController.addAction(cancelarAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(editarAction)
                    alertController.addAction(excluirAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                        RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                        
                        WishareAPI.toReport(self.store.posts[cell.IndexPathNow.row].id!, { (error : Error?) in
                            
                            if error == nil {
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            
                        })
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(denunciarAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
            }
            
        }
        
    }
    
    func likeSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
        if self.store.posts[cell.IndexPathNow.row].ilikes == "1" {
            
            self.store.posts[cell.IndexPathNow.row].ilikes = "0"
            self.store.posts[cell.IndexPathNow.row].likes = self.store.posts[cell.IndexPathNow.row].likes! - 1
            //self.feedCollectionView.reloadItems(at: [cell.IndexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.store.posts[cell.IndexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.store.posts[cell.IndexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.store.posts[cell.IndexPathNow.row].ilikes = "1"
            self.store.posts[cell.IndexPathNow.row].likes = self.store.posts[cell.IndexPathNow.row].likes! + 1
            //self.feedCollectionView.reloadItems(at: [cell.IndexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.store.posts[cell.IndexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.store.posts[cell.IndexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
            
            
        }
        
        
    }
    
    func commentSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentStoreSegue", sender: self.store.posts[cell.IndexPathNow.row])
    }
    
    func shareSimpleStore(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.store.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func storeSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
    }
    
    func storePostSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "showImagePost", sender: cell.postStoreImageView.image)
    }
    
    func nameStoreSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
    }
    
}

// MARK: - ProductStoreCollectionViewCellDelegate
extension PostsPageStoreViewController : ProductStoreCollectionViewCellDelegate {
    
    func showMoreCommentsProductStore(cell: ProductStoreCollectionViewCell, indexPath: IndexPath) {
        
    }
    
    func storeThumbProductStore(cell: ProductStoreCollectionViewCell) {
        
    }
    
    func nameStoreProductStore(cell: ProductStoreCollectionViewCell) {
        
    }
    
    func buyProductStore(cell: ProductStoreCollectionViewCell) {
        
        
        if self.store.posts[cell.indexPathNow.row].products_info[0].unavailable! == "I" && self.store.posts[cell.indexPathNow.row].products_info[0].quantity! == "0" {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.store.posts[cell.indexPathNow.row].products_info[0].unavailable! == "E" && self.store.posts[cell.indexPathNow.row].products_info[0].quantity! == "0" {
            
            
            let alertController = UIAlertController(title: "OPS, O ESTOQUE FOI VENDIDO!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                WishareAPI.waitingStock(self.store.posts[cell.indexPathNow.row].products_info[0].id!, { (error : Error?) in })
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
           
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.store.posts[cell.indexPathNow.row].products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
        
    }
    
    func postProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.store.posts[cell.indexPathNow.row].products_info[0].unavailable! == "I" && self.store.posts[cell.indexPathNow.row].products_info[0].quantity! == "0" {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.store.posts[cell.indexPathNow.row].products_info[0].unavailable! == "E" && self.store.posts[cell.indexPathNow.row].products_info[0].quantity! == "0" {
            
            
            let alertController = UIAlertController(title: "OPS, O ESTOQUE FOI VENDIDO!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                WishareAPI.waitingStock(self.store.posts[cell.indexPathNow.row].products_info[0].id!, { (error : Error?) in })
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.store.posts[cell.indexPathNow.row].products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
        
    }
    
    func likeProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.store.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.store.posts[cell.indexPathNow.row].ilikes = "0"
            self.store.posts[cell.indexPathNow.row].likes = self.store.posts[cell.indexPathNow.row].likes! - 1
            //self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.store.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.store.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.store.posts[cell.indexPathNow.row].ilikes = "1"
            self.store.posts[cell.indexPathNow.row].likes = self.store.posts[cell.indexPathNow.row].likes! + 1
            //self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.store.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.store.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
            
            
        }
        
        
    }
    
    func commentProductStore(cell: ProductStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentStoreSegue", sender: self.store.posts[cell.indexPathNow.row])
    }
    
    func shareProductStore(cell: ProductStoreCollectionViewCell, indexPath : IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
        
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                url = urlBase.appendingPathComponent("posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.store.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func wishListProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.store.posts[cell.indexPathNow.row].iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.store.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.store.posts[cell.indexPathNow.row].iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.store.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.store.posts[cell.indexPathNow.row].iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
}

// MARK: - LookStoreCollectionViewCellDelegate
extension PostsPageStoreViewController : LookStoreCollectionViewCellDelegate {
    func viewLikesLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let likeViewController = storyboard.instantiateViewController(withIdentifier: "likesID") as! LikeViewController
        likeViewController.post = self.store.posts[indexPath.row]
        
        self.navigationController?.pushViewController(likeViewController, animated: true)
    }
    
    
    func showMoreCommentsLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        
    }
    
    func numberOfItemsInSectionLook(_ cellForLook : LookStoreCollectionViewCell) -> Int {
        return self.store.posts[cellForLook.indexPathNow.row].products.count
    }
    
    func cellForItemLook(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook : LookStoreCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[cellForLook.indexPathNow.row].products[indexPath.row]["url"]!)")
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItem(_ collection: UICollectionView, _ indexPath: IndexPath, _ now : IndexPath) {
        
        var isValidate : Bool = false
        
        
        for prod_info in self.store.posts[now.row].products_info {
            
            if prod_info.id! == self.store.posts[now.row].products[indexPath.row]["id"] {
                isValidate = true
            }
            
        }
        
        if !isValidate {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
        
            
    
            
            print("IR PARA LOJA :)")
        
        }
        
    }
    
    func buyLookStore( _ sender : UIButton, _ indexPath : IndexPath) {
        
        if self.store.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookStoreSegue", sender: self.store.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    func buyLookStoreImage(_ indexPath: IndexPath) {
        
        if self.store.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookStoreSegue", sender: self.store.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func storeThumbLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func buyLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func postLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func likeLookStore(cell: LookStoreCollectionViewCell) {
        
        if self.store.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.store.posts[cell.indexPathNow.row].ilikes = "0"
            self.store.posts[cell.indexPathNow.row].likes = self.store.posts[cell.indexPathNow.row].likes! - 1
            
            //self..reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            let indexPath = IndexPath(row: 0, section: 0)
            cell.collectionViewCarouselLook.selectItem(at: indexPath, animated: false, scrollPosition: .left)
            
            WishareAPI.like(self.store.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.store.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.store.posts[cell.indexPathNow.row].ilikes = "1"
            self.store.posts[cell.indexPathNow.row].likes = self.store.posts[cell.indexPathNow.row].likes! + 1
            
            //self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            let indexPath = IndexPath(row: 0, section: 0)
            cell.collectionViewCarouselLook.selectItem(at: indexPath, animated: false, scrollPosition: .left)
            
            
            WishareAPI.like(self.store.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.store.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        
                        
                    }
                    
                }
                
            })
            
            
            
        }
        
        
    }
    
    func commentLookStore(cell: LookStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentStoreSegue", sender: self.store.posts[cell.indexPathNow.row])
    }
    
    func shareLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.store.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.store.posts[indexPath.row].picture != nil && self.store.posts[indexPath.row].picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.store.posts[indexPath.row].id!)/thumb/\(self.store.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.store.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func wishlistLookStore(cell: LookStoreCollectionViewCell) {
        
        if self.store.posts[cell.indexPathNow.row].iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.store.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.store.posts[cell.indexPathNow.row].iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.store.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.store.posts[cell.indexPathNow.row].iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
    func storeNameLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
}
