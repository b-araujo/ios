//
//  FollowStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin

class FollowStoreViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var followStoreCollectionView: UICollectionView!
    @IBOutlet weak var comecarButton: UIButton!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    // MARK: - Properties
    var stores : [[String : AnyObject]] = [[:]]
    let reuseIdentifier : String = "followStoreCell"
    var quantityStores : Int = 0
    var loader : Loader!
    var manager : Manager!
    var isLoading : Bool = false
    var footerView : FooterCollectionReusableView?
    let footerViewReuseIdentifier = "footerViewReuseIdentifier"
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.followStoreCollectionView.delegate = self
        self.followStoreCollectionView.dataSource = self

        self.followStoreCollectionView.register(UINib(nibName: "FollowStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.followStoreCollectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerViewReuseIdentifier")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.followStoreCollectionView.isHidden = true
        self.loadingActivityIndicator.startAnimating()
        
        
        WishareAPI.recommendFollowStore(20) { ( result : [String : AnyObject], error : Error?) in
        
            if error == nil {
                
                if let stores : [[String : AnyObject]] = result["Stores"] as? [[String : AnyObject]] {
                    self.stores = stores
                }
                
                self.followStoreCollectionView.reloadData()
                self.followStoreCollectionView.isHidden = false
                self.loadingActivityIndicator.stopAnimating()
            }
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    @IBAction func FollowAndNext(_ sender: UIButton) {
        
        if self.quantityStores < 3 {
            
            let alertController = UIAlertController(title: "Quantidade miníma de loja", message: "Selecione pelo menos 3 lojas para continuar", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            self.performSegue(withIdentifier: "startToFollowStore", sender: nil)
        
        }
        
        
    }

}


extension FollowStoreViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
        
    }
    
}

extension FollowStoreViewController : UICollectionViewDataSource {

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.stores.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FollowStoreCollectionViewCell
        
        cell.delegate = self
        
        
        
        if let store : [String : AnyObject] = self.stores[indexPath.row]["Store"] as? [String : AnyObject] {
            
            cell.accessibilityHint = store["id"] as? String
            cell.nameStoreLabel.text = store["name"] as? String
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
            let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(store["picture"] as! String)")
            cell.loadingActivityIndicator.startAnimating()
            
            self.manager.loadImage(with: url, into: cell.storeImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storeImageView.image = result.value?.circleMask
            }
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter {
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
            self.footerView = footerView
            self.footerView?.backgroundColor = UIColor.clear
            return footerView
            
        } else {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
            
        }
        
    }
    
    
}


extension FollowStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 10, height: (self.view.bounds.width / 3) + 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if self.isLoading {
            return CGSize.zero
        }
        
        return CGSize(width: collectionView.bounds.size.width, height: 55.0)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold = 100.0
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold = Float((diffHeight - frameHeight))/Float(threshold);
        
        triggerThreshold = min(triggerThreshold, 0.0)
        let pullRatio = min(fabs(triggerThreshold),1.0);
        
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        
        if pullRatio >= 1.0 {
            self.footerView?.animateFinal()
        }
        
        print("pullRation:\(pullRatio)")
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        
        print("pullHeight:\(pullHeight)");
        
        if pullHeight == 0.0 {
            if (self.footerView?.isAnimatingFinal)! {
                
                print("load more trigger")
                self.isLoading = true
                self.footerView?.startAnimate()
                
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                    
                    print("CARREGANDO CONTEUDO NOVO")
                    //self.collectionView.reloadData()
                    self.isLoading = false
                    
                })
                
            }
        }
    }

    
}

extension FollowStoreViewController : FollowStoreCollectionViewCellDelegate {
    
    func followStore(cell: FollowStoreCollectionViewCell, sender: UIButton) {
        
        print("Follow-Me")
        print("ID STORE: \(cell.accessibilityHint!)")
        
        if sender.title(for: .normal) == "Seguir" {
    
            if let idStore = cell.accessibilityHint {
                
                WishareAPI.followStore(idStore, true, { (result : [String : AnyObject], error : Error?) in
                    
                    if error == nil {
                        
                        print(result)
                        
                        self.quantityStores += 1
                        sender.backgroundColor = UIColor.white
                        sender.setTitle("Seguindo", for: .normal)
                        sender.setTitleColor(UIColor.black, for: .normal)
                        sender.layer.borderWidth = 2.0
                        sender.layer.borderColor = UIColor.black.cgColor
                        
                    }
                
                })
            
            }
        
        } else {
            
            if let idStore = cell.accessibilityHint {
                
                WishareAPI.followStore(idStore, false, { (result : [String : AnyObject], error : Error?) in
                    
                    if error == nil {
                        
                        print(result)
                        
                        self.quantityStores -= 1
                        sender.backgroundColor = UIColor.black
                        sender.setTitle("Seguir", for: .normal)
                        sender.setTitleColor(UIColor.white, for: .normal)
                        sender.layer.borderWidth = 0.0
                        sender.layer.borderColor = UIColor.clear.cgColor

                    }
                    
                })
                
            }
            
        }

    }
    
    func showUserOrStore(_ cell: FollowStoreCollectionViewCell, _ indexPath: IndexPath) {
        
        if let id = cell.accessibilityHint {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
            pageStoreViewController.idStore = id
            
            self.navigationController?.pushViewController(pageStoreViewController, animated: true)

        }
        
    }
    
}



