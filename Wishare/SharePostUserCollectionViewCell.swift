//
//  SharePostUserCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/2/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class SharePostUserCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var userThumbImageView: UIImageView!
    @IBOutlet weak var userNameTopButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var moreImageView: UIImageView!
    
    
    @IBOutlet weak var userShareThumbImageView: UIImageView!
    @IBOutlet weak var userMameShareButton: UIButton!
    
    @IBOutlet weak var shareUserPostImageView: UIImageView!
    @IBOutlet weak var shareCommentLabel: ActiveLabel!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeSharePostUserImageView: UIImageView!
    @IBOutlet weak var commentSharePostUserImageView: UIImageView!
    @IBOutlet weak var shareSharePostUserImageView: UIImageView!
    @IBOutlet weak var wishListSharePostUserImageView: UIImageView!
    
    @IBOutlet weak var viewLikes: UIView!
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    @IBOutlet weak var userNameBottomButton: UIButton!
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var commentStoreLabel: ActiveLabel!
    @IBOutlet weak var commentsStoreLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    
    @IBOutlet weak var shareView: UIView!
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    var delegateBase : WishareBasePostDelegate!
    var delegate : SharePostUserCollectionViewCellDelegate?
    var indexPathNow : IndexPath!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.shareView.layer.cornerRadius = 5
        self.shareView.layer.borderWidth = 1
        self.shareView.layer.borderColor = UIColor.darkGray.cgColor

        // ================================================================
        
        
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerDreamImageView = UITapGestureRecognizer(target: self, action: #selector(tapGift))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapLikesView))
        let tapGestureRecognizerCommentShowLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        
        self.likeSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerCommentImageView)
        self.shareSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        self.wishListSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerDreamImageView)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        self.commentsStoreLabel.addGestureRecognizer(tapGestureRecognizerCommentShowLabel)
        
        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMore))
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        
        // ================================================================
        
        self.userNameTopButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        self.userNameBottomButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        
        let tapGestureRecognizerUserThumbImageView = UITapGestureRecognizer(target: self, action: #selector(openUserImage))
        self.userThumbImageView.addGestureRecognizer(tapGestureRecognizerUserThumbImageView)
        
        self.userMameShareButton.addTarget(self, action: #selector(openShareUserButton(_:)), for: .touchUpInside)
        
        let tapGestureRecognizerUserShareThumbImageView = UITapGestureRecognizer(target: self, action: #selector(openShareUserImage))
        self.userShareThumbImageView.addGestureRecognizer(tapGestureRecognizerUserShareThumbImageView)
        
    }
    
    @objc func tapMore() {
        self.delegateBase.more(cell: self)
    }
    
    @objc func tapLike() {
        self.delegateBase.like(cell: self)
    }
    
    @objc func tapComment() {
        self.delegateBase.showComments(cell: self)
    }
    
    @objc func tapShare() {
        self.delegateBase.share(cell: self, indexPath: indexPathNow)
    }
    
    @objc func tapGift() {
        self.delegateBase.wishlist(cell: self)
    }
    
    @objc func tapLikesView() {
        self.delegateBase.showLikes(cell: self)
    }
    
    @objc func openUserImage() {
        self.delegate?.openUserImageSharePostUser(indexPathNow)
    }
    
    @objc func openUserButton ( _ sender : UIButton) {
        self.delegate?.openUserButtonSharePostUser(sender, indexPathNow)
    }
    
    @objc func openShareUserButton ( _ sender : UIButton) {
        self.delegate?.openUserShareSharePostUser(sender, indexPathNow)
    }

    @objc func openShareUserImage () {
        self.delegate?.openUserShareImageSharePostUser(indexPathNow)
    }
    
}

protocol SharePostUserCollectionViewCellDelegate {
    func openUserButtonSharePostUser( _ sender : UIButton,  _ indexPath : IndexPath)
    func openUserImageSharePostUser( _ indexPath : IndexPath)
    func openUserShareSharePostUser( _ sender : UIButton,  _ indexPath : IndexPath)
    func openUserShareImageSharePostUser( _ indexPath : IndexPath)
}
