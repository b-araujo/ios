//
//  FinishOrderViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class FinishOrderViewController: UIViewController {

    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tipoEnderecoEntregaLabel: UILabel!
    @IBOutlet weak var nomeDaRuaEntregaLabel: UILabel!
    @IBOutlet weak var bairroCidadeEstadoEntregaLabel: UILabel!
    @IBOutlet weak var cepEntregaLabel: UILabel!
    @IBOutlet weak var tipoEntregaDiasLabel: UILabel!
    @IBOutlet weak var precoEntregaLabel: UILabel!
    
    @IBOutlet weak var flagCardLabel: UILabel!
    @IBOutlet weak var totalItemsLabel: UILabel!
    @IBOutlet weak var priceProductsLabel: UILabel!
    @IBOutlet weak var priceFreteLabel: UILabel!
    
    @IBOutlet weak var totalPedidoLabel: UILabel!
    @IBOutlet weak var totalPedidoParceladoLabel: UILabel!
    
    @IBOutlet weak var purchaseFinish: UIButton!
    
    var store : Store!
    
    var loader : Loader!
    var manager : Manager!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.navigationController?.navigationBar.backItem?.title = "    "
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "ProductCartCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "productShoppingCell")
        
        self.purchaseFinish.layer.cornerRadius = 5.0
        self.purchaseFinish.layer.masksToBounds = false
        self.purchaseFinish.layer.shadowColor = UIColor.black.cgColor
        self.purchaseFinish.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.purchaseFinish.layer.shadowRadius = 2.0
        self.purchaseFinish.layer.shadowOpacity = 0.24
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getAddresses { (addresses : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                var primaryS = false
                var primaryP = false
                
                for address in addresses {
                    if let oneAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                        
                        if (oneAddress["type"] as! String) == "P" && primaryP ==  false {
                            primaryP = true
                        } else if (oneAddress["type"] as! String) == "S" && primaryS ==  false {
                            
                            if (oneAddress["id"] as? String) == self.store.order.id_address {
                                self.store.order.id_address = oneAddress["id"] as? String
                                
                                self.tipoEnderecoEntregaLabel.text = oneAddress["name"] as? String
                                self.nomeDaRuaEntregaLabel.text = "\(oneAddress["address"] as? String ?? ""), \(oneAddress["number"] as? String ?? "")"
                                if let complemento = oneAddress["more"] as? String, !complemento.isEmpty {
                                    self.nomeDaRuaEntregaLabel.text?.append(", \(complemento)")
                                }
                                
                                self.bairroCidadeEstadoEntregaLabel.text = "\((oneAddress["neighborhood"] as! String)) - \((oneAddress["city"] as! String)) - \((oneAddress["state"] as! String))"
                                self.cepEntregaLabel.text = "C.E.P.: \((oneAddress["post_code"] as! String))"
                                
                                primaryS = true
                            }
                        }
                        
                    }
                    
                }
                
                
                
                self.tipoEntregaDiasLabel.text = "\(self.store.order.type_freight!) - \(self.store.order.delivery_days!) dia(s)"
                self.precoEntregaLabel.text = "R$ \(self.store.order.freight_value!)"
                self.flagCardLabel.text = self.store.order.flag_card
                
                var totalPrice : Double = 0.0
                var quantityTotal = 0
                
                if self.store.order.total != nil {
                    totalPrice = self.store.order.total!
                } else {

                    for product in self.store.order.products {
                        
                        quantityTotal += (product.select_quatity! as NSString).integerValue
                        
                        
                        totalPrice += (product.select_quatity! as NSString).doubleValue * ((product.promo_price! == "") ? (product.price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (product.promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)
                        
                    }
                }
                
                let frete : Double = (self.store.order.freight_value!.components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue
                let totalFinal = totalPrice + frete
                
                self.totalItemsLabel.text = "Subtotal (\(quantityTotal) itens)"
                self.priceProductsLabel.text = totalPrice.asLocaleCurrency
                self.priceFreteLabel.text = frete.asLocaleCurrency
                
                self.totalPedidoLabel.text = totalFinal.asLocaleCurrency
                self.totalPedidoParceladoLabel.text = "\(self.store.order.parcel_number!)x \(((totalFinal / (self.store.order.parcel_number! as NSString).doubleValue)).asLocaleCurrency)"
                
                self.heightConstraint.constant = CGFloat(150 * self.store.order.products.count)
                
                RappleActivityIndicatorView.stopAnimation()
                
            
            }
        }
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func finishPurchase(_ sender: UIButton) {

        RappleActivityIndicatorView.startAnimatingWithLabel("Finalizando...", attributes: attributes)
        
        WishareAPI.saveOrder(idAddress: self.store.order.id_address!, cuponCode: self.store.order.cupon_code, products: self.store.order.products, idStore: self.store.id!, idCard: self.store.order.id_card!, installments: self.store.order.parcel_number!, freightValue: self.store.order.freight_value!, service: self.store.order.id_service!, delivery_days: self.store.order.delivery_days!, cvv: self.store.order.cvv!) { (validate : Bool, error : Error?) in
            
            if error == nil {
                
                for product in self.store.order.products {
                    WishareCoreData.removeShoppingCart(pId: product.id!, size: product.size!, sId: self.store.id!, completion: { (_) in })
                }
                
                WishareCoreData.showShoppingCart(completion: { ( _, stores : [WS_Store], error : Error?) in
                    if error == nil {
                        
                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: nil, completionTimeout: 1)
                        
                        let alertController = UIAlertController(title: "Compra Finalizada", message: "Compra realizada com sucesso!", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { ( _ ) in
                            
                            if stores.count > 0 {
                                
                                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
                                self.navigationController?.viewControllers.insert(cartPurchasesViewController, at: 1)
                                self.navigationController?.popToViewController(cartPurchasesViewController, animated: true)

                                
                            } else {
                                
                                self.navigationController?.popToRootViewController(animated: true)
    
                            }
                            
                        })
                        
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        if validate {
                            print("VALIDO")
                        } else {
                            print("NAO FOI VALIDO")
                        }
                        
                    }
                })
                
            } else {

                RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: nil, completionTimeout: 1)
                
                let alertController = UIAlertController(title: "Compra não finalizada!", message: "Não foi possivel finalizar a sua compra, tente novamente!", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                

            }
            
        }
        
        
    }

}

extension FinishOrderViewController : UICollectionViewDelegate {
    
}

extension FinishOrderViewController : UICollectionViewDelegateFlowLayout {
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width, height: 140.0)
    }
    
    
}

extension FinishOrderViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.store.order.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productShoppingCell", for: indexPath) as! ProductCartCollectionViewCell
        
        cell.nameProductLabel.text = self.store.order.products[indexPath.row].name!
        cell.sizeProductLabel.text = "Tamanho: \(self.store.order.products[indexPath.row].size!)"
        cell.quantityProductLabel.text = "Quant.: \(self.store.order.products[indexPath.row].select_quatity!)"
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/products/\(self.store.order.products[indexPath.row].pId!)/\(self.store.order.products[indexPath.row].picture!)")
        
        self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
            cell.productImageView.image = result.value
        }
        
        let totalPrice = (self.store.order.products[indexPath.row].select_quatity! as NSString).doubleValue * ((self.store.order.products[indexPath.row].promo_price! == "") ? (self.store.order.products[indexPath.row].price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue : (self.store.order.products[indexPath.row].promo_price!.components(separatedBy: ".").joined(separator: "").components(separatedBy: ",").joined(separator: ".") as NSString).doubleValue)
        
        cell.priceFinalProductLabel.text = totalPrice.asLocaleCurrency
        
        
        return cell
        
    }
    
}
