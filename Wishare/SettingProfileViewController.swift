//
//  SettingProfileViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/5/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin
import Fusuma
import TLCustomMask

class SettingProfileViewController: UIViewController {

    @IBOutlet weak var backgroundUserImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastnameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var birthdayTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var genreTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var rgTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cpfTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cellTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var editPictureButton: UIButton!
    @IBOutlet weak var editBackgroundButton: UIButton!
    
    @IBOutlet weak var deleteAccontButton : UIButton!
    @IBOutlet weak var saveInformationButton : UIButton!
    
    var cpfMask = TLCustomMask(formattingPattern: "$$$.$$$.$$$-$$")
    
    var homePhoneMaskForPhone = TLCustomMask(formattingPattern: "($$) $$$$-$$$$")
    var mobilePhoneMaskForPhone = TLCustomMask(formattingPattern: "($$) $$$$$-$$$$")
    var usingMobileForPhone : Bool = false
    
    var homePhoneMaskForMobile = TLCustomMask(formattingPattern: "($$) $$$$-$$$$")
    var mobilePhoneMaskForMobile = TLCustomMask(formattingPattern: "($$) $$$$$-$$$$")
    var usingMobileForMobile : Bool = false
    
    var bottomConstraint : NSLayoutConstraint?
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var loader : Loader!
    var manager : Manager!
    
    var birthday : String?
    var birthday_date : Int64?
    let genre : [String] = ["Masculino", "Feminino"]
    
    var username : String?
    
    var isExistName : Bool = false
    
    var originFusuma : Int = 0
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserProfile(notification:)), name: Notification.Name.init("updateUserProfile"), object: nil)
        
        self.saveInformationButton.layer.cornerRadius = 5.0
        self.saveInformationButton.layer.masksToBounds = false
        self.saveInformationButton.layer.shadowColor = UIColor.black.cgColor
        self.saveInformationButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.saveInformationButton.layer.shadowRadius = 2.0
        self.saveInformationButton.layer.shadowOpacity = 0.24
        
        self.birthdayTextField.tag = 10
        self.genreTextField.tag = 20
        self.usernameTextField.tag = 30
        
        self.usernameTextField.delegate = self
        self.birthdayTextField.delegate = self
        self.genreTextField.delegate = self
        self.phoneTextField.delegate = self
        self.cellTextField.delegate = self
        self.cpfTextField.delegate = self

        self.editPictureButton.layer.cornerRadius = 5.0
        self.editBackgroundButton.layer.cornerRadius = 5.0
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        bottomConstraint = NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillHide, object: nil)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        self.genreTextField.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let okButtonBar = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(doneGender))
        let spaceButtonBar = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButtonBar, okButtonBar], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.genreTextField.inputAccessoryView = toolBar
        
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                WishareAPI.getInformationUser(idUser: user!.id!, { (user : User?, error : Error?) in
                    
                    if error == nil {
                        
                        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                        let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(user!.picture!)")
                        let urlPerfilFull = urlBase.appendingPathComponent("files/photo-profile/user/\(user!.picture!)")
                        
                        self.manager.loadImage(with: urlPerfil, into: self.userImageView) { ( result, _ ) in
                            if result.error == nil {
                                self.userImageView.image = result.value?.circleMask
                                self.loadingActivityIndicator.stopAnimating()
                                self.loadingActivityIndicator.isHidden = true
                            } else {
                                self.manager.loadImage(with: urlPerfilFull, into: self.userImageView, handler: { (result, _) in
                                    self.userImageView.image = result.value?.circleMask
                                    self.loadingActivityIndicator.stopAnimating()
                                    self.loadingActivityIndicator.isHidden = true
                                })
                            }
                        }
                        
                        self.username = user!.username
                        self.userNameLabel.text = user!.username
                        self.usernameTextField.text = user!.username
                        self.nameTextField.text = user!.name
                        self.lastnameTextField.text = user!.last_name
                        if user?.birthday != nil {
                            self.birthday = user!.birthday!
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/Y"
                            self.birthdayTextField.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(user!.birthday!)!))
                        }

                        
                        
                        if user?.gender != nil {
                            self.genreTextField.text = (user!.gender! == "M") ? "Masculino" : "Feminino"
                        }
                        
                        self.rgTextField.text = user!.rg
                        if let cpf = user?.cpf {
                            self.cpfTextField.text = self.cpfMask.formatString(string: cpf)
                        }
                        
                        if let homePhone = user!.phone {
                            let homePhoneFormated = self.homePhoneMaskForPhone.formatString(string: homePhone)
                            let mobilePhoneFormated = self.mobilePhoneMaskForPhone.formatString(string: homePhone)
                            
                            if homePhone.count <= 10 {
                                self.phoneTextField.text = homePhoneFormated
                            } else {
                                self.phoneTextField.text = mobilePhoneFormated
                            }
                        }
                        
                        if let mobilePhone = user!.cell {
                            let homePhoneFormated = self.homePhoneMaskForMobile.formatString(string: mobilePhone)
                            let mobilePhoneFormated = self.mobilePhoneMaskForMobile.formatString(string: mobilePhone)
                            
                            if mobilePhone.count <= 10 {
                                self.cellTextField.text = homePhoneFormated
                            } else {
                                self.cellTextField.text = mobilePhoneFormated
                            }
                        }
                        
                        
                        if user!.profile_image!.type! == "local" {
                            
                            if user!.profile_image!.url == nil {
                                self.backgroundUserImageView.image = UIImage(named: "\(user!.profile_image!.url_local!)")
                            } else {
                                self.backgroundUserImageView.image = UIImage(named: "\(user!.profile_image!.url!)")
                            }
                            
                            RappleActivityIndicatorView.stopAnimation()
                        
                        } else {
                            
                            //let url = URL(string: "http://www.wishare.com.br/code/")!
                            let urlBackground = urlBase.appendingPathComponent("\(user!.profile_image!.url!)")
                            
                            self.manager.loadImage(with: urlBackground, into: self.backgroundUserImageView) { ( result, _ ) in
                                self.backgroundUserImageView.image = result.value
                                RappleActivityIndicatorView.stopAnimation()
                            }
                            
                        }
                        
                        
                        
                    }
                    
                })
                
            }
            
        }
        
    }

    @objc func updateUserProfile(notification : Notification) {
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        self.loadingActivityIndicator.isHidden = false
        self.loadingActivityIndicator.startAnimating()
        
        WishareCoreData.getFirstUser { (userCore : UserActive?, error : Error?) in
            
            if error == nil && userCore != nil {
                
                WishareAPI.getInformationUser(idUser: userCore!.id!, { (user : User?, error : Error?) in
                    
                    if error == nil {
                        
                        self.userNameLabel.text = user!.username
                        self.usernameTextField.text = user!.username
                        self.nameTextField.text = user!.name
                        self.lastnameTextField.text = user!.last_name
                        
                        if user?.birthday != nil {
                            self.birthday = user!.birthday!
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/Y"
                            self.birthdayTextField.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(user!.birthday!)!))
                        }
                        
                        if user?.gender != nil {
                            self.genreTextField.text = (user!.gender! == "M") ? "Masculino" : "Feminino"
                        } else {
                            self.genreTextField.text = ""
                        }
                        
                        self.rgTextField.text = user!.rg
                        if let cpf = user?.cpf {
                            self.cpfTextField.text = self.cpfMask.formatString(string: cpf)
                        }
                        self.phoneTextField.text = user!.phone
                        self.cellTextField.text = user!.cell
                        
                        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                        let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(user!.picture!)")
                        let urlPerfilFull = urlBase.appendingPathComponent("files/photo-profile/user/\(user!.picture!)")
                        
                        self.manager.loadImage(with: urlPerfil, into: self.userImageView) { ( result, _ ) in
                            if result.error == nil {
                                self.userImageView.image = result.value?.circleMask
                                self.loadingActivityIndicator.stopAnimating()
                                self.loadingActivityIndicator.isHidden = true
                                
                                if user!.profile_image!.type! == "local" {
                                    if user!.profile_image!.url == nil {
                                        self.backgroundUserImageView.image = UIImage(named: "\(user!.profile_image!.url_local!)")
                                    } else {
                                        self.backgroundUserImageView.image = UIImage(named: "\(user!.profile_image!.url!)")
                                    }
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                                } else {
                                    //let url = URL(string: "http://www.wishare.com.br/code/")!
                                    let urlBackground = urlBase.appendingPathComponent("\(user!.profile_image!.url!)")
                                    self.manager.loadImage(with: urlBackground, into: self.backgroundUserImageView) { ( result, _ ) in
                                        self.backgroundUserImageView.image = result.value
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                                    }
                                }
                                
                            } else {
                                self.manager.loadImage(with: urlPerfilFull, into: self.userImageView, handler: { (result, _) in
                                    self.userImageView.image = result.value?.circleMask
                                    self.loadingActivityIndicator.stopAnimating()
                                    self.loadingActivityIndicator.isHidden = true
                                    
                                    if user!.profile_image!.type! == "local" {
                                        if user!.profile_image!.url == nil {
                                            self.backgroundUserImageView.image = UIImage(named: "\(user!.profile_image!.url_local!)")
                                        } else {
                                            self.backgroundUserImageView.image = UIImage(named: "\(user!.profile_image!.url!)")
                                        }
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                                    } else {
                                        //let url = URL(string: "http://www.wishare.com.br/code/")!
                                        let urlBackground = urlBase.appendingPathComponent("\(user!.profile_image!.url!)")
                                        self.manager.loadImage(with: urlBackground, into: self.backgroundUserImageView) { ( result, _ ) in
                                            self.backgroundUserImageView.image = result.value
                                            RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                                        }
                                    }
                                })
                            }
                        }
                
                    }
                    
                })
                
            }
            
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func doneGender () {
        self.becomeFirstResponder()
    }
    
    @objc func handleKeyBoardNotification(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                
                //let indexPath = IndexPath(item: (self.coments.count != 0) ? self.coments.count - 1 : 0, section: 0)
                //self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
            })
            
        }
        
    }

    @IBAction func alterPictureUser(_ sender: UIButton) {
        
        self.originFusuma = 1
        
        let fusumaController = FusumaViewController()
        fusumaController.delegate = self
        fusumaTintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        self.present(fusumaController, animated: true, completion: nil)
        
    }
    
    @IBAction func alterBackgroundUser(_ sender: UIButton) {
    
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let libraryAction = UIAlertAction(title: "Galeria de Fotos", style: .default) { ( _ ) in

            self.originFusuma = 2
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary
            //imagePickerController.allowsEditing = true
            
            self.present(imagePickerController, animated: true, completion: nil)
            
        }
        
        let suggestImagesAction = UIAlertAction(title: "Imagens Sugeridas", style: .default) { ( _ ) in
            
            self.performSegue(withIdentifier: "suggestImagesSegue", sender: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(libraryAction)
        alertController.addAction(suggestImagesAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func deleteAccount ( _ sender : UIButton) {
        
        let alertController = UIAlertController(title: "Deseja realmente deletar sua conta?", message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { ( _ ) in
            
            WishareAPI.deleteAccount({ (error : Error?) in
                if error == nil {
                    
                    WishareCoreData.deleteUsers { (error : Error?) in
                        if error == nil {
                            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                    
                }
            })

            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func saveInformationUser ( _ sender : UIButton) {
        
        
        var countError = 0
        
        if self.isExistName {
            countError += 1
        }
        
        if (self.usernameTextField.text?.isEmpty)! {
            
            self.usernameTextField.errorMessage = "Usuário Obrigatorio"
            countError += 1
            
        } else {
            if !self.isExistName {
                self.usernameTextField.errorMessage = nil
            }
        }
        
        
        if (self.nameTextField.text?.isEmpty)! {
            
            self.nameTextField.errorMessage = "Usuário Obrigatorio"
            countError += 1
            
        } else {
            
            self.nameTextField.errorMessage = nil
            
        }
        
        
        if (self.lastnameTextField.text?.isEmpty)! {
            
            self.lastnameTextField.errorMessage = "Usuário Obrigatorio"
            countError += 1
            
        } else {
            
            self.lastnameTextField.errorMessage = nil
            
        }
        
        
        if (self.rgTextField.text?.isEmpty)! {
            
            self.rgTextField.errorMessage = "RG Obrigatorio"
            countError += 1
            
        } else {
            
            self.rgTextField.errorMessage = nil
            
        }
        
        if (self.cpfTextField.text?.isEmpty)! {
            
            self.cpfTextField.errorMessage = "CPF Obrigatorio"
            countError += 1
            
        } else {
            
            if self.cpfMask.cleanText.isValidCPF {
                self.cpfTextField.errorMessage = nil
            } else {
                self.cpfTextField.errorMessage = "CPF Inválido"
                countError += 1
            }
            
        }
        
        if (self.phoneTextField.text?.isEmpty)! {
            
            self.phoneTextField.errorMessage = "Telefone Obrigatorio"
            countError += 1
            
        } else {
            
            self.phoneTextField.errorMessage = nil
            
        }
        
        if (self.cellTextField.text?.isEmpty)! {
            
            self.cellTextField.errorMessage = "Celular Obrigatorio"
            countError += 1
            
        } else {
            
            self.cellTextField.errorMessage = nil
            
        }
        
        
        if countError == 0 {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Atualizando...", attributes: attributes)
            
            
            let homePhone = self.usingMobileForPhone ? self.mobilePhoneMaskForPhone : self.homePhoneMaskForPhone
            let mobilePhone = self.usingMobileForMobile ? self.mobilePhoneMaskForMobile : self.homePhoneMaskForMobile
            
            WishareAPI.updateInformationUser(username: self.usernameTextField.text!, name: self.nameTextField.text!, lastName: self.lastnameTextField.text!, birthday: self.birthdayTextField.text, gender: self.genreTextField.text, rg: self.rgTextField.text!, cpf: self.cpfMask.cleanText, phone: homePhone.cleanText, cell: mobilePhone.cleanText) { (error : Error?) in
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Atualizado", completionTimeout: 1)
                    
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { ( _ ) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                }
            }
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.becomeFirstResponder()
    }
    
    @objc func datePickerValueChanged( _ sender : UIDatePicker) {
        
        let dateFormatter = DateFormatter() 
        dateFormatter.dateFormat = "dd/MM/Y"
        self.birthday_date = Int64(sender.date.timeIntervalSince1970)
        self.birthday = "\(Int64(sender.date.timeIntervalSince1970))"
        self.birthdayTextField.text = dateFormatter.string(from: sender.date)
        
    }
    
}

extension SettingProfileViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 10 {
            
            let datePicker : UIDatePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            if self.birthday != nil {
                datePicker.date = Date(timeIntervalSince1970: TimeInterval(self.birthday!)!)
            }
            textField.inputView = datePicker
            datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
            
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor.black
            toolBar.sizeToFit()
            
            let okButtonBar = UIBarButtonItem(title: "Selecionar", style: .plain, target: self, action: #selector(doneGender))
            let spaceButtonBar = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            toolBar.setItems([spaceButtonBar, okButtonBar], animated: false)
            toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
            
        }
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == cpfTextField {
            let cpfFormatted = self.cpfMask.formatStringWithRange(range: range, string: string)
            
            textField.text = cpfFormatted
            
            return false
        }
        
        if textField == phoneTextField {
            
            var mobile = self.mobilePhoneMaskForPhone.formatStringWithRange(range: range, string: string)
            let home = self.homePhoneMaskForPhone.formatStringWithRange(range: range, string: string)
            
            if home == "" {
                mobile = self.mobilePhoneMaskForPhone.formatStringWithRange(range: range, string: string)
            }
            
            if self.mobilePhoneMaskForPhone.cleanText.count > 10 {
                textField.text = mobile
                usingMobileForPhone = true
                
            } else {
                textField.text = home
                usingMobileForPhone = false
            }
            
            return false
            
            
        }
        
        if textField == cellTextField {
            
            var mobile = self.mobilePhoneMaskForMobile.formatStringWithRange(range: range, string: string)
            let home = self.homePhoneMaskForMobile.formatStringWithRange(range: range, string: string)
            
            if home == "" {
                mobile = self.mobilePhoneMaskForMobile.formatStringWithRange(range: range, string: string)
            }
            
            if self.mobilePhoneMaskForMobile.cleanText.count > 10 {
                textField.text = mobile
                usingMobileForMobile = true
                
            } else {
                textField.text = home
                usingMobileForMobile = false
            }
            
            return false
            
            
        }
        
        
        
        if textField.tag == 30 {
            
            if string.isEmpty {
                
                let totalChar = (textField.text?.characters.count)! - 1
                let textString = (textField.text! as NSString).substring(to: totalChar)
                
                if totalChar >= 3 {
                    
                    if textString != self.username! {
                        
                        let isValidate = WishareAPI.checkUserNameExists(textString)
                        
                        if !isValidate {
                            self.usernameTextField.errorMessage = "Usuário Existente"
                            self.isExistName = true
                        } else {
                            self.usernameTextField.errorMessage = nil
                        }
                        
                    }
    
                }

                
            } else {
                
                let textString = textField.text! + string
                let totalChar = textString.characters.count
                
                if totalChar >= 3 {
                    
                    if textString != self.username! {
                        
                        let isValidate = WishareAPI.checkUserNameExists(textString)
                        
                        if !isValidate {
                            self.usernameTextField.errorMessage = "Usuário Existente"
                            self.isExistName = true
                        } else {
                            self.usernameTextField.errorMessage = nil
                        }
                        
                    }
                    
                }
                
            }

            
        }
        
        return true
    }

    
}

extension SettingProfileViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.genre.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.genre[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genreTextField.text = self.genre[row]
    }
    
}

extension SettingProfileViewController : FusumaDelegate {
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
    }
    
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        if self.originFusuma == 1 {
            
            let fixed = image.fixOrientation()
            
            if let base64String = UIImageJPEGRepresentation(fixed , 1)?.base64EncodedString() {
                
                RappleActivityIndicatorView.startAnimatingWithLabel("Atualizando...", attributes: attributes)
                
                WishareAPI.updateUserPicture(picture: base64String, { (error : Error?) in
                    if error == nil {
                        NotificationCenter.default.post(name: NSNotification.Name.init("updateUserProfile"), object: 1)
                        RappleActivityIndicatorView.stopAnimation()
                    }
                })
                
            }
        
            self.originFusuma = 0
            
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
    }
    
}

extension SettingProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
        
            if self.originFusuma == 2 {
                
                let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                let previewBackgroundUserViewController = storyboard.instantiateViewController(withIdentifier: "previewBackgroundUserID") as! PreviewBackgroundUserViewController
                previewBackgroundUserViewController.image = image
                previewBackgroundUserViewController.origin = 2
                
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.pushViewController(previewBackgroundUserViewController, animated: true)
                
                self.originFusuma = 0
                
            }
        
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
        
            if self.originFusuma == 2 {
            
                let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                let previewBackgroundUserViewController = storyboard.instantiateViewController(withIdentifier: "previewBackgroundUserID") as! PreviewBackgroundUserViewController
                previewBackgroundUserViewController.image = image
                previewBackgroundUserViewController.origin = 2
            
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.pushViewController(previewBackgroundUserViewController, animated: true)
            
                self.originFusuma = 0
            
            }
        
        } else {
            
            if self.originFusuma == 2 {
                
                let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                let previewBackgroundUserViewController = storyboard.instantiateViewController(withIdentifier: "previewBackgroundUserID") as! PreviewBackgroundUserViewController
                previewBackgroundUserViewController.image = nil
                previewBackgroundUserViewController.origin = 2
                
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.pushViewController(previewBackgroundUserViewController, animated: true)
                
                self.originFusuma = 0
                
            }
            
        }
    
    }

}
