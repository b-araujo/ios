//
//  AcquireWishTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class AcquireWishTableViewCell: UITableViewCell {

    @IBOutlet weak var acquireImageView: UIImageView!
    @IBOutlet weak var nameAcquireLabel: UILabel!
    @IBOutlet weak var dataAcquireLabel: UILabel!
    @IBOutlet weak var pointsAcquireLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
