//
//  PopupCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PopupCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productPopupImageView: UIImageView!
    @IBOutlet weak var productNamePopupLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var viewButton: UIView!
    
    var delegate : PopupCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.continueButton.addTarget(self, action: #selector(pressButton), for: .touchUpInside)
    }
    
    @objc func pressButton(_ sender : UIButton) {
        self.delegate?.buyProductOnly()
    }

}

protocol PopupCollectionViewCellDelegate {
    
    func buyProductOnly()
    
}
