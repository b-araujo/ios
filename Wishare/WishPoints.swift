//
//   WishPoints.swift
//  Wishare
//
//  Created by Wishare iMac on 7/24/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class WishPoints {
    
    var idLevel : String?
    var level : Int?
    var actualPoints : String?
    var totalPoints : String?
    var nameLevel : String?
    var descriptionLevel : String?
    var imageLevel : String?
    var objectives : [Objective] = []
    var benefits : [Benefit] = []
    
    init ( _ userLevel : [String : AnyObject] ) {
        
        if let levelUser : [String : AnyObject] = userLevel["userLevel"] as? [String : AnyObject] {
            
            if let myPoints : Int = levelUser["Points"] as? Int {
                self.actualPoints = "\(myPoints)"
            }
            
            if let level : [String : AnyObject] = levelUser["level"] as? [String : AnyObject] {
                
                if let wishLevel : [String : AnyObject] = level["WishLevel"] as? [String : AnyObject] {
                    
                    self.idLevel = wishLevel["id"] as? String
                    self.totalPoints = wishLevel["points"] as? String
                    self.nameLevel = wishLevel["name"] as? String
                    self.descriptionLevel = wishLevel["description"] as? String
                    
                }
                
                if let benefit : [[String : AnyObject]] = level["Benefit"] as? [[String :AnyObject]] {
                    for b in benefit {
                        let bene = Benefit(b)
                        self.benefits.append(bene)
                    }
                }
                
                if let benefits : [String] = level["Benefits"] as? [String] {
                    for b in self.benefits {
                        if b.type == "0" {
                            if let myLevel : [String : AnyObject] = levelUser["MyLevel"] as? [String : AnyObject] {
                                b.message = benefits[Int(b.type!)!]
                                b.code = myLevel["code"] as? String
                            }
                        } else if b.type == "2" {
                            b.message = "\(b.value_from!) de Remuneração em vendas"
                        } else if b.type == "3" {
                            b.message = "Receber \(b.value_from!) de compra de volta"
                        } else if b.type == "4" {
                            b.message = "Frete Grátis nas compras acima de \((b.value_from! as NSString).doubleValue.asLocaleCurrency)"
                        } else {
                            b.message = benefits[Int(b.type!)!]
                        }
                    
                    }
                }
                
                if let objectives : [[String : AnyObject]] = level["Objective"] as? [[String : AnyObject]] {
                    for objective in objectives {
                        let obj = Objective(objective)
                        self.objectives.append(obj)
                    }
                }
                
                if let myLevel : [String : AnyObject] = levelUser["MyLevel"] as? [String : AnyObject] {
                    self.level = (myLevel["level"] as? NSString)?.integerValue
                }
                
                
                if let myLevel : [String : AnyObject] = levelUser[self.idLevel!] as? [String : AnyObject] {
                    
                    if let metric : [[String : AnyObject]] = myLevel["metric"] as? [[String : AnyObject]] {
                        
                        for (key, value) in metric.enumerated() {
                            
                            self.objectives[key].isClosed = value["objective_closed"] as? Bool
                            self.objectives[key].qtd = value["qtd"] as? String
                            self.objectives[key].difference = value["difference"] as? Int
                            
                            if self.objectives[key].isClosed! {
                                if let closed : [String : AnyObject] = value["closed"] as? [String : AnyObject] {
                                    if let finalized : [String : AnyObject] = closed["FinalizedObjective"] as? [String : AnyObject] {
                                        self.objectives[key].finalizedObjective = finalized["created"] as? String
                                    }
                                }
                            }
    
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
}
