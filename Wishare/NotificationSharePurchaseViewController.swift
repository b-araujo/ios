//
//  NotificationSharePurchaseViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 8/2/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class NotificationSharePurchaseViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageTextView: UITextView!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: RappleStyle.apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var store : Store!
    var products : [[String : AnyObject]] = []
    var productsSelected : [[String : AnyObject]] = []
    
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Publicar", style: .plain, target: self, action: #selector(sharePurschase))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func sharePurschase () {
        
        if self.productsSelected.count == 0 {
            
            let alertController = UIAlertController(title: "Selecione ao menos um produto", message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Publicando...", attributes: attributes)
            
            WishareAPI.sharePurchase(text: self.messageTextView.text, idStore: self.store.id!, products: self.productsSelected, orderId: self.productsSelected[0]["id_order"] as! String) { (error : Error?) in
                if error == nil {
                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Publicado!", completionTimeout: 2)
                    Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { ( _ ) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            
        }
        
    }

}

extension NotificationSharePurchaseViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewMaskYellow : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            view.alpha = 0.3
            view.tag = 113
            return view
        }()
        
        if self.productsSelected.count > 0 {
            
            var contains : Bool = false
            
            for ps in self.productsSelected {
            
                if (self.products[indexPath.row]["product_id"] as! String) == (ps["product_id"] as! String) {
                    
                    contains = true
                    let cell = collectionView.cellForItem(at: indexPath)
                    
                    for v in cell!.subviews {
                        if v.tag == 113 {
                            v.removeFromSuperview()
                        }
                    }
                    
                    let productsSelectedClone = self.productsSelected
                    self.productsSelected.removeAll()

                    for psc in productsSelectedClone {
                        if (self.products[indexPath.row]["product_id"] as! String) != (psc["product_id"] as! String) {
                            self.productsSelected.append(psc)
                        }
                    }
                
                }
                
            }
            
            
            if contains == false {
                
                let cell = collectionView.cellForItem(at: indexPath)
                cell!.addSubview(viewMaskYellow)
                
                viewMaskYellow.topAnchor.constraint(equalTo: cell!.topAnchor).isActive = true
                viewMaskYellow.bottomAnchor.constraint(equalTo: cell!.bottomAnchor).isActive = true
                viewMaskYellow.leftAnchor.constraint(equalTo: cell!.leftAnchor).isActive = true
                viewMaskYellow.rightAnchor.constraint(equalTo: cell!.rightAnchor).isActive = true

                self.productsSelected.append(self.products[indexPath.row])
                
            }
            

        } else {
            
            let cell = collectionView.cellForItem(at: indexPath)
            cell!.addSubview(viewMaskYellow)
            
            viewMaskYellow.topAnchor.constraint(equalTo: cell!.topAnchor).isActive = true
            viewMaskYellow.bottomAnchor.constraint(equalTo: cell!.bottomAnchor).isActive = true
            viewMaskYellow.leftAnchor.constraint(equalTo: cell!.leftAnchor).isActive = true
            viewMaskYellow.rightAnchor.constraint(equalTo: cell!.rightAnchor).isActive = true

            self.productsSelected.append(self.products[indexPath.row])
            
        }
        
    }
    
}

extension NotificationSharePurchaseViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        cell.loadingActivityIndicator.startAnimating()

        let urlStringUser : URL = urlBase.appendingPathComponent("files/orders_itens/\(self.products[indexPath.row]["product_id"] as! String)/\(self.products[indexPath.row]["url"] as! String)")
        
        self.manager.loadImage(with: urlStringUser, into: cell.imageProductsLook, handler: { (result, _) in
            cell.imageProductsLook.image = result.value
            cell.loadingActivityIndicator.stopAnimating()
        })
        
        return cell
        
    }
}

extension NotificationSharePurchaseViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.height / 5, height: self.view.bounds.height / 5)
    }
    
}
