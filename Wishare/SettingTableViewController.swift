//
//  SettingTableViewController.swift
//  Wishare
//
//  Created by Vinicius França on 04/07/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import ENMBadgedBarButtonItem
import RappleProgressHUD
import UICircularProgressRing

class SettingTableViewController: UITableViewController {

    @IBOutlet weak var privateSwitch: UISwitch!
    @IBOutlet weak var levelWishLabel: UILabel!
    @IBOutlet weak var pointsLevelLabel: UILabel!
    @IBOutlet weak var progressBar: UICircularProgressRingView!
    
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    var barButton : BadgedBarButtonItem!
    var user : User!
    var wishPoints : WishPoints? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                WishareAPI.getInformationUser(idUser: user!.id!) { (user : User?, error : Error?) in
                    if error == nil {
                        
                        WishareAPI.wishPoints { (wishPoints : WishPoints?, error : Error?) in
                            if error == nil {
                                
                                self.user = user!
                                
                                if wishPoints?.idLevel == nil {
                                    
                                    self.levelWishLabel.text = ""
                                    self.pointsLevelLabel.text = ""
                                    
                                    self.progressBar.setProgress(value: 0, animationDuration: 2)
                                    self.progressBar.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
                                    
                                } else {
                                    
                                    self.wishPoints = wishPoints
                                    
                                    self.levelWishLabel.text = wishPoints!.nameLevel
                                    self.pointsLevelLabel.text = "\(wishPoints!.actualPoints!) / \(wishPoints!.totalPoints!)"
                                    
                                    self.progressBar.setProgress(value: CGFloat((100.0 * (wishPoints!.actualPoints! as NSString).floatValue) / (wishPoints!.totalPoints! as NSString).floatValue), animationDuration: 2)
                                    
                                    self.progressBar.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
                                    
                                    self.progressBar.setProgress(value: CGFloat((100.0 * (wishPoints!.actualPoints! as NSString).floatValue) / (wishPoints!.totalPoints! as NSString).floatValue), animationDuration: 3, completion: nil)
                                    
                                }
                                

                        
                                if user!.privateW! == "0" {
                                    
                                    self.privateSwitch.isOn = false
                                    
                                } else {
                                    
                                    self.privateSwitch.isOn = true
                                    
                                }
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                
                            }
                        }
                        
                    }
                }
                
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "problemReportSegue" {
            
            let problemReportViewController = segue.destination as! ProblemReportViewController
            let senderValue : [AnyObject] = sender as! [AnyObject]
            problemReportViewController.titleReport = senderValue[0] as? String
            problemReportViewController.commentReport = senderValue[1] as? String
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
        
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func myCart() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
    }

    @IBAction func privateOnOrOff(_ sender: UISwitch) {
        
        if sender.isOn {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Alterando...", attributes: attributes)
            
            WishareAPI.privateUser({ ( error : Error?) in
                if error == nil {
                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Ativado!", completionTimeout: 1)
                }
            })
            
        } else {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Alterando...", attributes: attributes)
            
            WishareAPI.unprivateUser({ (error : Error?) in
                if error == nil {
                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Desativado!", completionTimeout: 1)
                }
            })
            
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                if self.wishPoints?.idLevel == nil {
                    
                    let alertController = UIAlertController(title: "Em Breve", message: nil, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    self.performSegue(withIdentifier: "wishPointsSegue", sender: nil)
                
                }
                
                
            }
        }
        
        if indexPath.section == 3 {
            
            if indexPath.row == 0 {
         
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                let emailAction = UIAlertAction(title: "E-mail", style: .default, handler: { ( _ ) in
                    
                    let mailComposeViewController = MFMailComposeViewController()
                    mailComposeViewController.mailComposeDelegate = self
                    
                    mailComposeViewController.setSubject("Seu amigo \(self.user.name!) \(self.user.last_name!) te fez um convite")
                    mailComposeViewController.setMessageBody("Olá, \n \n Estou utilizando o Wishare e quero convidar você! \n \n O Wishare é a primeira rede social de compras que permite: \n \n • Compartilhar suas compras e seus looks com milhares de usuários ou amigos; \n • Seguir suas marcas e lojas preferidas; \n • Ver dicas de influencers e ficar na moda; \n • Realizar suas compras sem precisar sair do App; \n • Ganhar pontos e se destacar seu perfil com ínumeros benefícios; \n \n Clique aqui e venha fazer parte do Wishare!", isHTML: false)
                    
//                    mailComposeViewController.setMessageBody("<!doctype html> <html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <title>Responsive Email Template</title> <style type=\"text/css\"> y { font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#57585A} a {text-decoration:none; color:#959595; font-weight:bold} .paragrafo {padding-left: 17px;} .linha2 {border:1px dashed #C3C4CA;} .linha {border-bottom: 1px dashed #C3C4CA;} .x {text-indent:30px;} .ReadMsgBody {width: 100%; background-color: #ffffff;} .ExternalClass {width: 100%; background-color: #ffffff;} body {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif} table {border-collapse: collapse;} @media only screen and (max-width: 640px)  {body[yahoo] .deviceWidth {width:440px!important; padding:0;} body[yahoo] .center {text-align: center!important;} } @media only screen and (max-width: 479px) {body[yahoo] .deviceWidth {width:280px!important; padding:0;} body[yahoo] .center {text-align: center!important;} } </style> </head> <body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" yahoo=\"fix\" style=\"font-family: Georgia, Times, serif\"> <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\"> <tr> <td width=\"100%\" valign=\"top\" bgcolor=\"#ffffff\" style=\"padding-top:20px\"> <table width=\"580\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"deviceWidth\" style=\"margin:0 auto;\"> <tr> <td width=\"100%\" bgcolor=\"#ffffff\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" class=\"deviceWidth\"> <tr> <td style=\"padding:10px 0px\" class=\"center\"> <a href=\"http://www.wishare.com.br\"><img src=http://wishare.com.br/emailmkt/logo3.svg alt=\"\" border=\"0\" /></a> </td> </tr> </table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"right\" class=\"deviceWidth\"> <tr> <td class=\"center\" style=\"font-size: 13px; color: #272727; font-weight: light; text-align: center; font-family: Arial, sans-serif; line-height: 10px; vertical-align: middle; padding:10px 90px; font-style:regular\"> <p><a style=\"text-decoration: none; color: #57585A;\">Número do Pedido</a> </p> <p><a style=\"text-decoration: none; color: #FCD000;\">[ORDER_ID]</a> </p></td> </tr> </table> </td> </tr> </table> <div class=\"linha\"> </div> <div style=\"height:10px;margin:0 auto;\">&nbsp;</div> <table width=\"580\"  class=\"deviceWidth\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" bgcolor=\"#eeeeed\" style=\"margin:0 auto;\"> <tr> <td valign=\"top\" style=\"padding:0\" bgcolor=\"#ffffff\"> </td> </tr> <tr> <td style=\"font-size: 15px; color: #959595; font-weight: normal; text-align: left; font-family: Arial, sans-serif; line-height: 24px; vertical-align: top; padding:20px 8px 10px 8px\" bgcolor=\"#ffffff\"> <div style=\"height:15px;margin:0 auto;\">&nbsp;</div> <a  style=\"text-decoration: none; color: #272727; font-size: 16px; color: #57585A; font-weight: bold; font-family:Arial, sans-serif \">&nbsp;&nbsp;&nbsp; Olá, <b style=\"color: #FCD000;\">[USER_NAME]</b>!</a> <p class=\"x\" ALIGN=\"justify\"> O seu pedido <b style=\"color: #57585A;\">[ORDER] </b>foi CANCELADO. <p class=\"x\" ALIGN=\"justify\"> Os produtos não serão enviados e seu respectivo valor não será cobrado. Caso o pagamento já tenha sido efetuado e confirmado, solicitamos que entre em contato com o nosso SAC (Serviço de Atendimento ao Consumidor) para estorno do valor. <div class=\"linha2\" style=\"height:25px; margin:0 auto; font-size: 16px; color: #57585A; font-weight: bold; font-family:Arial, sans-serif\"> &nbsp;&nbsp;</a> </div> <p ALIGN=\"center\"> Estamos sempre a disposição! </p> <p style= \"text-align: center;\">Abraços,</p> <p  style= \"text-align: center; color: #FCD000; font-weight: bold;line-height:0px;\">Equipe Wishare </p> <div class=\"linha\" style=\"height:15px;margin:0 auto;\">&nbsp;</div> <p style= \"text-align: center; font-size:10px\">Em caso de dúvida, fique a vontade para responder este e-mail ou nos contatar pelo telefone: (11) 3080-8600</p> </td> </tr> </table> <div class=\"linha\"> </div> <table width=\"580\"  class=\"deviceWidth\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" bgcolor=\"#eeeeed\" style=\"margin:0 auto;\"> <tr> <td style=\"font-size: 12px; color: #959595; font-weight: normal; text-align: center; font-family: Arial, sans-serif; line-height: 24px; vertical-align: top; padding:20px 8px 10px 8px\" bgcolor=\"#ffffff\"> <a href=\"http://www.wishare.com.br\" target=\"_blank\"> SITE </a>| <a href=\"http://wishare.com.br/vendas-no-wishare/\" target=\"_blank\"> VENDA NO WISHARE </a> | <a href=\"http://wishare.com.br/historia-do-wishare/\" target=\"_blank\"> SOBRE O WISHARE</a> | <a href=\"https://www.facebook.com/wishareapp\" target=\"_blank\"> FACEBOOK </a> |<a href=\"https://www.instagram.com/wishareapp/\" target=\"_blank\"> INSTAGRAM </a> </td> </tr> </table> <div style=\"display:none; white-space:nowrap; font:15px courier; color:#ffffff;\"> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </div> </body> </html>".printableAscii(), isHTML: true)

                    
                    if MFMailComposeViewController.canSendMail() {
                        self.present(mailComposeViewController, animated: true, completion: nil)
                    } else {
                        print("DEU ERRO NO EMAIL")
                    }
                
                })
                
                let whatsAction = UIAlertAction(title: "WhatsApp", style: .default, handler: { ( _ ) in
                    
                    let msg = "Olá, \n \n Estou utilizando o Wishare e quero convidar você! \n \n O Wishare é a primeira rede social de compras que permite: \n \n • Compartilhar suas compras e seus looks com milhares de usuários ou amigos; \n • Seguir suas marcas e lojas preferidas; \n • Ver dicas de influencers e ficar na moda; \n • Realizar suas compras sem precisar sair do App; \n • Ganhar pontos e se destacar seu perfil com ínumeros benefícios; \n \n Clique aqui e venha fazer parte do Wishare!"
                    let urlWhats = "whatsapp://send?text=\(msg)"
                    
                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                        if let whatsappURL = NSURL(string: urlString) {
                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                                UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                            } else {
                                
                                let alertController = UIAlertController(title: "WhatsApp não está instalado", message: "O WhatsApp não se encontra instalado nesse celular", preferredStyle: .alert)
                                
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                
                                alertController.addAction(okAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                    
                            }
                        }
                    }
                    
                })
                
                let facebookAction = UIAlertAction(title: "Facebook", style: .default, handler: { ( _ ) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let facebookFriendViewController = storyboard.instantiateViewController(withIdentifier: "facebookFriendID") as! FacebookFriendViewController
                    facebookFriendViewController.isSettingOrigin = true
                    self.navigationController?.pushViewController(facebookFriendViewController, animated: true)

                })
                
                let moreAction = UIAlertAction(title: "Mais", style: .default, handler: { ( _ ) in
                    
                    let activityViewController = UIActivityViewController(activityItems: ["Olá, \n \n Estou utilizando o Wishare e quero convidar você! \n \n O Wishare é a primeira rede social de compras que permite: \n \n • Compartilhar suas compras e seus looks com milhares de usuários ou amigos; \n • Seguir suas marcas e lojas preferidas; \n • Ver dicas de influencers e ficar na moda; \n • Realizar suas compras sem precisar sair do App; \n • Ganhar pontos e se destacar seu perfil com ínumeros benefícios; \n \n Clique aqui e venha fazer parte do Wishare!"], applicationActivities: nil)
                    self.present(activityViewController, animated: true, completion: nil)
                    
                })
                
                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                
                alertController.addAction(emailAction)
                alertController.addAction(whatsAction)
                alertController.addAction(facebookAction)
                alertController.addAction(moreAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }
        
        
        
        
        if indexPath.section == 4 {
            
            if indexPath.row == 0 {
                
                let safariViewController = SFSafariViewController(url: URL(string: "http://www.wisharecode.com.br/code/users/getReturn")!, entersReaderIfAvailable: true)
                self.present(safariViewController, animated: true, completion: nil)
                
            }
            
            if indexPath.row == 1 {
                
                let safariViewController = SFSafariViewController(url: URL(string: "http://www.wisharecode.com.br/code/users/getTerms")!, entersReaderIfAvailable: true)
                self.present(safariViewController, animated: true, completion: nil)
                
            }
            
            if indexPath.row == 2 {
                
                let safariViewController = SFSafariViewController(url: URL(string: "http://www.wisharecode.com.br/code/users/getDelivery")!, entersReaderIfAvailable: true)
                self.present(safariViewController, animated: true, completion: nil)
                
            }
            
        }
        
        
        if indexPath.section == 6 {
            if indexPath.row == 0 {
                
                let alertController = UIAlertController(title: "Relatar um Problema", message: nil, preferredStyle: .alert)
                let oneAction = UIAlertAction(title: "Spam ou Abuso", style: .default, handler: { ( _ ) in
                    let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                    let problemReportViewController = storyboard.instantiateViewController(withIdentifier: "problemReportID") as! ProblemReportViewController
                    problemReportViewController.titleReport = "Spam ou Abuso"
                    problemReportViewController.commentReport = "Explique qual conteúdo foi considerado ofensivo, prejudicial, perigoso ou se algum está importunando você"
                    self.navigationController?.pushViewController(problemReportViewController, animated: true)
                })
                let twoAction = UIAlertAction(title: "Mau funcionamento", style: .default, handler: { ( _ ) in
                    let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                    let problemReportViewController = storyboard.instantiateViewController(withIdentifier: "problemReportID") as! ProblemReportViewController
                    problemReportViewController.titleReport = "Mau funcionamento"
                    problemReportViewController.commentReport = "Explique em poucas palavras o que aconteceu"
                    self.navigationController?.pushViewController(problemReportViewController, animated: true)
                })
                let threeAction = UIAlertAction(title: "Comentários Gerais", style: .default, handler: { ( _ ) in
                    let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                    let problemReportViewController = storyboard.instantiateViewController(withIdentifier: "problemReportID") as! ProblemReportViewController
                    problemReportViewController.titleReport = "Comentários Gerais"
                    problemReportViewController.commentReport = "Explique em poucas palavras o que você gosta ou o que poderia melhorar"
                    self.navigationController?.pushViewController(problemReportViewController, animated: true)
                })
                
                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                
                alertController.addAction(oneAction)
                alertController.addAction(twoAction)
                alertController.addAction(threeAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
        
        
        if indexPath.section == 7 {
            
            if indexPath.row == 0 {
                
                let alertController = UIAlertController(title: "Deseja sair?", message: nil, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "Confirmar", style: .destructive, handler: { ( _ ) in
                    
                    FacebookAPI.facebookLoginManager.logOut()
                    
                    
                    WishareAPI.disableNotificationDevice(token: UserDefaults.standard.object(forKey: "device_token") as! String, { (error : Error?) in
                        if error == nil {
                            
                            WishareCoreData.clearShoppingCart(completion: { (error: Error?) in
                                
                            })
                            
                            
                            WishareCoreData.deleteUsers { (error : Error?) in
                                if error == nil {
                                    
                                    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                                }
                            }
                            

                            
                        }
                    })
                    
                })
                
                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                
                alertController.addAction(confirmAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)

            }
            
        }
        
    }
}

extension SettingTableViewController : MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
