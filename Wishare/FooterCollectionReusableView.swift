//
//  FooterCollectionReusableView.swift
//  Wishare
//
//  Created by Vinicius França on 21/04/17.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import UIKit

class FooterCollectionReusableView: UICollectionReusableView {

    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    var isAnimatingFinal : Bool = false
    var currentTransform : CGAffineTransform?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareInitialAnimation()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setTransform(inTransform : CGAffineTransform, scaleFactor : CGFloat) {
        
        if self.isAnimatingFinal {
            return
        }
        
        self.currentTransform = inTransform
        self.loadingActivityIndicator.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
        
    }
    
    func prepareInitialAnimation() {
    
        self.isAnimatingFinal = false
        self.loadingActivityIndicator.stopAnimating()
        self.loadingActivityIndicator.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
    
    }
    
    func startAnimate() {
    
        self.isAnimatingFinal = true
        self.loadingActivityIndicator.startAnimating()
    
    }
    
    func stopAnimate() {
        
        self.isAnimatingFinal = false
        self.loadingActivityIndicator.stopAnimating()
    
    }

    func animateFinal() {
        
        if isAnimatingFinal {
            return
        }
        
        self.isAnimatingFinal = true
        UIView.animate(withDuration: 0.2) { 
            self.loadingActivityIndicator.transform = CGAffineTransform.identity
        }
        
    }
    
    
}
