//
//  WishareAPI.swift
//  Wishare
//
//  Created by Wishare iMac on 4/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import Alamofire

var urlBase : URL = URL(string: "http://www.wisharecode.com.br/code/")!
var urlBaseWishare : URL = URL(string: "http://www.wisharecode.com.br/")!

class WishareAPI {
    
    class func passwordForgot( _ userOrEmail : String, completion : @escaping ( _ result : [String : AnyObject], _ error : Error? ) -> Void ) {
        
        let url = urlBase.appendingPathComponent("users/forgetpwd.json")
        
        let parameters : Parameters = ["User": ["email" : userOrEmail]]
        let encoding : ParameterEncoding = JSONEncoding.default
        let headers : HTTPHeaders = ["Accept": "application/json"]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
            
            if response.result.isSuccess {
                if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                    completion(resultAPI, nil)
                } else {
                    completion([:], response.error)
                }
            }
            
            if response.result.isFailure {
                completion([:], response.error)
            }
        }
    }
    
    class func userAuthorization( _ user : String, _ password : String, _ isSocial : Bool, _ completion : @escaping ( _ isAuthorization : Bool, _ error : Error?, _ firstAccess : String?) -> Void ) {
        
        let url = urlBase.appendingPathComponent("users/login.json")
        
        let encoding : ParameterEncoding = JSONEncoding.default
        var headers : HTTPHeaders = ["Accept": "application/json"]
        
        if let authorizationHeader = Request.authorizationHeader(user: user, password: password) {
            headers[authorizationHeader.key] = authorizationHeader.value
        }
        let urlString = isSocial ? "\(url.absoluteString)?social=true" : "\(url.absoluteString)?social=false"
       
        Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
            
            if response.result.isSuccess {
                if let resultAPI = response.value {
                    let result : AnyObject = resultAPI as AnyObject
                    if let message = result["message"] {
                            
                        if message == nil {
                            
                            WishareCoreData.deleteUsers({ (error : Error?) in
                                
                                if error == nil {
                                    
                                    let userLogin = User(result)
                                    
                                    if isSocial {
                                        userLogin.username = password
                                    }
                                    
                                    userLogin.password = password
                                    userLogin.isSocial = isSocial
                                    WishareCoreData.saveUser(userLogin, { (error : Error?) in
                                        
                                        if error == nil {
                                            
                                            // UPDATE DO FIRST ACCESS
                                            WishareAPI.firstAccess(userLogin.username!, userLogin.password!, 0, isSocial, { (isSuccess : Bool, error : Error?) in
                                                
                                                if error == nil {
                                                    completion(true, nil, userLogin.faccess)
                                                } else {
                                                    completion(false, error, nil)
                                                }
                                            })
                                        } else {
                                            completion(false, error, nil)
                                        }
                                    })
                                } else {
                                    completion(false, error, nil)
                                }
                            })
                        } else {
                            completion(false, nil, nil)
                        }
                    }
                }
            }
            
            if response.result.isFailure {
                completion(false, response.error, nil)
            }
        }
    }
    
    class func checkedFieldsExistsUser( _ field : String, _ value : String, _ completion : @escaping ( _ isFieldExist : Bool, _ error : Error?) -> Void)    {
        
        let url = urlBase.appendingPathComponent("users/issetField.json")
        let parameters : Parameters = ["User.\(field)": value]
        let encoding : ParameterEncoding = JSONEncoding.default
        let headers : HTTPHeaders = ["Accept": "application/json"]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
            
            if response.result.isSuccess {
                print("Checked")
                
                if let isExist : [String : Bool] = response.value as? [String : Bool] {
                    
                    if isExist["return"]! {
                        completion(false, nil)
                    } else {
                        completion(true, nil)
                    }
                }
            }
            
            if response.result.isFailure {
                completion(false, response.error)
            }
        }
    }
    
    class func recommendFollowStore ( _ limit : Int, _ completion : @escaping ( _  result : [String : AnyObject], _ error : Error?) -> Void) {
       
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getLimit/")
                
                let urlString = user!.isSocial ? "\(url.absoluteString)\(limit).json?social=true" : "\(url.absoluteString)\(limit).json?social=false"
                print(urlString)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                print("============= \(urlString)=================")
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultResponse : [String : AnyObject] = response.value as? [String : AnyObject] {
                            completion(resultResponse, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([:], response.error)
                    }
                }
            } else {
                completion([:], error)
            }
        }
    }
    
    class func followUser( _ idUser : String, isFollow : Bool, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/follow/\(idUser)/\(isFollow).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept": "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func followStore( _ idStore : String, _ isFollow : Bool, _ completion : @escaping ( _ result : [String : AnyObject], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/follow/\(idStore)/\(isFollow).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept": "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                print("==== Follow Store:  \(urlString) ====")
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            completion(resultAPI, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([:], response.error)
                    }
                }
            }
        }
    }
    
    class func followUserFacebook ( _ idFacebook : String, _ isFollow : Bool, _ completion : @escaping ( _ result : [String : AnyObject], _ error : Error?) -> Void ) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/followByFacebookId/\(idFacebook)/\(isFollow).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept": "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }

                print("==== Follow User:  \(urlString) ====")
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            completion(resultAPI, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([:], response.error)
                    }
                }
            }
        }
    }
    
    class func firstAccess ( _ user : String, _ password : String, _ isFirstAccess : Int, _ isSocial : Bool, _ completion : @escaping ( _ isSuccess : Bool, _ error : Error?) -> Void) {
                
        let url = urlBase.appendingPathComponent("users/updateUser.json")
        let urlString : String = "\(url.absoluteString)?social=\(isSocial)"
        let parameters : Parameters = ["faccess": isFirstAccess]
        let encoding : ParameterEncoding = JSONEncoding.default
        var headers : HTTPHeaders = ["Accept" : "application/json"]
        
        
        if let authorizationHeader = Request.authorizationHeader(user: user, password: password) {
            headers[authorizationHeader.key] = authorizationHeader.value
        }
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
            
            if response.result.isSuccess {
                completion(true, nil)
            }
            if response.result.isFailure {
                completion(false, response.error)
            }
        }
    }
    
    class func listPosts( _ lastPost : String?, _ completion : @escaping ( _ result : [Post], _ error : Error?) -> Void ) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
            
                let url = urlBase.appendingPathComponent("posts/index_fast")
                var urlString : String!
                
                if lastPost != nil {
                    
                    urlString = "\(url.absoluteString)/\(Int(lastPost!.convertToDate().timeIntervalSince1970)).json?social=\(user!.isSocial)"
                    print("NOVOS POSTS: \(urlString)")
                    
                } else {
                    urlString = "\(url.absoluteString).json?social=\(user!.isSocial)"
                }
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        print("====== POSTS ========")
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let posts : [[String : AnyObject]] = resultAPI["posts"] as? [[String : AnyObject]] {
                             
                                var postsClass : [Post] = []
                                
                                for post in posts {
                                    postsClass.append(Post(post))
                                }
                                for postProducts in postsClass {

                                    // - Verificando quantidade de produtos
                                    
                                    if postProducts.products.count > 0 {
                                        
                                        // - Configurando rota para products 
                                        
                                        let url = urlBase.appendingPathComponent("products/get/")
                                        var idString : [String] = []
                                        for id in postProducts.products {
                                            idString.append(id["id"]!)
                                        }
                                        
                                        let stringId = idString.joined(separator: ",")
                                        let urlStringId = "\(url.absoluteString)\(stringId).json"
                                        let urlString : String = "\(urlStringId)?social=\(user!.isSocial)"
                                        
                                        Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                                            if response.result.isSuccess {
                                                if let responseAPI : [String : [[String : AnyObject]]] = response.value as? [String : [[String : AnyObject]]] {
                                                    
                                                    for product in responseAPI["Products"]! {

                                                        let prod = Product(product)
                                                        postProducts.products_info.append(prod)
                                                    }
                                                }
                                            }
                                            if response.result.isFailure {
                                                completion([], response.error)
                                            }
                                            
                                        })
                                    }
                                }
                                completion(postsClass, nil)
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func existsNewPosts( _ firstPost : String ,_ completion : @escaping ( _ result : Int?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
             
                let url = urlBase.appendingPathComponent("posts/index_fast")
                var urlString : String!
                
                urlString = "\(url.absoluteString)/\(Int(firstPost.convertToDate().timeIntervalSince1970))/true.json?social=\(user!.isSocial)"
                //print(urlString)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : Int] = response.value as? [String : Int] {
                            completion(resultAPI["posts"], nil)
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion(nil,response.error)
                    }
                    
                })
                
            }
            
        }
        
    }
    
    class func publishPost( _ params : [String : Any], _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
             
                let url = urlBase.appendingPathComponent("posts/mobilepost.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["text": params["text"]!, "picture" : params["picture"]!, "tags" : params["tags"]!]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })

                
            }
            
        }
        
    }
    
    class func getProducts( _ products : [[String : String]], _ completion : @escaping ( _ result : [String : [[String : AnyObject]]], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/get/")
                
                // PRODUTOS AQUI
                var idString : [String] = []
                
                for id in products {
                    idString.append(id["id"]!)
                }
                
                let stringId = idString.joined(separator: ",")
                let urlStringId = "\(url.absoluteString)\(stringId).json"
                let urlString : String = "\(urlStringId)?social=\(user!.isSocial)"
                print(urlString)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let responseAPI : [String : [[String : AnyObject]]] = response.value as? [String : [[String : AnyObject]]] {
                            completion(responseAPI, nil)
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([:], response.error)
                    }
                    
                })
                
            }
            
        }

    }
    
    class func repost( _ id : String, _ text : String, _ completion : @escaping ( _ validate : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                //posts/action/[id do post]/[true/false (add ou remove ) ]/2.json
                let url = urlBase.appendingPathComponent("posts/action/")
                let urlString : String = "\(url.absoluteString)\(id)/true/2.json?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["share_text": text]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(true, nil)
                    }
                    
                    if response.result.isFailure {
                        completion(false, response.error)
                    }
                    
                })
                
            }
        }
    }
    
    class func shareProduct( _ idProduct : String, text : String, _ completion : @escaping ( _ validate : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                //code/products/share/[id do produto].json
                let url = urlBase.appendingPathComponent("products/share/")
                let urlString : String = "\(url.absoluteString)\(idProduct).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                let parameters : Parameters = ["text": text]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(true, nil)
                    }
                    
                    if response.result.isFailure {
                        completion(false, response.error)
                    }
                })
            }
        }
    }
    
    class func reportProblem( _ problem : String, _ text : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/reportProblem.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["problem": problem, "text" : text]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func like( _ id : String, _ type : Bool, _ completion : @escaping ( _ validate : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/action/")
                let urlString : String = "\(url.absoluteString)\(id)/\(type)/1.json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(true, nil)
                    }
                    
                    if response.result.isFailure {
                        completion(false, response.error)
                    }
                    
                })
            }
        }
    }
    
    class func likeProduct ( _ id : String, _ type : Bool, _ completion : @escaping ( _ validate : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/action/")
                let urlString : String = "\(url.absoluteString)\(id)/\(type)/1.json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(true, nil)
                    }
                    
                    if response.result.isFailure {
                        completion(false, response.error)
                    }
                })
            }
        }
    }
    
    class func likeUsers( _ id : String, _ completion : @escaping ( _ validate : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/getFollow/")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var usersPost : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let users : [[String : AnyObject]] = resultAPI["users"] as? [[String : AnyObject]] {
                           
                                for user in users {
                                    usersPost.append(User(likesUser: user))
                                }
                                //completion(usersPost, nil)
                            
                            }
                            if let stores : [[String : AnyObject]] = resultAPI["stores"] as? [[String : AnyObject]] {
                                
                                for store in stores {
                                    usersPost.append(User(likesUserStore: store))
                                }
                                 completion(usersPost, nil)
                            }
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func comments( _ id : String, _ type : Bool, _ action : Int?, _ content : String?, _ completion : @escaping ( _ result : [Comment]?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                if action == nil {
                    
                    
                    let url = urlBase.appendingPathComponent("posts/getcomments/")
                    let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                    
                    let encoding : ParameterEncoding = JSONEncoding.default
                    var headers : HTTPHeaders = ["Accept" : "application/json"]
                    
                    if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                        headers[authorizationHeader.key] = authorizationHeader.value
                    }
                    
                    Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                        
                        if response.result.isSuccess {
                            
                            if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                                if let comments : [[String : AnyObject]] = resultAPI["comments"] as? [[String : AnyObject]] {
                                    
                                    var commentsPost : [Comment] = []
                                    
                                    for comment in comments {
                                        commentsPost.append(Comment(postComments: comment))
                                    }
                                    
                                    completion(commentsPost, nil)
                                }
                            }
                        }
                        
                        if response.result.isFailure {
                            completion(nil, response.error)
                        }
                    })
                } else {
                    
                    let url = urlBase.appendingPathComponent("posts/action/")
                    let urlString : String = "\(url.absoluteString)\(id)/\(type)/\(action!).json?social=\(user!.isSocial)"
                    let parameters : Parameters = ["content": content!]
                    
                    let encoding : ParameterEncoding = JSONEncoding.default
                    var headers : HTTPHeaders = ["Accept" : "application/json"]
                    
                    
                    if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                        headers[authorizationHeader.key] = authorizationHeader.value
                    }
                    Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                        
                        if response.result.isSuccess {
                            let url = urlBase.appendingPathComponent("posts/getcomments/")
                            let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                            
                            let encoding : ParameterEncoding = JSONEncoding.default
                            var headers : HTTPHeaders = ["Accept" : "application/json"]
                            
                            if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                                headers[authorizationHeader.key] = authorizationHeader.value
                            }
                            
                            Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                                
                                if response.result.isSuccess {
                                    
                                    if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                                        if let comments : [[String : AnyObject]] = resultAPI["comments"] as? [[String : AnyObject]] {
                                            
                                            var commentsPost : [Comment] = []
                                            
                                            for comment in comments {
                                                commentsPost.append(Comment(postComments: comment))
                                            }
                                            
                                            completion(commentsPost, nil)
                                            
                                        }
                                    }
                                    
                                }
                                
                                if response.result.isFailure {
                                    completion(nil, response.error)
                                }
                                
                            })
                        }
                        
                        if response.result.isFailure {
                            completion(nil, response.error)
                        }
                    })
                }
            }
        }
    }
    
    class func moreComments( _ id : String, _ lastComment : String, _ completion : @escaping ( _ result : [Comment]?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                    let url = urlBase.appendingPathComponent("posts/getcomments/")
                    let urlString : String = "\(url.absoluteString)\(id)/index:\(lastComment).json?social=\(user!.isSocial)"
                
                    let encoding : ParameterEncoding = JSONEncoding.default
                    var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                    if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                        headers[authorizationHeader.key] = authorizationHeader.value
                    }
                    Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                        
                        if response.result.isSuccess {
                            
                            if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                                if let comments : [[String : AnyObject]] = resultAPI["comments"] as? [[String : AnyObject]] {
                                    
                                    var commentsPost : [Comment] = []
                                    
                                    for comment in comments {
                                        commentsPost.append(Comment(postComments: comment))
                                    }
                                    completion(commentsPost, nil)
                                }
                            }
                        }
                        if response.result.isFailure {
                            completion(nil, response.error)
                        }
                        
                    })
            }
        }
    }

    class func commentDelete( _ id : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                let url = urlBase.appendingPathComponent("posts/removeComments/")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                    
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                        
                    if response.result.isSuccess {
                        completion(nil)
                    }
                        
                    if response.result.isFailure {
                        completion(response.error)
                    }
                })
            }
        }
    }
    
    class func commentEdit( _ id : String, _ content : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/editComments/")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                let parameters : Parameters = ["content": content]
    
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func commentToReport( _ id : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("complaints/newComplaint.json")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                let parameters : Parameters = ["comments_id": id]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func wishlist( _ id : String, _ type : Bool, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/action/")
                let urlString : String = "\(url.absoluteString)\(id)/\(type)/3.json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                print(urlString)
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func wishlistProduct ( _ id : String, _ type : Bool, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/action/")
                let urlString : String = "\(url.absoluteString)\(id)/\(type)/3.json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                print(urlString)
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func toReport( _ id : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("complaints/newComplaint.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                let parameters : Parameters = ["post_id": id]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func userToReport( _ id : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("complaints/newComplaint.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                let parameters : Parameters = ["id_user": id]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func postDelete( _ id : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/app_delete/")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                })
            }
        }
    }
    
    class func postEditing( _ id : String, _ text : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/app_edit/")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                let parameters : Parameters = ["text": text]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func recommendations( _ id : String, _ completion : @escaping ( _ categories : Categories?, _ top : [Product]?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/getRecommendations/")
                let urlString : String = "\(url.absoluteString)\(id).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        let categoryPost : Categories = Categories()
                        var products : [Product] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let categories : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                        
                                for category in categories {
                                    categoryPost.category.append(Category(withProduct: category))
                                }
                            }
                        }
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let categoriesTop : [[String : AnyObject]] = resultAPI["top"] as? [[String : AnyObject]] {
                                
                                for category in categoriesTop {
                                    products.append(Product(byCategory: category))
                                }
                                
                                completion(categoryPost, products, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion(nil, nil, response.error)
                    }
                })
            }
        }
    }
    
    class func suggestMenu( _ completion : @escaping ( _ categories : [Category], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("categories/getSuggestMenu.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let categories : [[String : AnyObject]] = resultAPI["Categories"] as? [[String : AnyObject]] {
                                
                                var categoryPost : [Category] = []
                                
                                for category in categories {
                                    categoryPost.append(Category(category))
                                }
                                
                                completion(categoryPost, nil)
                                
                            }
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func searchTabs( _ searchText : String, _ completion : @escaping ( _ products : [Product], _ stores : [Store], _ users : [User], _ wips : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/searchTabs.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["text": searchText]
                UserDefaults.standard.set("1", forKey: "pageProductsFilters")
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                
                        var productsPost : [Product] = []
                        var storesPost : [Store] = []
                        var usersPost : [User] = []
                        var wipsPost : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let products : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                            
                                for product in products {
                                    productsPost.append(Product(byCategory2: product))
                                }
                            }
                            if let stores : [[String : AnyObject]] = resultAPI["stores"] as? [[String : AnyObject]] {
                                
                                for store in stores {
                                    storesPost.append(Store(byCategory: store))
                                }
                            }
                            if let users : [[String : AnyObject]] = resultAPI["users"] as? [[String : AnyObject]] {
                                
                                for user in users {
                                    usersPost.append(User(byCategory: user))
                                }
                            }
                            if let wips : [[String : AnyObject]] = resultAPI["wips"] as? [[String : AnyObject]] {
                                
                                for wip in wips {
                                    wipsPost.append(User(byCategory: wip))
                                }
                            }
                            print(productsPost.count)
                            
                            var productPostFilter : [Product] = []
                            
                            for prod in productsPost {
                                if !(prod.quantity! == "0" && prod.unavailable! == "R") {
                                    productPostFilter.append(prod)
                                }
                            }
                            print(productPostFilter.count)
                            completion(productPostFilter, storesPost, usersPost, wipsPost, nil)
                        }
                    }
                    if response.result.isFailure {
                       completion([], [], [], [], response.error)
                    }
                })
            }
        }
    }
    
    class func searchProductFilter( _ orderBy : String, _ type : String, _ completion : @escaping ( _ products : [Product], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/getProductFilter/")
                let urlString : String = "\(url.absoluteString)orderby:\(type)/order:\(orderBy).json?social=\(user!.isSocial)"
                print(urlString)
                //let parameters : Parameters = ["text": searchText]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var productsPost : [Product] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let products : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                for product in products {
                                    
                                    if let prod : [String : AnyObject] = product["product"] as? [String : AnyObject] {
                                        //print(prodProduct)
                                        productsPost.append(Product(byCategory: prod))
                                    }
                                }
                            }
                            completion(productsPost, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func searchProductFilterBy( _ text : String, _ category : [String], _ size : [String], _ color : [String], _ brand : [String], _ store : [String], _ orderBy : String, _ type : String, _ completion : @escaping ( _ categories : [String], _ size : [String], _ color : [String], _ brand : [String], _ products : [Product], _ tot : Int, _ categoriesName : [Category] ,_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                let url = urlBase.appendingPathComponent("products/getProductsFilters/")
                let urlString : String = "\(url.absoluteString)orderby:\(type)/order:\(orderBy).json?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["filters" : ["text" : "\(text)", "category" : "\(category)", "size" : "\(size)", "color" : "\(color)", "brand" : "\(brand)", "store" : "\(store)"], "page" : "1"]
                UserDefaults.standard.set("1", forKey: "pageProductsFilters")
                
                //print("====> \(parameters)")
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = [:]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
 
                    if response.result.isSuccess {
                        
                        var productsPost : [Product] = []
                        var size : [String] = []
                        var color :  [String] = []
                        var brand : [String] = []
                        var categories : [String] = []
                        var categoriesName : [Category] = []
                        var tot : Int = 0
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let products : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                for product in products {
                                    if let prod : [String : AnyObject] = product["product"] as? [String : AnyObject] {
                                        productsPost.append(Product(byCategory: prod))
                                    }
                                }
                            }
                            if let sort : [String : AnyObject] = resultAPI["sort"] as? [String : AnyObject] {
                                size.append(contentsOf: sort["size"] as! [String])
                                color.append(contentsOf: sort["color"] as! [String])
                                brand.append(contentsOf: sort["brand"] as! [String])
                                categories.append(contentsOf: sort["categories"] as! [String])
                                
                                let categoriesData = sort["categories-data"] as! [[String : AnyObject]]
                                
                                for cat in categoriesData {
                                    categoriesName.append(Category(categoriesResponse: cat))
                                }
                            }
                            if let totResult : Int = resultAPI["tot"] as? Int {
                                tot = totResult
                            }
                            completion(categories, size, color, brand, productsPost, tot, categoriesName, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], [], [], [], [], 0, [], response.error)
                    }
                })
            }
        }
    }
    
    class func searchProductFilterByPagination( _ text : String, _ category : [String], _ size : [String], _ color : [String], _ brand : [String], _ store : [String], _ orderBy : String, _ type : String, page : String, _ completion : @escaping ( _ products : [Product], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                let url = urlBase.appendingPathComponent("products/getProductsFilters/")
                let urlString : String = "\(url.absoluteString)orderby:\(type)/order:\(orderBy).json?social=\(user!.isSocial)"
                //let urlString : String = "\(url.absoluteString)index:\(lastId).json?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["filters" : ["text" : "\(text)", "category" : "\(category)", "size" : "\(size)", "color" : "\(color)", "brand" : "\(brand)", "store" : "\(store)"], "page" : page]
                
                print(urlString)
                print(parameters)
                                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = [:]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var productsPost : [Product] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let products : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                for product in products {
                                    if let prod : [String : AnyObject] = product["product"] as? [String : AnyObject] {
                                        productsPost.append(Product(byCategory: prod))
                                    }
                                }
                            }
                            completion(productsPost, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func searchProduct(_ limit : String, _ completion : @escaping ( _ products : [Product], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/getLimit/")
                let urlString : String = "\(url.absoluteString)/\(limit).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var productsPost : [Product] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let products : [[String : AnyObject]] = resultAPI["Products"] as? [[String : AnyObject]] {
                                
                                for product in products {
                                    productsPost.append(Product(byCategory2: product))
                                }
                            }
                            completion(productsPost, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func searchStore(_ limit : String, _ completion : @escaping ( _ stores : [Store], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getLimit/")
                let urlString : String = "\(url.absoluteString)/\(limit).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var storePost : [Store] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let stores : [[String : AnyObject]] = resultAPI["Stores"] as? [[String : AnyObject]] {
                            
                                for store in stores {
                                    storePost.append(Store(byCategory: store))
                                }
                                
                            }
                            completion(storePost, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func searchUser(_ limit : String, _ completion : @escaping ( _ users : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/getLimit_ios/")
                let urlString : String = "\(url.absoluteString)/\(limit).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var userPost : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let users : [[String : AnyObject]] = resultAPI["Users"] as? [[String : AnyObject]] {
                                
                                for user in users {
                                    userPost.append(User(bySearch: user))
                                }
                                
                            }
                            completion(userPost, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func waitingStock(_ idProduct : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/waitingStock/")
                let urlString : String = "\(url.absoluteString)/\(idProduct).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func searchWips( _ completion : @escaping ( _ products : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("bloggers/get.json")
                let urlString : String = "\(url.absoluteString).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var usersPost : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let users : [[String : AnyObject]] = resultAPI["wips"] as? [[String : AnyObject]] {
                                
                                for user in users {
                                    usersPost.append(User(bySearch: user))
                                }
                            }
                            completion(usersPost, nil)
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func searchComplete(_ text : String, _ completion : @escaping (_ products : [String], _ stores : [String], _ users : [String], _ wips : [String], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/autoComplete.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["text": text]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var productsPost : [String] = []
                        var storesPost : [String] = []
                        var usersPost : [String] = []
                        var wipsPost : [String] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let words : [String : AnyObject] = resultAPI["words"] as? [String : AnyObject] {
                             
                                if let products : [String] = words["products"] as? [String] {
                                    
                                    for product in products {
                                        productsPost.append(product)
                                    }
                                }
                                if let stores : [String] = words["stores"] as? [String] {
                                    
                                    for store in stores {
                                        storesPost.append(store)
                                    }
                                }
                                if let users : [String] = words["users"] as? [String] {
                                    
                                    for user in users {
                                        usersPost.append(user)
                                    }
                                }
                                if let wips : [String] = words["wips"] as? [String] {
                                    
                                    for wip in wips {
                                        wipsPost.append(wip)
                                    }
                                }
                                completion(productsPost, storesPost, usersPost, wipsPost, nil)
                            }
                        }
                    }
                    if response.result.isFailure {
                        completion([], [], [], [], response.error)
                    }
                })
            }
        }
    }
    
    class func commentMentions ( _ completion : @escaping ( _ mentions : [Mention], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/getTags.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var mentionsPost : [Mention] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let words : [String : AnyObject] = resultAPI["words"] as? [String : AnyObject] {
                                
                                if let stores : [[String : String]] = words["stores"] as? [[String : String]] {
                                    
                                    for store in stores {
                                        mentionsPost.append(Mention(name: store))
                                    }
                                }
                                if let users : [[String : String]] = words["users"] as? [[String : String]] {
                                    
                                    for user in users {
                                        mentionsPost.append(Mention(username: user))
                                    }
                                }
                                completion(mentionsPost, nil)
                            }
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
    
    class func getAllStores ( _ completion : @escaping ( _ stores : [Mention], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getStores.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        
                        var mentionsPost : [Mention] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let stores : [[String : AnyObject]] = resultAPI["Stores"] as? [[String : AnyObject]] {
                                
                                for store in stores {
                                    mentionsPost.append(Mention(byStore: store))
                                }

                                completion(mentionsPost, nil)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                    
                })
                
                
            }
            
        }
        
        
    }
    
    
    class func storeProfile(idStore: String, _ completion: @escaping ( _ store : Store?, _ category : [Category]?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            if error ==  nil && user != nil {
                let url = urlBase.appendingPathComponent("stores/getStoreProfile_ios/")
                let urlString : String = "\(url.absoluteString)\(idStore).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response: DataResponse<Any>) in
                    switch response.result {
                    case .success(let value):
                        var store : Store!
                        var categoriesPost : [Category] = []
                        
                        if let resultAPI = value as? [String:AnyObject] {
                            if let storeResponse = resultAPI["stores"] as? [String:AnyObject] {
                                if let postsResponse : [[String : AnyObject]] = resultAPI["posts"] as? [[String : AnyObject]] {
                                    store = Store(storeForProfile: storeResponse, posts: postsResponse)
                                }
                            }
                            
                            if let promos: [[String : AnyObject]] = resultAPI["promo"] as? [[String : AnyObject]] {
                                
                                if promos.count > 0 {
                                    
                                    let promoCategory: Category = Category(id: "p", name: "Promoção")
                                    
                                    for (_, promo) in promos.enumerated() {
                                        if let campanha : [String : AnyObject] = promo["Campaign"] as? [String : AnyObject] {
                                            promoCategory.promo.append(Promo(id: campanha["id"] as? String, name: campanha["image-title"] as? String, imageUrl: campanha["image-url"] as? String))
                                        }
                                    }
                                    
                                    categoriesPost.append(promoCategory)
                                    
                                }
                                
                            }
                            
                            if let destaques : [[String : AnyObject]] = resultAPI["featuredProducts"] as? [[String : AnyObject]] {
                                
                                if destaques.count > 0 {
                                    
                                    let destaqueCategory : Category = Category(id: "x", name: "Destaques")
                                    
                                    for (key, destaque) in destaques.enumerated() {
                                        if key <= 3 {
                                            destaqueCategory.products.append(Product(forCategoryFeatured: destaque))
                                        }
                                    }
                                    
                                    categoriesPost.append(destaqueCategory)
                                    
                                }
                            }
                            completion(store, categoriesPost, nil)
                            break
                        }
                        completion(nil, nil, error)
                        break
                        
                    case .failure(let error):
                        print(error)
                        completion(nil, nil, error)
                        break
                    }
                })
                
            
            
            
            }
        }
        
    }
    
    
    class func storeFeed ( _ idStore : String, _ completion : @escaping ( _ store : Store?, _ category : [Category]?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/store_feed/")
                let urlString : String = "\(url.absoluteString)\(idStore).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                
                        var categoriesPost : [Category] = []
                        var categoriesClone : [Category] = []
                        var store : Store!
                        
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let stores : [String : AnyObject] = resultAPI["stores"] as? [String : AnyObject] {
                                
                                if let posts : [[String : AnyObject]] = resultAPI["posts"] as? [[String : AnyObject]] {
                                    store = Store(byCategory: stores, withPosts: true, posts: posts)
                                }
                                
                                
                                // --------ADICIONAR PRODUCTS DO POSTS---------
                                
                                
                                for post in store.posts {
                                    
                                    if post.products.count > 0 {
                                        
                                        let url = urlBase.appendingPathComponent("products/get/")
                                        
                                        var idString : [String] = []
                                        
                                        for id in post.products {
                                            idString.append(id["id"]!)
                                        }
                                        
                                        let stringId = idString.joined(separator: ",")
                                        let urlStringId = "\(url.absoluteString)\(stringId).json"
                                        let urlString : String = "\(urlStringId)?social=\(user!.isSocial)"
                                        
                                        
                                        let response = Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON()
                                        
                                        if response.result.isSuccess {
                                            
                                            if let responseAPI : [String : [[String : AnyObject]]] = response.value as? [String : [[String : AnyObject]]] {
                                                
                                                for product in responseAPI["Products"]! {
                                                    
                                                    let prod = Product(product)
                                                    post.products_info.append(prod)
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                        if response.result.isFailure {
                                           completion(nil, nil, response.error)
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                                
                                
                                // --------------------------------------------
                                
                                if let categories : [[String : AnyObject]] = stores["Category"] as? [[String : AnyObject]] {
    
                                    var categoryInstance : Category!
                                    
                                    for category in categories {
                                        categoryInstance = Category(categoriesResponse: category)
                                        categoriesPost.append(categoryInstance)
                                    }
                                    
    
                                    // -
                                    
                                    let url = urlBase.appendingPathComponent("products/getProductsByCategory/")
                                    let urlString : String = "\(url.absoluteString)\(store.id!).json"
                                    
                                    let response = Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON()
                                    
                                    if response.result.isSuccess {
                                        
                                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                                            if let productsCategories : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                                
                                                for (key, prod) in productsCategories.enumerated() {
                                                    
                                                    if let productsProducts : [[String : AnyObject]] = prod["products"] as? [[String : AnyObject]] {
                                                        
                                                        for p in productsProducts {
                                                            
                                                            categoriesPost[key].products.append(Product(byCategory: p))
                                                            
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                
                                
                                
                            }
                            
                            if let destaques : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                if destaques.count > 0 {
                                    
                                    let destaqueCategory : Category = Category(id: "x", name: "Destaques")
                                    
                                    for (key, destaque) in destaques.enumerated() {
                                        if key <= 3 {
                                            destaqueCategory.products.append(Product(byCategoryDestaque: destaque))
                                        }
                                    }
                                    
                                    categoriesClone.append(destaqueCategory)
                                    categoriesClone.append(contentsOf: categoriesPost)
                                    categoriesPost.removeAll()
                                    categoriesPost.append(contentsOf: categoriesClone)
                                    
                                }
 
                            }
                            
                            
                            if let promos : [[String : AnyObject]] = resultAPI["promo"] as? [[String : AnyObject]] {
                                
                                if promos.count > 0 {
                                    
                                    let promoCategory : Category = Category(id: "p", name: "Promoção")
                                    
                                    for (_, promo) in promos.enumerated() {
                                        if let campanha : [String : AnyObject] = promo["Campaign"] as? [String : AnyObject] {
                                            promoCategory.promo.append(Promo(id: campanha["id"] as? String, name: campanha["image-title"] as? String, imageUrl: campanha["image-url"] as? String))
                                        }
                                    }
                                    
                                    categoriesClone.removeAll()
                                    categoriesClone.append(promoCategory)
                                    categoriesClone.append(contentsOf: categoriesPost)
                                    categoriesPost.removeAll()
                                    categoriesPost.append(contentsOf: categoriesClone)
                                    
                                }
                                
                            }
                            
                            completion(store, categoriesPost, nil)
                        }
                        
                        
                    }
                    
                    if response.result.isFailure {
                        completion(nil, nil, response.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    class func storeDetail ( _ idStore : String, _ completion : @escaping ( _ store : Store?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getCartStore/")
                let urlString : String = "\(url.absoluteString)\(idStore).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                    
                        var store : Store!
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let stores : [String : AnyObject] = resultAPI["stores"] as? [String : AnyObject] {
                                store = Store(byCategory: stores)
                                completion(store, nil)
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion(nil, response.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }

    class func storeGetFollowers( _ idStore : String, _ completion : @escaping ( _ user : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getFollowers/")
                let urlString : String = "\(url.absoluteString)\(idStore).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var userPost : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let users : [[String : AnyObject]] = resultAPI["users"] as? [[String : AnyObject]] {
                                
                                for user in users {
                                    if let u : [String : AnyObject] = user["User"] as? [String : AnyObject] {
                                        userPost.append(User(byCategory: u))
                                    }
                                }
                                
                                completion(userPost, nil)
                                
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.result.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    class func productsByCategory ( _ idStore : String, _ idCategory : String?, _ completion : @escaping ( _ categories : [Category], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/getProductsByCategory/")
                var urlString : String = "\(url.absoluteString)\(idStore)"
                
                if let categoryId = idCategory {
                    urlString.append("/\(categoryId)")
                }
                
                urlString.append(".json")
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let productsCategories : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                var categoriesPost : [Category] = []
                                
                                for (_, prod) in productsCategories.enumerated() {
                                
                                    
                                    if let productsCategory : [String : AnyObject] = prod["category"] as? [String : AnyObject] {
                                        categoriesPost.append(Category(categoriesResponse: productsCategory))
                                    }
                                    
                                    
                                    if let productsProducts : [[String : AnyObject]] = prod["products"] as? [[String : AnyObject]] {
                                        for p in productsProducts {
                                            categoriesPost.last?.products.append(Product(byCategory2: p))
                                        }
                                    }
                                    
                                    
                                }
                                
                                completion(categoriesPost, nil)
                                
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.result.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    class func searchProductFilterByStore( _ text : String, _ category : [String], _ size : [String], _ color : [String], _ brand : [String], _ store : [String], _ orderBy : String, _ type : String, _ featured : Bool, _ page : String, _ completion : @escaping ( _ categories : [String], _ size : [String], _ color : [String], _ brand : [String], _ products : [Product], _ tot : Int, _ categoriesName : [Category] , _ fatherCategory : [String], _ childrenOneCategory : [String], _ childrenTwoCategory : [String], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/getProductsFilters/")
                let urlString : String = "\(url.absoluteString)orderby:\(type)/order:\(orderBy).json?social=\(user!.isSocial)"
                
                
                var parameters : Parameters!
                
                //let parameters : Parameters = ["filters" : ["text" : "\(text)", "category" : "\(category)", "size" : "\(size)", "color" : "\(color)", "brand" : "\(brand)", "store" : "\(store)", "featured" : "\(featured)"], "page" : page]
                
                if let promo : String = UserDefaults.standard.object(forKey: "promoCampanha") as? String {
                    parameters = ["filters" : ["text" : "\(text)", "category" : "[]", "size" : "\(size)", "color" : "\(color)", "brand" : "\(brand)", "store" : "\(store)", "promo_id" : "[\"\(promo)\"]" ], "page" : page]
                } else {
                    parameters = ["filters" : ["text" : "\(text)", "category" : "\(category)", "size" : "\(size)", "color" : "\(color)", "brand" : "\(brand)", "store" : "\(store)", "featured" : "\(featured)"], "page" : page]
                }
                
                
                UserDefaults.standard.set(page, forKey: "pageProductsFiltersCategory")
                UserDefaults.standard.set(type, forKey: "orderByCategories")
                UserDefaults.standard.set(orderBy, forKey: "typeByCategories")
                
                print(urlString)
                print(parameters)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = [:]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    
                    if response.result.isSuccess {
                        
                        var productsPost : [Product] = []
                        var size : [String] = []
                        var color :  [String] = []
                        var brand : [String] = []
                        var categories : [String] = []
                        var categoriesName : [Category] = []
                        var tot : Int = 0
                        
                        var fatherCategory : [String] = []
                        var childrenOneCategory : [String] = []
                        var childrenTwoCategory : [String] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let products : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                for product in products {
                                    if let prod : [String : AnyObject] = product["product"] as? [String : AnyObject] {
                                        productsPost.append(Product(byCategory2: prod))
                                    }
                                }
                                
                            }
                            
                            
                            
                            if let sort : [String : AnyObject] = resultAPI["sort"] as? [String : AnyObject] {
                                
                                size.append(contentsOf: sort["size"] as! [String])
                                color.append(contentsOf: sort["color"] as! [String])
                                brand.append(contentsOf: sort["brand"] as! [String])
                                categories.append(contentsOf: sort["categories"] as! [String])
                                
                                let categoriesData = sort["categories-data"] as! [[String : AnyObject]]
                                
                                for cat in categoriesData {
                                    categoriesName.append(Category(categoriesResponse: cat))
                                }

                                if let cat : [String : AnyObject] = sort["cat"] as? [String : AnyObject] {
                                    
                                    if let n2 : [String] = cat["n2"] as? [String] {
                                        fatherCategory.append(contentsOf: n2)
                                    }
                                    
                                    if let n1 : [String] = cat["n1"] as? [String] {
                                        childrenOneCategory.append(contentsOf: n1)
                                    }
                                    
                                    if let n0 : [String] = cat["n0"] as? [String] {
                                        childrenTwoCategory.append(contentsOf: n0)
                                    }
                                    
                                }
                            }
                            
                            if let totResult : Int = resultAPI["tot"] as? Int {
                                tot = totResult
                            }
                            
                            completion(categories, size, color, brand, productsPost, tot, categoriesName, fatherCategory, childrenOneCategory, childrenTwoCategory, nil)
                            
                        }
                        
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], [], [], [], [], 0, [], [], [], [], response.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    class func recentSearch ( _ completion : @escaping ( _ words : [String], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("UsersSearchs/getTopSearch.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let wordsSearch : [[String : AnyObject]] = resultAPI["Words"] as? [[String : AnyObject]] {
                                
                                var words : [String] = []
                                
                                for word in wordsSearch {
                                    if let w : [String : String] = word["UsersSearch"] as? [String : String] {
                                        if w["word"]! != "" {
                                            words.append(w["word"]!)
                                        }
                                    }
                                }
                                
                                completion(words, nil)
                                
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.result.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    /**
        Método de requisicao de detalhes de produtos
     
        - Alternativa de reconexao em caso de falha na requisicao, no proprio erro ele realiza outra requisicao;
        - Construtor Store (postStore)
        - Construtor Product (byProductDetail)
     
        - Parameters: 
            - idProduct: ID do produto
     
        - parameter completion: Bloco de código executado
            - parameter product: Retorna um objeto de produto
            - parameter store: Retorna um objeto de store
            - parameter error: Retorna um objeto error caso exista
     
    */
    class func productDetail ( idProduct : String, numberOfTime : Int? = nil, _ completion : @escaping ( _ product : Product, _ store : Store, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/products_feed/")
                let urlString : String = "\(url.absoluteString)\(idProduct).json?social=\(user!.isSocial)"
                
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let products : [[String : AnyObject]] = resultAPI["products"] as? [[String : AnyObject]] {
                                
                                var productResponse : Product!
                                var storeResponse : Store!
                                
                                if let productJson : [String : AnyObject] = products.first?["product"] as? [String : AnyObject] {
                                    productResponse = Product(byProductDetail: productJson)
                                    
                                }
                                
                                if let storeJson : [String : AnyObject] = products.first?["store"] as? [String : AnyObject] {
                                    storeResponse = Store(postStore: storeJson)
                                }
                                
                                completion(productResponse, storeResponse, nil)
                                
                            } else if let noProducts : [String : AnyObject] = resultAPI["products"] as? [String : AnyObject] {
                                if let noProduct : String = noProducts["code"] as? String {
                                    
                                    let productError : Product = Product(id: "-1", pId: "", name: noProduct, desc: "", brand: "", price: "", promo_price: "", unavailable: "", quantity: "", picture: "")
                                    let storeError : Store = Store(id: "", name: "", picture: "", advance_commission: "")
                                    
                                    completion(productError, storeError, NSError(domain: noProduct, code: -1, userInfo: nil))
                                    
                                }
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            self.productDetail(idProduct: idProduct, numberOfTime: 1, { (product : Product, store : Store, error : Error?) in
                                if error == nil {
                                    completion(product, store, nil)
                                }
                            })
                            
                        } else {
                            
                            let productError : Product = Product(id: "", pId: "", name: "", desc: "", brand: "", price: "", promo_price: "", unavailable: "", quantity: "", picture: "")
                            let storeError : Store = Store(id: "", name: "", picture: "", advance_commission: "")
                            
                            completion(productError, storeError, response.error)
                        }
                        

                        
                    }
                })
            }
        }
    }
    
    
    /**
        Método de requisicao de rating de produtos
     */
    class func ratingProduct ( idProduct : String, numberOfTime : Int? = nil, _ completion : @escaping ( _ rate : [String : Int], _ tot : Int, _ users : [[String : AnyObject]], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("rateproducts/get_all/")
                let urlString : String = "\(url.absoluteString)\(idProduct).json?social=\(user!.isSocial)"
                
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                print(urlString)
                print(user!.username!)
                print(user!.password!)
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            var rateResponse : [String : Int] = [:]
                            var totResponse : Int = 0
                            var usersResponse : [[String : AnyObject]] = []
                            
                            if let rateJson : [String : Int] = resultAPI["rate"] as? [String : Int] {
                                rateResponse = rateJson
                            }
                            
                            if let totJson : Int = resultAPI["tot"] as? Int {
                                totResponse = totJson
                            }
                            
                            if let usersJson : [[String : AnyObject]] = resultAPI["allrate"] as? [[String : AnyObject]] {
                                usersResponse = usersJson
                            }
                            

                            completion(rateResponse, totResponse, usersResponse, nil)
                            
                        }
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            self.ratingProduct(idProduct: idProduct, numberOfTime: 1, { (rate : [String : Int], tot : Int, users : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    completion(rate, tot, users, nil)
                                }
                            })
                            
                        } else {
                            completion([:], 0, [], response.error)
                        }
                        
                        
                        
                    }
                })
            }
        }
    }
    
    /**
        Método de requisicao de hashtags de produtos
     */
    class func hashTagsProduct ( text : String, numberOfTime : Int? = nil, _ completion : @escaping ( _ products : [Product], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/getHashTags.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                let parameters : Parameters = ["text": text]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var productsResponse : [Product] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let tags : [String : AnyObject] = resultAPI["tags"] as? [String : AnyObject] {
                                
                                if let products : [[String : AnyObject]] = tags["products"] as? [[String : AnyObject]] {
                                    
                                    for product in products {
                                        print(product)
                                        productsResponse.append(Product(byCategory: product))
                                    }
                                    
                                    completion(productsResponse, nil)
                                }
                                
                            }
                            
                        }
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            self.hashTagsProduct(text: text, numberOfTime: 1, { (products : [Product], error : Error?) in
                                if error == nil {
                                    completion(products, nil)
                                }
                            })
                            
                        } else {
                            completion([], response.error)
                        }
                        
                    }
                })
            }
        }
    }
    
    class func wishPoints( _ completion : @escaping ( _ wishPoints : WishPoints?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("UsersPoints/getUserPointsObjectives.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
    
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.result.value as? [String : AnyObject] {
                            let wish = WishPoints(resultAPI)
                            completion(wish, nil)
                        }
                    
                    }
                    
                    if response.result.isFailure {
                        completion(nil, response.error)
                    }
                    
                }
                
                
            }
        }
        
    }
    
    class func likeAndWishProductDetail( _ pId : String, type : Bool, _ completion : @escaping ( _ users : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/getFollow/")
                let urlString : String = "\(url.absoluteString)\(pId)/product/\(((type) ? "like" : "wish")).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var usersResponse : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let users : [[String : AnyObject]] = resultAPI["users"] as? [[String : AnyObject]] {
                                
                                for user in users {
                                    usersResponse.append(User(likesUser: user))
                                }
                                
                                completion(usersResponse, nil)
                                
                            }
                        }
                        
                        
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    class func ratingProductPost ( rating : Float , pId : String, title : String, description : String, anonymous : String,_ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("rateproducts/post_rate.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                let parameters : Parameters = ["anonymous": anonymous, "rate" : rating, "product_id" : pId, "title" : title, "description" : description]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                       completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    class func ratingProductRemove ( pId : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("rateproducts/remove_rate")
                let urlString : String = "\(url.absoluteString).json?social=\(user!.isSocial)"
                let parameters : Parameters = ["product_id": pId]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
    }
    
    // Shopping Cart
    
    class func postageService( storeId : String, cep : String, products : [Product], numberOfTime : Int? = nil, _ completion : @escaping ( _ result : [[String : AnyObject]], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("postageService/getPrice/")
                let urlString : String = "\(url.absoluteString)\(storeId)/\(cep).json?social=\(user!.isSocial)"
                
                var productsRequest : [[String : String]] = []
                
                for product in products {
                    productsRequest.append(["id" : product.id!, "qtd" : product.select_quatity!])
                }
                
                let parameters : Parameters = ["products" : productsRequest]
                
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let postage : [[String : AnyObject]] = resultAPI["return"] as? [[String : AnyObject]] {
                            
                                completion(postage, nil)
                                
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            postageService(storeId: storeId, cep: cep, products: products, numberOfTime: 1, { (result : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    completion(result, nil)
                                }
                            })
                            
                            
                            
                        } else {
                            
                            completion([], response.error)
                            
                        }
                    }
                    
                })
            }
        }
        
    }
    
    class func getAddressByCep( cep : String, numberOfTime : Int? = nil, _ completion : @escaping ( _ address : [String : AnyObject], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("PostageService/getAddressByCEP/")
                let urlString : String = "\(url.absoluteString)\(cep).json?social=\(user!.isSocial)"

                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let postage : [String : AnyObject] = resultAPI["address"] as? [String : AnyObject] {
                                if let address : [String : AnyObject] = postage["return"] as? [String : AnyObject] {
                                    completion(address, nil)
                                }
                            } else {
                                completion(["message" : "não encontrado" as AnyObject], nil)
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            getAddressByCep(cep: cep, numberOfTime: 1, { (address : [String : AnyObject], error : Error?) in
                                if error == nil {
                                    completion(address, nil)
                                }
                            })
                            
                        } else {
                            
                            completion([:], response.error)
                            
                        }
                    }
                    
                })
            }
        }

        
    }

    class func addresses (name : String, postCode : String, address : String, number : String, more : String, city : String, state : String, country : String, type : String, neighborhood : String, recipientName : String, numberOfTime : Int? = nil, newAddress : Bool = true, idAddress : String? = nil, _ completion : @escaping ( _ result : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("addresses/save")
                
                var urlString : String!
                
                if !newAddress {
                    urlString = "\(url.absoluteString)/\(idAddress!).json?social=\(user!.isSocial)"
                } else {
                    urlString = "\(url.absoluteString).json?social=\(user!.isSocial)"
                }
                
                let parameters : Parameters = ["name" : name, "post_code" : postCode, "address" : address, "number" : number, "more" : more, "city" : city, "state" : state, "country" : country, "type" : type, "neighborhood" : neighborhood, "recipient_name" : recipientName]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let postage : [String : AnyObject] = resultAPI["return"] as? [String : AnyObject] {
                                if let _ : [String : AnyObject] = postage["Address"] as? [String : AnyObject] {
                                    
                                    completion(true, nil)
                                    
                                }
                            }
                        } else {
                            completion(false, nil)
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            addresses(name: name, postCode: postCode, address: address, number: number, more: more, city: city, state: state, country: country, type: type, neighborhood: neighborhood, recipientName: recipientName, numberOfTime: 1, newAddress: newAddress, idAddress: idAddress, { (validate : Bool, error : Error?) in
                                
                                if validate ==  true {
                                    
                                    completion(validate, nil)
                                    
                                }
                                
                            })
                            
                            
                        } else {
                            
                            completion(false, response.error)
                            
                        }
                    }
                    
                })
            }
        }
        
        
    }
    
    
    class func removeAddress(idAddress : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/removeAddress/")
                let urlString = "\(url.absoluteString)\(idAddress).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
        
        
    }
    
    class func userCards (number : String, name : String, valid : String, flag : String, numberOfTime : Int? = nil, _ completion : @escaping ( _ validate : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("userscards/addCard")
                let urlString = "\(url.absoluteString).json?social=\(user!.isSocial)"

                let parameters : Parameters = ["number" : number, "name" : name, "valid" : valid, "flag" : flag]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let postage : [String : AnyObject] = resultAPI["return"] as? [String : AnyObject] {
                                if let _ : [String : AnyObject] = postage["UsersCard"] as? [String : AnyObject] {
                                    completion(true, nil)
                                }
                            }
                        } else {
                            completion(false, nil)
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            userCards(number: number, name: name, valid: valid, flag: flag, { (validate : Bool, error : Error?) in
                                
                                if validate == true {
                                    completion(validate, nil)
                                }
                                
                            })
                            
                            
                            
                        } else {
                            
                            completion(false, response.error)
                            
                        }
                    }
                    
                })
            }
        }
        
    }
    
    class func getCards ( _ completion : @escaping ( _ cards : [[String : AnyObject]], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("userscards/getCards")
                let urlString : String = "\(url.absoluteString).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let cards : [[String : AnyObject]] = resultAPI["cards"] as? [[String : AnyObject]] {
                                completion(cards, nil)
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                    
                })
            }
        }
        
    }
    
    //UsersCards/removeCard/[id do cartao].json
    class func removeCard ( idCard : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("UsersCards/removeCard/")
                let urlString : String = "\(url.absoluteString)\(idCard).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                })
            }
        }
        
    }
    
    class func getAddresses( numberOfTime : Int? = nil, _ completion : @escaping ( _ addresses : [[String : AnyObject]], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("addresses/getAddress")
                let urlString : String = "\(url.absoluteString).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let postage : [[String : AnyObject]] = resultAPI["Adresses"] as? [[String : AnyObject]] {
                                
                                completion(postage, nil)
                                
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        if numberOfTime == nil {
                            
                            getAddresses(numberOfTime: 1, { (addresses : [[String : AnyObject]], error : Error?) in
                                if error == nil {
                                    completion(addresses, nil)
                                }
                            })
                            
                        } else {
                            
                            completion([], response.error)
                            
                        }
                    }
                    
                })
            }
        }

        
    }
    
    class func saveOrder (idAddress : String, cuponCode : String = "", products : [Product], idStore : String, idCard : String, installments : String, freightValue : String, service : String, delivery_days : String, cvv : String, numberOfTimes : Int? = nil, _ completion : @escaping (_ validate : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("orders/saveOrder")
                let urlString : String = "\(url.absoluteString).json?social=\(user!.isSocial)"
                
                var productsRequest : [[String : String]] = []
                
                for product in products {
                    productsRequest.append(["id" : product.id!, "qtd" : product.select_quatity!])
                }
                
                let parameters : Parameters = ["addressid" : idAddress, "cuponcode" : cuponCode, "products" : productsRequest, "idstore" : idStore, "cardid" : idCard, "installments" : installments, "freightValue" : freightValue, "service" : service, "delivery_days" : delivery_days, "cvv" : cvv]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
            
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let message = resultAPI["message"] as? String, message.toBool() == true{
                                completion(true, nil)
                            }
                            
                            completion(false, nil)
                        }
                    }
                    
                    if response.result.isFailure {
                        completion(false, response.error)
                    }
                    
                })
            }
        }
        
    }
    
    class func getUser (idUser : String, numberOfTime : Int? = nil, _ completion : @escaping ( _ user : User?, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/get")
                let urlString = "\(url.absoluteString).json?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["id" : [idUser]]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            if let users : [[String : AnyObject]] = resultAPI["Users"] as? [[String : AnyObject]] {
                                
                                let userInstance = User(likesUser: users[0])
                                completion(userInstance, nil)
                                
                            }
                            
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        
                        
                        completion(nil, response.error)
                        
                    }
                    
                })
            }
        }
        
    }
    
    class func updateUser ( name : String, lastName : String, rg : String, cpf : String, telefone : String, celular : String, _ completion : @escaping ( _ isSuccess : Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["name": name, "last_name" : lastName, "rg" : rg, "cpf" : cpf, "phone" : telefone, "cell" : celular]
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(true, nil)
                    }
                    
                    if response.result.isFailure {
                        completion(false, response.error)
                    }
                    
                }
                
               
            }
        }

    }
    
    // Page User
    
    // TODO: - GET INFO USER
    
    
    class func getInfoUser(idUser:String, _ completion: @escaping (_ user: User?, _ posts: [Post]?, _ error: Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/getProfile_ios/")
                let urlString : String = "\(url.absoluteString)\(idUser).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    
                    if response.result.isSuccess {
                        if let resultAPI: [String : AnyObject] = response.value as? [String:AnyObject] {
                            
                            var userProfile: User?
                            
                            if let users: [String:AnyObject] = resultAPI["users"] as? [String:AnyObject] {
                                if let user: [String:AnyObject] = users["User"] as? [String:AnyObject] {
                                    
                                    print(user)
                                    if let id = user["id"] as? String, let picture = user["picture"] as? String, let username = user["username"] as? String, let isPrivate = user["private"] as? String, let profilePicture = user["profile_image"] as? String {
                                        
                                        var profileDict: [String: Any]
                                        
                                        if let dictionary = profilePicture.convertToDictionary() {
                                            profileDict = dictionary
                                        } else {
                                            profileDict = [
                                                "type":"local",
                                                "url":"5"
                                            ]
                                        }
                                        
                                        userProfile = User(forProfileWithId: id, username: username, picture: picture, profileBackground: profileDict as [String : AnyObject], isPrivate: isPrivate)
                                    }
                                    
                                    if let zero: [String : AnyObject] = users["0"] as? [String:AnyObject] {
                                        if let followingBool = zero["ifollowing"] as? String {
                                            if followingBool == "true" {
                                                userProfile?.following = true
                                            } else {
                                                userProfile?.following = false
                                            }
                                        }
                                        userProfile?.isFollowing = zero["ifollowing"] as? String
                                        userProfile?.qtd_followers = zero["followers"] as? String
                                        userProfile?.qtd_following = zero["following"] as? String
                                        userProfile?.qtd_wips = zero["wips"] as? String
                                        userProfile?.qtd_posts = zero["posts"] as? String
                                        userProfile?.qtd_wishes_products = zero["wishes"] as? String
                                        userProfile?.qtd_orders = zero["orders"] as? String
                                        userProfile?.qtd_stores = zero["stores"] as? String
                                    }
                                }
                            }

                            completion(userProfile, nil, nil)
                        }
                    }
                    
                    if response.result.isFailure {
                        completion(nil, nil, response.error)
                    }
                }
            }
        }
        
    }
    
    class func getInformationUser (idUser : String, _ completion : @escaping (_ user : User?, _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/get.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = ["id" : [ [idUser] ]]
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let users : [[String : AnyObject]] = resultAPI["Users"] as? [[String : AnyObject]] {
                                
                                let user : User = User(byUser: users[0])
                                
                                if let zeroFollowing : [String : AnyObject] = users[0]["0"] as? [String : AnyObject] {
                                    user.isFollowing = zeroFollowing["following"] as? String
                                }
                                
                                completion(user, nil)
                            
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion(nil, response.error)
                    }
                    
                }
                
                
            }
        }
        
    }
    
    
    class func updateInformationUser (username : String, name : String, lastName : String, birthday : String?, gender : String?, rg : String, cpf : String, phone : String, cell : String, _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                
                var parameters : Parameters = [
                    "username" : username,
                    "name" : name,
                    "last_name" : lastName,
                    "rg" : rg,
                    "cpf" : cpf,
                    "phone" : phone,
                    "cell" : cell,
                ]
                
                if let birthday = birthday, birthday != "" {
                    parameters["birthday_date"] = birthday
                }
                
                if let gender = gender, gender != "" {
                    
                    let genderToSend = (gender == "Masculino") ? "M" : "F"
                    
                    parameters["gender"] = genderToSend
                }
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {

                        userAuthorization(username, user!.password!, user!.isSocial, { (validate : Bool, error : Error?, _) in
                            
                            if error == nil {
                                completion(nil)
                            }
                            
                        })
                        
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                    
                }
                
                
            }
        }
        
    }
    
    class func getFollowersUser( idUser : String, _ completion : @escaping ( _ users : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/get/followers/\(idUser).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var userResponse : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let returnResponse : [[String : AnyObject]] = resultAPI["return"] as? [[String : AnyObject]] {
                                
                                for userReturn in returnResponse {
                                    userResponse.append(User(byUser: userReturn))
                                }
                                
                                completion(userResponse, nil)

                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                    
                }
                
                
            }
        }
        
    }
    
    class func getFollowingUser( idUser : String, _ completion : @escaping ( _ users : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/get/following/\(idUser).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var userResponse : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let returnResponse : [[String : AnyObject]] = resultAPI["return"] as? [[String : AnyObject]] {
                                
                                for userReturn in returnResponse {
                                    userResponse.append(User(byUser: userReturn))
                                }
                                
                                completion(userResponse, nil)
                                
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                    
                }
                
                
            }
        }
        
    }
    
    class func getWipsUser( idUser : String, _ completion : @escaping ( _ users : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("bloggers/get.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                let parameters : Parameters = [
                    "following" : idUser,
                ]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var userResponse : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let returnResponse : [[String : AnyObject]] = resultAPI["wips"] as? [[String : AnyObject]] {
                                
                                for userReturn in returnResponse {
                                    userResponse.append(User(byUser: userReturn))
                                }
                                
                                completion(userResponse, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func getStoresUser( idUser : String, _ completion : @escaping ( _ users : [Store], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/get/stores/\(idUser).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var storeResponse : [Store] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let returnResponse : [[String : AnyObject]] = resultAPI["return"] as? [[String : AnyObject]] {
                                
                                for storeReturn in returnResponse {
                                    storeResponse.append(Store(byCategory: storeReturn))
                                }
                                
                                completion(storeResponse, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func deleteAccount ( _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "active" : 0,
                ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    
    class func reactiveAccount ( _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "active" : 1,
                    ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func updateEmailUser ( newEmail : String, informationByEmail : String,_ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "email" : newEmail,
                    "optin" : informationByEmail
                ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func updateUserProfileCover (type : String, content : String, image : String, _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUserProfileCover.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "type" : type,
                    "content" : content,
                    "image" : image
                ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(nil)
                    }
                }
            }
        }
    }
    
    class func updatePassword (newPassoword : String, _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "pwd" : newPassoword
                ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        let user : User = User()
                        
                        WishareCoreData.getFirstUser({ (userCoreData : UserActive?, error : Error?) in
                            
                            if error == nil {
                                
                                user.birthday_date = userCoreData!.birthday_date
                                user.cell = userCoreData!.cell
                                user.cpf = userCoreData!.cpf
                                user.email = userCoreData!.email
                                user.gender = userCoreData!.gender
                                user.name = userCoreData!.name
                                
                                if let responsePassword : [String : String] = response.value as? [String : String] {
                                    user.password = responsePassword["pwd"]
                                }
                                
                                user.picture = userCoreData!.picture
                                user.username = userCoreData!.username
                                user.isSocial = userCoreData!.isSocial
                                user.id = userCoreData!.id
                                
                            }
                        })
                        WishareCoreData.deleteUsers({ (error : Error?) in
                            if error == nil {
                                
                                WishareCoreData.saveUser(user, { (error : Error?) in
                                    if error == nil {
                                        completion(nil)
                                    }
                                })
                            }
                        })
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func updateUserPicture (picture : String, _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/updateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "profilePicture" : picture
                ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        let user : User = User()
                        
                        WishareCoreData.getFirstUser({ (userCoreData : UserActive?, error : Error?) in
                            
                            if error == nil {
                                
                                user.birthday_date = userCoreData!.birthday_date
                                user.cell = userCoreData!.cell
                                user.cpf = userCoreData!.cpf
                                user.email = userCoreData!.email
                                user.gender = userCoreData!.gender
                                user.name = userCoreData!.name
                                
                                if let responsePicture : [String : String] = response.value as? [String : String] {
                                    user.picture = responsePicture["picture"]
                                }
                                
                                user.username = userCoreData!.username
                                user.password = userCoreData!.password
                                user.isSocial = userCoreData!.isSocial
                                user.id = userCoreData!.id
                            }
                        })
                        WishareCoreData.deleteUsers({ (error : Error?) in
                            if error == nil {
                                
                                WishareCoreData.saveUser(user, { (error : Error?) in
                                    if error == nil {
                                        completion(nil)
                                    }
                                })
                            }
                        })
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    
    class func getOrders(typeOrder : String, _ completion : @escaping (_ orders : [[String : String]], _ items : [[[String : String]]],_ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("orders/getOrders.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var responseOrders : [[String : String]] = []
                        var responseItems : [[[String : String]]] = [[]]
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let orders : [[String : AnyObject]] = resultAPI["orders"] as? [[String : AnyObject]] {
                                
                                for (i, o) in orders.enumerated() {
                                
                                    if let order : [String : AnyObject] = o["Order"] as? [String : AnyObject] {
                                        
                                        if let orderedItem : [[String : AnyObject]] = o["OrderedItem"] as? [[String : AnyObject]] {
                                            
                                            var totalQuantity : Int = 0
                                            
                                            for item in orderedItem {
                                                totalQuantity += ((item["qtd"] as? NSString)?.integerValue)!
                                            }
                                            
                                            if let status : [String : AnyObject] = order["status"] as? [String : AnyObject] {
                                                if let card : [String : AnyObject] = order["card"] as? [String : AnyObject] {
                                                    if let usersCard : [String : AnyObject] = card["UsersCard"] as? [String : AnyObject] {
                                                        if let address : [String : AnyObject] = order["address"] as? [String : AnyObject] {
                                                            if let usersAddress : [String : AnyObject] = address["Address"] as? [String : AnyObject] {
                                                                if let store : [String : AnyObject] = order["store"] as? [String : AnyObject] {
                                                                    
                                                                    
                                                                    let myOrder : [String : String] = [
                                                                        "id" : order["id"] as! String,
                                                                        "tot" : order["tot"] as! String,
                                                                        "purshase_date" : order["purshase_date"] as! String,
                                                                        "items" : "\(totalQuantity)",
                                                                        "status_id" : status["id_status"] as! String,
                                                                        "installments" : order["installments"] as! String,
                                                                        "value_freight" : order["value_freight"] as! String,
                                                                        "name_card" : usersCard["name"] as! String,
                                                                        "number_card" : usersCard["number"] as! String,
                                                                        "flag" : usersCard["flag"] as! String,
                                                                        "name_address" : usersAddress["name"] as! String,
                                                                        "address" : usersAddress["address"] as! String,
                                                                        "number" : usersAddress["number"] as! String,
                                                                        "neighborhood" : usersAddress["neighborhood"] as! String,
                                                                        "city" : usersAddress["city"] as! String,
                                                                        "state" : usersAddress["state"] as! String,
                                                                        "post_code" : usersAddress["post_code"] as! String,
                                                                        "store_name" : store["name"] as! String,
                                                                        "store_picture" : store["picture"] as! String
                                                                    ]
                                                                    
                                                                    responseOrders.append(myOrder)
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            
                                                if let items : [[String : AnyObject]] = order["itens"] as? [[String : AnyObject]] {
                                                    
                                                    for item in items {
                                                        
                                                        let myItem : [String : String] = [
                                                            "item" : item["item"] as! String,
                                                            "size" : item["size"] as! String,
                                                            "color" : item["color"] as! String,
                                                            "value" : item["value"] as! String,
                                                            "qtd" : item["qtd"] as! String,
                                                            "url" : item["url"] as! String,
                                                            "product_id" : item["product_id"] as! String
                                                        ]
                                                        
                                                        responseItems.append([])
                                                        responseItems[i].append(myItem)
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if typeOrder == "PA" {
                                    
                                    var newResponseOrder : [[String : String]] = [[String : String]]()
                                    var newResponseItems : [[[String : String]]] = [[[String : String]]]()
                                    
                                    for (key, order) in responseOrders.enumerated() {
                                        
                                        if (order["status_id"]! as NSString).integerValue == 1 || (order["status_id"]! as NSString).integerValue == 2 || (order["status_id"]! as NSString).integerValue == 17 || (order["status_id"]! as NSString).integerValue == 18 {
                                            newResponseOrder.append(order)
                                            newResponseItems.append(responseItems[key])
                                        }
                                        
                                    }
                                    
                                    responseOrders = newResponseOrder
                                    responseItems = newResponseItems
                                    
                                } else if typeOrder == "PE" {
                                    
                                    var newResponseOrder : [[String : String]] = [[String : String]]()
                                    var newResponseItems : [[[String : String]]] = [[[String : String]]]()
                                    
                                    for (key, order) in responseOrders.enumerated() {
                                        
                                        if (order["status_id"]! as NSString).integerValue == 25 {
                                            newResponseOrder.append(order)
                                            newResponseItems.append(responseItems[key])
                                        }
                                        
                                    }
                                    
                                    responseOrders = newResponseOrder
                                    responseItems = newResponseItems
                                    
                                } else if typeOrder == "PC" {
                                    
                                    var newResponseOrder : [[String : String]] = [[String : String]]()
                                    var newResponseItems : [[[String : String]]] = [[[String : String]]]()
                                    
                                    for (key, order) in responseOrders.enumerated() {
                                        
                                        if (order["status_id"]! as NSString).integerValue == 5 || (order["status_id"]! as NSString).integerValue == 26  {
                                            newResponseOrder.append(order)
                                            newResponseItems.append(responseItems[key])
                                        }
                                    }
                                    responseOrders = newResponseOrder
                                    responseItems = newResponseItems
                                    
                                }
                                completion(responseOrders, responseItems, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], [[]], response.error)
                    }
                }
            }
        }
    }
    
    class func blockedUsers ( _ completion : @escaping (_ user : [User], _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("UsersBlockeds/blockedUsers.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        var responseUsers : [User] = []
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let users : [[String : AnyObject]] = resultAPI["users"] as? [[String : AnyObject]] {
                                
                                for user in users {
                                    
                                    let user : User = User(byUser: user)
                                    responseUsers.append(user)
                                }
                                
                                completion(responseUsers, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func unblockedUser (idUser : String , _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("UsersBlockeds/blockUser/\(idUser)/false.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func getPostsUser (idUser : String , timestamp : String? = nil, _ completion : @escaping ( _ posts : [Post], _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/getPosts.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                var parameters : Parameters!
                
                if timestamp != nil {
                    parameters = ["id" : [idUser], "index" : Int(timestamp!.convertToDate().timeIntervalSince1970)]
                } else {
                    parameters = ["id" : [idUser]]
                }
                
                print(parameters)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.result.value as? [String : AnyObject] {
                            if let postsResponse : [[String : AnyObject]] = resultAPI["Posts"] as? [[String : AnyObject]] {
                             
                                var postResult : [Post] = []
                                
                                for p in postsResponse {
                                    postResult.append(Post(post: p))
                                }
                                
                                // ============================================
                                // ======= ADICIONAR PRODUCTS DO POSTS ========
                                // ================== TODO ====================
                                
                                for post in postResult {
                                    
                                    if post.products.count > 0 {
                                        
                                        let url = urlBase.appendingPathComponent("products/get/")
                                        
                                        var idString : [String] = []
                                        
                                        for id in post.products {
                                            idString.append(id["id"]!)
                                        }
                                        
                                        let stringId = idString.joined(separator: ",")
                                        let urlStringId = "\(url.absoluteString)\(stringId).json"
                                        let urlString : String = "\(urlStringId)?social=\(user!.isSocial)"
                                        
                                        
                                        let response = Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON()
                                        
                                        if response.result.isSuccess {
                                            
                                            if let responseAPI : [String : [[String : AnyObject]]] = response.value as? [String : [[String : AnyObject]]] {
                                                
                                                for product in responseAPI["Products"]! {
                                                    
                                                    let prod = Product(product)
                                                    post.products_info.append(prod)
                                                }
                                            }
                                        }
                                        
                                        if response.result.isFailure {
                                            completion([], response.error)
                                        }
                                    }
                                }
                                // =====================================
                                
                                completion(postResult, nil)
                                
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func getPurchaseBuy (idUser : String? = nil, userBy : User,_ completion : @escaping ( _ posts : [Post], _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
            
                let url = urlBase.appendingPathComponent("users/getSharedOrders/")
                let urlString : String = "\(url.absoluteString)\(idUser!).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.result.value as? [String : AnyObject] {
                            
                            var posts : [Post] = []
                            var actualOrder : Int = 0
                            var totalOrders : Int = 0
                            
                            if let orders_tmp  : [[String : AnyObject]] = resultAPI["orders_tmp"] as? [[String : AnyObject]] {
                                
                                totalOrders = orders_tmp.count
                                
                                for order in orders_tmp {
                                 
                                    let newOrder = Post()
                                 
                                    if let products : [[String : AnyObject]] = order["products"] as? [[String : AnyObject]] {
                                        
                                        var ids : [String] = []
                                        
                                        for p in products {
                                            if let id = p["id"] as? String {
                                                ids.append(id)
                                            }
                                        }
                                        
                                        
                                        WishareAPI.getProductsBy(ids: ids, { (products : [Product], error : Error?) in
                                            if error == nil {
                                                if products.count > 0 {
                                                    WishareAPI.storeDetail(products.first!.storeId!, { (store : Store?, error : Error?) in
                                                        if error == nil {
                                                            
                                                            newOrder.products_info.append(contentsOf: products)
                                                            newOrder.store = store
                                                            newOrder.user = userBy
                                                            
                                                            posts.append(newOrder)
                                                            
                                                            actualOrder += 1
                                                            
                                                            if totalOrders == actualOrder {
                                                                completion(posts, nil)
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                            }
                            
                        } else {
                            completion([], nil)
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    
    class func blockedUser (idUser : String , _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("UsersBlockeds/blockUser/\(idUser).json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func privateUser ( _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/privateUser.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func unprivateUser ( _ completion : @escaping ( _ error : Error? ) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/privateUser/false.json")
                let urlString : String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    
    // TODO: - Refazer logica de cadastro
    class func userCreateOrFails( _ user : User , _ social : Bool, _ completion : @escaping ( _ isCreated : Bool, _ error : Error?) -> Void) {
        
        var parameters : Parameters = [:]
        user.isSocial = social
        
        if let name = user.name {
            parameters["name"] = name
        }
        
        if let last_name = user.last_name {
            parameters["last_name"] = last_name
        }
        
        if let gender = user.gender {
            parameters["gender"] = gender
        }
        
        if let birthday_date = user.birthday_date {
            parameters["birthday_date"] = birthday_date
        }
        
        if let state = user.state {
            parameters["state"] = state
        }
        
        if let username = user.username {
            parameters["username"] = username
        }
        
        if let email = user.email {
            parameters["email"] = email
        }
        
        if let password = user.password {
            parameters["pwd"] = password
        }
        
        
        if let facebook_id = user.id_face {
            
            parameters["id_face"] = facebook_id
            parameters["id_face_password"] = facebook_id
            parameters["picture_face"] = user.picture
            parameters["pwd"] = facebook_id
            
        }
        
        
        let encoding : ParameterEncoding = JSONEncoding.default
        let headers : HTTPHeaders = ["Accept": "application/json"]
        
        let url = urlBase.appendingPathComponent("users/add.json")
        
        let urlString = social ? "\(url.absoluteString)?social=true" : "\(url.absoluteString)?social=false"
        
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
            
            if response.result.isSuccess {
                
                if let message : [String : AnyObject] = response.value as? [String : AnyObject] {
                    
                    if let success = message["success"] {
                        
                        let isValidate = (success as! CFBoolean) as! Bool
                        
                        if !isValidate {
                            completion(false, nil)
                        }
                        
                        WishareCoreData.saveUser(user, { (error : Error?) in
                            
                            if error == nil {
                                completion(true, nil)
                            } else {
                                completion(false, error)
                            }
                            
                        })
                    }
                }
            }
            
            if response.result.isFailure {
                completion(false, response.error)
            }
        }
    }
    
    // TODO: - Trocar pela funcao checkedFieldsExistsUser
    class func checkUserNameExists( _ username : String ) -> Bool {
        
        let headers : HTTPHeaders = ["Accept": "application/json"]
        
        let url = urlBase.appendingPathComponent("users/validUsernameLogoff/")
        let urlString = "\(url.absoluteString)\(username)"
        
        let response = Alamofire.request(urlString, encoding: JSONEncoding.default, headers: headers).responseJSON()
        
        if response.result.isSuccess {
            
            print("SUCESSO")
            if let result : [String : AnyObject] = response.value as? [String : AnyObject] {
                
                
                if let boolResult = result["message"] {
                    
                    let isTrueValue = (boolResult as! CFBoolean) as! Bool
                    return isTrueValue
                }
            }
        }
        
        if response.result.isFailure {
            print("FALHOU")
            return false
        }
        return false
        
    }
    
    class func checkEmailExists( _ email : String ) -> Bool {
        
        let parameters : Parameters = ["User.email" : email]
        let encoding : ParameterEncoding = JSONEncoding.default
        let headers : HTTPHeaders = ["Accept": "application/json"]
        
        let url = urlBase.appendingPathComponent("users/issetField.json")
        
        let response = Alamofire.request(url, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON()
        
        if response.result.isSuccess {
            
            if let isExist : [String : Bool] = response.value as? [String : Bool] {
                
                if isExist["return"]! {
                    return false
                } else {
                    return true
                }
            }
        }
        
        if response.result.isFailure {
            print("FALHOU")
            return false
        }
        return false
        
    }
    // ===================================================================================
    // ================================= WishareAPI 2.0 ==================================
    // ===================================================================================
    
    
    class func storeByLimit(quantity : String, name : String? = nil, _ completion : @escaping ( _ stores : [Store], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getLimit/")
                
                let urlString = "\(url.absoluteString)\(quantity).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                let parameters : Parameters = [
                    "name" : (name != nil) ? name! : "",
                ]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let stores : [[String : AnyObject]] = resultAPI["Stores"] as? [[String : AnyObject]] {
                                
                                var storesInstance : [Store] = []
                                
                                for iStore in stores {
                                    let storeInstance : Store = Store(byCategory: iStore)
                                    storesInstance.append(storeInstance)
                                }
                                
                                completion(storesInstance, nil)
                            }
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                    
                }
                
            }
            
        }
        
    }
    
    class func userByLimit(quantity : String, name : String? = nil, _ completion : @escaping ( _ stores : [User], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/getLimit/")
                
                let urlString = "\(url.absoluteString)\(quantity)\(((name != nil) ? "/index:\(name!)" : "")).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let users : [[String : AnyObject]] = resultAPI["Users"] as? [[String : AnyObject]] {
                                
                                var usersInstance : [User] = []
                                
                                for iUser in users {
                                    let userInstance : User = User(byUser: iUser)
                                    usersInstance.append(userInstance)
                                }
                                
                                completion(usersInstance, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func userNotification(_ completion : @escaping ( _ notifications : [WSNotification], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("usersNotifications/getNotification.json")
                
                let urlString = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let notifications : [[String : AnyObject]] = resultAPI["Notification"] as? [[String : AnyObject]] {
                             
                                var notificatonResponse : [WSNotification] = []
                                
                                for n in notifications {
                                    if n["actionID"] as! String == "6" {
                                        
                                        let not = WSNotification(order: n)
                                        notificatonResponse.append(not)
                                        
                                    } else {
                                        
                                        let not = WSNotification(n)
                                        notificatonResponse.append(not)
                                        
                                    }
                                }
                                completion(notificatonResponse, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }

    class func userNotificationBy(page : String, lastId : String, _ completion : @escaping ( _ notifications : [WSNotification], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("usersNotifications/getNotification/")
                
                let urlString = "\(url.absoluteString)index:\(lastId)/page:\(page).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                print(urlString)
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let notifications : [[String : AnyObject]] = resultAPI["Notification"] as? [[String : AnyObject]] {
                                
                                var notificatonResponse : [WSNotification] = []
                                
                                for n in notifications {
                                    if n["actionID"] as! String == "6" {
                                        
                                        let not = WSNotification(order: n)
                                        notificatonResponse.append(not)
                                        
                                    } else {
                                        
                                        let not = WSNotification(n)
                                        notificatonResponse.append(not)
                                        
                                    }
                                }
                                
                                completion(notificatonResponse, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func userNotificationAction(_ completion : @escaping ( _ notifications : [WSNotification], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("usersNotifications/getNotification/action.json")
                
                let urlString = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let notifications : [[String : AnyObject]] = resultAPI["Notification"] as? [[String : AnyObject]] {
                                
                                var notificatonResponse : [WSNotification] = []
                                
                                for n in notifications {
                                    if n["actionID"] as! String == "6" {
                                        
                                        let not = WSNotification(order: n)
                                        notificatonResponse.append(not)
                                        
                                    } else {
                                        
                                        let not = WSNotification(action:n)
                                        notificatonResponse.append(not)
                                    }
                                }
                                completion(notificatonResponse, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func userNotificationActionBy(page : String, lastId : String, _ completion : @escaping ( _ notifications : [WSNotification], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("usersNotifications/getNotification/action/")
                
                let urlString = "\(url.absoluteString)index:\(lastId)/page:\(page).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let notifications : [[String : AnyObject]] = resultAPI["Notification"] as? [[String : AnyObject]] {
                                
                                var notificatonResponse : [WSNotification] = []
                                
                                for n in notifications {
                                    if n["actionID"] as! String == "6" {
                                        
                                        let not = WSNotification(order: n)
                                        notificatonResponse.append(not)
                                        
                                    } else {
                                        
                                        let not = WSNotification(action:n)
                                        notificatonResponse.append(not)
                                    }
                                }
                                
                                completion(notificatonResponse, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func getPostOnly(idPost : String, _ completion : @escaping ( _ notifications : [Post], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/getPost/")
                
                let urlString = "\(url.absoluteString)\(idPost).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let posts : [[String : AnyObject]] = resultAPI["posts"] as? [[String : AnyObject]] {
                                
                                var postsClass : [Post] = []
                                
                                for post in posts {
                                    postsClass.append(Post(post))
                                }
                                
                                for postProducts in postsClass {
                                    
                                    // - Verificando quantidade de produtos
                                    
                                    if postProducts.products.count > 0 {
                                        
                                        
                                        // - Configurando rota para products
                                        
                                        let url = urlBase.appendingPathComponent("products/get/")
                                        var idString : [String] = []
                                        for id in postProducts.products {
                                            idString.append(id["id"]!)
                                        }
                                        
                                        let stringId = idString.joined(separator: ",")
                                        let urlStringId = "\(url.absoluteString)\(stringId).json"
                                        let urlString : String = "\(urlStringId)?social=\(user!.isSocial)"
                                        
                                        
                                        
                                        Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                                            
                                            if response.result.isSuccess {
                                                
                                                
                                                if let responseAPI : [String : [[String : AnyObject]]] = response.value as? [String : [[String : AnyObject]]] {
                                                    
                                                    for product in responseAPI["Products"]! {
                                                        
                                                        let prod = Product(product)
                                                        postProducts.products_info.append(prod)
                                                    }
                                                }
                                            }
                                            
                                            if response.result.isFailure {
                                                completion([], response.error)
                                            }
                                        })
                                    }
                                }
                                completion(postsClass, nil)
                                
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func sharePurchase(text : String, idStore : String, products : [[String : AnyObject]], orderId : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("posts/sharePurchase/\(orderId).json")
                
                let urlString = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                var productsPurchase : [String] = []
                
                for p in products {
                    productsPurchase.append(p["id_item"] as! String)
                }
                
                let parameters : Parameters = [
                    "text" : text,
                    "store_id" : idStore,
                    "products" : productsPurchase
                ]

                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func acceptInvite(idNotification : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/replyFollow/")
                
                let urlString = "\(url.absoluteString)\(idNotification).json?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    
    class func quantityNotification( _ completion : @escaping ( _ number : String, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("usersNotifications/getQtd.json")
                
                let urlString = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let n : Int = resultAPI["count"] as? Int {
                                completion("\(n)", nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion("", response.error)
                    }
                }
            }
        }
    }
    
    
    class func getWishes( idUser : String, _ completion : @escaping ( _ products : [Product], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/getWishes.json")
                
                let urlString = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                let parameters : Parameters = ["id" : [idUser]]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            
                            var postResult : [Product] = [Product]()
                            
                            if let wishes : [[String : AnyObject]] = resultAPI["Wishes"] as? [[String : AnyObject]] {
                                for wish in wishes {
                                    if let data : [String : AnyObject] = wish["data"] as? [String : AnyObject] {
                                        if let product : [String : AnyObject] = data["Product"] as? [String : AnyObject] {
                                    
                                            let p = Product(product)
                                            
                                            if let feedProduct : [String : AnyObject] = data["FeedProduct"] as? [String : AnyObject] {
                                                
                                                if let group : String = feedProduct["group"] as? String {
                                                    p.pId = group
                                                }
                                                
                                                if let pictures : [[String : String]] = feedProduct["pictures"] as? [[String : String]] {
                                                    for prod in pictures {
                                                        if prod["index"] == "0" {
                                                            p.picture = prod["img"]
                                                        }
                                                    }
                                                }
                                            }
                                            let store = Store(byCategory: data)
                                            p.store = store
                                            postResult.append(p)
                                        }
                                    }
                                }
                                
                                completion(postResult, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    
    class func declineInvite(idNotification : String, _ completion : @escaping (_ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/replyFollow/")
                
                let urlString = "\(url.absoluteString)\(idNotification)/false.json?social=\(user!.isSocial)"
                
                print(urlString)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    class func enableNotificationDevice( token : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/setFcmToken/")
                
                let urlString = "\(url.absoluteString)\(user!.id!)/I.json?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "fcm_token" : token,
                    ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }
    
    
    class func disableNotificationDevice( token : String, _ completion : @escaping ( _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("users/deleteFcmToken/")
                
                let urlString = "\(url.absoluteString)\(user!.id!).json?social=\(user!.isSocial)"
                
                let parameters : Parameters = [
                    "fcm_token" : token,
                    ]
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        completion(nil)
                    }
                    
                    if response.result.isFailure {
                        completion(response.error)
                    }
                }
            }
        }
    }

    
    class func getAllBrands( _ completion : @escaping ( _ brands : [String], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/getAllBrands.json")
                
                let urlString = "\(url.absoluteString)?social=\(user!.isSocial)"
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let brands : [String] = resultAPI["brands"] as? [String] {
                                completion(brands, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    
    class func getNotaFiscal(id: String, _ completion: @escaping ( _ urls: [String]?, _ error: Error?) -> Void) {
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            if error == nil && user != nil {

                let requestURL : URL = urlBase.appendingPathComponent("orders/getPDFOrder/")
                let requestURLString : String = "\(requestURL)\(id).json?social=\(user!.isSocial)"
                
                
                print("URL NF: \(requestURL.absoluteString)")
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(requestURLString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let returnResult : [String : AnyObject] = resultAPI["return"] as? [String : AnyObject] {
                                
                                var urls : [String]?
                                
                                if let path : String = returnResult["path"] as? String {
                                    
                                    if let files : [String] = returnResult["file"] as? [String] {
                                        for file in files {
                                            var nfURLString : String = urlBase.absoluteString
                                            nfURLString.append("\(path)\(file.addingPercentEncoding(withAllowedCharacters: .alphanumerics)!)")
                                            //retornar strings
                                            
                                            if urls?.append(nfURLString) == nil {
                                                urls = [nfURLString]
                                            }
                                        }
                                        completion(urls, nil)
                                    }
                                    else {
                                        completion(nil, nil)
                                    }
                                }
                                else {
                                    completion(nil, nil)
                                }
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion(nil, response.error)
                    }
                    
                }
            }
        }
    }
    
    
    class func getProductsBy( ids : [String], _ completion : @escaping ( _ products : [Product], _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let url = urlBase.appendingPathComponent("products/get/")
                let urlString = "\(url.absoluteString)\(ids.joined(separator: ",")).json?social=\(user!.isSocial)"
                
                print(urlString)
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let productsResponse : [[String : AnyObject]] = resultAPI["Products"] as? [[String : AnyObject]] {
                             
                                var products : [Product] = []
                                
                                for p in productsResponse {
                                    let prod = Product(p)
                                    products.append(prod)
                                }
                                
                                completion(products, nil)
                            }
                        }
                    }
                    
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                }
            }
        }
    }
    
    class func getStoresNotFollowing(_ idStores: [String]?, _ storeCount: Int?, _ completion : @escaping ( _ stores: [Store], _ products: [[Product]], _ moreStores: Bool, _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
                
                let count = storeCount != nil ? "\(storeCount!)" : "10"
                let url = urlBase.appendingPathComponent("stores/getStoresNotFollowing/\(count)")
                
                let encoder = JSONEncoder()
                encoder.outputFormatting = .prettyPrinted
                
                var encodedIds: String?
                
                if let followedIds = idStores {
                    do {
                        let data = try encoder.encode(followedIds)
                        
                        encodedIds = String(data: data, encoding: .utf8)
                    }
                    catch {
                        print("failed to encode")
                    }
                }

                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
//                var parameters : Parameters = [:]
//
//                parameters["ids"] = ["9", "12"]
                
                var ids: String = ""
                
                if let idss = idStores {
                    for id in idss {
                        ids += id
                        if idss.index(of: id) != (idss.count - 1) {
                            ids += "-"
                        }
                    }
                }
                
                let urlString = "\(url.absoluteString)/\(ids).json?social=\(user!.isSocial)"

                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let storeResponse : [[String : AnyObject]] = resultAPI["stores"] as? [[String : AnyObject]] {
                                
                                var stores: [Store] = [Store]()
                                var products: [[Product]] = [[Product]]()
                                var morePosts: Bool = false
                                
                                for s in storeResponse {

                                    var followers: String?
                                    
                                    if let f = s["0"] as? [String:AnyObject] {
                                        followers = f["followers"] as? String
                                    }
                                    
                                    var categories: String?
                                    
                                    if let cat = s["Categories"] as? [String:String] {
                                        for textCat in cat.values {
                                            if categories?.append(" - \(textCat)") == nil {
                                                categories = textCat
                                            }
                                        }
                                    }
                                    
                                    if let sIsStore = s["Store"] as? [String:String] {
                                        
                                        let store = Store(id: sIsStore["id"]!, name: sIsStore["name"]!, picture: sIsStore["picture"]!, advance_commission: "")
                                        store.followers = followers
                                        store.following = false
                                        store.productTypes = categories
                                        stores.append(store)
                                    }
                                    
                                    var prods: [Product] = []
                                    
                                    if let productStore = s["Products"] as? [[String:AnyObject]] {
                                        for prod in productStore {
                                            var id: String?
                                            var url: String = urlBase.absoluteString
                                            var variationId: String?
                                            
                                            if let prodForId = prod["ProductsSetting"] as? [String:AnyObject] {
                                                id = prodForId["product_id"] as? String
                                                variationId = prodForId["id"] as? String
                                            }
                                            if let prodForPicture = prod["Pictures"] {
                                                url.append("files/products/\(id ?? "")/thumb/\(prodForPicture["url"] as? String ?? "")")
                                            }
                                            let product = Product.init(id: id ?? "", picture: url, variationId: variationId ?? "")
                                            
                                            prods.append(product)
                                        }
                                    }
                                    
                                    products.append(prods)
                                }
                                
                                if let qtdNext = resultAPI["qtdNext"] as? Int {
                                    if qtdNext > 0 {
                                        morePosts = true
                                    }
                                }
                                
                                completion(stores, products, morePosts, nil)
                            }
                        }
                        
                        
                    }
                    
                    if response.result.isFailure {
                        completion([], [], false, response.error)
                    }
                }
            }
        }
    }
    
    class func checkProduct(idProduct: String, _ completion : @escaping ( _ continue : Bool, _ code: String?, _ option: String?,  _ error : Error?) -> Void) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error == nil && user != nil {
    
                let url = urlBase.appendingPathComponent("products/issetProduct/")
                let urlString : String = "\(url.absoluteString)\(idProduct).json?social=\(user!.isSocial)"
                
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let resultReturn : [String : AnyObject] = resultAPI["return"] as? [String : AnyObject] {
                                if let resultExists : Bool = resultReturn["exists"] as? Bool {
                                    var code : String?
                                    var option : String?
                                    if let codeExists : String = resultReturn["code"] as? String {
                                        code = codeExists
                                    }
                                    if let optionExists : String = resultReturn["option"] as? String {
                                        option = optionExists
                                    }
                                    
                                    completion(resultExists, code, option, nil)
                                }
                            }
                        }
                        completion(false, nil, nil, error)
                    }
                    if response.result.isFailure {
                        completion(false, nil, nil, error)
                    }
                })
            }
        }
    }
    
    struct ProductForCoupom {
        var id: String
        var quantity: String
        
        
        func convertToDictionary() -> [String : String] {
            let dic: [String: String] = ["id":self.id, "quantity":self.quantity]
            return dic
        }
        
    }
    
    class func applyCoupon(coupon: String?, addressId: String?, idStore: String?, freightValue: String?, service: String?, products: [Product], _ completion : @escaping ( _ valid : Bool, _ products: [ProductUpdate], _ totalPrice: String, _ error : Error?) -> Void) {
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            if error == nil && user != nil {
                let url = urlBase.appendingPathComponent("coupons/validate_coupon.json")
                let urlString: String = "\(url.absoluteString)?social=\(user!.isSocial)"
                
//                var productForCoupon: [ProductForCoupom] = Array<ProductForCoupom>()
                
                var productsRequest : [[String : String]] = []
                
                for product in products {
                    if let qtd = product.select_quatity, let id = product.id {
                        productsRequest.append(["id" : id, "quantity" : qtd])
                    }
                }
                
//                for prod in products {
//                    if let qtd = prod.quantity, let id = prod.id {
//                        productForCoupon.append(ProductForCoupom(id: id, quantity: qtd))
//                    }
//                }
//
//                let dicArray = productForCoupon.map { $0.convertToDictionary() }
                
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                var parameters: Parameters = [:]

                if let coupon = coupon {
                    parameters["coupon"] = coupon
                }
                if let idStore = idStore {
                    parameters["idstore"] = idStore
                }
                if let addressId = addressId {
                    parameters["addressid"] = addressId
                }
                if let freightValue = freightValue {
                    parameters["freightValue"] = freightValue
                }
                if let service = service {
                    parameters["service"] = service
                }
                
                parameters["products"] = productsRequest
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                
                Alamofire.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    switch response.result {
                    case .success(let value):
                        if let resultAPI = value as? [String:AnyObject] {
                            if let discount = resultAPI["discount"] as? [String:AnyObject] {
                                var newProducts: [ProductUpdate] = [ProductUpdate]()
                                var total: String?
                                
                                if let products = discount["products"] as? [[String: String]] {
                                    for product in products {
                                        if let id = product["id"], let pId = product["pId"], let newValue = product["new_value"], let qtd = product["qtd"] {
                                            let prod = ProductUpdate(id: id, pId: pId, price: newValue, quantity: qtd)
                                            newProducts.append(prod)
                                        }
                                        
                                    }
                                }
                                if let newTotal = discount["total"] as? String {
                                    total = newTotal
                                }
                                completion(true, newProducts, total ?? "", nil)
                                break
                            } else {
                                completion(false, [], "", nil)
                                break
                            }
                        }
                        completion(false, [], "", nil)
                        break
                        
                    case .failure(let error):
                        print(error)
                        completion(false, [], "", nil)
                        break
                    }
                })
            }
        }
    }
    
    class func getPostsStore(_ storeId: String, _ lastPost : String?, _ completion : @escaping ( _ result : [Post], _ error : Error?) -> Void ) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                let url = urlBase.appendingPathComponent("stores/getPosts/\(storeId)")
                var urlString : String!
                
                if lastPost != nil {
                    
                    urlString = "\(url.absoluteString)/\(Int(lastPost!.convertToDate().timeIntervalSince1970)).json?social=\(user!.isSocial)"
                    print("NOVOS POSTS: \(urlString)")
                    
                } else {
                    urlString = "\(url.absoluteString).json?social=\(user!.isSocial)"
                }
                let encoding : ParameterEncoding = JSONEncoding.default
                var headers : HTTPHeaders = ["Accept" : "application/json"]
                
                if let authorizationHeader = Request.authorizationHeader(user: user!.username!, password: user!.password!) {
                    headers[authorizationHeader.key] = authorizationHeader.value
                }
                Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                    
                    if response.result.isSuccess {
                        
                        print("====== POSTS ========")
                        
                        if let resultAPI : [String : AnyObject] = response.value as? [String : AnyObject] {
                            if let posts : [[String : AnyObject]] = resultAPI["Posts"] as? [[String : AnyObject]] {
                                
                                var postsClass : [Post] = []
                                
                                for post in posts {
                                    postsClass.append(Post(post))
                                }
                                
                                for postProducts in postsClass {
                                    
                                    // - Verificando quantidade de produtos
                                    if postProducts.products.count > 0 {
                                        // - Configurando rota para products
                                        
                                        let url = urlBase.appendingPathComponent("products/get/")
                                        var idString : [String] = []
                                        for id in postProducts.products {
                                            idString.append(id["id"]!)
                                        }
                                        
                                        let stringId = idString.joined(separator: ",")
                                        let urlStringId = "\(url.absoluteString)\(stringId).json"
                                        let urlString : String = "\(urlStringId)?social=\(user!.isSocial)"
                                        
                                        Alamofire.request(urlString, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                                            
                                            if response.result.isSuccess {
                                                if let responseAPI : [String : [[String : AnyObject]]] = response.value as? [String : [[String : AnyObject]]] {
                                                    
                                                    for product in responseAPI["Products"]! {
                                                        
                                                        let prod = Product(product)
                                                        postProducts.products_info.append(prod)
                                                    }
                                                }
                                            }
                                            if response.result.isFailure {
                                                completion([], response.error)
                                            }
                                        })
                                    }
                                }
                                completion(postsClass, nil)
                            }
                        }
                    }
                    if response.result.isFailure {
                        completion([], response.error)
                    }
                })
            }
        }
    }
}
