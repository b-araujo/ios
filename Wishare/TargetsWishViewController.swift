//
//  TargetsWishViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin

class TargetsWishViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var wishPoints : WishPoints!
    var objectivesTarget : [Objective] = []
    
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "TargetsWishCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "targetsWishCell")
        
        for a in self.wishPoints.objectives {
            
            if a.isClosed! == false {
                self.objectivesTarget.append(a)
            }
            
        }
        
        self.collectionView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func convertToGrayScale(image: CGImage) -> CGImage {
        let height = image.height
        let width = image.width
        let colorSpace = CGColorSpaceCreateDeviceGray();
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        let context = CGContext.init(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        context.draw(image, in: rect)
        return context.makeImage()!
    }
    
}

extension TargetsWishViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("openPopupWishPoints"), object: self.objectivesTarget[indexPath.row])
    }
    
}

extension TargetsWishViewController : UICollectionViewDataSource {


    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.objectivesTarget.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "targetsWishCell", for: indexPath) as! TargetsWishCollectionViewCell
        
        cell.nameTargetLabel.text = self.objectivesTarget[indexPath.row].title
        cell.pointsTargetLabel.text = "+ \(self.objectivesTarget[indexPath.row].points!) wpts"
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/wish_objective/thumb/")!
        let url = urlBase.appendingPathComponent("files/wish_objective/thumb/\(self.objectivesTarget[indexPath.row].image!)")
        
        
        self.manager.loadImage(with: url, into: cell.targetImageView) { ( result, _ ) in
            if self.wishPoints.objectives[indexPath.row].isClosed! {
                cell.targetImageView.layer.cornerRadius = cell.targetImageView.layer.bounds.width / 2
                cell.targetImageView.image = result.value
            } else {
                cell.targetImageView.layer.cornerRadius = cell.targetImageView.layer.bounds.width / 2
                cell.targetImageView.image = UIImage(cgImage: self.convertToGrayScale(image: result.value!.cgImage!))
            }
            
        }
        
        return cell
        
    }
    
}

extension TargetsWishViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.bounds.width / 3.0) - 1.25, height: ((self.collectionView.bounds.width / 3.0) + 60.0) - 2.5)
    }
    
}
