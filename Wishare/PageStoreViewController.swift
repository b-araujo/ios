//
//  PageStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/5/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import SwiftyAttributes
import ENMBadgedBarButtonItem
import Kingfisher

class PageStoreViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchViewBottomConstraint : NSLayoutConstraint!
    
    
    var categories : [Category] = []
    var store : Store = Store(id: "1", name: "", picture: "", advance_commission: "")
//    var loader : Loader!
//    var manager : Manager!
    var lastContentOffSet : CGPoint = CGPoint.zero
    var idStore : String!
    
    var scrollDirectionDetermined : String = ""
    
    var searchView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var searchBar : UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .prominent
        searchBar.placeholder = "Pesquisar"
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.barTintColor = UIColor(red: 234/255, green: 232/255, blue: 232/255, alpha: 1)
        searchBar.layer.borderWidth = 1.0
        searchBar.layer.borderColor = UIColor(red: 234/255, green: 232/255, blue: 232/255, alpha: 1).cgColor
        return searchBar
    }()
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var barButton : BadgedBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.tag = 10
        self.collectionView.register(UINib(nibName: "PageStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "pageStoreCell")
        self.collectionView.register(UINib(nibName: "CategoriesPageStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoriesPageStoreCell")
        
        self.searchBar.delegate = self
        
//        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
//        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando loja", attributes: attributes)
        
//        WishareAPI.storeFeed(self.idStore) { (store : Store?, categories : [Category]?, error : Error?) in
//
//            if error == nil {
//
//                let removeCategories : [Category] = categories!
//
//                for (_, cat) in removeCategories.enumerated() {
//                    if cat.products.count != 0 || cat.promo.count != 0 {
//                        self.categories.append(cat)
//                    }
//                }
//
//                self.store = store!
//                self.collectionView.reloadData()
//                RappleActivityIndicatorView.stopAnimation()
//
//            }
//
//        }
        
        WishareAPI.storeProfile(idStore: self.idStore) { (store : Store?, categories : [Category]?, error : Error?) in
            
            if error == nil {
                
                WishareAPI.productsByCategory(self.idStore, nil, { (categories, error) in
                    if error == nil {
                        
                        let removeCategories : [Category] = categories
                        
                        for (_, cat) in removeCategories.enumerated() {
                            if cat.products.count != 0 || cat.promo.count != 0 {
                                self.categories.append(cat)
                            }
                        }
                        self.collectionView.reloadData()
                    }
                })
                
                let removeCategories : [Category] = categories!
                
                for (_, cat) in removeCategories.enumerated() {
                    if cat.products.count != 0 || cat.promo.count != 0 {
                        self.categories.append(cat)
                    }
                }
                
                self.store = store!
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
            
        }
        
        self.view.addSubview(self.searchView)
        
        self.searchView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.searchView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.searchView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.searchViewBottomConstraint = self.searchView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        self.searchViewBottomConstraint.isActive = true
        
        self.searchView.addSubview(self.searchBar)
        
        self.searchBar.topAnchor.constraint(equalTo: self.searchView.topAnchor).isActive = true
        self.searchBar.bottomAnchor.constraint(equalTo: self.searchView.bottomAnchor).isActive = true
        self.searchBar.leftAnchor.constraint(equalTo: self.searchView.leftAnchor).isActive = true
        self.searchBar.rightAnchor.constraint(equalTo: self.searchView.rightAnchor).isActive = true
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
        updateButtonIfChanged()
        
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func myCart() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "followStoreSegue" {
            let followPageStoreViewController : FollowPageStoreViewController = segue.destination as! FollowPageStoreViewController
            followPageStoreViewController.store = sender as? Store
        }
        
        if segue.identifier == "postsStoreSegue" {
            let postsPageStoreViewController : PostsPageStoreViewController = segue.destination as! PostsPageStoreViewController
            postsPageStoreViewController.store = sender as? Store
        }
        
        if segue.identifier == "showSubCategoriesStoreSegue" {
            
            let senderObject : [AnyObject] = sender as! [AnyObject]
            
            let showSubCategoriesStoreViewController : ShowSubCategoriesStoreViewController = segue.destination as! ShowSubCategoriesStoreViewController
            showSubCategoriesStoreViewController.category = senderObject[0] as? Category
            showSubCategoriesStoreViewController.store = senderObject[1] as? Store
            
        }
        
        if segue.identifier == "productsCategoriesStoreSegue" {
            
            let senderObject : [AnyObject] = sender as! [AnyObject]
            
            let productsCategoriesStoreViewController : ProductsCategoriesStoreViewController = segue.destination as! ProductsCategoriesStoreViewController
            productsCategoriesStoreViewController.category = senderObject[0] as? Category
            productsCategoriesStoreViewController.store = senderObject[1] as? Store
            
            if senderObject.count == 3 {
                productsCategoriesStoreViewController.promo = senderObject[2] as? Promo
            }
            
        }
        
        if segue.identifier == "recentSearchSegue" {
            
            let recentSearchViewController = segue.destination as! RecentSearchViewController
            recentSearchViewController.category = Category(id: "x", name: self.store.name!)
            recentSearchViewController.store = sender as? Store
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateButtonIfChanged() {
        self.collectionView.reloadData()
    }

}

extension PageStoreViewController : UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
        let translation = scrollView.panGestureRecognizer.translation(in: self.view)
            
        if translation.y > 0 {
            
            if self.scrollDirectionDetermined != "UP" {
                UIView.animate(withDuration: 0.4, animations: {
                    self.searchViewBottomConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
            }

            self.scrollDirectionDetermined = "UP"
                
                
        } else if translation.y < 0 {
                
            if self.scrollDirectionDetermined != "DOWN" {
                UIView.animate(withDuration: 0.4, animations: {
                    self.searchViewBottomConstraint.constant = 44
                    self.view.layoutIfNeeded()
                })
            }
                
            self.scrollDirectionDetermined = "DOWN"
                
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 12 {
            if let indexPathRow = collectionView.accessibilityHint {
                if self.categories[Int(indexPathRow)!].products.count > 0 {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                    productDetailViewController.idProduct = self.categories[Int(indexPathRow)!].products[indexPath.row].id!
                    productDetailViewController.isFirst = true
                    
                    self.navigationController?.pushViewController(productDetailViewController, animated: true)
                    
                }
            }
        }
        
    }
    
}

extension PageStoreViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 11 {
            return self.categories.count
        }
        
        if collectionView.tag == 12 {
            if let indexPathRow = collectionView.accessibilityHint {
                
                if self.categories[Int(indexPathRow)!].products.count > 0 {
                    return self.categories[Int(indexPathRow)!].products.count
                } else {
                    return self.categories[Int(indexPathRow)!].promo.count
                }
                
            }
            
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView.tag == 10 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pageStoreCell", for: indexPath) as! PageStoreCollectionViewCell
            
            cell.collectionView.tag = 11
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.register(UINib(nibName: "CategoriesPageStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoriesPageStoreCell")
            cell.collectionView.accessibilityHint = "\(indexPath.row)"
            cell.collectionView.reloadData()
            
            cell.delegate = self
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/store-logo/thumb/")!
            let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.store.picture!)")
            cell.storeImageView.kf.indicatorType = .activity
            cell.storeImageView.kf.setImage(with: url) { (image: Image?, error: NSError?, _, _) in
                if let image = image {
                    cell.storeImageView.image = image.circleMask
                }
            }
            
//            self.manager.loadImage(with: url, into: cell.storeImageView) { ( result, _ ) in
//                cell.storeImageView.image = result.value?.circleMask
//            }
            
            cell.nameStoreLabel.text = self.store.name
            
            if let _ : String = self.store.followers {
                cell.quantityFollowersLabel.text = self.store.followers!
                cell.quantityPostsLabel.text = self.store.qtdPosts
                
                if self.store.following! {
                    
                    cell.followButton.backgroundColor = UIColor.white
                    cell.followButton.setTitle("Seguindo", for: .normal)
                    cell.followButton.setTitleColor(UIColor.black, for: .normal)
                    cell.followButton.layer.borderWidth = 1.0
                    cell.followButton.layer.borderColor = UIColor.black.cgColor
                
                } else {
                    
                    cell.followButton.backgroundColor = UIColor.black
                    cell.followButton.setTitle("Seguir", for: .normal)
                    cell.followButton.setTitleColor(UIColor.white, for: .normal)
                    cell.followButton.layer.borderWidth = 0.0
                    cell.followButton.layer.borderColor = UIColor.clear.cgColor
                    
                }
                
            }

            
            return cell
            
        }
        
        
        if collectionView.tag == 12 {
            
            
            if let indexPathRow = collectionView.accessibilityHint {
                
                if self.categories[Int(indexPathRow)!].promo.count > 0 {
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "promoCell", for: indexPath) as! PromoStoreCollectionViewCell
                    
                    cell.delegate = self
                    cell.row = indexPathRow
                    cell.accessibilityHint = "\(indexPath.row)"
                    
                    //let urlBase = URL(string: "http://www.wishare.com.br/")!
                    
                    let url = urlBaseWishare.appendingPathComponent(self.categories[Int(indexPathRow)!].promo[indexPath.row].imageUrl!)
                    
                    cell.promoImageView.kf.indicatorType = .activity
                    cell.promoImageView.kf.setImage(with: url)
                    
//                    self.manager.loadImage(with: url, into: cell.promoImageView) { ( result, _ ) in
//                        cell.promoImageView.image = result.value
//                    }
                    
                    
                    cell.descriptionPromoLabel.text = self.categories[Int(indexPathRow)!].promo[indexPath.row].name
                    
                    
                    return cell
                    
                } else {
                
                    if self.categories[Int(indexPathRow)!].products[indexPath.row].id != nil {
                        
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyShareCell", for: indexPath) as! BuyShareCollectionViewCell
                        
                        cell.nameProductLabel.text = self.categories[Int(indexPathRow)!].products[indexPath.row].brand
                        cell.descriptionProductLabel.text = self.categories[Int(indexPathRow)!].products[indexPath.row].name
                        

                        let teste = self.categories[Int(indexPathRow)!].products[indexPath.row].picture!
                        
                        let url = urlBase.appendingPathComponent("files/products/\(self.categories[Int(indexPathRow)!].products[indexPath.row].pId!)/thumb/\(self.categories[Int(indexPathRow)!].products[indexPath.row].picture!)")
                        
                        
                        cell.productImageView.kf.indicatorType = .activity
                        cell.productImageView.kf.setImage(with: url)
                        
                        if self.categories[Int(indexPathRow)!].products[indexPath.row].promo_price == "" {
                            
                            let price = "R$ \(self.categories[Int(indexPathRow)!].products[indexPath.row].price!)".withFont(.boldSystemFont(ofSize: 14))
                            
                            cell.priceProductLabel.attributedText = price
                            cell.promoPriceProductLabel.text = nil
                            
                        } else {
                            
                            let promo_price = "R$ \(self.categories[Int(indexPathRow)!].products[indexPath.row].price!)".withStrikethroughStyle(.styleSingle).withBaselineOffset(0).withFont(.systemFont(ofSize: 14)).withTextColor(UIColor.lightGray)
                            let price = "R$ \(self.categories[Int(indexPathRow)!].products[indexPath.row].promo_price!)".withFont(.boldSystemFont(ofSize: 15)).withTextColor(UIColor.black)
                            let por = " por ".withTextColor(UIColor.lightGray).withFont(.systemFont(ofSize: 14))
                            
                            cell.priceProductLabel.attributedText = promo_price + por
                            cell.promoPriceProductLabel.attributedText = price
                            
                        }
                        
                        return cell
                    } else {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyShareCell", for: indexPath) as! BuyShareCollectionViewCell
                        cell.nameProductLabel.text = ""
                        cell.descriptionProductLabel.text = ""
                        cell.priceProductLabel.text = nil
                        cell.promoPriceProductLabel.text = nil
                        cell.productImageView.image = nil
                        
                        return cell
                    }
                }
                
                
            }
            

        }
        
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesPageStoreCell", for: indexPath) as! CategoriesPageStoreCollectionViewCell
        
        cell.categoryNameLabel.text = self.categories[indexPath.row].name
        cell.delegate = self
        cell.accessibilityHint = "\(indexPath.row)"
        
        if self.categories[indexPath.row].promo.count > 0 {
            cell.moreCategoriesButoon.isHidden = true
        } else {
            cell.moreCategoriesButoon.isHidden = false
        }
        
        
        cell.collectionView.tag = 12
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        cell.collectionView.accessibilityHint = "\(indexPath.row)"
        cell.collectionView.register(UINib(nibName: "BuyShareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "buyShareCell")
        cell.collectionView.register(UINib(nibName: "PromoStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "promoCell")
        cell.collectionView.reloadData()
        
        return cell

    }
}

extension PageStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // Tela
        if collectionView.tag == 10 {
            
            var heightScreenPageStore : CGFloat = 0.0
            
            for category in self.categories {
                
                if category.promo.count > 0 {
                    heightScreenPageStore += ((self.view.bounds.width / 2) + 20) * (CGFloat(category.promo.count))
                } else if category.products.count > 0 {
                    let sizeCategoriesProductInBox = (category.products.count % 2) + (category.products.count / 2)
                    heightScreenPageStore += 45.0 + (((self.view.bounds.width / 2) + ((sizeCategoriesProductInBox == 1) ? 102.5 : 100.0)) * CGFloat(sizeCategoriesProductInBox))
                }
                
                print("Products: \(category.products.count), Promo: \(category.promo.count)")
            }
            
            return CGSize(width: self.view.bounds.width, height: heightScreenPageStore + 280)
            
        }
        
        // Produto
        if collectionView.tag == 12 {
            if let indexPathRow = collectionView.accessibilityHint {
                if self.categories[Int(indexPathRow)!].products.count > 0 {
                    return CGSize(width: (self.view.bounds.width / 2) - 11, height: ((self.view.bounds.width / 2) - 25) + 115)
                } else {
                    return CGSize(width: self.view.bounds.width - 15, height: (self.view.bounds.width / 2) - 15)
                }
            }
        }
        
        
        // Box categoria
        if self.categories.count > 0 {
            
            if self.categories[indexPath.row].products.count > 0 {
                
                let sizeCategories = (self.categories[indexPath.row].products.count % 2) + (self.categories[indexPath.row].products.count / 2)
                return CGSize(width: self.view.bounds.width, height: 45 + (((self.view.bounds.width / 2) + ((sizeCategories == 1) ? 102.5 : 100)) * CGFloat(sizeCategories)))
                
            } else {
                
                var promoCount = self.categories[indexPath.row].promo.count
                if promoCount == 0 {
                    promoCount += 1
                }
                return CGSize(width: self.view.bounds.width, height: ((self.view.bounds.width / 2) + 20) * CGFloat(self.categories[indexPath.row].promo.count) + CGFloat((30 / promoCount)))
            }
            
            
        }
        
        return CGSize.zero
    }
    
}

extension PageStoreViewController : PageStoreViewControllerDelegate {
    
    func followPageStore() {
        self.performSegue(withIdentifier: "followStoreSegue", sender: self.store)
    }
    
    func postsPageStore() {
        self.performSegue(withIdentifier: "postsStoreSegue", sender: self.store)
    }
    
    func followStore( _ sender : UIButton) {
        
        if sender.title(for: .normal) == "Seguir" {
            
            sender.backgroundColor = UIColor.white
            sender.setTitle("Seguindo", for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            sender.layer.borderWidth = 1.0
            sender.layer.borderColor = UIColor.black.cgColor
            
            WishareAPI.followStore(self.store.id!, true, { (result : [String : AnyObject], error : Error?) in
                
                if let followed = result["success"] as? Bool {
                    self.store.following = followed
                }
                
            })
            
            
        } else {
            
            let alertController = UIAlertController(title: "Deseja deixar de seguir \(self.store.name!)?", message: nil, preferredStyle: .actionSheet)
            let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                
                sender.backgroundColor = UIColor.black
                sender.setTitle("Seguir", for: .normal)
                sender.setTitleColor(UIColor.white, for: .normal)
                sender.layer.borderWidth = 0.0
                sender.layer.borderColor = UIColor.clear.cgColor
                WishareAPI.followStore(self.store.id!, false, { (result : [String : AnyObject], error : Error?) in
                    
                    if let unfollowed = result["success"] as? Bool {
                        self.store.following = !unfollowed
                    }
                    
                })
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
}

extension PageStoreViewController : CategoriesPageStoreCollectionViewCellDelegate {
    
    func moreCategories(cell: CategoriesPageStoreCollectionViewCell) {
        
        if let row = cell.accessibilityHint {
         
            if self.categories[Int(row)!].name == "Destaques" {
                
                self.performSegue(withIdentifier: "productsCategoriesStoreSegue", sender: [self.categories[Int(row)!], self.store])
                
            } else {
                self.performSegue(withIdentifier: "showSubCategoriesStoreSegue", sender: [self.categories[Int(row)!], self.store])
            }
            
            
            
        }
        
    }
    
}

extension PageStoreViewController : PromoStoreCollectionViewCellDelegate {
    
    func viewPromo(cell : PromoStoreCollectionViewCell) {
        
        if let row = cell.accessibilityHint {
            self.performSegue(withIdentifier: "productsCategoriesStoreSegue", sender: [self.categories[Int(cell.row)!], self.store, self.categories[Int(cell.row)!].promo[Int(row)!]])
        }
        
    }
    
}

extension PageStoreViewController : UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.performSegue(withIdentifier: "recentSearchSegue", sender: self.store)
        return false
    }
    
}
