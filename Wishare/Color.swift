//
//  Color.swift
//  Wishare
//
//  Created by Wishare iMac on 6/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Color {
    
    var id : String?
    var quantity : String?
    var color : String?
    var picture : String?
    
    init (byProductDetail color : [String : AnyObject]) {
        
        self.id = color["id"] as? String
        self.quantity = color["qtd"] as? String
        self.color = color["color"] as? String
        
        let pictures = color["picture"] as? [[String : String]]
        
        if let pict = pictures {
            
            for p in pict {
                
                if p["index"] == "0" {
                    self.picture = p["img"]
                    break
                } else {
                    self.picture = p["img"]
                }
                
            }
            
        }
        
        
        
    }
    
//    "id": "1440",
//    "depth": "0",
//    "photo_id": "0",
//    "variation": "",
//    "price": "159,00",
//    "promo_price": "",
//    "qtd": "100",
//    "color": " CHUMBO",
//    "size": "P",
//    "ref_tp": "REF",
//    "ref": "Chumbo1",
//    "product_id": "188",
//    "created": "",
//    "modified": "",
//    "active": "A",
//    "promo_id": null,
//    "picture": ""
    
}
