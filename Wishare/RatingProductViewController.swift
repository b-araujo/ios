//
//  RatingProductViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/20/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import FloatRatingView

class RatingProductViewController: UIViewController {

    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var opnionTextView: UITextView!
    @IBOutlet weak var anonymousButton: UIButton!
    
    var idProduct : String!
    var isEditingRating : Bool!
    var myRatingProduct : [String : AnyObject] = [:]
    var isAnonymous : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.myRatingProduct.count > 0 {
            
            self.ratingView.rating = Float(self.myRatingProduct["rate"] as! String)!
            self.titleTextField.text = self.myRatingProduct["title"] as? String
            self.opnionTextView.text = self.myRatingProduct["description"] as! String
            
            if (self.myRatingProduct["anonymous"] as! String) != "0" {
                
                self.isAnonymous = true
                self.anonymousButton.setImage(UIImage(named: "checkbox_filled"), for: .normal)
                
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func anonymousComment(_ sender: UIButton) {
        
        if self.isAnonymous {
            
            sender.setImage(UIImage(named: "checkbox"), for: .normal)
            self.isAnonymous = false
            
        } else {
            
            sender.setImage(UIImage(named: "checkbox_filled"), for: .normal)
            self.isAnonymous = true
            
        }
        
    }
    
    @IBAction func removeComment(_ sender: UIButton) {
    
        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
        
        WishareAPI.ratingProductRemove(pId: self.idProduct) { (error : Error?) in
            if error == nil {
                
                RappleActivityIndicatorView.stopAnimation()
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    
    }
    
    
    @IBAction func sendRating(_ sender: UIBarButtonItem) {
        
        if self.ratingView.rating == 0.0 {
            
            let alertController = UIAlertController(title: nil, message: "Por favor, classifique", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.titleTextField.text == "" {
            
            let alertController = UIAlertController(title: nil, message: "Coloque um título na sua avaliação", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            
            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Salvando...", attributes: attributes)
            
            
            WishareAPI.ratingProductPost(rating: self.ratingView.rating, pId: self.idProduct, title: self.titleTextField.text!, description: self.opnionTextView.text, anonymous: (self.isAnonymous) ? "1" : "0", { (error : Error?) in
                
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation()
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            })
            
        }
        
        
        
    }

}
