//
//  WishareNotification.swift
//  Wishare
//
//  Created by Wishare iMac on 8/1/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Foundation

@objc protocol WishareNotificationDelegate {

    func openUserProfile(_ tableView : UITableView, _ indexPath : IndexPath)
    func acceptOrDeclineInvite(_ tableView : UITableView, _ indexPath : IndexPath, _ sender : UIButton)
    func followOrShare(_ tableView : UITableView, _ indexPath : IndexPath, _ sender : UIButton)
    func userProfile(_ tableView : UITableView, _ indexPath : IndexPath, _ gesture : UITapGestureRecognizer)
    func openPostInformation(_ tableView : UITableView, _ indexPath : IndexPath)
    func openStore(_ tableView : UITableView, _ indexPath : IndexPath)
    
}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label : UILabel, inRange targetRange : NSRange) -> Bool {
        
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: (locationOfTouchInLabel.x - textContainerOffset.x), y: 0)
        // Adjust for multiple lines of text
        let lineModifier = Int(ceil(locationOfTouchInLabel.y / label.font.lineHeight)) - 1
        let rightMostFirstLinePoint = CGPoint(x: labelSize.width, y: 0.0)
        let charsPerLine = layoutManager.characterIndex(for: rightMostFirstLinePoint, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        let adjustedRange = indexOfCharacter + (lineModifier * charsPerLine)
        
        return NSLocationInRange(adjustedRange, targetRange)
    
        
    }
    
}
