//
//  RecommendationSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/16/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Pager
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class RecommendationSearchViewController: PagerController {
    
    @IBOutlet weak var dropMenu: UIButton!

    var controllers : [CustomViewController] = []
    var titles : [String] = []
    var products : [[Product]] = []
    var cat : Categories = Categories()
    var top : [Product] = []
    
    var items_search : [[String]] = [[],[],[],[]]
    var tab_selected : Int = 0
    
    var loader : Loader!
    var manager : Manager!
    
    var id_category : String!
    var name_category : String!
    var auto_complete : Bool = false
    var isFirstAccess : Bool = true
    
    let defaults = UserDefaults.standard
    
    // - Search Complete
    
    let searchView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.red
        view.tag = 24
        view.isHidden = true
        return view
    }()
    
    
    let searchTableView : UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.white
        tableView.tag = 25
        tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.onDrag
        return tableView
    }()
    
    
    
    // - Dados
    
    var productsResult = [Product]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: productsResult)
        }
    }
    
    var usersResult = [User]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("usersResultUpdated"), object: usersResult)
        }
    }
    
    var storesResult = [Store]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("storesResultUpdated"), object: storesResult)
        }
    }
    
    var wipsResult = [User]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("usersWipsResultUpdated"), object: wipsResult)
        }
    }
    
    
    // - Search Controller 
    
    var resultSearchController : UISearchController!
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: false)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateChangeTabs(notification:)), name: Notification.Name.init("changeTabs"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPageStore(notification:)), name: Notification.Name.init("openPageStore"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPageUser(notification:)), name: Notification.Name.init("openPageUser"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openProductDetail(notification:)), name: Notification.Name.init("openProductDetail"), object: nil)
        
        
        self.searchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        
        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        
        // - Setup Search Controller
        
        resultSearchController = UISearchController(searchResultsController: GeneralSearchViewController())
        resultSearchController.searchResultsUpdater = self
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        resultSearchController.searchBar.searchBarStyle = .prominent
        resultSearchController.searchBar.sizeToFit()
        resultSearchController.searchBar.placeholder = "Pesquisar"
        resultSearchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
        resultSearchController.searchBar.delegate = self
        
        resultSearchController.view.addSubview(searchView)
        
        //searchView.heightAnchor.constraint(equalToConstant: self.view.bounds.width / 2.5).isActive = true
        searchView.bottomAnchor.constraint(equalTo: resultSearchController.view.bottomAnchor).isActive = true
        searchView.leftAnchor.constraint(equalTo: resultSearchController.view.leftAnchor).isActive = true
        searchView.rightAnchor.constraint(equalTo: resultSearchController.view.rightAnchor).isActive = true
        searchView.topAnchor.constraint(equalTo: resultSearchController.view.topAnchor, constant: 64).isActive = true
        
        searchView.addSubview(searchTableView)
        
        searchTableView.topAnchor.constraint(equalTo: searchView.topAnchor).isActive = true
        searchTableView.bottomAnchor.constraint(equalTo: searchView.bottomAnchor).isActive = true
        searchTableView.leftAnchor.constraint(equalTo: searchView.leftAnchor).isActive = true
        searchTableView.rightAnchor.constraint(equalTo: searchView.rightAnchor).isActive = true
        
        self.navigationItem.titleView = resultSearchController.searchBar
        definesPresentationContext = true
        
        // ================================
        
        self.dataSource = self
        self.navigationItem.hidesBackButton = true
        
        //self.createSearchBar()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        
        dropMenu.set(image: UIImage(named: "expand"), title: NSString(string: self.name_category), titlePosition: .left, additionalSpacing: 10, state: .normal)
    
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando produtos...", attributes: attributes)

        self.indicatorColor = UIColor.black
        self.contentViewBackgroundColor = UIColor.white
        self.tabsTextColor = UIColor.black
        self.selectedTabTextColor = UIColor.black
        self.tabsViewBackgroundColor = UIColor.white
        
        WishareAPI.recommendations(self.id_category) { (categories : Categories?, products : [Product]?, error : Error?) in
            
            if error == nil {
                
                if let topCat = products {
                    self.top = topCat
                }
                
                if let categoriesResult = categories {
                    
                    self.cat = categoriesResult
                    
                    for (index, category) in self.cat.category.enumerated() {
                        
                        let controller = CustomViewController()
                        self.products.append(category.products)
                        
                        if index == 0 {
                            
                            let layout = UICollectionViewFlowLayout()
                            layout.itemSize = CGSize(width: 100, height: 100)
                            layout.minimumInteritemSpacing = 2.5
                            layout.minimumLineSpacing = 5
                            let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
                            collectionView.translatesAutoresizingMaskIntoConstraints = false
                            collectionView.backgroundColor = UIColor.white
                            collectionView.tag = index
                            collectionView.delegate = self
                            collectionView.dataSource = self
                            collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
                            collectionView.register(UINib(nibName: "TopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "topCell")
                            collectionView.showsHorizontalScrollIndicator = false
                            controller.view.addSubview(collectionView)
                            
                            collectionView.topAnchor.constraint(equalTo: controller.view.topAnchor, constant: 5).isActive = true
                            collectionView.bottomAnchor.constraint(equalTo: controller.view.bottomAnchor).isActive = true
                            collectionView.rightAnchor.constraint(equalTo: controller.view.rightAnchor).isActive = true
                            collectionView.leftAnchor.constraint(equalTo: controller.view.leftAnchor).isActive = true

                            
                        } else {
                            
                            let layout = UICollectionViewFlowLayout()
                            layout.itemSize = CGSize(width: 100, height: 100)
                            layout.minimumInteritemSpacing = 2.5
                            layout.minimumLineSpacing = 5
                            let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
                            collectionView.translatesAutoresizingMaskIntoConstraints = false
                            collectionView.backgroundColor = UIColor.white
                            collectionView.tag = index
                            collectionView.delegate = self
                            collectionView.dataSource = self
                            collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
                            collectionView.showsHorizontalScrollIndicator = false
                            controller.view.addSubview(collectionView)
                            
                            collectionView.topAnchor.constraint(equalTo: controller.view.topAnchor, constant: 10).isActive = true
                            collectionView.bottomAnchor.constraint(equalTo: controller.view.bottomAnchor).isActive = true
                            collectionView.rightAnchor.constraint(equalTo: controller.view.rightAnchor).isActive = true
                            collectionView.leftAnchor.constraint(equalTo: controller.view.leftAnchor).isActive = true
                            
                        }
                        
                        self.controllers.append(controller)
                        self.titles.append(category.name!)
                        
                    }
                    
                }
                

                
                
                self.setupPager(tabNames: self.titles, tabControllers: self.controllers)
                self.indicatorColor = UIColor.black
                self.contentViewBackgroundColor = UIColor.white
                self.tabsTextColor = UIColor.black
                self.selectedTabTextColor = UIColor.black
                self.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
            }
            
        }
        
        
    }

    override func viewDidDisappear(_ animated: Bool) {
        print("SAIU")
        self.isFirstAccess = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("REFRESH SUGESTOES")
        
        if !self.isFirstAccess {
            
            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando produtos...", attributes: attributes)
            
            WishareAPI.recommendations(self.id_category) { (categories : Categories?, products : [Product]?, error : Error?) in
                
                if error == nil {
                    
                    if let topCat = products {
                        self.top.removeAll()
                        self.top = topCat
                    }
                    
                    if let categoriesResult = categories {
                        
                        self.products.removeAll()
                        self.cat = categoriesResult
                        
                        for (_, category) in self.cat.category.enumerated() {
                            self.products.append(category.products)
                        }
                    }
                    
                    for controller in self.controllers {
                        
                        let views = controller.view.subviews
                        
                        for view in views {
                            if let collectionView : UICollectionView = view as? UICollectionView {
                                collectionView.reloadData()
                            }
                        }
                        
                    }
                    
                    RappleActivityIndicatorView.stopAnimation()
                }
                
            }
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func myCart() {
        
    }
    
    func createSearchBar () {
        
        let searchBar = UISearchBar()
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Pesquisar"
        searchBar.delegate = self
        
        self.navigationItem.titleView = searchBar
        
    }

    @objc func updateChangeTabs(notification : Notification) {
        
        let tabSelected = notification.object as! Int
        self.tab_selected = tabSelected
        
        print("====== ABA ATUAL =======")
        print("\(self.tab_selected)/\(tabSelected)")
        print("========================")
        
        if self.tab_selected != 3 {
            self.searchTableView.reloadData()
        }
        
    }
    
    @objc func openPageStore(notification : Notification) {
        let idStore = notification.object as! String
        self.performSegue(withIdentifier: "pageStoreSegue", sender: idStore)
    }
    
    @objc func openPageUser(notification : Notification) {
        let idUser = notification.object as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = idUser
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
    }
    
    @objc func openProductDetail(notification : Notification) {
        
        let idProduct = notification.object as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = idProduct
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pageStoreSegue" {
            let pageStoreViewController = segue.destination as! PageStoreViewController
            pageStoreViewController.idStore = sender as! String
        }
        
    }
    
    @IBAction func showCategories(_ sender: UIButton) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "id_category")
        defaults.removeObject(forKey: "name_category")
        self.performSegue(withIdentifier: "unwindToCategories", sender: nil)
    }
}

extension RecommendationSearchViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        searchController.searchResultsController?.view.isHidden = false
    }
    
}

extension RecommendationSearchViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: resultSearchController)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("BUSCANDO ...")
        
        self.searchView.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: true)
        
        if let searchText = searchBar.text {
            
            WishareAPI.searchTabs(searchText, { (products : [Product], stores : [Store], users : [User], wips : [User], error : Error?) in
                
                if error == nil {
                 
                    // ATUALIZAR RESULTS CONTROLLER 
                    self.productsResult = products
                    self.usersResult = users
                    self.storesResult = stores
                    self.wipsResult = wips
                    NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: false)
                    NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: false)
                    NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: false)
                    NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: true)
                    UserDefaults.standard.set(true, forKey: "isSearchTabs")
                    
                    // - AQUI
                    self.defaults.set(searchText, forKey: "word_search")
                    
                }
                
            })
            
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        print("Cancel")
        self.searchView.isHidden = true
        
        // - PERSONALIZADO RECARREGAR AO CLICAR NO CANCELAR
        
        self.searchView.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: true)
        NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: false)
        
        WishareAPI.searchUser("9999", { (users : [User], error : Error?) in
            
            if error == nil {
                self.usersResult = users
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: false)
            }
            
        })
        
        
        WishareAPI.searchProduct("10", { (products : [Product], error : Error?) in
            
            if error == nil {
                self.productsResult = products
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: false)
            }
            
        })
        
        WishareAPI.searchStore("9999", { (stores : [Store], error : Error?) in
            
            if error == nil {
                self.storesResult = stores
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: false)
            }
            
        })
        
        WishareAPI.searchWips { (users : [User], error : Error?) in
            
            if error == nil {
                self.wipsResult = users
                // -
            }
            
        }

        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        UserDefaults.standard.removeObject(forKey: "previewOption")
        
        if searchText == "" {
            
            self.searchView.isHidden = true
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: true)
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: true)
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: true)
            NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: false)
            
            if let decodedUsers : Data =  UserDefaults.standard.object(forKey: "initialUsers") as? Data {
                self.usersResult =  NSKeyedUnarchiver.unarchiveObject(with: decodedUsers) as! [User]
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: false)
            }
            
            let decodedProducts =  UserDefaults.standard.object(forKey: "initialProducts") as! Data
            self.productsResult =  NSKeyedUnarchiver.unarchiveObject(with: decodedProducts) as! [Product]
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: false)
            
            if let decodedStores : Data =  UserDefaults.standard.object(forKey: "initialStores") as? Data {
                self.storesResult =  NSKeyedUnarchiver.unarchiveObject(with: decodedStores) as! [Store]
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: false)
            }
            
            
            WishareAPI.searchWips { (users : [User], error : Error?) in
                if error == nil {
                    self.wipsResult = users
                }
            }
            
            
        } else if searchText.count >= 3 && self.auto_complete == false {
            
            WishareAPI.searchComplete(searchText, { (products : [String], stores : [String], users : [String], wips : [String], error : Error?) in
                        
                if error == nil {
        
                    self.items_search[0] = products
                    self.items_search[1] = users
                    self.items_search[2] = stores
                    self.items_search[3] = wips
                    
                
                    if self.items_search[self.tab_selected].count == 0 {
                        self.searchView.isHidden = true
                    } else {
                        self.searchView.isHidden = false
                    }
                    
                    self.searchTableView.reloadData()
                            
                }
                        
            })
            
        }
        
        self.auto_complete = false
        
    }
    
    //QANDO ELE GANHAR O FOCO APOS TER ENTRADO NO CANCELAR, ELE DEVE VOLTAR PARA PAGINA QUE ESTAVA 
    
}

extension RecommendationSearchViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.items_search[self.tab_selected][indexPath.row])
        
        self.resultSearchController.searchBar.text = self.items_search[self.tab_selected][indexPath.row]
        self.becomeFirstResponder()
        self.view.endEditing(true)
        self.searchView.isHidden = true
        self.auto_complete = true
        
        WishareAPI.searchTabs(self.items_search[self.tab_selected][indexPath.row], { (products : [Product], stores : [Store], users : [User], wips : [User], error : Error?) in
            
            if error == nil {
                
                // ATUALIZAR RESULTS CONTROLLER
                self.productsResult = products
                self.usersResult = users
                self.storesResult = stores
                self.wipsResult = wips
                
                // - AQUI
                self.defaults.set(self.items_search[self.tab_selected][indexPath.row], forKey: "word_search")
                NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: true)
                UserDefaults.standard.set(true, forKey: "isSearchTabs")
            }
            
        })
        
    }
    
}

extension RecommendationSearchViewController : UITableViewDataSource {
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items_search[self.tab_selected].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = self.items_search[self.tab_selected][indexPath.row]
        
        return cell
        
    }
    
}

extension RecommendationSearchViewController : PagerDataSource {
    
    func numberOfTabs(_ pager: PagerController) -> Int {
        return self.titles.count
    }
    
}

extension RecommendationSearchViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if let hint = collectionView.accessibilityHint {
            
            if hint == "top" {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                productDetailViewController.idProduct = self.top[indexPath.row].id!
                productDetailViewController.isFirst = true
                
                self.navigationController?.pushViewController(productDetailViewController, animated: true)
                
            }
            
        } else {
            
            if collectionView.tag == 0 {
            
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                productDetailViewController.idProduct = self.products[collectionView.tag][indexPath.row-1].id!
                productDetailViewController.isFirst = true
                
                self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
            } else {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                productDetailViewController.idProduct = self.products[collectionView.tag][indexPath.row].id!
                productDetailViewController.isFirst = true
                
                self.navigationController?.pushViewController(productDetailViewController, animated: true)
                
            }
                
        }
        
        

        
    }
    
}

extension RecommendationSearchViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        if indexPath.row == 0 && collectionView.tag == 0 && collectionView.isPrimary == nil{
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width / 2)
        }
        
        
        if let hint = collectionView.accessibilityHint {
            if hint == "top"  {
                return CGSize(width: self.view.bounds.width / 2, height: self.view.bounds.width / 2)
            }
        }
        
        return CGSize(width: (self.view.bounds.width / 3) - 2.5, height: (self.view.bounds.width / 3) - 5)
    
    }
    
}

extension RecommendationSearchViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let hint = collectionView.accessibilityHint {
            if hint == "top" {
                return self.top.count
            }
        }
        
        return self.products[collectionView.tag].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if let hint = collectionView.accessibilityHint {
            if hint == "top" {
               
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url = urlBase.appendingPathComponent("files/products/\(self.top[indexPath.row].pId!)/thumb/\(self.top[indexPath.row].picture!)")
                
                cell.imageProductsLook.image = nil
                cell.loadingActivityIndicator.startAnimating()
                
                self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.imageProductsLook.image = result.value
                }
                
                return cell

                
            }
        }
        
        
        if indexPath.row == 0 && collectionView.tag == 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topCell", for: indexPath) as! TopCollectionViewCell
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
            cell.collectionView.accessibilityHint = "top"
            cell.collectionView.tag = 0
            cell.collectionView.isPrimary = "true"
            cell.collectionView.backgroundColor = UIColor.white
            cell.collectionView.reloadData()
            return cell
            
        }
        
        
        if collectionView.tag == 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/products/\(self.products[collectionView.tag][indexPath.row-1].pId!)/thumb/\(self.products[collectionView.tag][indexPath.row-1].picture!)")
            
            cell.imageProductsLook.image = nil
            cell.loadingActivityIndicator.startAnimating()
            
            self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.imageProductsLook.image = result.value
            }
            
            return cell
            
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/products/\(self.products[collectionView.tag][indexPath.row].pId!)/\(self.products[collectionView.tag][indexPath.row].picture!)")
        
        cell.imageProductsLook.image = nil
        cell.loadingActivityIndicator.startAnimating()
        
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        return cell
        
    }
    
}
