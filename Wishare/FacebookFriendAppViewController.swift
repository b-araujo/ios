//
//  FacebookFriendAppViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/24/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin

class FacebookFriendAppViewController: UIViewController {

    
    @IBOutlet weak var friendFacebookCollection: UICollectionView!
    
    var friendsList : [[String : AnyObject]] = [[:]]
    var loader : Loader!
    var manager : Manager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FacebookAPI.getFacebookFriends(viewControllerIfNoPermition: self) { (result : NSArray, error : Error?) in
            
            if error == nil {
                
                self.friendsList.removeAll()
                
                for friend in result {
                    self.friendsList.append(friend as! [String : AnyObject])
                }
                
                print("=============================")
                print(self.friendsList)
                
                self.friendFacebookCollection.reloadData()
            }
            
        }
        
        
        self.friendFacebookCollection.delegate = self
        self.friendFacebookCollection.dataSource = self
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.friendFacebookCollection.register(UINib(nibName: "FollowUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "friendFacebookCell")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func continueClicked(_ sender: Any) {
        print("Continue clicked")
    }
    
}

extension FacebookFriendAppViewController : UICollectionViewDelegate {
    
    
}

extension FacebookFriendAppViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.friendsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "friendFacebookCell", for: indexPath) as! FollowUserCollectionViewCell
        
        cell.delegate = self
        
        cell.accessibilityHint = self.friendsList[indexPath.row]["id"] as? String
        cell.nameUserLabel.text = self.friendsList[indexPath.row]["name"] as? String
        
        var url : URL?
        
        if let data : [String : AnyObject] = self.friendsList[indexPath.row]["picture"] as? [String : AnyObject]{
            if let urlPicture : [String : AnyObject] = data["data"] as? [String : AnyObject] {
                url = URL(string: urlPicture["url"] as! String)!
            }
        }
        
        
        cell.loadingActivityIndicator.startAnimating()
        
        if let urlPhotoFacebook = url {

            self.manager.loadImage(with: urlPhotoFacebook, into: cell.userPhotoImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.userPhotoImageView.image = result.value?.circleMask
            }
            
        }


        return cell
        
    }
    
}

extension FacebookFriendAppViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 1.25, height: (self.view.bounds.width / 3) + 100)
    }
    
}

extension FacebookFriendAppViewController : FollowUserCollectionViewCellDelegate {
    
    func followUser(cell: FollowUserCollectionViewCell, sender: UIButton) {
        
        if sender.title(for: .normal) == "Seguir" {
            
            if let idFacebook = cell.accessibilityHint {
                
                print(idFacebook)
                
                WishareAPI.followUserFacebook(idFacebook, true, { (result : [String : AnyObject], error : Error?) in
                    
                    if error == nil {
                        
                        print(result)
                        
                        sender.backgroundColor = UIColor.white
                        sender.setTitle("Seguindo", for: .normal)
                        sender.setTitleColor(UIColor.black, for: .normal)
                        sender.layer.borderWidth = 2.0
                        sender.layer.borderColor = UIColor.black.cgColor
                        
                    }
                    
                })
                
            }
            
        } else {
            
            if let idFacebook = cell.accessibilityHint {
                
                WishareAPI.followUserFacebook(idFacebook, false, { (result : [String : AnyObject], error : Error?) in
                    
                    if error == nil {
                        
                        print(result)
                    
                        sender.backgroundColor = UIColor.black
                        sender.setTitle("Seguir", for: .normal)
                        sender.setTitleColor(UIColor.white, for: .normal)
                        sender.layer.borderWidth = 0.0
                        sender.layer.borderColor = UIColor.clear.cgColor
                        
                    }
                    
                })
                
            }
            
        }
        
    }
    
}
