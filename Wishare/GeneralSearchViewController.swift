//
//  GeneralSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Pager
import RappleProgressHUD

class GeneralSearchViewController: PagerController {
    
    
    var isSearch : Bool = false
    var tabSelected : Int = 0
    
    let viewButton : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.isHidden = true
        return view
    }()
    
    
    let filterButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black, for: .normal)
        button.set(image: UIImage(named: "filter"), title: NSString(string: "Filtrar"), titlePosition: UIViewContentMode.right, additionalSpacing: 10, state: .normal)
        return button
    }()
    
    
    let orderButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black, for: .normal)
        button.set(image: UIImage(named: "order"), title: NSString(string: "Ordenar"), titlePosition: UIViewContentMode.right, additionalSpacing: 10, state: .normal)
        return button
    }()
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        orderButton.addTarget(self, action: #selector(pressOrderBy), for: .touchUpInside)
        filterButton.addTarget(self, action: #selector(pressFilter), for: .touchUpInside)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productsSearch = storyboard.instantiateViewController(withIdentifier: "productsSearch") as! ProductSearchViewController
        let userSearch = storyboard.instantiateViewController(withIdentifier: "userSearch") as! UserSearchViewController
        let storeSearch = storyboard.instantiateViewController(withIdentifier: "storeSearch") as! StoreSearchViewController
        let wipsSearch = storyboard.instantiateViewController(withIdentifier: "wipsSearch") as! WipsSearchViewController
        
        customizeTab()
        self.setupPager(tabNames: ["Produtos", "Usuários", "Lojas", "WIP'S"], tabControllers: [productsSearch, userSearch, storeSearch, wipsSearch])
        
        NotificationCenter.default.addObserver(self, selector: #selector(searchUpdate(notification:)), name: Notification.Name.init("searchUpdate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateChangeTabs(notification:)), name: Notification.Name.init("changeTabs"), object: nil)
        
        self.view.addSubview(viewButton)
        self.viewButton.addSubview(filterButton)
        self.viewButton.addSubview(orderButton)
        
        
        var viewTopConstant: CGFloat = CGFloat.init(112)
        
        if UIDevice.isIphoneX {
            viewTopConstant += 24.0
        }
        
        self.viewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: viewTopConstant).isActive = true
        self.viewButton.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        self.viewButton.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        self.viewButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        self.filterButton.topAnchor.constraint(equalTo: self.viewButton.topAnchor).isActive = true
        self.filterButton.leftAnchor.constraint(equalTo: self.viewButton.leftAnchor).isActive = true
        self.filterButton.bottomAnchor.constraint(equalTo: self.viewButton.bottomAnchor).isActive = true
        self.filterButton.widthAnchor.constraint(equalToConstant: self.view.bounds.width / 2).isActive = true
        
        self.orderButton.topAnchor.constraint(equalTo: self.viewButton.topAnchor).isActive = true
        self.orderButton.rightAnchor.constraint(equalTo: self.viewButton.rightAnchor).isActive = true
        self.orderButton.bottomAnchor.constraint(equalTo: self.viewButton.bottomAnchor).isActive = true
        self.orderButton.widthAnchor.constraint(equalToConstant: self.view.bounds.width / 2).isActive = true
        

        
        print("VIEW DID LOAD GENERAL SEARCH")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("VIEW DID APPEAR GENERAL SEARCH")
        self.selectTabAtIndex(self.tabSelected)
    }
    
    func customizeTab() {
        //tabTopOffset = -20.0
        tabTopOffset = 44.0
        tabWidth = self.view.bounds.width / 4
        tabOffset = 0
//        fixMenu = true
        animation = .during
//        contentViewBackgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
//        self.contentViewBackgroundColor = UIColor.white
        self.indicatorColor = UIColor.black
        self.tabHeight += 5
        self.indicatorColor = UIColor.black
        self.contentViewBackgroundColor = UIColor.white
        self.centerCurrentTab = true
        self.tabsViewBackgroundColor = UIColor.white
        self.tabsTextColor = UIColor.black
        self.selectedTabTextColor = UIColor.black
    }
    
    @objc func searchUpdate(notification: Notification) {
        
        let isSearch = notification.object as! Bool
        self.isSearch = isSearch
        
        if isSearch && self.tabSelected == 0 {
            self.viewButton.isHidden = false
            UserDefaults.standard.set("asc", forKey: "orderBy")
            UserDefaults.standard.set("brand", forKey: "typeBy")
        } else {
            self.viewButton.isHidden = true
        }
        
    }
    
    @objc func pressOrderBy() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        let menorPriceAction = UIAlertAction(title: "Menor Preço", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Menor Preço...", attributes: self.attributes)
            
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOption") as? [String : Any] {
               
                WishareAPI.searchProductFilterBy(lastPreviewOption["text"] as! String, lastPreviewOption["category"] as! [String], lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], lastPreviewOption["store"] as! [String], "asc", "price", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("asc", forKey: "orderBy")
                        UserDefaults.standard.set("price", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search") as? String {
              
                WishareAPI.searchProductFilterBy(word_search, [], [], [], [], [], "asc", "price", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("asc", forKey: "orderBy")
                        UserDefaults.standard.set("price", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                        
                    }
                    
                })
                
            }
            
        }
        
        let maiorPriceAction = UIAlertAction(title: "Maior Preço", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Maior Preço...", attributes: self.attributes)
            
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOption") as? [String : Any] {
                
                WishareAPI.searchProductFilterBy(lastPreviewOption["text"] as! String, lastPreviewOption["category"] as! [String], lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], lastPreviewOption["store"] as! [String], "desc", "price", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("desc", forKey: "orderBy")
                        UserDefaults.standard.set("price", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search") as? String {
                
                WishareAPI.searchProductFilterBy(word_search, [], [], [], [], [], "desc", "price", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("desc", forKey: "orderBy")
                        UserDefaults.standard.set("price", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                        
                    }
                    
                })
                
            }
            
        }
        
        let marcaAZAction = UIAlertAction(title: "Marca A-Z", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Marca A-Z...", attributes: self.attributes)
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOption") as? [String : Any] {
                
                WishareAPI.searchProductFilterBy(lastPreviewOption["text"] as! String, lastPreviewOption["category"] as! [String], lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], lastPreviewOption["store"] as! [String], "asc", "brand", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("asc", forKey: "orderBy")
                        UserDefaults.standard.set("brand", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search") as? String {
                
                WishareAPI.searchProductFilterBy(word_search, [], [], [], [], [], "asc", "brand", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("asc", forKey: "orderBy")
                        UserDefaults.standard.set("brand", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                        
                    }
                    
                })
                
            }
            
        }
        
        let marcaZAAction = UIAlertAction(title: "Marca Z-A", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Marca Z-A...", attributes: self.attributes)
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOption") as? [String : Any] {
                
                WishareAPI.searchProductFilterBy(lastPreviewOption["text"] as! String, lastPreviewOption["category"] as! [String], lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], lastPreviewOption["store"] as! [String], "desc", "brand", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("desc", forKey: "orderBy")
                        UserDefaults.standard.set("brand", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search") as? String {
                
                WishareAPI.searchProductFilterBy(word_search, [], [], [], [], [], "desc", "brand", { (_, _, _, _, products : [Product], _, _, error : Error?) in
                    
                    if error == nil {
                        
                        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: products)
                        UserDefaults.standard.set("desc", forKey: "orderBy")
                        UserDefaults.standard.set("brand", forKey: "typeBy")
                        UserDefaults.standard.set(false, forKey: "isSearchTabs")
                        RappleActivityIndicatorView.stopAnimation()
                        
                        
                    }
                    
                })
                
            }
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(menorPriceAction)
        alertController.addAction(maiorPriceAction)
        alertController.addAction(marcaAZAction)
        alertController.addAction(marcaZAAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }

    @objc func pressFilter() {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "navFilterSearchID") as! UINavigationController
        self.present(navigationController, animated:true, completion: nil)
        
    }
    
    @objc func updateChangeTabs(notification : Notification) {
        
        let tabSelected = notification.object as! Int
        self.tabSelected = tabSelected
        
        
        
        if tabSelected == 0 && self.isSearch == true {
            self.viewButton.isHidden = false
        } else {
            self.viewButton.isHidden = true
        }
        
    }

    
}

extension GeneralSearchViewController : PagerDataSource {
    
}
