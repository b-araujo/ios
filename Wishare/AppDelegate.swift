//
//  AppDelegate.swift
//  Wishare
//
//  Created by Wishare iMac on 4/11/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoginKit
import Fabric
import Crashlytics
import UserNotifications

import Firebase
import FirebaseInstanceID
import FirebaseMessaging

fileprivate let notificationIdentifier = "POST_IDENTIFIER"
fileprivate let orderIdentifier = "ORDER_IDENTIFIER"

fileprivate let notificationCategory = "Post"
fileprivate let orderCategory = "Order"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        UINavigationBar.appearance().tintColor = UIColor.black
        
        Fabric.with([Crashlytics.self])
    
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            
            let notificationAction = UNNotificationAction(identifier: notificationIdentifier, title: "Ver Notificações", options: [UNNotificationActionOptions.foreground])
            let orderAction = UNNotificationAction(identifier: orderIdentifier, title: "Ver Pedidos", options: [UNNotificationActionOptions.foreground])
            
            let notificationCategoryUN = UNNotificationCategory(identifier: notificationCategory, actions: [notificationAction], intentIdentifiers: [], options: [])
            let orderCategoryUN = UNNotificationCategory(identifier: orderCategory, actions: [orderAction], intentIdentifiers: [], options: [])
            
            UNUserNotificationCenter.current().setNotificationCategories([notificationCategoryUN, orderCategoryUN])
            
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
            
            Messaging.messaging().delegate = self
            
        } else {
            
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        
        }
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification(notification:)), name: Notification.Name.InstanceIDTokenRefresh, object: nil)
        
        if let _ = launchOptions?[.remoteNotification] as? [String : AnyObject] {
            NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationBadge"), object: 1)
        }
        
        return true
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationBadge"), object: 1)
        
        let userInfo = notification.request.content.userInfo
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID (willPresent): \(messageID)")
        }
        
        print(userInfo)
        
        completionHandler([.alert, .badge, .sound])
    
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    
        let userInfo = response.notification.request.content.userInfo
    
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID (didReceive): \(messageID)")
        }
        
        print(userInfo)
        
        
        let actionIdentifier = response.actionIdentifier
        let content = response.notification.request.content
        
        switch actionIdentifier {
            
        case UNNotificationDismissActionIdentifier:
            completionHandler()
            
        case UNNotificationDefaultActionIdentifier:
            
            if content.categoryIdentifier == "Post" {
                
                UserDefaults.standard.set("post", forKey: "remoteNotificationDidReceive")
                NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationDidReceive"), object: "post", userInfo: content.userInfo)
                
                
            } else if content.categoryIdentifier == "Order" {
                
                UserDefaults.standard.set("post", forKey: "remoteNotificationDidReceive")
                NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationDidReceive"), object: "order", userInfo: content.userInfo)
                
            } else if content.categoryIdentifier == "User" {
                
                UserDefaults.standard.set("post", forKey: "remoteNotificationDidReceive")
                NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationDidReceive"), object: "post", userInfo: content.userInfo)
                
            }
            
            completionHandler()
            
        case "POST_IDENTIFIER":
            
            UserDefaults.standard.set("post", forKey: "remoteNotificationDidReceive")
            NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationDidReceive"), object: "post", userInfo: content.userInfo)
            
            completionHandler()
        
        case "ORDER_IDENTIFIER":
            
            UserDefaults.standard.set("post", forKey: "remoteNotificationDidReceive")
            NotificationCenter.default.post(name: NSNotification.Name.init("remoteNotificationDidReceive"), object: "order", userInfo: content.userInfo)
            
            completionHandler()
        
        default:
        
            completionHandler()
        
        }
        
    
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        UserDefaults.standard.set(fcmToken, forKey: "device_token")
        print("Firebase registration token: \(fcmToken)")
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("didRegisterForRemoteNotificationsWithDeviceToken()")
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaults.standard.set(deviceTokenString, forKey: "device_token")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    

    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID (didReceiveRemoteNotification): \(messageID)")
        }
        
        print(userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID (fetchCompletionHandler): \(messageID)")
        }
        
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        // Messaging.messaging().disconnect()
        // print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        WishareCoreData.isLoggedIn { (result : Bool, _, _) in
            if result {
                WishareAPI.quantityNotification({ (number : String, error : Error?) in
                    if error == nil {
                        
                        print("Number Bagde +")
                        UserDefaults.standard.set(number, forKey: "numberBagde")
                    
                    }
                })
            }
        }
        
        
        connectToFcm()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    
    @objc func tokenRefreshNotification(notification : Notification) {
        
        print("tokenRefreshNotification")
        
        if let refreshToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshToken)")
        }
        
        connectToFcm()
        
    }
    
    func connectToFcm () {
        
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        Messaging.messaging().shouldEstablishDirectChannel = true
        
    }
    
    
    
    // MARK: Facebook
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Wishare")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

