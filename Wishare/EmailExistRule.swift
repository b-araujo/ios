//
//  EmailExistRule.swift
//  Wishare
//
//  Created by Wishare iMac on 4/19/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import SwiftValidator

public class EmailExistRule : Rule {
    
    private var message : String
    
    public init(_ message : String = "E-mail existente") {
        self.message = message
    }
    
    
    public func validate(_ value: String) -> Bool {
        
        if WishareAPI.checkEmailExists(value) {
            return true
        }
    
        return false
        
    }
    
    
    public func errorMessage() -> String {
        return self.message
    }
    
}
