//
//  FilterSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SwiftyAttributes

class FilterSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var aplicarButton: UIButton!
    @IBOutlet weak var quantityProductsLabel: UILabel!
    @IBOutlet weak var clearBarButtonItem: UIBarButtonItem!
    
    var products : [Product] = []
    var result : [[String]] = []
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    let categories : [String] = ["Categorias", "Tamanho", "Cores", "Marca"]
    var previewOption : [String : Any] = ["text": "", "category": [], "size": [], "color": [], "brand": [], "store": [], "orderBy": UserDefaults.standard.object(forKey: "orderBy") as! String, "type": UserDefaults.standard.object(forKey: "typeBy") as! String]
    var option : [String] = []
    var items : [[String]] = []
    let defaults = UserDefaults.standard
    var categoriesName : [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        
        if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOption") as? [String : Any] {
            self.previewOption = lastPreviewOption
        }
        
        self.previewOption["text"] = defaults.object(forKey: "word_search") as! String
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateFilter(notification:)), name: Notification.Name.init("updateFilterShow"), object: nil)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.searchProductFilterBy(defaults.object(forKey: "word_search") as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], self.previewOption["brand"] as! [String], self.previewOption["store"] as! [String], UserDefaults.standard.object(forKey: "orderBy") as! String, UserDefaults.standard.object(forKey: "typeBy") as! String) { (categories : [String], size : [String], color : [String], brand : [String], products : [Product], tot : Int, categoriesName : [Category], error : Error?) in
            
            if error == nil {
                
                self.categoriesName = categoriesName
                
                if (self.previewOption["size"] as! [String]).count != 0 || (self.previewOption["color"] as! [String]).count != 0 || (self.previewOption["category"] as! [String]).count != 0 {
                    
                    WishareAPI.searchProductFilterBy(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], [], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, { ( categoriesFilter : [String], sizesFilter : [String], colorFilter : [String], brandFilter : [String], productsFilter : [Product], totFilter : Int, categoriesName : [Category], error : Error?) in
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            self.items.append(categoriesFilter)
                            self.items.append(sizesFilter)
                            self.items.append(color)
                            self.items.append(brandFilter)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brand)
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                        }
                        
                    })
                    
                } else {
                    
                    // - Caso so a loja seja selecionada, retorna as categorias, sizes e colors da pesquisa e a brandOriginal (Todas as lojas)
                    WishareAPI.searchProductFilterBy(self.defaults.object(forKey: "word_search") as! String, [], [], [], [], [], UserDefaults.standard.object(forKey: "orderBy") as! String, UserDefaults.standard.object(forKey: "typeBy") as! String, { ( _ : [String], _ : [String], colorOriginal : [String], brandOriginal : [String], _ : [Product], totOriginal : Int, categoriesName : [Category], error : Error?) in
                        
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            self.items.append(categories)
                            self.items.append(size)
                            self.items.append(color)
                            self.items.append(brandOriginal)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brandOriginal)
                            
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                        }
                        
                    })
                    
                }
                
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func updateFilter(notification : Notification) {
        
        let valueRequest = notification.object as! [String : Any]
        self.previewOption = valueRequest
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando", attributes: attributes)

        WishareAPI.searchProductFilterBy(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], self.previewOption["brand"] as! [String], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String) { (categories : [String], size : [String], color : [String], brand : [String], products : [Product], tot : Int, categoriesName : [Category], error : Error?) in
            
            if error == nil {
                
                
                if (self.previewOption["size"] as! [String]).count != 0 || (self.previewOption["color"] as! [String]).count != 0 || (self.previewOption["category"] as! [String]).count != 0 {
                    
                    WishareAPI.searchProductFilterBy(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], [], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, { ( categoriesFilter : [String], sizesFilter : [String], colorFilter : [String], brandFilter : [String], productsFilter : [Product], totFilter : Int, categoriesName : [Category], error : Error?) in
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            self.items.append(categoriesFilter)
                            self.items.append(sizesFilter)
                            self.items.append(color)
                            self.items.append(brandFilter)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brand)
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                        }
                        
                    })
                    
                } else {
                    
                    // - Caso so a loja seja selecionada, retorna as categorias, sizes e colors da pesquisa e a brandOriginal (Todas as lojas)
                    WishareAPI.searchProductFilterBy(self.defaults.object(forKey: "word_search") as! String, [], [], [], [], [], UserDefaults.standard.object(forKey: "orderBy") as! String, UserDefaults.standard.object(forKey: "typeBy") as! String, { ( _ : [String], _ : [String], colorOriginal : [String], brandOriginal : [String], _ : [Product], totOriginal : Int, categoriesName : [Category], error : Error?) in
                        
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            self.items.append(categories)
                            self.items.append(size)
                            self.items.append(color)
                            self.items.append(brandOriginal)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brandOriginal)
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                        }
                        
                    })
                    
                }
                
                
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "filterShowSegue" {
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            let showFilterSearchViewController = segue.destination as! ShowFilterSearchViewController
            showFilterSearchViewController.showValueItem = senderValue[0] as! [String]
            showFilterSearchViewController.previewOption = senderValue[1] as! [String : Any]
            showFilterSearchViewController.itemShow = senderValue[2] as! Int
            showFilterSearchViewController.categoriesName = senderValue[3] as! [Category]
            
        }
        
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clear(_ sender: UIBarButtonItem) {
        
        if !((self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0) {
        
            self.previewOption["category"] = []
            self.previewOption["size"] = []
            self.previewOption["color"] = []
            self.previewOption["brand"] = []
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando", attributes: attributes)
            
            WishareAPI.searchProductFilterBy(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], self.previewOption["brand"] as! [String], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String) { (categories : [String], size : [String], color : [String], brand : [String], products : [Product], tot : Int, categoriesName : [Category], error : Error?) in
                
                if error == nil {
                    
                    self.items.removeAll()
                    self.items.append(categories)
                    self.items.append(size)
                    self.items.append(color)
                    self.items.append(brand)
                    self.tableView.reloadData()
                    self.products = products
                    NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: self.products)
                    UserDefaults.standard.set(self.previewOption, forKey: "previewOption")
                    self.quantityProductsLabel.text = "\(tot)"
                    RappleActivityIndicatorView.stopAnimation()
                    
                    self.aplicarButton.backgroundColor = UIColor.lightGray
                    self.clearBarButtonItem.tintColor = UIColor.darkGray
                    self.aplicarButton.isEnabled = false
                    self.clearBarButtonItem.isEnabled = false
                    
            
                }
                
            }
            
            
        }
        
    }
    
    @IBAction func apply(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: self.products)
        UserDefaults.standard.set(self.previewOption, forKey: "previewOption")
        UserDefaults.standard.set(false, forKey: "isSearchTabs")
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

extension FilterSearchViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != 0 {
            self.performSegue(withIdentifier: "filterShowSegue", sender: [self.items[indexPath.row], self.previewOption, indexPath.row, self.categoriesName])
        } else {
            self.performSegue(withIdentifier: "filterShowSegue", sender: [self.items[indexPath.row], self.previewOption, indexPath.row, self.categoriesName])
        }
        
        
    }
    
}

extension FilterSearchViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.items.count > 0 {
            
            var optionSelected = "-1"
            
            if indexPath.row == 0 {
                optionSelected = "category"
            } else if indexPath.row == 1 {
                optionSelected = "size"
            } else if indexPath.row == 2 {
                optionSelected = "color"
            } else if indexPath.row == 3 {
                optionSelected = "brand"
            }
            
            // - Verifica se o resultado nao é sem color, tamanha ou category e marca
            if self.result[indexPath.row].count == 1 {
                if self.result[indexPath.row][0] == "" {
                    let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "subCell")
                    
                    cell.textLabel?.text = self.categories[indexPath.row]
                    cell.isHighlighted = false
                    cell.isUserInteractionEnabled = false
                    cell.isSelected = false
                    cell.textLabel?.textColor = UIColor.lightGray
                    cell.detailTextLabel?.textColor = UIColor.darkGray
                    cell.detailTextLabel?.text = "Opções indisponíveis"
                    
                    let lineView = UIView()
                    lineView.translatesAutoresizingMaskIntoConstraints = false
                    lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                    
                    cell.addSubview(lineView)
                    
                    lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
                    lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
                    lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
                    
                    
                    return cell
                }
                
            }
            
            
            if optionSelected != "-1" {
                
                if (self.previewOption[optionSelected] as! [String]).count > 0 {
                    
                    let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "subCell")
                    
                    cell.isHighlighted = false
                    cell.isUserInteractionEnabled = true
                    cell.isSelected = true
                    
                    cell.textLabel?.text = self.categories[indexPath.row]
                    cell.accessoryType = .disclosureIndicator
                    
                    var words = ""
                    var attributesWord = "".withTextColor(UIColor.darkGray)
                    
                    
                    for word in (self.previewOption[optionSelected] as! [String])  {
                        
                        if words.characters.count == 0 {
                            
                            if !self.result[indexPath.row].contains(word) {
                                
                                if indexPath.row == 0 {
                                    
                                    for category in self.categoriesName {
                                        if category.id! == word {
                                            words = "\(category.name!)"
                                            attributesWord = category.name!.withStrikethroughStyle(.styleSingle).withBaselineOffset(0)
                                        }
                                    }
                                    
                                } else {
                                    
                                    words = "\(word)"
                                    attributesWord = word.withStrikethroughStyle(.styleSingle).withBaselineOffset(0)
                                    
                                }
                                
                            } else {
                                
                                if indexPath.row == 0 {
                                    
                                    for category in self.categoriesName {
                                        if category.id! == word {
                                            words = "\(category.name!)"
                                            attributesWord = category.name!.withTextColor(UIColor.darkGray)
                                        }
                                    }
                                    
                                } else {
                                    
                                    words = "\(word)"
                                    attributesWord = word.withTextColor(UIColor.darkGray)
                                    
                                }
                                
                            }
                            
                            
                        } else {
                            
                            if !self.result[indexPath.row].contains(word) {
                                words = "\(words), \(word)"
                                attributesWord = attributesWord + ", ".withTextColor(UIColor.darkGray) + word.withStrikethroughStyle(.styleSingle).withBaselineOffset(0)
                            } else {
                                words = "\(words), \(word)"
                                attributesWord = attributesWord + ", ".withTextColor(UIColor.darkGray) + word.withTextColor(UIColor.darkGray)
                            }
                            
                            
                        }
                    }
                    
                    cell.detailTextLabel?.attributedText = attributesWord
                    
                    cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                    cell.detailTextLabel?.textColor = UIColor.darkGray
                    
                    
                    let lineView = UIView()
                    lineView.translatesAutoresizingMaskIntoConstraints = false
                    lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                    
                    cell.addSubview(lineView)
                    
                    lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
                    lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
                    lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
                    
                    
                    return cell
                }
                
            }


        }

        

        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.isHighlighted = false
        cell.isUserInteractionEnabled = true
        cell.isSelected = true
        
        cell.textLabel?.text = self.categories[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = UIColor.darkGray
        
        
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        
        cell.addSubview(lineView)
        
        lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
        lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
        lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        return cell
        
    }
    
}
