//
//  MyAddressEditableViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SkyFloatingLabelTextField

class MyAddressEditableViewController: UIViewController {
    
    @IBOutlet weak var cepTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var enderecoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var numeroTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var complementoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var bairroTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var cidadeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var estadoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tipoEnderecoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var destinatarioTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tipoEnderecoSegmented : UISegmentedControl!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addressButton: UIButton!
    
    var bottomConstraint : NSLayoutConstraint?
    var countTextField : Int = 3
    
    var address : [String : String] = [:]
    var otherAddress : [String : String] = [:]
    
    var primaryAddress : Bool = true
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.address.isEmpty {
            self.addressButton.isEnabled = false
            self.addressButton.alpha = 0.3
        }
        
        self.cepTextField.delegate = self
        self.numeroTextField.delegate = self
        self.tipoEnderecoTextField.delegate = self
        self.destinatarioTextField.delegate = self
        
        self.cepTextField.tag = 1
        self.numeroTextField.tag = 3
        self.tipoEnderecoTextField.tag = 7
        self.destinatarioTextField.tag = 8
    
        self.addressButton.layer.cornerRadius = 5.0
        self.addressButton.layer.masksToBounds = false
        self.addressButton.layer.shadowColor = UIColor.black.cgColor
        self.addressButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.addressButton.layer.shadowRadius = 2.0
        self.addressButton.layer.shadowOpacity = 0.24
        
        self.tipoEnderecoSegmented.isHidden = true
        
        bottomConstraint = NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardNotification), name: .UIKeyboardWillHide, object: nil)
        
        if address.count > 0 {
            
            self.cepTextField.text = address["post_code"]
            self.enderecoTextField.text = address["address"]
            self.numeroTextField.text = address["number"]
            self.complementoTextField.text = address["more"]
            self.bairroTextField.text = address["neighborhood"]
            self.cidadeTextField.text = address["city"]
            self.estadoTextField.text = address["state"]
            self.tipoEnderecoTextField.text = address["name"]
            self.destinatarioTextField.text = address["recipient_name"]
            
            self.tipoEnderecoSegmented.isEnabled = false
            
            if address["type"] == "S" {
                self.tipoEnderecoSegmented.selectedSegmentIndex = 0
            } else {
                self.tipoEnderecoSegmented.selectedSegmentIndex = 1
            }
            
            self.addressButton.setTitle("Editar Endereço", for: .normal)
            self.navigationItem.title = "Editar Endereço"

        } else {
            if self.otherAddress.count == 0 {
                self.tipoEnderecoSegmented.selectedSegmentIndex = 0
                self.addressButton.setTitle("Continuar", for: .normal)
                self.navigationItem.title = "Novo Endereço"
            } else {
                self.tipoEnderecoSegmented.selectedSegmentIndex = 0
                self.addressButton.setTitle("Continuar", for: .normal)
                self.navigationItem.title = "Novo Endereço Cobrança"
            }
        }
        
        WishareCoreData.getFirstUser { (userCore : UserActive?, error : Error?) in
            if error == nil {
                self.destinatarioTextField.text = "\(userCore!.name!) \(userCore!.last_name!)"
            }
        }
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getAddresses { ( addresses : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                if addresses.count >= 2 {
                    self.primaryAddress = false
                }
                
                RappleActivityIndicatorView.stopAnimation()
            }
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveOrEditAddressButton(_ sender: UIButton) {
        
        var countError = 0
        
        if (self.cepTextField.text?.isEmpty)! {
            self.cepTextField.errorMessage = "C.E.P. Obrigatorio"
            countError += 1
        } else {
            self.cepTextField.errorMessage = nil
        }
        
        if (self.enderecoTextField.text?.isEmpty)! {
            self.enderecoTextField.errorMessage = "Endereço Obrigatorio"
            countError += 1
        } else {
            self.enderecoTextField.errorMessage = nil
        }
        
        if (self.numeroTextField.text?.isEmpty)! {
            self.numeroTextField.errorMessage = "Número Obrigatorio"
            countError += 1
        } else {
            self.numeroTextField.errorMessage = nil
        }
        
        if (self.bairroTextField.text?.isEmpty)! {
            self.bairroTextField.errorMessage = "Bairro Obrigatorio"
            countError += 1
        } else {
            self.bairroTextField.errorMessage = nil
        }
        
        if (self.cidadeTextField.text?.isEmpty)! {
            self.cidadeTextField.errorMessage = "Cidade Obrigatorio"
            countError += 1
        } else {
            self.cidadeTextField.errorMessage = nil
        }
        
        if (self.estadoTextField.text?.isEmpty)! {
            self.estadoTextField.errorMessage = "Estado Obrigatorio"
            countError += 1
        } else {
            self.estadoTextField.errorMessage = nil
        }
        
        if (self.tipoEnderecoTextField.text?.isEmpty)! {
            self.tipoEnderecoTextField.errorMessage = "Tipo Endereço Obrigatorio"
            countError += 1
        } else {
            self.tipoEnderecoTextField.errorMessage = nil
        }
        
        if (self.destinatarioTextField.text?.isEmpty)! {
            self.destinatarioTextField.errorMessage = "Destinatário Obrigatorio"
            countError += 1
        } else {
            self.destinatarioTextField.errorMessage = nil
        }

        
        
        if countError == 0 {
            
            if self.address.count > 0 {
                
                // ===================================
                // ======== EDITANDO ENDERECO ========
                // ===================================
                
                RappleActivityIndicatorView.startAnimatingWithLabel("Editando...", attributes: attributes)
                
                WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: "", city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: self.address["type"]!, neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, numberOfTime: nil, newAddress: false, idAddress: self.address["id"]!, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        RappleActivityIndicatorView.stopAnimation()
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                })
                
                // ===================================
                // ===================================
                // ===================================
                
                
            } else {
                
            
                if self.otherAddress.count == 0 {
                    
                    if self.primaryAddress == true {
                        
                        // ==================================================
                        // ======= CADASTRADANDO PRIMEIROS ENDERECOS ========
                        // ==================================================
                        
                        let alertController = UIAlertController(title: "Este endereço é o mesmo endereço de cobrança?", message: nil, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: "Sim", style: .default, handler: { ( _ ) in
                            
                            RappleActivityIndicatorView.startAnimatingWithLabel("Salvando...", attributes: self.attributes)
                            
                            WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: "", city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "S", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                                
                                if error == nil {
                                    
                                    WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: "", city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "P", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                                        
                                        if error == nil {
                                            
                                            RappleActivityIndicatorView.stopAnimation()
                                            self.navigationController?.popViewController(animated: true)
                                            
                                        }
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let noAction = UIAlertAction(title: "Não", style: .default, handler: { ( _ ) in
                            
                            let other : [String : String] = [
                                "post_code" : self.cepTextField.text!,
                                "address" : self.enderecoTextField.text!,
                                "number" : self.numeroTextField.text!,
                                "more" : self.complementoTextField.text!,
                                "neighborhood" : self.bairroTextField.text!,
                                "city" : self.cidadeTextField.text!,
                                "state" : self.estadoTextField.text!,
                                "name" : self.tipoEnderecoTextField.text!,
                                "recipent_name" : self.destinatarioTextField.text!,
                                ]
                            
                            
                            let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                            let myAddressEditableViewController = storyboard.instantiateViewController(withIdentifier: "myAddressEditableID") as! MyAddressEditableViewController
                            myAddressEditableViewController.otherAddress = other
                            
                            self.navigationController?.pushViewController(myAddressEditableViewController, animated: true)
                            
                            
                        })
                        
                        alertController.addAction(yesAction)
                        alertController.addAction(noAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        // ==================================================
                        // ==================================================
                        // ==================================================
                        
                        
                    } else {
                        
                        // ===================================================================
                        // ======= REDIRECIONAR PARA SELECAO DE ENDERECOS DE COBRANCAS =======
                        // ===================================================================
                        
                        
                        let alertController = UIAlertController(title: "Este endereço é o mesmo endereço de cobrança?", message: nil, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: "Sim", style: .default, handler: { ( _ ) in
                            
                            RappleActivityIndicatorView.startAnimatingWithLabel("Salvando...", attributes: self.attributes)
                            
                            WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: "", city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "S", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                                
                                if error == nil {
                                    
                                    WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: "", city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "P", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                                        
                                        if error == nil {
                                            
                                            RappleActivityIndicatorView.stopAnimation()
                                            
                                            var containsAddressOrderViewController : Bool = false
                                            
                                            for viewController in self.navigationController!.viewControllers {
                                                if viewController is AddressOrderViewController {
                                                    containsAddressOrderViewController = true
                                                }
                                            }
                                            
                                            if containsAddressOrderViewController == true {
                                                
                                                let addressSelected : [String : String] = [
                                                    "post_code" : self.cepTextField.text!,
                                                    "address" : self.enderecoTextField.text!,
                                                    "number" : self.numeroTextField.text!,
                                                    "more" : self.complementoTextField.text!,
                                                    "neighborhood" : self.bairroTextField.text!,
                                                    "city" : self.cidadeTextField.text!,
                                                    "state" : self.estadoTextField.text!,
                                                    "name" : self.tipoEnderecoTextField.text!,
                                                    "recipent_name" : self.destinatarioTextField.text!,
                                                    ]
                                                
                                                for viewController in self.navigationController!.viewControllers {
                                                    if viewController is AddressOrderViewController {
                                                        let addressOrderViewController = viewController as! AddressOrderViewController
                                                        addressOrderViewController.myAddressPaymentSelected = [addressSelected]
                                                        addressOrderViewController.myAddressShippingSelected = [addressSelected]
                                                        self.navigationController?.popToViewController(addressOrderViewController, animated: true)
                                                    }
                                                }
                                                
                                            } else {
                                                self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let noAction = UIAlertAction(title: "Não", style: .default, handler: { ( _ ) in
                            
                            let other : [String : String] = [
                                "post_code" : self.cepTextField.text!,
                                "address" : self.enderecoTextField.text!,
                                "number" : self.numeroTextField.text!,
                                "more" : self.complementoTextField.text!,
                                "neighborhood" : self.bairroTextField.text!,
                                "city" : self.cidadeTextField.text!,
                                "state" : self.estadoTextField.text!,
                                "name" : self.tipoEnderecoTextField.text!,
                                "recipent_name" : self.destinatarioTextField.text!,
                                ]
                            
                            
                            let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                            let myAddressViewController = storyboard.instantiateViewController(withIdentifier: "myAddressID") as! MyAddressViewController
                            myAddressViewController.selectedItem = true
                            myAddressViewController.isPayment = true
                            myAddressViewController.otherAddress = other
                            
                            self.navigationController?.pushViewController(myAddressViewController, animated: true)
                            
                            
                        })
                        
                        alertController.addAction(yesAction)
                        alertController.addAction(noAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                
                        
                        // ===================================================================
                        // ===================================================================
                        // ===================================================================
                        
                    }
                    
                } else {
                    
                    // ==================================================================
                    // ========  CADASTRANDO 2 ENDERECOS ( ENTREGA E COBRANCA ) =========
                    // ==================================================================
                    
                    RappleActivityIndicatorView.startAnimatingWithLabel("Salvando...", attributes: attributes)
                    
                    WishareAPI.addresses(name: self.otherAddress["name"]!, postCode: self.otherAddress["post_code"]!, address: self.otherAddress["address"]!, number: self.otherAddress["number"]!, more: self.otherAddress["more"]!, city: self.otherAddress["city"]!, state: self.otherAddress["state"]!, country: "Brasil", type: "S", neighborhood: self.otherAddress["neighborhood"]!, recipientName: self.otherAddress["recipent_name"]!, { (validate : Bool, error : Error?) in
                        
                        if error == nil {
                            
                            WishareAPI.addresses(name: self.tipoEnderecoTextField.text!, postCode: self.cepTextField.text!, address: self.enderecoTextField.text!, number: self.numeroTextField.text!, more: "", city: self.cidadeTextField.text!, state: self.estadoTextField.text!, country: "Brasil", type: "P", neighborhood: self.bairroTextField.text!, recipientName: self.destinatarioTextField.text!, { (validate : Bool, error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    var containsAddressOrderViewController : Bool = false
                                    
                                    for viewController in self.navigationController!.viewControllers {
                                        if viewController is AddressOrderViewController {
                                            containsAddressOrderViewController = true
                                        }
                                    }
                                    
                                    if containsAddressOrderViewController == true {
                                        
                                        let addressSelected : [String : String] = [
                                            "post_code" : self.cepTextField.text!,
                                            "address" : self.enderecoTextField.text!,
                                            "number" : self.numeroTextField.text!,
                                            "more" : self.complementoTextField.text!,
                                            "neighborhood" : self.bairroTextField.text!,
                                            "city" : self.cidadeTextField.text!,
                                            "state" : self.estadoTextField.text!,
                                            "name" : self.tipoEnderecoTextField.text!,
                                            "recipent_name" : self.destinatarioTextField.text!,
                                            ]
                                        
                                        for viewController in self.navigationController!.viewControllers {
                                            if viewController is AddressOrderViewController {
                                                let addressOrderViewController = viewController as! AddressOrderViewController
                                                addressOrderViewController.myAddressPaymentSelected = [addressSelected]
                                                addressOrderViewController.myAddressShippingSelected = [self.otherAddress]
                                                self.navigationController?.popToViewController(addressOrderViewController, animated: true)
                                            }
                                        }
                                        
                                    } else {
                                        
                                        for viewController in self.navigationController!.viewControllers {
                                            if viewController is MyAddressViewController {
                                                let myAddressViewController = viewController as! MyAddressViewController
                                                self.navigationController?.popToViewController(myAddressViewController, animated: true)
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                
                            })
                            
                        }
                        
                    })
                    
                    // ==================================================================
                    // ==================================================================
                    // ==================================================================
                    
                    
                }
                
            }
            
        }
        
    }
    
    @objc func handleKeyBoardNotification(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                
                //let indexPath = IndexPath(item: (self.coments.count != 0) ? self.coments.count - 1 : 0, section: 0)
                //self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
            })
            
        }
        
    }
    
    @IBAction func findCEP(_ sender: Any) {
        
        let wk = WSWKViewController()
        
        let request = URLRequest(url: URL(string: "http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm")!)
        
        wk.wkWebView.load(request)
        
        self.navigationController?.pushViewController(wk, animated: true)
        
    }
    
    

}

extension MyAddressEditableViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 1 {
          
            RappleActivityIndicatorView.startAnimatingWithLabel("Buscando CEP...", attributes: attributes)
            
            WishareAPI.getAddressByCep(cep: textField.text!) { (address : [String : AnyObject], error : Error?) in
                
                if error == nil {
                    
                    if let _ = address["message"] as? String {
                        
                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "C.E.P. não encontrado!", completionTimeout: 1)
                        
                    } else {
                        
                        self.bairroTextField.text = address["bairro"] as? String
                        self.cidadeTextField.text = address["cidade"] as? String
                        self.enderecoTextField.text = address["end"] as? String
                        self.estadoTextField.text = address["uf"] as? String
                        
                        if (self.cepTextField.text?.isEmpty)! {
                            
                            self.cepTextField.errorMessage = "C.E.P. Obrigatorio"
                            
                        } else {
                            
                            self.cepTextField.errorMessage = nil
                            
                        }
                        
                        if (self.enderecoTextField.text?.isEmpty)! {
                            
                            self.enderecoTextField.errorMessage = "Endereço Obrigatorio"
                            
                        } else {
                            
                            self.enderecoTextField.errorMessage = nil
                            
                        }
                        
                        if (self.numeroTextField.text?.isEmpty)! {
                            
                            self.numeroTextField.errorMessage = "Número Obrigatorio"
                            
                        } else {
                            
                            self.numeroTextField.errorMessage = nil
                            
                        }
                        
                        if (self.bairroTextField.text?.isEmpty)! {
                            
                            self.bairroTextField.errorMessage = "Bairro Obrigatorio"
                            
                        } else {
                            
                            self.bairroTextField.errorMessage = nil
                            
                        }
                        
                        if (self.cidadeTextField.text?.isEmpty)! {
                            
                            self.cidadeTextField.errorMessage = "Cidade Obrigatorio"
                            
                        } else {
                            
                            self.cidadeTextField.errorMessage = nil
                            
                        }
                        
                        if (self.estadoTextField.text?.isEmpty)! {
                            
                            self.estadoTextField.errorMessage = "Estado Obrigatorio"
                            
                        } else {
                            
                            self.estadoTextField.errorMessage = nil
                            
                        }
                        
                        if (self.tipoEnderecoTextField.text?.isEmpty)! {
                            
                            self.tipoEnderecoTextField.errorMessage = "Tipo Endereço Obrigatorio"
                            
                        } else {
                            
                            self.tipoEnderecoTextField.errorMessage = nil
                            
                        }
                        
                        if (self.destinatarioTextField.text?.isEmpty)! {
                            
                            self.destinatarioTextField.errorMessage = "Destinatário Obrigatorio"
                            
                        } else {
                            
                            self.destinatarioTextField.errorMessage = nil
                            
                        }
                        
                        RappleActivityIndicatorView.stopAnimation()

                        
                    }
                    
                    
                } else {
                    
                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: "C.E.P. não encontrado!", completionTimeout: 1)
                    
                }
                
            }
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 1 {
        
            if !(textField.text?.isEmpty)! || string.trimmingCharacters(in: .whitespaces) != "" {
                if textField.accessibilityHint == nil {
                    textField.accessibilityHint = "\(textField.tag)"
                    self.countTextField -= 1
                }
            }
            
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
            
        } else if textField.tag == 3 {
            if !(textField.text?.isEmpty)! || string.trimmingCharacters(in: .whitespaces) != "" {
                if textField.accessibilityHint == nil {
                    textField.accessibilityHint = "\(textField.tag)"
                    self.countTextField -= 1
                }
            }
        } else if textField.tag == 7 {
            if !(textField.text?.isEmpty)! || string.trimmingCharacters(in: .whitespaces) != "" {
                if textField.accessibilityHint == nil {
                    textField.accessibilityHint = "\(textField.tag)"
                    self.countTextField -= 1
                }
            }
        }
        
        
        if self.countTextField == 0 {
            self.addressButton.isEnabled = true
            self.addressButton.alpha = 1.0
        }
        
        return true
        
    }
    
}

