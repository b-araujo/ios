//
//  User.swift
//  Wishare
//
//  Created by Wishare iMac on 4/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class User : NSObject, NSCoding {
    
    var blogger : Blogger?
    var active : String?
    var birthday_date : Int64?
    var blogger_id : Int?
    var bloggers : Int?
    var cell : String?
    var cpf : String?
    var created : String?
    var email : String?
    var email_password : String?
    var faccess : String?
    
    var friends : Int?
    var gender : String?
    var id : String?
    var id_face : String?
    var id_face_password : String?
    var last_name : String?
    var modified : String?
    var name : String?
    var optin : String?
    var phone : String?
    var phone2 : String?
    var picture : String?
    var privateW : String?
    var profile_image : ProfileImage?
    var isProfileImageLocal: Bool {
        get {
            if self.profile_image?.type == "local" {
                return true
            } else {
                return false
            }
        }
        set {
            
        }
    }
    var rg : String?
    var state : String?
    var stores : Int?
    var tokenhash : String?
    var username : String?
    var password : String?
    var isSocial : Bool?
    var following : Bool?
    var isStore : Bool?
    var isFollowing : String?
    
    var birthday : String?
    
    var qtd_followers : String?
    var qtd_following : String?
    var followers : Int?
    var qtd_wips : String?
    var qtd_stores : String?
    
    var qtd_posts : String?
    var qtd_orders : String?
    var qtd_wishes_actions : String?
    var qtd_wishes_products : String?
    
    init( _ user : AnyObject) {
        
        let userResponse : [String : AnyObject] = user["user"] as! [String : AnyObject]
        
        let blogger : Blogger = Blogger()
        let userBlogger : [String : AnyObject] = userResponse["Blogger"] as! [String : AnyObject]
        
        blogger.teenPosts = userBlogger["10posts"] as? String
        blogger.onePost = userBlogger["1post"] as? String
        blogger.threePost = userBlogger["3posts"] as? String
        blogger.fivePost = userBlogger["5posts"] as? String
        blogger.eightPost = userBlogger["8posts"] as? String
        blogger.active = userBlogger["active"] as? String
        blogger.created = userBlogger["created"] as? String
        blogger.email = userBlogger["email"] as? String
        blogger.id = userBlogger["id"] as? String
        blogger.level = userBlogger["level"] as? String
        blogger.modified = userBlogger["modified"] as? String
        blogger.name = userBlogger["name"] as? String
        blogger.password = userBlogger["password"] as? String
        blogger.picture = userBlogger["picture"] as? String
        blogger.profile = userBlogger["profile"] as? String
        
        
        let profileImage : ProfileImage = ProfileImage()
        let userProfileImage : [String : AnyObject] = userResponse["profile_image"] as! [String : AnyObject]
        
        profileImage.type = (userProfileImage["type"] as? String)?.lowercased()
        profileImage.url = userProfileImage["url"] as? String
        
        
        self.blogger = blogger
        self.active = userResponse["active"] as? String
        self.birthday_date = userResponse["birthday_date"] as? Int64
        self.blogger_id = userResponse["blogger_id"] as? Int
        self.bloggers = userResponse["bloggers"] as? Int
        self.cell = userResponse["cell"] as? String
        self.cpf = userResponse["cpf"] as? String
        self.created = userResponse["created"] as? String
        self.email = userResponse["email"] as? String
        self.email_password = userResponse["email_password"] as? String
        self.faccess = userResponse["faccess"] as? String
        self.followers = userResponse["followers"] as? Int
        self.friends = userResponse["friends"] as? Int
        self.gender = userResponse["gender"] as? String
        self.id = userResponse["id"] as? String
        self.id_face = userResponse["id_face"] as? String
        self.id_face_password = userResponse["id_face_password"] as? String
        self.last_name = userResponse["last_name"] as? String
        self.modified = userResponse["modified"] as? String
        self.name = userResponse["name"] as? String
        self.optin = userResponse["optin"] as? String
        self.phone = userResponse["phone"] as? String
        self.phone2 = userResponse["phone2"] as? String
        self.picture = userResponse["picture"] as? String
        self.privateW = userResponse["private"] as? String
        self.profile_image = profileImage
        self.rg = userResponse["rg"] as? String
        self.state = userResponse["state"] as? String
        self.stores = userResponse["stores"] as? Int
        self.tokenhash = userResponse["tokenhash"] as? String
        self.username = userResponse["username"] as? String
        
    }
    
    
    // Metodo usado nos posts
    init( postUser user : [String : AnyObject]) {
        
        self.id = user["id"] as? String
        self.username = user["username"] as? String
        self.picture = user["picture"] as? String
        
    }
    
    // Metodo usado nos comentarios
    init( commentUser user : [String : AnyObject]) {
        
        self.id = user["id"] as? String
        self.username = user["username"] as? String
        self.picture = user["picture"] as? String
        
        
    }
    
    
    // Metodo usado nos Like User
    init( likesUser user : [String : AnyObject]) {
        
        let userAPI = user["User"] as? [String : AnyObject]
        
        self.username = userAPI!["username"] as? String
        self.picture = userAPI!["picture"] as? String
        self.following = (userAPI!["following"] as? String == "true") ? true : false
        self.id = userAPI!["id"] as? String
        
        self.name = userAPI!["name"] as? String
        self.last_name = userAPI!["last_name"] as? String
        self.rg = userAPI!["rg"] as? String
        self.cpf = userAPI!["cpf"] as? String
        self.phone = userAPI!["phone"] as? String
        self.cell = userAPI!["cell"] as? String
        self.email = userAPI!["email"] as? String
        self.optin = userAPI!["optin"] as? String
        
        self.isStore = false
        
    }
    
    init( likesUserStore userStore : [String : AnyObject]) {
        
        let userAPI = userStore["Store"] as! [String : AnyObject]
        self.username = userAPI["name"] as? String
        self.picture = userAPI["picture"] as? String
        self.following = true
        self.id = userAPI["id"] as? String
        self.isStore = true
        
    }
    
    init( byCategory user : [String : AnyObject] ) {
        
        self.name = user["name"] as? String
        self.username = user["username"] as? String
        self.picture = user["picture"] as? String
        self.following = (user["following"] as? String == "true") ? true : false
        self.id = user["id"] as? String
        if let profileImageDict: [String : AnyObject] = user["profile_image"] as? [String : AnyObject] {
            let profileImage: ProfileImage = ProfileImage()
            
            profileImage.type = (profileImageDict["type"] as? String)?.lowercased()
            profileImage.url = profileImageDict["url"] as? String
            self.profile_image = profileImage
        }
    }
    
    init( byNotification user : [String : AnyObject] ) {
        
        self.name = user["name"] as? String
        self.username = user["username"] as? String
        self.picture = user["picture"] as? String
        self.following = (user["ifollow"] as? String == "true") ? true : false
        self.isFollowing = user["ifollow"] as? String
        self.privateW = user["private"] as? String
        self.id = user["id"] as? String
        
    }
    
    init( bySearch user : [String : AnyObject] ) {
        
        let userAPI : [String : AnyObject] = user["User"] as! [String : AnyObject]
        
        self.username = userAPI["username"] as? String
        self.picture = userAPI["picture"] as? String
        self.following = (userAPI["following"] as? String == "true") ? true : false
        self.id = userAPI["id"] as? String
        
    }
    
    init( byUser user : [String : AnyObject] ) {
        
        let userAPI : [String : AnyObject] = user["User"] as! [String : AnyObject]
        
        self.id = userAPI["id"] as? String
        self.active = userAPI["active"] as? String
        self.birthday = userAPI["birthday_date"] as? String
        self.cell = userAPI["cell"] as? String
        self.cpf = userAPI["cpf"] as? String
        self.created = userAPI["created"] as? String
        self.email = userAPI["email"] as? String
        self.email_password = userAPI["email_password"] as? String
        self.followers = userAPI["followers"] as? Int
        self.friends = userAPI["friends"] as? Int
        self.gender = userAPI["gender"] as? String
        self.last_name = userAPI["last_name"] as? String

        self.name = userAPI["name"] as? String
        self.phone = userAPI["phone"] as? String
        self.picture = userAPI["picture"] as? String
        self.privateW = userAPI["private"] as? String
        self.rg = userAPI["rg"] as? String
        self.state = userAPI["state"] as? String
        self.stores = userAPI["stores"] as? Int
        self.tokenhash = userAPI["tokenhash"] as? String
        self.username = userAPI["username"] as? String
        
        self.qtd_following = userAPI["qtd_following"] as? String
        self.qtd_wips = userAPI["qtd_wips"] as? String
        self.qtd_stores = userAPI["stores"] as? String
        self.qtd_followers = userAPI["followers"] as? String
        
        self.qtd_posts = userAPI["qtd_posts"] as? String
        self.qtd_orders = userAPI["qtd_orders"] as? String
        self.qtd_wishes_actions = userAPI["qtd_wishes_actions"] as? String
        self.qtd_wishes_products = userAPI["qtd_wishes_products"] as? String
        
        let profileImage : ProfileImage = ProfileImage()
        let userProfileImage : [String : AnyObject] = userAPI["profile_image"] as! [String : AnyObject]
        
        profileImage.type = (userProfileImage["type"] as? String)?.lowercased()
        profileImage.url = userProfileImage["url"] as? String
        profileImage.url_local = userProfileImage["url"] as? Int
        
        self.profile_image = profileImage
        self.following = (userAPI["following"] as? String == "true") ? true : false
        self.isFollowing = userAPI["following"] as? String
    }
    
    
    init( user: String?, picture : String? ) {
        
        self.username = user
        self.picture = picture
        
    }
    
    
    init(id : String, username : String, picture : String) {
        
        self.id = id
        self.username = username
        self.picture = picture
        
    }
    
    init(forProfileWithId id: String, username: String, picture: String, profileBackground: [String:AnyObject], isPrivate: String ) {
        self.id = id
        self.username = username
        self.picture = picture
        
        let profileImage : ProfileImage = ProfileImage()
        profileImage.type = (profileBackground["type"] as? String)?.lowercased()
        profileImage.url = profileBackground["url"] as? String
        profileImage.url_local = profileBackground["url"] as? Int
        self.profile_image = profileImage
        
        self.privateW = isPrivate
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let username = aDecoder.decodeObject(forKey: "username") as! String
        let picture = aDecoder.decodeObject(forKey: "picture") as! String
        self.init(id: id, username: username, picture: picture)
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(picture, forKey: "picture")
        
    }
    
    
    override init() {
        
    }
    
//    func isProfileImageLocal() -> Bool{
//        if self.profile_image?.type == "local" {
//            return true
//        } else {
//            return false
//        }
//    }
    
}
