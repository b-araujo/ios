//
//  MyCardViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/10/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD

class MyCardViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var registerCardButton: UIButton!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var selectedItem : Bool = false
    var mySelectedCard : [[String : String]] = []
    
    var myCards : [[String : String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCardButton.layer.cornerRadius = 5.0
        self.registerCardButton.layer.masksToBounds = false
        self.registerCardButton.layer.shadowColor = UIColor.black.cgColor
        self.registerCardButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.registerCardButton.layer.shadowRadius = 2.0
        self.registerCardButton.layer.shadowOpacity = 0.24
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "addressCell")
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getCards { (cards : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                self.myCards.removeAll()
                
                for card in cards {
                    
                    if let myCard : [String : String] = card["UsersCard"] as? [String : String] {
                        self.myCards.append(myCard)
                    }
                    
                }
                
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
            }
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getCards { (cards : [[String : AnyObject]], error : Error?) in
            if error == nil {
                
                self.myCards.removeAll()
                
                for card in cards {
                    
                    if let myCard : [String : String] = card["UsersCard"] as? [String : String] {
                        self.myCards.append(myCard)
                    }
                    
                }

                self.registerCardButton.isHidden = true
                self.tableView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
                if self.myCards.count == 0 {
                    
                    let alertController = UIAlertController(title: "Nenhum cartão cadastrado. Gostaria de cadastrar um novo cartão?", message: nil, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                        self.performSegue(withIdentifier: "registerNewCardSegue", sender: nil)
                    })
                    let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: { ( _ ) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    self.registerCardButton.isHidden = false
                }
                
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func registerNewCard ( _ sender : UIButton) {
        
        if self.selectedItem == true {
            if self.mySelectedCard.count > 0 {
                
                for viewController in self.navigationController!.viewControllers {
                    if viewController is CardOrderViewController {
                        let cardOrderViewController = viewController as! CardOrderViewController
                        cardOrderViewController.myCardSelected = self.mySelectedCard
                        self.navigationController?.popToViewController(cardOrderViewController, animated: true)
                    }
                }
                
            } else {
                self.performSegue(withIdentifier: "registerNewCardSegue", sender: nil)
            }
        } else {
            self.performSegue(withIdentifier: "registerNewCardSegue", sender: nil)
        }
            
    }

}


extension MyCardViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Excluir") { ( _ , indexPath : IndexPath) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: self.attributes)
            
            WishareAPI.removeCard(idCard: self.myCards[indexPath.row]["id"]!, { (error : Error?) in
                if error == nil {
                    
                    if self.mySelectedCard.count > 0 {
                      
                        if self.mySelectedCard[0]["id"] == self.myCards[indexPath.row]["id"] {
                            self.mySelectedCard.removeAll()
                        }
                        
                    }
                    
                    self.myCards.remove(at: indexPath.row)
                    self.tableView.reloadData()
                    RappleActivityIndicatorView.stopAnimation()
                
                }
            })
            
        }
        
        return [deleteAction]
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedItem == true {
            
            if self.mySelectedCard.count > 0 {
                if self.mySelectedCard[0]["id"] != self.myCards[indexPath.row]["id"] {
                    
                    self.mySelectedCard.removeAll()
                    self.mySelectedCard.append(self.myCards[indexPath.row])
                    
                } else {
                    
                    tableView.reloadData()
                    self.mySelectedCard.removeAll()
                    self.registerCardButton.setTitle("Registrar Novo Cartão de Crédito", for: .normal)
                }
                
                
            } else {
                
                self.mySelectedCard.append(self.myCards[indexPath.row])
                self.registerCardButton.setTitle("Usar cartão selecionado", for: .normal)
                
            }
        
        }
        
    }
    

    
}

extension MyCardViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myCards.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Cartão de Crédito"
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! AddressTableViewCell
        
        cell.accessoryType = .none
        cell.typeImageView.image = UIImage(named: "icons8-bank_card_back_side_filled")
        cell.nameAddressLabel.text = self.myCards[indexPath.row]["flag"]?.uppercased()
        cell.addressLabel.text = "XXXX - XXXX - XXXX - \((self.myCards[indexPath.row]["number"]! as NSString).substring(from: 12))"
        
        return cell
        
    }
    
}
