//
//  PreviewRatingCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/21/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import FloatRatingView

class PreviewRatingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var mediumRateLabel: UILabel!
    @IBOutlet weak var mediumRateView: FloatRatingView!
    @IBOutlet weak var totalEvaluationLabel: UILabel!
    
    @IBOutlet weak var fiveRateLabel: UILabel!
    @IBOutlet weak var fourRateLabel: UILabel!
    @IBOutlet weak var threeRateLabel: UILabel!
    @IBOutlet weak var twoRateLabel: UILabel!
    @IBOutlet weak var oneRateLabel: UILabel!
    
    @IBOutlet weak var fiveBaseRateView: UIView!
    @IBOutlet weak var fourBaseRateView: UIView!
    @IBOutlet weak var threeBaseRateView: UIView!
    @IBOutlet weak var twoBaseRateView: UIView!
    @IBOutlet weak var oneBaseRateView: UIView!
    
    @IBOutlet weak var ratingProductButton: UIButton!
    
    var delegate : PreviewRatingDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func pressRatingProduct(_ sender: UIButton) {
        self.delegate?.ratingProduct(sender)
    }
    
    @IBAction func orderBy(_ sender: UIButton) {
        self.delegate?.orderBy(sender)
    }

}

protocol PreviewRatingDelegate {
    func ratingProduct( _ sender : UIButton)
    func orderBy(_ sender : UIButton)
}
