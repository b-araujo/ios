//
//  ShareStoreLookCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/2/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class ShareStoreLookCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var userThumbImageView: UIImageView!
    @IBOutlet weak var userNameTopButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var moreImageView: UIImageView!
    
    
    @IBOutlet weak var storeShareThumbImageView: UIImageView!
    @IBOutlet weak var storeNameShareButton: UIButton!
    @IBOutlet weak var buyStore: UIButton!
    
    @IBOutlet weak var shareStorePostImageView: UIImageView!
    @IBOutlet weak var shareCommentLabel: ActiveLabel!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeSharePostUserImageView: UIImageView!
    @IBOutlet weak var commentSharePostUserImageView: UIImageView!
    @IBOutlet weak var shareSharePostUserImageView: UIImageView!
    @IBOutlet weak var wishListSharePostUserImageView: UIImageView!
    
    @IBOutlet weak var viewLikes: UIView!
    
    @IBOutlet weak var userNameBottomButton: UIButton!
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var commentStoreLabel: ActiveLabel!
    @IBOutlet weak var commentsStoreLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    var delegateBase : WishareBasePostDelegate!
    var delegate : ShareStoreLookCollectionViewCellDelegate!
    var indexPathNow : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.shareView.layer.cornerRadius = 5
        self.shareView.layer.borderWidth = 1
        self.shareView.layer.borderColor = UIColor.darkGray.cgColor
        
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
        
        self.collectionView.backgroundView = nil
        self.collectionView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        
        // ================================================================
        
        
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerDreamImageView = UITapGestureRecognizer(target: self, action: #selector(tapGift))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapLikesView))
        let tapGestureRecognizerCommentShowLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        
        
        let tapStorePostGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPostStoreImageView))
        self.shareStorePostImageView.addGestureRecognizer(tapStorePostGestureRecognizer)
        
        
        self.likeSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerCommentImageView)
        self.shareSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        self.wishListSharePostUserImageView.addGestureRecognizer(tapGestureRecognizerDreamImageView)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        self.commentsStoreLabel.addGestureRecognizer(tapGestureRecognizerCommentShowLabel)
        
        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMore))
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        
        let tapGestureRecognizerStoreShareThumbImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreShareThumb))
        let tapGestureRecognizerStoreNameShareButton = UITapGestureRecognizer(target: self, action: #selector(tapNameShare))
        
        self.storeShareThumbImageView.addGestureRecognizer(tapGestureRecognizerStoreShareThumbImageView)
        self.storeNameShareButton.addGestureRecognizer(tapGestureRecognizerStoreNameShareButton)
        
        // =================================================
        
        self.userNameTopButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        self.userNameBottomButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        
        let tapGestureRecognizerUserThumbImageVieww = UITapGestureRecognizer(target: self, action: #selector(openUserImage))
        self.userThumbImageView.addGestureRecognizer(tapGestureRecognizerUserThumbImageVieww)
        
    }
    
    
    @objc func tapMore() {
        self.delegateBase.more(cell: self)
    }
    
    @objc func tapPostStoreImageView () {
        self.delegate.buyShareLookStoreImage(indexPathNow)
    }
    
    @objc func tapLike() {
        self.delegateBase.like(cell: self)
    }
    
    @objc func tapComment() {
        self.delegateBase.showComments(cell: self)
    }
    
    @objc func tapShare() {
        self.delegateBase.share(cell: self, indexPath: indexPathNow)
    }
    
    @objc func tapGift() {
        self.delegateBase.wishlist(cell: self)
    }
    
    @objc func tapLikesView() {
        self.delegateBase.showLikes(cell: self)
    }
    
    @objc func tapStoreShareThumb () {
        self.delegateBase.thumbStorePostImageView(cell: self)
    }
    
    @objc func tapNameShare() {
        self.delegateBase.userNameStorePostLabel(cell: self)
    }
    
    @objc func openUserImage() {
        self.delegate?.openUserImageShareStoreLook(indexPathNow)
    }
    
    @objc func openUserButton ( _ sender : UIButton) {
        self.delegate?.openUserButtonShareStoreLook(sender, indexPathNow)
    }
    
    @IBAction func pressBuyButton(_ sender: UIButton) {
        self.delegate.buyLookStoreButton(sender, indexPathNow)
    }

}


extension ShareStoreLookCollectionViewCell : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.accessibilityHint = "\(self.indexPathNow.row)"
        self.delegate.selectedItemShareLook(collectionView, indexPath)
    }
    
}

extension ShareStoreLookCollectionViewCell : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate.numberOfItemsInSectionShareLook(self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.delegate.cellForItemShareLook(collectionView, indexPath, self)
    }
}


extension ShareStoreLookCollectionViewCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width / 3, height: self.collectionView.bounds.height)
    }
    
}

protocol ShareStoreLookCollectionViewCellDelegate {
    func selectedItemShareLook(_ collection : UICollectionView, _ indexPath : IndexPath)
    func cellForItemShareLook(_ collection : UICollectionView, _ indexPath : IndexPath, _ cellForLook : ShareStoreLookCollectionViewCell) -> UICollectionViewCell
    func numberOfItemsInSectionShareLook(_ cellForLook : ShareStoreLookCollectionViewCell) -> Int
    func buyShareLookStoreImage ( _ indexPath : IndexPath)
    func buyLookStoreButton ( _ sender : UIButton, _ indexPath : IndexPath)
    func openUserButtonShareStoreLook( _ sender : UIButton,  _ indexPath : IndexPath)
    func openUserImageShareStoreLook( _ indexPath : IndexPath)
}
