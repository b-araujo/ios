//
//  TagViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class TagViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    var photoImage : UIImage? = nil
    var isEndedLongPress = false
    
    var searchBarController : CustomSearchController!
    
    var previousTouch : CGPoint = CGPoint.zero
    
    var tags : [[String : Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let showBrandAndStoreViewController = storyboard.instantiateViewController(withIdentifier: "ShowBrandAndStoreID") as! ShowBrandAndStoreViewController
        
        self.searchBarController = CustomSearchController(searchResultsController: showBrandAndStoreViewController)
        self.searchBarController.searchResultsUpdater = self
        self.searchBarController.hidesNavigationBarDuringPresentation = false
        self.searchBarController.dimsBackgroundDuringPresentation = true
        self.searchBarController.searchBar.searchBarStyle = .prominent
        self.searchBarController.searchBar.sizeToFit()
        self.searchBarController.searchBar.placeholder = "Pesquisar"
        self.searchBarController.searchBar.delegate = self
        self.searchBarController.delegate = self
        self.definesPresentationContext = true
        
        
        self.photoImageView.image = photoImage
        self.photoImageView.isUserInteractionEnabled = true
        
        let tapGestureRecognizerPhotoImageView = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        self.photoImageView.addGestureRecognizer(tapGestureRecognizerPhotoImageView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(createTag(notification:)), name: Notification.Name.init("createTag"), object: nil)
        
        if self.tags.count > 0 {
            
            for t in tags {
                
                let leftMargin = (self.view.bounds.width * (t["leftMargin"] as! CGFloat)) / 100
                let topMargin = (self.view.bounds.width * ((t["topMargin"] as! CGFloat) + 20.0)) / 100

                let tagInfoLabel = UILabel(frame: CGRect(x: leftMargin, y: topMargin, width: (t["text"] as! NSString).size(withAttributes: nil).width + 50, height: 25))
                tagInfoLabel.isUserInteractionEnabled = true
                tagInfoLabel.backgroundColor = UIColor.darkGray
                tagInfoLabel.layer.cornerRadius = 5
                tagInfoLabel.layer.masksToBounds = true
                tagInfoLabel.accessibilityHint = "\(self.tags.count - 1)"
                tagInfoLabel.text = t["text"] as? String
                tagInfoLabel.textAlignment = .center
                tagInfoLabel.textColor = UIColor.white
                tagInfoLabel.adjustsFontSizeToFitWidth = true
                self.view.addSubview(tagInfoLabel)
                
                let panGestureRecognizerTagInfoLabel = UIPanGestureRecognizer(target: self, action: #selector(pan(_:)))
                let longPressGestureRecognizerTagInfoLabel = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
                
                panGestureRecognizerTagInfoLabel.delegate = self
                longPressGestureRecognizerTagInfoLabel.delegate = self
                longPressGestureRecognizerTagInfoLabel.minimumPressDuration = 0.0
                
                tagInfoLabel.addGestureRecognizer(panGestureRecognizerTagInfoLabel)
                tagInfoLabel.addGestureRecognizer(longPressGestureRecognizerTagInfoLabel)
                
            }
            
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    @objc func longPress( _ sender : UILongPressGestureRecognizer) {
        
        if sender.state == .began {
            
            self.isEndedLongPress = false
            self.infoImageView.image = UIImage(named: "trash_empty")
            self.infoLabel.text = "Toque na tag para move-la ou se quiser excluir, arraste até aqui."
            
        } else if sender.state == .ended {
            
            for view in self.view.subviews {
                
                if view.accessibilityHint != nil {
                    
                    if view.frame.origin.y > ((self.photoImageView.bounds.height + self.photoImageView.frame.origin.y) - 25) {
                        view.removeFromSuperview()
                        self.tags[Int(view.accessibilityHint!)!]["type"] = "X"
                    }
                    
                }
                
            }
            
            self.isEndedLongPress = true
            self.infoImageView.image = UIImage(named: "touch_user")
            self.infoLabel.text = "Toque na foto para selecionar as marcas que você está usando."
            
        }
        
    }
    
    @objc func pan( _ sender : UIPanGestureRecognizer) {
        
        let translation = sender.translation(in: self.view)
        let myView = sender.view!
        
        if (myView.center.y != 76.5 && myView.center.y >= 76.5) && (myView.center.x != 40.0 && myView.center.x >= 40.0) && (myView.center.x != (self.photoImageView.bounds.width - 40) && myView.center.x <= (self.photoImageView.bounds.width - 40)) {
        
            myView.center = CGPoint(x: myView.center.x + translation.x, y: myView.center.y + translation.y)
        
        } else {
            
            if myView.center.y != 76.5 && myView.center.y < 76.5 {
            
                myView.center = CGPoint(x: myView.center.x + translation.x, y: 76.501)
            
            } else if myView.center.x != 40.0 && myView.center.x < 40.0 {
            
                myView.center = CGPoint(x: 40.001, y: myView.center.y + translation.y)
            
            } else if myView.center.x != (self.photoImageView.bounds.width - 40) && myView.center.x > (self.photoImageView.bounds.width - 40) {
                
                myView.center = CGPoint(x: (self.photoImageView.bounds.width - 40), y: myView.center.y + translation.y)
                
            } else {
                
                myView.center = CGPoint(x: myView.center.x + translation.x, y: myView.center.y + translation.y)
            
            }
        
        }
        
        self.tags[Int(myView.accessibilityHint!)!]["leftMargin"] = (((((myView.center.x + translation.x) - CGFloat(40)) * 100) / self.view.bounds.width))
        self.tags[Int(myView.accessibilityHint!)!]["topMargin"] = (((((myView.center.y + translation.y) - CGFloat(12.5)) * 100) / self.view.bounds.width) - 20)
        
        sender.setTranslation(CGPoint.zero, in: self.view)
        
        if (myView.center.y + 12.5) > (self.photoImageView.bounds.width + self.photoImageView.frame.origin.y) {
            self.infoImageView.image = UIImage(named: "trash_full")
        } else {
            
            if self.isEndedLongPress == false {
                self.infoImageView.image = UIImage(named: "trash_empty")
            } else {
                self.infoImageView.image = UIImage(named: "touch_user")
            }
            
        }
        
    }
    
    @objc func tap( _ sender : UITapGestureRecognizer) {
        
        let locationTouch = sender.location(in: self.view)
        
        self.previousTouch = locationTouch
        
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = self.searchBarController
        } else {
            // Fallback on earlier versions
            navigationItem.titleView = self.searchBarController.searchBar
        }
//        self.navigationItem.titleView = self.searchBarController.searchBar
        self.searchBarController.searchBar.becomeFirstResponder()
        self.navigationItem.rightBarButtonItem = nil
        
    }
    
    @objc func okActionBarButtonItem( _ sender : UIBarButtonItem) {
       
        
        var tagsVerification : [[String : Any]] = []
        
        for t in self.tags {
            if (t["type"] as! String) != "X" {
                tagsVerification.append(t)
            }
        }
        
        self.tags.removeAll()
        self.tags = tagsVerification
        
        NotificationCenter.default.post(name: NSNotification.Name.init("myTags"), object: self.tags)
        self.navigationController?.popViewController(animated: true)
    }

    @objc func createTag(notification : Notification) {
        
        self.searchBarController.isActive = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Ok", style: .plain, target: self, action: #selector(okActionBarButtonItem(_:)))
        self.navigationItem.titleView = nil
        
        let tagInfo = notification.object as! [String : String]
        
        print(self.previousTouch.x)
        print(self.previousTouch.y)
        
        let tag : [String : Any] = [ "id": self.tags.count,
                                     "text" : tagInfo["name"]!,
                                     "leftMargin" : ((self.previousTouch.x * 100) / self.view.bounds.width),
                                     "topMargin" : (((self.previousTouch.y * 100) / self.view.bounds.width) - 20),
                                     "screenWidth" : self.view.bounds.width,
                                     "type" : "\(((tagInfo["id"]! == "0") ? "BRAND" : "STORE"))",
                                     "storeId" : Int(tagInfo["id"]!)!
                                    ]
        
        self.tags.append(tag)
        
        
        let tagInfoLabel = UILabel(frame: CGRect(x: self.previousTouch.x, y: self.previousTouch.y, width: (tagInfo["name"]! as NSString).size(withAttributes: nil).width + 50, height: 25))
        
        tagInfoLabel.isUserInteractionEnabled = true
        tagInfoLabel.backgroundColor = UIColor.darkGray
        tagInfoLabel.layer.cornerRadius = 5
        tagInfoLabel.layer.masksToBounds = true
        tagInfoLabel.accessibilityHint = "\(self.tags.count - 1)"
        tagInfoLabel.text = tagInfo["name"]
        tagInfoLabel.textAlignment = .center
        tagInfoLabel.textColor = UIColor.white
        tagInfoLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(tagInfoLabel)
    
        let panGestureRecognizerTagInfoLabel = UIPanGestureRecognizer(target: self, action: #selector(pan(_:)))
        let longPressGestureRecognizerTagInfoLabel = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        
        panGestureRecognizerTagInfoLabel.delegate = self
        longPressGestureRecognizerTagInfoLabel.delegate = self
        longPressGestureRecognizerTagInfoLabel.minimumPressDuration = 0.0
        
        tagInfoLabel.addGestureRecognizer(panGestureRecognizerTagInfoLabel)
        tagInfoLabel.addGestureRecognizer(longPressGestureRecognizerTagInfoLabel)
        
    }
    
}

extension TagViewController : UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension TagViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if (searchController.searchBar.text?.count)! >= 3 {
            searchBarController.searchResultsController?.view.isHidden = false
        } else {
            searchBarController.searchResultsController?.view.isHidden = true
        }
        
    }
    
}


extension TagViewController: UISearchControllerDelegate {
    
//    func didPresentSearchController(_ searchController: UISearchController) {
////        searchController.searchBar.showsCancelButton = false
//    }
//
//    func willPresentSearchController(_ searchController: UISearchController) {
//        searchController.searchBar.setShowsCancelButton(false, animated: false)
//    }
}

extension TagViewController : UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Ok", style: .plain, target: self, action: #selector(okActionBarButtonItem(_:)))
        self.navigationItem.titleView = nil
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count >= 3 {
            NotificationCenter.default.post(name: NSNotification.Name.init("changeTextSearch"), object: searchText)
        }
        
    }
}

class CustomSearchBar: UISearchBar {
    
    override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
        super.setShowsCancelButton(false, animated: false)
    }
}

class CustomSearchController: UISearchController {
    lazy var _searchBar: CustomSearchBar = {
        [unowned self] in
        let customSearchBar = CustomSearchBar(frame: CGRect.zero)
        return customSearchBar
        }()
    
    override var searchBar: UISearchBar {
        get {
            return _searchBar
        }
    }
}

extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func setTextFieldColor(color: UIColor) {
        
        if let textField = getViewElement(type: UITextField.self) {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
                
            case .prominent, .default:
                textField.backgroundColor = color
            }
        }
    }
}
