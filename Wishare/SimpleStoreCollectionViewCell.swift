//
//  SimpleStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class SimpleStoreCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentsImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var moreImageView: UIImageView!
    @IBOutlet weak var postStoreImageView: UIImageView!
    
    @IBOutlet weak var nameStoreTopButton: UIButton!
    @IBOutlet weak var nameStoreBottomButton: UIButton!
    
    @IBOutlet weak var commentsLabel: ActiveLabel!
    @IBOutlet weak var datePostLabel: UILabel!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var viewLikes: UIView!
    @IBOutlet weak var countLikesLabel: UILabel!
    @IBOutlet weak var countCommentsLabel: UILabel!
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    @IBOutlet weak var moreComment: UIButton!
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    
    
    
    var delegate : SimpleStoreCollectionViewCellDelegate?
    var IndexPathNow : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureRecognizerStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreImageView))
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLikeImageView))
        let tapGestureRecognizerCommentsImageView = UITapGestureRecognizer(target: self, action: #selector(tapCommentsImageView))
        let tapGestureRecognizerCommentsLabel = UITapGestureRecognizer(target: self, action: #selector(tapCommentsImageView))
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(tapShareImageView))
        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMoreImageView))
        let tapGestureRecognizerPostStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapPostStoreImageView))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapViewLikes))

    
        self.storeImageView.addGestureRecognizer(tapGestureRecognizerStoreImageView)
        self.likeImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentsImageView.addGestureRecognizer(tapGestureRecognizerCommentsImageView)
        self.shareImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        self.postStoreImageView.addGestureRecognizer(tapGestureRecognizerPostStoreImageView)
        self.countCommentsLabel.addGestureRecognizer(tapGestureRecognizerCommentsLabel)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        
        self.moreComment.addTarget(self, action: #selector(tapShowMoreComments(_:)), for: .touchUpInside)
    }
    
    
    @IBAction func tapNameStore(_ sender: UIButton) {
        self.delegate?.nameStoreSimpleStore(cell: self)
    }
    
    @objc func tapStoreImageView() {
        self.delegate?.storeSimpleStore(cell: self)
    }
    
    @objc func tapLikeImageView() {
        self.delegate?.likeSimpleStore(cell: self)
    }
    
    @objc func tapViewLikes() {
        self.delegate?.viewLikes(cell: self, indexPath: IndexPathNow)
    }
    
    @objc func tapCommentsImageView() {
        self.delegate?.commentSimpleStore(cell: self)
    }
    
    @objc func tapShareImageView() {
        self.delegate?.shareSimpleStore(cell: self, indexPath: IndexPathNow)
    }
    
    @objc func tapMoreImageView() {
        self.delegate?.moreSimpleStore(cell: self)
    }
    
    @objc func tapPostStoreImageView() {
        self.delegate?.storePostSimpleStore(cell: self)
    }
    
    @objc func tapShowMoreComments( _ sender : UIButton) {
        self.delegate?.showMoreCommentsSimpleStore(cell: self, indexPath: IndexPathNow)
    }
}

protocol SimpleStoreCollectionViewCellDelegate {
    
    func storeSimpleStore(cell : SimpleStoreCollectionViewCell)
    func likeSimpleStore(cell : SimpleStoreCollectionViewCell)
    func commentSimpleStore(cell : SimpleStoreCollectionViewCell)
    func shareSimpleStore(cell : SimpleStoreCollectionViewCell, indexPath : IndexPath)
    func moreSimpleStore(cell : SimpleStoreCollectionViewCell)
    func storePostSimpleStore(cell : SimpleStoreCollectionViewCell)
    func nameStoreSimpleStore(cell : SimpleStoreCollectionViewCell)
    func showMoreCommentsSimpleStore(cell : SimpleStoreCollectionViewCell, indexPath : IndexPath)
    func viewLikes(cell: SimpleStoreCollectionViewCell, indexPath : IndexPath)
    
}


