//
//  WSNotification.swift
//  Wishare
//
//  Created by Wishare iMac on 8/1/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class WSNotification {
    
    var id : String?
    var user : User?
    var actionMessage : String?
    var actionID : String?
    var seen : String?
    var date : String?
    var idPost : String?
    var picturePost : String?
    var store : Store?
    var products : [[String : AnyObject]] = []
    var productsPicture : [[String : String]] = []
    var target : User?
    
    init ( _ notification : [String : AnyObject]) {
        
        self.id = notification["id"] as? String
        self.user = User(byNotification: notification["user"] as! [String : AnyObject])
        self.actionMessage = notification["action"] as? String
        self.actionID = notification["actionID"] as? String
        self.seen = notification["seen"] as? String
        self.date = notification["date"] as? String
        
        if let target : [String : AnyObject] = notification["target"] as? [String : AnyObject] {
            self.idPost = target["id"] as? String
            self.picturePost = (target["picture"] is NSNull) ? nil : target["picture"] as? String
            
            if let products : String = target["products"] as? String {
                if products.contains("url") {
                    
                    var productsArray : [String] = []
                    var productsJSON : [String] = []
                    var url : [String] = []
                    
                    let prod = products.components(separatedBy: CharacterSet(charactersIn: ",[{}]")).filter({ (value) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    for string in prod {
                        
                        if string.contains("\"id\":") {
                            productsArray.append(string.components(separatedBy: "\"id\":")[1])
                        } else if string.contains("\"url\":") {
                            productsArray.append(string.components(separatedBy: "\"url\":")[1])
                        }
                        
                    }
                    
                    for string in productsArray {
                        
                        if string.contains("\"") {
                            productsJSON.append(string.components(separatedBy: "\"")[1])
                        } else {
                            productsJSON.append(string)
                        }
                        
                    }
                    
                    for string in productsJSON {
                        
                        if string.contains("\\") {
                            let parseString : String = string.components(separatedBy: "\\")[0]
                            url.append("\(parseString)\(string.components(separatedBy: "\\")[1])")
                        } else {
                            url.append(string)
                        }
                        
                    }
                    
                    
                    for i in 0...((url.count / 2) - 1) {
                        self.productsPicture.append(["id": url[i * 2], "url" : url[(i * 2) + 1]])
                    }
                    
                }
            }
        }
        
    }
    
    init ( action notification : [String : AnyObject]) {
        
        self.id = notification["id"] as? String
        self.user = User(byNotification: notification["user"] as! [String : AnyObject])
        self.actionMessage = notification["action"] as? String
        self.actionID = notification["actionID"] as? String
        self.seen = notification["seen"] as? String
        self.date = notification["date"] as? String
        
        if let target : [String : AnyObject] = notification["target"] as? [String : AnyObject] {
            self.idPost = target["id"] as? String
            self.picturePost = (target["picture"] is NSNull) ? nil : target["picture"] as? String
            
            if let actionID = self.actionID {
                
                if actionID == "5" {
                    self.actionMessage = "começou a seguir \(target["username"] as! String)"
                    self.target = User.init(byNotification: target)
                } else if actionID == "7" {
                    self.actionMessage = "deseja seguir \(target["username"] as! String)"
                } else if actionID == "11" {
                    self.store = Store.init(commentStore: target["Store"] as! [String : AnyObject])
                    self.actionMessage = "começou a seguir \(store?.name ?? "")"
                }
                
            }
                
            if let products : String = target["products"] as? String {
                if products.contains("url") {
                    
                    var productsArray : [String] = []
                    var productsJSON : [String] = []
                    var url : [String] = []
                    
                    let prod = products.components(separatedBy: CharacterSet(charactersIn: ",[{}]")).filter({ (value) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    for string in prod {
                        
                        if string.contains("\"id\":") {
                            productsArray.append(string.components(separatedBy: "\"id\":")[1])
                        } else if string.contains("\"url\":") {
                            productsArray.append(string.components(separatedBy: "\"url\":")[1])
                        }
                        
                    }
                    
                    for string in productsArray {
                        
                        if string.contains("\"") {
                            productsJSON.append(string.components(separatedBy: "\"")[1])
                        } else {
                            productsJSON.append(string)
                        }
                        
                    }
                    
                    for string in productsJSON {
                        
                        if string.contains("\\") {
                            let parseString : String = string.components(separatedBy: "\\")[0]
                            url.append("\(parseString)\(string.components(separatedBy: "\\")[1])")
                        } else {
                            url.append(string)
                        }
                        
                    }
                    
                    
                    for i in 0...((url.count / 2) - 1) {
                        self.productsPicture.append(["id": url[i * 2], "url" : url[(i * 2) + 1]])
                    }
                    
                }
            }

        }
        
        if let targerUser : [String : AnyObject] = notification["target_user"] as? [String : AnyObject] {
            
            if let actionID = self.actionID {
                if actionID == "1" {
                    self.actionMessage = "curtiu a publicação de \(targerUser["username"] as! String)"
                } else if actionID == "2" {
                    self.actionMessage = "compartilhou a publicação de \(targerUser["username"] as! String)"
                } else if actionID == "3" {
                    self.actionMessage = "desejou a publicação de \(targerUser["username"] as! String)"
                } else if actionID == "4" {
                    self.actionMessage = "comentou a publicação de \(targerUser["username"] as! String)"
                }
            }
            
        }
        
    }
    
    init ( order notification : [String : AnyObject]) {
        
        self.id = notification["id"] as? String
        self.user = User(byNotification: notification["user"] as! [String : AnyObject])
        self.actionMessage = notification["action"] as? String
        self.actionID = notification["actionID"] as? String
        self.seen = notification["seen"] as? String
        self.date = notification["date"] as? String
        
        if let target : [String : AnyObject] = notification["target"] as? [String : AnyObject] {
            self.store = Store(byCategory: target)
            self.products = target["OrderedItem"]! as! [[String : AnyObject]]
        }
        
    }
    
}
