//
//  UserMentionTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/2/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class UserMentionTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var loadingAcitivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var topConstraintImageView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintImageView: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
