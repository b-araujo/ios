//
//  PageUserViewController.swift
//  Wishare
//
//  Created by Vinicius França on 04/07/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import CollieGallery
import Kingfisher

class PageUserViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let refreshControll = UIRefreshControl()
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var loader : Loader!
    var manager : Manager!
    
    var user : User? = nil
    
    var posts : [Post] = []
    var posts_products : [Post] = []
    var wish_products : [Product] = []
    
    var idUser : String = "0"
    var isMyProfile = true
    
    var isUserBlocked : Bool = false
    var isFirstLoading : Bool = true
    
    var backIndicatorImage : UIImage?
    var backIndicatorTransitionMaskImage : UIImage?
    
    var isLoading : Bool = false
    let footerViewReuseIdentifier = "footerViewReuseIdentifier"
    var footerView : FooterCollectionReusableView?
    
    var pictures = [CollieGalleryPicture]()
    
    var previewPosts : Int = 0 {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("changeViewPosts"), object: previewPosts)
        }
    }
    
    var previewType : Int = 0 {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("changeViewTypeOption"), object: previewType)
        }
    }
    
    var postsLoaded: Bool = false
    var wishesLoaded: Bool = false
    var purchasesLoaded: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControll.tintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        self.refreshControll.addTarget(self, action: #selector(refreshShow), for: .valueChanged)
        self.collectionView.refreshControl = self.refreshControll
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeViewPosts), name: NSNotification.Name.init("changeViewPosts"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeViewTypeOption), name: NSNotification.Name.init("changeViewTypeOption"), object: nil)
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.tag = 10
        
        self.collectionView.register(UINib(nibName: "PageUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "pageUserCell")
        self.collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerViewReuseIdentifier")
        
        if self.isFirstLoading == true {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
            
            if !self.isMyProfile {
                WishareCoreData.getFirstUser({ ( userLogin : UserActive?, error : Error?) in
                    if error == nil {
                        WishareAPI.getInfoUser(idUser: self.idUser, { (user, posts, error) in
                            if error == nil {
                                self.user = user
                                if userLogin!.id! == user!.id! {
                                    self.isMyProfile = true
                                }
                                
                                if user!.isFollowing! == "false" && user!.privateW! == "1" && self.isMyProfile == false {
                                    
                                    self.posts_products.removeAll()
                                    self.posts_products = []
                                    self.user = user
                                    
                                    self.posts = []
                                    self.wish_products = []
                                    self.collectionView.reloadData()
                                    
                                } else {
                                    
                                    WishareAPI.getPostsUser(idUser: self.idUser, { (posts : [Post], error : Error?) in
                                        if error == nil {
                                            self.posts = posts
                                            self.collectionView.reloadData()
                                        }
                                        self.postsLoaded = true
                                    })
                                    WishareAPI.getPurchaseBuy(idUser: self.idUser, userBy: user!, { (postsPurchase : [Post], error : Error?) in
                                        if error == nil {
                                            self.posts_products.removeAll()
                                            self.posts_products = postsPurchase
                                            self.collectionView.reloadData()
                                        }
                                        self.purchasesLoaded = true
                                    })
                                    WishareAPI.getWishes(idUser: user!.id!, { (products : [Product], error : Error?) in
                                        if error == nil {
                                            self.wish_products = products
                                            self.collectionView.reloadData()
                                        }
                                        self.wishesLoaded = true
                                    })
                                }
                                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "reticencias_vertical"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.moreInformation))
                                RappleActivityIndicatorView.stopAnimation()
                            }
                        })
                    }
                })
            } else {
                WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                    if error ==  nil && user != nil {
                        WishareAPI.getInfoUser(idUser: user!.id!, { (user, posts, error) in
                            self.user = user
                            self.collectionView.reloadData()
                            RappleActivityIndicatorView.stopAnimation()
                            WishareAPI.getPurchaseBuy(idUser: user!.id!, userBy: user!, { (postsPurchase : [Post], error : Error?) in
                                if error == nil {
                                    self.posts_products.removeAll()
                                    self.posts_products = postsPurchase
                                    self.collectionView.reloadData()
                                }
                                self.purchasesLoaded = true
                            })
                        })
                        
                        WishareAPI.getPostsUser(idUser: user!.id!, { (posts : [Post], error : Error?) in
                            if error == nil {
                                self.posts = posts
                                self.collectionView.reloadData()
                            }
                            self.postsLoaded = true

                        })
                        
                        WishareAPI.getWishes(idUser: user!.id!, { (products : [Product], error : Error?) in
                            if error == nil {
                                self.wish_products = products
                                self.collectionView.reloadData()
                            }
                            self.wishesLoaded = true
                        })
                    }
                }
            }
            
        
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.backIndicatorImage = self.navigationController?.navigationBar.backIndicatorImage
        self.backIndicatorTransitionMaskImage = self.navigationController?.navigationBar.backIndicatorTransitionMaskImage
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-left")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-left")
        
        if self.isFirstLoading == true {
            self.isFirstLoading = false
        } else {
            
            
            if !self.isMyProfile {
                
                WishareAPI.blockedUsers({ ( users : [User], error : Error?) in
                    if error == nil {
                        
                        
                        WishareCoreData.getFirstUser({ ( userLogin : UserActive?, error : Error?) in
                            if error == nil {
                                
                                WishareAPI.getInformationUser(idUser: self.idUser, { (user : User?, error : Error?) in
                                    if error == nil {
                                        
                                        if userLogin!.id! == user!.id! {
                                            self.isMyProfile = true
                                        }

                                        if user!.isFollowing! == "false" && user!.privateW! == "1" && self.isMyProfile == false {
                                            
                                            self.posts_products.removeAll()
                                            self.posts_products = []
                                            self.user = user
                                            
//                                            self.pictures.removeAll()
//                                            let url = "\(urlBase.absoluteString)files/photo-profile/user/\(self.user!.picture!)"
//                                            let pict = CollieGalleryPicture(url: url)
//                                            self.pictures.append(pict)
                                            
                                            self.posts = []
                                            self.collectionView.reloadData()

                                            
                                        } else {
                                            
                                            self.user = user
                                            
//                                            self.pictures.removeAll()
//                                            let url = "\(urlBase.absoluteString)files/photo-profile/user/\(self.user!.picture!)"
//                                            let pict = CollieGalleryPicture(url: url)
//                                            self.pictures.append(pict)
                                            
                                            self.collectionView.reloadData()
                                            
                                        }
                                        
                                    }
                                })
                                
                            }
                        })
                        
                    }
                })
                
            } else {
                
                WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                    
                    if error ==  nil && user != nil {
                        
                        WishareAPI.getInformationUser(idUser: user!.id!) { (user : User?, error : Error?) in
                            if error == nil {
                                
                                WishareAPI.getPostsUser(idUser: user!.id!, { (posts : [Post], error : Error?) in
                                    if error == nil {
                                        
                                        WishareAPI.getPurchaseBuy(idUser: user!.id!, userBy: user!, { (postsPurchase : [Post], error : Error?) in
                                            if error == nil {
                                                
                                                WishareAPI.getWishes(idUser: user!.id!, { (products : [Product], error : Error?) in
                                                    if error == nil {
                                                        
                                                        self.posts_products.removeAll()
                                                        self.posts_products = postsPurchase
                                                        self.user = user
                                                        
//                                                        self.pictures.removeAll()
//                                                        let url = "\(urlBase.absoluteString)files/photo-profile/user/\(self.user!.picture!)"
//                                                        let pict = CollieGalleryPicture(url: url)
//                                                        self.pictures.append(pict)
                                                        
                                                        self.posts = posts
                                                        self.wish_products = products
                                                        self.collectionView.reloadData()
                                                        
                                                    }
                                                })
                                                
                                            }
                                        })
                                        
                                    }
                                })
                                
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        
        self.navigationController?.navigationBar.backIndicatorImage = self.backIndicatorImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = self.backIndicatorTransitionMaskImage
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "followersUserSegue" {
            let followersPageUserViewController = segue.destination as! FollowersPageUserViewController
            followersPageUserViewController.idUser = sender as! String
        }
        
        if segue.identifier == "followUserSegue" {
            let followingPageUserViewController = segue.destination as! FollowingPageUserViewController
            followingPageUserViewController.idUser = sender as! String
        }
        
        if segue.identifier == "followWipsUserSegue" {
            let followingWipsPageUserViewController = segue.destination as! FollowingWipsPageUserViewController
            followingWipsPageUserViewController.idUser = sender as! String
        }
        
        if segue.identifier == "followStoreUserSegue" {
            let followingStorePageUserViewController = segue.destination as! FollowingStorePageUserViewController
            followingStorePageUserViewController.idUser = sender as! String
        }
        
        if segue.identifier == "previewPostPageUserSegue" {
            
            let previewPostPageUserViewController = segue.destination as! PreviewPostPageUserViewController
            previewPostPageUserViewController.posts = sender as! Post
            
        }
        
        // ========= SEGUES RESTANTES ==========
        
        if segue.identifier == "showImagePost" {
            
            let showImagePostViewController = segue.destination as! ShowImagePostViewController
            showImagePostViewController.pictureImageString = sender as? UIImage
            
        }
        
        if segue.identifier == "fotoLookSegue" {
            
            let fotoLookViewController = segue.destination as! FotoLookViewController
            fotoLookViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "shareRepostSegue" {
            
            let repostViewController = segue.destination as! RepostViewController
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            repostViewController.imageRespost = senderValue[0] as? UIImage
            repostViewController.id = senderValue[1] as? String
            
        }
        
        if segue.identifier == "buyShareSegue" {
            
            let buyShareViewController = segue.destination as! BuyShareViewController
            buyShareViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "commentSegue" {
            
            let commentViewController = segue.destination as! CommentViewController
            commentViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "likeSegue" {
            
            let likeViewController = segue.destination as! LikeViewController
            likeViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "publishEditingSegue" {
            
            let publishEditingViewController = segue.destination as! PublishEditingViewController
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            publishEditingViewController.post = senderValue[0] as! Post
            publishEditingViewController.indexPath = senderValue[1] as! IndexPath
            
        }
        
        if segue.identifier == "pageStoreSegue" {
            let pageStoreViewController = segue.destination as! PageStoreViewController
            pageStoreViewController.idStore = sender as! String
        }
        
        
    }
    
    func customNavigationBar () {
        let nav = (self.navigationController?.navigationBar)!
        let transparencia = UIImage(named: "transparente")
        nav.setBackgroundImage(transparencia, for: .default)
        nav.shadowImage = transparencia
        nav.backgroundColor = UIColor.clear
    }
    
    @objc func moreInformation () {
        
        let userAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let blockedAction = UIAlertAction(title: "Bloquear Usuário", style: .default) { ( _ ) in
            
            let alertController = UIAlertController(title: "Bloquear \(self.user!.username!)?", message: "Ele não poderá encontrar seu perfil ou publicações. O Wishare não informará a ele que você o bloqueou", preferredStyle: .actionSheet)
            
            let confirmAction = UIAlertAction(title: "Confirmar", style: .default) { ( _ ) in
                
                RappleActivityIndicatorView.startAnimatingWithLabel("Bloqueando...", attributes: self.attributes)
                
                WishareAPI.blockedUser(idUser: self.user!.id!, { (error : Error?) in
                    if error == nil {
                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Bloqueado", completionTimeout: 1)
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)

            
        }
        
        let denunciarAction = UIAlertAction(title: "Denunciar Usuário", style: .default) { ( _ ) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: self.attributes)
            WishareAPI.userToReport(self.user!.id!, { (error : Error?) in
                if error == nil {
                    
                    RappleActivityIndicatorView.stopAnimation()
                    
                    let alertController = UIAlertController(title: "ALGO ESTÁ ERRADO?", message: "Obrigado pela informação", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
                    
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            })
            
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        userAlertController.addAction(blockedAction)
        userAlertController.addAction(denunciarAction)
        userAlertController.addAction(cancelAction)
        
        self.present(userAlertController, animated: true, completion: nil)
        
        
        
    }
    
    @objc func changeViewPosts () {
        
        self.collectionView.reloadData()
        
        if self.previewPosts == 0 {
            
            let blockButton = self.view.viewWithTag(1) as! UIButton
            blockButton.setImage(UIImage(named: "menu-block-filled"), for: .normal)
            
            let listButton = self.view.viewWithTag(2) as! UIButton
            listButton.setImage(UIImage(named: "menu-list"), for: .normal)
            
        } else {
            
            let blockButton = self.view.viewWithTag(1) as! UIButton
            blockButton.setImage(UIImage(named: "menu-block"), for: .normal)
            
            let listButton = self.view.viewWithTag(2) as! UIButton
            listButton.setImage(UIImage(named: "menu-list-filled"), for: .normal)
            
        }
        
    }
    
    @objc func changeViewTypeOption () {
        self.previewPosts = 0
    }
    
    @objc func refreshShow() {
        
        if !self.isMyProfile {
            
            WishareCoreData.getFirstUser({ ( userLogin : UserActive?, error : Error?) in
                if error == nil {
                    
                    WishareAPI.getInformationUser(idUser: self.idUser, { (user : User?, error : Error?) in
                        if error == nil {
                            
                            if userLogin!.id! == user!.id! {
                                self.isMyProfile = true
                            }
                            
                            
                            
                            if user!.isFollowing! == "false" && user!.privateW! == "1" && self.isMyProfile == false {
                                
                                self.posts_products.removeAll()
                                self.posts_products = []
                                self.user = user
                                self.posts = []
                                self.wish_products = []
                                self.collectionView.reloadData()
                                self.refreshControll.endRefreshing()
                                
                            } else {
                                
                                WishareAPI.getPostsUser(idUser: self.idUser, { (posts : [Post], error : Error?) in
                                    if error == nil {
                                        
                                        WishareAPI.getPurchaseBuy(idUser: self.idUser, userBy: user!, { (postsPurchase : [Post], error : Error?) in
                                            if error == nil {
                                                
                                                WishareAPI.getWishes(idUser: self.idUser, { (products : [Product], error : Error?) in
                                                    if error == nil {
                                                        
                                                        self.posts_products.removeAll()
                                                        self.posts_products = postsPurchase
                                                        self.user = user
                                                        self.posts = posts
                                                        self.wish_products = products
                                                        self.collectionView.reloadData()
                                                        self.refreshControll.endRefreshing()
                                                        
                                                    }
                                                })
                                                
                                            }
                                        })
                                        
                                    }
                                })
                                
                                
                            }
                            
                            
                        }
                    })
                    
                    
                }
            })
            
            
            
            
//            if !(self.user!.isFollowing! == "false" && self.user!.privateW! == "1" && self.isMyProfile == false) {
//                
//                WishareAPI.getPostsUser(idUser: self.idUser, { (posts : [Post], error : Error?) in
//                    if error == nil {
//                        
//                        self.posts = posts
//                        self.collectionView.reloadData()
//                        self.refreshControll.endRefreshing()
//                        
//                    }
//                })
//                
//            } else {
//                self.refreshControll.endRefreshing()
//            }
            
        } else {
            
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    WishareAPI.getInformationUser(idUser: user!.id!) { (user : User?, error : Error?) in
                        if error == nil {
                            
                            WishareAPI.getPostsUser(idUser: user!.id!, { (posts : [Post], error : Error?) in
                                if error == nil {
                                    
                                    WishareAPI.getPurchaseBuy(idUser: user!.id!, userBy: user!, { (postsPurchase : [Post], error : Error?) in
                                        if error == nil {
                                            
                                            WishareAPI.getWishes(idUser: user!.id!, { (products : [Product], error : Error?) in
                                                if error == nil {
                                                    
                                                    self.posts_products.removeAll()
                                                    self.posts_products = postsPurchase
                                                    self.user = user
                                                    self.posts = posts
                                                    self.wish_products = products
                                                    self.collectionView.reloadData()
                                                    self.refreshControll.endRefreshing()
                                                    
                                                }
                                            })
                                            
                                        }
                                    })
                                    
                                }
                            })
                            
                        }
                    }
                }
            }
            
        }
        
    }
    
    func likeAnimation(_ image : UIImageView ) {
        
        UIView.animate(withDuration: 1.6, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: .allowUserInteraction, animations: {
            image.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
            image.alpha = 1.0
        }, completion: { (finished : Bool) in
            image.alpha = 0.0
            image.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        
    }
    
    func completionHandler(activityType: UIActivityType?, shared: Bool, items: [Any]?, error: Error?) {
        
        if shared {
            
            let alertController = UIAlertController(title: "Sucesso", message: "Compartilhamento realizado com sucesso!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func openTags(gesture : UITapGestureRecognizer) {
        
        if let hint = gesture.accessibilityHint {
            
            let info = hint.components(separatedBy: ",").filter({ (value) -> Bool in
                return value != "" ? true : false
            })
            
            if let type = self.posts[Int(info[0])!].tags[Int(info[1])!].type {
                if type == "STORE" {
                    self.performSegue(withIdentifier: "pageStoreSegue", sender: "\(self.posts[Int(info[0])!].tags[Int(info[1])!].storeId!)")
                } else if type == "BRAND" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = self.posts[Int(info[0])!].tags[Int(info[1])!].text!
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                }
            }
            
        }
        
    }
}

extension PageUserViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 111 {
            if self.previewType != 2 {
                if self.previewType != 1 && self.previewType != 2 && self.previewPosts != 1 {
                    self.performSegue(withIdentifier: "previewPostPageUserSegue", sender: self.posts[indexPath.row])
                } else if self.previewType == 1 && self.previewPosts == 0 {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                    productDetailViewController.idProduct = self.wish_products[indexPath.row].pId!
                    productDetailViewController.isFirst = true
                    
                    self.navigationController?.pushViewController(productDetailViewController, animated: true)
                    
                }
            }
        }
        
    }
    
}

extension PageUserViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 10 {
            return 1
        }
        
        if self.previewType == 1 {
            return self.wish_products.count
        } else if self.previewType == 2 {
            return self.posts_products.count
        }
        
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // PAGINA DO USUARIO
        if collectionView.tag == 10 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pageUserCell", for: indexPath) as! PageUserCollectionViewCell
            
            if self.user != nil {
                
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.isScrollEnabled = false
                cell.collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerViewReuseIdentifier")
                cell.collectionView.register(UINib(nibName: "PurchaseShareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "purchaseShareCell")
                cell.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
                cell.collectionView.register(UINib(nibName: "UserPostCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userPostCell")
                cell.collectionView.register(UINib(nibName: "ShareBuyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareBuyCell")
                cell.collectionView.register(UINib(nibName: "SharePostUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "sharePostUserCell")
                cell.collectionView.register(UINib(nibName: "ShareStoreUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareStoreUserCell")
                cell.collectionView.register(UINib(nibName: "ShareStoreProductUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareStoreProductUserCell")
                cell.collectionView.register(UINib(nibName: "ShareStoreLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareStoreLookUserCell")
                cell.collectionView.register(UINib(nibName: "ShareProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareProductUserCell")
                cell.collectionView.register(UINib(nibName: "WishesProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "wishesProductCell")
                cell.collectionView.tag = 111
                cell.collectionView.reloadData()
                
                
                //RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
                cell.loadingActivityIndicator.isHidden = false
                cell.loadingActivityIndicator.startAnimating()
                
                cell.delegate = self
                cell.messageNotFoundPurchaseView.isHidden = true
                
                cell.postsLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
                cell.descriptionPostLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
                
                cell.wishesLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
                cell.descriptionWishesLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
                
                cell.purchasesLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
                cell.descriptionPurchaseLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
                
                cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width / 2
                cell.userImageView.clipsToBounds = true
                
                if self.previewType == 0 {
                
                    if postsLoaded == true {
                        if self.posts.count == 0 {
                            cell.messageNotFoundPurchaseView.isHidden = false
                            cell.messageNotFoundPurchaseLabel.text = "Nenhuma publicação"
                        }
                    }
                    else {
                        cell.messageNotFoundPurchaseView.isHidden = false
                        cell.messageNotFoundPurchaseLabel.text = "Carregando posts..."
                    }

                    
                    cell.postsLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                    cell.descriptionPostLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
                    
                } else if self.previewType == 1 {
                    
                    if wishesLoaded == true {
                        if self.wish_products.count == 0 {
                            cell.messageNotFoundPurchaseView.isHidden = false
                            cell.messageNotFoundPurchaseLabel.text = "Nenhum produto desejado"
                        }
                    }
                    else {
                        cell.messageNotFoundPurchaseView.isHidden = false
                        cell.messageNotFoundPurchaseLabel.text = "Carregando desejos..."
                    }
                    
                    cell.wishesLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                    cell.descriptionWishesLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
                    
                } else if self.previewType == 2 {
                
                    if purchasesLoaded == true {
                        if self.posts_products.count == 0 {
                            cell.messageNotFoundPurchaseView.isHidden = false
                            cell.messageNotFoundPurchaseLabel.text = "Nenhuma compra compartilhada"
                        }
                    }
                    else {
                        cell.messageNotFoundPurchaseView.isHidden = false
                        cell.messageNotFoundPurchaseLabel.text = "Carregando compras..."
                    }
                
                    cell.purchasesLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
                    cell.descriptionPurchaseLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
                    
                }
                
                
                if self.isMyProfile {
                    cell.editOrFollowButton.setTitle("Editar", for: .normal)
                } else {
                    
                    cell.editOrFollowButton.titleLabel?.adjustsFontSizeToFitWidth = true
                    
                    if self.user!.isFollowing! == "true" {
                        cell.editOrFollowButton.backgroundColor = UIColor.white
                        cell.editOrFollowButton.setTitle("Seguindo", for: .normal)
                        cell.editOrFollowButton.setTitleColor(UIColor.black, for: .normal)
                        cell.editOrFollowButton.layer.borderWidth = 1.0
                        cell.editOrFollowButton.layer.borderColor = UIColor.black.cgColor
                    } else if self.user!.isFollowing == "false" {
                        cell.editOrFollowButton.backgroundColor = UIColor.black
                        cell.editOrFollowButton.setTitle("Seguir", for: .normal)
                        cell.editOrFollowButton.setTitleColor(UIColor.white, for: .normal)
                        cell.editOrFollowButton.layer.borderWidth = 0.0
                        cell.editOrFollowButton.layer.borderColor = UIColor.clear.cgColor
                    } else if self.user!.isFollowing == "waiting" {
                        cell.editOrFollowButton.backgroundColor = UIColor.white
                        cell.editOrFollowButton.setTitle("Aguardando", for: .normal)
                        cell.editOrFollowButton.setTitleColor(UIColor.black, for: .normal)
                        cell.editOrFollowButton.layer.borderWidth = 1.0
                        cell.editOrFollowButton.layer.borderColor = UIColor.black.cgColor
                    }
                    
                }
                
                
//                self.pictures.removeAll()
//                let url = "\(urlBase.absoluteString)files/photo-profile/user/\(self.user!.picture!)"
//                let pict = CollieGalleryPicture(url: url)
//                self.pictures.append(pict)
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.user!.picture!)")
                let urlPerfilFull = urlBase.appendingPathComponent("files/photo-profile/user/\(self.user!.picture!)")
                
                self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                    if result.error == nil {
                        
                        cell.userImageView.image = result.value?.circleMask
                        cell.loadingActivityIndicator.stopAnimating()
                        cell.loadingActivityIndicator.isHidden = true
                        
                        self.pictures.removeAll()
                        let pict = CollieGalleryPicture(image: result.value!.circleBorderMiddle)
                        self.pictures.append(pict)
                        
                        if self.user!.profile_image!.type! == "local" {
                            if self.user!.profile_image!.url == nil {
                                cell.backgroundImageView.image = UIImage(named: "\(self.user!.profile_image!.url_local!)")
                            } else {
                                cell.backgroundImageView.image = UIImage(named: "\(self.user!.profile_image!.url!)")
                            }
                            //RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                        } else {
                            //let url = URL(string: "http://www.wishare.com.br/code/")!
                            let urlBackground = urlBase.appendingPathComponent("\(self.user!.profile_image!.url!)")
                            cell.backgroundImageView.kf.indicatorType = .activity
                            cell.backgroundImageView.kf.setImage(with: urlBackground, placeholder: #imageLiteral(resourceName: "blur_image"), options: nil, progressBlock: nil, completionHandler: nil)
                            
                        }
                        
                    } else {
                        self.manager.loadImage(with: urlPerfilFull, into: cell.userImageView, handler: { (result, _) in
                            
                            cell.userImageView.image = result.value?.circleMask
                            cell.loadingActivityIndicator.stopAnimating()
                            cell.loadingActivityIndicator.isHidden = true
                            
                            self.pictures.removeAll()
                            let pict = CollieGalleryPicture(image: result.value!.circleBorderMiddle)
                            self.pictures.append(pict)
                            
                            if self.user!.profile_image!.type! == "local" {
                                if self.user!.profile_image!.url == nil {
                                    cell.backgroundImageView.image = UIImage(named: "\(self.user!.profile_image!.url_local!)")
                                } else {
                                    cell.backgroundImageView.image = UIImage(named: "\(self.user!.profile_image!.url!)")
                                }
                                //RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                            } else {
                                //let url = URL(string: "http://www.wishare.com.br/code/")!
                                let urlBackground = urlBase.appendingPathComponent("\(self.user!.profile_image!.url!)")
                                self.manager.loadImage(with: urlBackground, into: cell.backgroundImageView) { ( result, _ ) in
                                    cell.backgroundImageView.image = result.value
                                    //RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: nil, completionTimeout: 1)
                                }
                            }
                        })
                    }
                }
                
                
                cell.followersLabel.text = self.user!.qtd_followers!
                cell.followingLabel.text = self.user!.qtd_following!
                cell.nameUserLabel.text = self.user!.username
                cell.wipsLabel.text = self.user!.qtd_wips!
                cell.storeLabel.text = self.user!.qtd_stores!
                
                cell.postsLabel.text = self.user!.qtd_posts!
                cell.wishesLabel.text = "\(Int(self.user!.qtd_wishes_products!)!)"
                cell.purchasesLabel.text = self.user!.qtd_orders!
                
                print(self.user!.isFollowing ?? "")
                print(self.user!.privateW ?? "")
                print(self.isMyProfile)
                
                if self.user!.isFollowing! == "false" && self.user!.privateW! == "1" && self.isMyProfile == false {
                    cell.messagePrivateAccountView.isHidden = false
                }
                
            }
            
            return cell
            
        }
        
        // DESEJOS E COMPRAS
        if self.previewType == 1 {
            
            if self.previewPosts == 0 {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
                
                cell.imageProductsLook.image = nil
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
                let url : URL = urlBase.appendingPathComponent("files/products/\(self.wish_products[indexPath.row].id!)/thumb/\(self.wish_products[indexPath.row].picture!)")

                cell.loadingActivityIndicator.startAnimating()
                
                self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.imageProductsLook.image = result.value
                }
                
                return cell
            
            } else {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wishesProductCell", for: indexPath) as! WishesProductCollectionViewCell
                
                cell.indexPath = indexPath
                cell.delegate = self
                cell.nameStoreLabel.text = self.wish_products[indexPath.row].store!.name!
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
                let url : URL = urlBase.appendingPathComponent("files/products/\(self.wish_products[indexPath.row].id!)/thumb/\(self.wish_products[indexPath.row].picture!)")
                let urlStore : URL = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.wish_products[indexPath.row].store!.picture!)")
                
                self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
                    cell.productImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlStore, into: cell.storeImageView) { ( result, _ ) in
                    cell.storeImageView.image = result.value?.circleMask
                }
                
                return cell
                
            }
            
        } else if self.previewType == 2 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "purchaseShareCell", for: indexPath) as! PurchaseShareCollectionViewCell
            
            cell.delegate = self
            cell.indexPath = indexPath
            cell.nameStoreLabel.text = self.posts_products[indexPath.row].store!.name
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts_products[indexPath.row].store!.picture!)")
            
            cell.firstProductPurchase.image = nil
            cell.twoProductPurchaseImageView.image = nil
            cell.threeProductPurchaseImageView.image = nil
            cell.fourProductPurchaseImageView.image = nil
            
            self.manager.loadImage(with: urlStore, into: cell.storeThumbImageView) { ( result, _ ) in
                cell.storeThumbImageView.image = result.value?.circleMask
            }
            
            if self.posts_products[indexPath.row].products_info.count >= 1 {
                
                let url = urlBase.appendingPathComponent("files/orders_itens/\(self.posts_products[indexPath.row].products_info[0].pId!)/thumb/\(self.posts_products[indexPath.row].products_info[0].picture!)")
                
                self.manager.loadImage(with: url, into: cell.firstProductPurchase) { ( result, _ ) in
                    cell.firstProductPurchase.image = result.value
                }
                
                if self.posts_products[indexPath.row].products_info.count > 4 {
                    
                    for i in 0...3 {
                        
                        if i != 0 {
                            
                            let url = urlBase.appendingPathComponent("files/orders_itens/\(self.posts_products[indexPath.row].products_info[i].pId!)/thumb/\(self.posts_products[indexPath.row].products_info[i].picture!)")
                            
                            print(url.absoluteString)
                            
                            if i == 1 {
                                
                                self.manager.loadImage(with: url, into: cell.twoProductPurchaseImageView) { ( result, _ ) in
                                    cell.twoProductPurchaseImageView.image = result.value
                                }
                                
                            } else if i == 2 {
                                
                                self.manager.loadImage(with: url, into: cell.threeProductPurchaseImageView) { ( result, _ ) in
                                    cell.threeProductPurchaseImageView.image = result.value
                                }
                                
                            } else if i == 3 {
                                
                                self.manager.loadImage(with: url, into: cell.fourProductPurchaseImageView) { ( result, _ ) in
                                    cell.fourProductPurchaseImageView.image = result.value
                                }
                                
                            }

                        }
                        
                    }
                    
                } else {
                    
                    for (key, _) in self.posts_products[indexPath.row].products_info.enumerated() {
                        
                        if key != 0 {
                            
                            let url = urlBase.appendingPathComponent("files/orders_itens/\(self.posts_products[indexPath.row].products_info[key].pId!)/thumb/\(self.posts_products[indexPath.row].products_info[key].picture!)")
                            
                            print(url.absoluteString)
                            
                            if key == 1 {
                                
                                self.manager.loadImage(with: url, into: cell.twoProductPurchaseImageView) { ( result, _ ) in
                                    cell.twoProductPurchaseImageView.image = result.value
                                }
                                
                            } else if key == 2 {
                                
                                self.manager.loadImage(with: url, into: cell.threeProductPurchaseImageView) { ( result, _ ) in
                                    cell.threeProductPurchaseImageView.image = result.value
                                }
                                
                            } else if key == 3 {
                                
                                self.manager.loadImage(with: url, into: cell.fourProductPurchaseImageView) { ( result, _ ) in
                                    cell.fourProductPurchaseImageView.image = result.value
                                }
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                
                

            }
            
            return cell
            
        }
        
        // POSTS
        if self.previewPosts == 1 {
            
            
            switch self.posts[indexPath.row].typePost {
                
            case 1:
                
                // =========================
                // ==== POST DE USUARIO ====
                // =========================
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userPostCell", for: indexPath) as! UserPostCollectionViewCell
                cell.postImageView.viewWithTag(99)?.removeFromSuperview()
                
                cell.delegate = self
                cell.indexPathNow = indexPath
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                //cell.commentUserLabel.text = self.posts[indexPath.row].text!
                cell.postDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                cell.postImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                cell.commentUserLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentUserLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentUserLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentUserLabel.text = comments_final
                    
                } else {
                    cell.commentUserLabel.text = self.posts[indexPath.row].text!
                }
                
                // ========================================
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.countLikeLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.countCommentsLabel.isHidden = false
                    cell.countCommentsLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.countCommentsLabel.isHidden = true
                }
                
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                
                if self.posts[indexPath.row].tags.count > 0 {
                    
                    cell.tagInformation.isHidden = false
                    cell.postImageView.viewWithTag(99)?.removeFromSuperview()
                    
                    for (key, tag) in self.posts[indexPath.row].tags.enumerated() {
                        
                        let leftMargin = (cell.postImageView.bounds.width * CGFloat(tag.leftMargin!)) / 100
                        let topMargin = (cell.postImageView.bounds.width * CGFloat(tag.topMargin!)) / 100
                        
                        let tagView = UIView(frame: CGRect(x: leftMargin , y: topMargin, width: (tag.text! as NSString).size(withAttributes: nil).width + 50, height: 25))
                        tagView.backgroundColor = UIColor.darkGray
                        tagView.tag = 99
                        tagView.layer.cornerRadius = 5
                        
                        let tagLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tagView.bounds.width, height: tagView.bounds.height))
                        let gestureRecognizerTagLabel = UITapGestureRecognizer(target: self, action: #selector(openTags(gesture:)))
                        
                        tagLabel.isUserInteractionEnabled = true
                        gestureRecognizerTagLabel.accessibilityHint = "\(indexPath.row),\(key)"
                        tagLabel.addGestureRecognizer(gestureRecognizerTagLabel)
                        tagLabel.text = tag.text!
                        tagLabel.textColor = UIColor.white
                        tagLabel.adjustsFontSizeToFitWidth = true
                        tagLabel.textAlignment = .center
                        
                        tagView.addSubview(tagLabel)
                        tagView.isHidden = true
                        
                        
                        cell.postImageView.addSubview(tagView)
                        
                    }
                    
                } else {
                    cell.tagInformation.isHidden = true
                    cell.postImageView.viewWithTag(99)?.removeFromSuperview()
                }
                
                
                
                self.manager.loadImage(with: url, into: cell.postImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.postImageView.image = result.value
                }
                
                
                self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                    cell.userImageView.image = result.value?.circleMask
                }
                
                
                return cell
                
            case 5:
                
                
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareBuyCell", for: indexPath) as! ShareBuyCollectionViewCell
                
                cell.delegate = self
                cell.delegateBase = self
                cell.indexPathNow = indexPath
                
                if self.posts[indexPath.row].products.count > 1 {
                    cell.collectionView.isHidden = false
                    cell.collectionView.reloadData()
                } else {
                    cell.collectionView.isHidden = true
                }
                
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameStoreTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameStoreBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.nameStoreButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
                cell.storePostImageView.image = nil
                
//                if self.posts[indexPath.row].share_text == "" || self.posts[indexPath.row].share_text == nil {
//                    cell.commentStoreLabel.text = self.posts[indexPath.row].text!
//                } else {
//                    cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
//                }
                
                // =========== Hashtags/Mention ===========
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    
                    if self.posts[indexPath.row].share_text == "" || self.posts[indexPath.row].share_text == nil {
                        
                        if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                            
                            let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                                return value != "" ? true : false
                            })
                            
                            
                            var isActiveMention = false
                            
                            for word in comment {
                                
                                let information_comments = word.components(separatedBy: ":")
                                
                                if (word.contains("@id:") && word.contains("@storename:")) {
                                    
                                    if information_comments.last! == mention && isActiveMention == false {
                                        
                                        isActiveMention = true
                                        self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                        
                                    }
                                    
                                    
                                } else if (word.contains("@id:") && word.contains("@username:"))  {
                                    
                                    if information_comments.last! == mention && isActiveMention == false {
                                        
                                        isActiveMention = true
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                        pageUserViewController.isMyProfile = false
                                        pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                        
                                        self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                        
                                    }
                                    
                                }
                                
                                
                            }
                            
                            
                        }
                        
                        
                        
                    } else {
                        
                        if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                            
                            let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                                return value != "" ? true : false
                            })
                            
                            
                            var isActiveMention = false
                            
                            for word in comment {
                                
                                let information_comments = word.components(separatedBy: ":")
                                
                                if (word.contains("@id:") && word.contains("@storename:")) {
                                    
                                    if information_comments.last! == mention && isActiveMention == false {
                                        
                                        isActiveMention = true
                                        self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                        
                                    }
                                    
                                    
                                } else if (word.contains("@id:") && word.contains("@username:"))  {
                                    
                                    if information_comments.last! == mention && isActiveMention == false {
                                        
                                        isActiveMention = true
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                        pageUserViewController.isMyProfile = false
                                        pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                        
                                        self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                        
                                    }
                                    
                                }
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    
                    
                    
                    
                    
                })
                
                
                if self.posts[indexPath.row].share_text == "" || self.posts[indexPath.row].share_text == nil {
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        var comments_final : String = ""
                        
                        for word in comment {
                            
                            if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                                let information_comments = word.components(separatedBy: ":")
                                comments_final += "@\(information_comments.last!)"
                            } else {
                                comments_final += word
                            }
                            
                        }
                        
                        cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        cell.commentStoreLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    }
                    
                    
                    
                } else {
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        var comments_final : String = ""
                        
                        for word in comment {
                            
                            if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                                let information_comments = word.components(separatedBy: ":")
                                comments_final += "@\(information_comments.last!)"
                            } else {
                                comments_final += word
                            }
                            
                        }
                        
                        cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        cell.commentStoreLabel.text = self.posts[indexPath.row].share_text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    }
                }
                
                
                // ========================================
                
                
                
                cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                cell.informationLabel.text = "realizou uma compra na"
                
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeShareBuyImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeShareBuyImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                //let urlBasePost = URL(string: "http://www.wishare.com.br/code/")!
                
                let url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
                let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                print(url.absoluteString)
                
                self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.storePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlStore, into: cell.storeThumbImageView) { ( result, _ ) in
                    cell.storeThumbImageView.image = result.value?.circleMask
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                    cell.userThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
            case 6:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sharePostUserCell", for: indexPath) as! SharePostUserCollectionViewCell
                
                cell.delegate = self
                cell.delegateBase = self
                cell.indexPathNow = indexPath
                
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userMameShareButton.setTitle(self.posts[indexPath.row].share_user_username, for: .normal)
                //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
                cell.informationLabel.text = "compartilhou a publicação de"
                cell.shareUserPostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
                cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                }
                
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.shareCommentLabel.text = comments_final
                    
                } else {
                    cell.shareCommentLabel.text = self.posts[indexPath.row].text!
                }
                
                // ========================================
                
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                let urlShareProfile = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].share_user_picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                print(url.absoluteString)
                
                self.manager.loadImage(with: url, into: cell.shareUserPostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.shareUserPostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlShareProfile, into: cell.userShareThumbImageView) { ( result, _ ) in
                    cell.userShareThumbImageView.image = result.value?.circleMask
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                    cell.userThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
            case 7:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareStoreUserCell", for: indexPath) as! ShareStoreUserCollectionViewCell
                
                cell.delegate = self
                cell.delegateBase = self
                cell.indexPathNow = indexPath
                
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.storeNameShareButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
                //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
                cell.informationLabel.text = "compartilhou a publicação de"
                cell.shareStorePostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
                cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                }
                
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.shareCommentLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    cell.shareCommentLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                }
                
                // ========================================
                
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.shareStorePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                    cell.storeShareThumbImageView.image = result.value?.circleMask
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                    cell.userThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
            case 8:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareStoreProductUserCell", for: indexPath) as! ShareStoreProductUserCollectionViewCell
                
                cell.delegate = self
                cell.delegateBase = self
                cell.indexPathNow = indexPath
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.storeNameShareButton.setTitle(self.posts[indexPath.row].share_store_name, for: .normal)
                //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
                cell.informationLabel.text = "compartilhou a publicação de"
                cell.shareStorePostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
                cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                }
                
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.shareCommentLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    cell.shareCommentLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                }
                
                // ========================================
                
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.shareStorePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                    cell.storeShareThumbImageView.image = result.value?.circleMask
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                    cell.userThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
                
            case 9:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareStoreLookUserCell", for: indexPath) as! ShareStoreLookCollectionViewCell
                
                cell.delegate = self
                cell.delegateBase = self
                cell.indexPathNow = indexPath
                cell.collectionView.reloadData()
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.storeNameShareButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
                //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
                cell.informationLabel.text = "compartilhou a publicação de"
                cell.shareStorePostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
                cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
                }
                
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.shareCommentLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    cell.shareCommentLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                }
                
                // ========================================
                
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeSharePostUserImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.shareStorePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                    cell.storeShareThumbImageView.image = result.value?.circleMask
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                    cell.userThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
                
            default:
                
                // ============== 10 ===============
                // === COMPARTILHAMENTO SEM POST ===
                // =================================
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareProductUserCell", for: indexPath) as! ShareProductCollectionViewCell
                
                cell.delegate = self
                cell.delegateBase = self
                cell.indexPathNow = indexPath
                
                cell.loadingActivityIndicator.startAnimating()
                cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
                cell.storeNameShareButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
                //cell.commentStoreLabel.text = self.posts[indexPath.row].text!
                cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
                cell.informationLabel.text = "compartilhou um produto de"
                cell.shareStorePostImageView.image = nil
                
                // =========== Hashtags/Mention ===========
                
                cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
                
                cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = hashtag
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                })
                
                cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                })
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                }
                
                // ========================================
                
                // - CURTIDAS
                if self.posts[indexPath.row].likes! > 0 {
                    
                    cell.viewLikes.isHidden = false
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    
                    cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                    
                } else {
                    
                    cell.viewLikes.isHidden = true
                    
                    cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                    cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    
                }
                
                
                // - COMENTARIOS
                if self.posts[indexPath.row].comments! > 0 {
                    cell.commentsStoreLabel.isHidden = false
                    cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
                } else {
                    cell.commentsStoreLabel.isHidden = true
                }
                
                if self.posts[indexPath.row].ilikes == "1" {
                    cell.likeShareProductImageView.image = UIImage(named: "Like Filled")
                } else {
                    cell.likeShareProductImageView.image = UIImage(named: "Like")
                }
                
                if self.posts[indexPath.row].iwish == "1" {
                    cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                } else {
                    cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                }
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                
                let pictureString = self.posts[indexPath.row].picture?.components(separatedBy: "/")
                let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/thumb/\(pictureString!.last!)")
                
//                let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
                let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
                
                self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                    cell.loadingActivityIndicator.stopAnimating()
                    cell.shareStorePostImageView.image = result.value
                }
                
                self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                    cell.storeShareThumbImageView.image = result.value?.circleMask
                }
                
                self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                    cell.userThumbImageView.image = result.value?.circleMask
                }
                
                return cell
                
                
            }
            
            
            
            
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        cell.imageProductsLook.image = nil
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
        let url : URL!
        
        if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture! != "" {
            let pictureString = self.posts[indexPath.row].picture?.components(separatedBy: "/")
            url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/thumb/\(pictureString!.last!)")
        } else {
            if self.posts[indexPath.row].typePost == 5 {
                url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
            }
            
        }
        
        cell.loadingActivityIndicator.startAnimating()
        
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        return cell

        
        
        
    }
    
}

extension PageUserViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 10 {

            // TAMANHO PAGE USER
            
            if self.previewType == 0 {
                
                if self.posts.count == 0 {
                    return CGSize(width: self.view.bounds.width, height: 421 + 150)
                }
                
                let lines = (self.posts.count / 2) + (self.posts.count % 2)
                
                if self.previewPosts == 0 {
                    
                    let sizeForCell = (self.view.bounds.width / 2) - 2.5
                    
                    if self.user != nil {
                        if self.posts.count == 0 && self.user!.isFollowing! == "false" && self.user!.privateW! == "1" && self.isMyProfile == false {
                            return CGSize(width: self.view.bounds.width, height: 421 + 150)
                        } else {
                            return CGSize(width: self.view.bounds.width, height: 431 + (CGFloat(lines) * CGFloat(sizeForCell)))
                        }
                    } else {
                        return CGSize(width: self.view.bounds.width, height: 431 + (CGFloat(lines) * CGFloat(sizeForCell)))
                    }

                    
                } else {
            
                    var totalSizePosts : CGFloat = 0.0
                    
                    for post in self.posts {
                        
                        let sizeForComments = (post.comments! == 0) ? 30.0 : 0.0
                        let sizeForLikes = (post.likes! == 0) ? 30.0 : 0.0
                        
                        if post.typePost == 1 {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(275.0 - (sizeForComments + sizeForLikes)))
                        } else if post.typePost == 5 {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(325.0 - (sizeForComments + sizeForLikes)))
                        } else if post.typePost == 6 {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(357.0 - (sizeForComments + sizeForLikes)))
                        } else if post.typePost == 7 {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(365.0 - (sizeForComments + sizeForLikes)))
                        } else if post.typePost == 8 {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(357.0 - (sizeForComments + sizeForLikes)))
                        } else if post.typePost == 9 {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(355.0 - (sizeForComments + sizeForLikes)))
                        } else {
                            totalSizePosts += CGFloat(self.view.bounds.width + CGFloat(320.0 - (sizeForComments + sizeForLikes)))
                        }
                        
                    }
                    
                    return CGSize(width: self.view.bounds.width, height: 451.0 + totalSizePosts)
                }
            
                
            } else if self.previewType == 1 {
                
                if self.wish_products.count == 0 {
                    return CGSize(width: self.view.bounds.width, height: 421 + 150)
                }
                
                let lines = (self.wish_products.count / 2) + (self.wish_products.count % 2)
                
                if self.previewPosts == 0 {
                    
                    let sizeForCell = (self.view.bounds.width / 2) - 2.5
                    return CGSize(width: self.view.bounds.width, height: 431 + (CGFloat(lines) * CGFloat(sizeForCell)))
                
                } else {
                    
                    return CGSize(width: self.view.bounds.width, height: 421 + ((self.collectionView.bounds.width + 93) * CGFloat(self.wish_products.count)))
                
                }
                
            } else {
                
                if self.posts_products.count == 0 {
                    return CGSize(width: self.view.bounds.width, height: 421 + 150)
                }
            
                let lines = (self.posts_products.count / 2) + (self.posts_products.count % 2)
                
                if self.previewPosts == 0 {
                    let sizeForCell = 61 + ((self.view.bounds.width / 2) - 2.5)
                    return CGSize(width: self.view.bounds.width, height: 421 + (CGFloat(lines) * CGFloat(sizeForCell)))
                } else {
                    let sizeForCell = 61 + self.view.bounds.width - 2.5
                    return CGSize(width: self.view.bounds.width, height: 421 + (CGFloat(self.posts_products.count) * CGFloat(sizeForCell)))
                }
                
            }

        }
        
        if self.previewType == 1 {
            if self.previewPosts == 0 {
                return CGSize(width: (self.view.bounds.width / 2) - 1.25, height: (self.view.bounds.width / 2) - 2.5)
            } else {
                return CGSize(width: self.view.bounds.width, height: self.collectionView.bounds.width + 93)
            }
        }
        
        
        // ABA COMPRAS
        if self.previewType == 2 {
            if self.previewPosts == 0 {
                return CGSize(width: (self.view.bounds.width / 2) - 1.25, height: 61 + (self.view.bounds.width / 2) - 2.5)
            } else {
                return CGSize(width: self.view.bounds.width - 1.25, height: 61 + (self.view.bounds.width * 1.05) - 2.5)
            }
        }
        
        if self.previewPosts == 1 {
            
            // Verificando se existe comentarios e curtidas
            var base : CGFloat = 0.0
            if self.posts[indexPath.row].comments! == 0 { base += 30.0 }
            if self.posts[indexPath.row].likes! == 0 { base += 30.0 }
            
            switch self.posts[indexPath.row].typePost {
            case 1:
                // Usuario Simples - OK
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (275 - base))
            case 5:
                // Compartilhamento de compra - OK
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (325 - base))
            case 6:
                // Post Usuario - OK
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (357 - base))
            case 7:
                // Post Loja Simples - OK
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (365 - base))
            case 8:
                // Post Loja Produto - OK
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (357 - base))
            case 9:
                // Post Loja Look - OK
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (355 - base))
            default:
                return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (320 - base))
            }
            
            
        }
        
        
        return CGSize(width: (self.view.bounds.width / 2) - 1.25, height: (self.view.bounds.width / 2) - 2.5)
        
    }
    
    // ======================================================
    // ==================== LOADING =========================
    // ======================================================
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if self.isLoading {
            return CGSize.zero
        }
        
        return CGSize(width: collectionView.bounds.size.width, height: 55.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter {
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
            self.footerView = footerView
            self.footerView?.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            return footerView
            
        } else {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold = 100.0
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold = Float((diffHeight - frameHeight))/Float(threshold);
        
        triggerThreshold = min(triggerThreshold, 0.0)
        let pullRatio = min(fabs(triggerThreshold),0.4);
        
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        
        if pullRatio >= 0.4 {
            self.footerView?.animateFinal()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        
        if pullHeight >= 0.0  && pullHeight <= 1.0 {
            if (self.footerView?.isAnimatingFinal)! {
            
                self.isLoading = true
                self.footerView?.startAnimate()
                
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                    
                    if !self.isMyProfile {
                        
                        if self.previewType == 0 {

                            
                            if !(self.user!.isFollowing! == "false" && self.user!.privateW! == "1" && self.isMyProfile == false) {
                                
                                WishareAPI.getPostsUser(idUser: self.idUser, timestamp: self.posts.last?.created, { (posts : [Post], error : Error?) in
                                    if error == nil {
                                        
                                        self.posts.append(contentsOf: posts)
                                        self.collectionView.reloadData()
                                        self.isLoading = false
                                        
                                    }
                                })
                                
                            }

                        
                        } else {
                            
                            self.collectionView.reloadData()
                            self.isLoading = false
                            
                        }
                        
                    } else {
                        
                        if self.previewType == 0 {
                            
                            WishareCoreData.getFirstUser({ (user : UserActive?, error : Error?) in
                                if error == nil && user != nil {
                                    WishareAPI.getPostsUser(idUser: user!.id!, timestamp: self.posts.last?.created, { (posts : [Post], error : Error?) in
                                        if error == nil {
                                            
                                            self.posts.append(contentsOf: posts)
                                            self.collectionView.reloadData()
                                            self.isLoading = false
                                            
                                        }
                                    })
                                }
                            })
                            
                        } else {
                            
                            self.collectionView.reloadData()
                            self.isLoading = false
                            
                        }
                        

                        
                    }
                    
                })
                
            }
        }
    }
    
    // ======================================================
    // ======================================================
    // ======================================================
    
}

extension PageUserViewController : WishesProductDelegate {
    
    func openProductWishes(cell: WishesProductCollectionViewCell, indexPath: IndexPath, sender: UIButton) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.wish_products[indexPath.row].pId!
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
        
    }
    
    func openStoreWishes(cell: WishesProductCollectionViewCell, indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
        pageStoreViewController.idStore = self.wish_products[indexPath.row].storeId!
        
        self.navigationController?.pushViewController(pageStoreViewController, animated: true)

        
    }
    
    func openProductImage(cell: WishesProductCollectionViewCell, indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.wish_products[indexPath.row].pId!
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
}

extension PageUserViewController : PageUserDelegate {
    
    func showImagePictureUser() {
        
        self.tabBarController?.tabBar.isHidden = true
        
        let options = CollieGalleryOptions()
        options.excludedActions = [UIActivityType.assignToContact, UIActivityType.copyToPasteboard, UIActivityType.print]
        
        let gallery = CollieGallery(pictures: self.pictures, options: options, theme: nil)
        gallery.delegate = self
        
        gallery.presentInViewController(self, transitionType: CollieGalleryTransitionType.zoom(fromView: (self.collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as! PageUserCollectionViewCell).userImageView , zoomTransitionDelegate: self))
        
    }
    
    func editOrFollow(_ sender: UIButton, cell: PageUserCollectionViewCell) {
    
        if self.isMyProfile {
            
            let storyboard = UIStoryboard(name: "Setting", bundle: nil)
            let settingTableViewController = storyboard.instantiateViewController(withIdentifier: "settingUserID") as! SettingTableViewController
            self.navigationController?.pushViewController(settingTableViewController, animated: true)
            
        } else {
            
            if self.user!.isFollowing! == "true" {
                
                let alertController = UIAlertController(title: "Deseja deixar de seguir \(self.user!.username!)?", message: nil, preferredStyle: .actionSheet)
                let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                    
                    WishareAPI.followUser(self.user!.id!, isFollow: false, { ( _ ) in })
                    self.user!.isFollowing = "false"
                    self.collectionView.reloadData()
                    
                })
                
                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                
                alertController.addAction(confirmAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            } else if self.user!.isFollowing! == "false" {
                
                if self.user!.privateW! == "1" {
                    
                    WishareAPI.followUser(self.user!.id!, isFollow: true, { ( _ ) in })
                    self.user!.isFollowing = "waiting"
                    self.collectionView.reloadData()
                    
                } else {
                    
                    WishareAPI.followUser(self.user!.id!, isFollow: true, { ( _ ) in })
                    self.user!.isFollowing = "true"
                    self.collectionView.reloadData()
                    
                }
                
            } else if self.user!.isFollowing == "waiting" {
                
                let alertController = UIAlertController(title: "Deseja deixar de seguir \(self.user!.username!)", message: nil, preferredStyle: .actionSheet)
                let confirmAction = UIAlertAction(title: "Confirmar", style: .default, handler: { ( _ ) in
                    
                    WishareAPI.followUser(self.user!.id!, isFollow: false, { ( _ ) in })
                    self.user!.isFollowing = "false"
                    self.collectionView.reloadData()
                    
                })
                
                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                
                alertController.addAction(confirmAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            
        }
        
    }
    
    func openFollowers() {
        self.performSegue(withIdentifier: "followersUserSegue", sender: self.user!.id!)
    }
    
    func openFollowing() {
        self.performSegue(withIdentifier: "followUserSegue", sender: self.user!.id!)
    }
    
    func openWips() {
        self.performSegue(withIdentifier: "followWipsUserSegue", sender: self.user!.id!)
    }
    
    func openStore() {
        self.performSegue(withIdentifier: "followStoreUserSegue", sender: self.user!.id!)
    }
    
    func changeModeOption( _ sender : UIButton) {
        
        if sender.tag == 1 {
            self.previewPosts = 0
        } else {
            self.previewPosts = 1
        }
        
    }
    
    func changeTypeOption(_ type : Int) {
        
        self.previewType = type
        
    }
    
}

extension PageUserViewController : PurchaseShareDelegate {
    
    func openStoreByLabel(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
        pageStoreViewController.idStore = self.posts_products[indexPath.row].store!.id!
        
        self.navigationController?.pushViewController(pageStoreViewController, animated: true)
        
    }
    
    func openStoreByThumb(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
        pageStoreViewController.idStore = self.posts_products[indexPath.row].store!.id!
        
        self.navigationController?.pushViewController(pageStoreViewController, animated: true)
        
    }
    
    func openProductsByImageView(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let buyShareViewController = storyboard.instantiateViewController(withIdentifier: "buyShareID") as! BuyShareViewController
        buyShareViewController.post = self.posts_products[indexPath.row]
        
        self.navigationController?.pushViewController(buyShareViewController, animated: true)
        
    }
    
}

extension PageUserViewController : UserPostCollectionViewCellDelegate {
    
    func moreUserPost(cell: UserPostCollectionViewCell) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                if self.posts[cell.indexPathNow.row].user_id == user!.id {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                        self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                    })
                    
                    let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                            
                            WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                    
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                        
                                        //self.navigationController?.popViewController(animated: true)
                                        let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                        (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                        
                        alertController.addAction(okAction)
                        alertController.addAction(cancelarAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(editarAction)
                    alertController.addAction(excluirAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                        RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                        
                        WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                            
                            if error == nil {
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            
                        })
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(denunciarAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
            }
            
        }
        
        
    }
    
    func likeUserPost(cell: UserPostCollectionViewCell) {
        
        
        
        if self.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.posts[cell.indexPathNow.row].ilikes = "0"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.posts[cell.indexPathNow.row].ilikes = "1"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
        }
        
    }
    
    func commentUserPost(cell: UserPostCollectionViewCell) {
        self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
    func shareUserPost(cell: UserPostCollectionViewCell, _ indexPath : IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[cell.indexPathNow.row].id!)/\(self.posts[cell.indexPathNow.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[cell.indexPathNow.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[cell.indexPathNow.row].id!)/\(self.posts[cell.indexPathNow.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func giftUserPost(cell: UserPostCollectionViewCell) {
        
        if self.posts[cell.indexPathNow.row].iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.posts[cell.indexPathNow.row].iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.posts[cell.indexPathNow.row].iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
        
        
        
    }
    
    func userImageUserPost(cell: UserPostCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[cell.indexPathNow.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func postImageUserPost(cell: UserPostCollectionViewCell) {
        print("CLICOU NA IMAGEM DO POST")
        self.performSegue(withIdentifier: "showImagePost", sender: cell.postImageView.image)
    }
    
    func userNameUserPost(cell: UserPostCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[cell.indexPathNow.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
    }
    
    func tagInformationUserPost(cell: UserPostCollectionViewCell) {
        print("CLICOU NA TAG")
        
        if cell.tagOnOrOff == false {
            
            cell.tagImageView.image = UIImage(named: "tag-on")
            cell.tagOnOrOff = true
            cell.tagInformation.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 0.5)
            
            for view in cell.postImageView.subviews {
                
                if view.tag == 99 {
                    view.isHidden = false
                }
                
            }
            
            
            
        } else {
            
            cell.tagImageView.image = UIImage(named: "tag-off")
            cell.tagOnOrOff = false
            cell.tagInformation.backgroundColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 0.5)
            
            for view in cell.postImageView.subviews {
                
                if view.tag == 99 {
                    view.isHidden = true
                }
                
            }
            
            
        }
        
    }
    
    func viewLikes(cell: UserPostCollectionViewCell) {
        self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
}

extension PageUserViewController : ShareStoreProductUserCollectionViewCellDelegate {
    
    func buyShareStore(cell: ShareStoreProductUserCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
        
    }
    
    func postShareStore(cell: ShareStoreProductUserCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
        
    }
    
    func openUserButtonShareStoreProduct(_ sender: UIButton, _ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserImageShareStoreProduct(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension PageUserViewController : ShareProductCollectionViewCellDelegate {
    func showMoreCommentsShareProductStore(cell: ShareProductCollectionViewCell, indexPath: IndexPath) {
        self.posts[indexPath.row].showMoreComments = true
        self.collectionView.reloadData()
    }
    
    
    func buyShareStoreProduct(cell: ShareProductCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    func postShareStoreProduct(cell: ShareProductCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    func openUserImageShareStoreProdutNotFound(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonShareStoreProdutNotFound(_ sender: UIButton, _ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension PageUserViewController : ShareStoreLookCollectionViewCellDelegate {
    
    func numberOfItemsInSectionShareLook(_ cellForLook : ShareStoreLookCollectionViewCell) -> Int {
        return self.posts[cellForLook.indexPathNow.row].products.count
    }
    
    func cellForItemShareLook(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook : ShareStoreLookCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[cellForLook.indexPathNow.row].products[indexPath.row]["url"]!)")
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItemShareLook(_ collection: UICollectionView, _ indexPath: IndexPath) {
    
        var isValidate : Bool = false
        
        for prod_info in self.posts[Int(collection.accessibilityHint!)!].products_info {
            
            if prod_info.id! == self.posts[Int(collection.accessibilityHint!)!].products[indexPath.row]["id"] {
                isValidate = true
            }
            
        }
        
        if !isValidate {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[Int(collection.accessibilityHint!)!].products[indexPath.row]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
    }
    
    func buyShareLookStoreImage(_ indexPath: IndexPath) {
        
        if self.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookSegue", sender: self.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func buyLookStoreButton(_ sender: UIButton, _ indexPath: IndexPath) {
        
        if self.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookSegue", sender: self.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    func openUserButtonShareStoreLook(_ sender: UIButton, _ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserImageShareStoreLook(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension PageUserViewController : SharePostUserCollectionViewCellDelegate {
    
    func openUserImageSharePostUser(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonSharePostUser(_ sender: UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserShareSharePostUser(_ sender : UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].share_user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserShareImageSharePostUser(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].share_user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension PageUserViewController : ShareStoreUserCollectionViewCellDelegate {
    
    func openUserImageShareStore(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonShareStore(_ sender: UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension PageUserViewController : ShareBuyCollectionViewCellDelegate {
    
    func numberOfItemsInSectionShare(_ cellForLook: ShareBuyCollectionViewCell) -> Int {
        return self.posts[cellForLook.indexPathNow.row].products.count - 1
    }
    
    func cellForItemShare(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook: ShareBuyCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
        let url : URL!
        
        if indexPath.row == 0 {
            url = urlBase.appendingPathComponent("\(self.posts[cellForLook.indexPathNow.row].products[1]["url"]!)")
        } else {
            url = urlBase.appendingPathComponent("\(self.posts[cellForLook.indexPathNow.row].products[indexPath.row + 1]["url"]!)")
        }
        
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItemShare(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellIndexPath : IndexPath) {
        print("CLICOU CELULA \(indexPath.row)")
        self.performSegue(withIdentifier: "buyShareSegue", sender: self.posts)
        
    }
    
    func storePostImage(_ indexPath: IndexPath) {
        self.performSegue(withIdentifier: "buyShareSegue", sender: self.posts)
    }
    
    
    func openUserImage(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButton(_ sender: UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
}

extension PageUserViewController : WishareBasePostDelegate {
    
    func more(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            //self.navigationController?.popViewController(animated: true)
                                            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            //self.navigationController?.popViewController(animated: true)
                                            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            //self.navigationController?.popViewController(animated: true)
                                            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            //self.navigationController?.popViewController(animated: true)
                                            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            //self.navigationController?.popViewController(animated: true)
                                            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            //self.navigationController?.popViewController(animated: true)
                                            let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                                            (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func like(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                (self.collectionView.cellForItem(at: indexPath) as! PageUserCollectionViewCell).collectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func share(cell: UICollectionViewCell, indexPath : IndexPath) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            //let cell = cell as! ShareBuyCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            //alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            //let cell = cell as! SharePostUserCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            //let cell = cell as! ShareStoreUserCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            //let cell = cell as! ShareStoreProductUserCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            //let cell = cell as! ShareStoreLookCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            //let cell = cell as! ShareProductCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 100, y: result.value!.size.height - 40, width: 90, height: 30), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            print("N FUNCIONOU")
        }
        
        
    }
    
    func wishlist(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else {
            print("N FUNCIONOU")
        }
        
        
    }
    
    func showLikes(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func showComments(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func thumbStorePostImageView(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            //let cell = cell as! SharePostUserCollectionViewCell
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func userNameStorePostLabel(cell: UICollectionViewCell) {
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            //let cell = cell as! SharePostUserCollectionViewCell
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else {
            print("N FUNCIONOU")
        }
    }
}

extension PageUserViewController : CollieGalleryDelegate {
    
    func gallery(_ gallery: CollieGallery, indexChangedTo index: Int) {
        
    }
    
}

extension PageUserViewController : CollieGalleryZoomTransitionDelegate {
    
    func zoomTransitionContainerBounds() -> CGRect {
        return self.collectionView.frame
    }
    
    func zoomTransitionViewToDismissForIndex(_ index: Int) -> UIView? {
        self.tabBarController?.tabBar.isHidden = false
        return (self.collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as! PageUserCollectionViewCell).userCloneImageView
        //return nil
    }
    
}

struct YellowIndicator: Indicator {
    var view: IndicatorView
    
    func startAnimatingView() { view.isHidden = false }
    func stopAnimatingView() { view.isHidden = true }
    
    init() {
        let indicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
        indicatorView.color = UIColor(red:1.00, green:0.80, blue:0.03, alpha:1.0)
        view = indicatorView
    }
}
