//
//  PhotoViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Fusuma
import RappleProgressHUD

class PhotoViewController: UIViewController {

    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var photoUser : UIImage?
    
    var tags : [[String : Any]] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(myTags(notification:)), name: Notification.Name.init("myTags"), object: nil)
        
        self.photoImageView.image = photoUser
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "TagCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "tagCell")
        
    }

    override func viewDidAppear(_ animated: Bool) {
        self.collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func exitPhoto(_ sender: UIBarButtonItem) {
        
        let fusumaController = FusumaViewController()
        fusumaController.delegate = self
        fusumaTintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        self.present(fusumaController, animated: true, completion: nil)
        
    }
    
    @IBAction func publishPhoto(_ sender: UIBarButtonItem) {
        
        self.view.endEditing(true)
        
        let atributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Publicando...", attributes: atributes)
    
        // Convertendo a image tirada com a camera para a orientacao correta
        let fixed = self.photoUser?.fixOrientation()
    
        if let base64String = UIImageJPEGRepresentation(fixed! , 1)?.base64EncodedString() {
            
            var params : [String : Any] = [:]
            
            params["text"] = self.messageTextView.text
            params["picture"] = base64String
            params["tags"] = self.tags

            WishareAPI.publishPost(params, { (error : Error?) in
                
                if error == nil {
                    print("FUNCIONOU PUBLICADO !!!!")
                } else {
                    print("ERRO NA PUBLICACAO")
                }

                
                RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Publicado!", completionTimeout: 1)
                
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                    
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc : TabBarViewController = storyboard.instantiateViewController(withIdentifier: "tabsID") as! TabBarViewController
                    
                    vc.modalTransitionStyle = .flipHorizontal
                    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                    appDelegate.window?.rootViewController = vc
                    
                    
                })
                
            })
            
        }
        
        
    }
    
    @IBAction func addTag(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "addTagSegue", sender: photoUser)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "addTagSegue" {
            
            let tagViewController = segue.destination as! TagViewController
            tagViewController.photoImage = sender as? UIImage
            tagViewController.tags = tags
            
        }
        
    }
    
    @objc func myTags(notification : Notification) {
        
        let tags : [[String : Any]] = notification.object as! [[String : Any]]
        self.tags = tags
        
    }
    
}

extension PhotoViewController : FusumaDelegate {
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
    }
    
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "navPreviewID") as! UINavigationController
        
        if let viewcontroller = navigationController.viewControllers.first as? PreviewPhotoViewController {
            viewcontroller.imagePhoto = image
        }
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.set(rootViewController: navigationController, withTransition: transition)
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        
    }
    
    func fusumaClosed() {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : TabBarViewController = storyboard.instantiateViewController(withIdentifier: "tabsID") as! TabBarViewController
        
        vc.modalTransitionStyle = .flipHorizontal
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController = vc
        self.dismiss(animated: false, completion: nil)
        
    }
    
}

extension PhotoViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //self.tags.remove(at: indexPath.row)
        //collectionView.deleteItems(at: [indexPath])
        
    }
    
}

extension PhotoViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Quantidade de Tags: \(self.tags.count)")
        return self.tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCell", for: indexPath) as! TagCollectionViewCell
        
        cell.delegate = self
        cell.indexPath = indexPath
        
        //cell.nameTagLabel.layer.cornerRadius = 12.5
        //cell.nameTagLabel.layer.masksToBounds = true
        cell.nameTagLabel.adjustsFontSizeToFitWidth = true
        cell.nameTagLabel.text = self.tags[indexPath.row]["text"] as? String
        cell.tagView.layer.cornerRadius = 12.5
        cell.tagView.layer.masksToBounds = true
        
        return cell
        
    }
    
}

extension PhotoViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.tags[indexPath.row]["text"] as! NSString).size(withAttributes: nil).width + 60, height: 25)
    }
    
}

extension PhotoViewController : TagCollectionDelegate {
    
    func closeTag( _ indexPath : IndexPath) {
        self.tags.remove(at: indexPath.row)
        self.collectionView.deleteItems(at: [indexPath])
        
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { ( _ ) in
            self.collectionView.reloadData()
        }
        
        
    }
    
}
