//
//  FeedViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/24/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import ENMBadgedBarButtonItem
import SwiftyAttributes
import ActiveLabel

class FeedViewController: UIViewController {
    
    // FINALIZAR HOJE (28/04)
    // - Refresh Control (Top and Bottom) - OK
    // - More (Alert)
    // - Like (quantidade e ilike)
    // - Comentarios (?)
    

    // MARK: - Outlets
    @IBOutlet weak var feedCollectionView: UICollectionView!
    @IBOutlet weak var searchStoresAndUsersView: UIView!
    
    // MARK: - Properties
    var posts : [Post] = []
    var newPosts : [Post] = []
    var loader : Loader!
    var manager : Manager!
    
    var stores: [Store]?
    var products: [[Product]]?
    
    
    let refreshControll = UIRefreshControl()
    var isLoading : Bool = false
    let footerViewReuseIdentifier = "footerViewReuseIdentifier"
    var footerView : FooterCollectionReusableView?
    
    var barButton : BadgedBarButtonItem!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var lastPost : String?
    
    var moreStoresInCarousel: Bool = true
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { ( _ ) in
            if let number = UserDefaults.standard.object(forKey: "numberBagde") as? String {
                
                if number == "0" {
                    self.tabBarController?.tabBar.items?[3].badgeValue = nil
                } else {
                    self.tabBarController?.tabBar.items?[3].badgeValue = number
                    self.tabBarController?.tabBar.items?[3].badgeColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                }
                
                
            }
        }
        
        if let type = UserDefaults.standard.object(forKey: "remoteNotificationDidReceive") as? String {
            
            if type == "post" {
                self.tabBarController?.selectedIndex = 3
            } else {
                
                self.tabBarController?.selectedIndex = 4
                
                let storyboard = UIStoryboard(name: "Setting", bundle: nil)
                let ordersViewController = storyboard.instantiateViewController(withIdentifier: "myOrderID") as! OrdersViewController
                self.tabBarController?.selectedViewController?.childViewControllers[0].navigationController?.pushViewController(ordersViewController, animated: true)
                
            }

            UserDefaults.standard.removeObject(forKey: "remoteNotificationDidReceive")
            
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(remoteNotificationBadge(notification:)), name: Notification.Name.init("remoteNotificationBadge"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(remoteNotificationDidReceive(notification:)), name: Notification.Name.init("remoteNotificationDidReceive"), object: nil)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        self.refreshControll.tintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        self.feedCollectionView.refreshControl = self.refreshControll
        self.refreshControll.addTarget(self, action: #selector(refreshShow), for: .valueChanged)
        
        
        self.feedCollectionView.delegate = self
        self.feedCollectionView.dataSource = self
        
        self.feedCollectionView.register(UINib(nibName: "UserPostCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userPostCell")
        self.feedCollectionView.register(UINib(nibName: "SimpleStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "simpleStoreCell")
        self.feedCollectionView.register(UINib(nibName: "ProductStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "productStoreCell")
        self.feedCollectionView.register(UINib(nibName: "LookStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "lookStoreCell")
        self.feedCollectionView.register(UINib(nibName: "ShareBuyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareBuyCell")
        self.feedCollectionView.register(UINib(nibName: "SharePostUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "sharePostUserCell")
        self.feedCollectionView.register(UINib(nibName: "ShareStoreUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareStoreUserCell")
        self.feedCollectionView.register(UINib(nibName: "ShareStoreProductUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareStoreProductUserCell")
        self.feedCollectionView.register(UINib(nibName: "ShareStoreLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareStoreLookUserCell")
        self.feedCollectionView.register(UINib(nibName: "ShareProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "shareProductUserCell")
        
        self.feedCollectionView.register(UINib(nibName: "StoreSugestionCollectionViewCell",  bundle: nil), forCellWithReuseIdentifier: "storeSugestionCell")
        
        self.feedCollectionView.register(UINib(nibName: "WisharePostCollectionViewCell",  bundle: nil), forCellWithReuseIdentifier: "wisharePostCell")
        
        self.feedCollectionView.register(UINib(nibName: "TesteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "testeCell")
        
        self.feedCollectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerViewReuseIdentifier")
        
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        
        Timer.scheduledTimer(withTimeInterval: 30, repeats: true) { (timer : Timer) in
            print("============= TIMER NEW POST =============")
            
            if let first = self.posts.first?.created {
                
                WishareAPI.existsNewPosts(first, { ( quantityPosts : Int?, error : Error?) in
                    
                    if error == nil {
                        
                        if let quantity = quantityPosts {
                            if quantity > 0 {
                                
                                print("======= Novos Posts Encontrados =======")
                                
                                WishareAPI.listPosts(nil, { (posts : [Post], error : Error?) in
                                    if error == nil {
                                        
                                        self.newPosts.removeAll()
                                        self.newPosts = posts
                                        
                                        var containsSubViewTag : Bool = false
                                        
                                        for v in self.view.subviews {
                                            if v.tag == 1808 {
                                                containsSubViewTag = true
                                            }
                                        }
                                        
                                        if !containsSubViewTag {
                                            
                                            let newPostLabel = UILabel(frame: CGRect(x: (self.view.bounds.width / 2) - (self.view.bounds.width / 4), y: 80, width: (self.view.bounds.width / 2), height: 30))
                                            newPostLabel.text = "Novas Publicações"
                                            newPostLabel.textColor = UIColor.white
                                            newPostLabel.adjustsFontSizeToFitWidth = true
                                            newPostLabel.tag = 1808
                                            newPostLabel.isUserInteractionEnabled = true
                                            newPostLabel.textAlignment = .center
                                            newPostLabel.layer.masksToBounds = true
                                            newPostLabel.layer.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1).cgColor
                                            newPostLabel.layer.cornerRadius = 10
                                            
                                            let tapGestureRecognizerNewPosts = UITapGestureRecognizer(target: self, action: #selector(self.pressNewPosts))
                                            newPostLabel.addGestureRecognizer(tapGestureRecognizerNewPosts)
                                            
                                            self.view.addSubview(newPostLabel)
                                            
                                        }
                                        
                                    }
                                })
                                
                            }
                        }
                        
                    }
                    
                })
                
            }
            
            
        }
        
        Timer.scheduledTimer(withTimeInterval: 30, repeats: true) { ( _ ) in
            
            WishareAPI.quantityNotification({ (quantity : String, error : Error?) in
                if error == nil {
                    UserDefaults.standard.set(quantity, forKey: "numberBagde")
                    
                    if quantity != "0" && self.tabBarController?.selectedIndex != 3 {
                        NotificationCenter.default.post(name: NSNotification.Name.init("isChangedNotification"), object: true)
                    }
                    
                }
            })
            
        }
        
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
    
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.posts.count == 0 {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
            
            WishareAPI.listPosts(nil) { (result : [Post], error : Error?) in
                
                if error == nil {
                    
                    self.posts = result
                    
                    if self.posts.count == 0 {
                        self.searchStoresAndUsersView.isHidden = false
                        self.feedCollectionView.isHidden = true
                    } else {
                        self.searchStoresAndUsersView.isHidden = true
                        self.feedCollectionView.isHidden = false
                    }
                    
                    if self.posts.count > 0 {
                        WishareAPI.getStoresNotFollowing(nil, 10) { (stores, products, morePosts, error) in
                            if error == nil {
                                if stores.count > 0 && products.count > 0 {
                                    let notFollowing = Post()
                                    notFollowing.typePost = 99
                                    notFollowing.comments = 0
                                    notFollowing.likes = 0
                                    self.posts.insert(notFollowing, at: 3)
                                    self.stores = stores
                                    self.products = products
                                    self.moreStoresInCarousel = morePosts
                                    self.feedCollectionView.reloadData()
                                }
                                
                            }
                            RappleActivityIndicatorView.stopAnimation()
                            
                        }
                    }
                    else {
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                    self.feedCollectionView.reloadData()
                }
                
            }
            
        } else {
            RappleActivityIndicatorView.stopAnimation()
        }
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func remoteNotificationBadge(notification : Notification) {
        
        let bagdeValue = self.tabBarController?.tabBar.items?[3].badgeValue
        
        let bagdeValueInt = (bagdeValue == nil) ? 0 : Int(bagdeValue!)!
        let newBagdeInt = notification.object as! Int

        UserDefaults.standard.set("\(bagdeValueInt + newBagdeInt)", forKey: "numberBagde")
        self.tabBarController?.tabBar.items?[3].badgeValue = "\(bagdeValueInt + newBagdeInt)"
        self.tabBarController?.tabBar.items?[3].badgeColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
        
    }
    
    @objc func remoteNotificationDidReceive(notification : Notification) {
        
        let type = notification.object as! String
        
        UserDefaults.standard.removeObject(forKey: "remoteNotificationDidReceive")
        
        if type == "post" {
            
            self.tabBarController?.selectedIndex = 3
    
        } else {
            
            self.tabBarController?.selectedIndex = 4
            let storyboard = UIStoryboard(name: "Setting", bundle: nil)
            let ordersViewController = storyboard.instantiateViewController(withIdentifier: "myOrderID") as! OrdersViewController
            self.tabBarController?.selectedViewController?.childViewControllers[0].navigationController?.pushViewController(ordersViewController, animated: true)
            
        }
        
        
        
    }

    // MARK: - Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showImagePost" {
            
            let showImagePostViewController = segue.destination as! ShowImagePostViewController
            showImagePostViewController.pictureImageString = sender as? UIImage
            
        }
        
        if segue.identifier == "fotoLookSegue" {

            let fotoLookViewController = segue.destination as! FotoLookViewController
            fotoLookViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "shareRepostSegue" {
            
            let repostViewController = segue.destination as! RepostViewController
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            repostViewController.imageRespost = senderValue[0] as? UIImage
            repostViewController.id = senderValue[1] as? String
            
        }
        
        if segue.identifier == "buyShareSegue" {
            
            let buyShareViewController = segue.destination as! BuyShareViewController
            buyShareViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "commentSegue" {
            
            let commentViewController = segue.destination as! CommentViewController
            commentViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "likeSegue" {
            
            let likeViewController = segue.destination as! LikeViewController
            likeViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "publishEditingSegue" {
            
            let publishEditingViewController = segue.destination as! PublishEditingViewController
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            publishEditingViewController.post = senderValue[0] as! Post
            publishEditingViewController.indexPath = senderValue[1] as! IndexPath
            
        }
        
        if segue.identifier == "pageStoreSegue" {
            let pageStoreViewController = segue.destination as! PageStoreViewController
            pageStoreViewController.idStore = sender as! String
        }
        
    }
    
    // MARK: - Methods
    @objc func myCart() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    @objc func refreshShow() {
        
        // RECARREGAR O ARRAY
        print("REFRESH .....")
        
        WishareAPI.listPosts(nil) { (result : [Post], error : Error?) in
            
            if error == nil {
                
                self.posts = result
                
                if self.posts.count == 0 {
                    self.searchStoresAndUsersView.isHidden = false
                    self.feedCollectionView.isHidden = true
                } else {
                    self.searchStoresAndUsersView.isHidden = true
                    self.feedCollectionView.isHidden = false
                }
                
                self.feedCollectionView.reloadData()
                self.refreshControll.endRefreshing()
            
            }
            
        }
        
    }
    
    @objc func pressNewPosts() {
        print("NEW POSTS CARREGADO ..... ")
        
        let newPostLabel = self.view.viewWithTag(1808)
        newPostLabel?.removeFromSuperview()
        
        let index = IndexPath(row: 0, section: 0)
        self.posts = self.newPosts
        self.feedCollectionView.reloadData()
        self.feedCollectionView.scrollToItem(at: index, at: UICollectionViewScrollPosition.top, animated: true)

        
        
//        WishareAPI.listPosts(nil) { (result : [Post], error : Error?) in
//            
//            if error == nil {
//                
//                let index = IndexPath(row: 0, section: 0)
//                self.posts = result
//                self.feedCollectionView.reloadData()
//                self.feedCollectionView.scrollToItem(at: index, at: UICollectionViewScrollPosition.top, animated: true)
//
//            }
//            
//        }
        
    }
    
    func likeAnimation(_ image : UIImageView ) {
    
        UIView.animate(withDuration: 2.6, delay: 0, usingSpringWithDamping: 1.4, initialSpringVelocity: 1.2, options: .allowUserInteraction, animations: {
            image.transform = CGAffineTransform(scaleX: 2.6, y: 2.6)
            image.alpha = 1.0
        }, completion: { (finished : Bool) in
            image.alpha = 0.0
            image.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        })
        
        
    }
    
    func completionHandler(activityType: UIActivityType?, shared: Bool, items: [Any]?, error: Error?) {
        
        if shared {
            
            let alertController = UIAlertController(title: "Sucesso", message: "Compartilhamento realizado com sucesso!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func openTags(gesture : UITapGestureRecognizer) {
       
        if let hint = gesture.accessibilityHint {
            
            let info = hint.components(separatedBy: ",").filter({ (value) -> Bool in
                return value != "" ? true : false
            })
            
            if let type = self.posts[Int(info[0])!].tags[Int(info[1])!].type {
                if type == "STORE" {
                    self.performSegue(withIdentifier: "pageStoreSegue", sender: "\(self.posts[Int(info[0])!].tags[Int(info[1])!].storeId!)")
                } else if type == "BRAND" {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                    showProductHashTagsViewController.textHashTag = self.posts[Int(info[0])!].tags[Int(info[1])!].text!
                    
                    self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                    
                }
            }
        
        }
        
    }
    
    // MARK: - Actions
    @IBAction func unwindToViewController1(segue: UIStoryboardSegue) {
        
        if segue.source.isKind(of: PublishEditingViewController.self) {
            
            let publishEditingViewController = segue.source as! PublishEditingViewController
            
            if self.posts[publishEditingViewController.indexPath.row].text! != publishEditingViewController.commentTextView.text {
                
                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                RappleActivityIndicatorView.startAnimatingWithLabel("Editando...",attributes: attributes)
                
                WishareAPI.postEditing(self.posts[publishEditingViewController.indexPath.row].id!, publishEditingViewController.commentTextView.text, { (error : Error?) in
                    
                    if error == nil {
                        
                        RappleActivityIndicatorView.stopAnimation()
                        self.posts[publishEditingViewController.indexPath.row].text = publishEditingViewController.commentTextView.text
                        self.feedCollectionView.reloadItems(at: [publishEditingViewController.indexPath])
                        
                    }
                    
                })
                
            }
            
        }
        
    }
    
    
}


// MARK: - UICollectionViewDelegate
extension FeedViewController : UICollectionViewDelegate {
    
    
}

// MARK: - UICollectionViewDataSource
extension FeedViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.posts[indexPath.row].typePost {
            
        case 1:
            // POST DE USUARIO
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userPostCell", for: indexPath) as! UserPostCollectionViewCell
            cell.postImageView.viewWithTag(99)?.removeFromSuperview()
            
            cell.delegate = self
            cell.indexPathNow = indexPath
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            
            // =========== Hashtags/Mention ===========
            
            cell.commentUserLabel.enabledTypes = [.hashtag, .mention]
            
            cell.commentUserLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentUserLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentUserLabel.text = comments_final
                
            } else {
                cell.commentUserLabel.text = self.posts[indexPath.row].text!
            }
            
            // ========================================
            
            cell.postDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.postImageView.image = nil
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.countLikeLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.countCommentsLabel.isHidden = false
                cell.countCommentsLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.countCommentsLabel.isHidden = true
            }
            
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            
            if self.posts[indexPath.row].tags.count > 0 {
                
                cell.tagInformation.isHidden = false
                cell.postImageView.viewWithTag(99)?.removeFromSuperview()
                
                
                for (key, tag) in self.posts[indexPath.row].tags.enumerated() {
                    
                    let leftMargin = (cell.postImageView.bounds.width * CGFloat(tag.leftMargin!)) / 100
                    let topMargin = (cell.postImageView.bounds.width * CGFloat(tag.topMargin!)) / 100
                    
                    let tagView = UIView(frame: CGRect(x: leftMargin , y: topMargin, width: (tag.text! as NSString).size(withAttributes: nil).width + 50, height: 25))
                    //let tagView = UIView(frame: CGRect(x: leftMargin, y: topMargin + 18, width: 140, height: 24))
                    
                    tagView.backgroundColor = UIColor.darkGray
                    tagView.tag = 99
                    tagView.layer.cornerRadius = 5
                    
                    let tagLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tagView.bounds.width, height: tagView.bounds.height))
                    let gestureRecognizerTagLabel = UITapGestureRecognizer(target: self, action: #selector(openTags(gesture:)))
                    
                    tagLabel.isUserInteractionEnabled = true
                    gestureRecognizerTagLabel.accessibilityHint = "\(indexPath.row),\(key)"
                    tagLabel.addGestureRecognizer(gestureRecognizerTagLabel)
                    tagLabel.text = tag.text! //"\((tag.text! as NSString).size(attributes: nil).width)" //tag.text!
                    tagLabel.textColor = UIColor.white
                    tagLabel.adjustsFontSizeToFitWidth = true
                    tagLabel.textAlignment = .center
                    
                    tagView.addSubview(tagLabel)
                    tagView.isHidden = true
                    
                    
                    cell.postImageView.addSubview(tagView)
                    
                }
                
            } else {
                cell.tagInformation.isHidden = true
                cell.postImageView.viewWithTag(99)?.removeFromSuperview()
            }
            
            
            
            self.manager.loadImage(with: url, into: cell.postImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.postImageView.image = result.value
            }
            
            
            self.manager.loadImage(with: urlPerfil, into: cell.userImageView) { ( result, _ ) in
                cell.userImageView.image = result.value?.circleMask
            }
            
            
            return cell
            
        case 2:
            // POST DE LOJA SIMPLES
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simpleStoreCell", for: indexPath) as! SimpleStoreCollectionViewCell
            
            cell.delegate = self
            cell.IndexPathNow = indexPath
            
            if self.posts[indexPath.row].showMoreComments {
                cell.commentsLabel.numberOfLines = 0
            } else {
                cell.commentsLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.nameStoreTopButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.nameStoreBottomButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.datePostLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.postStoreImageView.image = nil
            
            //cell.commentsLabel.text = self.posts[indexPath.row].text!
            
            // =========== Hashtags/Mention ===========
            
            //let customType = ActiveType.custom(pattern: "\\^[A-z0-9.!?%\\s]+\\~")
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentsLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentsLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentsLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            cell.commentsLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentsLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentsLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                //cell.commentsLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentsLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts[indexPath.row].showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentsLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentsLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentsLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
                
            }
            
            // ========================================
            
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.countLikesLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.countCommentsLabel.isHidden = false
                cell.countCommentsLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.countCommentsLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeImageView.image = UIImage(named: "Like")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/thumb/\(self.posts[indexPath.row].picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.posts[indexPath.row].store!.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.postStoreImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.postStoreImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.storeImageView) { ( result, _ ) in
                cell.storeImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 3:
            
            // POST DE LOJA PRODUTO
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productStoreCell", for: indexPath) as! ProductStoreCollectionViewCell
            
            cell.delegate = self
            cell.indexPathNow = indexPath
            
            if self.posts[indexPath.row].showMoreComments {
                cell.commentStoreLabel.numberOfLines = 0
            } else {
                cell.commentStoreLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.nameStoreTopButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.nameStoreBottomButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.storePostImageView.image = nil
            
            //cell.commentStoreLabel.text = self.posts[indexPath.row].text!
            
            // =========== Hashtags/Mention ===========
            
            //let customType = ActiveType.custom(pattern: "\\^[A-z0-9.!?%\\s]+\\~")
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentStoreLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentStoreLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                
                if (heightLabelWithText(self.view.bounds.width - 20, self.posts[indexPath.row].text!) + 18.0) <= 30.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts[indexPath.row].showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, self.posts[indexPath.row].text!) + 18.0) > 54.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.55))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
                
            } else {
                
                
                
                let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts[indexPath.row].showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
            }
            
            
            
            // ========================================
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeProductStoreImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeProductStoreImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/thumb/\(self.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.posts[indexPath.row].store!.picture!)")
            
            
            
            self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.storeThumbImageView) { ( result, _ ) in
                cell.storeThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 4:
            
            // POST DE LOJA LOOK
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lookStoreCell", for: indexPath) as! LookStoreCollectionViewCell
            
            cell.delegate = self
            cell.indexPathNow = indexPath
            cell.collectionViewCarouselLook.reloadData()
            
            if self.posts[indexPath.row].showMoreComments {
                cell.commentStoreLabel.numberOfLines = 0
            } else {
                cell.commentStoreLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.nameStoreTopButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.nameStoreBottomButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.storePostImageView.image = nil
            
            //cell.commentStoreLabel.text = self.posts[indexPath.row].text!
            
            // =========== Hashtags/Mention ===========
            
            //let customType = ActiveType.custom(pattern: "\\^[A-z0-9.!?%\\s]+\\~")
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentStoreLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentStoreLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                //cell.commentStoreLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts[indexPath.row].showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
            }
            
            // ========================================
            
            
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeLookStoreImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeLookStoreImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/thumb/\(self.posts[indexPath.row].picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.posts[indexPath.row].store!.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.storeThumbImageView) { ( result, _ ) in
                cell.storeThumbImageView.image = result.value?.circleMask
            }
            
            
            return cell
            
        case 5:
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareBuyCell", for: indexPath) as! ShareBuyCollectionViewCell
            
            cell.delegate = self
            cell.delegateBase = self
            cell.indexPathNow = indexPath
            
            if self.posts[indexPath.row].products.count > 1 {
                cell.collectionView.isHidden = false
                cell.collectionView.reloadData()
            } else {
                cell.collectionView.isHidden = true
            }
            
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameStoreTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameStoreBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.nameStoreButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.storePostImageView.image = nil
            
            
            // =========== Hashtags/Mention ===========
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                
                if self.posts[indexPath.row].share_text == "" || self.posts[indexPath.row].share_text == nil {
                    
                    if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                    
                    
                } else {
                    
                    if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                        
                        let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                            return value != "" ? true : false
                        })
                        
                        
                        var isActiveMention = false
                        
                        for word in comment {
                            
                            let information_comments = word.components(separatedBy: ":")
                            
                            if (word.contains("@id:") && word.contains("@storename:")) {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                    
                                }
                                
                                
                            } else if (word.contains("@id:") && word.contains("@username:"))  {
                                
                                if information_comments.last! == mention && isActiveMention == false {
                                    
                                    isActiveMention = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                    pageUserViewController.isMyProfile = false
                                    pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                    
                                    self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                }
                
                
                
                
                
            })
            
            
            if self.posts[indexPath.row].share_text == "" || self.posts[indexPath.row].share_text == nil {
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                }
                
                
                
            } else {
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    var comments_final : String = ""
                    
                    for word in comment {
                        
                        if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                            let information_comments = word.components(separatedBy: ":")
                            comments_final += "@\(information_comments.last!)"
                        } else {
                            comments_final += word
                        }
                        
                    }
                    
                    cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    cell.commentStoreLabel.text = self.posts[indexPath.row].share_text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                }
            }
            
            
            // ========================================
            
            
            
            
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.informationLabel.text = "realizou uma compra na"
            
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeShareBuyImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeShareBuyImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            //let urlBasePost = URL(string: "http://www.wishare.com.br/code/")!
            
            let url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
            let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            print(url.absoluteString)
            
            self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlStore, into: cell.storeThumbImageView) { ( result, _ ) in
                cell.storeThumbImageView.image = result.value?.circleMask
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                cell.userThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 6:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sharePostUserCell", for: indexPath) as! SharePostUserCollectionViewCell
            
            cell.delegate = self
            cell.delegateBase = self
            cell.indexPathNow = indexPath
            
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userMameShareButton.setTitle(self.posts[indexPath.row].share_user_username, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.informationLabel.text = "compartilhou a publicação de"
            cell.shareUserPostImageView.image = nil
            
            
            
            // =========== Hashtags/Mention ===========
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
            cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            
            if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final
                
            } else {
                cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            }
            
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.shareCommentLabel.text = comments_final
                
            } else {
                cell.shareCommentLabel.text = self.posts[indexPath.row].text!
            }
            
            // ========================================
            
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            let urlShareProfile = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].share_user_picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            print(url.absoluteString)
            
            self.manager.loadImage(with: url, into: cell.shareUserPostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.shareUserPostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlShareProfile, into: cell.userShareThumbImageView) { ( result, _ ) in
                cell.userShareThumbImageView.image = result.value?.circleMask
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                cell.userThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 7:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareStoreUserCell", for: indexPath) as! ShareStoreUserCollectionViewCell
            
            cell.delegate = self
            cell.delegateBase = self
            cell.indexPathNow = indexPath
            
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.storeNameShareButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.informationLabel.text = "compartilhou a publicação de"
            cell.shareStorePostImageView.image = nil
            
            //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
            
            // =========== Hashtags/Mention ===========
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
            cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            
            if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final
                
            } else {
                cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            }
            
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.shareCommentLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                cell.shareCommentLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            }
            
            // ========================================
            
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.shareStorePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                cell.storeShareThumbImageView.image = result.value?.circleMask
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                cell.userThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 8:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareStoreProductUserCell", for: indexPath) as! ShareStoreProductUserCollectionViewCell
            
            cell.delegate = self
            cell.delegateBase = self
            cell.indexPathNow = indexPath
            
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.storeNameShareButton.setTitle(self.posts[indexPath.row].share_store_name, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.informationLabel.text = "compartilhou a publicação de"
            cell.shareStorePostImageView.image = nil
            
            //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
            
            // =========== Hashtags/Mention ===========
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
            cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            
            if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final
                
            } else {
                cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            }
            
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.shareCommentLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                cell.shareCommentLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            }
            
            // ========================================
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
            }
            
            
            let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.shareStorePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                cell.storeShareThumbImageView.image = result.value?.circleMask
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                cell.userThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
            
        case 9:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareStoreLookUserCell", for: indexPath) as! ShareStoreLookCollectionViewCell
            
            cell.delegate = self
            cell.delegateBase = self
            cell.indexPathNow = indexPath
            cell.collectionView.reloadData()
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.storeNameShareButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.informationLabel.text = "compartilhou a publicação de"
            cell.shareStorePostImageView.image = nil
            
            //cell.shareCommentLabel.text = self.posts[indexPath.row].text!
            //cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            
            // =========== Hashtags/Mention ===========
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention]
            cell.shareCommentLabel.enabledTypes = [.hashtag, .mention]
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            cell.shareCommentLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.shareCommentLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            
            if (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@username:")) || (self.posts[indexPath.row].share_text!.contains("@id:") && self.posts[indexPath.row].share_text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].share_text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final
                
            } else {
                cell.commentStoreLabel.text = self.posts[indexPath.row].share_text!
            }
            
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.shareCommentLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                cell.shareCommentLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            }
            
            // ========================================
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeSharePostUserImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.shareStorePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                cell.storeShareThumbImageView.image = result.value?.circleMask
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                cell.userThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 10:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareProductUserCell", for: indexPath) as! ShareProductCollectionViewCell
            
            cell.delegate = self
            cell.delegateBase = self
            cell.indexPathNow = indexPath
            
            if self.posts[indexPath.row].showMoreComments {
                cell.commentStoreLabel.numberOfLines = 0
            } else {
                cell.commentStoreLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.userNameBottomButton.setTitle(self.posts[indexPath.row].user!.username!, for: .normal)
            cell.storeNameShareButton.setTitle(self.posts[indexPath.row].store!.name!, for: .normal)
            cell.publishedDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.informationLabel.text = "compartilhou um produto de"
            cell.shareStorePostImageView.image = nil
            
            //cell.commentStoreLabel.text = self.posts[indexPath.row].text!
            
            // =========== Hashtags/Mention ===========
            
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentStoreLabel.customColor[customType] = UIColor.clear
            cell.commentStoreLabel.customSelectedColor[customType] = UIColor.clear
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                
                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                //cell.commentsLabel.text = self.posts[indexPath.row].text! + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                let string = "^\(self.posts[indexPath.row].user!.username!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.viewMore.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].user!.username!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts[indexPath.row].showMoreComments {
                        
                        cell.viewMore.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].user!.username!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.viewMore.isHidden = false
                            let string = "^\(self.posts[indexPath.row].user!.username!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.viewMore.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts[indexPath.row].user!.username!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
                
            }
            
            // ========================================
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeShareProductImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeShareProductImageView.image = UIImage(named: "Like")
            }
            
            if self.posts[indexPath.row].iwish == "1" {
                cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            
            let pictureString = self.posts[indexPath.row].picture?.components(separatedBy: "/")
            
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(pictureString!.last!)")
            let urlStore = urlBase.appendingPathComponent("files/store-logo/post/\(self.posts[indexPath.row].store!.picture!)")
            let urlPerfil = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.posts[indexPath.row].user!.picture!)")
            
            self.manager.loadImage(with: url, into: cell.shareStorePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.shareStorePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlStore, into: cell.storeShareThumbImageView) { ( result, _ ) in
                cell.storeShareThumbImageView.image = result.value?.circleMask
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.userThumbImageView) { ( result, _ ) in
                cell.userThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 11:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wisharePostCell", for: indexPath) as! WisharePostCollectionViewCell

            cell.postImageView.viewWithTag(99)?.removeFromSuperview()
            
            cell.delegate = self
            cell.indexPathNow = indexPath
            
            if self.posts[indexPath.row].showMoreComments {
                cell.commentUserLabel.numberOfLines = 0
            } else {
                cell.commentUserLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.userNameTopButton.setTitle("Wishare", for: .normal)
            cell.userNameBottomButton.setTitle("Wishare", for: .normal)
            
            // =========== Hashtags/Mention ===========
            
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentUserLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentUserLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentUserLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            cell.commentUserLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentUserLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {
                    
                    let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@username:")) || (self.posts[indexPath.row].text!.contains("@id:") && self.posts[indexPath.row].text!.contains("@storename:")) {

                let comment = self.posts[indexPath.row].text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })

                var comments_final : String = ""

                for word in comment {

                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }

                }

                cell.commentUserLabel.text = comments_final + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")

            } else {

                let string = "^Wishare~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")

                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {

                    cell.showMoreButton.isHidden = true
                    cell.commentUserLabel.text = "^Wishare~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")

                } else {

                    if self.posts[indexPath.row].showMoreComments {

                        cell.showMoreButton.isHidden = true
                        cell.commentUserLabel.text = "^Wishare~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")

                    } else {

                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.showMoreButton.isHidden = false
                            let string = "^Wishare~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentUserLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."

                        } else {
                            cell.showMoreButton.isHidden = true
                            cell.commentUserLabel.text = "^Wishare~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }

                    }

                }


            }

            
            // ========================================
            
            cell.postDateLabel.text = self.posts[indexPath.row].created!.convertToDate().timeAgoDisplay()
            cell.postImageView.image = nil
            
            // - CURTIDAS
            if self.posts[indexPath.row].likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.countLikeLabel.text = "\(self.posts[indexPath.row].likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts[indexPath.row].comments! > 0 {
                cell.countCommentsLabel.isHidden = false
                cell.countCommentsLabel.text = "Ver \(self.posts[indexPath.row].comments!) comentário(s)"
            } else {
                cell.countCommentsLabel.isHidden = true
            }
            
            
            if self.posts[indexPath.row].ilikes == "1" {
                cell.likeImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeImageView.image = UIImage(named: "Like")
            }
            
            
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")            
            
            
            self.manager.loadImage(with: url, into: cell.postImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.postImageView.image = result.value
            }
            
            
            cell.userImageView.image = #imageLiteral(resourceName: "logo_app").circleMask
            
            
            return cell
            
        case 99:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storeSugestionCell", for: indexPath) as! StoreSugestionCollectionViewCell
            
            cell.logoImageView.image = #imageLiteral(resourceName: "logo_app").circleMask
            cell.stores = stores
            cell.products = products
            cell.delegate = self
            cell.moreStores = self.moreStoresInCarousel
            cell.storeCarousel.reloadData()
            
            return cell
            
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "testeCell", for: indexPath)
            return cell
        }
        
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension FeedViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // Verificando se existe comentarios e curtidas
        var base : CGFloat = 0.0
        if self.posts[indexPath.row].comments! == 0 { base += 30.0 }
        if self.posts[indexPath.row].likes! == 0 { base += 30.0 }
        
        switch self.posts[indexPath.row].typePost {
        case 1:
            // Usuario Simples - OK
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (275 - base))
        case 2:
            // Loja Simples - OK
            
            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            let labelSize = heightLabelWithText(self.view.bounds.width - 20.0, string) + 18.0
            
            var size : CGFloat = 0.0
            
            if labelSize <= 36.0 {
                
                size = CGFloat(270.0)
                
            } else {
                
                if self.posts[indexPath.row].showMoreComments {
                    
                    size = CGFloat(240.0) + labelSize
                    
                } else {
                    
                    if labelSize >= 72.0 {
                        size = CGFloat(240.0) + CGFloat(54.0)
                    } else {
                        size = CGFloat(240.0) + (labelSize - 18.0)
                    }
                    
                }
                
            }
            
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (size - base))

        case 3:
            // Loja Produto - OK
            
            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            let labelSize = heightLabelWithText(self.view.bounds.width - 20.0, string) + 18.0
            
            var size : CGFloat = 0.0
            
            if labelSize <= 36.0 {
                
                size = CGFloat(270.0)
            
            } else {
                
                if self.posts[indexPath.row].showMoreComments {
                    
                    size = CGFloat(240.0) + labelSize
                    
                } else {
                    
                    if labelSize >= 72.0 {
                        size = CGFloat(240.0) + CGFloat(54.0)
                    } else {
                        size = CGFloat(240.0) + (labelSize - 18.0)
                    }
                    
                }
        
            }
            
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (size - base))
            
        case 4:
            // Loja Look - OK
            let string = "^\(self.posts[indexPath.row].store!.name!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            let labelSize = heightLabelWithText(self.view.bounds.width - 20.0, string) + 18.0
            
            var size : CGFloat = 0.0
            
            if labelSize <= 36.0 {
                
                size = CGFloat(270.0)
                
            } else {
                
                if self.posts[indexPath.row].showMoreComments {
                    
                    size = CGFloat(240.0) + labelSize
                    
                } else {
                    
                    if labelSize >= 72.0 {
                        size = CGFloat(240.0) + CGFloat(54.0)
                    } else {
                        size = CGFloat(240.0) + (labelSize - 18.0)
                    }
                    
                }
                
            }
            
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (size - base))
        case 5:
            // Compartilhamento de compra - OK
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (325 - base))
        case 6:
            // Post Usuario - OK
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (357 - base))
        case 7:
            // Post Loja Simples - OK
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (365 - base))
        case 8:
            // Post Loja Produto - OK
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (357 - base))
        case 9:
            // Post Loja Look - OK
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (355 - base))
        case 10:
            // Sem Post - OK
            let string = "^\(self.posts[indexPath.row].user!.username!)~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            let labelSize = heightLabelWithText(self.view.bounds.width - 20.0, string) + 18.0
            
            var size : CGFloat = 0.0
            
            if labelSize <= 36.0 {
                
                size = CGFloat(270.0)
                
            } else {
                
                if self.posts[indexPath.row].showMoreComments {
                    
                    size = CGFloat(240.0) + labelSize
                    
                } else {
                    
                    if labelSize >= 72.0 {
                        size = CGFloat(240.0) + CGFloat(54.0)
                    } else {
                        size = CGFloat(240.0) + (labelSize - 18.0)
                    }
                    
                }
                
            }
            
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (size - base) + 50)
        case 11:
            // Wishare Post - OK
            let string = "^Wishare~" + " " + self.posts[indexPath.row].text!.components(separatedBy: .newlines).joined() + " " + self.posts[indexPath.row].hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            let labelSize = heightLabelWithText(self.view.bounds.width - 20.0, string) + 18.0
            
            var size : CGFloat = 0.0
            
            if labelSize <= 36.0 {
                
                size = CGFloat(270.0)
                
            } else {
                
                if self.posts[indexPath.row].showMoreComments {
                    
                    size = CGFloat(240.0) + labelSize
                    
                } else {
                    
                    if labelSize >= 72.0 {
                        size = CGFloat(240.0) + CGFloat(54.0)
                    } else {
                        size = CGFloat(240.0) + (labelSize - 18.0)
                    }
                    
                }
                
            }
            
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (size - base))
            
//            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (275 - base))
        case 99:
            // Recomendação de lojas
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (200 - base))
        default:
            return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (self.view.bounds.width / 1.5))
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if self.isLoading {
            return CGSize.zero
        }
        
        return CGSize(width: collectionView.bounds.size.width, height: 55.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter {
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
            self.footerView = footerView
            self.footerView?.backgroundColor = UIColor.clear
            return footerView
            
        } else {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold = 100.0
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold = Float((diffHeight - frameHeight))/Float(threshold);
        
        triggerThreshold = min(triggerThreshold, 0.0)
        let pullRatio = min(fabs(triggerThreshold),0.4);
        
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        
        if pullRatio >= 0.4 {
            self.footerView?.animateFinal()
        }
        
        //print("pullRation:\(pullRatio)")
        
    }
    
    fileprivate func loadMorePosts() {
        if (self.footerView?.isAnimatingFinal)! {
            
            //print("load more trigger")
            //                if self.isLoading {
            self.isLoading = true
            self.footerView?.startAnimate()
            
            //                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
            
            
            // APPENDING NO ARRAY
            if self.lastPost != self.posts.last?.created {
                self.lastPost = self.posts.last?.created
                print("REFRESH APPENDING ....")
                
                WishareAPI.listPosts(self.posts.last?.created) { (result : [Post], error : Error?) in
                    
                    if error == nil {
                        self.posts.append(contentsOf: result)
                        self.feedCollectionView.reloadData()
                    }
                    
                    self.isLoading = false
                    
                }
            }
            
            //                    })
            //                }
            
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
//        let contentOffset = scrollView.contentOffset.y;
//        let contentHeight = scrollView.contentSize.height;
//        let diffHeight = contentHeight - contentOffset;
//        let frameHeight = scrollView.bounds.size.height;
//        let pullHeight  = fabs(diffHeight - frameHeight);
//
//        print("ContentOffset: \(contentOffset)")
//        print("Content Height: \(contentHeight)")
//        print("Frame Height: \(frameHeight)")
//        print("pullHeight:\(pullHeight)");
//
//        if pullHeight >= 0.0  && pullHeight <= 1.0 {
////            loadMorePosts()
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            loadMorePosts()
        }
    }
    
    
}

// MARK: - UserPostCollectionViewCellDelegate
extension FeedViewController : UserPostCollectionViewCellDelegate {
    
    func moreUserPost(cell: UserPostCollectionViewCell) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                if self.posts[cell.indexPathNow.row].user_id == user!.id {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                        self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                    })
                    
                    let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                            
                            WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                    
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                        
                                        self.posts.remove(at: cell.indexPathNow.row)
                                        self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                    
                        alertController.addAction(okAction)
                        alertController.addAction(cancelarAction)
                        self.present(alertController, animated: true, completion: nil)

                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(editarAction)
                    alertController.addAction(excluirAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                        RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                        
                        WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                            
                            if error == nil {
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            
                        })
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(denunciarAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
            
            }
            
        }
        

    }
    
    func likeUserPost(cell: UserPostCollectionViewCell) {
     
        
        
        if self.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.posts[cell.indexPathNow.row].ilikes = "0"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            

            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.posts[cell.indexPathNow.row].ilikes = "1"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            

            
        }
        
    }
    
    func commentUserPost(cell: UserPostCollectionViewCell) {
        self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
    func shareUserPost(cell: UserPostCollectionViewCell, _ indexPath : IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in

                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
            
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
            
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler

            }
            

        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func giftUserPost(cell: UserPostCollectionViewCell) {
        
        if self.posts[cell.indexPathNow.row].iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.posts[cell.indexPathNow.row].iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.dreamImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.posts[cell.indexPathNow.row].iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
        
        
        
    }
    
    func userImageUserPost(cell: UserPostCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[cell.indexPathNow.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func postImageUserPost(cell: UserPostCollectionViewCell) {
        print("CLICOU NA IMAGEM DO POST")
        self.performSegue(withIdentifier: "showImagePost", sender: cell.postImageView.image)
    }
    
    func userNameUserPost(cell: UserPostCollectionViewCell) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[cell.indexPathNow.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
    }
    
    func tagInformationUserPost(cell: UserPostCollectionViewCell) {
        print("CLICOU NA TAG")
        
        if cell.tagOnOrOff == false {
            
            cell.tagImageView.image = UIImage(named: "tag-on")
            cell.tagOnOrOff = true
            cell.tagInformation.backgroundColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 0.5)
            
            for view in cell.postImageView.subviews {
                
                if view.tag == 99 {
                    view.isHidden = false
                }
                
            }
            
            
           
        } else {
            
            cell.tagImageView.image = UIImage(named: "tag-off")
            cell.tagOnOrOff = false
            cell.tagInformation.backgroundColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 0.5)
            
            for view in cell.postImageView.subviews {
                
                if view.tag == 99 {
                    view.isHidden = true
                }
                
            }
            
            
        }
        
    }
    
    func viewLikes(cell: UserPostCollectionViewCell) {
        self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
}

// MARK: - SimpleStoreCollectionViewDelegate
extension FeedViewController : SimpleStoreCollectionViewCellDelegate {
    
    func viewLikes(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        self.performSegue(withIdentifier: "likeSegue", sender: self.posts[indexPath.row])
    }
    
    
    func showMoreCommentsSimpleStore(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        
        self.posts[indexPath.row].showMoreComments = true
        self.feedCollectionView.reloadItems(at: [indexPath])
        
    }
    
    func moreSimpleStore(cell: SimpleStoreCollectionViewCell) {

        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                if self.posts[cell.IndexPathNow.row].user_id == user!.id {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                        self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.IndexPathNow.row], cell.IndexPathNow])
                    })
                    
                    let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                            
                            WishareAPI.postDelete(self.posts[cell.IndexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                    
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                        
                                        self.posts.remove(at: cell.IndexPathNow.row)
                                        self.feedCollectionView.deleteItems(at: [cell.IndexPathNow])
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                        
                        alertController.addAction(okAction)
                        alertController.addAction(cancelarAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(editarAction)
                    alertController.addAction(excluirAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                        RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                        
                        WishareAPI.toReport(self.posts[cell.IndexPathNow.row].id!, { (error : Error?) in
                            
                            if error == nil {
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            
                        })
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(denunciarAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
            }
        
        }
    
    }
    
    func likeSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
        if self.posts[cell.IndexPathNow.row].ilikes == "1" {
            
            self.posts[cell.IndexPathNow.row].ilikes = "0"
            self.posts[cell.IndexPathNow.row].likes = self.posts[cell.IndexPathNow.row].likes! - 1
            self.feedCollectionView.reloadItems(at: [cell.IndexPathNow])
            
            WishareAPI.like(self.posts[cell.IndexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.IndexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.posts[cell.IndexPathNow.row].ilikes = "1"
            self.posts[cell.IndexPathNow.row].likes = self.posts[cell.IndexPathNow.row].likes! + 1
            self.feedCollectionView.reloadItems(at: [cell.IndexPathNow])
            
            WishareAPI.like(self.posts[cell.IndexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.IndexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
            
            
        }

        
    }
    
    func commentSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.IndexPathNow.row])
    }
    
    func shareSimpleStore(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func storeSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.IndexPathNow.row].store!.id!)
    }
    
    func storePostSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "showImagePost", sender: cell.postStoreImageView.image)
    }
    
    func nameStoreSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.IndexPathNow.row].store!.id!)
    }
    
    
}

// MARK: - ProductStoreCollectionViewCellDelegate 
extension FeedViewController : ProductStoreCollectionViewCellDelegate {
    
    func showMoreCommentsProductStore(cell: ProductStoreCollectionViewCell, indexPath: IndexPath) {
        
        self.posts[indexPath.row].showMoreComments = true
        self.feedCollectionView.reloadItems(at: [indexPath])
        
    }
    
    func storeThumbProductStore(cell: ProductStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
    }
    
    func nameStoreProductStore(cell: ProductStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
    }
    
    func buyProductStore(cell: ProductStoreCollectionViewCell) {
        checkProduct(fromCell: cell)
    }
    
    func postProductStore(cell: ProductStoreCollectionViewCell) {
        checkProduct(fromCell: cell)
    }
    
    
    func checkProduct(fromCell cell: ProductStoreCollectionViewCell) {
        
        // check das condições do produto
        
        if let idProduct = self.posts[cell.indexPathNow.row].products[0]["id"] {
            
            WishareAPI.checkProduct(idProduct: idProduct) { (exists: Bool, code: String?, option: String?, error: Error?)  in
                if error == nil {
                    if exists {
                        //se tudo der certo
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                        productDetailViewController.idProduct = idProduct
                        productDetailViewController.isFirst = true
                        
                        self.navigationController?.pushViewController(productDetailViewController, animated: true)
                    }
                    else {
                        if let code = code {
                            switch code {
                            case "E":
                                let alertController = UIAlertController(title: "Ops, o estoque foi vendido!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
                                
                                let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                                    WishareAPI.waitingStock(idProduct, { (error : Error?) in })
                                })
                                
                                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                                
                                alertController.addAction(okAction)
                                alertController.addAction(cancelAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                break
                            case "I", "R":
                                let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                break
                            case "O":
                                let alertController = UIAlertController(title: "Acabou e agora?", message: "Temos outras opções de cores deste produto!", preferredStyle: .alert)
                                
                                let otherOption = UIAlertAction(title: "Visualizar", style: .default) { _ in
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                                    productDetailViewController.idProduct = option
                                    productDetailViewController.isFirst = true
                                    
                                    self.navigationController?.pushViewController(productDetailViewController, animated: true)
                                }
                                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                                
                                alertController.addAction(otherOption)
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion: nil)
                                break
                            default:
                                //removido
                                let alertController = UIAlertController(title: "Ah, que pena!", message: "Este produto não existe mais 😞", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                break
                            }
                        }
                    }
                    
                }
                
                
            }
            

        }
    }
    
    func likeProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.posts[cell.indexPathNow.row].ilikes = "0"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.posts[cell.indexPathNow.row].ilikes = "1"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
            
            
        }

        
    }
    
    func commentProductStore(cell: ProductStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
    func shareProductStore(cell: ProductStoreCollectionViewCell, indexPath : IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
            }
            
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func wishListProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.posts[cell.indexPathNow.row].iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.posts[cell.indexPathNow.row].iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.posts[cell.indexPathNow.row].iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
    
    
}

// MARK: - LookStoreCollectionViewCellDelegate
extension FeedViewController : LookStoreCollectionViewCellDelegate {
    
    func viewLikesLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
    
    
    func showMoreCommentsLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        self.posts[indexPath.row].showMoreComments = true
        self.feedCollectionView.reloadItems(at: [indexPath])
    }

    
    func numberOfItemsInSectionLook(_ cellForLook : LookStoreCollectionViewCell) -> Int {
        return self.posts[cellForLook.indexPathNow.row].products.count
    }
    
    func cellForItemLook(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook : LookStoreCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[cellForLook.indexPathNow.row].products[indexPath.row]["url"]!)")
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItem(_ collection: UICollectionView, _ indexPath: IndexPath, _ now : IndexPath) {
        
        var isValidate : Bool = false
        
        for prod_info in self.posts[now.row].products_info {
            
            if prod_info.id! == self.posts[now.row].products[indexPath.row]["id"] {
                isValidate = true
            }
            
        }
        
        if !isValidate {

            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[now.row].products[indexPath.row]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
            
        }
        
    }
    
    
    func buyLookStore( _ sender : UIButton, _ indexPath : IndexPath) {
        
        if self.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookSegue", sender: self.posts[indexPath.row])
        
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    
    func buyLookStoreImage(_ indexPath: IndexPath) {
        
        if self.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookSegue", sender: self.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func storeThumbLookStore(cell: LookStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
    }
    
    func buyLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func postLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func likeLookStore(cell: LookStoreCollectionViewCell) {
        
        if self.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.posts[cell.indexPathNow.row].ilikes = "0"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
            
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            let indexPath = IndexPath(row: 0, section: 0)
            cell.collectionViewCarouselLook.selectItem(at: indexPath, animated: false, scrollPosition: .left)
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
            
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.posts[cell.indexPathNow.row].ilikes = "1"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
            
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            let indexPath = IndexPath(row: 0, section: 0)
            cell.collectionViewCarouselLook.selectItem(at: indexPath, animated: false, scrollPosition: .left)
    
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        
                        
                    }
                    
                }
                
            })
            
            
            
        }

        
    }
    
    func commentLookStore(cell: LookStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
    func shareLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            
//            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url) {
                print("entrou no share post do look")
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [$0.value!, self.posts[indexPath.row].id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
                self.present(activityViewController, animated: true, completion: nil)
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func wishlistLookStore(cell: LookStoreCollectionViewCell) {
        
        if self.posts[cell.indexPathNow.row].iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.posts[cell.indexPathNow.row].iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.posts[cell.indexPathNow.row].iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
    func storeNameLookStore(cell: LookStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
    }
    
}

extension FeedViewController : ShareStoreProductUserCollectionViewCellDelegate {
    
    func buyShareStore(cell: ShareStoreProductUserCollectionViewCell) {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    func postShareStore(cell: ShareStoreProductUserCollectionViewCell) {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    func openUserImageShareStoreProduct(_ indexPath: IndexPath) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonShareStoreProduct(_ sender: UIButton, _ indexPath: IndexPath) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension FeedViewController : ShareProductCollectionViewCellDelegate {
    
    func buyShareStoreProduct(cell: ShareProductCollectionViewCell) {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    func postShareStoreProduct(cell: ShareProductCollectionViewCell) {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[cell.indexPathNow.row].products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    func openUserButtonShareStoreProdutNotFound(_ sender: UIButton, _ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserImageShareStoreProdutNotFound(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

extension FeedViewController : SharePostUserCollectionViewCellDelegate {
    
    func openUserImageSharePostUser(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonSharePostUser(_ sender: UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    
    func openUserShareSharePostUser(_ sender : UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].share_user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserShareImageSharePostUser(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].share_user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func showMoreCommentsShareProductStore(cell: ShareProductCollectionViewCell, indexPath: IndexPath) {
        self.posts[indexPath.row].showMoreComments = true
        self.feedCollectionView.reloadData()
    }
    
}

// MARK: - ShareStoreLookCollectionViewCellDelegate
extension FeedViewController : ShareStoreLookCollectionViewCellDelegate {
    
    func numberOfItemsInSectionShareLook(_ cellForLook : ShareStoreLookCollectionViewCell) -> Int {
        return self.posts[cellForLook.indexPathNow.row].products.count
    }
    
    func cellForItemShareLook(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook : ShareStoreLookCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[cellForLook.indexPathNow.row].products[indexPath.row]["url"]!)")
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItemShareLook(_ collection: UICollectionView, _ indexPath: IndexPath) {

        
        var isValidate : Bool = false
        
        
        for prod_info in self.posts[Int(collection.accessibilityHint!)!].products_info {
            
            if prod_info.id! == self.posts[Int(collection.accessibilityHint!)!].products[indexPath.row]["id"] {
                isValidate = true
            }
            
        }
        
        if !isValidate {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts[Int(collection.accessibilityHint!)!].products[indexPath.row]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
    }

    func buyShareLookStoreImage(_ indexPath: IndexPath) {
        
        if self.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookSegue", sender: self.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func buyLookStoreButton(_ sender: UIButton, _ indexPath: IndexPath) {
        
        if self.posts[indexPath.row].products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookSegue", sender: self.posts[indexPath.row])
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    func openUserImageShareStoreLook(_ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonShareStoreLook(_ sender: UIButton, _ indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}

// MARK: - ShareBuyCollectionViewCellDelegate
extension FeedViewController : ShareBuyCollectionViewCellDelegate {
    
    func numberOfItemsInSectionShare(_ cellForLook: ShareBuyCollectionViewCell) -> Int {
        return self.posts[cellForLook.indexPathNow.row].products.count - 1
    }
    
    func cellForItemShare(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook: ShareBuyCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
        let url : URL!
        
        if indexPath.row == 0 {
            url = urlBase.appendingPathComponent("\(self.posts[cellForLook.indexPathNow.row].products[1]["url"]!)")
        } else {
            url = urlBase.appendingPathComponent("\(self.posts[cellForLook.indexPathNow.row].products[indexPath.row + 1]["url"]!)")
        }
        
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItemShare(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellIndexPath : IndexPath) {
        print("CLICOU CELULA \(indexPath.row)")
        self.performSegue(withIdentifier: "buyShareSegue", sender: self.posts[cellIndexPath.row])
        
    }
    
    func storePostImage(_ indexPath: IndexPath) {
        self.performSegue(withIdentifier: "buyShareSegue", sender: self.posts[indexPath.row])
    }
    
    func openUserImage(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButton(_ sender: UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
}

extension FeedViewController : ShareStoreUserCollectionViewCellDelegate {
    
    func openUserImageShareStore(_ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
    func openUserButtonShareStore(_ sender: UIButton, _ indexPath : IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = self.posts[indexPath.row].user_id!
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
        
    }
    
}


// MARK - Wishare Delegate Base
extension FeedViewController : WishareBasePostDelegate {
    
    func more(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            self.posts.remove(at: cell.indexPathNow.row)
                                            self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            self.posts.remove(at: cell.indexPathNow.row)
                                            self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            self.posts.remove(at: cell.indexPathNow.row)
                                            self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            self.posts.remove(at: cell.indexPathNow.row)
                                            self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            self.posts.remove(at: cell.indexPathNow.row)
                                            self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
                
                if error ==  nil && user != nil {
                    
                    if self.posts[cell.indexPathNow.row].user_id == user!.id {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                            self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts[cell.indexPathNow.row], cell.indexPathNow])
                        })
                        
                        let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                                
                                let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                                RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                                
                                WishareAPI.postDelete(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                    
                                    if error == nil {
                                        
                                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                        
                                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                            
                                            self.posts.remove(at: cell.indexPathNow.row)
                                            self.feedCollectionView.deleteItems(at: [cell.indexPathNow])
                                            
                                        })
                                        
                                    }
                                    
                                })
                                
                                
                            })
                            
                            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelarAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(editarAction)
                        alertController.addAction(excluirAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                            
                            WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation()
                                    
                                    let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                }
                                
                            })
                            
                        })
                        
                        alertController.addAction(cancelarAction)
                        alertController.addAction(denunciarAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                    
                }
                
            }
            
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func like(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                cell.collectionView.reloadData()
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                cell.collectionView.reloadData()
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            if self.posts[cell.indexPathNow.row].ilikes == "1" {
                
                self.posts[cell.indexPathNow.row].ilikes = "0"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes == 0 {
                            cell.viewLikes.isHidden = true
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        }
                        
                        
                        
                    }
                    
                })
                
                
                
            } else {
                
                likeAnimation(cell.likeEffectsImageView)
                self.posts[cell.indexPathNow.row].ilikes = "1"
                self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
                self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
                
                WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                    
                    if error == nil {
                        
                        if self.posts[cell.indexPathNow.row].likes! == 1 {
                            cell.viewLikes.isHidden = false
                            cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                            cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                            cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        }
                        
                    }
                    
                })
                
                
                
            }
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func share(cell: UICollectionViewCell, indexPath : IndexPath) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            //let cell = cell as! ShareBuyCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
                }

                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            //let cell = cell as! SharePostUserCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            //let cell = cell as! ShareStoreUserCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            //let cell = cell as! ShareStoreProductUserCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }

                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            //let cell = cell as! ShareStoreLookCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL!
                
                if self.posts[indexPath.row].picture != nil && self.posts[indexPath.row].picture != "" {
                    url = urlBase.appendingPathComponent("files/posts-image/\(self.posts[indexPath.row].id!)/\(self.posts[indexPath.row].picture!)")
                } else {
                    url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                }
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 160, y: result.value!.size.height - 60, width: 150, height: 50), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            //let cell = cell as! ShareProductCollectionViewCell
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts[indexPath.row].id!])
                }
                
            }
            
            let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
                
                //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
                let url : URL = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts[indexPath.row].products[0]["url"]!)")
                
                let imageView : UIImageView = UIImageView()
                
                self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                    
                    let img2 = UIImage(named: "logo-share")!
                    
                    let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                    
                    UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    context?.setFillColor(UIColor.white.cgColor)
                    context?.fill(rect)
                    
                    result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                    img2.draw(in: CGRect(x: result.value!.size.width - 100, y: result.value!.size.height - 40, width: 90, height: 30), blendMode: .normal, alpha: 0.6)
                    
                    
                    let result = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    
                    let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    activityViewController.completionWithItemsHandler = self.completionHandler
                    
                }
                
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(publishedMessageShareAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            print("N FUNCIONOU")
        }

        
    }
    
    func wishlist(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListShareBuyImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
        
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
        
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListSharePostUserImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            if self.posts[cell.indexPathNow.row].iwish == "1" {
                
                let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
                let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                    
                    WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, false, { (error : Error?) in
                        
                        if error == nil {
                            cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                            self.posts[cell.indexPathNow.row].iwish = "0"
                        }
                        
                    })
                    
                })
                
                let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                
                alertController.addAction(confirmarAction)
                alertController.addAction(cancelarAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            } else {
                
                let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                
                WishareAPI.wishlist(self.posts[cell.indexPathNow.row].id!, true, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListShareProductImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                        self.posts[cell.indexPathNow.row].iwish = "1"
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                })
                
            }
            
        } else {
            print("N FUNCIONOU")
        }
        
        
    }
    
    func showLikes(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func showComments(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            let cell = cell as! SharePostUserCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func thumbStorePostImageView(cell: UICollectionViewCell) {
        
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
           self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            //let cell = cell as! SharePostUserCollectionViewCell
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else {
            print("N FUNCIONOU")
        }
        
    }
    
    func userNameStorePostLabel(cell: UICollectionViewCell) {
        if cell.isKind(of: ShareBuyCollectionViewCell.self) {
            
            // Compartilhamento de compra
            let cell = cell as! ShareBuyCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
            
        } else if cell.isKind(of: SharePostUserCollectionViewCell.self) {
            
            // Compartilhamento - Post Usuario
            //let cell = cell as! SharePostUserCollectionViewCell
            
            
        } else if cell.isKind(of: ShareStoreUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Simples
            let cell = cell as! ShareStoreUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreProductUserCollectionViewCell.self) {
            
            // Compartilhamento - Loja Produto
            let cell = cell as! ShareStoreProductUserCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareStoreLookCollectionViewCell.self) {
            
            // Compartilhamento - Loja Look
            let cell = cell as! ShareStoreLookCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else if cell.isKind(of: ShareProductCollectionViewCell.self) {
            
            // Compartilhamento - Sem Post
            let cell = cell as! ShareProductCollectionViewCell
            self.performSegue(withIdentifier: "pageStoreSegue", sender: self.posts[cell.indexPathNow.row].store!.id!)
            
        } else {
            print("N FUNCIONOU")
        }
    }
    
    
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//
//        let tabBarIndex = tabBarController.selectedIndex
//
//        print(tabBarIndex)
//
//        if tabBarIndex == 0 {
//            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
//        }
//    }
    

    
}

// MARK: - WisharePostCollectionViewCellDelegate

extension FeedViewController : WisharePostCollectionViewCellDelegate {
    
    func postImageWSPost(cell: WisharePostCollectionViewCell) {
        
    }
    
    
    func moreWSPost(cell: WisharePostCollectionViewCell) {
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                        RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                        
                        WishareAPI.toReport(self.posts[cell.indexPathNow.row].id!, { (error : Error?) in
                            
                            if error == nil {
                                
                                
                                let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            
                            RappleActivityIndicatorView.stopAnimation()

                            
                        })
                        
                    })
                    
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    
                    alertController.addAction(denunciarAction)
                    alertController.addAction(cancelarAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    func likeWSPost(cell: WisharePostCollectionViewCell) {
        if self.posts[cell.indexPathNow.row].ilikes == "1" {
            
            self.posts[cell.indexPathNow.row].ilikes = "0"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! - 1
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            likeAnimation(cell.likeEffectsImageView)
            self.posts[cell.indexPathNow.row].ilikes = "1"
            self.posts[cell.indexPathNow.row].likes = self.posts[cell.indexPathNow.row].likes! + 1
            self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            
            WishareAPI.like(self.posts[cell.indexPathNow.row].id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts[cell.indexPathNow.row].likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                }
            })
        }
    }
    
    func commentWSPost(cell: WisharePostCollectionViewCell) {
        self.performSegue(withIdentifier: "commentSegue", sender: self.posts[cell.indexPathNow.row])
    }

    
    func viewLikesWS(cell: WisharePostCollectionViewCell) {
        self.performSegue(withIdentifier: "likeSegue", sender: self.posts[cell.indexPathNow.row])
    }
    
    func showMoreCommentsWisharePost(cell: WisharePostCollectionViewCell, indexPath: IndexPath) {
        self.posts[indexPath.row].showMoreComments = true
        self.feedCollectionView.reloadData()
//        self.feedCollectionView.reloadItems(at: [indexPath])
//        let indexPathNext = IndexPath(row: indexPath.row + 1, section: indexPath.section)
//        self.feedCollectionView.reloadItems(at: [indexPathNext])
    }
    
}

extension FeedViewController : StoreSuggestionsProtocol {
    func productTapped(productId: String) {
        WishareAPI.checkProduct(idProduct: productId) { (exists: Bool, code: String?, option: String?, error: Error?)  in
            if error == nil {
                if exists {
                    //se tudo der certo
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                    productDetailViewController.idProduct = productId
                    productDetailViewController.isFirst = true
                    
                    self.navigationController?.pushViewController(productDetailViewController, animated: true)
                }
                else {
                    if let code = code {
                        switch code {
                        case "E":
                            let alertController = UIAlertController(title: "Ops, o estoque foi vendido!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
                            
                            let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                                WishareAPI.waitingStock(productId, { (error : Error?) in })
                            })
                            
                            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            break
                        case "I", "R":
                            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            break
                        case "O":
                            let alertController = UIAlertController(title: "Acabou e agora?", message: "Temos outras opções de cores deste produto!", preferredStyle: .alert)
                            
                            let otherOption = UIAlertAction(title: "Visualizar", style: .default) { _ in
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
                                productDetailViewController.idProduct = option
                                productDetailViewController.isFirst = true
                                
                                self.navigationController?.pushViewController(productDetailViewController, animated: true)
                            }
                            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                            
                            alertController.addAction(otherOption)
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                            break
                        default:
                            //removido
                            let alertController = UIAlertController(title: "Ah, que pena!", message: "Este produto não existe mais 😞", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            break
                        }
                    }
                }
            }
        }
    }
    
    func openStore(storeId: String) {
        self.performSegue(withIdentifier: "pageStoreSegue", sender: storeId)
    }
    
    func moreStores(ids: [String]?, cell: StoreSugestionCollectionViewCell) {
        WishareAPI.getStoresNotFollowing(ids, 10) { (stores, products, morePosts, error) in
            if self.stores != nil {
                self.stores! += stores
                cell.stores = self.stores
                if stores.count > 0 {
                    cell.moreStores = true
                } else {
                    cell.moreStores = false
                }
            }
            if self.products != nil {
                self.products! += products
                cell.products = self.products
            }
            self.moreStoresInCarousel = morePosts
            cell.moreStores = self.moreStoresInCarousel
            cell.storeCarousel.reloadData()
        }
    }

    
}






