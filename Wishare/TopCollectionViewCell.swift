//
//  TopCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/22/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class TopCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
