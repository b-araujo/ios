//
//  LikeUserTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class LikeUserTableViewCell: UITableViewCell {


    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameButton: UIButton!
    @IBOutlet weak var followUser: UIButton!
    
    var indexPathNow : IndexPath!
    var delegate : LikeUserTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let profileUserGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(pressUserName))
        self.userImageView.addGestureRecognizer(profileUserGestureRecognizer)
        
        self.userNameButton.addTarget(self, action: #selector(pressUserName), for: .touchUpInside)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func follow(_ sender: UIButton) {
        self.delegate.followLikeUser(sender: sender, cell: self)
    }
    
    @objc func pressUserName() {
        self.delegate.showUserLikeUser(cell: self)
    }
    
    
}

protocol LikeUserTableViewCellDelegate {
    
    func followLikeUser(sender : UIButton, cell : LikeUserTableViewCell)
    func showUserLikeUser(cell : LikeUserTableViewCell)
    
}
