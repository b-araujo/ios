//
//  CpfRule.swift
//  Wishare
//
//  Created by Wishare iMac on 8/25/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import SwiftValidator

public class CpfRule : Rule {
    
    private var message : String
    
    public init(_ message : String = "CPF Inválidos") {
        self.message = message
    }
    
    
    public func validate(_ value: String) -> Bool {
        
        // TRUE - Passou na validacao
        // FALSE - Nao passou
        
        if value.characters.count < 11 {
            return false
        }
        
        if value == "11111111111" || value == "22222222222" || value == "33333333333" || value == "44444444444" || value == "55555555555" || value == "66666666666" || value == "77777777777" || value == "88888888888" || value == "99999999999" {
            return false
        }
        
        let cpf = value
        
        if value.contains(".") || value.contains("-") {
            return false
        }
        
        var valueTotal = 0
        
        let primaryVerification = (cpf as NSString).substring(with: NSRange(location: 9, length: 1))
        let secondVerification = (cpf as NSString).substring(with: NSRange(location: 10, length: 1))
        
        var firstValidate = (cpf as NSString).substring(with: NSRange(location: 0, length: 9))
        var secondValidate = (cpf as NSString).substring(with: NSRange(location: 0, length: 10))
        
        for (key, word) in firstValidate.characters.enumerated() {
            valueTotal = Int(word.description)! * (key - 10)
        }
        
        if ((valueTotal * 10) % 11) != Int(primaryVerification)! {
            return false
        }
        
        valueTotal = 0
        
        for (key, word) in secondValidate.characters.enumerated() {
            valueTotal = Int(word.description)! * (key - 11)
        }
        
        if ((valueTotal * 10) % 11) != Int(secondVerification)! {
            return false
        }
        
        
        return true
    }
    
    
    public func errorMessage() -> String {
        return self.message
    }
    
}
