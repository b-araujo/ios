//
//  MyPasswordViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/7/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SkyFloatingLabelTextField

class MyPasswordViewController: UIViewController {

    @IBOutlet weak var senhaAtualTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var novaSenhaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var repeatNovaSenhaTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var alterPasswordButton: UIButton!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var myPassword : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alterPasswordButton.layer.cornerRadius = 5.0
        self.alterPasswordButton.layer.masksToBounds = false
        self.alterPasswordButton.layer.shadowColor = UIColor.black.cgColor
        self.alterPasswordButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.alterPasswordButton.layer.shadowRadius = 2.0
        self.alterPasswordButton.layer.shadowOpacity = 0.24
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            if user != nil && error == nil {
                
                self.myPassword = user!.password!
                RappleActivityIndicatorView.stopAnimation()
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func alterMyPassword ( _ sender : UIButton) {
        
        if self.senhaAtualTextField.text?.components(separatedBy: .whitespaces).joined() == "" {
            
            let alertController = UIAlertController(title: nil, message: "Digite sua senha atual", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.senhaAtualTextField.text! != self.myPassword {
            
            let alertController = UIAlertController(title: nil, message: "Senha atual inválida", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            self.senhaAtualTextField.errorMessage = "Senha Atual Inválida"
            
        } else if self.novaSenhaTextField.text?.components(separatedBy: .whitespaces).joined() == "" {
            
            let alertController = UIAlertController(title: nil, message: "Informe uma nova senha", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            self.senhaAtualTextField.errorMessage = nil
            
        } else if self.novaSenhaTextField.text != self.repeatNovaSenhaTextField.text {
            
            let alertController = UIAlertController(title: nil, message: "Senhas não conferem", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            self.senhaAtualTextField.errorMessage = nil
            
        } else {
            
            self.senhaAtualTextField.errorMessage = nil
            RappleActivityIndicatorView.startAnimatingWithLabel("Atualizando...", attributes: attributes)
            
            WishareAPI.updatePassword(newPassoword: self.novaSenhaTextField.text!, { (error : Error?) in
                if error == nil {
                   
                    RappleActivityIndicatorView.stopAnimation()
                    
                    let alertController = UIAlertController(title: "Sucesso", message: "Senha alterada com sucesso", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            })
            

            
        }
        
    }

}
