//
//  Products.swift
//  Wishare
//
//  Created by Vinicius França on 08/05/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Product : NSObject, NSCoding {
    
    var id : String?
    var pId : String?
    var name : String?
    var desc : String?
    var brand : String?
    var price : String?
    var promo_price : String?
    var unavailable : String?
    var quantity : String?
    var sizes : [Size] = []
    var picture : String?
    var pictures : [String] = []
    var model : String?
    var ref : String?
    var color : String?
    var allColor : [Color] = []
    var tags : [String] = []
    var sizes_guide : String?
    var storeId : String?
    var store : Store?
    
    var likes : String?
    var wishes : String?
    var shares : String?
    
    var ilike : String?
    var iwish : String?
    var ishare : String?
    var size : String?
    var select_quatity : String?
    
    var variationId: String?
    
    init( _ product : [String : AnyObject]) {
        
        let productAPI : [String : AnyObject] = product as [String : AnyObject]
        
        self.id = productAPI["id"] as? String
        self.pId = productAPI["pId"] as? String
        self.name = productAPI["name"] as? String
        self.desc = productAPI["description"] as? String
        self.brand = productAPI["brand"] as? String
        self.price = productAPI["price"] as? String
        self.promo_price = productAPI["promo_price"] as? String
        self.unavailable = productAPI["unavailable"] as? String
        self.quantity = productAPI["quantity"] as? String
        self.model = productAPI["model"] as? String
        self.ref = productAPI["ref"] as? String
        self.color = productAPI["color"] as? String
        self.storeId = productAPI["store_id"] as? String
        
        let pictures = productAPI["pictures"] as? [[String : String]]
        
        if let pic = pictures {
            for p in pic {
                if p["index"] == "0" {
                    self.picture = p["img"]
                }
                
            }
        }

        
        
        if let sizesAPI : [[String : AnyObject]] = productAPI["all_sizes"] as? [[String : AnyObject]] {
            
            if sizesAPI.count > 0 {
                
                for size in sizesAPI {
                    
                    let sizeInstance = Size(size)
                    self.sizes.append(sizeInstance)
                    
                }
                
            }
            
        }
    
    }
    
    
    
    init( byCategory product : [String : AnyObject]) {
        
        let productAPI : [String : AnyObject] = product["product"] as! [String : AnyObject]
        
        self.id = productAPI["id"] as? String
        self.pId = productAPI["pId"] as? String
        self.name = productAPI["name"] as? String
        self.desc = productAPI["description"] as? String
        self.brand = productAPI["brand"] as? String
        self.price = productAPI["price"] as? String
        self.promo_price = productAPI["promo_price"] as? String
        self.unavailable = productAPI["unavailable"] as? String
        self.quantity = productAPI["quantity"] as? String
        
        let pictures = productAPI["picture"] as? [[String : String]]
        
        if let pic = pictures {
            for p in pic {
                if p["index"] == "0" {
                    self.picture = p["img"]
                    break
                } else {
                    self.picture = p["img"]
                }
            }
        }
        
    }
    
    init( byCategory2 product : [String : AnyObject]) {
        
        let productAPI : [String : AnyObject] = product["product"] as! [String : AnyObject]
        
        self.id = productAPI["id"] as? String
        self.pId = productAPI["pId"] as? String
        self.name = productAPI["name"] as? String
        self.desc = productAPI["description"] as? String
        self.brand = productAPI["brand"] as? String
        self.price = productAPI["price"] as? String
        self.promo_price = productAPI["promo_price"] as? String
        self.unavailable = productAPI["unavailable"] as? String
        self.quantity = productAPI["quantity"] as? String
        
        let pictures = productAPI["picture"] as? [[String : AnyObject]]
        
        if let pic = pictures {
            for p in pic {
                let index = p["index"] as! String
                if index == "0"{
                    self.picture = p["img"] as? String
                }
                self.pictures.append(p["img"] as! String)
            }
        }
        
    }
    
    init( byCategoryDestaque product : [String : AnyObject]) {
        
        let productAPI : [String : AnyObject] = product["Product"] as! [String : AnyObject]
        
        self.id = productAPI["id"] as? String
        self.pId = productAPI["pId"] as? String
        self.name = productAPI["name"] as? String
        self.desc = productAPI["description"] as? String
        self.brand = productAPI["brand"] as? String
        self.price = productAPI["price"] as? String
        self.promo_price = productAPI["promo_price"] as? String
        self.unavailable = productAPI["unavailable"] as? String
        self.quantity = productAPI["quantity"] as? String
        
        let pictures = productAPI["picture"] as? [[String : String]]
        
        if let pic = pictures {
            for p in pic {
                if p["index"] == "0" {
                    self.picture = p["img"]
                    break
                } else {
                    self.picture = p["img"]
                }
            }
        }
        
    }
    
    init(forCategoryFeatured product : [String : AnyObject]) {
        
        if let product = product["product"] as? [String:AnyObject] {
            self.id = product["id"] as? String
            self.pId = product["pId"] as? String
            self.name = product["name"] as? String
            self.desc = product["description"] as? String
            self.brand = product["brand"] as? String
            self.price = product["price"] as? String
            self.promo_price = product["promo_price"] as? String
            self.unavailable = product["unavailable"] as? String
            self.quantity = product["quantity"] as? String
            
            let pictures = product["picture"] as? [[String : AnyObject]]

            if let pic = pictures {
                for p in pic {
                    let index = p["index"] as! String
                    if index == "0"{
                        self.picture = p["img"] as? String
                    }
                    self.pictures.append(p["img"] as! String)
                }
            }
        }
        
    }
    
    init( byProductDetail product : [String : AnyObject]) {
        
        self.id = product["id"] as? String
        self.pId = product["pId"] as? String
        self.name = product["name"] as? String
        self.desc = product["description"] as? String
        self.brand = product["brand"] as? String
        self.price = product["price"] as? String
        self.promo_price = product["promo_price"] as? String
        self.unavailable = product["unavailable"] as? String
        self.quantity = product["quantity"] as? String
        self.model = product["model"] as? String
        self.ref = product["ref"] as? String
        self.color = product["color"] as? String
        
        self.sizes_guide = product["sizes_guide"] as? String
        
        if (product["sizes_guide"] as? String)?.characters.first?.description != "<" && (product["sizes_guide"] as? String) != "" {
            print("<img style=\"width:100%;\" src=\"\(urlBase.absoluteString)files/size//\(product["sizes_guide"] as! String)\">")
            self.sizes_guide = "<img style=\"width:100%;\" src=\"\(urlBase.absoluteString)/files/size/\(product["pId"] as! String)/thumb/\(product["sizes_guide"] as! String)\">"
        }
        
        
        self.likes = product["likes"] as? String
        self.wishes = product["wishes"] as? String
        self.shares = product["shares"] as? String
        
        self.ilike = product["ilike"] as? String
        self.iwish = product["iwish"] as? String
        self.ishare = product["ishare"] as? String
        
        
        let allColor = product["all_color"] as? [[String : AnyObject]]
        
        if let colors = allColor {
            for color in colors {
                self.allColor.append(Color(byProductDetail: color))
            }
        }
        
        
        let pictures = product["picture"] as? [[String : AnyObject]]
        
        if let pic = pictures {
            for p in pic {
                let index = p["index"] as! String
                if index == "0"{
                    self.picture = p["img"] as? String
                }
                self.pictures.append(p["img"] as! String)
            }
        }
        
        if let sizesAPI : [[String : AnyObject]] = product["all_sizes"] as? [[String : AnyObject]] {
            if sizesAPI.count > 0 {
                for size in sizesAPI {
                    //let sizeInstance = Size(size)
                    let sizeInstance = Size(productDetailSize: size)
                    self.sizes.append(sizeInstance)
                }
            }
        }
        
        let tags = (product["tags"] as! String).components(separatedBy: ",")
        self.tags = tags
    
    }
    
    init(id : String, pId : String, name : String, desc : String, brand : String, price : String, promo_price : String, unavailable : String, quantity : String, picture : String) {

        self.id = id
        self.pId = pId
        self.name = name
        self.desc = desc
        self.brand = brand
        self.price = price
        self.promo_price = promo_price
        self.unavailable = unavailable
        self.quantity = quantity
        self.picture = picture
        self.sizes_guide = ""

    }
    
    init(id: String, picture: String, variationId: String) {
        self.id = id
        self.picture = picture
        self.variationId = variationId
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let pId = aDecoder.decodeObject(forKey: "pId") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let desc = aDecoder.decodeObject(forKey: "desc") as! String
        let brand = aDecoder.decodeObject(forKey: "brand") as! String
        let price = aDecoder.decodeObject(forKey: "price") as! String
        let promo_price = aDecoder.decodeObject(forKey: "promo_price") as! String
        let unavailable = aDecoder.decodeObject(forKey: "unavailable") as! String
        let quantity = aDecoder.decodeObject(forKey: "quantity") as! String
        let picture = aDecoder.decodeObject(forKey: "picture") as! String
        self.init(id: id, pId: pId, name: name, desc: desc, brand: brand, price: price, promo_price: promo_price, unavailable: unavailable, quantity: quantity, picture: picture)
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(pId, forKey: "pId")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(brand, forKey: "brand")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(promo_price, forKey: "promo_price")
        aCoder.encode(unavailable, forKey: "unavailable")
        aCoder.encode(quantity, forKey: "quantity")
        aCoder.encode(picture, forKey: "picture")
    }
    
    
    
}

struct ProductUpdate {
    var id: String
    var pId: String
    var price: String
    var quantity: String
}

