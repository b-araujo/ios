//
//  ImagesLookCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/27/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class ImagesLookCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageProductsLook: UIImageView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        self.imageProductsLook.image = nil
    }

}
