//
//  NotificationInformationTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 8/1/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class NotificationInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var previewPostImageView: UIImageView!
    
    var delegate : WishareNotificationDelegate? = nil
    var indexPath : IndexPath? = nil
    var tableView : UITableView? = nil
    
    var store : Store?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionLabel.isUserInteractionEnabled = true
        let tapGestureRecognizerDescriptionLabel = UITapGestureRecognizer(target: self, action: #selector(tapLabel(gesture:)))
        self.descriptionLabel.addGestureRecognizer(tapGestureRecognizerDescriptionLabel)
        
        
        let tapGestureRecognizerUserImageView = UITapGestureRecognizer(target: self, action: #selector(pressUserImageView))
        self.userImageView.addGestureRecognizer(tapGestureRecognizerUserImageView)
        
        self.userNameButton.addTarget(self, action: #selector(pressUserNameButton(_:)), for: .touchUpInside)
        
        let tapGestureRecognizerPreviewPostImageView = UITapGestureRecognizer(target: self, action: #selector(pressPostImageView))
        self.previewPostImageView.addGestureRecognizer(tapGestureRecognizerPreviewPostImageView)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func pressUserImageView() {
        
        if let tableView = self.tableView, let indexPath = self.indexPath {
            self.delegate?.openUserProfile(tableView, indexPath)
        }
    }
    
    @objc func tapLabel (gesture : UITapGestureRecognizer) {
        if let tableView = self.tableView, let indexPath = self.indexPath {
            self.delegate?.userProfile(tableView, indexPath, gesture)
        }
    }
    
    @objc func pressUserNameButton( _ sender : UIButton) {
        if let tableView = self.tableView, let indexPath = self.indexPath {
            self.delegate?.openUserProfile(tableView, indexPath)
        }
    }
    
    @objc func pressPostImageView() {
        if let tableView = self.tableView, let indexPath = self.indexPath {
            if store != nil {
                self.delegate?.openStore(tableView, indexPath)
            }
            else {
                self.delegate?.openPostInformation(tableView, indexPath)
            }
        }
    }
    
}
