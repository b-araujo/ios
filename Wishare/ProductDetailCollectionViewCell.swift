//
//  ProductDetailCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/16/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class ProductDetailCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceProductLabel: UILabel!
    @IBOutlet weak var parcelProductLabel: UILabel!
    @IBOutlet weak var promoPriceLabel: UILabel!
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var wishImageView: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var wishLabel: UILabel!
    
    @IBOutlet weak var guideSizeButton: UIButton!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    @IBOutlet weak var storePageLabel: UILabel!
    @IBOutlet weak var descriptionProductLabel: UILabel!
    @IBOutlet weak var referenceProductLabel: UILabel!
    @IBOutlet weak var modelProductLabel: UILabel!
    @IBOutlet weak var colorProductLabel: UILabel!
    @IBOutlet weak var brandProductLabel: UILabel!
    
    @IBOutlet weak var moreDescriptionButton: UIButton!
    
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var sizeView: UIView!
    @IBOutlet weak var otherColorView: UIView!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var tagTitleLabel: UILabel!
    @IBOutlet weak var sizeTitleLabel: UILabel!
    
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var descriptionToSizesConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionToColorsConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagsToDescription: NSLayoutConstraint!
    @IBOutlet weak var tagsToInformation: NSLayoutConstraint!
    @IBOutlet weak var sizeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagsHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var moreTextConstraint: NSLayoutConstraint!
    @IBOutlet weak var lessTextConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var colorToModelConstraint: NSLayoutConstraint!
    @IBOutlet weak var colorToReferenceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var colorToModelProductConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var colorToReferenceProductConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var modelLabel: UILabel!
    
    @IBOutlet weak var descriptionView: UIView!
    
    var images : [String]? = nil
    var delegate : ProductDetailDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageScrollView.delegate = self
        
        let tapGestureRecognizerStorePageLabel = UITapGestureRecognizer(target: self, action: #selector(pressStorePageLabel))
        self.storePageLabel.addGestureRecognizer(tapGestureRecognizerStorePageLabel)
        
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(like))
        self.likeImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        
        let tapGestureRecognizerWishImageView = UITapGestureRecognizer(target: self, action: #selector(wish))
        self.wishImageView.addGestureRecognizer(tapGestureRecognizerWishImageView)
        
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(share))
        self.shareImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        
        
        let tapGestureRecognizerLikeLabel = UITapGestureRecognizer(target: self, action: #selector(showLikes))
        self.likeLabel.addGestureRecognizer(tapGestureRecognizerLikeLabel)
        
        let tapGestureRecognizerWishLabel = UITapGestureRecognizer(target: self, action: #selector(showWishes))
        self.wishLabel.addGestureRecognizer(tapGestureRecognizerWishLabel)
        
    }
    
    @objc func pressStorePageLabel () {
        self.delegate?.openPageStoreByProductDetail()
    }
    
    @objc func like() {
        self.delegate?.likeProduct()
    }
    
    @objc func wish() {
        self.delegate?.wishProduct()
    }
    
    @objc func share() {
        self.delegate?.shareProduct()
    }
    
    @objc func showLikes() {
        self.delegate?.showLikes()
    }
    
    @objc func showWishes() {
        self.delegate?.showWishes()
    }
    
    
    @IBAction func moreDescription(_ sender: UIButton) {
        self.delegate?.moreDescription(sender)
    }
    
    @IBAction func lessDescription(_ sender: UIButton) {
        self.delegate?.lessDescription(sender)
    }
    
    @IBAction func showGuidesSize(_ sender: UIButton) {
        self.delegate?.showGuideSize(sender)
    }

}

protocol ProductDetailDelegate {

    func scrollViewDidScrollProduct ( _ scrollView : UIScrollView, cell : ProductDetailCollectionViewCell)
    func openPageStoreByProductDetail()
    func moreDescription( _ sender : UIButton)
    func lessDescription( _ sender : UIButton)
    func showGuideSize( _ sender : UIButton)
    func likeProduct()
    func wishProduct()
    func shareProduct()
    func showLikes()
    func showWishes()
    
}

extension ProductDetailCollectionViewCell : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.scrollViewDidScrollProduct(scrollView, cell: self)
    }
    
}




