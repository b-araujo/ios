//
//  WisharePostCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 3/26/18.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class WisharePostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var moreImageView: UIImageView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var commentUserLabel: ActiveLabel!
    @IBOutlet weak var postDateLabel: UILabel!
    
    @IBOutlet weak var userNameTopButton: UIButton!
    @IBOutlet weak var userNameBottomButton: UIButton!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    
    @IBOutlet weak var viewLikes: UIView!
    @IBOutlet weak var countLikeLabel: UILabel!
    @IBOutlet weak var countCommentsLabel: UILabel!
    @IBOutlet weak var showMoreButton: UIButton!
    
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    
    var delegate : WisharePostCollectionViewCellDelegate?
    var lastScale : CGFloat = 1.0
    var tagOnOrOff = false
    var indexPathNow : IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMore))
        let tapGestureRecognizerPostImageView = UITapGestureRecognizer(target: self, action: #selector(tapPostImage))
        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapLikesView))
        let tapGestureRecognizerCommentShowLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        self.postImageView.addGestureRecognizer(tapGestureRecognizerPostImageView)
        self.likeImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentImageView.addGestureRecognizer(tapGestureRecognizerCommentImageView)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        self.countCommentsLabel.addGestureRecognizer(tapGestureRecognizerCommentShowLabel)
                
        
//        let pinchGestureRecognizerPostImageView = UIPinchGestureRecognizer(target: self, action: #selector(zoom(sender:)))
//        self.postImageView.addGestureRecognizer(pinchGestureRecognizerPostImageView)
        
    }
    
    
    
    func zoom(sender: UIPinchGestureRecognizer) {
        
        if(sender.state == .began) {
            lastScale = sender.scale
        }
        
        if (sender.state == .began || sender.state == .changed) {
            let currentScale = sender.view!.layer.value(forKeyPath:"transform.scale")! as! CGFloat
            let kMaxScale:CGFloat = 2.0
            let kMinScale:CGFloat = 1.0
            var newScale = 1 -  (lastScale - sender.scale)
            newScale = min(newScale, kMaxScale / currentScale)
            newScale = max(newScale, kMinScale / currentScale)
            let transform = (sender.view?.transform)!.scaledBy(x: newScale, y: newScale);
            sender.view?.transform = transform
            lastScale = sender.scale
        }
        
    }
    
    @objc func tapMore() {
        self.delegate?.moreWSPost(cell: self)
    }
    
    @objc func tapLike() {
        self.delegate?.likeWSPost(cell: self)
    }
    
    @objc func tapComment() {
        self.delegate?.commentWSPost(cell: self)
    }
    
    @objc func tapPostImage() {
//        self.delegate?.postImageUserPost(cell: self)
    }

    @objc func tapLikesView() {
        self.delegate?.viewLikesWS(cell: self)
    }
    
    @IBAction func tapViewMore(_ sender: Any) {
        self.delegate?.showMoreCommentsWisharePost(cell: self, indexPath: indexPathNow)
    }
    
    
}

protocol WisharePostCollectionViewCellDelegate {
    func moreWSPost(cell : WisharePostCollectionViewCell)
    func likeWSPost(cell : WisharePostCollectionViewCell)
    func commentWSPost(cell : WisharePostCollectionViewCell)
    func postImageWSPost(cell : WisharePostCollectionViewCell)
    func viewLikesWS(cell : WisharePostCollectionViewCell)
    func showMoreCommentsWisharePost(cell : WisharePostCollectionViewCell, indexPath : IndexPath)
    
}
