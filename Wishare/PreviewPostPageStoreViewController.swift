//
//  PreviewPostPageStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/9/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD
import ENMBadgedBarButtonItem
import ActiveLabel


class PreviewPostPageStoreViewController: UIViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var posts : Post!
    var loader : Loader!
    var manager : Manager!
    
    var barButton : BadgedBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "SimpleStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "simpleStoreCell")
        self.collectionView.register(UINib(nibName: "ProductStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "productStoreCell")
        self.collectionView.register(UINib(nibName: "LookStoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "lookStoreCell")
        
        let logoImageView = UIImageView(image: UIImage(named: "logo_login")?.imageWithInsets(insets: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)))
        logoImageView.frame.size.width = 100;
        logoImageView.frame.size.height = 30;
        logoImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = logoImageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
        
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func myCart() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "commentStoreSegue" {
            
            let commentViewController = segue.destination as! CommentViewController
            commentViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "shareRepostSegue" {
            
            let repostViewController = segue.destination as! RepostViewController
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            repostViewController.imageRespost = senderValue[0] as? UIImage
            repostViewController.id = senderValue[1] as? String
            
        }
        
        if segue.identifier == "fotoLookStoreSegue" {
            
            let fotoLookViewController = segue.destination as! FotoLookViewController
            fotoLookViewController.post = sender as! Post
            
        }
        
        if segue.identifier == "showImagePost" {
            let showImagePostViewController = segue.destination as! ShowImagePostViewController
            showImagePostViewController.pictureImageString = sender as? UIImage
        }
        
        if segue.identifier == "showLikes" {
            let likeViewController = segue.destination as! LikeViewController
            likeViewController.post = sender as! Post
        }
        
    }
    
    func completionHandler(activityType: UIActivityType?, shared: Bool, items: [Any]?, error: Error?) {
        
        if shared {
            
            let alertController = UIAlertController(title: "Sucesso", message: "Compartilhamento realizado com sucesso!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }

}

extension PreviewPostPageStoreViewController : UICollectionViewDelegate {
    
}

extension PreviewPostPageStoreViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.posts.typePost {
            
        case 2:
            
            // POST DE LOJA SIMPLES
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simpleStoreCell", for: indexPath) as! SimpleStoreCollectionViewCell
            
            cell.delegate = self
            cell.IndexPathNow = indexPath
            
            if self.posts.showMoreComments {
                cell.commentsLabel.numberOfLines = 0
            } else {
                cell.commentsLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.nameStoreTopButton.setTitle(self.posts.store!.name!, for: .normal)
            cell.nameStoreBottomButton.setTitle(self.posts.store!.name!, for: .normal)
            //cell.commentsLabel.text = self.posts.text!
            cell.datePostLabel.text = self.posts.created!.convertToDate().timeAgoDisplay()
            cell.postStoreImageView.image = nil
            
            // =========== Hashtags/Mention ===========
            
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentsLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentsLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentsLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            
            cell.commentsLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentsLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts.text!.contains("@id:") && self.posts.text!.contains("@username:")) || (self.posts.text!.contains("@id:") && self.posts.text!.contains("@storename:")) {
                    
                    let comment = self.posts.text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts.text!.contains("@id:") && self.posts.text!.contains("@username:")) || (self.posts.text!.contains("@id:") && self.posts.text!.contains("@storename:")) {
                
                let comment = self.posts.text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentsLabel.text = comments_final + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                //cell.commentsLabel.text = self.posts.text! + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentsLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts.showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentsLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentsLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentsLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
                
            }
            
//            cell.commentsLabel.text = self.posts.text! + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            // ========================================
            
            
            // - CURTIDAS
            if self.posts.likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.countLikesLabel.text = "\(self.posts.likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts.comments! > 0 {
                cell.countCommentsLabel.isHidden = false
                cell.countCommentsLabel.text = "Ver \(self.posts.comments!) comentário(s)"
            } else {
                cell.countCommentsLabel.isHidden = true
            }
            
            if self.posts.ilikes == "1" {
                cell.likeImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeImageView.image = UIImage(named: "Like")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            
            let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.posts.store!.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.postStoreImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.postStoreImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.storeImageView) { ( result, _ ) in
                cell.storeImageView.image = result.value?.circleMask
            }
            
            return cell
            
        case 3:
            
            // POST DE LOJA PRODUTO
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productStoreCell", for: indexPath) as! ProductStoreCollectionViewCell
            
            cell.delegate = self
            cell.indexPathNow = indexPath
            
            if self.posts.showMoreComments {
                cell.commentStoreLabel.numberOfLines = 0
            } else {
                cell.commentStoreLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.nameStoreTopButton.setTitle(self.posts.store!.name!, for: .normal)
            cell.nameStoreBottomButton.setTitle(self.posts.store!.name!, for: .normal)
            //cell.commentStoreLabel.text = self.posts.text!
            cell.publishedDateLabel.text = self.posts.created!.convertToDate().timeAgoDisplay()
            cell.storePostImageView.image = nil
            
            // =========== Hashtags/Mention ===========
            
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")

            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentStoreLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentStoreLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts.text!.contains("@id:") && self.posts.text!.contains("@username:")) || (self.posts.text!.contains("@id:") && self.posts.text!.contains("@storename:")) {
                    
                    let comment = self.posts.text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            
            if (self.posts.text!.contains("@id:") && self.posts.text!.contains("@username:")) || (self.posts.text!.contains("@id:") && self.posts.text!.contains("@storename:")) {
                
                let comment = self.posts.text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                
                if (heightLabelWithText(self.view.bounds.width - 20, self.posts.text!) + 18.0) <= 30.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + comments_final + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts.showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + comments_final + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, self.posts.text!) + 18.0) > 54.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts.store!.name!)~" + " " + comments_final + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.55))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + comments_final + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
                
            } else {
                
                
                
                let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts.showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
                
            }
            
//            cell.commentStoreLabel.text = self.posts.text! + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            // ========================================
            
            
            // - CURTIDAS
            if self.posts.likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts.likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts.comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts.comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            
            if self.posts.ilikes == "1" {
                cell.likeProductStoreImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeProductStoreImageView.image = UIImage(named: "Like")
            }
            
            if self.posts.iwish == "1" {
                cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.posts.store!.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.storeThumbImageView) { ( result, _ ) in
                cell.storeThumbImageView.image = result.value?.circleMask
            }
            
            return cell
            
        default:
            
            // POST DE LOJA LOOK
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lookStoreCell", for: indexPath) as! LookStoreCollectionViewCell
            
            cell.delegate = self
            cell.indexPathNow = indexPath
            cell.collectionViewCarouselLook.reloadData()
            
            if self.posts.showMoreComments {
                cell.commentStoreLabel.numberOfLines = 0
            } else {
                cell.commentStoreLabel.numberOfLines = 3
            }
            
            cell.loadingActivityIndicator.startAnimating()
            cell.nameStoreTopButton.setTitle(self.posts.store!.name!, for: .normal)
            cell.nameStoreBottomButton.setTitle(self.posts.store!.name!, for: .normal)
            //cell.commentStoreLabel.text = self.posts.text!
            cell.publishedDateLabel.text = self.posts.created!.convertToDate().timeAgoDisplay()
            cell.storePostImageView.image = nil
            
            // =========== Hashtags/Mention ===========
            
            let customType = ActiveType.custom(pattern: "\\^[À-ÿ0-9\\w\\s-!?%.]+\\~")
            
            cell.commentStoreLabel.enabledTypes = [.hashtag, .mention, customType]
            
            cell.commentStoreLabel.customColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            cell.commentStoreLabel.customSelectedColor[customType] = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            
            
            cell.commentStoreLabel.handleHashtagTap({ (hashtag : String) in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let showProductHashTagsViewController = storyboard.instantiateViewController(withIdentifier: "hashTagsProductsID") as! ShowProductHashTagsViewController
                showProductHashTagsViewController.textHashTag = hashtag
                
                self.navigationController?.pushViewController(showProductHashTagsViewController, animated: true)
                
            })
            
            cell.commentStoreLabel.handleMentionTap({ (mention : String) in
                
                if (self.posts.text!.contains("@id:") && self.posts.text!.contains("@username:")) || (self.posts.text!.contains("@id:") && self.posts.text!.contains("@storename:")) {
                    
                    let comment = self.posts.text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                        return value != "" ? true : false
                    })
                    
                    
                    var isActiveMention = false
                    
                    for word in comment {
                        
                        let information_comments = word.components(separatedBy: ":")
                        
                        if (word.contains("@id:") && word.contains("@storename:")) {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                self.performSegue(withIdentifier: "pageStoreSegue", sender: information_comments[1].components(separatedBy: ",")[0])
                                
                            }
                            
                            
                        } else if (word.contains("@id:") && word.contains("@username:"))  {
                            
                            if information_comments.last! == mention && isActiveMention == false {
                                
                                isActiveMention = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
                                pageUserViewController.isMyProfile = false
                                pageUserViewController.idUser = information_comments[1].components(separatedBy: ",")[0]
                                
                                self.navigationController?.pushViewController(pageUserViewController, animated: true)
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
            })
            
            if (self.posts.text!.contains("@id:") && self.posts.text!.contains("@username:")) || (self.posts.text!.contains("@id:") && self.posts.text!.contains("@storename:")) {
                
                let comment = self.posts.text!.components(separatedBy: CharacterSet(charactersIn: "[]")).filter({ (value : String) -> Bool in
                    return value != "" ? true : false
                })
                
                var comments_final : String = ""
                
                for word in comment {
                    
                    if (word.contains("@id:") && word.contains("@username:")) || (word.contains("@id:") && word.contains("@storename:")) {
                        let information_comments = word.components(separatedBy: ":")
                        comments_final += "@\(information_comments.last!)"
                    } else {
                        comments_final += word
                    }
                    
                }
                
                cell.commentStoreLabel.text = comments_final + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
            } else {
                //cell.commentStoreLabel.text = self.posts.text! + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                
                if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) <= 36.0 {
                    
                    cell.moreComment.isHidden = true
                    cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                    
                } else {
                    
                    if self.posts.showMoreComments {
                        
                        cell.moreComment.isHidden = true
                        cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        
                    } else {
                        
                        if (heightLabelWithText(self.view.bounds.width - 20, string) + 18.0) > 72.0 {
                            cell.moreComment.isHidden = false
                            let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                            cell.commentStoreLabel.text = (string as NSString).substring(with: NSRange(location: 0, length: Int(self.view.bounds.width / 3.65))) + " ..."
                            
                        } else {
                            cell.moreComment.isHidden = true
                            cell.commentStoreLabel.text = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
                        }
                        
                    }
                    
                }
            }
            
//            cell.commentStoreLabel.text = self.posts.text! + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
            
            // ========================================
            
            
            // - CURTIDAS
            if self.posts.likes! > 0 {
                
                cell.viewLikes.isHidden = false
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                
                cell.likesStoreLabel.text = "\(self.posts.likes!) curtida(s)"
                
            } else {
                
                cell.viewLikes.isHidden = true
                
                cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                
            }
            
            
            // - COMENTARIOS
            if self.posts.comments! > 0 {
                cell.commentsStoreLabel.isHidden = false
                cell.commentsStoreLabel.text = "Ver \(self.posts.comments!) comentário(s)"
            } else {
                cell.commentsStoreLabel.isHidden = true
            }
            
            if self.posts.ilikes == "1" {
                cell.likeLookStoreImageView.image = UIImage(named: "Like Filled")
            } else {
                cell.likeLookStoreImageView.image = UIImage(named: "Like")
            }
            
            if self.posts.iwish == "1" {
                cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
            } else {
                cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
            }
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != "" {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let urlPerfil = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.posts.store!.picture!)")
            
            
            self.manager.loadImage(with: url, into: cell.storePostImageView) { ( result, _ ) in
                cell.loadingActivityIndicator.stopAnimating()
                cell.storePostImageView.image = result.value
            }
            
            self.manager.loadImage(with: urlPerfil, into: cell.storeThumbImageView) { ( result, _ ) in
                cell.storeThumbImageView.image = result.value?.circleMask
            }
            
            
            return cell
            
        }
        
        
        
        
        
        

        
    }
    
    
    
    
}

extension PreviewPostPageStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var base : CGFloat = 0.0
        if self.posts.comments! == 0 { base += 30.0 }
        if self.posts.likes! == 0 { base += 30.0 }
        
        
        
        let string = "^\(self.posts.store!.name!)~" + " " + self.posts.text!.components(separatedBy: .newlines).joined() + " " + self.posts.hashtag!.components(separatedBy: ",").joined(separator: " ")
        
        let labelSize = heightLabelWithText(self.view.bounds.width - 20.0, string) + 18.0
        
        var size : CGFloat = 0.0
        
        if labelSize <= 36.0 {
            
            size = CGFloat(270.0)
            
        } else {
            
//            if self.posts.showMoreComments {
            
                size = CGFloat(240.0) + labelSize
//
//            } else {
//
//                if labelSize >= 72.0 {
//                    size = CGFloat(240.0) + CGFloat(54.0)
//                } else {
//                    size = CGFloat(240.0) + (labelSize - 18.0)
//                }
//
//            }
            
        }
        
        return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (size - base))
        
//        return CGSize(width: self.view.bounds.width, height: self.view.bounds.width + (270 - base))
        
    }
    
}

// MARK: - SimpleStoreCollectionViewDelegate
extension PreviewPostPageStoreViewController : SimpleStoreCollectionViewCellDelegate {
    func viewLikes(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showLikes", sender: self.posts)
    }
    
    
    func showMoreCommentsSimpleStore(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        
    }

    func moreSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
        WishareCoreData.getFirstUser { (user : UserActive?, error : Error?) in
            
            if error ==  nil && user != nil {
                
                if self.posts.user_id == user!.id {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let editarAction = UIAlertAction(title: "Editar", style: .default, handler: { (alert : UIAlertAction) in
                        self.performSegue(withIdentifier: "publishEditingSegue", sender: [self.posts, cell.IndexPathNow])
                    })
                    
                    let excluirAction = UIAlertAction(title: "Excluir", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let alertController = UIAlertController(title: "Excluir Post", message: "Deseja excluir este post?", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert : UIAlertAction) in
                            
                            let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                            RappleActivityIndicatorView.startAnimatingWithLabel("Excluindo...", attributes: attributes)
                            
                            WishareAPI.postDelete(self.posts.id!, { (error : Error?) in
                                
                                if error == nil {
                                    
                                    RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: "Excluído", completionTimeout: 1)
                                    
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer : Timer) in
                                        
                                        //self.posts.remove(at: cell.IndexPathNow.row)
                                        //self.feedCollectionView.deleteItems(at: [cell.IndexPathNow])
                                        self.collectionView.reloadData()
                                        
                                    })
                                    
                                }
                                
                            })
                            
                            
                        })
                        
                        let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
                        
                        alertController.addAction(okAction)
                        alertController.addAction(cancelarAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(editarAction)
                    alertController.addAction(excluirAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                } else {
                    
                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                    let denunciarAction = UIAlertAction(title: "Denunciar", style: .destructive, handler: { (alert : UIAlertAction) in
                        
                        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
                        RappleActivityIndicatorView.startAnimatingWithLabel("Denunciando...", attributes: attributes)
                        
                        WishareAPI.toReport(self.posts.id!, { (error : Error?) in
                            
                            if error == nil {
                                
                                RappleActivityIndicatorView.stopAnimation()
                                
                                let alertController = UIAlertController(title: "Algo errado?", message: "Obrigado pela informação.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                            
                        })
                        
                    })
                    
                    alertController.addAction(cancelarAction)
                    alertController.addAction(denunciarAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
            }
            
        }
        
    }
    
    func likeSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
        if self.posts.ilikes == "1" {
            
            self.posts.ilikes = "0"
            self.posts.likes = self.posts.likes! - 1
            //self.feedCollectionView.reloadItems(at: [cell.IndexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.posts.id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts.likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            //likeAnimation(cell.likeEffectsImageView)
            self.posts.ilikes = "1"
            self.posts.likes = self.posts.likes! + 1
            //self.feedCollectionView.reloadItems(at: [cell.IndexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.posts.id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts.likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
            
            
        }
        
        
    }
    
    func commentSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentStoreSegue", sender: self.posts)
    }
    
    func shareSimpleStore(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts.id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
                
                
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    

    
    func storeSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
    }
    
    func storePostSimpleStore(cell: SimpleStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "showImagePost", sender: cell.postStoreImageView.image)
    }
    
    func nameStoreSimpleStore(cell: SimpleStoreCollectionViewCell) {
        
    }
    
    
}

// MARK: - ProductStoreCollectionViewCellDelegate
extension PreviewPostPageStoreViewController : ProductStoreCollectionViewCellDelegate {
    
    func showMoreCommentsProductStore(cell: ProductStoreCollectionViewCell, indexPath: IndexPath) {
        
    }
    
    func storeThumbProductStore(cell: ProductStoreCollectionViewCell) {
        
    }
    
    func nameStoreProductStore(cell: ProductStoreCollectionViewCell) {
        
    }
    
    func buyProductStore(cell: ProductStoreCollectionViewCell) {
        
        
        if self.posts.products_info[0].unavailable! == "I" && self.posts.products_info[0].quantity! == "0" {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.posts.products_info[0].unavailable! == "E" && self.posts.products_info[0].quantity! == "0" {
            
            
            let alertController = UIAlertController(title: "OPS, O ESTOQUE FOI VENDIDO!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                WishareAPI.waitingStock(self.posts.products_info[0].id!, { (error : Error?) in })
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts.products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
        
    }
    
    func postProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.posts.products_info[0].unavailable! == "I" && self.posts.products_info[0].quantity! == "0" {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.posts.products_info[0].unavailable! == "E" && self.posts.products_info[0].quantity! == "0" {
            
            
            let alertController = UIAlertController(title: "OPS, O ESTOQUE FOI VENDIDO!", message: "Clique no botão abaixo e avisaremos assim que o produto estiver disponível", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Avise-me quando chegar", style: .default, handler: { (action : UIAlertAction) in
                WishareAPI.waitingStock(self.posts.products_info[0].id!, { (error : Error?) in })
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts.products[0]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
        
    }
    
    func likeProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.posts.ilikes == "1" {
            
            self.posts.ilikes = "0"
            self.posts.likes = self.posts.likes! - 1
            //self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.posts.id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts.likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                    
                    
                }
                
            })
            
            
            
        } else {
            
            //likeAnimation(cell.likeEffectsImageView)
            self.posts.ilikes = "1"
            self.posts.likes = self.posts.likes! + 1
            //self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            WishareAPI.like(self.posts.id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts.likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                    }
                    
                }
                
            })
            
            
            
        }
        
        
    }
    
    func commentProductStore(cell: ProductStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentStoreSegue", sender: self.posts)
    }
    
    func shareProductStore(cell: ProductStoreCollectionViewCell, indexPath : IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts.id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func wishListProductStore(cell: ProductStoreCollectionViewCell) {
        
        if self.posts.iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.posts.id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.posts.iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.posts.id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishListProductStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.posts.iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
}

// MARK: - LookStoreCollectionViewCellDelegate
extension PreviewPostPageStoreViewController : LookStoreCollectionViewCellDelegate {
    func viewLikesLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        func viewLikes(cell: SimpleStoreCollectionViewCell, indexPath: IndexPath) {
            self.performSegue(withIdentifier: "showLikes", sender: self.posts)
        }
    }
    
    
    func showMoreCommentsLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        
    }
    
    func numberOfItemsInSectionLook(_ cellForLook : LookStoreCollectionViewCell) -> Int {
        return self.posts.products.count
    }
    
    func cellForItemLook(_ collection: UICollectionView, _ indexPath: IndexPath, _ cellForLook : LookStoreCollectionViewCell) -> UICollectionViewCell {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "imagesLookCell", for: indexPath) as! ImagesLookCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[indexPath.row]["url"]!)")
        
        cell.loadingActivityIndicator.startAnimating()
        self.manager.loadImage(with: url, into: cell.imageProductsLook) { ( result, _ ) in
            cell.loadingActivityIndicator.stopAnimating()
            cell.imageProductsLook.image = result.value
        }
        
        
        return cell
    }
    
    func selectedItem(_ collection: UICollectionView, _ indexPath: IndexPath, _ now : IndexPath) {
        
        var isValidate : Bool = false
        
        
        for prod_info in self.posts.products_info {
            
            if prod_info.id! == self.posts.products[indexPath.row]["id"] {
                isValidate = true
            }
            
        }
        
        if !isValidate {
            
            let alertController = UIAlertController(title: "Produto Indisponivel", message: "Esse produto esta indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.posts.products[indexPath.row]["id"]
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
            
            print("IR PARA LOJA :)")
        }
        
    }
    
    
    func buyLookStore( _ sender : UIButton, _ indexPath : IndexPath) {
        
        if self.posts.products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookStoreSegue", sender: self.posts)
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
    }
    
    
    func buyLookStoreImage(_ indexPath: IndexPath) {
        
        if self.posts.products_info.count > 0 {
            
            self.performSegue(withIdentifier: "fotoLookStoreSegue", sender: self.posts)
            
        } else {
            
            let alertController = UIAlertController(title: "Produtos Indisponivel", message: "Todos os produtos estao indisponivel no momento", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func storeThumbLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func buyLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func postLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
    func likeLookStore(cell: LookStoreCollectionViewCell) {
        
        if self.posts.ilikes == "1" {
            
            self.posts.ilikes = "0"
            self.posts.likes = self.posts.likes! - 1
            
            //self..reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            let indexPath = IndexPath(row: 0, section: 0)
            cell.collectionViewCarouselLook.selectItem(at: indexPath, animated: false, scrollPosition: .left)
            
            WishareAPI.like(self.posts.id!, false, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts.likes == 0 {
                        cell.viewLikes.isHidden = true
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 999)
                    }
                    
                }
                
            })
            
            
            
        } else {
            
            //likeAnimation(cell.likeEffectsImageView)
            self.posts.ilikes = "1"
            self.posts.likes = self.posts.likes! + 1
            
            //self.feedCollectionView.reloadItems(at: [cell.indexPathNow])
            self.collectionView.reloadData()
            
            let indexPath = IndexPath(row: 0, section: 0)
            cell.collectionViewCarouselLook.selectItem(at: indexPath, animated: false, scrollPosition: .left)
            
            
            WishareAPI.like(self.posts.id!, true, { (validate : Bool, error : Error?) in
                
                if error == nil {
                    
                    if self.posts.likes! == 1 {
                        cell.viewLikes.isHidden = false
                        cell.nameButtonToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.nameButtonToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        cell.comentLabelToLikesConstraints.priority = UILayoutPriority(rawValue: 999)
                        cell.comentLabelToLineConstraints.priority = UILayoutPriority(rawValue: 750)
                        
                        
                    }
                    
                }
                
            })
            
            
            
        }
        
        
    }
    
    func commentLookStore(cell: LookStoreCollectionViewCell) {
        self.performSegue(withIdentifier: "commentStoreSegue", sender: self.posts)
    }
    
    func shareLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publishedMessageShareAction = UIAlertAction(title: "Escrever na Publicação", style: .default, image: UIImage(named: "publish")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                self.performSegue(withIdentifier: "shareRepostSegue", sender: [result.value!, self.posts.id!])
            }
            
        }
        
        let shareAction = UIAlertAction(title: "Compartilhar com ...", style: .default, image: UIImage(named: "share")!) { (alert : UIAlertAction) in
            
            //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
            let url : URL!
            
            if self.posts.picture != nil && self.posts.picture! != ""   {
                url = urlBase.appendingPathComponent("files/posts-image/\(self.posts.id!)/thumb/\(self.posts.picture!)")
            } else {
                url = urlBase.appendingPathComponent("files/posts-image/products/\(self.posts.products[0]["url"]!)")
            }
            
            let imageView : UIImageView = UIImageView()
            
            self.manager.loadImage(with: url, into: imageView) { ( result, _ ) in
                
                let img2 = UIImage(named: "logo-share")!
                
                let rect = CGRect(x: 0, y: 0, width: result.value!.size.width, height: result.value!.size.height)
                
                UIGraphicsBeginImageContextWithOptions(result.value!.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context?.setFillColor(UIColor.white.cgColor)
                context?.fill(rect)
                
                result.value!.draw(in: rect, blendMode: .normal, alpha: 1)
                img2.draw(in: CGRect(x: result.value!.size.width - 360, y: result.value!.size.height - 110, width: 350, height: 100), blendMode: .normal, alpha: 0.6)
                
                
                let result = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                
                let activityViewController = UIActivityViewController(activityItems: [(result!)], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                
                self.present(activityViewController, animated: true, completion: nil)
                
                activityViewController.completionWithItemsHandler = self.completionHandler
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(publishedMessageShareAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func wishlistLookStore(cell: LookStoreCollectionViewCell) {
        
        if self.posts.iwish == "1" {
            
            let alertController = UIAlertController(title: nil, message: "Deseja excluir esse produto da sua lista de desejos ?", preferredStyle: .alert)
            let confirmarAction = UIAlertAction(title: "Confirmar", style: .default, handler: { (alert : UIAlertAction) in
                
                WishareAPI.wishlist(self.posts.id!, false, { (error : Error?) in
                    
                    if error == nil {
                        cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist")
                        self.posts.iwish = "0"
                    }
                    
                })
                
            })
            
            let cancelarAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alertController.addAction(confirmarAction)
            alertController.addAction(cancelarAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let alertController = UIAlertController(title: nil, message: "Você adicionou este produto na sua lista de desejos.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            
            WishareAPI.wishlist(self.posts.id!, true, { (error : Error?) in
                
                if error == nil {
                    cell.wishListLookStoreImageView.image = UIImage(named: "Bot_C3_A3o_Wishlist_A_C3_A7_C3_A3o")
                    self.posts.iwish = "1"
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }
        
    }
    
    func storeNameLookStore(cell: LookStoreCollectionViewCell) {
        
    }
    
}
