//
//  WishareProtocol.swift
//  Wishare
//
//  Created by Wishare iMac on 5/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import UIKit

protocol WishareBasePostDelegate {
    func like(cell : UICollectionViewCell)
    func share(cell: UICollectionViewCell, indexPath : IndexPath)
    func wishlist(cell : UICollectionViewCell)
    func showLikes(cell: UICollectionViewCell)
    func showComments(cell: UICollectionViewCell)
    func more(cell : UICollectionViewCell)
    
    func thumbStorePostImageView(cell : UICollectionViewCell)
    func userNameStorePostLabel(cell: UICollectionViewCell)
    
}
