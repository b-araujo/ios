//
//  ShowImagePostViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 4/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class ShowImagePostViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    var pictureImageString : UIImage?
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.delegate = self
        
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        
        if let image = self.pictureImageString {
            self.postImageView.image = image
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.postImageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        
        if scrollView.zoomScale == 1.0 {
            
            self.backButton.alpha = 0.0
            self.backButton.isHidden = true
            
        }
        
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        
        if scale == 1.0 {
            
            self.backButton.isHidden = false
            
            UIView.animate(withDuration: 0.2, animations: {
                
                self.backButton.alpha = 1.0
                
            })
            
        }
        
    }

    @IBAction func backFeed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}
