//
//  ProfileImage.swift
//  Wishare
//
//  Created by Wishare iMac on 4/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class ProfileImage {
    
    var type : String?
    var url : String? {
        didSet {
            if let urlUnwrapped = url {
                if let fullUrlString = URL(string: urlBase.absoluteString + urlUnwrapped) {
                    let lastPathUrl = fullUrlString.lastPathComponent
                    let urlWithoutLastComponent = fullUrlString.deletingLastPathComponent()
                    
                    self.urlThumb = urlWithoutLastComponent.absoluteString + "thumb/" + lastPathUrl
                    self.urlPost = urlWithoutLastComponent.absoluteString + "post/" + lastPathUrl
                    self.urlOriginal = fullUrlString.absoluteString
                }
            
            }
        }
    }
    var url_local : Int?
    
    var urlThumb: String?
    var urlPost: String?
    var urlOriginal: String?
    
}
