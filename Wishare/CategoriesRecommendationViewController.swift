//
//  CategoriesRecommendationViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 8/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Pager
import RappleProgressHUD
import Nuke
import NukeAlamofirePlugin

class CategoriesRecommendationViewController: PagerController {
    
    var controllers : [CategoriesShowViewController] = []
    var titles : [String] = []
    var loader : Loader!
    var manager : Manager!
    
    var items_search : [[String]] = [[],[],[],[]]
    var tab_selected : Int = 0
    var auto_complete : Bool = false
    let defaults = UserDefaults.standard
    var isFirstAccess : Bool = true
    var isLoadingOrNot : Bool = false
    
    let searchView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.red
        view.tag = 24
        view.isHidden = true
        return view
    }()
    
    let searchTableView : UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.white
        tableView.tag = 25
        tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.onDrag
        return tableView
    }()
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    var productsResult = [Product]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdated"), object: productsResult)
        }
    }
    
    var usersResult = [User]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("usersResultUpdated"), object: usersResult)
        }
    }
    
    var storesResult = [Store]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("storesResultUpdated"), object: storesResult)
        }
    }
    
    var wipsResult = [User]() {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name.init("usersWipsResultUpdated"), object: wipsResult)
        }
    }
    
    var resultSearchController : UISearchController!
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        customizeTabs()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        WishareAPI.suggestMenu { (categories : [Category], error : Error?) in
            
            if error == nil {
                
                for c in categories {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let categoriesShowViewController = storyboard.instantiateViewController(withIdentifier: "categoriesShowID") as! CategoriesShowViewController
                    categoriesShowViewController.category_id = c.id!
                        
                    self.titles.append(c.name!)
                    self.controllers.append(categoriesShowViewController)
                
                }
                
                
                self.setupPager(tabNames: self.titles, tabControllers: self.controllers)
                self.reloadData()
                
            }
            
        }
        
        
        NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: false)

        NotificationCenter.default.addObserver(self, selector: #selector(updateChangeTabs(notification:)), name: Notification.Name.init("changeTabs"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPageStore(notification:)), name: Notification.Name.init("openPageStore"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPageUser(notification:)), name: Notification.Name.init("openPageUser"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openProductDetail(notification:)), name: Notification.Name.init("openProductDetail"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(isLoadingOrNot(notification:)), name: Notification.Name.init("isLoadingOrNot"), object: nil)
        
        
        self.searchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        
        
        resultSearchController = UISearchController(searchResultsController: GeneralSearchViewController())
        resultSearchController.searchResultsUpdater = self
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        resultSearchController.searchBar.searchBarStyle = .prominent
        resultSearchController.searchBar.sizeToFit()
        resultSearchController.searchBar.placeholder = "Pesquisar"
        resultSearchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
        resultSearchController.searchBar.delegate = self
        
        resultSearchController.view.addSubview(searchView)
        
        searchView.bottomAnchor.constraint(equalTo: resultSearchController.view.bottomAnchor).isActive = true
        searchView.leftAnchor.constraint(equalTo: resultSearchController.view.leftAnchor).isActive = true
        searchView.rightAnchor.constraint(equalTo: resultSearchController.view.rightAnchor).isActive = true
        searchView.topAnchor.constraint(equalTo: resultSearchController.view.topAnchor, constant: 64).isActive = true
        
        searchView.addSubview(searchTableView)
        
        searchTableView.topAnchor.constraint(equalTo: searchView.topAnchor).isActive = true
        searchTableView.bottomAnchor.constraint(equalTo: searchView.bottomAnchor).isActive = true
        searchTableView.leftAnchor.constraint(equalTo: searchView.leftAnchor).isActive = true
        searchTableView.rightAnchor.constraint(equalTo: searchView.rightAnchor).isActive = true
        
        self.navigationItem.titleView = resultSearchController.searchBar
        definesPresentationContext = true
        
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("SAIU")
        self.isFirstAccess = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("REFRESH SUGESTOES")
        
        if !self.isFirstAccess {
            
            if !self.isLoadingOrNot {
                
                RappleActivityIndicatorView.startAnimatingWithLabel("Carregando Produtos...", attributes: attributes)
                
                WishareAPI.suggestMenu { (categories : [Category], error : Error?) in
                    
                    if error == nil {
                        
                        self.titles.removeAll()
                        self.controllers.removeAll()
                        
                        
                        for c in categories {
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let categoriesShowViewController = storyboard.instantiateViewController(withIdentifier: "categoriesShowID") as! CategoriesShowViewController
                            categoriesShowViewController.category_id = c.id!
                            
                            self.titles.append(c.name!)
                            self.controllers.append(categoriesShowViewController)
                            
                        }
                        
                        
                        self.setupPager(tabNames: self.titles, tabControllers: self.controllers)
                        self.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                        
                    }
                    
                }
                
            }

            
        }
        
        self.isLoadingOrNot = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func customizeTabs() {
        self.indicatorColor = UIColor.black
        self.contentViewBackgroundColor = UIColor.white
        self.tabsTextColor = UIColor.black
        self.selectedTabTextColor = UIColor.black
        self.tabsViewBackgroundColor = UIColor.white
        self.tabWidth = 160.0
    }
    
    @objc func updateChangeTabs(notification : Notification) {
        
        let tabSelected = notification.object as! Int
        self.tab_selected = tabSelected
        
        print("====== ABA ATUAL =======")
        print("\(self.tab_selected)/\(tabSelected)")
        print("========================")
        
        if self.tab_selected != 3 {
            self.searchTableView.reloadData()
        }
        
    }
    
    @objc func openPageStore(notification : Notification) {
        let idStore = notification.object as! String
        self.performSegue(withIdentifier: "pageStoreSegue", sender: idStore)
    }
    
    @objc func openPageUser(notification : Notification) {
        let idUser = notification.object as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageUserViewController = storyboard.instantiateViewController(withIdentifier: "pageUserID") as! PageUserViewController
        pageUserViewController.isMyProfile = false
        pageUserViewController.idUser = idUser
        
        self.navigationController?.pushViewController(pageUserViewController, animated: true)
    }
    
    @objc func openProductDetail(notification : Notification) {
        
        let idProduct = notification.object as! String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
        productDetailViewController.idProduct = idProduct
        productDetailViewController.isFirst = true
        
        self.navigationController?.pushViewController(productDetailViewController, animated: true)
        
    }
    
    @objc func isLoadingOrNot(notification : Notification) {
        
        self.isLoadingOrNot = true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pageStoreSegue" {
            let pageStoreViewController = segue.destination as! PageStoreViewController
            pageStoreViewController.idStore = sender as! String
        }
        
    }
    
}

extension CategoriesRecommendationViewController : PagerDataSource {
    
    func numberOfTabs(_ pager: PagerController) -> Int {
        return self.titles.count
    }
    
}

extension CategoriesRecommendationViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        searchController.searchResultsController?.view.isHidden = false
    }
    
}

extension CategoriesRecommendationViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: resultSearchController)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("BUSCANDO ...")
        
        self.searchView.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: true)
        
        if let searchText = searchBar.text {
            
            WishareAPI.searchTabs(searchText, { (products : [Product], stores : [Store], users : [User], wips : [User], error : Error?) in
                
                if error == nil {
                    
                    // ATUALIZAR RESULTS CONTROLLER
                    self.productsResult = products
                    self.usersResult = users
                    self.storesResult = stores
                    self.wipsResult = wips
                    NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: false)
                    NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: false)
                    NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: false)
                    NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: true)
                    UserDefaults.standard.set(true, forKey: "isSearchTabs")
                    
                    // - AQUI
                    self.defaults.set(searchText, forKey: "word_search")
                    
                }
                
            })
            
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        print("Cancel")
        self.searchView.isHidden = true
        
        // - PERSONALIZADO RECARREGAR AO CLICAR NO CANCELAR
        
        self.searchView.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: true)
        NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: true)
        NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: false)
        
        WishareAPI.searchUser("9999", { (users : [User], error : Error?) in
            
            if error == nil {
                self.usersResult = users
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: false)
            }
            
        })
        
        
        WishareAPI.searchProduct("10", { (products : [Product], error : Error?) in
            
            if error == nil {
                self.productsResult = products
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: false)
            }
            
        })
        
        WishareAPI.searchStore("9999", { (stores : [Store], error : Error?) in
            
            if error == nil {
                self.storesResult = stores
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: false)
            }
            
        })
        
        WishareAPI.searchWips { (users : [User], error : Error?) in
            
            if error == nil {
                self.wipsResult = users
                // -
            }
            
        }
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        UserDefaults.standard.removeObject(forKey: "previewOption")
        
        if searchText == "" {
            
            self.searchView.isHidden = true
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: true)
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: true)
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: true)
            NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: false)
            
            if let decodedUsers : Data =  UserDefaults.standard.object(forKey: "initialUsers") as? Data {
                self.usersResult =  NSKeyedUnarchiver.unarchiveObject(with: decodedUsers) as! [User]
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabUser"), object: false)
            }
            
            let decodedProducts =  UserDefaults.standard.object(forKey: "initialProducts") as! Data
            self.productsResult =  NSKeyedUnarchiver.unarchiveObject(with: decodedProducts) as! [Product]
            NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabProduct"), object: false)
            
            if let decodedStores : Data =  UserDefaults.standard.object(forKey: "initialStores") as? Data {
                self.storesResult =  NSKeyedUnarchiver.unarchiveObject(with: decodedStores) as! [Store]
                NotificationCenter.default.post(name: NSNotification.Name.init("loadingTabStore"), object: false)
            }
            
            
            WishareAPI.searchWips { (users : [User], error : Error?) in
                if error == nil {
                    self.wipsResult = users
                }
            }
            
            
        } else if searchText.characters.count >= 3 && self.auto_complete == false {
            
            WishareAPI.searchComplete(searchText, { (products : [String], stores : [String], users : [String], wips : [String], error : Error?) in
                
                if error == nil {
                    
                    self.items_search[0] = products
                    self.items_search[1] = users
                    self.items_search[2] = stores
                    self.items_search[3] = wips
                    
                    
                    if self.items_search[self.tab_selected].count == 0 {
                        self.searchView.isHidden = true
                    } else {
                        self.searchView.isHidden = false
                    }
                    
                    self.searchTableView.reloadData()
                    
                }
                
            })
            
        }
        
        self.auto_complete = false
        
    }
    
    //QANDO ELE GANHAR O FOCO APOS TER ENTRADO NO CANCELAR, ELE DEVE VOLTAR PARA PAGINA QUE ESTAVA
    
}

extension CategoriesRecommendationViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.items_search[self.tab_selected][indexPath.row])
        
        self.resultSearchController.searchBar.text = self.items_search[self.tab_selected][indexPath.row]
        self.becomeFirstResponder()
        self.view.endEditing(true)
        self.searchView.isHidden = true
        self.auto_complete = true
        
        WishareAPI.searchTabs(self.items_search[self.tab_selected][indexPath.row], { (products : [Product], stores : [Store], users : [User], wips : [User], error : Error?) in
            
            if error == nil {
                
                // ATUALIZAR RESULTS CONTROLLER
                self.productsResult = products
                self.usersResult = users
                self.storesResult = stores
                self.wipsResult = wips
                
                // - AQUI
                self.defaults.set(self.items_search[self.tab_selected][indexPath.row], forKey: "word_search")
                NotificationCenter.default.post(name: Notification.Name.init("searchUpdate"), object: true)
                UserDefaults.standard.set(true, forKey: "isSearchTabs")
            }
            
        })
        
    }
    
}

extension CategoriesRecommendationViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items_search[self.tab_selected].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = self.items_search[self.tab_selected][indexPath.row]
        
        return cell
        
    }
    
}
