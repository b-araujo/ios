//
//  LibraryLocalViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/6/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class LibraryLocalViewController: UIViewController {

    @IBOutlet weak var zeroImageView : UIImageView!
    @IBOutlet weak var oneImageView : UIImageView!
    @IBOutlet weak var twoImageView : UIImageView!
    @IBOutlet weak var threeImageView : UIImageView!
    @IBOutlet weak var fourImageView : UIImageView!
    @IBOutlet weak var fiveImageView : UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizerZeroImageView = UITapGestureRecognizer(target: self, action: #selector(pressImageViewZero))
        let tapGestureRecognizerOneImageView = UITapGestureRecognizer(target: self, action: #selector(pressImageViewOne))
        let tapGestureRecognizerTwoImageView = UITapGestureRecognizer(target: self, action: #selector(pressImageViewTwo))
        let tapGestureRecognizerThreeImageView = UITapGestureRecognizer(target: self, action: #selector(pressImageViewThree))
        let tapGestureRecognizerFourImageView = UITapGestureRecognizer(target: self, action: #selector(pressImageViewFour))
        let tapGestureRecognizerFiveImageView = UITapGestureRecognizer(target: self, action: #selector(pressImageViewFive))
        
        
        self.zeroImageView.addGestureRecognizer(tapGestureRecognizerZeroImageView)
        self.oneImageView.addGestureRecognizer(tapGestureRecognizerOneImageView)
        self.twoImageView.addGestureRecognizer(tapGestureRecognizerTwoImageView)
        self.threeImageView.addGestureRecognizer(tapGestureRecognizerThreeImageView)
        self.fourImageView.addGestureRecognizer(tapGestureRecognizerFourImageView)
        self.fiveImageView.addGestureRecognizer(tapGestureRecognizerFiveImageView)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func pressImageViewZero() {
        self.segueNextPreview("0")
    }
    
    @objc func pressImageViewOne() {
        self.segueNextPreview("1")
    }

    @objc func pressImageViewTwo() {
        self.segueNextPreview("2")
    }
    
    @objc func pressImageViewThree() {
        self.segueNextPreview("3")
    }
    
    @objc func pressImageViewFour() {
        self.segueNextPreview("4")
    }
    
    @objc func pressImageViewFive() {
        self.segueNextPreview("5")
    }
    
    func segueNextPreview ( _ local : String) {
        
        let storyboard = UIStoryboard(name: "Setting", bundle: nil)
        let previewBackgroundUserViewController = storyboard.instantiateViewController(withIdentifier: "previewBackgroundUserID") as! PreviewBackgroundUserViewController
        previewBackgroundUserViewController.image = UIImage(named: local)
        previewBackgroundUserViewController.origin = 1
        previewBackgroundUserViewController.content = local
        
        self.navigationController?.pushViewController(previewBackgroundUserViewController, animated: true)
        
    }
    
}
