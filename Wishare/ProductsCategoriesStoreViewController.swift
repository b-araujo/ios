//
//  ProductsCategoriesStoreViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes
import RappleProgressHUD
import ENMBadgedBarButtonItem

class ProductsCategoriesStoreViewController: UIViewController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var filtersView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var notFoundLabel: UILabel!
    
    var store : Store!
    var category : Category!
    var promo : Promo?
    
    var origin : Bool = false
    var searchWord : String?
    
    var productResult = [Product]()
    var productResultOriginal = [Product]()
    
    var loader : Loader!
    var manager : Manager!
    
    var isLoading : Bool = false
    var isPagination : Bool = true
    let footerViewReuseIdentifier = "footerViewReuseIdentifier"
    var footerView : FooterCollectionReusableView?
    
    let filterButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black, for: .normal)
        button.set(image: UIImage(named: "filter"), title: NSString(string: "Filtrar"), titlePosition: UIViewContentMode.right, additionalSpacing: 10, state: .normal)
        return button
    }()
    
    let orderButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black, for: .normal)
        button.set(image: UIImage(named: "order"), title: NSString(string: "Ordenar"), titlePosition: UIViewContentMode.right, additionalSpacing: 10, state: .normal)
        return button
    }()
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)

    var barButton : BadgedBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageShoppingCart = UIImage(named: "Cart")
        let buttonFrame = CGRect(x: 0.0, y: 0.0, width: imageShoppingCart!.size.width, height: imageShoppingCart!.size.height)
        self.barButton = BadgedBarButtonItem(startingBadgeValue: 0, frame: buttonFrame, image: imageShoppingCart)
        self.barButton.badgeProperties.verticalPadding = 0.7
        self.barButton.badgeProperties.horizontalPadding = 0.7
        self.barButton.badgeProperties.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.2579999864, blue: 0.2160000056, alpha: 1)
        self.barButton.badgeProperties.textColor = UIColor.white
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        
        UserDefaults.standard.set(self.category.id!, forKey: "categoryStoreId")
        UserDefaults.standard.set(self.store.id!, forKey: "storeStoreId")
        UserDefaults.standard.set((self.category.id! == "x") ? true : false, forKey: "featuredStore")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "BuyShareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "buyShareCell")
        self.collectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerViewReuseIdentifier")

        NotificationCenter.default.addObserver(self, selector: #selector(updateProductResult(notification:)), name: Notification.Name.init("productsResultUpdatedCategory"), object: nil)

        if let promo = self.promo {
            UserDefaults.standard.set("\(promo.id!)", forKey: "promoCampanha")
        } else {
            UserDefaults.standard.removeObject(forKey: "promoCampanha")
        }
        
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        if self.origin {
            
            if let word = self.searchWord {
                
                self.searchBar.text = word
                UserDefaults.standard.set(word, forKey: "word_search_category")
                
                if var lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
                    
                    lastPreviewOption["text"] = word
                    lastPreviewOption["category"] = ((self.category.id! == "x") ? [] : ["\(self.category.id!)"])
                    UserDefaults.standard.set(lastPreviewOption, forKey: "previewOptionCategory")
                    
                }
                
                
                WishareAPI.searchProductFilterByStore(word, ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1") { ( categories : [String], sizes : [String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                    
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                            self.filtersView.isHidden = true
                        } else {
                            self.notFoundLabel.isHidden = true
                            self.filtersView.isHidden = false
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    
                    }
                    
                }
                
            }
            
        } else {
            
            WishareAPI.searchProductFilterByStore("", ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1") { ( categories : [String], sizes : [String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                
                if error == nil {
                    
                    let previewOption : [String : Any] = ["text": "", "category": ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), "size": [], "color": [], "brand": [], "store": ["\(self.store.id!)"], "orderBy": "asc", "type": "brand", "page" : "1", "featured" : ((self.category.id! == "x") ? true : false)]
                    UserDefaults.standard.set(previewOption, forKey: "previewOptionCategory")
                    
                    self.productResult = products
                    
                    if self.productResult.count == 0 {
                        self.notFoundLabel.isHidden = false
                        self.filtersView.isHidden = true
                    } else {
                        self.notFoundLabel.isHidden = true
                        self.filtersView.isHidden = false
                    }
                    
                    self.collectionView.reloadData()
                    RappleActivityIndicatorView.stopAnimation()
                }
                
            }
            
        }
        
        
        

    
        
        orderButton.addTarget(self, action: #selector(pressOrderBy), for: .touchUpInside)
        filterButton.addTarget(self, action: #selector(pressFilter), for: .touchUpInside)
    
        self.filtersView.addSubview(filterButton)
        self.filtersView.addSubview(orderButton)
        
        self.filterButton.topAnchor.constraint(equalTo: self.filtersView.topAnchor).isActive = true
        self.filterButton.leftAnchor.constraint(equalTo: self.filtersView.leftAnchor).isActive = true
        self.filterButton.bottomAnchor.constraint(equalTo: self.filtersView.bottomAnchor).isActive = true
        self.filterButton.widthAnchor.constraint(equalToConstant: self.view.bounds.width / 2).isActive = true
        
        self.orderButton.topAnchor.constraint(equalTo: self.filtersView.topAnchor).isActive = true
        self.orderButton.rightAnchor.constraint(equalTo: self.filtersView.rightAnchor).isActive = true
        self.orderButton.bottomAnchor.constraint(equalTo: self.filtersView.bottomAnchor).isActive = true
        self.orderButton.widthAnchor.constraint(equalToConstant: self.view.bounds.width / 2).isActive = true
        
        self.navigationItem.title = self.category.name
        
        self.searchBar.delegate = self
    
        UserDefaults.standard.set("", forKey: "categoryMother")

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        WishareCoreData.showShoppingCart { (products : [WS_ShoppingCart], stores : [WS_Store], error : Error?) in
            if error == nil {
                self.barButton.badgeValue = stores.count
            }
        }
        
        
        self.barButton.addTarget(self, action: #selector(myCart))
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func myCart() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cartPurchasesViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartID") as! CartPurchasesViewController
        self.navigationController?.pushViewController(cartPurchasesViewController, animated: true)
        
    }
    
    @objc func pressOrderBy() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        let menorPriceAction = UIAlertAction(title: "Menor Preço", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Menor Preço...", attributes: self.attributes)
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
                
                
                WishareAPI.searchProductFilterByStore(lastPreviewOption["text"] as! String, ((self.category.id! == "p" ) ? [] : lastPreviewOption["category"] as! [String]), lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], ["\(self.store.id!)"], "asc", "price", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search_category") as? String {
                
                WishareAPI.searchProductFilterByStore(word_search, (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "price", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
            } else {
                
                WishareAPI.searchProductFilterByStore("", (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "price", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
            }
            
        }
        
        let maiorPriceAction = UIAlertAction(title: "Maior Preço", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Maior Preço...", attributes: self.attributes)
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
                
                
                WishareAPI.searchProductFilterByStore(lastPreviewOption["text"] as! String, ((self.category.id! == "p") ? [] : lastPreviewOption["category"] as! [String]), lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], ["\(self.store.id!)"], "desc", "price", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search_category") as? String {
                
                WishareAPI.searchProductFilterByStore(word_search, (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "desc", "price", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
            } else {
                
                WishareAPI.searchProductFilterByStore("", (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "desc", "price", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
            }

            
        }
        
        let marcaAZAction = UIAlertAction(title: "Marca A-Z", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Marca A-Z...", attributes: self.attributes)
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
                
                
                WishareAPI.searchProductFilterByStore(lastPreviewOption["text"] as! String, ((self.category.id! == "p") ? [] : lastPreviewOption["category"] as! [String]), lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search_category") as? String {
                
                WishareAPI.searchProductFilterByStore(word_search, (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
            } else {
                
                WishareAPI.searchProductFilterByStore("", (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
            }
            
            
        }
        
        let marcaZAAction = UIAlertAction(title: "Marca Z-A", style: .default) { (alert : UIAlertAction) in
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Marca Z-A...", attributes: self.attributes)
            
            
            
            if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
                
                
                WishareAPI.searchProductFilterByStore(lastPreviewOption["text"] as! String, ((self.category.id! == "p") ? [] : lastPreviewOption["category"] as! [String]), lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], ["\(self.store.id!)"], "desc", "brand", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
                
            } else if let word_search : String = UserDefaults.standard.object(forKey: "word_search_category") as? String {
                
                WishareAPI.searchProductFilterByStore(word_search, (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "desc", "brand", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
                
            } else {
                
                WishareAPI.searchProductFilterByStore("", (((self.category.id! == "p") || (self.category.name! == "Destaques" && self.category.id! == "x" )) ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "desc", "brand", ((self.category.id! == "x") ? true : false), "1", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                    
                    if error == nil {
                        
                        self.productResult = products
                        
                        if self.productResult.count == 0 {
                            self.notFoundLabel.isHidden = false
                        } else {
                            self.notFoundLabel.isHidden = true
                        }
                        
                        self.collectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    
                })
                
            }
        
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(menorPriceAction)
        alertController.addAction(maiorPriceAction)
        alertController.addAction(marcaAZAction)
        alertController.addAction(marcaZAAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func pressFilter() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "navFilterSearchCategoryID") as! UINavigationController
        self.present(navigationController, animated:true, completion: nil)
    }

    @objc func updateProductResult(notification: Notification) {
        self.productResult = notification.object as! [Product]
        self.collectionView.reloadData()
    }
    
}


extension ProductsCategoriesStoreViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.productResult.count > 0 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: "productDetailID") as! ProductDetailViewController
            productDetailViewController.idProduct = self.productResult[indexPath.row].id!
            productDetailViewController.isFirst = true
            
            self.navigationController?.pushViewController(productDetailViewController, animated: true)
            
        }
        
    }

}

extension ProductsCategoriesStoreViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buyShareCell", for: indexPath) as! BuyShareCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        
        let url = urlBase.appendingPathComponent("files/products/\(self.productResult[indexPath.row].pId!)/thumb/\(self.productResult[indexPath.row].picture!)")
        
        self.manager.loadImage(with: url, into: cell.productImageView) { ( result, _ ) in
            cell.productImageView.image = result.value
        }
        
        cell.nameProductLabel.text = self.productResult[indexPath.row].brand
        cell.descriptionProductLabel.text = self.productResult[indexPath.row].name
        
        if self.productResult[indexPath.row].promo_price == "" {
            
            let price = "R$ \(self.productResult[indexPath.row].price!)".withFont(.boldSystemFont(ofSize: 14))
            
            cell.priceProductLabel.attributedText = price
            cell.promoPriceProductLabel.text = nil
            
        } else {
            
            let promo_price = "R$ \(self.productResult[indexPath.row].price!)".withStrikethroughStyle(.styleSingle).withBaselineOffset(0).withFont(.systemFont(ofSize: 14)).withTextColor(UIColor.lightGray)
            let price = "R$ \(self.productResult[indexPath.row].promo_price!)".withFont(.boldSystemFont(ofSize: 15)).withTextColor(UIColor.black)
            let por = " por ".withTextColor(UIColor.lightGray).withFont(.systemFont(ofSize: 14))
            
            cell.priceProductLabel.attributedText = promo_price + por
            cell.promoPriceProductLabel.attributedText = price
            
        }
        
        return cell
        
    }
    
}

extension ProductsCategoriesStoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 2) - 2.5, height: ((self.view.bounds.width / 2) - 5) + 115)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if self.isLoading {
            return CGSize.zero
        }
        
        return CGSize(width: collectionView.bounds.size.width, height: 55.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter {
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! FooterCollectionReusableView
            self.footerView = footerView
            self.footerView?.backgroundColor = UIColor.clear
            return footerView
            
        } else {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold = 100.0
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold = Float((diffHeight - frameHeight))/Float(threshold);
        
        triggerThreshold = min(triggerThreshold, 0.0)
        let pullRatio = min(fabs(triggerThreshold),0.4);
        
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        
        if pullRatio >= 0.4 {
            self.footerView?.animateFinal()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);

        
        if pullHeight >= 0.0  && pullHeight <= 1.0 {
            if (self.footerView?.isAnimatingFinal)! {
                
                self.isLoading = true
                self.footerView?.startAnimate()
                
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                    
                    
                    if self.isPagination {
                        
                        if var lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
                            
                            print("REFRESH APPENDING PARAMETROS ....")
                            
                            let orderBy = UserDefaults.standard.object(forKey: "orderByCategories") as! String
                            let typeBy = UserDefaults.standard.object(forKey: "typeByCategories") as! String
                            let page = UserDefaults.standard.object(forKey: "pageProductsFiltersCategory") as! String
                            let pageInt = Int(page)! + 1
                            UserDefaults.standard.set("\(pageInt)", forKey: "pageProductsFilters")
                        
                            WishareAPI.searchProductFilterByStore(self.searchBar.text!, lastPreviewOption["category"] as! [String], lastPreviewOption["size"] as! [String], lastPreviewOption["color"] as! [String], lastPreviewOption["brand"] as! [String], ["\(self.store.id!)"], typeBy, orderBy, lastPreviewOption["featured"] as! Bool, "\(pageInt)", { (_, _, _, _, products : [Product], _, _, _, _, _, error : Error?) in
                                
                                if error == nil {
                                    UserDefaults.standard.set(self.searchBar.text!, forKey: "word_search_category")
                                    self.productResult.append(contentsOf: products)
                                    self.collectionView.reloadData()
                                    self.isLoading = false
                                }

                            })
                            
                            
                        } else if let orderBy : String = UserDefaults.standard.object(forKey: "orderByCategories") as? String, let typeBy : String = UserDefaults.standard.object(forKey: "typeByCategories") as? String {
                            
                            if let page : String = UserDefaults.standard.object(forKey: "pageProductsFiltersCategory") as? String {
                                
                                print("REFRESH APPENDING ORDERBY ....")
                                
                                let pageInt = Int(page)! + 1
                                UserDefaults.standard.set("\(pageInt)", forKey: "pageProductsFiltersCategory")
                                
                                
                                WishareAPI.searchProductFilterByStore(self.searchBar.text!, ((self.category.id! == "x" || self.category.id! == "p") ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], typeBy, orderBy, ((self.category.id! == "x") ? true : false), "\(pageInt)", { (categories : [String], sizes :[String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                                    
                                    if error == nil {
                                        UserDefaults.standard.set(self.searchBar.text!, forKey: "word_search_category")
                                        self.productResult.append(contentsOf: products)
                                        self.collectionView.reloadData()
                                        self.isLoading = false
                                        RappleActivityIndicatorView.stopAnimation()
                                    }
                                    
                                })
                                
                                
                            } else {
                                self.collectionView.reloadData()
                                self.isLoading = false
                            }
                        }
                    }
                    
                })
                
            }
        }
    }
    
}

extension ProductsCategoriesStoreViewController : UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        if self.origin {
            self.navigationController?.popViewController(animated: true)
            return false
        } else {
            return true
        }
        
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let text = searchBar.text {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Buscando...", attributes: attributes)
            
            WishareAPI.searchProductFilterByStore(text, ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1") { ( categories : [String], sizes : [String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                
                if error == nil {
                    UserDefaults.standard.set(text, forKey: "word_search_category")
                    let previewOption : [String : Any] = ["text": text, "category": ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), "size": [], "color": [], "brand": [], "store": ["\(self.store.id!)"], "orderBy": "asc", "type": "brand", "page" : "1", "featured" : ((self.category.id! == "x") ? true : false)]
                    UserDefaults.standard.set(previewOption, forKey: "previewOptionCategory")
                    self.productResult = products
                    self.collectionView.reloadData()
                    RappleActivityIndicatorView.stopAnimation()
                }
                
            }
            
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
            
            WishareAPI.searchProductFilterByStore("", ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), [], [], [], ["\(self.store.id!)"], "asc", "brand", ((self.category.id! == "x") ? true : false), "1") { ( categories : [String], sizes : [String], colors : [String], brands : [String], products : [Product], tot : Int, categoriesName : [Category], _, _, _, error : Error?) in
                
                if error == nil {
                    UserDefaults.standard.removeObject(forKey: "word_search_category")
                    let previewOption : [String : Any] = ["text": "", "category": ((self.category.id! == "x") ? [] : ["\(self.category.id!)"]), "size": [], "color": [], "brand": [], "store": ["\(self.store.id!)"], "orderBy": "asc", "type": "brand", "page" : "1", "featured" : ((self.category.id! == "x") ? true : false)]
                    UserDefaults.standard.set(previewOption, forKey: "previewOptionCategory")
                    self.productResult = products
                    self.collectionView.reloadData()
                    RappleActivityIndicatorView.stopAnimation()
                }
                
            }
        
        }
        
        
    }
    
}
