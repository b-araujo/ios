//
//  PageUserCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 7/4/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PageUserCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var crowImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var wishpointsImageView: UIImageView!
    @IBOutlet weak var socialStackView: UIStackView!
    
    @IBOutlet weak var oneImageView: UIImageView!
    @IBOutlet weak var twoImageView: UIImageView!
    @IBOutlet weak var threeImageView: UIImageView!
    
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var wipsLabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    
    @IBOutlet weak var postsLabel: UILabel!
    @IBOutlet weak var wishesLabel: UILabel!
    @IBOutlet weak var purchasesLabel: UILabel!
    
    @IBOutlet weak var descriptionPostLabel: UILabel!
    @IBOutlet weak var descriptionWishesLabel: UILabel!
    @IBOutlet weak var descriptionPurchaseLabel: UILabel!
    
    @IBOutlet weak var editOrFollowButton: UIButton!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var menuListButton: UIButton!
    @IBOutlet weak var menuBlockButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messagePrivateAccountView: UIView!
    
    @IBOutlet weak var messageNotFoundPurchaseView: UIView!
    @IBOutlet weak var messageNotFoundPurchaseLabel: UILabel!
    @IBOutlet weak var userCloneImageView: UIImageView!
    
    var delegate : PageUserDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.editOrFollowButton.backgroundColor = UIColor.black
        self.editOrFollowButton.setTitleColor(UIColor.white, for: .normal)
        self.editOrFollowButton.layer.cornerRadius = 5.0
        self.editOrFollowButton.layer.borderWidth = 0.0
        self.editOrFollowButton.layer.borderColor = UIColor.clear.cgColor
        
        let tapGestureRecognizerFollowers = UITapGestureRecognizer(target: self, action: #selector(followers))
        let tapGestureRecognizerFollowing = UITapGestureRecognizer(target: self, action: #selector(following))
        let tapGestureRecognizerWips = UITapGestureRecognizer(target: self, action: #selector(wips))
        let tapGestureRecognizerStore = UITapGestureRecognizer(target: self, action: #selector(store))
        
        self.followersLabel.addGestureRecognizer(tapGestureRecognizerFollowers)
        self.followingLabel.addGestureRecognizer(tapGestureRecognizerFollowing)
        self.wipsLabel.addGestureRecognizer(tapGestureRecognizerWips)
        self.storeLabel.addGestureRecognizer(tapGestureRecognizerStore)
        
        let tapGestureRecognizerPosts = UITapGestureRecognizer(target: self, action: #selector(changeType))
        let tapGestureRecognizerWishes = UITapGestureRecognizer(target: self, action: #selector(changeType))
        let tapGestureRecognizerPurchase = UITapGestureRecognizer(target: self, action: #selector(changeType))
        tapGestureRecognizerPosts.accessibilityHint = "0"
        tapGestureRecognizerWishes.accessibilityHint = "1"
        tapGestureRecognizerPurchase.accessibilityHint = "2"
        
        self.postsLabel.addGestureRecognizer(tapGestureRecognizerPosts)
        self.wishesLabel.addGestureRecognizer(tapGestureRecognizerWishes)
        self.purchasesLabel.addGestureRecognizer(tapGestureRecognizerPurchase)
        
        let tapGestureRecognizerUserImageView = UITapGestureRecognizer(target: self, action: #selector(tapUserImageView))
        self.userImageView.addGestureRecognizer(tapGestureRecognizerUserImageView)
        self.userImageView.isUserInteractionEnabled = true
        
        
    }
    
    @IBAction func editFollow(_ sender: UIButton) {
        self.delegate?.editOrFollow(sender, cell: self)
    }
    
    @IBAction func changeMode( _ sender : UIButton) {
        self.delegate?.changeModeOption(sender)
    }
    
    @objc func changeType( _ sender : UITapGestureRecognizer) {
       self.delegate?.changeTypeOption(Int(sender.accessibilityHint!)!)
    }
    
    @objc func followers() {
        self.delegate?.openFollowers()
    }
    
    @objc func following() {
        self.delegate?.openFollowing()
    }
    
    @objc func wips() {
        self.delegate?.openWips()
    }
    
    @objc func store() {
        self.delegate?.openStore()
    }
    
    @objc func tapUserImageView () {
        self.delegate?.showImagePictureUser()
    }

}

protocol PageUserDelegate {
    func editOrFollow ( _ sender : UIButton, cell : PageUserCollectionViewCell)
    func openFollowers()
    func openFollowing()
    func openWips()
    func openStore()
    func changeModeOption( _ sender : UIButton)
    func changeTypeOption( _ type : Int)
    func showImagePictureUser()
}
