//
//  FollowingStorePageUserViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 7/20/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import RappleProgressHUD

class FollowingStorePageUserViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var idUser : String!
    var stores : [Store] = []
    var loader : Loader!
    var manager : Manager!
    
    let attributes : [String : Any] = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "UserSearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userSearchCell")
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.getStoresUser(idUser: self.idUser) { (stores : [Store], error : Error?) in
            if error == nil {
                self.stores = stores
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
                
                if self.stores.count == 0 {
                    self.messageLabel.isHidden = false
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension FollowingStorePageUserViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageStoreViewController = storyboard.instantiateViewController(withIdentifier: "pageStoreID") as! PageStoreViewController
        pageStoreViewController.idStore = self.stores[indexPath.row].id!
        
        self.navigationController?.pushViewController(pageStoreViewController, animated: true)
        
    }
    
}

extension FollowingStorePageUserViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.stores.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userSearchCell", for: indexPath) as! UserSearchCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/store-logo/thumb/")!
        let url = urlBase.appendingPathComponent("files/store-logo/thumb/\(self.stores[indexPath.row].picture!)")
        
        cell.userLabel.text = self.stores[indexPath.row].name!
        
        self.manager.loadImage(with: url, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        return cell
        
    }
    
}

extension FollowingStorePageUserViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 1.25, height: ((self.view.bounds.width / 3) - 2.5))
    }
    
}
