//
//  RatingTableViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/27/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import FloatRatingView

class RatingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleCommentRatingLabel: UILabel!
    @IBOutlet weak var dateCommentRatingLabel: UILabel!
    @IBOutlet weak var ratingCommentView: FloatRatingView!
    @IBOutlet weak var byCommentRatingLabel: UILabel!
    @IBOutlet weak var commentRatingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
