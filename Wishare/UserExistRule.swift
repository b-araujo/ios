//
//  UserExistRule.swift
//  Wishare
//
//  Created by Wishare iMac on 4/17/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import SwiftValidator

public class UserExistRule : Rule {
    
    private var message : String
    
    public init(_ message : String = "Usuário existente") {
        self.message = message
    }
    
    
    public func validate(_ value: String) -> Bool {
        
        if WishareAPI.checkUserNameExists(value) {
            return true
        }
        
//        WishareAPI.checkedFieldsExistsUser("username", value) { (result : Bool, error : Error?) in
//            
//            if error == nil {
//                
//                if result {
//                    self.isSuccessOrFail = false
//                    let _ = self.validate(value)
//                }
//                
//                
//                self.isSuccessOrFail = true
//                let _ = self.validate(value)
//            }
//            
//        }
        
        return false
    
    }
    
    
    public func errorMessage() -> String {
        return self.message
    }
    
}
