//
//  TabBarViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/3/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Fusuma

class TabBarViewController: UITabBarController {
    
    var index = 0
    var item : UITabBarItem?
    var isPreviewSelected : Int = 0
    
    var firstClick : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "photoSegue" {
            
            let photoViewController = segue.destination as! PhotoViewController
            photoViewController.photoUser = sender as? UIImage
            
        }
        
    }

}

extension TabBarViewController : UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if item == (self.tabBar.items!)[0] {
        
            self.index = 0
            self.item = item
            self.isPreviewSelected = 0
            
        } else if item == (self.tabBar.items!)[1] {
            
            self.index = 1
            self.item = item
            self.isPreviewSelected += 1
            
        } else if item == (self.tabBar.items!)[2] {
            
            let fusumaController = FusumaViewController()
            fusumaController.delegate = self
            //fusumaController.modeOrder = .cameraFirst
            fusumaTintColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
            self.present(fusumaController, animated: true, completion: nil)
            self.item = item
            self.isPreviewSelected = 0
            
        } else if item == (self.tabBar.items!)[3] {
            
            self.index = 3
            self.item = item
            self.isPreviewSelected = 0
            
        } else if item == (self.tabBar.items!)[4] {
            
            self.index = 4
            self.item = item
            self.isPreviewSelected = 0
            
        }
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController == tabBarController.viewControllers![2] {
            self.selectedIndex = index
            return false
        } else {
            if self.item == (tabBarController.tabBar.items!)[1] {
                if self.isPreviewSelected > 1 {
                    return false
                }
            }

            return true
        }
    
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let tabBarIndex = tabBarController.selectedIndex
//        print(tabBarIndex)
        if firstClick == tabBarIndex {
            if tabBarIndex == 0 && viewController is UINavigationController {
                if let feed = (viewController as! UINavigationController).viewControllers[0] as? FeedViewController {
                    if feed.feedCollectionView.numberOfItems(inSection: 0) > 0 {
                        feed.feedCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition(rawValue: 0), animated: true)
                    }
                }
            }
        }
        else {
            firstClick = tabBarIndex
        }
    }
    
}


extension TabBarViewController : FusumaDelegate {
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
    }
    
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight

        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "navPreviewID") as! UINavigationController
        
        if let viewcontroller = navigationController.viewControllers.first as? PreviewPhotoViewController {
            viewcontroller.imagePhoto = image
        }
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.set(rootViewController: navigationController, withTransition: transition)
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
     
    }
    
}


