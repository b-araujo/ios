//
//  PageStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 6/5/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class PageStoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var quantityPostsLabel: UILabel!
    @IBOutlet weak var quantityFollowersLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var postsView: UIView!

    var delegate : PageStoreViewControllerDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        let gestureRecognizerFollowView = UITapGestureRecognizer(target: self, action: #selector(pressFollow))
        self.followView.addGestureRecognizer(gestureRecognizerFollowView)
        
        let gestureRecognizerPostsView = UITapGestureRecognizer(target: self, action: #selector(pressPosts))
        self.postsView.addGestureRecognizer(gestureRecognizerPostsView)

    }
    
    @objc func pressFollow () {
        self.delegate.followPageStore()
    }
    
    @objc func pressPosts () {
        self.delegate.postsPageStore()
    }
    
    @IBAction func pressFollowButton(_ sender: UIButton) {
        self.delegate.followStore(sender)
    }

}


protocol PageStoreViewControllerDelegate {
    func followPageStore()
    func postsPageStore()
    func followStore( _ sender : UIButton)
}
