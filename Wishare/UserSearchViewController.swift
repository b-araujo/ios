//
//  UserSearchViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 5/23/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import Nuke
import NukeAlamofirePlugin
import SwiftyAttributes
import RappleProgressHUD

class UserSearchViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var resultNotFoundLabel: UILabel!

    var loader : Loader!
    var manager : Manager!
    var usersResult = [User]()
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = Nuke.Loader(loader: NukeAlamofirePlugin.DataLoader())
        manager = Nuke.Manager(loader: loader, cache: Cache.shared)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "UserSearchCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "userSearchCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserResult(notification:)), name: Notification.Name.init("usersResultUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loading(notification:)), name: Notification.Name.init("loadingTabUser"), object: nil)
        
        let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        WishareAPI.searchUser("9999") { (users : [User], error : Error?) in
            if error == nil {
                self.usersResult = users
                let encodedUsers = NSKeyedArchiver.archivedData(withRootObject: users)
                UserDefaults.standard.set(encodedUsers, forKey: "initialUsers")
                self.collectionView.reloadData()
                RappleActivityIndicatorView.stopAnimation()
            }
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        print("Users WillApper")
        NotificationCenter.default.post(name: NSNotification.Name.init("changeTabs"), object: 1)
    }
    
    @objc func updateUserResult(notification: Notification) {
        self.usersResult = notification.object as! [User]
        print(self.usersResult.count)
        if self.usersResult.count == 0 {
            self.collectionView.isHidden = true
            self.resultNotFoundLabel.isHidden = false
        } else {
            self.collectionView.isHidden = false
            self.resultNotFoundLabel.isHidden = true
        }
        self.collectionView.reloadData()
    }
    
    @objc func loading(notification: Notification) {
        
        let isLoading : Bool = notification.object as! Bool
        
        if isLoading {
            self.collectionView.isHidden = true
            self.resultNotFoundLabel.isHidden = true
            self.loadingActivityIndicator.startAnimating()
        } else {
            self.loadingActivityIndicator.stopAnimating()
            self.collectionView.isHidden = false
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.becomeFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init("usersResultUpdated"), object: nil)
    }
    
}

extension UserSearchViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name.init("openPageUser"), object: self.usersResult[indexPath.row].id!)
    }
    
}

extension UserSearchViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.usersResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userSearchCell", for: indexPath) as! UserSearchCollectionViewCell
        
        //let urlBase = URL(string: "http://www.wishare.com.br/code/files/")!
        let url = urlBase.appendingPathComponent("files/photo-profile/user/thumb/\(self.usersResult[indexPath.row].picture!)")
        
        cell.userLabel.text = self.usersResult[indexPath.row].username
        
        self.manager.loadImage(with: url, into: cell.userImageView) { ( result, _ ) in
            cell.userImageView.image = result.value?.circleMask
        }
        
        return cell
        
    }
    
}

extension UserSearchViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width / 3) - 1.25, height: ((self.view.bounds.width / 3) - 2.5))
    }
    
}
