//
//  LookStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/27/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class LookStoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var storeThumbImageView: UIImageView!
    @IBOutlet weak var nameStoreTopButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    
    @IBOutlet weak var storePostImageView: UIImageView!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeLookStoreImageView: UIImageView!
    @IBOutlet weak var commentLookStoreImageView: UIImageView!
    @IBOutlet weak var shareLookStoreImageView: UIImageView!
    @IBOutlet weak var wishListLookStoreImageView: UIImageView!
    
    
    @IBOutlet weak var viewLikes: UIView!
    
    @IBOutlet weak var nameStoreBottomButton: UIButton!
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var commentStoreLabel: ActiveLabel!
    @IBOutlet weak var commentsStoreLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    
    @IBOutlet weak var collectionViewCarouselLook: UICollectionView!
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    @IBOutlet weak var moreComment: UIButton!
    
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    
    var delegate : LookStoreCollectionViewCellDelegate!
    var indexPathNow : IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionViewCarouselLook.delegate = self
        self.collectionViewCarouselLook.dataSource = self
        self.collectionViewCarouselLook.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
        
        self.collectionViewCarouselLook.backgroundView = nil
        self.collectionViewCarouselLook.backgroundColor = UIColor.clear
        self.viewCollection.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        
        let tapStorePostGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPostStoreImageView))
        self.storePostImageView.addGestureRecognizer(tapStorePostGestureRecognizer)
        
        let tapGestureRecognizerStoreThumbImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreThumb))
        let tapGestureRecognizerLikeProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerCommentProductStoreLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerWishListProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapWishList))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapViewLikes))

        
        self.storeThumbImageView.addGestureRecognizer(tapGestureRecognizerStoreThumbImageView)
        self.likeLookStoreImageView.addGestureRecognizer(tapGestureRecognizerLikeProductStoreImageView)
        self.commentsStoreLabel.addGestureRecognizer(tapGestureRecognizerCommentProductStoreLabel)
        self.shareLookStoreImageView.addGestureRecognizer(tapGestureRecognizerShareProductStoreImageView)
        self.wishListLookStoreImageView.addGestureRecognizer(tapGestureRecognizerWishListProductStoreImageView)
        self.commentLookStoreImageView.addGestureRecognizer(tapGestureRecognizerCommentProductStoreImageView)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)

        
        let tapGestureRecognizerNameStoreTopButton = UITapGestureRecognizer(target: self, action: #selector(tapNameStoreTop))
        self.nameStoreTopButton.addGestureRecognizer(tapGestureRecognizerNameStoreTopButton)
        
        self.moreComment.addTarget(self, action: #selector(tapShowMoreComments(_:)), for: .touchUpInside)
    }
    
    @IBAction func buytLookStore(_ sender: UIButton) {
        self.delegate.buyLookStore(sender, indexPathNow)
    }
    
    
    @objc func tapPostStoreImageView () {
        self.delegate.buyLookStoreImage(indexPathNow)
    }
    
    @objc func tapStoreThumb() {
        self.delegate?.storeThumbLookStore(cell: self)
    }
    
    func tapBuy() {
        self.delegate?.buyLookStore(cell: self)
    }
    
    func tapPost() {
        self.delegate?.postLookStore(cell: self)
    }
    
    @objc func tapLike() {
        self.delegate?.likeLookStore(cell: self)
    }
    
    @objc func tapViewLikes() {
        self.delegate.viewLikesLookStore(cell: self, indexPath: indexPathNow)
    }
    
    @objc func tapComment() {
        self.delegate?.commentLookStore(cell: self)
    }
    
    @objc func tapShare() {
        self.delegate?.shareLookStore(cell: self, indexPath: indexPathNow)
    }
    
    @objc func tapWishList() {
        self.delegate?.wishlistLookStore(cell: self)
    }
    
    @objc func tapNameStoreTop() {
        self.delegate?.storeNameLookStore(cell: self)
    }
    
    @objc func tapShowMoreComments( _ sender : UIButton) {
        self.delegate?.showMoreCommentsLookStore(cell: self, indexPath: indexPathNow)
    }

}

extension LookStoreCollectionViewCell : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate.selectedItem(collectionView, indexPath, indexPathNow)
    }
    
}

extension LookStoreCollectionViewCell : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate.numberOfItemsInSectionLook(self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.delegate.cellForItemLook(collectionView, indexPath, self)
    }
    
}

extension LookStoreCollectionViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionViewCarouselLook.bounds.width / 3, height: self.collectionViewCarouselLook.bounds.height)
    }
    
}


protocol LookStoreCollectionViewCellDelegate {
    func selectedItem( _ collection : UICollectionView, _ indexPath : IndexPath, _ now : IndexPath)
    func cellForItemLook( _ collection : UICollectionView, _ indexPath : IndexPath, _ cellForLook : LookStoreCollectionViewCell) -> UICollectionViewCell
    func numberOfItemsInSectionLook( _ cellForLook : LookStoreCollectionViewCell) -> Int
    func buyLookStore( _ sender : UIButton, _ indexPath : IndexPath)
    func buyLookStoreImage ( _ indexPath : IndexPath)
   
    func storeThumbLookStore(cell: LookStoreCollectionViewCell)
    func storeNameLookStore(cell: LookStoreCollectionViewCell)
    func buyLookStore(cell: LookStoreCollectionViewCell)
    func postLookStore(cell: LookStoreCollectionViewCell)
    func likeLookStore(cell: LookStoreCollectionViewCell)
    func commentLookStore(cell: LookStoreCollectionViewCell)
    func viewLikesLookStore(cell: LookStoreCollectionViewCell, indexPath: IndexPath)
    func shareLookStore(cell: LookStoreCollectionViewCell, indexPath : IndexPath)
    func wishlistLookStore(cell: LookStoreCollectionViewCell)
    func showMoreCommentsLookStore(cell : LookStoreCollectionViewCell, indexPath : IndexPath)
    
}
