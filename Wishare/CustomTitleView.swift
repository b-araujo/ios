//
//  CustomTitleView.swift
//  Wishare
//
//  Created by Wishare on 12/03/18.
//  Copyright © 2018 Wishare iMac. All rights reserved.
//

import UIKit

class CustomTitleView: UIView {

    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }

}
