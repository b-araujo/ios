//
//  ProductStoreCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class ProductStoreCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var storeThumbImageView: UIImageView!
    @IBOutlet weak var nameStoreTopButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    
    @IBOutlet weak var storePostImageView: UIImageView!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeProductStoreImageView: UIImageView!
    @IBOutlet weak var commentProductStoreImageView: UIImageView!
    @IBOutlet weak var shareProductStoreImageView: UIImageView!
    @IBOutlet weak var wishListProductStoreImageView: UIImageView!
    
    @IBOutlet weak var viewLikes: UIView!
    @IBOutlet weak var likeEffectsImageView: UIImageView!
    
    @IBOutlet weak var nameStoreBottomButton: UIButton!
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var commentStoreLabel: ActiveLabel!
    @IBOutlet weak var commentsStoreLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var moreComment : UIButton!
    
    
    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    var indexPathNow : IndexPath!
    var delegate : ProductStoreCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        let tapGestureRecognizerStoreThumbImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreThumb))
        let tapGestureRecognizerStorePostImageView = UITapGestureRecognizer(target: self, action: #selector(tapPost))
        let tapGestureRecognizerLikeProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerCommentProductStoreLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerWishListProductStoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapWishList))
        
        self.storeThumbImageView.addGestureRecognizer(tapGestureRecognizerStoreThumbImageView)
        self.storePostImageView.addGestureRecognizer(tapGestureRecognizerStorePostImageView)
        self.likeProductStoreImageView.addGestureRecognizer(tapGestureRecognizerLikeProductStoreImageView)
        self.commentProductStoreImageView.addGestureRecognizer(tapGestureRecognizerCommentProductStoreImageView)
        self.shareProductStoreImageView.addGestureRecognizer(tapGestureRecognizerShareProductStoreImageView)
        self.wishListProductStoreImageView.addGestureRecognizer(tapGestureRecognizerWishListProductStoreImageView)
        self.commentsStoreLabel.addGestureRecognizer(tapGestureRecognizerCommentProductStoreLabel)
    
        self.buyButton.addTarget(self, action: #selector(tapBuy), for: .touchUpInside)
        self.nameStoreTopButton.addTarget(self, action: #selector(tapStoreThumb), for: .touchUpInside)
        
        self.moreComment.addTarget(self, action: #selector(tapShowMoreComments(_:)), for: .touchUpInside)
        
    }
    
    @objc func tapStoreThumb() {
        self.delegate?.storeThumbProductStore(cell: self)
    }
    
    @objc func tapBuy() {
        self.delegate?.buyProductStore(cell: self)
    }
    
    @objc func tapPost() {
        self.delegate?.postProductStore(cell: self)
    }
    
    @objc func tapLike() {
        self.delegate?.likeProductStore(cell: self)
    }
    
    @objc func tapComment() {
        self.delegate?.commentProductStore(cell: self)
    }
    
    @objc func tapShare() {
        self.delegate?.shareProductStore(cell: self, indexPath: indexPathNow)
    }

    @objc func tapWishList() {
        self.delegate?.wishListProductStore(cell: self)
    }

    @objc func tapShowMoreComments( _ sender : UIButton) {
        self.delegate?.showMoreCommentsProductStore(cell: self, indexPath: indexPathNow)
    }
}




protocol ProductStoreCollectionViewCellDelegate {
    
    func storeThumbProductStore(cell : ProductStoreCollectionViewCell)
    func nameStoreProductStore(cell : ProductStoreCollectionViewCell)
    func buyProductStore(cell : ProductStoreCollectionViewCell)
    func postProductStore(cell : ProductStoreCollectionViewCell)
    func likeProductStore(cell : ProductStoreCollectionViewCell)
    func commentProductStore(cell : ProductStoreCollectionViewCell)
    func shareProductStore(cell : ProductStoreCollectionViewCell, indexPath : IndexPath)
    func wishListProductStore(cell : ProductStoreCollectionViewCell)
    func showMoreCommentsProductStore(cell : ProductStoreCollectionViewCell, indexPath : IndexPath)
    
    
}


