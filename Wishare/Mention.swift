//
//  Mention.swift
//  Wishare
//
//  Created by Wishare iMac on 6/1/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Mention {
    
    var id : String!
    var username : String!
    var picture : String!
    var objectMentin : String!
    
    init ( username user : [String : String]) {
        
        self.id = user["id"]
        self.username = user["username"]
        self.picture = user["picture"]
        self.objectMentin = "[@id:\(self.id!),@username:\(self.username!)]"
        
    }

    
    init ( name store : [String : String]) {
        
        self.id = store["id"]
        self.username = store["name"]!.components(separatedBy: .whitespaces).joined()
        self.username = self.username.components(separatedBy: "-").joined()
        self.picture = store["picture"]
        self.objectMentin = "[@id:\(self.id!),@storename:\(self.username!)]"
    }
    
    init ( stores store : [String : String]) {
        
        self.id = store["id"]
        self.username = store["name"]
        self.picture = store["picture"]
        self.objectMentin = "[@id:\(self.id!),@storename:\(self.username!)]"
    }
    
    init ( byStore store : [String : AnyObject]) {
        
        let storeAPI = store["Store"] as! [String : AnyObject]
        
        self.id = storeAPI["id"] as! String
        self.username = storeAPI["name"] as! String
        self.picture = storeAPI["picture"] as! String
        self.objectMentin = "[@id:\(self.id!),@storename:\(self.username!)]"
        
    }
    
    
}
