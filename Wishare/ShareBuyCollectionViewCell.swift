//
//  ShareBuyCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 4/27/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import ActiveLabel

class ShareBuyCollectionViewCell: UICollectionViewCell {
   
    
    @IBOutlet weak var userThumbImageView: UIImageView!
    @IBOutlet weak var userNameStoreTopButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var moreImageView: UIImageView!
    
    
    @IBOutlet weak var storeThumbImageView: UIImageView!
    @IBOutlet weak var nameStoreButton: UIButton!
   
    @IBOutlet weak var storePostImageView: UIImageView!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likeShareBuyImageView: UIImageView!
    @IBOutlet weak var commentShareBuyImageView: UIImageView!
    @IBOutlet weak var shareShareBuyImageView: UIImageView!
    @IBOutlet weak var wishListShareBuyImageView: UIImageView!
    
    @IBOutlet weak var viewLikes: UIView!
    
    @IBOutlet weak var userNameStoreBottomButton: UIButton!
    
    @IBOutlet weak var likesStoreLabel: UILabel!
    @IBOutlet weak var commentStoreLabel: ActiveLabel!
    @IBOutlet weak var commentsStoreLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var likeEffectsImageView: UIImageView!

    @IBOutlet weak var collectionView: UICollectionView!

    // - CONSTRAINTS
    @IBOutlet weak var nameButtonToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var nameButtonToLineConstraints: NSLayoutConstraint!  // 750
    
    @IBOutlet weak var comentLabelToLikesConstraints: NSLayoutConstraint! // 999
    @IBOutlet weak var comentLabelToLineConstraints: NSLayoutConstraint!  // 750
    
    var delegateBase : WishareBasePostDelegate!
    var delegate : ShareBuyCollectionViewCellDelegate!
    var indexPathNow : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.shareView.layer.cornerRadius = 5
        self.shareView.layer.borderWidth = 1
        self.shareView.layer.borderColor = UIColor.darkGray.cgColor
        
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "ImagesLookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imagesLookCell")
        
        self.collectionView.backgroundView = nil
        self.collectionView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        
        let gestureRecognizerStorePostImageView = UITapGestureRecognizer(target: self, action: #selector(pressStorePostImageView))
        self.storePostImageView.addGestureRecognizer(gestureRecognizerStorePostImageView)
        
        
        // ================================================================
        

        let tapGestureRecognizerLikeImageView = UITapGestureRecognizer(target: self, action: #selector(tapLike))
        let tapGestureRecognizerCommentImageView = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        let tapGestureRecognizerShareImageView = UITapGestureRecognizer(target: self, action: #selector(tapShare))
        let tapGestureRecognizerDreamImageView = UITapGestureRecognizer(target: self, action: #selector(tapGift))
        let tapGestureRecognizerViewLikes = UITapGestureRecognizer(target: self, action: #selector(tapLikesView))
        let tapGestureRecognizerCommentShowLabel = UITapGestureRecognizer(target: self, action: #selector(tapComment))
        
        self.likeShareBuyImageView.addGestureRecognizer(tapGestureRecognizerLikeImageView)
        self.commentShareBuyImageView.addGestureRecognizer(tapGestureRecognizerCommentImageView)
        self.shareShareBuyImageView.addGestureRecognizer(tapGestureRecognizerShareImageView)
        self.wishListShareBuyImageView.addGestureRecognizer(tapGestureRecognizerDreamImageView)
        self.viewLikes.addGestureRecognizer(tapGestureRecognizerViewLikes)
        self.commentsStoreLabel.addGestureRecognizer(tapGestureRecognizerCommentShowLabel)
        
        
        let tapGestureRecognizerMoreImageView = UITapGestureRecognizer(target: self, action: #selector(tapMore))
        self.moreImageView.addGestureRecognizer(tapGestureRecognizerMoreImageView)
        
        let tapGestureRecognizerStoreShareThumbImageView = UITapGestureRecognizer(target: self, action: #selector(tapStoreShareThumb))
        let tapGestureRecognizerStoreNameShareButton = UITapGestureRecognizer(target: self, action: #selector(tapNameShare))
        
        self.storeThumbImageView.addGestureRecognizer(tapGestureRecognizerStoreShareThumbImageView)
        self.nameStoreButton.addGestureRecognizer(tapGestureRecognizerStoreNameShareButton)
        
        // ================================================================
        
        self.userNameStoreTopButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        self.userNameStoreBottomButton.addTarget(self, action: #selector(openUserButton(_:)), for: .touchUpInside)
        
        let tapGestureRecognizerUserThumbImageView = UITapGestureRecognizer(target: self, action: #selector(openUserImage))
        self.userThumbImageView.addGestureRecognizer(tapGestureRecognizerUserThumbImageView)
        
        
    }
    
    @objc func tapMore() {
        self.delegateBase.more(cell: self)
    }
    
    @objc func tapLike() {
        self.delegateBase.like(cell: self)
    }
    
    @objc func tapComment() {
        self.delegateBase.showComments(cell: self)
    }
    
    @objc func tapShare() {
        self.delegateBase.share(cell: self, indexPath: indexPathNow)
    }
    
    @objc func tapGift() {
        self.delegateBase.wishlist(cell: self)
    }
    
    @objc func tapLikesView() {
        self.delegateBase.showLikes(cell: self)
    }

    @objc func pressStorePostImageView() {
        self.delegate.storePostImage(indexPathNow)
    }
    
    @objc func tapStoreShareThumb () {
        self.delegateBase.thumbStorePostImageView(cell: self)
    }
    
    @objc func tapNameShare() {
        self.delegateBase.userNameStorePostLabel(cell: self)
    }
    
    @objc func openUserImage() {
        self.delegate.openUserImage(indexPathNow)
    }
    
    @objc func openUserButton ( _ sender : UIButton) {
        self.delegate.openUserButton(sender, indexPathNow)
    }

}

extension ShareBuyCollectionViewCell : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate.selectedItemShare(collectionView, indexPath, indexPathNow)
    }
    
}

extension ShareBuyCollectionViewCell : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate.numberOfItemsInSectionShare(self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.delegate.cellForItemShare(collectionView, indexPath, self)
    }
    
}

extension ShareBuyCollectionViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width, height: self.collectionView.bounds.height / 3)
    }
    
}

protocol ShareBuyCollectionViewCellDelegate {
    
    func selectedItemShare( _ collection : UICollectionView, _ indexPath : IndexPath, _ cellIndexPath : IndexPath)
    func cellForItemShare( _ collection : UICollectionView, _ indexPath : IndexPath, _ cellForLook : ShareBuyCollectionViewCell) -> UICollectionViewCell
    func numberOfItemsInSectionShare( _ cellForLook : ShareBuyCollectionViewCell) -> Int
    func storePostImage( _ indexPath : IndexPath)
    func openUserButton( _ sender : UIButton,  _ indexPath : IndexPath)
    func openUserImage( _ indexPath : IndexPath)
}
