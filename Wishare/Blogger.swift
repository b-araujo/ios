//
//  Blogger.swift
//  Wishare
//
//  Created by Wishare iMac on 4/12/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Blogger {
    
    var teenPosts : String?
    var onePost : String?
    var threePost : String?
    var fivePost : String?
    var eightPost : String?
    var active : String?
    var created : String?
    var email : String?
    var id : String?
    var level : String?
    var modified : String?
    var name : String?
    var password : String?
    var picture : String?
    var profile : String?
    
}
