//
//  ShowFilterSearchCategoryViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class ShowFilterSearchCategoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var itemShow : Int!
    var showValueItem : [String] = []
    var previewOption : [String : Any] = [:]
    var categoriesName : [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        
        
        for (key, item) in self.showValueItem.enumerated() {
            if item == "" {
                self.showValueItem.remove(at: key)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}

extension ShowFilterSearchCategoryViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.itemShow == 0 {
            
            //self.previewOption["category"] = [self.showValueItem[indexPath.row]]
            
            var categories : [String] = self.previewOption["category"] as! [String]
            
            if categories.contains(self.showValueItem[indexPath.row]) {
                // - REMOVER AQUI
                
                for (key, category) in categories.enumerated() {
                    
                    if category == self.showValueItem[indexPath.row] {
                        categories.remove(at: key)
                    }
                    
                }
                
            } else {
                categories.removeAll()
                categories.append(self.showValueItem[indexPath.row])
            }
            
            self.previewOption["category"] = categories
            
            
        } else if self.itemShow == 1 {
            
            //self.previewOption["size"] = [self.showValueItem[indexPath.row]]
            
            var sizes : [String] = self.previewOption["size"] as! [String]
            
            if sizes.contains(self.showValueItem[indexPath.row]) {
                // - REMOVER AQUI
                
                for (key, size) in sizes.enumerated() {
                    
                    if size == self.showValueItem[indexPath.row] {
                        sizes.remove(at: key)
                    }
                    
                }
                
            } else {
                sizes.append(self.showValueItem[indexPath.row])
            }
            
            self.previewOption["size"] = sizes
            
            
        } else if self.itemShow == 2 {
            
            var colors : [String] = self.previewOption["color"] as! [String]
            
            if colors.contains(self.showValueItem[indexPath.row]) {
                // - REMOVER AQUI
                
                for (key, color) in colors.enumerated() {
                    
                    if color == self.showValueItem[indexPath.row] {
                        colors.remove(at: key)
                    }
                    
                }
                
            } else {
                colors.append(self.showValueItem[indexPath.row])
            }
            
            self.previewOption["color"] = colors
            
        } else if self.itemShow == 3 {
            
            var brands : [String] = self.previewOption["brand"] as! [String]
            
            if brands.contains(self.showValueItem[indexPath.row]) {
                // - REMOVER AQUI
                
                for (key, brand) in brands.enumerated() {
                    
                    if brand == self.showValueItem[indexPath.row] {
                        brands.remove(at: key)
                    }
                    
                }
                
            } else {
                brands.append(self.showValueItem[indexPath.row])
            }
            
            
            self.previewOption["brand"] = brands
        }
        
        NotificationCenter.default.post(name: Notification.Name.init("updateFilterShowCategory"), object: self.previewOption)
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ShowFilterSearchCategoryViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.showValueItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        
        var optionSelected = "-1"
        print(self.itemShow)
        if self.itemShow == 0 {
            optionSelected = "category"
        } else if self.itemShow == 1 {
            optionSelected = "size"
        } else if self.itemShow == 2 {
            optionSelected = "color"
        } else {
            optionSelected = "brand"
        }
        
        
        for optionMemory in (self.previewOption[optionSelected] as! [String]) {
            print(self.showValueItem[indexPath.row])
            print(optionMemory)
            if self.showValueItem[indexPath.row] == optionMemory {
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
                imageView.contentMode = .scaleAspectFit
                imageView.image = UIImage(named: "check")
                cell.accessoryView = imageView
                cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                
            }
        }
        
        if self.itemShow == 0 {
            
            
            for category in self.categoriesName {
                if category.id! == self.showValueItem[indexPath.row] {
                    cell.textLabel?.text = category.name!
                }
            }
            
            
        } else {
            cell.textLabel?.text = self.showValueItem[indexPath.row]
        }
        
        
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        
        cell.addSubview(lineView)
        
        lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
        lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
        lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        return cell
        
    }
    
}
