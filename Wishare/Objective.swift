//
//  Objective.swift
//  Wishare
//
//  Created by Wishare iMac on 7/24/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Objective {
    
    var title : String?
    var description : String?
    var points : String?
    var image : String?
    var isClosed : Bool?
    var qtd : String?
    var difference : Int?
    var finalizedObjective : String?
    
    init ( _ obj : [String : AnyObject]) {
        
        self.title = obj["title"] as? String
        self.description = obj["description"] as? String
        self.points = obj["points"] as? String
        self.image = obj["image"] as? String
        
    }
    
}
