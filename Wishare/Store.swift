//
//  Store.swift
//  Wishare
//
//  Created by Wishare iMac on 4/26/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation

class Store : NSObject, NSCoding {
    
    var id : String?
    var name : String?
    var picture : String?
    
    // advance_commission vindo do campo "transfer_rate_op" (TROCAR NOMES DAS VARIAVEIS)
    var advance_commission : String?
    var following : Bool?
    var followers : String?
    var posts : [Post] = []
    var products : [Product] = []
    var order : Order = Order()
    
    var productTypes: String?
    var qtdPosts: String?
    
    init( postStore store : [String : AnyObject]) {
        
        self.id = store["id"] as? String
        self.name = store["name"] as? String
        self.picture = store["picture"] as? String
        self.advance_commission = store["transfer_rate_op"] as? String
        self.following = (store["following"] as? String == "true") ? true : false
        
    }
    
    init( byCategory store : [String : AnyObject], withPosts : Bool = false, posts : [[String : AnyObject]] = [] ) {
        
        let storeAPI : [String : AnyObject] = store["Store"] as! [String : AnyObject]
        
        self.id = storeAPI["id"] as? String
        self.name = storeAPI["name"] as? String
        self.picture = storeAPI["picture"] as? String
        self.advance_commission = storeAPI["transfer_rate_op"] as? String
        self.following = (storeAPI["following"] as? String == "true") ? true : false
        
        
        if withPosts {
            
            self.followers = storeAPI["followers"] as? String
            
            if posts.count > 0 {
                
                for post in posts {
                    self.posts.append(Post(post))
                }
                
            }
            
        }
        
    }
    
    init(storeForProfile store : [String:AnyObject], posts : [[String:AnyObject]]?) {
        
        if let storeZero: [String:AnyObject] = store["0"] as? [String:AnyObject] {
            self.following = (storeZero["ifollowing"] as? String)?.toBool()
            self.qtdPosts = storeZero["qtd_posts"] as? String
            self.followers = storeZero["following"] as? String

        }
        
        if let storeAPI: [String : AnyObject] = store["Store"] as? [String:AnyObject] {
            self.id = storeAPI["id"] as? String
            self.name = storeAPI["name"] as? String
            self.picture = storeAPI["picture"] as? String
            self.advance_commission = storeAPI["transfer_rate_op"] as? String
        }
        
        if let withPosts = posts {
            if withPosts.count > 0 {
                for post in withPosts {
                    self.posts.append(Post(post))
                }
            }
        }
    }
    
    init( commentStore store : [String : AnyObject]) {
        
        self.id = store["id"] as? String
        self.name = store["name"] as? String
        self.picture = store["picture"] as? String
        
        
    }
    
    init(id : String, name : String, picture : String, advance_commission : String) {
        self.id = id
        self.name = name
        self.picture = picture
        self.advance_commission = advance_commission
    }
    
    init(name : String?, picture : String?) {
        
        self.name = name
        self.picture = picture
        
    }
    
    init(id: String?, name: String?, picture: String?, followers : String?, productTypes: String) {
        self.id = id
        self.name = name
        self.picture = picture
        self.followers = followers
        self.productTypes = productTypes
    }
    
    
    required convenience init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let picture = aDecoder.decodeObject(forKey: "picture") as! String
        let advance_commission = aDecoder.decodeObject(forKey: "advance_commission") as! String
        self.init(id: id, name: name, picture: picture, advance_commission: advance_commission)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(picture, forKey: "picture")
        aCoder.encode(advance_commission, forKey: "advance_commission")
    }
    
    
}
