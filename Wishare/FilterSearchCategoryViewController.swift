//
//  FilterSearchCategoryViewController.swift
//  Wishare
//
//  Created by Wishare iMac on 6/8/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SwiftyAttributes

class FilterSearchCategoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var aplicarButton: UIButton!
    @IBOutlet weak var quantityProductsLabel: UILabel!
    @IBOutlet weak var clearBarButtonItem: UIBarButtonItem!
    
    var products : [Product] = []
    var result : [[String]] = []
    
    let attributes = RappleActivityIndicatorView.attribute(style: .apple, tintColor: UIColor.white, screenBG: UIColor.black)
    let categories : [String] = ["Categorias", "Tamanho", "Cores", "Marca"]
    var previewOption : [String : Any] = ["text": "", "category": [], "size": [], "color": [], "brand": [], "store": [], "orderBy": UserDefaults.standard.object(forKey: "orderByCategories") as! String, "type": UserDefaults.standard.object(forKey: "typeByCategories") as! String, "page" : "1", "featured" : false]
    var previewOptionClone : [String : Any]!
    var option : [String] = []
    var items : [[String]] = []
    let defaults = UserDefaults.standard
    var categoriesName : [Category] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        
        if let lastPreviewOption : [String : Any] = UserDefaults.standard.object(forKey: "previewOptionCategory") as? [String : Any] {
            self.previewOption = lastPreviewOption
        }
        
        
        if let word_category : String = defaults.object(forKey: "word_search_category") as? String {
            self.previewOption["text"] = word_category
        } else {
            self.previewOption["text"] = ""
        }
        
        if let category : String = defaults.object(forKey: "categoryStoreId") as? String {
            if category != "x" {
                self.previewOption["category"] = ["\(category)"]
            } else {
                self.previewOption["category"] = []
            }
        }
        
        if let store : String = defaults.object(forKey: "storeStoreId") as? String {
            self.previewOption["store"] = ["\(store)"]
        }
        
        if let featured : Bool = defaults.object(forKey: "featuredStore") as? Bool {
            self.previewOption["featured"] = featured
        }
        
        self.previewOptionClone = self.previewOption
    
        //OBSERVADOR (ADICIONAR PARA PASSAR LISTA DE PRODUTOS)
        NotificationCenter.default.addObserver(self, selector: #selector(updateFilter(notification:)), name: Notification.Name.init("updateFilterShowCategory"), object: nil)
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando...", attributes: attributes)
        
        
        WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], self.previewOption["brand"] as! [String], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String) { (categories : [String], size : [String], color : [String], brand : [String], products : [Product], tot : Int, categoriesName : [Category], fatherCategory : [String], childrenOneCategory : [String], childrenTwoCategory : [String], error : Error?) in
            
            if error == nil {
                
                self.categoriesName = categoriesName
                
                if (self.previewOption["size"] as! [String]).count != 0 || (self.previewOption["color"] as! [String]).count != 0 || (self.previewOption["category"] as! [String]).count != 0 {
                    
                    
                    WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], [], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String) { (categoriesFilter : [String], sizesFilter : [String], colorFilter : [String], brandFilter : [String], productsFilter : [Product], totFilter : Int, categoriesName : [Category], fatherCategoryFilter : [String], childrenOneCategoryFilter : [String], childrenTwoCategoryFilter : [String], error : Error?) in
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            
                            // =================================

                            let categoriesArray : [String] = self.previewOption["category"] as! [String]
                            
                            if categoriesArray.count > 0 && UserDefaults.standard.object(forKey: "categoryMother") as! String != "father" {
                                
                                if categoriesArray[0] != "p" {
                                    if fatherCategoryFilter.contains(categoriesArray[0]) || UserDefaults.standard.object(forKey: "categoryMother") as! String == "childrenOne" {
                                        self.items.append(childrenOneCategory)
                                        UserDefaults.standard.set("childrenOne", forKey: "categoryMother")
                                    } else {
                                        self.items.append(childrenTwoCategory)
                                        UserDefaults.standard.set("childrenTwo", forKey: "categoryMother")
                                    }
                                } else {
                                    self.items.append(fatherCategory)
                                    UserDefaults.standard.set("father", forKey: "categoryMother")
                                }

                            } else {
                                self.items.append(fatherCategory)
                                UserDefaults.standard.set("father", forKey: "categoryMother")
                            }
                            
                            //self.items.append(categoriesFilter)
                            // =================================
                            
                            self.items.append(sizesFilter)
                            self.items.append(color)
                            self.items.append(brandFilter)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(sizesFilter)
                            self.result.append(colorFilter)
                            self.result.append(brandFilter)
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                            
                        }
                        
                    }
                    
                } else {
                    
                    
                    WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, [], [], [], [], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String) { (categoriesOriginal : [String], sizesOriginal : [String], colorOriginal : [String], brandOriginal : [String], productsOriginal : [Product], totOriginal : Int, categoriesName : [Category],fatherCategoryOriginal : [String], childrenOneCategoryOriginal : [String], childrenTwoCategoryOriginal : [String], error : Error?) in
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            // =================================
                            
                            let categoriesArray : [String] = self.previewOption["category"] as! [String]
                            
                            if categoriesArray.count > 0 && UserDefaults.standard.object(forKey: "categoryMother") as! String != "father" {
                                
                                if categoriesArray[0] != "p" {
                                    if fatherCategory.contains(categoriesArray[0]) || UserDefaults.standard.object(forKey: "categoryMother") as! String == "childrenOne" {
                                        self.items.append(childrenOneCategory)
                                        UserDefaults.standard.set("childrenOne", forKey: "categoryMother")
                                    } else {
                                        self.items.append(childrenTwoCategory)
                                        UserDefaults.standard.set("childrenTwo", forKey: "categoryMother")
                                    }
                                } else {
                                    self.items.append(fatherCategory)
                                    UserDefaults.standard.set("father", forKey: "categoryMother")
                                }
                                
                            } else {
                                self.items.append(fatherCategory)
                                UserDefaults.standard.set("father", forKey: "categoryMother")
                            }
                            
                            //self.items.append(categories)
                            // =================================
                            
                            self.items.append(size)
                            self.items.append(color)
                            self.items.append(brandOriginal)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brandOriginal)
                            
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                            
                        }
                        
                    }
                    
                    
                    
                }
                
                
                
            }
            
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func updateFilter(notification : Notification) {
        
        let valueRequest = notification.object as! [String : Any]
        self.previewOption = valueRequest
        
        RappleActivityIndicatorView.startAnimatingWithLabel("Carregando", attributes: attributes)
        
        WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], self.previewOption["brand"] as! [String], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String) { (categories : [String], size : [String], color : [String], brand : [String], products : [Product], tot : Int, categoriesName : [Category], fatherCategory : [String], childrenOneCategory : [String], childrenTwoCategory : [String], error : Error?) in
            
            if error == nil {
                
                if (self.previewOption["size"] as! [String]).count != 0 || (self.previewOption["color"] as! [String]).count != 0 || (self.previewOption["category"] as! [String]).count != 0 {
                    
                    
                    WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], [], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String) { (categoriesFilter : [String], sizesFilter : [String], colorFilter : [String], brandFilter : [String], productsFilter : [Product], totFilter : Int, categoriesName : [Category], fatherCategoryFilter : [String], childrenOneCategoryFilter : [String], childrenTwoCategoryFilter : [String], error : Error?) in
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            
                            // =================================
                            let categoriesArray : [String] = self.previewOption["category"] as! [String]
                            
                            if categoriesArray.count > 0 && UserDefaults.standard.object(forKey: "categoryMother") as! String != "father" {
                                
                                if categoriesArray[0] != "p" {
                                    if fatherCategoryFilter.contains(categoriesArray[0]) || UserDefaults.standard.object(forKey: "categoryMother") as! String == "childrenOne" {
                                        self.items.append(childrenOneCategory)
                                        UserDefaults.standard.set("childrenOne", forKey: "categoryMother")
                                    } else {
                                        self.items.append(childrenTwoCategory)
                                        UserDefaults.standard.set("childrenTwo", forKey: "categoryMother")
                                    }
                                } else {
                                    self.items.append(fatherCategory)
                                    UserDefaults.standard.set("father", forKey: "categoryMother")
                                }
                                
                            } else {
                                self.items.append(fatherCategory)
                                UserDefaults.standard.set("father", forKey: "categoryMother")
                            }
                            
                            //self.items.append(categoriesFilter)
                            // =================================
                            
                            self.items.append(sizesFilter)
                            self.items.append(color)
                            self.items.append(brandFilter)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brand)
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                            
                        }
                        
                    }
                    
                } else {
                    
                    
                    WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, [], [], [], [], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String) { (categoriesOriginal : [String], sizesOriginal : [String], colorOriginal : [String], brandOriginal : [String], productsOriginal : [Product], totOriginal : Int, categoriesName : [Category], fatherCategoryOriginal : [String], childrenOneCategoryOriginal : [String], childrenTwoCategoryOriginal : [String], error : Error?) in
                        
                        if error == nil {
                            
                            self.items.removeAll()
                            // =================================

                            let categoriesArray : [String] = self.previewOption["category"] as! [String]
                            
                            if categoriesArray.count > 0 && UserDefaults.standard.object(forKey: "categoryMother") as! String != "father" {
                                
                                if categoriesArray[0] != "p" {
                                    if fatherCategory.contains(categoriesArray[0]) || UserDefaults.standard.object(forKey: "categoryMother") as! String == "childrenOne" {
                                        self.items.append(childrenOneCategory)
                                        UserDefaults.standard.set("childrenOne", forKey: "categoryMother")
                                    } else {
                                        self.items.append(childrenTwoCategory)
                                        UserDefaults.standard.set("childrenTwo", forKey: "categoryMother")
                                    }
                                } else {
                                    self.items.append(fatherCategory)
                                    UserDefaults.standard.set("father", forKey: "categoryMother")
                                }
                                
                            } else {
                                self.items.append(fatherCategory)
                                UserDefaults.standard.set("father", forKey: "categoryMother")
                            }
                            
                            //self.items.append(categories)
                            // =================================
                            
                            self.items.append(size)
                            self.items.append(color)
                            self.items.append(brandOriginal)
                            
                            self.result.removeAll()
                            self.result.append(categories)
                            self.result.append(size)
                            self.result.append(color)
                            self.result.append(brandOriginal)
                            
                            
                            self.products = products
                            self.tableView.reloadData()
                            
                            
                            if (self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0 {
                                
                                self.aplicarButton.backgroundColor = UIColor.lightGray
                                self.clearBarButtonItem.tintColor = UIColor.darkGray
                                self.aplicarButton.isEnabled = false
                                self.clearBarButtonItem.isEnabled = false
                                
                            } else {
                                
                                self.aplicarButton.backgroundColor = UIColor(red: 42/255, green: 117/255, blue: 185/255, alpha: 1)
                                self.clearBarButtonItem.tintColor = UIColor.black
                                self.aplicarButton.isEnabled = true
                                self.clearBarButtonItem.isEnabled = true
                                
                            }
                            
                            self.quantityProductsLabel.text = "\(tot)"
                            
                            
                            RappleActivityIndicatorView.stopAnimation()
                            
                        }
                        
                    }
                    
                    
                    
                }
                
                
                
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "filterShowSegue" {
            
            let senderValue : [AnyObject] = sender as! [AnyObject]
            
            let showFilterSearchViewController = segue.destination as! ShowFilterSearchCategoryViewController
            showFilterSearchViewController.showValueItem = senderValue[0] as! [String]
            showFilterSearchViewController.previewOption = senderValue[1] as! [String : Any]
            showFilterSearchViewController.itemShow = senderValue[2] as! Int
            showFilterSearchViewController.categoriesName = senderValue[3] as! [Category]
            
        }
        
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clear(_ sender: UIBarButtonItem) {
        
        if !((self.previewOption["category"] as! [String]).count == 0 && (self.previewOption["size"] as! [String]).count == 0 && (self.previewOption["color"] as! [String]).count == 0 && (self.previewOption["brand"] as! [String]).count == 0) {
            
            if self.previewOption["featured"] as! Bool == true {
                self.previewOption["category"] = []
            } else {
                self.previewOption["category"] = self.previewOptionClone["category"]
            }
            
            self.previewOption["size"] = []
            self.previewOption["color"] = []
            self.previewOption["brand"] = []
            
            RappleActivityIndicatorView.startAnimatingWithLabel("Carregando", attributes: attributes)
            
            WishareAPI.searchProductFilterByStore(self.previewOption["text"] as! String, self.previewOption["category"] as! [String], self.previewOption["size"] as! [String], self.previewOption["color"] as! [String], self.previewOption["brand"] as! [String], self.previewOption["store"] as! [String], self.previewOption["orderBy"] as! String, self.previewOption["type"] as! String, self.previewOption["featured"] as! Bool, self.previewOption["page"] as! String, { (categories : [String], size : [String], color : [String], brand : [String], products : [Product], tot : Int, categoriesName : [Category], fatherCategory : [String], childrenOneCategory : [String], childrenTwoCategory : [String], error : Error?) in
                
                if error == nil {
                    
                    self.items.removeAll()
                    // =================================
                    
                    let categoriesArray : [String] = self.previewOption["category"] as! [String]
                    
                    if categoriesArray.count > 0 && UserDefaults.standard.object(forKey: "categoryMother") as! String != "father" {
                        
                        if fatherCategory.contains(categoriesArray[0]) || UserDefaults.standard.object(forKey: "categoryMother") as! String == "childrenOne" {
                            self.items.append(childrenOneCategory)
                            UserDefaults.standard.set("childrenOne", forKey: "categoryMother")
                        } else {
                            self.items.append(childrenTwoCategory)
                            UserDefaults.standard.set("childrenTwo", forKey: "categoryMother")
                        }
                        
                        
                    } else {
                        self.items.append(fatherCategory)
                        UserDefaults.standard.set("father", forKey: "categoryMother")
                    }
                    
                    //self.items.append(categories)
                    // =================================
                    
                    self.items.append(size)
                    self.items.append(color)
                    self.items.append(brand)
                    self.tableView.reloadData()
                    self.products = products
                    NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdatedCategory"), object: self.products)
                    UserDefaults.standard.set(self.previewOption, forKey: "previewOptionCategory")
                    self.quantityProductsLabel.text = "\(tot)"
                    RappleActivityIndicatorView.stopAnimation()
                    
                    self.aplicarButton.backgroundColor = UIColor.lightGray
                    self.clearBarButtonItem.tintColor = UIColor.darkGray
                    self.aplicarButton.isEnabled = false
                    self.clearBarButtonItem.isEnabled = false
                    
                    
                }
                
            })
            
        }
        
    }
    
    @IBAction func apply(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: NSNotification.Name.init("productsResultUpdatedCategory"), object: self.products)
        UserDefaults.standard.set(self.previewOption, forKey: "previewOptionCategory")
        print(self.previewOption)
        self.dismiss(animated: true, completion: nil)
        
    }

}

extension FilterSearchCategoryViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != 0 {
            self.performSegue(withIdentifier: "filterShowSegue", sender: [self.items[indexPath.row], self.previewOption, indexPath.row, self.categoriesName])
        } else {
            self.performSegue(withIdentifier: "filterShowSegue", sender: [self.items[indexPath.row], self.previewOption, indexPath.row, self.categoriesName])
        }
        
        
    }
    
}

extension FilterSearchCategoryViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.items.count > 0 {
            
            var optionSelected = "-1"
            
            if indexPath.row == 0 {
                optionSelected = "category"
            } else if indexPath.row == 1 {
                optionSelected = "size"
            } else if indexPath.row == 2 {
                optionSelected = "color"
            } else if indexPath.row == 3 {
                optionSelected = "brand"
            }
            
            // - Verifica se o resultado nao é sem color, tamanha ou category e marca
            if self.result[indexPath.row].count == 1 {
                if self.result[indexPath.row][0] == "" {
                    let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "subCell")
                    
                    cell.textLabel?.text = self.categories[indexPath.row]
                    cell.isHighlighted = false
                    cell.isUserInteractionEnabled = false
                    cell.isSelected = false
                    cell.textLabel?.textColor = UIColor.lightGray
                    cell.detailTextLabel?.textColor = UIColor.darkGray
                    cell.detailTextLabel?.text = "Opções indisponíveis"
                    
                    let lineView = UIView()
                    lineView.translatesAutoresizingMaskIntoConstraints = false
                    lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                    
                    cell.addSubview(lineView)
                    
                    lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
                    lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
                    lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
                    
                    
                    return cell
                }
                
            }
            
            
            if optionSelected != "-1" {
                
                if (self.previewOption[optionSelected] as! [String]).count > 0 {
                    
                    let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "subCell")
                    
                    cell.isHighlighted = false
                    cell.isUserInteractionEnabled = true
                    cell.isSelected = true
                    
                    cell.textLabel?.text = self.categories[indexPath.row]
                    cell.accessoryType = .disclosureIndicator
                    
                    var words = ""
                    var attributesWord = "".withTextColor(UIColor.darkGray)
                    
                    
                    for word in (self.previewOption[optionSelected] as! [String])  {
                        
                        if words.characters.count == 0 {
                            
                            if !self.result[indexPath.row].contains(word) {
                                
                                if indexPath.row == 0 {
                                    
                                    var categorySelected = false
                                    
                                    for category in self.categoriesName {
                                        if category.id! == word && self.items[0].contains(category.id!){
                                            categorySelected = true
                                            words = "\(category.name!)"
                                            attributesWord = category.name!.withTextColor(UIColor.darkGray)
                                            //attributesWord = category.name!.withStrikethroughStyle(.styleSingle).withBaselineOffset(0)
                                        } else {
                                            cell.textLabel?.textColor = UIColor.darkGray
                                        }
                                    }
                                    
                                    if categorySelected {
                                        cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                                    } else {
                                        cell.textLabel?.textColor = UIColor.darkGray
                                    }
                                    
                                } else {
                                    
                                    cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                                    words = "\(word)"
                                    attributesWord = word.withStrikethroughStyle(.styleSingle).withBaselineOffset(0)
                                    
                                }
                                
                            } else {
                                
                                if indexPath.row == 0 {
                                    
                                    var categorySelected = false
                                    
                                    for category in self.categoriesName {
                                        
                                        if category.id! == word && self.items[0].contains(category.id!) {
                                            categorySelected = true
                                            words = "\(category.name!)"
                                            attributesWord = category.name!.withTextColor(UIColor.darkGray)
                                        } else {
                                            cell.textLabel?.textColor = UIColor.darkGray
                                        }
                                        
                                    }
                                    
                                    if categorySelected {
                                        cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                                    } else {
                                        cell.textLabel?.textColor = UIColor.darkGray
                                    }
                                    
                                } else {
                                    
                                    cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                                    words = "\(word)"
                                    attributesWord = word.withTextColor(UIColor.darkGray)
                                    
                                }
                                
                            }
                            
                            
                        } else {
                            
                            if !self.result[indexPath.row].contains(word) {
                                words = "\(words), \(word)"
                                attributesWord = attributesWord + ", ".withTextColor(UIColor.darkGray) + word.withStrikethroughStyle(.styleSingle).withBaselineOffset(0)
                            } else {
                                words = "\(words), \(word)"
                                attributesWord = attributesWord + ", ".withTextColor(UIColor.darkGray) + word.withTextColor(UIColor.darkGray)
                            }
                            
                            
                        }
                    }
                    
                    cell.detailTextLabel?.attributedText = attributesWord
                    
                    //cell.textLabel?.textColor = UIColor(red: 255/255, green: 204/255, blue: 7/255, alpha: 1)
                    cell.detailTextLabel?.textColor = UIColor.darkGray
                    
                    
                    let lineView = UIView()
                    lineView.translatesAutoresizingMaskIntoConstraints = false
                    lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                    
                    cell.addSubview(lineView)
                    
                    lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
                    lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
                    lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
                    
                    
                    return cell
                }
                
            }
            
            
        }
        
        
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.isHighlighted = false
        cell.isUserInteractionEnabled = true
        cell.isSelected = true
        
        cell.textLabel?.text = self.categories[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = UIColor.darkGray
        
        
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
        
        cell.addSubview(lineView)
        
        lineView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
        lineView.leftAnchor.constraint(equalTo: cell.leftAnchor).isActive = true
        lineView.rightAnchor.constraint(equalTo: cell.rightAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        return cell
        
    }
    
}
