//
//  StoreCollection.swift
//  Wishare
//
//  Created by Wishare on 20/03/18.
//  Copyright © 2018 Wishare iMac. All rights reserved.
//

import Foundation
import UIKit
import Nuke
import Kingfisher


protocol StoreCarouselProtocol {
    func productTapped(productId: String)
    func openStore(storeId: String)
    func askedForMore()
}

class StoreCarousel : UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var storeProfileImageView: UIImageView!
    
    @IBOutlet weak var followButton: RoundButton!
    
    @IBOutlet weak var storeLabel: UILabel!
    
    @IBOutlet weak var followersLabel: UILabel!
    
    @IBOutlet weak var productTypesLabel: UILabel!
    
    @IBOutlet weak var borderView: UIView!
    
    @IBOutlet weak var storeImage1: UIButton!
    
    @IBOutlet weak var storeImage2: UIButton!
    
    @IBOutlet weak var storeImage3: UIButton!
    
    @IBOutlet weak var loadMoreView: UIView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var yellowActivityIndicatorView: UIActivityIndicatorView!
    
    var delegate : StoreCarouselProtocol?

    
    var store : Store? {
        didSet {
            if let hasPicture = store?.picture, let url = URL(string: "\(urlBase)/files/store-logo/thumb/\(hasPicture)") {
                storeProfileImageView.kf.indicatorType = .activity
                storeProfileImageView.kf.setImage(with: url) { (image, error, cacheType, url) in
                    self.storeProfileImageView.image = image?.circleMask
                }
            }
            if let storeName = store?.name {
                storeLabel.text = storeName
            }
            if let followersNumber = store?.followers {
                followersLabel.text = followersNumber + " seguidores"
            }
            productTypesLabel.text = store?.productTypes
            if let following = store?.following {
                updateFollowButton(following)
            }
        }
    }
    
    var products : [Product]? {
        didSet {
            cleanProducts()
            if let products = products {
                if products.indices.contains(0) {
                    storeImage1.kf.setBackgroundImage(with: URL(string: products[0].picture!), for: .normal)
                }
                if products.indices.contains(1) {
                    storeImage2.kf.setBackgroundImage(with: URL(string: products[1].picture!), for: .normal)
                }
                if products.indices.contains(2) {
                    storeImage3.kf.setBackgroundImage(with: URL(string: products[2].picture!), for: .normal)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("StoreCarousel", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        followButton.titleLabel?.text = "Seguir"
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
        contentView.layer.shadowRadius = 5
        
        logoImageView.layer.cornerRadius = 10
        logoImageView.clipsToBounds = true
        
        yellowActivityIndicatorView.stopAnimating()
//        let pictureTapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(storeTapped))
//
//        let nameTapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(storeTapped))
//
//        let followersTapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(storeTapped))
//
//        let productsTapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(storeTapped))
//
//
//        self.storeProfileImageView.addGestureRecognizer(pictureTapGesture)
//
//        self.storeLabel.addGestureRecognizer(nameTapGesture)
//
//        self.followersLabel.addGestureRecognizer(followersTapGesture)
//
//        self.productTypesLabel.addGestureRecognizer(productsTapGesture)
        
        
        let storeTapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(storeTapped))
        
        self.borderView.addGestureRecognizer(storeTapGesture)
    }
    
    func cleanProducts() {
        storeImage1.setImage(nil, for: .normal)
        storeImage2.setImage(nil, for: .normal)
        storeImage3.setImage(nil, for: .normal)
    }
    
    func updateFollowButton(_ following: Bool) {
        if following {
            self.followButton.backgroundColor = UIColor.white
            self.followButton.setTitle("Seguindo", for: .normal)
            self.followButton.setTitleColor(UIColor.black, for: .normal)
            self.followButton.layer.borderWidth = 2.0
            self.followButton.layer.borderColor = UIColor.black.cgColor
        } else {
            self.followButton.backgroundColor = UIColor(red:0.21, green:0.21, blue:0.21, alpha:1.0)
            self.followButton.setTitle("Seguir", for: .normal)
            self.followButton.setTitleColor(UIColor.white, for: .normal)
            self.followButton.layer.borderWidth = 0.0
            self.followButton.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    @objc func storeTapped() {
        self.delegate?.openStore(storeId: (self.store?.id)!)
    }

    @IBAction func productOneTapped(_ sender: Any) {
        if (products?.indices.contains(0))! {
            if let productOne = products?[0] {
                self.delegate?.productTapped(productId: productOne.variationId!)
            }
        }
    }
    
    @IBAction func productTwoTapped(_ sender: Any) {
        if (products?.indices.contains(1))! {
            if let productTwo = products?[1] {
                self.delegate?.productTapped(productId: productTwo.variationId!)
            }
        }
    }
    
    @IBAction func productThreeTapped(_ sender: Any) {
        if (products?.indices.contains(2))! {
            if let productThree = products?[2] {
                self.delegate?.productTapped(productId: productThree.variationId!)
            }
        }
    }
    
    @IBAction func loadMoreClicked(_ sender: Any) {
        yellowActivityIndicatorView.startAnimating()
        loadMoreView.isHidden = true
        self.delegate?.askedForMore()
    }
    
    @IBAction func followClicked(_ sender: Any) {
        
        if followButton.titleLabel?.text == "Seguir" {
            if let id = self.store?.id {
                WishareAPI.followStore(id, true) { (result, error) in
                    if error == nil {
                        if let followed = result["success"] as? Bool {
                            self.store?.following = followed
                            self.followButton.backgroundColor = UIColor.white
                            self.followButton.setTitle("Seguindo", for: .normal)
                            self.followButton.setTitleColor(UIColor.black, for: .normal)
                            self.followButton.layer.borderWidth = 2.0
                            self.followButton.layer.borderColor = UIColor.black.cgColor
                        }
                    }
                }
            }
        }
        else {
            if let id = self.store?.id {
                WishareAPI.followStore(id, false) { (result, error) in
                    if error == nil {
                        if let unfollowed = result["success"] as? Bool {
                            self.store?.following = !unfollowed
                            self.followButton.backgroundColor = UIColor(red:0.21, green:0.21, blue:0.21, alpha:1.0)
                            self.followButton.setTitle("Seguir", for: .normal)
                            self.followButton.setTitleColor(UIColor.white, for: .normal)
                            self.followButton.layer.borderWidth = 0.0
                            self.followButton.layer.borderColor = UIColor.clear.cgColor
                        }
                    }
                }
            }
        }
    }
}
