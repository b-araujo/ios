//
//  UserSearchCollectionViewCell.swift
//  Wishare
//
//  Created by Wishare iMac on 5/23/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import UIKit

class UserSearchCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgroundUserImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userLabel.adjustsFontSizeToFitWidth = true
    }
    
    override func prepareForReuse() {
        userImageView.image = nil
        userLabel.text = ""
        backgroundUserImageView.image = #imageLiteral(resourceName: "background_perfil.jpg")
    }

}
