//
//  ButtonExtension.swift
//  Wishare
//
//  Created by Wishare iMac on 5/18/17.
//  Copyright © 2017 Wishare iMac. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

extension UIButton {
    
    
    private struct customProductId {
        static var productID: String? = nil
        static var productSizeID: String? = nil
        static var productSize: String? = nil
    }
    
    var productID : String? {
        get {
            //return customProductId.productID
            return objc_getAssociatedObject(self, &customProductId.productID) as? String
        }
        set {
            //customProductId.productID = newValue
            objc_setAssociatedObject(self, &customProductId.productID, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var productSizeID : String? {
        get {
            //return customProductId.productSizeID
            return objc_getAssociatedObject(self, &customProductId.productSizeID) as? String
        }
        set {
            //customProductId.productSizeID = newValue
            objc_setAssociatedObject(self, &customProductId.productSizeID, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var productSize : String? {
        get {
            //return customProductId.productSize
            return objc_getAssociatedObject(self, &customProductId.productSize) as? String
        }
        set {
            //customProductId.productSize = newValue
            objc_setAssociatedObject(self, &customProductId.productSize, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    
    
    
    @objc func set(image anImage: UIImage?, title: NSString, titlePosition: UIViewContentMode, additionalSpacing: CGFloat, state: UIControlState){
        self.imageView?.contentMode = .center
        self.setImage(anImage?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), for: state)
        
        
        positionLabelRespectToImage(title: title, position: titlePosition, spacing: additionalSpacing)
        
        self.titleLabel?.contentMode = .center
        self.setTitle(title as String?, for: state)
    }
    
    private func positionLabelRespectToImage(title: NSString, position: UIViewContentMode, spacing: CGFloat) {
        
        let imageSize = self.imageRect(forContentRect: self.frame)
        let titleFont = self.titleLabel?.font!
        let titleSize = title.size(withAttributes: [NSAttributedStringKey.font : titleFont!])
        
        var titleInsets : UIEdgeInsets
        var imageInsets : UIEdgeInsets
        
        switch (position){
        case .top:
            titleInsets = UIEdgeInsets(top: -(imageSize.height + titleSize.height + spacing), left: -(imageSize.width), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
        case .bottom:
            titleInsets = UIEdgeInsets(top: (imageSize.height + titleSize.height + spacing), left: -(imageSize.width), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
        case .left:
            titleInsets = UIEdgeInsets(top: 0, left: -(imageSize.width * 2), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: -(titleSize.width * 2 + spacing))
        case .right:
            titleInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -spacing)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            titleInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        self.titleEdgeInsets = titleInsets
        self.imageEdgeInsets = imageInsets
    }
    
}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}


extension UICollectionView {
    
    
    private struct customIsPrimary {
        static var isPrimary: String? = nil
    }
    
    var isPrimary : String? {
        get {
            //return customProductId.productID
            return objc_getAssociatedObject(self, &customIsPrimary.isPrimary) as? String
        }
        set {
            //customProductId.productID = newValue
            objc_setAssociatedObject(self, &customIsPrimary.isPrimary, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
   
}


class TableViewRowAction: UITableViewRowAction
{
    var image: UIImage?
    
    func _setButton(button: UIButton)  {
        
        if let image = image, let titleLabel = button.titleLabel {
            
            let labelString = NSString(string: titleLabel.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedStringKey.font: titleLabel.font])
            button.tintColor = UIColor.white
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            button.imageEdgeInsets.right = -titleSize.width
        }
    }
}

extension UIWindow {
    
    func set(rootViewController newRootViewController: UIViewController, withTransition transition: CATransition? = nil) {
        
        let previousViewController = rootViewController
        
        if let transition = transition {
            layer.add(transition, forKey: kCATransition)
        }
        
        rootViewController = newRootViewController
        
        if UIView.areAnimationsEnabled {
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                newRootViewController.setNeedsStatusBarAppearanceUpdate()
            }
        } else {
            newRootViewController.setNeedsStatusBarAppearanceUpdate()
        }
        
        if let transitionViewClass = NSClassFromString("UITransitionView") {
            for subview in subviews where subview.isKind(of: transitionViewClass) {
                subview.removeFromSuperview()
            }
        }
        if let previousViewController = previousViewController {
            previousViewController.dismiss(animated: false) {
                previousViewController.view.removeFromSuperview()
            }
        }
    }
}
